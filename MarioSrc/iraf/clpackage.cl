#{ CLPACKAGE.CL -- Package definitions file for the "clpackage" package.
# When this script is run, the current package is "clpackage", the default
# startup package.  NOTE -- See hlib$zzsetenv.def for additional environment
# definitions.

szprcache	= 4

# IRAF standard system package script task declarations.

task    dataio.pkg      = "dataio$dataio.cl"
task	images.pkg	= "images$images.cl"
task	language.pkg	= "language$language.cl"
task	lists.pkg	= "lists$lists.cl"
task	plot.pkg	= "plot$plot.cl"
task	proto.pkg	= "proto$proto.cl"
task	system.pkg	= "system$system.cl"
task	utilities.pkg	= "utilities$utilities.cl"

# Define the external (user-configurable) packages.
cl < hlib$extern.pkg

# Load dynamically-defined external packages.
if (access ("hlib$extpkg.cl") == yes)
    cl < hlib$extpkg.cl
;

# Load the SYSTEM package.  Avoid printing menu, but do not change the
# default value of the menus switch.

if (menus) {
    menus = no;  system;  menus = yes
} else
    system

logregen = no

keep
