#{ NHPPS.CL -- Parse args for execution parameters.
#
# This parses standard execution parameters from the command line
# and if not found uses NHPPS environment variables.  Any remaining
# arguments are pushed back into the args variable.  This uses the same
# option flags as in other NHPPS tasks as much as possible; e.g. -d for
# dataset name.
#
#	-a application		-> nhpps.application
#	-d dataset name		-> nhpps.dataset
#	-m module name		-> nhpps.module
#	-p pipeline name	-> nhpps.pipeline
#	-s status flag		-> nhpps.flag

# Application name.
i = strstr ("-a", args)
if ((i==1 && substr(args,i,i+2) == "-a ") ||
    substr(args,i-1,i+2) == " -a ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, application, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_SYS_NAME"))
    application = envget ("NHPPS_SYS_NAME")
else
    application = "None"
nhpps.application = application

# Pipeline name.
i = strstr ("-p", args)
if ((i==1 && substr(args,i,i+2) == "-p ") ||
    substr(args,i-1,i+2) == " -p ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, pipeline, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_SUBPIPE_NAME"))
    pipeline = envget ("NHPPS_SUBPIPE_NAME")
else
    pipeline = "None"
nhpps.pipeline = pipeline

# Module name.
i = strstr ("-m", args)
if ((i==1 && substr(args,i,i+2) == "-m ") ||
    substr(args,i-1,i+2) == " -m ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, module, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("NHPPS_MODULE_NAME"))
    module = envget ("NHPPS_MODULE_NAME")
else
    module = "None"
nhpps.module = module

# Dataset name.
i = strstr ("-d", args)
if ((i==1 && substr(args,i,i+2) == "-d ") ||
    substr(args,i-1,i+2) == " -d ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, dataset, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("OSF_DATASET"))
    dataset = envget ("OSF_DATASET")
else if (defvar("EVENT_NAME"))
    dataset = envget ("EVENT_NAME")
else
    dataset = "None"
nhpps.dataset = dataset

# Flag.
i = strstr ("-s", args)
if ((i==1 && substr(args,i,i+2) == "-s ") ||
    substr(args,i-1,i+2) == " -s ") {
    s1 = substr (args, i, 999)
    j = fscan (s1, s2, flag, line)
    if (i > 1)
	args = substr(args,1,i-1) // line
    else
        args = line
} else if (defvar("OSF_FLAG"))
    flag = envget ("OSF_FLAG")
else
    flag = "None"
nhpps.flag = flag

keep
