#{ MSCRED -- Mosaic CCD Reduction Package
# This is a minimal package definition for host CL scripts.
# It is needed in order for package parameters to be found when the
# package prefix is not given; i.e. =instrument.

cl < "mscred$lib/zzsetenv.def"
package mscred, bin = mscbin$

set	msccmb		= "mscsrc$ccdred/src/combine/"

task	_ccdtool	= "mscsrc$x_ccdred.e"
task	fcombine,
	scombine,
	zcombine	= "msccmb$x_combine.e"

clbye()
