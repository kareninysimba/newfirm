# MSCOTFFLAT --	 Convert processed flat to OTF flat.
#
# Currently the OTF is a directory of pixel list files.

procedure mscotfflat (input, output, template)

file	input			{prompt="Input processed mosaic flat field"}
file	output			{prompt="Output OTF flat field"}
file	template		{prompt="Template raw mosaic flat field"}
int	bin = 4			{prompt="Binning size"}
real	resolution=0.005	{prompt="Resolution"}

struct	*fd

begin
	file	flat, otf, tmplt, in, out, temp1, temp2, temp3
	string	extname, imageid, trimsec
	real	ccdmean
	int	naxis1, naxis2

	# Temporary file names.
	temp1 = mktemp ("tmp$iraf")
	temp2 = mktemp ("tmp") // ".pl"
	temp3 = mktemp ("tmp")

	# Query parameters.
	flat = input
	otf = output

	# Create output file (really a directory of pl files)
	mkdir (otf)

	# Create OTF flat for each extension.
	imextensions (template, output="file", index="1-", extname="",
	    extver="", lindex+, lname-, lver-, ikparams="", > temp1)
	fd = temp1
	while (fscan (fd, tmplt) != EOF) {
	    hselect (tmplt, "extname,imageid,naxis1,naxis2,trimsec", yes) |
		scan (extname, imageid, naxis1, naxis2, trimsec)

	    in = flat // "[" // extname // "]"
	    out= otf // "/flat" //imageid// ".pl"
	    printf ("mscotfflat: %s -> %s\n", in, out)

	    # Compress input extension to OTF pl of same size.
	    flatcompress (in, temp2, bin=bin, resolution=resolution)

	    # Expand OTF pl to raw extension size.
	    hselect (temp2, "ccdmean", yes) | scan (ccdmean)
	    mkpattern (temp3, output="", pattern="constant", option="replace",
		v1=ccdmean, v2=1., size=1, title="", pixtype="short", ndim=2,
		ncols=naxis1, nlines=naxis2, header=tmplt)
	    hedit (temp3, "extname", extname, add+, verify-, show-, update+)
	    hedit (temp3, "ccdmean", ccdmean, add+, verify-, show-, update+)
	    imcopy (temp2, temp3//trimsec, verbose=no)
	    imdelete (temp2, verify=no)
	    imcopy (temp3, out, verbose=no)
	    imdelete (temp3, verify=no)
	}
	fd = ""; delete (temp1, verify=no)
end
