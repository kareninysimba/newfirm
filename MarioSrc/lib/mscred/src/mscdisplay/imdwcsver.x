# Copyright(c) 1986 Association of Universities for Research in Astronomy Inc.

# Dummy routines.

int procedure imd_wcsver ()

begin
	return (0)
end

procedure imd_setmapping (reg, sx, sy, snx, sny, dx, dy, dnx, dny, objref)

char	reg[SZ_FNAME]				#i region name
real	sx, sy					#i source raster
int	snx, sny
int	dx, dy					#i destination raster
int	dnx, dny
char	objref[SZ_FNAME]			#i object reference

begin
	return
end


# IMD_QUERY_MAP --  Return the mapping information in the server for the
# specified WCS number.

int procedure imd_query_map (wcs, reg, sx,sy,snx,sny, dx,dy,dnx,dny, objref)

int	wcs					#i WCS number of request
char	reg[SZ_FNAME]				#o region name
real	sx, sy					#o source raster
int	snx, sny
int	dx, dy					#o destination raster
int	dnx, dny
char	objref[SZ_FNAME]			#o object reference

begin
	return (ERR)
end
