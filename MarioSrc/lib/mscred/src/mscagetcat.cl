# MSCAGETCAT -- Interface to ASTCAT.AGETCAT.

procedure mscagetcat (output, catalog, ra, dec, rawidth, decwidth)

file	output			{prompt="Output list"}
string	catalog			{prompt="Catalog"}
string	ra			{prompt="RA[J2000] (hours)"}
string	dec			{prompt="DEC[J2000] (degrees)"}
real	rawidth			{prompt="RA width (minutes)"}
real	decwidth		{prompt="DEC width (minutes)"}

begin
	agetcat ("pars", output, catalogs=catalog, standard=no,
	    update=no, verbose=no, catdb="astcat$lib/catdb.dat",
	    aregpars="", rcra=ra, rcdec=dec, rrawidth=rawidth,
	    rdecwidth=decwidth, rcsystem="J2000", rcraunits="hours",
	    rcdecunits="degrees", filter=yes, afiltpars="", fsort="",
	    freverse=no, fexpr="yes", fields="ra,dec,mag1,mag2", fnames="",
	    fntypes="", fnunits="", fnformats="", fosystem="J2000",
	    fira="ra", fidec="dec", foraunits="hours",
	    fodecunits="degrees", foraformat="%.2h", fodecformat="%.1h",
	    fixp="xp", fiyp="yp", fixc="xc", fiyc="yc", foxformat="%10.3f",
	    foyformat="%10.3f")
end
