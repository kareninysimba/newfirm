#{ MSCFINDER.CL -- Script to set up tasks in the MSCFINDER package

proto
#stsdas.motd = no; stsdas; stsdas.motd = yes
tables
ttools
#stplot
immatch

package	mscfinder

task	msctpeak	= mscfinder$msctpeak.cl
task	mktpeaktab	= mscfinder$mktpeaktab.cl
hidetask mktpeaktab

task	tpltsol		= mscfinder$tpltsol.cl
#task	dssfinder	= mscfinder$dssfinder.cl
task	logfile		= mscfinder$logfile.cl
#task	redo		= mscfinder$redo.cl
#task	tastrom		= mscfinder$tastrom.cl
#task	tfinder		= mscfinder$tfinder.cl
task	tvmark_		= mscfinder$tvmark_.cl
task	catpars		= mscfinder$catpars.par
task	selectpars	= mscfinder$selectpars.par
task	_qpars		= mscfinder$_qpars.par

task	tpeak		= mscfinder$x_finder.e

hidetask mktpeaktab, tpeak, tpltsol
hidetask logfile, tvmark_, catpars, selectpars, _qpars


clbye()
