# ACEDETECT parameter structure.
define	PAR_SZSTR	199		# Length of strings in par structure
define	PAR_LEN		229		# Length of parameter structure

define	PAR_IMLIST	Memi[$1+$2-1]	# List of images (2)
define	PAR_BPMLIST	Memi[$1+$2+1]	# List of bad pixel masks (2)
define	PAR_SKYLIST	Memi[$1+$2+3]	# List of skys (2)
define	PAR_SIGLIST	Memi[$1+$2+5]	# List of sigmas (2)
define	PAR_EXPLIST	Memi[$1+$2+7]	# List of sigmas (2)
define	PAR_GAINLIST	Memi[$1+$2+9]	# List of measurement gain maps (2)
define	PAR_SCALELIST	Memi[$1+$2+11]	# List of scales (2)
define	PAR_OMLIST	Memi[$1+14]	# List of object masks
define	PAR_INCATLIST	Memi[$1+15]	# List of input catalogs
define	PAR_OUTCATLIST	Memi[$1+16]	# List of output catalogs
define	PAR_CATDEFLIST	Memi[$1+17]	# List of catalog definitions
define	PAR_CATFILTER	Memc[P2C($1+18)] # Output catalog filter
define	PAR_LOGLIST	Memi[$1+119]	# List of log files
define	PAR_OUTSKYLIST	Memi[$1+120]	# List of output sky images
define	PAR_OUTSIGLIST	Memi[$1+121]	# List of output sigma images

define	PAR_SKY		Memi[$1+122]	# Sky parameters
define	PAR_DET		Memi[$1+123]	# Detection parameters
define	PAR_SPT		Memi[$1+124]	# Split parameters
define	PAR_GRW		Memi[$1+125]	# Grow parameters
define	PAR_EVL		Memi[$1+126]	# Evaluate parameters

define	PAR_OMTYPE	Memi[$1+127]	# Output object mask type
define	PAR_EXTNAMES	Memc[P2C($1+128)] # Extensions names
