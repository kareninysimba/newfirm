task test

procedure test ()

int	i, j
int	axno[2], axval[2]
double	x, y, r, d, ltv[2], ltm[2,2], r[2]
pointer	im, mw, ctlw, ctwl, immap(), mw_openim(), mw_sctran()

begin
	im = immap ("test", READ_ONLY, 0)
	mw = mw_openim (im)


	x = 1
	y = 1000
	ctlw = mw_sctran (mw, "logical", "world", 3)
	call mw_c2trand (ctlw, x, y, d, r)
	call mw_ctfree (ctlw)
	call printf ("%.1f %.1f %.2H %.1h\n")
	    call pargd (x)
	    call pargd (y)
	    call pargd (r)
	    call pargd (d)

#	call mw_gaxmap (mw, axno, axval, 2)
#	axno[1] = 2
#	axno[2] = 1
#	call mw_saxmap (mw, axno, axval, 2)
#	call mw_gltermd (mw, ltm, ltv, 2)
#	x = ltv[1]
#	ltv[1] = ltv[2]
#	ltv[2] = x
#	x = ltm[1,1]
#	ltm[1,1] = ltm[1,2]
#	ltm[1,2] = x
#	x = ltm[2,2]
#	ltm[2,2] = ltm[2,1]
#	ltm[2,1] = x
#	call mw_sltermd (mw, ltm, ltv, 2)
	
	call mw_swattrs (mw, 1, "axtype", "ra")
	call mw_swattrs (mw, 2, "axtype", "dec")

	call mw_gwtermd (mw, r, ltv, ltm, 2)
#	x = r[1]
#	r[1] = r[2]
#	r[2] = x
	x = ltv[1]
	ltv[1] = ltv[2]
	ltv[2] = x
	x = ltm[1,1]
	ltm[1,1] = ltm[1,2]
	ltm[1,2] = x
	x = ltm[2,2]
	ltm[2,2] = ltm[2,1]
	ltm[2,1] = x
	call mw_swtermd (mw, r, ltv, ltm, 2)

	x = 1
	y = 1000
	ctlw = mw_sctran (mw, "logical", "world", 3)
	call mw_c2trand (ctlw, x, y, r, d)
	call mw_ctfree (ctlw)
	call printf ("%.1f %.1f %.2H %.1h\n")
	    call pargd (x)
	    call pargd (y)
	    call pargd (r)
	    call pargd (d)

	ctwl = mw_sctran (mw, "world", "logical", 3)
	call mw_c2trand (ctwl, r, d, x, y)
	call mw_ctfree (ctwl)
	call printf ("%.1f %.1f %.2H %.1h\n")
	    call pargd (x)
	    call pargd (y)
	    call pargd (r)
	    call pargd (d)

	call mw_close (mw)
	call imunmap (im)
end
