# Detection parameter structure.
define	DET_NBP		32		# Maximum number of BP ranges
define	DET_BPLEN	(32*3)		# Length of BP ranges
define	DET_LEN		(62+2*DET_BPLEN)	# Length of parameter structure
define	DET_STRLEN	99		# Length of strings in structure

define	DET_CNV		P2C($1)		# Convolution string
define	DET_HSIG	Memr[$1+51]	# High detection sigma
define	DET_LSIG	Memr[$1+52]	# Low detection sigma
define	DET_HDETECT	Memi[$1+53]	# Detect above sky?
define	DET_LDETECT	Memi[$1+54]	# Detect below sky?
define	DET_NEIGHBORS	Memi[$1+55]	# Neighbor type
define	DET_MINPIX	Memi[$1+56]	# Minimum number of pixels
define	DET_SIGAVG	Memr[$1+57]	# Minimum average above sky in sigma
define	DET_SIGPEAK	Memr[$1+58]	# Minimum peak above sky in sigma
define	DET_FRAC2	Memr[$1+59]	# Fraction of difference relative to 2
define	DET_BPVAL	Memi[$1+60]	# Output bad pixel value
define	DET_SKB		Memi[$1+61]	# Parameters for sky update
define	DET_BPDET	Memi[$1+62]		# BP ranges
define	DET_BPFLG	Memi[$1+62+DET_BPLEN]	# BP ranges
