include	<acecat.h>
include	<acecat1.h>
include	<tbset.h>


procedure catrrecs (cat, filt, id)

pointer	cat			#I Catalog pointer
char	filt[ARB]		#I Filter string
int	id			#I Entry ID for indexing (or -1 for row)

int	i, index, nrows, nrecs, nalloc, tbpsta()
pointer	tbl, tp, recs, rec
bool	filter()

begin
	# Check for a catalog data structure.
	if (cat == NULL)
	    return

	# Free any existing records.
	call catrecs_free (cat)

	# Check for a catalog to read.
	tbl = CAT_INTBL(cat)
	if (tbl == NULL)
	    return
	tp = TBL_TP(tbl)

	# Check the index request for errors.
	if (id != -1) {
	    if (id < -1 || id > CAT_RECLEN(cat))
		call error (1, "Catalog index field not defined.")
	    else if (CAT_DEF(cat,id) == NULL)
		call error (1, "Catalog index field not defined.")
	    else if (CAT_TYPE(cat,id) != TY_INT)
		call error (1, "Catalog index field not integer.")
	}

	# Initially allocate a record array for the full catalog.
	nrows = tbpsta (tp, TBL_NROWS)
	nalloc = nrows
	call calloc (recs, nalloc, TY_POINTER)

	nrecs = 0
	rec = NULL
	do i = 1, nrows {
	    call catrrec (cat, rec, i)
	    if (!filter (cat, rec, filt))
		next

	    if (id >= 0) {
		index = RECI(rec,id)
		if (index < 1 || IS_INDEFI(index))
		    next
	    } else
	        index = nrecs + 1

	    if (index > nalloc) {
		nalloc = nalloc + 1000
		call realloc (recs, nalloc, TY_POINTER)
		call aclri (Memi[recs+nalloc-1000], 1000)
	    }

	    if (Memi[recs+index-1] == NULL) {
		if (id >= 0)
		    nrecs = max (index, nrecs)
		else
		    nrecs = nrecs + 1
	    }

	    Memi[recs+index-1] = rec
	    rec = NULL
	}

	call realloc (recs, nrecs, TY_POINTER)

	CAT_RECS(cat) = recs
	CAT_NRECS(cat) = nrecs
end


procedure catrrec (cat, rec, row)

pointer	cat			#I Catalog pointer
pointer	rec			#U Record pointer
int	row			#I Table row

int	id, type, func
pointer	tbl, tp, stp, sym, cdef, sthead(), stnext()

begin
	if (cat == NULL)
	    return

	tbl = CAT_INTBL(cat)
	if (tbl == NULL)
	    return
	tp = TBL_TP(tbl)
	stp = TBL_STP(tbl)

	if (rec == NULL)
	    call calloc (rec, CAT_RECLEN(cat), TY_STRUCT)

	for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
	    id = ENTRY_ID(sym)
	    type = ENTRY_TYPE(sym)
	    cdef = ENTRY_CDEF(sym)
	    func = ENTRY_FUNC(sym)

	    #if (id > 1000 || id == ID_APFLUX)
	    if (id > 1000)
		next

	    #switch (id) {
	    #case ID_FLAGS:
	#	REC_FLAGS(rec) = 0
	#	ifnoerr (call tbegtt (tp, cdef, row, CAT_STR(cat), CAT_SZSTR)) {
	#	    if (Memc[CAT_STRPTR(cat)] == 'M')
	#		SETFLAG(rec,REC_SPLIT)
	#	}
	#	next
	#    }
		
	    switch (type) {
	    case TY_INT:
		if (cdef == NULL)
		    RECI(rec,id) = INDEFI
		else iferr (call tbegti (tp, cdef, row, RECI(rec,id)))
		    RECI(rec,id) = INDEFI
	    case TY_REAL:
		if (cdef == NULL)
		    RECR(rec,id) = INDEFR
		else iferr (call tbegtr (tp, cdef, row, RECR(rec,id)))
		    RECR(rec,id) = INDEFR
#call eprintf ("%d %d %d: %g\n")
#call pargi (row)
#call pargi (id)
#call pargi (cdef)
#call pargr (RECR(rec,id))
	    case TY_DOUBLE:
		if (cdef == NULL)
		    RECD(rec,id) = INDEFD
		else iferr (call tbegtd (tp, cdef, row, RECD(rec,id)))
		    RECD(rec,id) = INDEFD
#call eprintf ("%d %d %d: %g\n")
#call pargi (row)
#call pargi (id)
#call pargi (cdef)
#call pargd (RECD(rec,id))
	    default:
		if (cdef == NULL)
		    RECT(rec,id) = EOS
		else iferr (call tbegtt (tp, cdef, row, RECT(rec,id), -type))
		    RECT(rec,id) = EOS
	    }

	    # Apply function.
	    call catrfunc (cat, func, rec, id, type)
	}
end


# CATRFUNC -- Apply a function.

procedure catrfunc (cat, func, rec, id, type)

pointer	cat			#I Catalog structure
int	func			#I Function ID
pointer	rec			#I Pointer to record structure
int	id			#I Field ID
int	type			#I Datatype of value

begin
	switch (func) {
	case FUNC_MAG:
	    switch (type) {
	    case TY_INT:
		if (!IS_INDEFI(RECI(rec,id)))
		    RECI(rec,id) = 10. ** (-0.4 * (RECI(rec,id) -
		        CAT_MAGZERO(cat)))
	    case TY_REAL:
		if (!IS_INDEFR(RECR(rec,id)))
		    RECR(rec,id) = 10. ** (-0.4 * (RECR(rec,id) -
		        CAT_MAGZERO(cat)))
	    case TY_DOUBLE:
		if (!IS_INDEFD(RECD(rec,id)))
		    RECD(rec,id) = 10. ** (-0.4 * (RECD(rec,id) -
		        CAT_MAGZERO(cat)))
	    }
	}
end
