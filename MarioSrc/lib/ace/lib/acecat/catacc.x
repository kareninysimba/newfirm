include	<acecat.h>


# CATACC -- Is the catalog accessible?  Currently this is a wrapper for tbtacc.

int procedure catacc (catname, mode)

char	catname[ARB]		#I Catalog name
int	mode			#I Mode

int	tbtacc()

begin
	return (tbtacc(catname))
end
