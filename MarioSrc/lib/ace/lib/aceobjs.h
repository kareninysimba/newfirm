# This file defines the object parameters.

# The following are the parameter ids which are the offsets into the object
# data structure.  Note that the first group of parameters are those
# determined during detection for potential objects.  The second group
# are parameters added after an object has been accepted.

define  ID_NUM           1 # i "" ""		/ Object number
define  ID_PNUM          2 # i "" ""		/ Parent number
define	ID_XPEAK	 3 # r pixels %.3f	/ X peak coordinate
define	ID_YPEAK	 4 # r pixels %.3f	/ Y peak coordinate
define	ID_FLUX		 5 # r counts ""	/ Isophotal flux (I - sky)
define	ID_NPIX		 6 # i pixels ""	/ Number of pixels
define	ID_NDETECT	 7 # i pixels ""	/ Number of detected pixels
define	ID_SIG		 8 # r counts ""	/ Sky sigma
define	ID_ISIGAVG	 9 # r sigma ""		/ Average (I - sky) / sig
define	ID_ISIGMAX	10 # r sigma ""		/ Maximum (I - sky) / sig
define	ID_ISIGAVG2	11 # r sigma ""		/ *Ref average (I - sky) / sig
define	ID_FLAGS	12 # 8 "" ""		/ Flags

define	ID_SKY		17 # r counts ""	/ Mean sky
define	ID_THRESH	18 # r counts ""	/ Mean threshold above sky
define	ID_PEAK		19 # r counts ""	/ Peak pixel value above sky
define	ID_FCORE	20 # r counts ""	/ Core flux (I - sky)
define	ID_APFLUX	21 # r counts ""	/ Aperture flux
define	ID_APFLUX1	22 # r counts ""	/ Aperture flux
define	ID_APFLUX2	23 # r counts ""	/ Aperture flux
define	ID_APFLUX3	24 # r counts ""	/ Aperture flux
define	ID_APFLUX4	25 # r counts ""	/ Aperture flux
define	ID_APFLUX5	26 # r counts ""	/ Aperture flux
define	ID_APFLUX6	27 # r counts ""	/ Aperture flux
define	ID_APFLUX7	28 # r counts ""	/ Aperture flux
define	ID_APFLUX8	29 # r counts ""	/ Aperture flux
define	ID_APFLUX9	30 # r counts ""	/ Aperture flux
define	ID_FRACFLUX	31 # r counts ""	/ Apportioned flux
define	ID_FRAC		32 # r "" ""		/ Apportioned fraction
define	ID_XMIN		33 # i pixels %.3f	/ Minimum X
define	ID_XMAX		34 # i pixels %.3f	/ Maximum X
define	ID_YMIN		35 # i pixels %.3f	/ Minimum Y
define	ID_YMAX		36 # i pixels %.3f	/ Maximum Y
define	ID_WX		38 # d pixels %.3f	/ X world coordinate
define	ID_WY		40 # d pixels %.3f	/ Y world coordinate
define	ID_PX		42 # d pixels %.3f	/ X physical coordinate
define	ID_PY		44 # d pixels %.3f	/ Y physical coordinate
define	ID_XAP		46 # r pixels %.3f	/ X aperture coordinate
define	ID_YAP		47 # r pixels %.3f	/ Y aperture coordinate
define	ID_X1		48 # r pixels %.3f	/ X centroid
define	ID_Y1		49 # r pixels %.3f	/ Y centroid
define	ID_X2		50 # r pixels ""	/ X 2nd moment
define	ID_Y2		51 # r pixels ""	/ Y 2nd moment
define	ID_XY		52 # r pixels ""	/ X 2nd cross moment
define	ID_R		53 # r pixels %.3f	/ R moment
define	ID_RI2		54 # r pixels %.3f	/ RI2 moment
define	ID_FWHM		55 # r pixels %.3f	/ FWHM estimate

define	ID_FLUXVAR	56 # r counts ""	/ *Variance in the flux
define	ID_XVAR		57 # r pixels ""	/ *Variance in X centroid
define	ID_YVAR		58 # r pixels ""	/ *Variance in Y centroid
define	ID_XYCOV	59 # r pixels ""	/ *Covariance of X and Y

# The following are derived quantities which have ids above 1000.

define	ID_A		1001 # r pixels %.3f	/ Semimajor axis
define	ID_B		1002 # r pixels %.3f	/ Semiminor axis
define	ID_THETA	1003 # r degrees ""	/ Position angle
define	ID_ELONG	1004 # r "" ""		/ Elongation = A/B
define	ID_ELLIP	1005 # r "" ""		/ Ellipticity = 1 - B/A
define	ID_R2		1006 # r pixels %.3f	/ Second moment radius
define	ID_CXX		1007 # r pixels ""	/ Second moment ellipse
define	ID_CYY		1008 # r pixels ""	/ Second moment ellipse
define	ID_CXY		1009 # r pixels ""	/ Second moment ellipse

define	ID_FLUXERR	1011 # r counts ""	/ Error in flux
define	ID_XERR		1012 # r pixels ""	/ Error in X centroid
define	ID_YERR		1013 # r pixels ""	/ Error in Y centroid
define	ID_AERR		1014 # r "" ""		/ Error in A
define	ID_BERR		1015 # r "" ""		/ Error in B
define	ID_THETAERR	1016 # r degrees ""	/ Error in THETA
define	ID_CXXERR	1017 # r pixels ""	/ Error in CXX
define	ID_CYYERR	1018 # r pixels ""	/ Error in CYY
define	ID_CXYERR	1019 # r pixels ""	/ Error in CXY

# The following are appication structure definitions based on the above
# object record description.

define	OBJ_DETLEN	17			# Length for candidate objects
define	OBJ_LEN		60			# Length for accepted objects

# Detection pass parameters.
define	OBJ_NUM		RECI($1,ID_NUM)		# Object number
define	OBJ_PNUM	RECI($1,ID_PNUM)	# Parent object number
define	OBJ_XPEAK	RECR($1,ID_XPEAK)	# X peak logical coordinate
define	OBJ_YPEAK	RECR($1,ID_YPEAK)	# Y peak logical coordinate
define	OBJ_FLUX	RECR($1,ID_FLUX)	# Isophotal flux (I - sky)
define	OBJ_NPIX	RECI($1,ID_NPIX)	# Number of pixels
define	OBJ_NDETECT	RECI($1,ID_NDETECT)	# Number of detected pixels
define	OBJ_SIG		RECR($1,ID_SIG)		# Sky sigma
define	OBJ_ISIGAVG	RECR($1,ID_ISIGAVG)	# Average (I - sky) / sig
define	OBJ_ISIGMAX	RECR($1,ID_ISIGMAX)	# Maximum (I - sky) / sig
define	OBJ_ISIGAVG2	RECR($1,ID_ISIGAVG2)	# Ref average (I - sky) / sig
define	OBJ_FLAGS	RECT($1,ID_FLAGS)	# Flags
define	OBJ_FLAG	RECC($1,ID_FLAGS,$2)	# Flag

define	OBJ_SKY		RECR($1,ID_SKY)		# Mean sky
define	OBJ_THRESH	RECR($1,ID_THRESH)	# Mean threshold above sky
define	OBJ_PEAK	RECR($1,ID_PEAK)	# Peak pixel value above sky
define	OBJ_FCORE	RECR($1,ID_FCORE)	# Core flux (I - sky)
define	OBJ_APFLUX	RECR($1,ID_APFLUX)	# aperture flux (I - sky)
define	OBJ_APFLUX1	RECR($1,ID_APFLUX1)	# aperture flux (I - sky)
define	OBJ_APFLUX2	RECR($1,ID_APFLUX2)	# aperture flux (I - sky)
define	OBJ_APFLUX3	RECR($1,ID_APFLUX3)	# aperture flux (I - sky)
define	OBJ_APFLUX4	RECR($1,ID_APFLUX4)	# aperture flux (I - sky)
define	OBJ_APFLUX5	RECR($1,ID_APFLUX5)	# aperture flux (I - sky)
define	OBJ_APFLUX6	RECR($1,ID_APFLUX6)	# aperture flux (I - sky)
define	OBJ_APFLUX7	RECR($1,ID_APFLUX7)	# aperture flux (I - sky)
define	OBJ_APFLUX8	RECR($1,ID_APFLUX8)	# aperture flux (I - sky)
define	OBJ_APFLUX9	RECR($1,ID_APFLUX9)	# aperture flux (I - sky)
define	OBJ_FRACFLUX	RECR($1,ID_FRACFLUX)	# Apportioned flux
define	OBJ_FRAC	RECR($1,ID_FRAC)	# Approtioned fraction
define	OBJ_XMIN	RECI($1,ID_XMIN)	# Minimum X
define	OBJ_XMAX	RECI($1,ID_XMAX)	# Maximum X
define	OBJ_YMIN	RECI($1,ID_YMIN)	# Minimum Y
define	OBJ_YMAX	RECI($1,ID_YMAX)	# Maximum Y
define	OBJ_WX		RECD($1,ID_WX)		# X world coordinate
define	OBJ_WY		RECD($1,ID_WY)		# Y world coordinate
define	OBJ_PX		RECD($1,ID_PX)		# X physical coordinate
define	OBJ_PY		RECD($1,ID_PY)		# Y physical coordinate
define	OBJ_XAP		RECR($1,ID_XAP)		# X aperture coordinate
define	OBJ_YAP		RECR($1,ID_YAP)		# Y aperture coordinate
define	OBJ_X1		RECR($1,ID_X1)		# X centroid
define	OBJ_Y1		RECR($1,ID_Y1)		# Y centroid
define	OBJ_X2		RECR($1,ID_X2)		# X centroid
define	OBJ_Y2		RECR($1,ID_Y2)		# Y centroid
define	OBJ_XY		RECR($1,ID_XY)		# X centroid
define	OBJ_R		RECR($1,ID_R)		# R moment
define	OBJ_RI2		RECR($1,ID_RI2)		# RI2 moment
define	OBJ_FWHM	RECR($1,ID_FWHM)	# FWHM estimate

define	OBJ_FLUXVAR	RECR($1,ID_FLUXVAR)	# Variance in flux
define	OBJ_XVAR	RECR($1,ID_XVAR)	# Variance in X centroid
define	OBJ_YVAR	RECR($1,ID_YVAR)	# Variance in Y centroid
define	OBJ_XYCOV	RECR($1,ID_XYCOV)	# Covariance of X and Y centroid


# Core radius.
define	RCORE 		 4.			# Radius for FCORE/XAP/YAP


# Flags
define	SZ_FLAGS	5		# Size of flag string

define	SPLIT		0		# Split flag
define	DARK		1		# Dark flag
define	EVAL		2		# Evaluated flag
define	GROW		3		# Grown flag
define	BP		4		# Bad pixel flag

# I/O Transformation functions.
define	FUNCS		"|MAG|"
define	FUNC_MAG	1		# Magnitude


define	NUMSTART	11		# First object number
