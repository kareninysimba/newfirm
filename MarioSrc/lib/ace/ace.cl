#{ ACE - Astronomical Cataloging Environment

tables.motd = no
tables
ttools

cl < "ace$lib/zzsetenv.def"
package ace, bin = acebin$

task	acecatmatch,
	acecopy,
	acegeomap,
	acesetwcs,
	detect,
	evaluate,
	overlay,
	skyimages	= acesrc$x_ace.e

task	objmasks	= acesrc$objmasks.cl
task	objmasks1	= acesrc$objmasks.par
hidetask objmasks1

# Development and testing tools.
set	acetools	= "acesrc$acetools/"

clbye
