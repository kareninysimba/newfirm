.help getcal Apr03 mario
.ih
NAME
getcal -- get or query calibration files from the data manager
.ih
SUMMARY
Get or query calibration files from the data manager based on parameters,
such as instrument, time, and configuration, specified as keyword=value
on the STDIN stream.
.ih
USAGE
getcal caltype output
.ih
PARAMETERS
.ls caltype
Calibration type requested.  The choices are:

.nf
    xtalk - crosstalk coefficients
      bpm - bad pixel mask
     zero - zero level
    dflat - dome flat
.fi
.le
.ls output
Output filename for the returned calibration.  A null filename may be
specified to query for the existance of a calibration using the status
value.  If the specified output file already exists it is checked to
see if it is the same as the calibration in the data manager.  If it
is then the file is not changed otherwise it is replaced by the file
from the data manager.  The output filename extension is used as in IRAF to
indicate the format.

In the current implementation it is assumed that the format is the same as
stored and the data manager does not need to know how to convert formats.
In order to be able to associate a header with crosstalk files the
crosstalk calibration format will be changed to a FITS file.
.le
.ih
STDIN
Each calibration stored in the data manager has a set of keywords associated
with it.  To select a calibration a subset of keywords and values are
specified which must match those of a potential calibration.  The exception
to this is the keyword MJD-OBS which matches the calibration with the nearest
earlier value.  The format will typically be FITS but it may be free
format requiring only a keyword, an equal sign, and a value.  The value
is quoted if it contains blanks.  Comments may follow which are ignored.
.ih
STATUS
The program exit status is used to return success or failure codes.
The codes are:

.nf
    0 - Successfully found and returned a calibration
    1 - No calibration found
    2 - Could not write calibration file
.fi

Note that if no output filename is specified a status of 0 indicates the
data manager has a calibration and a status of 1 indicates it does not.
.ih
DESCRIPTION
.ih
EXAMPLES
The following example gets calibrations for an observation with the MJD-OBS
as shown.  This illustrates that different calibrations require different
qualifying keywords.  Only the last example shows a check of the status
value though in practice each request will check the status value and
take some error action if needed.

.nf
    % getcal xtalk $MEFDATA/cal/XTALK20030102.fits
    MJD-OBS =       52641.77388943 / MJD of observation start
    DETECTOR= 'Mosaic2 '           / Detector
    % getcal zero $SIFDATA/cal/ZERO20030102_01.fits
    MJD-OBS =       52641.77388943 / MJD of observation start
    DETECTOR= 'Mosaic2 '           / Detector
    IMAGEID =                    1 / Image identification
    % getcal bpm $SIFDATA/cal/BPM20030102_01.fits
    MJD-OBS =       52641.77388943 / MJD of observation start
    DETECTOR= 'Mosaic2 '           / Detector
    IMAGEID =                    1 / Image identification
    % getcal dflat $SIFDATA/cal/DFLAT20030102_01.fits
    MJD-OBS =       52641.77388943 / MJD of observation start
    DETECTOR= 'Mosaic2 '           / Detector
    IMAGEID =                    1 / Image identification
    FILTER  = 'V Harris c6026    ' / Filter name(s)
    % echo $status
    0
.fi

.ih
SEE ALSO
putcal
.endhelp
