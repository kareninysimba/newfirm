#!/bin/env pipecl
#
# OBS -- Send observation status to monitor.

string	node, opusmon, temp

task $obsfind = "$!find $1 > $2"
task $touch = "$!touch"
servers

cd ("NHPPS_DIR_ROOT$obs/")
touch ("obstime")
sleep (1)
temp = mktemp (".obstime")
if (access (".obstime")) {
    obsfind (". -name \*___\* -cnewer .obstime -print", temp)
    delete (".obstime", verify-)
} else
    obsfind (". -name \*___\* -print", temp)
rename ("obstime", ".obstime", field="all")

opusmon = envget ("NHPPS_PORT_PM") // ":" // envget ("NHPPS_NODE_SB")
node = envget ("NHPPS_NODE_NAME")

cpmsgs (input=temp, output=opusmon, switchbd="", prefix=node//" ",
    nlines=100, wait=no, reporterr=no)
delete (temp, verify-)

logout 1
