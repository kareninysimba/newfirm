#!/bin/env pipecl
# 
# FTPSUBMIT -- Submit data products to FTP.

string  dataset, shortname, ifile, tmp

# Define tasks and packages.
task $ftpsubmit = "$!ftpsubmit"
task $psqlog = "$!psqlog"

# Set names and directories.
names ("ftp", envget("OSF_DATASET"))
dataset = names.dataset
shortname = names.shortname
ifile = dataset // ".ftp"
tmp = dataset//".tmp"

# Log start of module.
printf ("\n%s (%s): ", strupr(envget("NHPPS_MODULE_NAME")), dataset); time

# Log in PSQ.  This should be done through the data manager but this
# is a quick hack which assumes this module runs on the DM machine.

psqlog (shortname, "archived", "ftp active")

# Find input file. Look in indir and then in DTS.
cd (names.indir)
if (access(names.indir//ifile))
    cd (names.indir)
else {
    dataset = substr (dataset, 1, strldx("-",dataset)) // "dts"
    names ("dts", dataset)
    s1 = names.rootdir//"output/"//names.dataset
    if (access(s1)) {
        cd (s1)
	concat ("*.dpslist", > ifile)
	count (ifile) | scan (i)
	if (i > 0) {
	    list = ifile
	    while (fscan (list, s2) != EOF) {
	        i = strldx ("/", s2)
		if (i > 0)
		    s2 = substr (s2, i+1, 999)
		;
		print (s2, >> tmp)
	    }
	    list = ""; rename (tmp, ifile)
	} else {
	    files ("*.fits") | match ("stk1", stop+, > ifile)
	    count (ifile) | scan (i)
	    if (i == 0)
	        delete (ifile)
	    ;
	}
	;
    }
    ;
}
;

# Submit files.
ftpsubmit (ifile, YES, YES, tmp)

psqlog (shortname, "archived", "ftp done")

if (access(tmp))
    delete (tmp)
;
if (access(ifile))
    logout 0
else
    logout 1
