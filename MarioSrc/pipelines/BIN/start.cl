#!/bin/env pipecl
#
# START -- Start pipeline.
#
# This is parameterized for use by different pipelines which follow the
# same basic directory substructure.  Retriggering is allowed if the
# dataset has a 'w' in first position of the OSF entry.

string	dup = "remove"		# Action on duplicate (ignore|error|warn|remove)
string	pipe  = ""		# Pipeline ID
string	stage = ""		# Stage name

int	status = 1
string	dataset, indir, datadir, ilist, lfile

# Tasks and packages.
task $osf_test = "$!osf_test -a $1 -p $2 -f $3 -h $4 -c 0 -s w"
task $ln       = "$!ln -s $1 $2"
utilities
proto

# Get arguments.
names (envget ("NHPPS_SUBPIPE_NAME"), envget ("OSF_DATASET"))
pipe  = names.pipe
stage = envget ("NHPPS_MODULE_NAME")
i     = fscan (cl.args, dup, pipe, stage)

# Set filenames.
dataset = names.dataset
datadir = names.datadir
indir	= names.indir
lfile	= names.lfile
ilist	= indir // dataset // "." // pipe

# Create dataset directory if needed.
if (access (datadir) == NO) {
    mkdir (datadir)
    sleep 1
}
;
cd (datadir)

# Create a link to the uparm directory if needed.
if (access (names.uparm) == NO) {
    s1 = osfn ("uparm$")
    # Strip off trailing '/' from uparm$...
    if (substr (s1, strlen (s1), strlen (s1)) == '/')
	s1 = substr (s1, 1, strlen (s1)-1)
    ;
    s2 = osfn (names.uparm)
    # Strip off trailing '/' from names.uparm...
    if (substr (s2, strlen (s2), strlen (s2)) == '/')
	s2 = substr (s2, 1, strlen (s2)-1)
    ;
    ln (s1, s2)
}
;

# Log processing.
printf ("\n%s (%s): ", strupr (stage), dataset) | tee (lfile)
time | tee (lfile)
if (access(ilist))
    concat (ilist) | tee (lfile)
;

# Check for duplicate entries.
count (ilist) | scan (i)
sort (ilist) | uniq | count | scan (j)
if (i != j) {
    if (dup == "error") {
	status = 0
	sendmsg ("ERROR", "Duplicate entries", ilist, "VRFY")
    } else if (dup == "warn") {
	status = 1
	sendmsg ("WARNING", "Duplicate entries", ilist, "VRFY")
    } else if (dup == "remove") {
	#status = 2
	status = 1
	sendmsg ("WARNING", "Duplicate entries removed", ilist, "VRFY")
	list = ilist
	for (i=1; fscan(list,line)!=EOF; i+=1)
	    printf ("%d %s\n", i, line, >> "start.tmp")
	list = ""; delete (ilist)
	sort ("start.tmp", col=2,  > ilist); delete ("start.tmp")
	list = ilist; s1 = "@BEGIN@"
	while (fscan (list, i, line) != EOF) {
	    if (line != s1) {
		if (s1 != "@BEGIN@")
		    printf ("%d %s\n", j, s1, >> "start.tmp")
		;
		j = i; s1 = line
	    } else {
		j = min (i, j)
	        printf( "Removing duplicate entry:\n%s\n", s1 )
	    } 
	}
	printf ("%d %s\n", j, s1, >> "start.tmp")
	list = ""; delete (ilist)
	sort ("start.tmp", num+) | fields ("STDIN", "2-", > ilist)
	delete ("start.tmp")
    } else
	status = 1
} else
    status = 1

logout (status)
