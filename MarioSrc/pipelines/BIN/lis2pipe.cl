#!/bin/env pipecl
#
# LIS2PIPE -- Break up an input list into sublists based on list of patterns
# and IDs.  For generality the list is specified by a command to be
# executed on the list, though a common command is just to cat a list.
# The sublist are submitted to the target pipeline.

string	option				# Option
string	patcmd				# Pattern command
string	cpipe				# Calling pipeline
string	tpipe 				# Target pipeline
int	ntrig = 2			# Number to trigger here

int	status = 2
int	numtrig
string	dataset, module, indir, datadir, ilist, subfile, lfile
string	olist, patlist, temp

# Set the default from the application parameters or environment.
numtrig = int (envget("CoresPerNode"))
ntrig = numtrig

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' '' '' $5 2>&1 > $6; echo $? > $7"

# The calling pipeline is set from the environment and the target pipeline
# is set by an argument.

names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))

cpipe = names.pipe
if (fscan (cl.args, option, patcmd, tpipe, ntrig) < 3)
    logout 0
;

# Files and directories.
dataset = names.dataset
module = envget ("NHPPS_MODULE_NAME")
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
olist = module//"1.tmp"
patlist = module//"2.tmp"
temp = module//"3.tmp"
cd (datadir)

# Select input list.
if (option == "trigger")
    ilist = indir // dataset // "." // cpipe
else if (option == "datalist")
    ilist = datadir // dataset // ".list"

# Log start of processing.
printf ("\n%s (%s): ", module, dataset) | tee (lfile)
time | tee (lfile)

# Check input list.
if (access (ilist) == NO) {
    sendmsg ("ERROR", "Input list not found", ilist, "VRFY")
    logout 0
}
;

# Generate list of patterns and IDs.
task $patcmd = ("$!" // patcmd // " $(1)")
patcmd (ilist, > patlist)
count (patlist) | scan (i)
if (i == 0) {
    sendmsg ("ERROR", "No data found", "", "VRFY")
    delete (pipes)
    delete (patlist)
    logout 3
}
;

# Loop over each pattern and create subfiles and list of subfiles.
list = patlist
while (fscan (list, s1, s2) != EOF) {
    subfile = datadir // module // "_" // s2 // ".tmp"
    if (access(subfile)) {
	match (s1, ilist, > temp)
	count (temp) | scan (i)
	printf ("pattern='%s', found=%d\n", s1, i)
	concat (temp, subfile, append+)
	delete (temp)
    } else {
	match (s1, ilist, > subfile)
	count (subfile) | scan (i)
	printf ("pattern='%s', found=%d\n", s1, i)
	if (i == 0)
	    delete (subfile)
	else
	    pathname (subfile, >> olist)
    }
}
list =""; delete (patlist)

# Check if any data was found.
if (access (olist) == NO) {
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 1
}
;

# Submit to the target pipeline.
callpipe (cpipe, tpipe, "split", datadir//olist, ntrig, patlist, temp)
concat (temp) | scan (status)
concat (patlist)
delete (module//"*.tmp")

logout (status)
