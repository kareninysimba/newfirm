#!/bin/env pipecl
#
# Linearize an input array

int status=1
real coadd, fowler, scale
string infile, detector
real s

images

if (fscan (cl.args, infile) < 1) {
    printf ("Usage: linearize.cl infile\n")
    logout 0
}

# Create short version of the filename
s1 = substr (infile, strldx("/",infile)+1, strlstr(".fits",infile)-1)

# The final relation is:
#	Ncorr = Nmeas / (1 - s * Nmeas)
#
# In IRAF-speak, this might be represented with an 'imexpr' statement as:
#	imexpr "a/(1-s*a)" output.fits a=input.fits s=6.59e-06
#
# Values for s determined from the preliminary analysis of the  
# linearity sequence observations from 2008-02-13, by Mark Dickinson
#
#	im1:  s = 6.59e-06
#	im2:  s = 7.67e-06
#	im3:  s = 5.79e-06
#	im4:  s = 6.38e-06
#
# These coefficients are valid for a single coadd and for Fowler sampling
# of 1. If either of these is higher, then the counts are multiplied. For
# example, if for coadd=1 and Fowler=1 the counts are 10 ADU, then for
# coadd=2 and Fowler=3 the counts will be 60 ADU. To linearize the data
# properly, they have to first be scaled to coadd=1 and Fowler=1, linearized,
# and then scaled back.
#
# Let the scale factor be x. The relation correcting for coadds and 
# Fowler sampling then becomes
# NmeasN = Nmeas/x                    # Normalize to unit Fowler and coadd
# NcorrN = NmeasN / (1 - s * NmeasN)  # Apply the correction
# Ncorr  = NcorrN*x                   # Scale back to original Fowler/coadd
# Combined, this becomes:
# Ncorr = Nmeas / ( 1 - (s*Nmeas)/x )

# Determine the scale factor
fowler = 0
hselect( infile, "fsample", yes ) | scan( fowler )
coadd = 0
hselect( infile, "ncoadd", yes ) | scan( coadd )
scale = fowler*coadd
if (scale==0) {
    sendmsg( "ERROR", "FSAMPLE or NCOADDS could not be read", infile, "PROC" )
    status = 0
}
;

if (status==1) {

    # Get the detector name
    detector = ""
    hselect( infile, "detector", yes ) | scan( detector )

    # Default to no correction
    s = 0

    if (detector=="SN019")
        s = 6.59e-06
    else if (detector=="SN022")
        s = 7.67e-06
    else if (detector=="SN013")
        s = 5.79e-06
    else if (detector=="SN011")
        s = 6.38e-06
    ;

    if (s==0) {
        sendmsg( "ERROR", "No nonlinearity correction performed",
	    infile, "PROC" )
        status = 0
    } else {
        imexpr( "a/(1-(b*a/c) )", "output.fits", infile, s, scale )
        imdel( infile )
        imrename( "output.fits", infile )
        printf( "Ncorr=Nmeas/(1-%f*Nmeas/%d)\n",s,scale ) | scan( s1 )
        hedit( infile, "LINEAR", s1, add+, ver- )
    }

}
;

logout( status )
