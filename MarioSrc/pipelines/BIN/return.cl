#!/bin/env pipecl
#
# RETURN -- Wait for all items to return from a pipeline.
# 
# This procedure is triggered by each item returned from a target pipeline.
# A check is made against a list of expected trigger files in the dataset
# directory and when all are found an OSF stage is marked as completed.
# The list of expected trigger files and the target pipeline datasets are
# normally setup by the CALL procedure.  However, spontaneous returns
# from a child pipeline (cases where the child pipeline is triggered
# externally) where the nlist parameter is defined and alternate returns
# from the same pipeline will be setup automatically.
#
# The return includes both log and data information.
# 
# The naming convention is that the return trigger file has the form
# <dataset>[-<item>].<extn>trig where <dataset> is the parent dataset name
# in the calling pipeline, <item> is an optional integer sequence number
# assigned to each item processed by the target pipeline, and <extn> is
# the target pipeline identifier.
#
# The return data file has the name <dataset>[-<item>].<extn> and an optional
# log file has the name <dataset>[-<item>].<extn>log.  It is an error if
# the data file is missing.  The calling pipeline can trigger an error by
# returning only the trigger file.
#
#	STATUS
#	0	Trapped or untrapped error
#	1	Data returned successfully, waiting for more returns
#	2	Data returned successfully, all data returned successfully
#
# NOTE: We can't use the status return in the xml file to set the OSF
# flag because the dataset used is the return event and not dataset we want.
#
# NOTE: This code has locking features but it is not 100% reliable.
# Therefore, the NHPPS xml file should insure that only one instance of
# this routine can run at a particular stage.

string  cpipe			# Calling pipeline
string  tpipe			# Target pipeline
string	startid			# Start ID stage for osf_update
string	stageid			# Stage ID for osf_update
string	erract = "warn"		# Action to take on an error
int	nlist = INDEF		# Number to check if no rntlist
bool    final = NO		# Is this the final return stage of pipeline?
bool	move = NO		# Move remote files to local pipeline?
bool	deldata = NO		# Delete trigger data when complete?
string	textn = ""		# Target pipeline trigger extension
string	rextn = ""		# Return trigger file extension
string	aextn = ""		# Alternate return trigger file extension

int	status = 0
bool	err
string	event, rtnname, dataset, node, new, pipes, newnode, oldnode
string	indir, spool, datadir, rtndir, rtnlist, lfile, osf
int	space, nreturn
struct	*fd

string	pipeApp

# Get arguments.
if (fscan (cl.args, cpipe, tpipe, startid, stageid, erract, nlist, final, move,
    deldata, textn, rextn, aextn) < 4)
    logout (status)
;

# For now we allow the "move" argument to be overridden by an environment
# variable.

if (defvar ("NHPPS_RETURNDATA"))
    move = (strupr (envget ("NHPPS_RETURNDATA")) == "YES")
;

pipeApp = envget ("NHPPS_SYS_NAME")

# Set default extensions.
if (textn == "")
    textn = "." // tpipe
;
if (rextn == "")
    rextn = "." // tpipe
;
if (aextn == "")
    aextn = "." // tpipe
;

# Tasks and packages.
task $returndata = "$!returndata.cl $1"
task $osf_test = "$!osf_test -a $3 -p $1 -f $2"
task $osf_update = "$!osf_update -a $5 -p $1 -f $2 -c $3 -s $4"
task $osf_create = "$!osf_create -a $5 -p $1 -f $2 -c $3 -s $4"
task $ln = "$!ln -s $1 $2"
task $pipeselect = "$!pipeselect"
task $diff = "$!diff"
images

# Set filenames.
event = envget ("EVENT_NAME")
i = strldx ("/", event) + 1
j = strlstr (rextn//"proc", event) - 1
if (j < 0)
    j = 1000
rtnname = substr (event, i, j)
event = rtnname // rextn
i = strldx ("-", rtnname)
if (i > 0)
    dataset = substr (rtnname, 1, i-1)
else
    dataset = rtnname
names (cpipe, dataset)
indir = names.indir
spool = indir // "spool/"

# If the dataset directory is missing create it and an OSF entry. 
datadir = names.datadir
if (access(datadir) == NO) {
    mkdir (datadir)
    cd (datadir)
    ln (osfn("uparm$"), "uparm")
    osf_create (cpipe, dataset, startid, "w", pipeApp)
}
;
set (uparm = names.uparm)

# Log start of processing.
lfile = datadir // names.lfile
printf ("\nRETURN %s from %s (%s): ", rextn, tpipe, rtnname) | tee (lfile)
time | tee (lfile)

# Make a return list if one does not exits.  There are two cases.
# One is when the return list is defined by the nlist parameter.
# The other is for an alternate return.

cd (datadir)
rtnlist = "pipedata/" // cpipe // rextn
if (access(rtnlist)==NO)
    rtnlist = dataset // rextn
;
if (access(rtnlist)==NO) {
    s1 = dataset // textn
    if (nlist != INDEF) {
	for (i=1; i<=nlist; i+=1)
	    print (i, >> rtnlist)
    } else if (access(s1)==YES) {
        list = s1
	while (fscan (list, s2) != EOF) {
	    i = strlstr (textn, s2)
	    if (i > 0)
	        print (substr (s2, 1, i-1) // rextn, >> rtnlist)
	    ;
	}
	list = ""
    }
    ;
    concat (rtnlist)
}
;

# A return without an event file or a log file is an error.
err = (access (indir//event) == NO && access (indir//event//"log") == NO)

# Since we want to keep data flowing first deal with additional datasets
# to spool.  Note that we delete the returned event after triggering new
# datasets so that other modules (eg split.cl) don't trigger more.

cd (spool)
if (access (event//".node")) {
    rtnlist = dataset // rextn
    pipes = dataset // rextn

    if (defvar ("NHPPS_MIN_DISK"))
	pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, envget("NHPPS_MIN_DISK")) |
	    sort (> event//"1.tmp")
    else
	pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0) | sort ( > event//"1.tmp")
    type (event//"1.tmp")

    # Check for new datasets.
    dir ("@"//datadir//rtnlist) | scan (line)
    if (line != "no files found") {
	# Find dataset to send to pipeline that just returned.
	head (event//".node") | scan (node)
	if (erract == "resubmit")
	    delete (event//".node")
	;
	list = datadir//rtnlist
	for (i=0; fscan (list, new)!=EOF;) {
	    if (access(new//".node")==NO && access(new//".lock")==NO &&
	        access(new//".err")==NO && access (new)==YES) {
		touch (new//".lock")

		s2 = ""; s3 = ""
		#match (node, pipes) | scan (s3)
		match (node, event//"1.tmp") | scan (s3)
		if (access (pipes//".exclude"))
		    match (node, pipes//".exclude") | scan (s2)
		;
		if (s3 != s2) {
		    s2 = substr (new, 1, strlstr(rextn,new)-1)
		    head (new) | scan (s1)
#		    printf ("Submitting %s\n", s3//s1)
#		    copy (indir//"data/"//s1//textn, s3//s1//textn)
#		    if (aextn != textn) {
#			pathname (indir//s2//rextn, >> s3//"return/"//s1//aextn)
#			pathname (indir//s2//textn, >> s3//"return/"//s1//textn)
#		    } else
#			pathname (indir//s2//rextn, >> s3//"return/"//s1//textn)
#		    touch (s3//s1//textn//"trig")

		    printf ("Submitting %s\n", s3//s1)
		    print (s3, > new//".node")
		    sendtrig (indir//"data/"//s1//textn, indir//s2//rextn,
		        s3//s1//textn, s3//s1//textn//"com",
			s3//"return/"//s1//textn, s3//s1//textn//"trig")
		    if (aextn != textn)
			pathname (indir//s2//rextn, > s3//"return/"//s1//aextn)
		    ;
		}
		delete (new//".lock")
		i += 1
		if (i > 0)
		    break
		;
	    }
	    ;
	}
	list = ""

	# Check for new or dead pipelines.
	if (access (pipes//".lock") == NO) {
	    touch (pipes//".lock")
	    diff (pipes, event//"1.tmp", > event//"2.tmp")
	    j = 0
	    fd = event//"2.tmp"
	    while (fscan (fd, s1, node) != EOF) {
		j += 1

		# Check for excluded node.
		if (access (pipes//".exclude")) {
		    s2 = ""; match (node, pipes//".exclude") | scan (s2)
		    if (s2 != "")
		        next
		}
		;
	        
	        if (s1 == ">") {
		    i = 0
		    list = datadir//rtnlist
		    while (fscan (list, new) != EOF) {
			if (access (new//".node")==NO &&
			    access(new//".lock")==NO &&
			    access(new//".err")==NO && access (new)==YES) {
			    touch (new//".lock")
			    i += 1
			    s2 = substr (new, 1, strlstr(rextn,new)-1)
			    head (new) | scan (s1)
#			    copy (indir//"data/"//s1//textn, node//s1//textn)
#
#			    if (aextn != textn) {
#				pathname (indir//s2//rextn,
#				    >> node//"return/"//s1//aextn)
#				pathname (indir//s2//textn,
#				    >> node//"return/"//s1//textn)
#			    } else
#				pathname (indir//s2//rextn,
#				    >> node//"return/"//s1//textn)
#			    printf ("Add: %s\n", node)
#			    touch (node//s1//textn//"trig")

			    printf ("Add: %s\n", node)
			    print (node, > new//".node")
			    sendtrig (indir//"data/"//s1//textn,
			        indir//s2//rextn,
				node//s1//textn,
				node//s1//textn//"com",
				node//"return/"//s1//textn,
				node//s1//textn//"trig")
			    if (aextn != textn)
				pathname (indir//s2//rextn,
				    > node//"return/"//s1//aextn)
			    ;
			    delete (new//".lock")
			}
			;

			if (i > 2)
			    break
			;
		    }
		} else if (s1 == "<") {
		    # Check if pipeline is excluded because of diskspace.
		    if (defvar ("NHPPS_MIN_DISK")) {
			pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0) | sort ( > event//"3.tmp")
			s2 = ""; match (node, event//"3.tmp") | scan (s2)
			delete (event//"3.tmp")
			if (s2 != "")
			    next
		    }
		    ;

		    # Pipeline has died.  Remove the node files to cause
		    # retriggering on next event.  Note we don't look at
		    # any other dataset nor do we retrigger.  If no
		    # other returns occur for the dataset then the
		    # remaining datasets would have to be manually
		    # retriggered.

		    printf ("Pipeline %s is no longer available\n", node)
		    list = datadir//rtnlist
		    while (fscan (list, new) != EOF) {
		        s1 = ""; match (node, new//".node*") | scan (s1)
			if (s1 == node)
			    delete (new//".node")
			;
		    }
		}
	    }
	    fd = ""; delete (event//"2.tmp")

	    # Replace new list of pipelines.
	    if (j > 0)
		copy (event//"1.tmp", pipes)
	    ;
	    delete (pipes//".lock")
	}
	;
    } else {
        delete (pipes)
    }

    # Clean up.
    delete (event//"1.tmp")
    if (err) {
        rename (event//".node", event//".err")
    } else {
	head (event) | scan (s1)
	head (event//".node") | scan (node)
	if (deldata)
	    imdelete ("@"//indir//"data/"//s1//textn)
	;
	delete (indir//"data/"//s1//textn)
	delete (event)
	delete (event//".node")
    }
}
;

# Do nothing else for a resubmit.
if (erract == "resubmit")
    logout (1)
;

# Check for returned log file and set parameters.
cd (indir)
if (access (event//"log")) {
    type (event//"log") | tee (lfile)
    delete (event//"log")
}
;

# If there is no event file then take the following action.
# Remember that we can't nest a logout more than one level deep.

if (access (event) == NO && erract == "warn") {
    sendmsg ("WARNING", "No data returned", "", "RETURN")
    touch (event)
}
;

if (access (event) == NO) {
    sendmsg ("ERROR", "No data returned", "", "RETURN")
    osf_update (cpipe, dataset, startid, "E", pipeApp)
    logout (status)
}
;

# Move return list to data directory.
if (final) {
    # Append to list file if stage is last return from pipeline.
    if (move)
        returndata (event)
    ;
    copy (event, datadir, verbose-)
    concatenate (event, datadir//dataset//".list", append+)
} else
    copy (event, datadir, verbose-)

delete (event)

# Compare number of files in list with number available.
# NOTE: We can't remove the rtnlist because some other return may be
# running at the same time and we can have a conflict.

cd (datadir)
rtnlist = "pipedata/" // cpipe // rextn
if (access(rtnlist)) {
    dir (dataset//"//@"//rtnlist, long-, ncols=1, sort-, all-) | count | scan (nreturn)
} else {
    rtnlist = dataset // rextn
    if (access(rtnlist)==NO) {
	osf_update (cpipe, dataset, stageid, "w", pipeApp)
	status = 1
	logout (status)
    }
    ;
    dir ("@"//rtnlist, long-, ncols=1, sort-, all-) | count | scan (nreturn)
}

count (rtnlist) | scan (nlist)
printf ("  Finished %d/%d\n", nreturn, nlist)

if (nreturn != nlist) {
    status = 1
    logout (status)
}
;

# Update the OSF.
#osf_update (cpipe, dataset, stageid, "c", pipeApp)

# Print some information.
printf ("\nRETURN %s from %s (%s): ", rextn, tpipe, rtnname) | tee (lfile)
time | tee (lfile)
printf ("  Processing of %d items by %s pipeline completed\n", nreturn, tpipe) |
    tee (lfile)

status = 2
logout (status)
