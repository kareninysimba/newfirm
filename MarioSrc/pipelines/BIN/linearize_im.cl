#!/bin/env pipecl
#
# Linearize an input array using 2D linearity coefficients
#
# 
# History
#
# 	R. Swaters? --------	Created.
#	T. Huard   20081224		Revised to enable reading of 2D linearity
#							coefficients instead of constant coefficients.
#							(lincoeff is a string instead of real; changed
#							one of the conditions to reflect that lincoeff
#							is now a string)
#	Last Revised:	T. Huard  20081224  9:30pm


int status=1
string infile, detector, lineq, lincoeff
string a, b

file    caldir = "MC$"

images
servers

if (fscan (cl.args, infile) < 1) {
    printf ("Usage: linearize.cl infile\n")
    logout 0
}
;

# Create short version of the filename
s1 = substr (infile, strldx("/",infile)+1, strlstr(".fits",infile)-1)

# Get the detector name
detector = ""
hselect( infile, "detector", yes ) | scan( detector )

# Default to no correction
#lincoeff = 0  # No need to define default 'lincoeff'

# Get the linearity coefficient and equation from the calibration database
getcal( infile, "lincoeff", cm, caldir, 
    obstype="", detector="!instrume", imageid="!detector",
    filter="", exptime="", mjd="!mjd-obs")
if ( getcal.statcode == 0 ) {
    getcal( infile, "lineq", cm, caldir, 
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="", mjd="!mjd-obs")
}
;
if (getcal.statcode>0) { # Getcal was not successful
    sendmsg( "ERROR",
        "Getcal failed - no nonlinearity correction performed",
        s1, "CAL" )
    status = 0
} else { # Getcal was successful
    # Get the linearity coefficient from the header
    lincoeff = ""
    lineq = ""
    hsel( infile, "lincoeff,lineq", yes ) | scan( lincoeff, lineq )
   if ( lincoeff == "" ) { # No coefficient in the header
        sendmsg( "ERROR",
            "No lincoeff in header - no nonlinearity correction performed",
            s1, "CAL" )
        status = 0
    } else {
	if ( lineq == "" ) {
            sendmsg( "ERROR",
                "No lineq in header - no nonlinearity correction performed",
                s1, "CAL" )
            status = 0
        } else {
            # Apply the nonlinearity correction using the 
            # linearity imexpression database
            imexpr( lineq, "output.fits", a=infile, b=lincoeff,
                exprdb="pipedata/linearity.db", outtype="real" )
imstat (infile//",output.fits")
            imdel( infile )
            imrename( "output.fits", infile )
            printf( "Linearized: coefficient image: %s\n", lincoeff ) | scan( line )
            hedit( infile, "LINEAR", line, add+, ver- )
        }
    }
    ;
}

logout( status )
