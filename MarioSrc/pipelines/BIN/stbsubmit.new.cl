#!/bin/env pipecl
# 
# STBSUBMIT -- Submit data products to STB.

string  ilist
string	tmp
int	sleep = 0
bool	dellist = NO
bool	deldata = NO
bool	submit = YES

int	status = 1
string	node, path, outfile
struct dtnsanam, instrume, mjd, proctype, prodtype, obstype

# Load tasks and packages.
task $funpack = "$!funpack -D -v $(1)"
task $ckarchive = "$!ckarchive"
task $dtsproc = "$!dtsproc $(1) 2> $(2)"
task $rdtsproc = "$!ssh $3 plexec dtsproc $(1) 2> $(2)"
task $associations = "$!associations.cl $(1) >> $(2)"
images
servers
utilities

# Get arguments.
tmp = mktemp ("stbsubmit") // ".tmp"
if (fscan (args, ilist, sleep, dellist, deldata, tmp) < 1) {
    printf ("usage: stbsubmit listfile [sleep deletelist deletedata tempfile]\n")
    logout 0
}
;

# Set status in case of a crash so that stuff isn't deleted.
print (0, > tmp//".status")

# Check files.
if (access(ilist)==NO) {
    printf ("ERROR: Can't access input list file (%s)\n", ilist)
    print (0, > tmp//".status")
    logout 0
}
;
if (access(tmp)==YES) {
    printf ("ERROR: Temporary file already exists (%s)\n", tmp)
    print (0, > tmp//".status")
    logout 0
}
;

# Loop through the list.
list = ilist
while (fscan (list, s1) != EOF) {

    # Check if data were compressed after data transport.
    # We can submit the compressed MEF but not the resampled since
    # the iSTB required keywords are not in the primary header.
    if (access(s1)==NO && access(s1//".fz")==YES) {
	if (strstr("_r.fits",s1)>0 || strstr("_stk.fits",s1)>0)
	    funpack (s1//".fz")
	else
	    s1 += ".fz"
    }
    ;
    
    printf ("Sending %s to DTS...\n", s1)

    # Check for file.
    if (imaccess(s1//"[0]")==NO) {
#	status = 0
	printf ("Data product not found (%s)\n", s1)
	next
    }
    ;

    # Check for PL keywords.
    hselect (s1//"[0]", "$PLFNAME", yes) | scan (s2)
    if (s2 == "INDEF") {
        status = 0
	printf ("File is not a pipeline data product (%s)\n", s1)
        next
    }
    ;

    # Check for previous version. Because it is so hard to pass
    # through values with spaces we replace them with underscores and
    # let the database match any character.
    hselect (s1//"[0]", "$DTNSANAM", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (dtnsanam)
    hselect (s1//"[0]", "$instrume", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (instrume)
    hselect (s1//"[0]", "$MJD-OBS", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (mjd)
    hselect (s1//"[0]", "$OBSTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (obstype)
    hselect (s1//"[0]", "$PROCTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (proctype)
    hselect (s1//"[0]", "$PRODTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (prodtype)
    if (mjd == "INDEF")
	hselect (s1//"[1]", "$MJD-OBS", yes) |
	    translit ("STDIN", " ", "_", del-, col-) | scan (mjd)
    ;
    printf ("'%s %s %s %s %s %s'\n",
	dtnsanam, instrume, mjd, obstype, proctype, prodtype) | scan (line)
    s2 = ""; ckarchive (line) | scan (s2)
    if (s2 != "" && s2 != "ERROR") {
	status = 0
	if ( s2 == "ERROR" ) {
	    printf ("Error in (execution of) SQL query for %s\n", s1)
	} else {
	    printf ("Earlier version in archive (%s)\n", s2)
	    if (deldata)
		if ( stb_verify )
		    imrename( s1, stb_verifypath )
		else
		    imdelete (s1)
	    ;
	}
        next
    }
    ;

    # Set node and path.
    i = stridx ("!", s1)
    if (i > 0) {
        node = substr (s1, 1, i-1)
	path = substr (s1, i+1, 1000)
	i = stridx (".", node)
	if (i > 0)
	    node = substr (node, 1, i-1)
	;
	if (node == envget("HOST"))
	    node = ""
	;
    } else {
	node = ""
	path = s1
    }

    # Harvest association information and write to file.
    s2 = envget( "NHPPS_SYS_NAME" )
    outfile = envget( s2 // "_STB" ) // "/output/"
    s2 = ""
    s3 = substr( s1, strldx("/",s1)+1, 999 )
    for(i=1;i<=2;i+=1) {
        j = stridx( "_", s3 )
        s2 += substr( s3, 1, j )
	s3 = substr( s3, j+1, 999 )
    }
    outfile += substr( s2, 1, strlen(s2)-1 )
    associations( s1, outfile )

    ## (2014/11) New NSA file naming squeme:
    nsaname (path, ver)

    # Send to DTS queue.
    touch (tmp)
    if (submit) {
	if (node == "")
	    dtsproc (path, tmp)
	else
	    rdtsproc (path, tmp, node)
    }
    ;

    # Check status.
    #count (tmp) | scan (i)
    i = 0
    if (i == 0) {
	#printf ("OK\n", s1)
	if (submit) {
	    sleep (sleep)
	    if (deldata)
		if ( stb_verify )
		    imrename( s1, stb_verifypath )
		else
		    imdelete (s1)
	    ;
	}
	;
    } else {
	status = 0
	printf ("FAILED\n", s1)
	concat (tmp)
    }
    delete (tmp)
}
list = ""

# Clean up.
if (submit && status == 1 && dellist) {
#    if (deldata) {
#	dir ("@"//ilist) | scan (line)
#	if (line == "no files found")
#	    delete (ilist)
#	;
#    } else
	delete (ilist)
}
;

print (status, > tmp//".status")
logout (status)
