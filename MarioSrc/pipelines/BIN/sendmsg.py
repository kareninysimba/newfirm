#!/bin/env python

# a simple client to send messages to the message monitor
#

import re, socket, os, sys
from pipeutil import *

def send_message ( host, port, msg='', idstr=None, datasetId=None,
                   node=None, lvl=None, moduleId=None, verbose=False ):

    # Use only the node name, not the domain name
    imr1 = node.split ('.')
    node = imr1[0]
    
    pkt = composePacket ({'ID':idstr, 'NODE':node, 
                           'MODULE':moduleId, 
                           'LEVEL':lvl, 
                           'MESSAGE': msg, 'DATASET': datasetId}
                         )
    # print the message to the command line so it will end up in
    # the processing logs in MarioHome
    print( "%s %s: %s (for %s on %s in %s)" % (idstr, lvl, msg, datasetId, node, moduleId) )

    if (verbose):
        print "SEND to %s PACKET:\n%s" % (host, pkt)

    try:
        sendPacketTo (pkt, host, port, will_respond=False, verbose=verbose)
    except:
        # fail relatively quietly
        if (verbose):
            print( "%s:%s" % (sys.exc_type, sys.exc_value) )
        print( "Write to message monitor failed. Is it running?" )
        pass

    return


if __name__ == '__main__':

    DATASET_ARG_REGEX = re.compile("^-d.*$")
    ID_ARG_REGEX = re.compile("^-id$")
    MOD_ARG_REGEX = re.compile("^-mo.*$")
    NODE_ARG_REGEX = re.compile("^-n.*$")
    SRVR_ARG_REGEX = re.compile("^-ser.*$")
    PORT_ARG_REGEX = re.compile("^-port$")
    LVL_ARG_REGEX = re.compile("^-level$")
    VERBOSE_ARG_REGEX = re.compile("^-v.*$")

    DEFAULT_HOST = "localhost" 
    DEFAULT_PORT = 1313
    
    host = None
    port = None
    node = None
    idstr = None
    level = None
    moduleId = None
    datasetId = None
    message = ''
    verbose=False

    readDataSet = False
    readId = False
    readLvl = False
    readMod = False
    readNode = False
    readPort = False
    readSrv = False
    if (len (sys.argv) >= 2):
        for arg in sys.argv[1:]:
            if (DATASET_ARG_REGEX.match(arg)):
                readDataSet = True
            elif (PORT_ARG_REGEX.match(arg)):
                readPort = True
            elif (ID_ARG_REGEX.match(arg)):
                readId = True
            elif (SRVR_ARG_REGEX.match(arg)):
                readSrv = True
            elif (LVL_ARG_REGEX.match(arg)):
                readLvl = True
            elif (MOD_ARG_REGEX.match(arg)):
                readMod = True
            elif (NODE_ARG_REGEX.match(arg)):
                readNode = True
            elif (VERBOSE_ARG_REGEX.match(arg)):
                verbose = True
            elif (readDataSet):
                datasetId = arg
                readDataSet = False
            elif (readId):
                idstr = arg
                readId = False
            elif (readLvl):
                level = arg
                readLvl = False
            elif (readMod):
                moduleId = arg
                readMod = False
            elif (readNode):
                node = arg
                readNode = False
            elif (readPort):
                port = int(arg)
                readPort = False
            elif (readSrv):
                host = arg
                readSrv = False
            else:
                message = "%s%s " % (message, arg)
    else:
        print "%s [-server <msg_srv_name>|-port <msg_srv_port>|-id <msg_id>|-dataset <id>|-level lvl|-node <nodeid>|-module <module>|-v] <message string>" % sys.argv[0]
        sys.exit(0)

    # trim off trailing space
    message = message[:-1]

    if (port == None):
        port = DEFAULT_PORT

    if (host == None):
        host = DEFAULT_HOST

    if (node == None):
        node = DEFAULT_HOST

    send_message ( host, port, message, idstr=idstr, datasetId=datasetId,
                   node=node, lvl=level, moduleId=moduleId, verbose=verbose )

