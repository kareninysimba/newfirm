#!/bin/env pipecl
# 
# SPLIT -- Split MEF into pieces and trigger subpipeline.
#
# The pieces are put in the dataset directory and files are setup to
# trigger the subpipeline.  This routine triggers a few pieces for
# each available subpipeline and spools the rest for triggering by
# stage that responds to the return triggers.

string	cpipe			# Calling pipeline
string	tpipe			# Target pipeline
string	option			# Option
int	ntrig = 2		# Number to trigger here

int	nmef, status, numtrig
string	dataset, ifile, indir, datadir, lfile
string	tname, extname, s4, listoflists, outlist
string	temp1, temp2
struct	*fd2

# Set the default from the application parameters or environment.
numtrig = int (envget("CoresPerNode"))
ntrig = numtrig

# Tasks and packages.
images		# IMCOPY
utilities	# TRANSLIT
mscred
task mscextensions = "mscsrc$x_mscred.e"
cache mscred
task $callpipe = "$!call.cl $1 $2 $3 '$4' $5 2>&1 > $6; echo $? > $7"

# Get arguments.
if (fscan (cl.args, cpipe, tpipe, option, ntrig) < 3) {
    printf ("Usage: split.cl cpipe tpipe option [ntrig]\n")
    logout 0
}
;

# Directories and file names.
names (cpipe, envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
set (uparm = names.uparm)
ifile = indir // dataset // "." // cpipe
lfile = datadir // names.lfile
set (pipedata = names.pipedata)
temp1 = "splitc1.tmp"
temp2 = "splitc2.tmp"
listoflists = indir//dataset//".lol"
outlist = indir//"data/"//dataset//"_*.ddrk"

# Work in data directory.
cd (datadir)

# Initialize for a restart.
delete (temp1//","//temp2//","//listoflists//","//outlist, >& "dev$null")

# Log start of processing.
printf ("\nSPLIT (%s): ", dataset | tee (lfile)
time | tee (lfile)

# Extract image extensions.
s2 = ""
if (access ("pipedata$extpat.dat"))
    head ("pipedata$extpat.dat") | scan (s2)
;
head (ifile) | scan (s1)
i = strlstr (".fits", s1)
if (i > 0)
    s1 = substr (s1, 1, i-1)
;
mscextensions (s1, extname=s2, lindex-, lname+, lver-) |
    translit ("STDIN", "[]", " ", > "split.ext")
#match( "im2", "split.ext2", stop+, >> "split.ext" )
#delete( "split.ext2" )
# split.ext contains, for the first image in the input list,
# a list of all extensions in the 2nd column (the 1st contains
# the file name of the first image).

b1 = no; j = 0
count (ifile) | scan (nmef)
list = "split.ext" 
while (fscan (list, s1, extname) != EOF) { # loop over all extensions
    j += 1

    # Set names.
    printf ("%s_%s\n", dataset, extname) | scan (tname)
    outlist = indir//"data/"//tname//".ddrk"

    # Split extension(s) and create @file.
    # The nmef==0 is a hack to prevent this part of the code from
    # running - the case of a list of one or a list of many should
    # be treated the same in the NEWFIRM pipeline, where slitc.cl
    # is used.
    if (nmef == 0) { # If the input list has only one MEF file, then:
	printf ("%s_%s.fits\n", substr(s1,strldx ("/$",s1)+1,1000), extname) |
	    scan (s2)
	if (j == 1) {
	    printf ("%s_00\n", dataset) | scan (s4)
	    if (imaccess(s4))
	        imdelete (s4)
	    ;
	    imcopy (s1//"[0]", s4)
	}
	;
	if (imaccess(s2))
	    imdelete (s2)
	;
	imcopy (s1//"["//extname//"]", s2, verbose+)
	pathname (s2, >> outlist)
    } else { # Multiple MEF files in input list, so loop over all:
	k = 0
	fd2 = ifile
	while (fscan (fd2, s1) != EOF) { # Loop over all MEF files:
	    if (strlstr(".fits",s1) > 0)
	        s1 = substr (s1, 1, strlstr(".fits",s1)-1)
	    ;
	    s3 = substr(s1,strldx ("/$",s1)+1,1000)
	    printf ("%s_%s.fits\n", s3, extname) | scan (s2)
	    if (j == 1) {
		k += 1 
		printf ("%s_00\n", s3 ) | scan (s4)
		if (imaccess(s4))
		    imdelete (s4)
		;
		imcopy (s1//"[0]", s4)
	    }
	    ;
	    if (imaccess(s2))
	        imdelete (s2)
	    ;
	    imcopy (s1//"["//extname//"]", s2, verbose+)
	    pathname (s2, >> outlist)
	}
	fd2 = ""
    }

    # Append the list for the current extension to the list of lists
    printf ( "%s\n", outlist, >> listoflists )

}
list = ""

callpipe( cpipe, tpipe, option, listoflists, ntrig, temp1, temp2 )
concat (temp2) | scan (status)
concat (temp1)

printf("Callpipe returned %d\n", status)

logout (status)
