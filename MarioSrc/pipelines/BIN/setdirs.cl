# SETDIRS -- Set pipeline directories.
#
# This requires the SERVERS package, CP task, LN task, "cm" variable, the
# uparm variable, and the pipedata variable.  The latter two may be absent.
# Note we make a copy for "uparm" because we don't know yet if the files
# are read only, otherwise a link could be used.
#
# The default MJD is used when this routine is called before the DIRVERIFY.

procedure setdirs (images)

string	images			{prompt="List of images defining detector and MJD"}
string	extn = ""		{prompt="Extension to append to input"}
string	udetector = "!DETECTOR"	{prompt="Detector/instrument for uparm"}
string	ufilter = ""		{prompt="Filter for uparm"}
string	umatch = ""		{prompt="Match string for uparm"}
string	umjd = "!MJD-OBS"	{prompt="MJD for uparm"}
string	pdetector = "!DETECTOR"	{prompt="Detector/instrument for pipedata"}
string	pfilter = ""		{prompt="Filter for pipedata"}
string	pmatch = ""		{prompt="Match string for pipedata"}
string	pmjd = "!MJD-OBS"	{prompt="MJD for pipedata"}
file	caldir = "MC$"		{prompt="Calibration directory"}
string	mjddef = "50000"	{prompt="Default MJD"}
bool	verbose = yes		{prompt="Verbose?"}

struct	*fd

begin
	int	status = 0
	string	dir, fname, subdir
	file	image

	# Check for no image list.
	if (images == "")
	    return

	# Need to make sure getcal is cached because this routine is
	# called before a dataset specific uparm has been created
	# since that is the purpose of this routine.
	cache getcal

	# Expand list.
	cache sections
	sections (images, option="fullname", > "setdirs.tmp")

	# UPARM
	fd = "setdirs.tmp"; dir = ""
	while (fscan (fd, image) != EOF) {
	    image += extn

	    if (verbose)
	        printf ("    Checking %s for uparm...", image)
	    
	    # Check for existing header keyword and calibration.
	    dir = ""; hselect (image, "uparm", yes) | scan (dir)
	    if (dir != "" && access(dir)) {
	        if (verbose)
		    printf ("%s\n", dir)
		if (strstr("ccdsum",umatch)==0)
		    break
	    }
	    if (verbose)
	        printf ("not found or need to check again\n")
		
	    # Get calibration.
	    getcal (image, "uparm", cm, caldir, obstype="",
		detector=udetector, imageid="", filter=ufilter,
		exptime="", match=umatch, mjd=umjd, > "dev$null")
	    if (getcal.statcode != 0 && mjddef != "")
		getcal (image, "uparm", cm, caldir, obstype="",
		    detector=udetector, imageid="", filter=ufilter, exptime="",
		    match=umatch, mjd=mjddef, > "dev$null")
	    ;
	    if (getcal.statcode == 0) {
		dir = getcal.value
	        if (verbose)
		    printf ("    getcal returns %s\n", dir)
		break
	    } else
	        printf ("    WARNING: %d %s\n", getcal.statcode, getcal.statstr)
	}
	fd = ""

	# Check if a calibration was not found.
	if (dir == "" || !access(dir)) {
	    #delete ("setdirs.tmp")
	    sendmsg ("ERROR","Getcal failed to set uparm", dir, "CAL")
	    error (1, "Getcal failed for uparm")
	}
	    
	# Make a copy if needed.
	if (defvar("uparm") && !access ("uparm$")) {
	    fname = osfn ("uparm$")
	    fname = substr(fname,1,strlen(fname)-1)
	    if (verbose)
	        printf ("    copy %s -> %s\n", osfn(dir), fname)
	    cp (osfn(dir), fname)
	} else if (verbose)
	    printf ("    directory %s already exists\n", osfn ("uparm$"))
	    
	# PIPEDATA
	fd = "setdirs.tmp"; dir = ""
	while (fscan (fd, image) != EOF) {
	    image += extn

	    if (verbose)
	        printf ("    Checking %s for pipedata...", image)
	    
	    # Check for existing header keyword and calibration.
	    dir = ""; hselect (image, "pipedata", yes) | scan (dir)
	    if (dir != "" && access(dir)) {
	        if (verbose)
		    printf ("%s\n", dir)
	        break
	    }
	    if (verbose)
	        printf ("not found\n")
		
	    # Get calibration.
	    getcal (image, "pipedata", cm, caldir, obstype="",
		detector=pdetector, imageid="", filter=pfilter,
		exptime="", match=pmatch, mjd=pmjd, > "dev$null")
	    if (getcal.statcode != 0 && mjddef != "")
		getcal (image, "pipedata", cm, caldir, obstype="",
		    detector=pdetector, imageid="", filter=pfilter, exptime="",
		    match=pmatch, mjd=mjddef, > "dev$null")
	    ;
	    if (getcal.statcode == 0) {
		dir = getcal.value
	        if (verbose)
		    printf ("    getcal returns %s\n", dir)
		break
	    } else
	        printf ("    WARNING: %d %s\n", getcal.statcode, getcal.statstr)
	}
	fd = ""; delete ("setdirs.tmp")

	# Check if a calibration was not found.
	if (dir == "" || !access(dir)) {
	    sendmsg ("ERROR","Getcal failed to set pipedata", "", "CAL")
	    error (2, "Getcal failed for pipedata")
	}
	    
	# Make link if needed.  LN doesn't like a trailing /.
	if (defvar("pipedata") && !access ("pipedata$")) {
	    fname = osfn ("pipedata$")
	    fname = substr(fname,1,strlen(fname)-1)
	    if (verbose)
	        printf ("    ln -s %s -> %s\n", osfn(dir), fname)
	    ln (osfn(dir), fname)
	} else if (verbose)
	    printf ("    directory %s already exists\n", osfn ("pipedata$"))
end
