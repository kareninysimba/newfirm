#
# WEIGHTSIF1
#
# Description:
#
#   This is a relatively general procedure for computing weighting factors
#   based on conditions of seeing, transparency, and skynoise.  The procedure
#   expects an input table (infile) with the following columns: filename,
#   seeing, transparency, skynoise.  The seeing weighting factors are given by:
#   
#       wseeing = 1 for [seeing <= midptSEE + usig1SEE * stddevSEE]
#       wseeing = ((midptSEE+usig1SEE*stddevSEE)/seeing)^2 otherwise
#
#   where midptSEE and stddevSEE are the midpoint and standard deviation of
#   the distribution of seeing, computed with the following input parameters:
#
#       usig0SEE:   upper sigma threshold for seeing
#       up0SEE:     upper threshold value for seeing
#       nclp0:      number of iterations performed for seeing distribution
#                   determination
#
#   So, usig1SEE is the number of standard deviations above the midpoint 
#   of seeing, below which the seeing weighting factors are unity and 
#   above which they scale as seeing^(-2).  If usig1SEE=INDEF, all seeing
#   weighting factors to unity.
#
#   The weighting factors based on transparency and skynoise are given
#   by:
#
#       wtransns = (1/skynoise)^2
#
#   and the total effective weighting factors are:
#
#       wtot = wseeing * wtransns
#
#   These weighting factors are written to the headers of the images as
#   WSEEING, WTRNSNS, and WTOT.  An output table (outfile) is created
#   with the following columns: filename, wseeing, wtransns, wtot
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful (not currently set in module)
# 
# Future Work:
#
#   More schemes for weighting?
#
# History
#
#   T. Huard    20090506    Created.
#   T. Huard    20090507    Added {prompt=""} to global parameter declarations.
#                           I don't think these are strictly necessary.  Also,
#                           infile was made a visible, required parameter
#                           instead of a hidden parameter.
#   T. Huard    20090622    wtransns=(1/skynoise)^2 instead of
#                           wtransns=(transparency/skynoise)^2, where skynoise
#                           is that for the SIF prior to scaling based on
#                           transparency.
#   T. Huard    20090813    Printing many lines to a log file (weightsif1-debug.log)
#                           for debugging purposes.
#   Last Revised:   T. Huard    20090904   11:30am 

procedure weightsif1 (infile)

# Declare global parameters.
string  infile                      {prompt="Input file"}
string  outfile = "weightsif1.out"  {prompt="Output file"}
real    usig1SEE = 1.               {prompt="Upper sigma critical seeing point"}
real    usig0SEE = 3.               {prompt="Upper sigma seeing threshold0"}
real    up0SEE = INDEF              {prompt="Upper seeing threshold0"}
real    nclp0 = 3.                  {prompt="Number of iterations0"}
int     status = 1                  {prompt="Exit status"}

begin

    # Declare local parameters	
    string  dim
    real    npixSEE,meanSEE,midptSEE,stddevSEE,minSEE,maxSEE
    real    seeing,trans,skynoise,wtransns,wseeing,wtot
    int     nrows

# Print to a log file for debug purposes
    if (access("weightsif1-debug.log")) delete("weightsif1-debug.log",verify-)
    ;
    printf("%s\n","***BEGINNING weightsif1:***",>>"weightsif1-debug.log")
    printf("%s\n","infile: "//infile,>>"weightsif1-debug.log")
    printf("%s\n","outfile: "//outfile,>>"weightsif1-debug.log")
    printf("%s\n",": usig1SEE"//str(usig1SEE),>>"weightsif1-debug.log")
    printf("%s\n",": usig0SEE"//str(usig0SEE),>>"weightsif1-debug.log")
    printf("%s\n",": up0SEE"//str(up0SEE),>>"weightsif1-debug.log")
    printf("%s\n",": nclp0"//str(nclp0),>>"weightsif1-debug.log")
    printf("%s\n",": status"//str(status),>>"weightsif1-debug.log")

    # Create FITS image of seeing from the table so that its initial
    # distributions may be characterized.
    if (imaccess("weightsif1-see")) imdelete("weightsif1-see",verify-)
    ;
    count(infile) | scan(nrows)
    dim="1,"//str(nrows)

    # Print to a log file for debug purposes
    printf("\n%s\n","***MIDDLE weightsif1.cl:***",>>"weightsif1-debug.log")
    printf("%s\n","nrows: "//str(nrows),>>"weightsif1-debug.log")

    fields(infile,"2",lines="1-") |
        rtextimage("STDIN","weightsif1-see",header-,pixels+,nskip=0,dim=dim)

    # Characterize the initial distribution of seeing.
    imstat("weightsif1-see.fits",fields="npix,mean,midpt,stddev,min,max",
        lower=INDEF,upper=up0SEE,nclip=nclp0,lsigma=INDEF,usigma=usig0SEE,
        format-,cache-) | scan(npixSEE,meanSEE,midptSEE,stddevSEE,minSEE,maxSEE)

    # Print to a log file for debug purposes
    printf("\n%s\n","***MIDDLE weightsif1.cl:***",>>"weightsif1-debug.log")
    printf("%s %s\n","npixSEE, meanSEE, midptSEE: ",
	    str(npixSEE)//", "//str(meanSEE)//", "//str(midptSEE),
        >>"weightsif1-debug.log")
    printf("%s %s\n","stddevSEE, minSEE, maxSEE: ",
	    str(stddevSEE)//", "//str(minSEE)//", "//str(maxSEE),
        >>"weightsif1-debug.log")

    # Compute weighting factors and write them to the appropriate keywords:
    # WSEEING, WTRNSNS, WTOT.  Create output table listing the images and
    # these weighting factors.
    if (access(outfile)) delete(outfile,verify-)
    ;
    list=infile
    while(fscan(list,s1,seeing,trans,skynoise) != EOF) {

        # Print to a log file for debug purposes
        printf("%s %s\n","Weighting procedure for image:",s1,>>"weightsif1-debug.log")
        printf("%s\n","seeing: "//str(seeing),>>"weightsif1-debug.log")
        printf("%s\n","trans: "//str(trans),>>"weightsif1-debug.log")
        printf("%s\n","skynoise: "//str(skynoise),>>"weightsif1-debug.log")

###        wtransns=(trans/skynoise)**2
        wtransns=(1./skynoise)**2
        if (usig1SEE != INDEF) {
            if (seeing <= midptSEE+usig1SEE*stddevSEE) {
                wseeing=1.
            } else {
                wseeing=((midptSEE+usig1SEE*stddevSEE)/(seeing))**2
            }
        } else {
            # usig1SEE=INDEF suggests uniform weighting based on seeing
            wseeing=1.
        }
        wtot=wtransns*wseeing
        # Print to a log file for debug purposes
        printf("%s\n","wtransns: "//str(wtransns),>>"weightsif1-debug.log")
        printf("%s\n","wseeing: "//str(wseeing),>>"weightsif1-debug.log")
        printf("%s\n","wtot: "//str(wtot),>>"weightsif1-debug.log")
        hedit(s1,"WTRNSNS",wtransns,add+,update+,verify-,show-)
        hedit(s1,"WSEEING",wseeing,add+,update+,verify-,show-)
        hedit(s1,"WTOT",wtot,add+,update+,verify-,show-)
        printf("%s %s %s %s\n",s1,wseeing,wtransns,wtot,>>outfile)
    }
    list=""

end
