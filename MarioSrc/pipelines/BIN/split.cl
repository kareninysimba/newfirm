#!/bin/env pipecl
# 
# SPLIT -- Split MEF into pieces and trigger subpipeline.
#
# The pieces are put in the dataset directory and files are setup to
# trigger the subpipeline.  This routine triggers a few pieces for
# each available subpipeline and spools the rest for triggering by
# stage that responds to the return triggers.

string	cpipe				# Calling pipeline
string	tpipe				# Target pipeline
string	textn = ""			# Target pipeline trigger extension
string	rextn = ""			# Return trigger file extension
string	aextn = ""			# Alternate trigger file extensions
int	ntrig = 2			# Number to trigger here

int	nmef, numtrig
string	dataset, ifile, indir, datadir, spool, lfile, pipes, odir
string	tname, cname, extname, s4
struct	*fd1, *fd2

# Set the default from the application parameters or environment.
numtrig = int (envget("CoresPerNode"))
ntrig = numtrig

# Tasks and packages.
task $pipeselect = "$!pipeselect"
images		# IMCOPY
utilities	# TRANSLIT
mscred
task mscextensions = "mscsrc$x_mscred.e"
cache mscred

# Get arguments.
if (fscan (cl.args, cpipe, tpipe, textn, rextn, aextn, ntrig) < 2) {
    printf ("Usage: split.cl cpipe tpipe [textn rextn aextn ntrig]\n")
    logout 0
}
;

# If the trigger file extensions are not specified the default action is a
# simple call and return using the target pipeline name.

if (textn == "")
    textn = "." // tpipe
;
if (rextn == "")
    rextn = "." // tpipe
;
if (aextn == "")
    aextn = "." // tpipe
;

# Directories and file names.
names (cpipe, envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
set (uparm = names.uparm)
ifile = indir // dataset // "." // cpipe
lfile = datadir // names.lfile
spool = indir // "spool/"
set (pipedata = names.pipedata)

# Work in data directory.
cd (datadir)

# Log start of processing.
printf ("\nSPLIT (%s): ", dataset | tee (lfile)
time | tee (lfile)

# Check for pipelines.
pipes = spool // dataset // rextn
touch (pipes)
if (defvar ("NHPPS_MIN_DISK"))
    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, envget("NHPPS_MIN_DISK"), > pipes)
else
    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, > pipes)
count (pipes) | scan (i)
if (i == 0) {
    print ("  ERROR: The " // tpipe // " pipeline is not running\n")
    delete (pipes)
    logout 2
}
;

# Extract image extensions.
s2 = ""
if (access ("pipedata$extpat.dat"))
    head ("pipedata$extpat.dat") | scan (s2)
;
head (ifile) | scan (s1)
i = strlstr (".fits", s1)
if (i > 0)
    s1 = substr (s1, 1, i-1)
;
mscextensions (s1, extname=s2, lindex-, lname+, lver-) |
    translit ("STDIN", "[]", " ", > "split.tmp")

# In order to allow work to begin in the subpipeline while the splitting
# is going on we trigger a few datasets and spool the rest.

b1 = no; j = 0
count (ifile) | scan (nmef)
list = "split.tmp"; fd1 = pipes
while (fscan (list, s1, extname) != EOF) {
    j += 1

    # Set names.
    printf ("%s_%s\n", dataset, extname) | scan (tname)
    cname = tname

    # Split extension(s) and create @file.
    if (nmef == 1) {
	printf ("%s_%s\n", substr(s1,strldx ("/$",s1)+1,1000), extname) |
	    scan (s2)
	if (j == 1) {
	    printf ("%s_00\n", dataset) | scan (s4)
	    imcopy (s1//"[0]", s4)
	}
	;
	imcopy (s1//"["//extname//"]", s2, verbose+)
	pathname (s2, >> indir//"data/"//tname//textn)
    } else {
	k = 0
	fd2 = ifile
	while (fscan (fd2, s1) != EOF) {
	    if (strlstr(".fits",s1) > 0)
	        s1 = substr (s1, 1, strlstr(".fits",s1)-1)
	    ;
	    printf ("%s_%s\n", substr(s1,strldx ("/$",s1)+1,1000), extname) |
		scan (s2)
	    if (j == 1) {
		k += 1 
		printf ("%s_%02d00\n", dataset, k) | scan (s4)
		imcopy (s1//"[0]", s4)
	    }
	    ;
	    imcopy (s1//"["//extname//"]", s2, verbose+)
	    pathname (s2, >> indir//"data/"//tname//textn)
	}
	fd2 = ""
    }

    # Setup files for triggering and return.
    print (tname, >> spool//cname//rextn)
    if (aextn != textn) {
	print (cname//rextn, >> dataset//aextn)
	print (cname//textn, >> dataset//textn)
    } else
	print (cname//rextn, >> dataset//textn)

    # Trigger subpipeline.  This is a little subtle because it is possible
    # for datasets to complete faster than new ones are submitted here and we
    # have to insure that there are ntrig instances triggered for each
    # pipeline in order for the return stage to keep retriggering the
    # desired number of instances.

    # Do we have to check anymore?
    if (b1)
        next
    ;

    # Go through the list of pipelines checking for the number of instances
    # already assigned to each pipeline.  The loop is structured such that
    # we start with the next pipeline after the previous one assigned.

    for (i = 0; i <= ntrig; ) {
	if (fscan (fd1, odir) == EOF) {
	    i += 1
	    fd1 = pipes
	    if (fscan (fd1, odir) == EOF)
	        ;
	    ;
	}

	# Count the number already assigned to a node.
	k=0; match (odir, spool//dataset//"*"//rextn//".node") |
	    count ("STDIN") | scan (k)
	if (k >= ntrig)
	    next
	;

	# Assign a new node.
	copy (indir//"data/"//tname//textn, odir//tname//textn)
	if (aextn != textn) {
	    pathname (indir//cname//rextn, >> odir//"return/"//tname//aextn)
	    pathname (indir//cname//textn, >> odir//"return/"//tname//textn)
	} else
	    pathname (indir//cname//rextn, >> odir//"return/"//tname//textn)
	print (odir, > spool//cname//rextn//".node")
	touch (odir//tname//textn//"trig")
	break
    }

    # Do we need to keep checking?
    if (k >= ntrig)
        b1 = yes
    ;
}
list =""; fd1 = ""
delete ("split.tmp")

logout 1
