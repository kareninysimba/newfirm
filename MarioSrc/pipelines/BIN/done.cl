#!/bin/env pipecl
#
# DONE -- Finish up pipeline.
#
# The end result of the pipeline is a list file of all the products.
# It is up to later software to decide when to fetch and clean up the results.
# If the pipeline was triggered with a return file the list is returned in
# the file specified in the return file otherwise a file is produced in
# the output directory.
#
# The last step is to remove the processing file.

int	founddir
string	extn			# Return file extension
string	stage			# Stage name for logging
bool    move_data		# Move data to repository?

string	dataset, indir, datadir, rtnfile, odir, ofile, lfile, rfile
string  target, odirs

# Tasks and packages.
task $pipeselect = "$!pipeselect $1 $2 1 0"

# Get arguments.
if (fscan (cl.args, extn, stage, move_data) != 3) {
    print "done.cl: incorrect command line arguments"
    logout 0
} else
    ;

names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
indir   = names.indir
datadir = names.datadir
rtnfile = names.indir   // "return/" // dataset // extn
odir    = names.rootdir // "output/"
lfile   = datadir // names.lfile
ofile   = dataset // ".list"
set (uparm = names.uparm)

# Log start of processing.
printf ("\n%s (%s): ", stage, dataset) | tee (lfile, out="text", append+)
time | tee (lfile, out="text", append+)

# Work in data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Make output list.
if (access (ofile))
    move (ofile, odir, verbose-)
else
    ;

# Set exclude files.
if (access("exclude.list")) {
    list = "exclude.list"
    while (fscan(list,s1)!=EOF) {
	if (access(s1)) {
	    pathnames (s1) | scan (s2)
	    print (s2//"-IGNORE", >> odir//ofile)
	    rename (s1, "."//s1)
	}
	;
    }
    rename ("exclude.list", ".exclude.list")
}
;

# Remove temporary files.
delete ("*.tmp")

# Create list of files to keep.
pathnames ("*.*,*_cat,*_usno", sort-, >> odir//ofile)

# Restore hidden files.
if (access(".exclude.list")) {
    list = ".exclude.list"
    while (fscan(list,s1)!=EOF) {
	if (access("."//s1))
	    rename ("."//s1, s1)
	;
    }
    rename (".exclude.list", "exclude.list")
}
;

cd (odir)
concat (ofile) | tee (lfile, out="text", append+)

# Trigger the data move (DTS) pipeline to move final data if called with 
# move_data equals yes.  Preserve the return file so the DTS pipeline can
# use it to restore the normal program flow.  Otherwise, return output file 
# and trigger the next stage.

#if (move_data && access (rtnfile) == NO) {
if (move_data) {
    printf("Doing DTS...\n")
    target = ""
    pipeselect (envget ("NHPPS_SYS_NAME"), "dts") | scan (target)
    if (target != "") {
	printf ("copy %s -> %s\n", ofile, target//dataset//"-dts.dts")
	copy (ofile, target//dataset//"-dts.dts")
	#delete (ofile)
        if (access (rtnfile)) {
            # send to DTS machine
	    printf ("copy %s -> %s\n",
	        rtnfile, target//"return/"//dataset//"-dts"//".dts")
            copy (rtnfile, target//"return/"//dataset//"-dts"//".dts")
	    delete (rtnfile)
        } else {
	    # Return the list to the output directory.
	    printf ("No return file (%s)\n", rtnfile)
	    pathname (odir//ofile, > target//"return/"//dataset//"-dts"//extn)
	}
            
	printf ("touch %s\n", target // dataset // "-dts" // ".dtstrig")
        touch (target // dataset // "-dts" // ".dtstrig")
    } else {
        print ("The dts pipeline is not running.")
        if (access (rtnfile)) {
	    head (rtnfile, nlines=1) | scan (rfile)
	    copy (ofile, rfile)
	    #delete (ofile)
	    touch (rfile//"trig")
	    delete (rtnfile, verify-)
	} else
	    printf ("No return file (%s)\n", rtnfile)
    }
} else {
    if (access (rtnfile)) {
        head (rtnfile, nlines=1) | scan (rfile)
        copy (ofile, rfile)
	#delete (ofile)
        touch (rfile//"trig")
        delete (rtnfile, verify-)
    } else
        printf ("No return file (%s)\n", rtnfile)
}

# Clean up processing files.
delete (indir//dataset//".*", verify-)

printf("Finished DONE stage!\n")

logout 1
