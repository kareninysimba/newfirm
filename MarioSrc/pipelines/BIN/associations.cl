#!/bin/env pipecl

string comfile, infile, infile0, obstype
string pipe, plfname, proctype, prodtype, type

images

# Get arguments.
if (fscan (cl.args, infile) < 1) {
    printf ("Usage: associations.cl <infile>\n")
    logout 0
}
;

pipe = envget( "NHPPS_SYS_NAME" )
infile0 = infile // "[0]"
hselect( infile0, "$PROCTYPE", yes ) | scan( proctype )
hselect( infile0, "$PRODTYPE", yes ) | scan( prodtype )
hselect( infile0, "$OBSTYPE", yes ) | scan( obstype )
hselect( infile0, "$PLFNAME", yes ) | scan( plfname )

if ( (prodtype=="INDEF") || (obstype=="INDEF") || ( plfname=="INDEF" ) )
    logout 0
;

if ( obstype == "sky" )
    obstype = "object"
;

if ( pipe == "NEWFIRM" ) {
    s1 = substr( plfname, stridx("_",plfname)+1, 999 ) // "_"
    type = substr( s1, 1, stridx("_",s1)-1 )
    if ( (obstype=="dark") || (obstype=="flat") )
        type = "default"
    ;
    if ( prodtype == "png" )
        infile0 = infile // "[0]" 
    else
        infile0 = infile // "[1]"
    if ( (prodtype=="image") && ( (proctype=="Stacked") || (proctype=="Resampled") ) )
	infile0 = infile // "[0]"
    ;
}

comfile = envget( "NHPPS_PIPESRC" )//"/"//pipe//"/dmMaster/associations/"
comfile += obstype//"_"//prodtype//"_"//type//".nhedit"

nhedit( infile0, comfile=comfile )

logout 1
