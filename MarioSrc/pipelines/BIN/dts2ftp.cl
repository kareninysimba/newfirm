#!/bin/env pipecl
#
# DTS2FTP -- Move data products to FTP directories.

if (dts_ftp == "no")
    logout 1
;

int	ver0, ver1, ver2
string	dataset, datadir, ilist, odir, lfile
string	plpropid, dtpropid, propid, ftpdir, nvodir, subdir
struct	*fd

# Load tasks and packages.
images
task $ftpdb = "$! echo ""$1"" | mysql -N -pMosaic_DHS $USER"
task $lmkdir = "$!mkdir -p $1"
task $rmkdir = "$!ssh $1 mkdir -p $2"
task $nvoln = "$!ssh $1 '(mkdir -p $2; cd $2; ln -s ../../../../pipeline/protected/$3 .)'"
task $scp = "$!scp -q $1 $2"

# Set names and directories.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ilist = datadir // dataset // ".list"
odir = names.rootdir // "output/" // dataset // "/"
lfile = datadir // names.lfile
set (uparm = names.uparm)

# Work in output directory.
if (access(odir))
    cd (odir)
else
    logout 1

# Log start of module.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Set the indices to remove the version component.
s1 = names.shortname
ver2 = stridx ("-", s1)
if (ver2 > 0)
    s1 = substr (s1, 1, ver2-1)
;
ver1 = strldx ("_", s1) - 1
ver0 = stridx ("_", s1) + 1

# Separate by PROPID.
touch ("dts2ftp.tmp")
propid = ""; ftpdir = ""
hselect ("*.fits[0]", "$I,$PLPROPID,$DTPROPID", yes, > "dts2ftp1.tmp")
list = "dts2ftp1.tmp"
while (fscan (list,s1,plpropid,dtpropid) != EOF) {
    s1 = substr (s1, 1, stridx("[",s1)-1)
    if (dtpropid == "INDEF")
        hselect (s1//"[1]", "$PLPROPID,$DTPROPID", yes) |
	    scan (plpropid, dtpropid)
    ;
    if (dtpropid == "INDEF") {
        sendmsg ("ERROR", "PROPID missing", s1, VRFY)
	dtpropid = "unknown"
    }
    ;
    if (plpropid == "INDEF")
	plpropid = dtpropid
    ;

    # Check for FTP directory.
    if (dtpropid != propid) {
	propid = dtpropid

	# Use dts_ftp to override the propid.  This is used for test datasets.
	if (dts_ftp != "yes")
	    dtpropid = dts_ftp
	;

	printf ("select ftp_dir, nvo_dir from FTP where proposal='%s';\n",
	    dtpropid) | scan (line)
	ftpdir = ""; nvodir = ""
	ftpdb (line) | scan (ftpdir, nvodir)
	if (ftpdir == "")
	    sendmsg ("ERROR", "No ftp information", line, "PROC") 
	;
    }
    ;

    if (ftpdir == "")
        next
    ;

    # Set subdirectory.
    subdir = names.shortname
    if (ver1 > 0) {
	if (plpropid == "CAL")
	    subdir = substr (subdir, ver0, ver1)
	else
	    subdir = substr (subdir, ver0, ver1) // substr (subdir, ver2, 999)
    }
    ;
    subdir = plpropid // "_" // subdir

    # Set files to be sent by excluding those not desired.
    if (strstr("_png",s1) > 0)
	next
    else if (strstr("_stk1",s1) > 0)
	next
    else if (strstr("-ccd",s1) > 0)
	next
    else if (strstr("_crp",s1) > 0)
	next
    ;

    if (access("dts"//subdir//".tmp") == NO)
        printf ("%s %s %s/ %s/\n",
	    subdir, plpropid, ftpdir, nvodir, >> "dts2ftp.tmp")
    ;
    print (s1, >> "dts"//subdir//".tmp")
}
list = ""; delete ("dts2ftp1.tmp")

# Copy the files.
list = "dts2ftp.tmp"
while (fscan (list,subdir,plpropid,ftpdir,nvodir) != EOF) {
    i = stridx (":", ftpdir)
    j = stridx (":", nvodir)
    if (i > 0) {
	ftpdir += "Reduced/Data/"
	rmkdir (substr(ftpdir,1,i-1), substr(ftpdir,i+1,999))
	if (j > 0)
	    nvoln (substr(nvodir,1,j-1), substr(nvodir,j+1,999),
		substr(ftpdir,i+1,999) | tee (lfile)
	;
	ftpdir += subdir // "/"
	rmkdir (substr(ftpdir,1,i-1), substr(ftpdir,i+1,999))
	fd = "dts"//subdir//".tmp"
	while (fscan (fd, s2) != EOF) {
	    s3 = s2
	    if (ver1 > 0)
		s3 = substr (s3, ver0, ver1) // substr (s3, ver2, 999)
	    ;
	    s3 = plpropid // "_" // s3
	    printf ("  %s -> %s\n", s2, ftpdir//s3) | tee (lfile)
	    scp (s2, ftpdir//s3)
	    delete (s2)
	}
	fd = ""
    } else {
        ftpdir += subdir // "/"
	lmkdir (ftpdir)
	fd = "dts"//subdir//".tmp"
	while (fscan (fd, s2) != EOF) {
	    s3 = s2
	    if (ver1 > 0)
		s3 = substr (s3, ver0, ver1) // substr (s3, ver2, 999)
	    ;
	    s3 = plpropid // "_" // s3
	    printf ("  %s -> %s\n", s2, ftpdir//s3) | tee (lfile)
	    rename (s2, ftpdir//s3)
	}
	fd = ""
    }
}
list = ""

# Clean up.
delete ("dts*.tmp")

logout 1
