# PSQREDO -- Filter the ENIDs based on previous processing.
# This is based on checking the MOSAIC_DTS/output directory.
# Therefore, this requires the DTS directories to not be deleted and to be
# on the same machine that runs this routine.  It also depends on knowledge
# of the names in the DTS pipeline.
#
#       "all": redo everything (i.e. don't look at DTS at all)
#      "none": don't redo anything (i.e. skip any enids in DTS)
#       "cal": don't redo any cal (i.e. skip enids in "Zero" or "?*F-)
#       "obj": don't redo anything other than cals
#    patterns: do only enids in DTS directories with specified pattern
#
# Note that specifying a pattern ignores the input enids entirely.  This
# is primarily used for redoing specific datasets and is equivalent to
# an enids list in the config directory.

procedure psqredo (queue, qname, enids, redo)

string	queue			{prompt="Queue"}
string	qname			{prompt="Dataset name in queue"}
file	enids			{prompt="List of enids"}
string	redo			{prompt="Redo selection"}

struct	*fd1, *fd2

begin
	int	ndone
	string	rootpat, pattern, dataset

	task comm = "$!comm -23 $1 $2"

	if (redo == "" || redo == "all" || redo == "yes")
	    return

	names ("dts", "")
	rootpat = names.rootdir//"output/"//queue//"_"//qname//"_???????-"
	files (rootpat//"*-dts", > enids//"tmp1")
	count (enids//"tmp1") | scan (ndone)

	if (redo == "none" || redo == "no" || redo == "cal" || redo == "obj") {
	    if (ndone == 0) {
		delete (enids//"tmp*")
		return
	    }
	    if (redo == "cal") {
		match (rootpat//"ftrZero",enids//"tmp1",stop+,>enids//"tmp2")
		match (rootpat//"ftr?*Flat",enids//"tmp2",stop+,>enids//"tmp1")
		match (rootpat//"ftr?*F-",enids//"tmp2",stop+,>enids//"tmp1")
		match (rootpat//"drk",enids//"tmp1",stop+,>enids//"tmp2")
		match (rootpat//"fil",enids//"tmp2",stop+,>enids//"tmp1")
		delete (enids//"tmp2")
	    } else if (redo == "obj") {
		match (rootpat//"ftrZero",enids//"tmp1",stop-,>enids//"tmp2")
		match (rootpat//"ftr?*Flat",enids//"tmp1",stop-,>>enids//"tmp2")
		match (rootpat//"ftr?*F-",enids//"tmp1",stop-,>>enids//"tmp2")
		match (rootpat//"drk",enids//"tmp1",stop-,>>enids//"tmp2")
		match (rootpat//"fil",enids//"tmp1",stop-,>>enids//"tmp2")
		rename (enids//"tmp2", enids//"tmp1")
	    }
	    fd1 = enids//"tmp1"; touch (enids//"tmp2")
	    while (fscan (fd1, dataset) != EOF)
		concat (dataset//"/*.enids", enids//"tmp2", append+)
	    fd2 = ""; delete (enids//"tmp1")
	    sort (enids//"tmp2") | uniq (> enids//"tmp1")
	    sort (enids) | uniq (> enids//"tmp2")
	    comm (enids//"tmp2", enids//"tmp1", > enids)
	} else {
	    delete (enids)
	    if (ndone == 0) {
		delete (enids//"tmp*")
		return
	    }
	    print (redo) | words (> enids//"tmp3")
	    fd1 = enids//"tmp3"
	    while (fscan (fd1, pattern) != EOF) {
		printf ("match %s\n", rootpat//"[fg][to]?"//pattern//"-dts/?*.enids")
	        match (rootpat//"[fg][to]?"//pattern//"-", enids//"tmp1", stop-,
		    > enids//"tmp2")
		fd2 = enids//"tmp2"
		while (fscan (fd2, dataset) != EOF)
		    concat (dataset//"/*.enids", enids, append+)
		fd2 = ""; delete (enids//"tmp2")
	    }
	    fd1 = ""; delete (enids//"tmp3")
	}

	delete (enids//"tmp*")
end
