#!/bin/env pipecl
#
# DUMMY -- It just prints a string to STDOUT and exits with
# specified exit code.

int	stat = 1		# Exit code
int	sleep = 0		# Sleep
struct	output = "DUMMY"	# Output string

i = fscan (cl.args, stat, sleep, output)

printf ("%s\n", output)
sleep (sleep)
logout (stat)
