#!/bin/env pipecl
#
# XTKCOR -- Split MEF file into pieces and send to a target pipeline.
#
# A file containing the names of the MEF files is specified by the dataset.
# The MEF extensions are crosstalk corrected and split into the data
# directory.  The list of images is then used to call a target pipeline.

string	cpipe			# Calling pipeline
string	tpipe			# Target pipeline

int	status = 1
int	nmef
string	dataset, idataset, indir, datadir, ifile, lfile, rpipe, temp1, temp2
string	imname, xtfile, olist
#file	caldir = "MarioCal$/"
file	caldir = "MC$"

# Get arguments.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
cpipe = names.pipe
if (cpipe == "xtm") {
    tpipe = "sif"
    rpipe = "mef"
} else {
    tpipe = "scl"
    rpipe = "cal"
}

# Tasks and packages.
images
servers
mscred
task xtalkcor = "mscsrc$x_mscred.e"
cache mscred, xtalkcor
task $cp = "$!cp -r $1 $2"
task $ln = "$!ln -s $1 $2"
task $pipeselect = "$!pipeselect $1 $2 0 0"
task $callpipe = "$!call.cl $1 $2 $3 '$4' $5 2>&1 > $6 ; echo $? > $7"

# Set file and directory names.
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
idataset = names.parent
ifile = indir // dataset // "." // cpipe
temp1 = "xtkcor1.tmp"
temp2 = "xtkcor2.tmp"
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (pipedata = names.pipedata)
if (access (datadir) == NO) {
    mkdir (datadir)
    sleep (1)
}
;
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

delete ("xtkcor*tmp")
delete ("*.fits,*.pl")

concat (ifile)

# Do quick check for target pipeline.
pipeselect (envget ("NHPPS_SYS_NAME"), tpipe)
pipeselect (envget ("NHPPS_SYS_NAME"), tpipe) | count | scan (i)
if (i == 0) {
    sendmsg ("WARNING", "Pipeline is not running", tpipe, "PIPE")
    logout 3
}
;

# Use first image for headers.
imname = ""; head (ifile, nlines=1) | scan (imname)
if (imname == "") {
    sendmsg ("ERROR", "No data found", "", "VRFY")
    logout 0
}
;
imname += "[0]"

# Set uparm.
s1 = ""; hselect (imname, "uparm", yes) | scan (s1)
i = 0
if (s1 == "" || access (s1) == NO) {
    getcal (imname, "uparm", cm, caldir, obstype="", detector=cl.instrument,
        imageid="", filter="", exptime="", mjd="!mjd-obs") | scan (i, line)
    if (i == 0) {
        s1 = ""; hselect (imname, "uparm", yes) | scan (s1)
    }
    ;
}
;
if (i != 0) {
    sendmsg ("ERROR", "Getcal failed for uparm", str(i)//" "//line, "CAL")
    logout 0
}
;

cp (osfn(s1), "uparm")
if (access ("uparm") == NO) {
    sendmsg ("ERROR", "Failed to copy uparm", "", "CAL")
    logout 0
}
;
set (uparm = names.uparm)
flpr

# Set pipedata.
s1 = ""; hselect (imname, "pipedata", yes) | scan (s1)
i = 0
if (s1 == "" || access (s1) == NO) {
    getcal (imname, "pipedata", cm, caldir, obstype="", detector=cl.instrument,
        imageid="", filter="", exptime="", mjd="!mjd-obs") | scan (i, line)
    if (i == 0) {
        s1 = ""; hselect (imname, "pipedata", yes) | scan (s1)
    }
    ;
}
;
if (i != 0) {
    sendmsg ("ERROR", "Getcal failed for pipedata", str(i)//" "//line, "CAL")
    logout 0
}
;
ln (osfn(s1), "pipedata")

# Set crosstalk file.
# For undiagnosed reasons this fails occasionally, usually right at
# the beginning of a new data block.  So lets add a retry and another
# check for failure.
for (j=0; j<10; j+=1) {
    getcal (imname, "xtalkfile", cm, caldir, obstype="", detector=cl.instrument,
	imageid="", filter="", exptime="", match="!nextend", mjd="!mjd-obs") |
	scan (i, line)
    if (i != 0) {
	sleep (j)
        next
    }
    ;
    xtfile = ""; hselect (imname, "xtalkfil", yes) | scan (xtfile)
    if (xtfile=="" || substr(xtfile,1,5)=="mscdb") {
	sleep (j)
        next
    }
    ;

    break
}
if (i != 0) {
    sendmsg ("ERROR", "Getcal failed for xtalkfile", str(i)//" "//line, "CAL")
    logout 0
}
;
if (xtfile=="" || substr(xtfile,1,5)=="mscdb") {
    sendmsg ("ERROR", "Crosstalk file not set", xtfile, "CAL")
    logout 0
}
;
if (access(xtfile) == NO)
    sleep (1)
;
if (access(xtfile) == NO) {
    sendmsg ("ERROR", "Crosstalk file not found", xtfile, "CAL")
    logout 0
}
;

# Set the output split crosstalk corrected names.  Note that we can't
# split to multiple machines with XTALKCOR so instead we split to the
# datadir directory and then trigger the SIF/SCL pipeline on other
# nodes to access the files on this node.

names (envget("NHPPS_SUBPIPE_NAME"), idataset)

count (ifile) | scan (nmef)
list = ifile
for (i=1; fscan(list,imname)!=EOF; i+=1) {
    if (nmef > 1 || tpipe == "scl") {
	printf ("%s_%02d\n", names.shortname, i, >> "xtkcorout.tmp")
	if (tpipe == "sif")
	    printf ("%s_xtm_%02d\n", idataset, i, >> "xtkcorxtm.tmp")
	;
    } else {
	printf ("%s\n", names.shortname, >> "xtkcorout.tmp")
	if (tpipe == "sif")
	    printf ("%s_xtm\n", idataset, >> "xtkcorxtm.tmp")
	;
    }
}
list = ""

# Do the crosstalk correction and split the files.
if (tpipe == "scl")
    xtalkcor ("@"//ifile, output="@xtkcorout.tmp", bpmasks="",
        xtalkfiles=xtfile, bpmthreshold=-10, split=yes, fextn="fits",
        noproc=no, >> lfile)
else 
    xtalkcor ("@"//ifile, output="@xtkcorout.tmp", bpmasks="@xtkcorxtm.tmp",
        xtalkfiles=xtfile, bpmthreshold=-10, split=yes, fextn="fits",
        noproc=no, >> lfile)
delete ("xtkcorout.tmp")

# Now make lists for calling the target pipeline.  We want to merge
# multiple amplifiers into CCD images which is done by including the
# amplifier images for the same CCD in the same list.  Note we are assuming
# that the input MEFs contain all the amplifiers for a set of CCDs.
# We use a file in pipedata to define a mapping between extension names and
# CCD names.

files ("*.fits", > "xtkcorims.tmp")
list = "xtkcorims.tmp"
while (fscan (list, imname) != EOF) {
    j = strstr (".fits", imname)
    i = strldx ("_", imname)
    s2 = substr (imname, i+1, j-1)
    match (s2//"$", "pipedata$extnames.dat") | scan (s3)
    s2 = idataset // "_" // s3 // "." // tpipe
    pathnames (imname, >> s2)
printf ("  %s\n    -> %s\n", imname, s2)
}
list = ""; delete ("xtkcorims.tmp")

pathnames ("*."//tpipe, > "xtkcorlist.tmp")

# Call pipeline.  Don't spool or look for returns.
callpipe (cpipe//"-"//rpipe, tpipe, "split", datadir//"xtkcorlist.tmp", 1000,
    temp1, temp2)
concat (temp2) | scan (status)
concat (temp1)

# Clean up files.
delete ("xtkcor*.tmp")
if (status == 1) {
imdelete ("@"//ifile)
delete (ifile)
}
;

logout (status)
