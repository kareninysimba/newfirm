#!/bin/env pipecl
# 
# DELIFINARCH -- Delete files confirmed to be in archive

string  ilist, olist
bool	deldata = YES

int	status = 1
int	missing = 0
string	node, path
struct dtnsanam, instrument, mjd, proctype, prodtype, obstype

# Load tasks and packages.
task $ckarchive = "$!ckarchive"
images
servers
utilities

# Get arguments.
if (fscan (args, ilist, deldata) < 1) {
    printf ("usage: delifinarch listfile [deldata]\n")
    logout 0
}
;

# Create output file name for data not found in archive
i = strldx( ".", ilist )
s1 = substr( ilist, 1, i )
olist = s1 // "NOT"

# Check for input list
if (access(ilist)==NO) {
    printf ("ERROR: Can't access input list file (%s)\n", ilist)
}
;

# Make sure output list does not exist
#if ( access( olist ) )
#    delete( olist )
#;

# Loop through the list.
list = ilist
while (fscan (list, s1) != EOF) {
    
    # Check for file.
    if (imaccess(s1//"[0]")==NO) {
	#status = 0
	if ( verbose > 0 ) 
	    printf ("Data product not found (%s)\n", s1)
	;
	next
    }
    ;

    # Check for PL keywords.
    hselect (s1//"[0]", "$PLFNAME", yes) | scan (s2)
    if (s2 == "INDEF") {
	printf ("File is not a pipeline data product (%s)\n", s1)
        next
    }
    ;

    # Check for previous version. Because it is so hard to pass
    # through values with spaces we replace them with underscores and
    # let the database match any character.
    hselect (s1//"[0]", "$DTNSANAM", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (dtnsanam)
    hselect (s1//"[0]", "$instrume", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (instrument)
    hselect (s1//"[0]", "$MJD-OBS", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (mjd)
    hselect (s1//"[0]", "$OBSTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (obstype)
    hselect (s1//"[0]", "$PROCTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (proctype)
    hselect (s1//"[0]", "$PRODTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (prodtype)
    if (mjd == "INDEF")
	hselect (s1//"[1]", "$MJD-OBS", yes) |
	    translit ("STDIN", " ", "_", del-, col-) | scan (mjd)
    ;
    printf ("'%s %s %s %s %s %s'\n",
	dtnsanam, instrument, mjd, obstype, proctype, prodtype) | scan (line)
    s2 = ""; ckarchive (line) | scan (s2)
    if (s2 != "") {

	if ( s2 == "ERROR" ) {
	    printf( "Error in (execution of) SQL query for %s\n", s1 )
	    printf( "DTNSANAM: %s\n", dtnsanam )
	    printf( "INSTRUME: %s\n", instrument )
	    printf( "MJD-OBS:  %s\n", mjd )
	    printf( "OBSTYPE:  %s\n", obstype )
	    printf( "PROCTYPE: %s\n", proctype )
	    printf( "PRODTYPE: %s\n", prodtype )
	    status = 0
	} else {
	    printf( "Earlier version in archive (%s)... ", s2 )
	    if (deldata) {
	        imdelete (s1)
		printf( "deleted!\n" )
	    } else
		printf( "but NOT deleted!\n" )
	        
	}

    } else {
	
	printf( "WARNING: dataset not found in archive\n" )
	print( s1 )
	printf( "%s %s %s %s %s %s\n",
	    dtnsanam, instrument, mjd, obstype, proctype, prodtype)
	print "------------------------------------------"
	missing = missing+1
	printf( "%s\n", s1, >> olist )

    }

    if ( status == 0 ) {
	logout( status )
	break
    }
    ;

}
list = ""

if ( missing > 0 ) {
    printf( "WARNING: %d files have not been found in the archive!\n", missing )
    printf( "See %s for list of those files.\n", olist )
} else if (deldata == YES)
    printf( "All files have been deleted!\n")
else
    printf( "Files can be deleted.\n")

logout (status)
