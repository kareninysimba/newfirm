# NAMES -- Generic name mapping routine.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure names (pipename, dataname)

string	pipename		{prompt = "Pipeline"}
string	dataname		{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file	src			{prompt = "Source directory"}
file	parent			{prompt = "Parent part of dataset"}
file	child			{prompt = "Child part of dataset"}
file	lfile			{prompt = "Log file"}
string	pattern = ""		{prompt = "Pattern"}
bool	base = no		{prompt = "Child part of dataset"}

begin
	int	i
	string	name, s1
	
	# Set query parameters.
	pipe = pipename
	name = dataname

	# Set the dataset.
	dataset = name
	if (pattern != "")
	    dataset += "-" // pipe // pattern

	# Set the parent and child.
	i = strstr ("-"//pipe, dataset)
	if (i > 0) {
	    parent = substr (dataset, 1, i-1)
	    child = substr (dataset, i+4, 1000)
	    if (base) {
		name = substr (name, i+4, 1000)
	        dataset = child
	    }
	} else {
	    parent = ""
	    child = dataset
	}

	# The following is a short version of the dataset.
	shortname = ""; s1 = name
	for (i=stridx("-",s1); i!=0; i=stridx("-",s1)) {
	    if (i > 1)
	        shortname += substr (s1, 1, i)
	    s1 = substr (s1, i+4, 1000)
	}
	if (s1 == "")
	    shortname = substr (shortname, 1, strlen(shortname)-1)
	else
	    shortname += s1
	if (pattern != "")
	    shortname += "-" // pattern

	# Set the directories.
	rootdir	= strupr (nhpps.application) // "_" // strupr (pipe) // "$/"
	indir	= rootdir // "input/"
	datadir	= rootdir // "data/" // dataset // "/"
	uparm	= datadir // "uparm/"
	pipedata = datadir//"pipedata/"
	src = "NHPPS_PIPEAPPSRC$/" // strupr (pipe) // "/"
	#lfile = shortname // ".log"
	lfile = dataset // ".log"

	if (pattern != "") {
	    ;
	}
end
