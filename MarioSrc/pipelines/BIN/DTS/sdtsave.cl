#!/bin/env pipecl2
#
# SDTSAVE -- save data to parent pipeline output directory
#
## ==========
## sdtsave.cl
## ==========
## ---------------------------------------------
## save data to parent pipeline output directory
## ---------------------------------------------
## 
## :Manual group:   BIN/DTS
## :Manual section: 1
## 
## Usage
## =====
## 
## ``sdtsave.cl``
## 
## Input
## =====
## 
## - standard input list containing files to be saved
## - standard return file providing the node and path to the parent pipeline
## - optional file ``[ParentDataset].odir`` or ``default.odir`` in the standard
##   input directory of the parent pipeline
## 
## Output
## ======
## 
## - saved files in the destination directory
## - a copy of the input list in ``sdtlist`` in data directory
## - a copy of the return file in ``sdtreturn`` in data directory
## 
## Exit Status
## ===========
## 
## | 0 - unspecified error
## | 1 - files successfully saved
## 
## Description
## ===========
## 
## The files in standard input list matching one of the patterns in the
## parameter ``dts_save`` are moved to a destination directory.  The
## destination directory is specified by the contents of the file
## ``[ParentDataset].odir`` or ``default.odir`` in the standard input directory
## of the parent pipeline.  If these files are not present (the usual case)
## then the directory ``[ParentDataset]`` in the standard output directory of
## the parent pipeline is used.
## 
## Files already in the parent pipeline directory are not moved.
##
## Temporary copies of the input list and return file are copied to the
## data directory since those original files are removed by the **done**
## stage which runs before the **clean** stage in the SDT pipeline.


string	ilist, rootdir, odir, udir

# Clean any previous processing.
delete ("dtssave*.tmp")

# Set input list.
ilist = names.indir // names.dataset // "." // names.pipe

# Set output root and save directory.
s1 = names.indir // "return/" // names.dataset // "." // names.pipe
if (access(s1)==NO) {
    sendmsg ("ERROR", "No return file", s1, "VRFY")
    plexit (code=0, name="NOFILE", class="ERROR",
        description="No return file", arg=s1)
}
;

# Save files to be used by sdtclean.cl
copy (ilist, "sdtlist"); ilist = "sdtlist"
copy (s1, "sdtreturn"); s1 = "sdtreturn"

concat (s1) | scan (s2)
rootdir = substr(s2, 1, strstr("/input",s2))
odir = rootdir // "output/" // names.parent

# The output directory can be overridden by a file in the input directory.
# It is an error if the default directory already exists.
if (access(rootdir//"input/"//names.parent//".odir")) {
    concat (rootdir//"input/"//names.parent//".odir") | scan (odir)
    udir = odir
} else if (access(rootdir//"input/"//"default.odir")) {
    concat (rootdir//"input/"//"default.odir") | scan (odir)
    udir = odir
} else
    udir = ""

odir += "/"
path (odir) | scan (odir)

print (odir)

# Check output directory.
if (access (odir) == NO)
    sendmsg ("ERROR", "Can't access output directory", odir, "VRFY")
;

# Don't touch files already in the root directory.
if (udir == "")
    match (rootdir, ilist, stop+, > "sdtsave1.tmp")
else
    copy (ilist, "sdtsave1.tmp")

# Don't consider flagged files.
match ("-IGNORE", "sdtsave1.tmp", stop+, > "sdtsave2.tmp")
rename ("sdtsave2.tmp", "sdtsave1.tmp")

# Expand pattern list and separate matches.
print (dts_save) | words (> "sdtsave_pat.tmp")
list = "sdtsave_pat.tmp"; touch ("sdtsave.tmp")
while (fscan (list, line) != EOF) {
    match (line, "sdtsave1.tmp", >> "sdtsave.tmp")
    match (line, "sdtsave1.tmp", stop+, > "sdtsave2.tmp")
    rename ("sdtsave2.tmp", "sdtsave1.tmp")
}
list = ""

# Save requested files and finish up.
sort ("sdtsave.tmp") | uniq (> "sdtsave.tmp")
move ("@"//"sdtsave.tmp", odir, verbose+)
delete ("sdtsave*.tmp")
plexit (code=1)
