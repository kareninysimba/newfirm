#!/bin/env pipecl2
#
# SDTCLEAN -- Clean unsaved data produced by the pipelines.
#
## ===========
## sdtclean.cl
## ===========
## --------------------------------------------
## clean (delete) data not in the save patterns
## --------------------------------------------
## 
## :Manual group:   BIN/DTS
## :Manual section: 1
## 
## Usage
## =====
## 
## ``sdtclean.cl``
## 
## Input
## =====
## 
## - ``sdtlist`` in the data directory
## - ``sdtreturn`` in the data directory
## 
## Output
## ======
## 
## - files are delete
## 
## Exit Status
## ===========
## 
## | 0 - unspecified error
## | 1 - files successfully deleted
## 
## Description
## ===========
## 
## The files in ``sdtlist`` not matching one of the patterns in the parameter
## ``dts_save`` are deleted.  Files in the parent pipeline directory, as
## identified by the contents of ``sdtreturn`` are not deleted.

string	ilist, rootdir

# Clean any previous processing.
delete ("dtsdelete*.tmp")

# Set input list.
ilist = "sdtlist"

# Set root directory to be excluded from deletion.
s1 = "sdtreturn"
if (access(s1)==NO) {
    sendmsg ("ERROR", "No return file", s1, "VRFY")
    plexit (code=0, name="NODATA", class="WARNING",
        description="No return file", arg=s1)
}
;
concat (s1) | scan (s2)
rootdir = substr(s2, 1, strstr("/input",s2))

# Add "orig.pl" to the dts_save list. This is done here to make sure
# it is not accidentally removed from the dts_save entry in the 
# configuration file. Including orig.pl is critical for the proper
# operation of the persistent persistence masks.
if ( envget( "NHPPS_SYS_NAME" ) == "NEWFIRM" ) {
    dts_save = dts_save // " orig.pl"
}
;

# Exclude files in the root directory, flagged, and in the save pattern.
match (rootdir, ilist, stop+, > "sdtdelete.tmp")
match ("-IGNORE", "sdtdelete.tmp", stop+, > "sdtdelete_temp.tmp")
rename ("sdtdelete_temp.tmp", "sdtdelete.tmp")
print (dts_save) | words (> "sdtdelete_pat.tmp")
list = "sdtdelete_pat.tmp"
while (fscan (list, line) != EOF) {
    match (line, "sdtdelete.tmp", stop+, > "sdtdelete_temp.tmp")
    rename ("sdtdelete_temp.tmp", "sdtdelete.tmp")
}
list = ""

# Delete files and finish up.
sort ("sdtdelete.tmp") | uniq (> "sdtdelete.tmp")
delete ("@sdtdelete.tmp")
delete ("sdtdelete*.tmp,*.log")

plexit
