#!/bin/env pipecl
# 
# DP2STB -- Send data products to STB.

string  dataset, datadir, lfile, node, path, fname
string	dtnsanam, instrument, mjd, proctype, prodtype, ptype1, ptype2
string	pldname, plpropid, plqueue, plqname, plprocid, plfname
struct	obstype

# Load tasks and packages.
task $ckarchive = "$!ckarchive"
task $dtsproc = "$!dtsproc $(1) 2> $(2)"
task $du = "$!du -B M $(1)"
task $rdtsproc = "$!ssh $3 plexec dtsproc $(1) 2> $(2)"
task $rdu = "$!ssh $2 du -B M $(1)"
images
servers
utilities

# Set names and directories.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)
cd (datadir)

# We override the PMAS address used by newdataproduct in order to
# record size information for the reports.
set (NHPPS_NODE_DM=envget("NHPPS_NODE_CM"))
set (NHPPS_PORT_DM=envget("NHPPS_PORT_CM"))

# Log start of module.
printf ("\n%s (%s): ", strupr(envget("NHPPS_MODULE_NAME")), dataset) |
    tee (lfile)
time | tee (lfile)

# Setup hierarchy.
pldname = names.shortname
pldname = substr (pldname, 1, strldx("-",pldname)-1)
plqname = substr (dataset, 1, stridx("-",dataset)-1)
i = stridx ('_', plqname)
if (i > 0) {
    plqueue = substr (plqname, 1, i-1)
    plqname = substr (plqname, i+1, 1000)
    i = stridx ('_', plqname)
    if (i > 0) {
	plprocid = substr (plqname, i+1, 1000)
	plqname = substr (plqname, 1, i-1)
    } else 
        plprocid = 'TEST'
} else {
    plqueue = 'TEST'
    plprocid = 'TEST'
}

# Send files.
if (access(dataset//".dpslist")==NO) {
    concat ("*.dps", > dataset//".dpslist")
    delete ("*.dps")
}
;

list = dataset//".dpslist"
while (fscan (list, s1) != EOF) {
    hselect (s1//"[0]", "$PROCTYPE", yes) | scan (proctype)
    hselect (s1//"[0]", "$PRODTYPE", yes) | scan (prodtype)

    # Set node for possibly remote data products.
    i = stridx ("!", s1)
    if (i > 0) {
	path = substr (s1, i+1, 1000)
        node = substr (s1, 1, i-1)
	i = stridx (".", node)
	if (i > 0)
	    node = substr (node, 1, i-1)
	;
    } else {
        path = s1
	node = ""
    }

    # Set PL proposal id.
    hselect (s1//"[0]", "$DTPROPID", yes) | scan (plpropid)
    if (proctype == "MasterCal")
        plpropid = "CAL"
    ;

    # Set PL filename
    fname = substr (path, strldx('/',path)+1, 1000)
    i = strlstr (".fits", fname)
    if (i > 0)
        fname = substr (fname, 1, i-1)
    ;
    plfname = substr (fname, stridx('-',fname)+1, 1000)

    # Set STB/PL keywords.  For string format.
    nhedit (s1//"[0]", "PLPROPID", "dummy", "PL Proposal ID", add+)
    nhedit (s1//"[0]", "PLDNAME", "dummy", "PL Dataset", add+)
    nhedit (s1//"[0]", "PLQUEUE", "dummy", "PL Queue", add+)
    nhedit (s1//"[0]", "PLQNAME", "dummy", "PL Dataset", add+)
    nhedit (s1//"[0]", "PLPROCID", "dummy", "PL Processing ID", add+)
    nhedit (s1//"[0]", "PLFNAME", "dummy", "PL Filename", add+)
    nhedit (s1//"[0]", "PLPROPID", plpropid, ".")
    nhedit (s1//"[0]", "PLDNAME", pldname, ".")
    nhedit (s1//"[0]", "PLQUEUE", plqueue, ".")
    nhedit (s1//"[0]", "PLQNAME", plqname, ".")
    nhedit (s1//"[0]", "PLPROCID", plprocid, ".")
    nhedit (s1//"[0]", "PLFNAME", plfname, ".")

    # Set the filename keyword.
    hselect (s1//"[0]", "$FILENAME", yes) | scan (s2)
    if (s2 != "INDEF") {
	nhedit (s1//"[0]", "RAWFILE", s2, "Original raw file", add+,
	    before="FILENAME")

	i = strlstr (".fits", s2)
	if (i > 0)
	    s2 = substr (s2, 1, i-1)
	;
	ptype1 = proctype; ptype2 = prodtype
        if (ptype1 == "NoSS")
	    ptype1 = "_noss"
        if (ptype1 == "Resampled")
	    ptype1 = "_r"
	else if (ptype1 == "Stacked")
	    ptype1 = "_stk"
	else
	    ptype1 = ""
	if (ptype2 == "dqmask")
	    ptype2 = "_bpm"
	else if (ptype2 == "expmap")
	    ptype2 = "_expmap"
	#else if (ptype2 == "png")
	#    ptype2 = "_png"
	else if (ptype2 == "png1")
	    ptype2 = "_png1"
	else if (ptype2 == "png2")
	    ptype2 = "_png2"
	else
	    ptype2 = ""
	if (ptype1 == "")
	    ptype1 = "_pl"
	;
	nhedit (s1//"[0]", "PLOFNAME", s2//ptype1//ptype2,
	    "Original file name", add+)
    }
    ;
    #nhedit (s1//"[0]", "FILENAME", fname, "Current filename", add+)
    nhedit (s1//"[0]", "FILENAME", 
        plqueue//"_"//plqname//"_"//plprocid//"-"//plfname,
        "Current filename", add+)

    nhedit (s1//"[0]", "add_blank", "", "", add+, before="PLQUEUE")
    #nhedit (s1//"[0]", "add_blank", " # PL information",
    #    "", add+, before="PLQUEUE")

    if (dps_archive=="no" ||
        (dps_archive != "yes" && dps_archive != "dummy") ||
	envget("OSF_FLAG")=="N") {
	if (node == "")
	    du (path) | scan (x)
	else
	    rdu (path, node) | scan (x)
	newdataproduct (fname, class="STBOutput", size=x)
        next
    }
    ;

    # Check for previous version. Because it is so hard to pass
    # through values with spaces we replace them with underscores and
    # let the database match any character.
    hselect (s1//"[0]", "$DTNSANAM", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (dtnsanam)
    hselect (s1//"[0]", "$DTINSTRU", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (instrument)
    hselect (s1//"[0]", "$MJD-OBS", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (mjd)
    hselect (s1//"[0]", "$OBSTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (obstype)
    hselect (s1//"[0]", "$PROCTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (proctype)
    hselect (s1//"[0]", "$PRODTYPE", yes) |
        translit ("STDIN", " ", "_", del-, col-) | scan (prodtype)
    if (mjd == "INDEF")
	hselect (s1//"[1]", "$MJD-OBS", yes) |
	    translit ("STDIN", " ", "_", del-, col-) | scan (mjd)
    ;
    printf ("'%s %s %s %s %s %s'\n",
	dtnsanam, instrument, mjd, obstype, proctype, prodtype) | scan (line)
    s2 = ""; ckarchive (line) | scan (s2)
    if (s2 != "" && s2 != "ERROR") {
        sendmsg ("WARN", "Earlier version in archive", s2, "DTS")
        next
    }
    ;

    # Send to STB.
    printf ("  Sending %s to STB\n", s1)
    if (node == "")
	dtsproc (path, "dp2stb.tmp")
    else
	rdtsproc (path, "dp2stb.tmp", node)

    # Check status and record dataproduct sent.
    count ("dp2stb.tmp") | scan (i)
    if (i == 0) {
	printf ("  %s sent to STB\n", s1) | tee (lfile)
	printf ("    PLQUEUE = %s\n", plqueue) | tee (lfile)
	printf ("    PLQNAME = %s\n", plqname) | tee (lfile)
	printf ("    PLPROCID = %s\n", plprocid) | tee (lfile)
	printf ("    PLFNAME = %s\n", plfname) | tee (lfile)

	if (node == "")
	    du (path) | scan (x)
	else
	    rdu (path, node) | scan (x)
	newdataproduct (s1, class="STBOutput", size=x)

	if (dts_ftp == "no" && dts_save == "") {
	    if (strstr("_png",s1) == 0)
		delete (s1)
	    ;
	}
	;

    } else {
	concat ("dp2stb.tmp")
	sendmsg ("ERROR", "STB failed", s1, "STB")
    }
    delete ("dp2stb.tmp")
}
list = ""

logout 1
