#!/bin/env pipecl
#
# LIS2MEF -- Break up an input list into MEF lists and call a pipeline.
# 
# The input is a list of all pipeline products.
# There must by 00.fits files to define the MEF datasets.
# Special hack for binned flats.

string	cpipe				# Calling pipeline
string	tpipe 				# Target pipeline
int	ntrig = 2			# Number to trigger here

int	founddir, numtrig
string	dataset, indir, datadir, ilist, lfile, spool, pipes, odir
string	tname, cname, textn, rextn
file	temp, temp1, temp2
struct	*fd1

# Set the default from the application parameters or environment.
numtrig = int (envget("CoresPerNode"))
ntrig = numtrig

# Tasks and packages.
task $pipeselect = "$!pipeselect"
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# The calling pipeline is set from the environment and the target pipeline
# is set by an argument.  The trigger and return extensions are set by
# the target pipeline.

names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))

cpipe = names.pipe
if (fscan (cl.args, tpipe, ntrig) < 1)
    logout 0
;
textn = "." // tpipe
rextn = "." // tpipe

# Files and directories.
dataset = names.dataset
indir = names.indir
datadir = names.datadir
spool = indir // "spool/"
ilist = indir // dataset // "." // cpipe
lfile = datadir // names.lfile
set (uparm = names.uparm)
temp = envget ("NHPPS_MODULE_NAME") // ".tmp"
temp1 = envget ("NHPPS_MODULE_NAME") // "1.tmp"
temp2 = envget ("NHPPS_MODULE_NAME") // "2.tmp"

# Work in data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)
if (access(temp))
    delete (temp)
;

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Check for pipelines.
pipes = spool // dataset // rextn
touch (pipes)
if (defvar ("NHPPS_MIN_DISK"))
    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, envget("NHPPS_MIN_DISK") | sort (> pipes)
else
    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0) | sort (> pipes)
count (pipes) | scan (i)
if (i < min(1,ntrig)) {
    sendmsg ("ERROR", "Pipeline not running", tpipe, "PIPE")
    delete (pipes)
    logout 2
}
;

# Get list of MEF global files.
match ("[^s]_00.fits", ilist, > temp1)

# Setup uparm directory from header of first file.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
s1 = ""; head (temp1) | scan (s1)
iferr {
    setdirs (s1)
} then {
    logout 0
} else
    ;

# Set MEF patterns.
list = temp1
while (fscan (list, s1) != EOF) {
    s2 = substr (s1, strldx("-",s1)+1, strlstr("_00.fits",s1)-1)
    print (s2, >> temp)
}
list = ""; delete (temp1)

# Set binned calibrations.
match ("Z2-ccd", ilist) | count | scan (i)
if (i > 0)
    print ("Z2-ccd", >> temp)
;
match ("F2-ccd", ilist) | count | scan (i)
if (i > 0)
    print ("F2-ccd", >> temp)
;

# Set PGR pattern.
match ("_pgr.fits", ilist) | count | scan (i)
if (i > 0)
    print ("pgr", >> temp)
;

# Set FRG pattern.
match ("_frg.fits", ilist) | count | scan (i)
if (i > 0)
    print ("frg", >> temp)
;

# Set SFT pattern.
match ("_sft.fits", ilist) | count | scan (i)
if (i > 0)
    print ("sft", >> temp)
;

count (temp) | scan (i)
if (i == 0) {
    sendmsg ("WARNING", "No data found", "", "VRFY")
    delete (pipes)
    delete (temp)
    logout 3
}
;

# Loop over each MEF and setup triggers and returns for calling pipeline.
# Trigger a few pipeline instances.  The rest will be triggered by the
# returns.

i = 1
copy (ilist, temp1)
list = temp; fd1 = pipes; touch (dataset//textn)
while (fscan (list, s1) != EOF) {
    # Only match in the last dataset name.
    s2 = s1 // "[^/]*$"

    # Set names.
    tname = dataset // "-" // tpipe // s1
    cname = dataset // "-" // tpipe // s1

    # Check for previous submission in case of a restart.
    s3 = ""; match (cname//rextn, dataset//textn) | scan (s3)
    if (s3 != "") {
	printf ("Data already submitted: %s\n", cname)
        i += 1
        next
    } else
	printf ("Submitting: %s\n", cname)

    # Create @file.
    match (s2, temp1, >> indir//"data/"//tname//textn)
    match (s2, temp1, stop+, > temp2)
    rename (temp2, temp1)

    # Setup files for triggering and return.
    print (tname, >> spool//cname//rextn)
    print (cname//rextn, >> dataset//textn)

    # Trigger a certain number of instances of the target pipeline.
    if (i > ntrig)
        next
    ;
    if (fscan (fd1, odir) == EOF) {
        i += 1
	fd1 = pipes
	if (fscan (fd1, odir) == EOF)
	    next
	;
    }
    ;
    if (i > ntrig)
        next
    ;

    # Trigger the target pipeline.
    copy (indir//"data/"//tname//textn, odir//tname//textn)
    pathname (indir//cname//rextn, >> odir//"return/"//tname//textn)
    print (substr(odir,1,stridx("!",odir)-1), > spool//cname//rextn//".node")
    touch (odir//tname//textn//"trig")
}
list =""; fd1 = ""
delete (temp)

logout 1
