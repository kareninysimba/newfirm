#!/bin/env python
#
#  header keyword remediation functions
#

import re
import os
import sys
import pyfits as p
import sqlite
from sqlutil import *
from sendmsg import *

# The code immediately below is a duplication of what is written in dirverify.py
# This should be consolidated into something loadable.

# Set up message monitor
msghost = os.environ['NHPPS_NODE_MM']
msgport = os.environ['NHPPS_PORT_MM']

# Get the node name
thishost = os.environ ['NHPPS_NODE_NAME']

# Wrapper function around send_message
def sendmsg ( status, message, submsg, id, dset ):
    omsg = message
    if (len(submsg)>0):
        message += ' ('+submsg+')'
    send_message( msghost, int(msgport), message, id, dset,
                  thishost, status, 'DIRVERIFY', False )
    return omsg

# Precompile search on whitespace
onwhitespace = re.compile('\s+')

# Load global variables for the pipeline
# This needs to be turned into something loadable, see
# dirverify.py for more info on this.
globals = file (os.path.join (
    os.environ ['NHPPS_PIPESRC'], os.environ ['NHPPS_SYS_NAME'],
    'config', '%s.cl' % (os.environ ['NHPPS_SYS_NAME'])), 'r')

for line in globals.readlines():
    thisline = line.strip()
    fields = onwhitespace.split(thisline)
    if (len(fields)>=5):
        if (fields[2]=='='):
            # Retrieve the argument (between the '=' and the '{')
            m = re.search( '=\s+(.*)\s*\{', thisline )
            thisvalue = m.group(1)
            if (fields[3]=='yes'): thisvalue='1'
            if (fields[3]=='no'): thisvalue='0'
            if (fields[3]=='INDEF'): thisvalue='"INDEF"'
            global verbose
            exec fields[1]+'='+thisvalue

# Set up connection to the data manager
DMConnected = True
dmhost = os.environ['NHPPS_NODE_DM']
dmport = os.environ['NHPPS_PORT_DM']
m = re.search( '(\d+)', dmport )
try:
    dmport = int(m.group(1))
except:
    DMConnected = False

def remediate( hdu, subset, key, expr, meth ):
    # Default is to assume no remediation is needed (-1).
    # 0 indicates remediation is needed but failed, 1 means
    # remediation is necessary and was successful.
#    print 'remediate %s %i %s %s' % ( key, subset, expr, meth )
    needed = -1
    x = ''
    type = 'str'
    instr = ''
    if re.search( '^_', meth ) and expr!='-':
        success = True
        #Remediation is done with a python method in this case
        # Try to get the keyword from the header
        try:
            x = hdu[subset].header[key]
        except:
            success = False
            x = 0
#        print 'orig %s %s %i %s %s' % ( key, x, subset, expr, meth )
        if success and meth == '_EVAL':
            # If we have the keyword and the method is "EVAL",
            # evaluate the expression and return the results
            myexpr = 'success='+expr
            try:
                exec myexpr
            except:
                success = False
            if success:
                # The expression was evaluated successfully and returned True
                needed = -1
            else:
                # The expression failed to evaluate or returned False
                needed = 0
            type = ''
            instr = ''
            return( needed, x, type, instr )

        # If we have the keyword, try to evaluate the expression
        if success:
            myexpr = 'success='+expr
            try:
                exec myexpr
            except:
                success = False
        # If we did not get the keyword, the expression failed
        # to execute, or returned false, try to remediate the keyword
        checkdel = False
        if not success:
            # Remediation is needed
            needed = 1
            if re.search( '^.D', meth ):
                # Remove the D from meth
                meth = '_'+meth[2:]
                try:
                    tmp = hdu[0].header[key]
                except:
                    if subset==0:
                        # Keyword may be missing from global header. If so
                        # deletion of subset keywords is needed if
                        # remediation below is successful.
                        checkdel = True
                    else:
                        # Keyword does not exist in global header, so
                        # it cannot be deleted from the subset
                        needed = -1
                else:
                    # Delete the keyword from all the subsets
                    try:
                        nextend = hdu[0].header['nextend']
                    except:
                        needed = 1
                    else:
                        needed = 1
                        instr = 'subdel'
            if len(meth)>2:
                myexec = 'success,x,type='+meth[1:]+'( hdu, subset )'
                try:
                    exec myexec
                except:
                    # Remediation failed
                    needed = 0
                else:
                    if success==1:
                        # If the remediation returned success, make
                        # sure the returned value is valid
                        myexpr = 'success='+expr
                        try:
                            exec myexpr
                            if not success:
                                # remediated value is not valid ...
                                needed = 0
                                # unless expr==False, which means that remediation
                                # was forced
                                if expr=='False':
                                    needed = 1
                        except:
                            needed = 0
                    elif success == -1:
                        # No remediation needed
                        needed = -1
                    elif success == 0:
                        needed = 0
                    else:
                        # We got an incorrect value back from the function 
                        sendmsg( 'ERROR', 'Remediation function returned forbidden value',
                                 '', 'VRFY', '' )
                        sys.exit(1)
            if checkdel and needed>0 and success:
                instr = 'subdel'
    # Return the results
    return( needed, x, type, instr )

def mjdobs( hdu, subset ):
    # Fix broken or missing mjd-obs

    success = True
    try:
        thedate = hdu[subset].header['dtcaldat']
    except:
        success = False
    else:
        fields = thedate.split('-')
        if len(fields)!=3:
            success = False
    try:
        themst = hdu[subset].header['lsthdr']
    except:
        success = False
    else:
        sub = themst.split(':')
        if len(sub)!=3:
            success = False

    success,tel = getkeyword( hdu, subset, 'telescop' )

    if success:
        mst = float(sub[0])+float(sub[1])/60+float(sub[2])/3600
        success,mjd,ut = getmjdut( int(fields[0]), int(fields[1]), int(fields[2]), mst, tel )
        
    if success:
        return( True, mjd, 'float' )
    else:
        return( False, 0, 'float' )

def timeobs( hdu, subset ):
    # Fix missing time-obs

    success,thedate = getkeyword( hdu, subset, 'dtcaldat' )
    if success:
        fields = thedate.split('-')
        if len(fields)!=3:
            success = False
    if success:
        success,themst = getkeyword( hdu, subset, 'lsthdr' )
        if success:
            sub = themst.split(':')
            if len(sub)!=3:
                success = False
    success,tel = getkeyword( hdu, subset, 'telescop' )

    if success:
        mst = float(sub[0])+float(sub[1])/60+float(sub[2])/3600
        success,mjd,ut = getmjdut( int(fields[0]), int(fields[1]), int(fields[2]), mst, tel )

    if success:
        hour,min,sec = tim2str( ut )
        uts = hour+':'+min+':'+sec
        return( True, uts, 'str' )
    else:
        return( False, '', 'str' )

def dateobs( hdu, subset ):
    # Fix missing obsid

    success,thedate = getkeyword( hdu, subset, 'dtcaldat' )
    if success:
        fields = thedate.split('-')
        if len(fields)!=3:
            success = False
    if success:
        success,themst = getkeyword( hdu, subset, 'lsthdr' )
        if success:
            sub = themst.split(':')
            if len(sub)!=3:
                success = False
    success,tel = getkeyword( hdu, subset, 'telescop' )
    
    if success:
        mst = float(sub[0])+float(sub[1])/60+float(sub[2])/3600
        success,mjd,ut = getmjdut( int(fields[0]), int(fields[1]), int(fields[2]), mst, tel )

    if success:
        # Build obsid from mjd and ut. Obsid has the form <DTTELESC>.<DATE-OBS>
        # date-obs has the form YYYY-MM-DD'T'HHMMSS.SSS, where 'T' is a literal T;
        # e.g. 20060501T121213.12. Example obsid: kp4m.20041117T232435
        # The UT calendar date is derived by feeding the mjd to mjd2date,
        # because dtcaldat contains the calendar date at the beginning of the
        # night, not the date at the time of observations
        year,month,day = mjd2date( mjd )
        hour,min,sec = tim2str( ut )
        dateobs = year+'-'+month+'-'+day+'T'+hour+':'+min+':'+sec
        return( True, dateobs, 'str' )
    else:
        return( False, '', 'str' )
    
def obsid( hdu, subset ):
    # Fix missing obsid

    success,thedate = getkeyword( hdu, subset, 'dtcaldat' )
    if success:
        fields = thedate.split('-')
        if len(fields)!=3:
            success = False
    if success:
        success,themst = getkeyword( hdu, subset, 'lsthdr' )
        if success:
            sub = themst.split(':')
            if len(sub)!=3:
                success = False
    success,tel = getkeyword( hdu, subset, 'telescop' )

    if success:
        if tel=='KPNO 4.0 meter telescope':
            telescope = 'kp4m'
        elif tel=='CTIO 4.0 meter telescope':
            telescope = 'ct4m'
        else:
            success = False
    
    success,tel = getkeyword( hdu, subset, 'telescop' )

    if success:
        mst = float(sub[0])+float(sub[1])/60+float(sub[2])/3600
        success,mjd,ut = getmjdut( int(fields[0]), int(fields[1]), int(fields[2]), mst, tel )

    if success:
        # Build obsid from mjd and ut. Obsid has the form <DTTELESC>.<DATE-OBS>
        # date-obs has the form YYYY-MM-DD'T'HHMMSS.SSS, where 'T' is a literal T;
        # e.g. 20060501T121213.12. Example obsid: kp4m.20041117T232435
        # The UT calendar date is derived by feeding the mjd to mjd2date,
        # because dtcaldat contains the calendar date at the beginning of the
        # night, not the date at the time of observations
        year,month,day = mjd2date( mjd )
        hour,min,sec = tim2str( ut )
        obsid = telescope+'.'+year+month+day+'T'+hour+min+sec
        return( True, obsid, 'str' )
    else:
        return( False, '', 'str' )

def gain( hdu, subset ):
    # Fix missing gain
    success,value = dogetcal( hdu, subset, 'gain' )
    return( success, float(value), 'float' )

def rdnoise( hdu, subset ):
    # Fix missing rdnoise
    success,value = dogetcal( hdu, subset, 'rdnoise' )
    return( success, float(value), 'float' )

def saturate( hdu, subset ):
    # Fix missing saturate
    success,value = dogetcal( hdu, subset, 'saturate' )
    return( success, int(value), 'int' )
    
def filter( hdu, subset ):

    # Check whether filter name is defined and whether it is known.
    # If it is not known, try to correct it
    try:
        filter = hdu[subset].header['filter']
    except:
	# Don't care about the filter for zeros or darks.
	try:
	    obstype = hdu[subset].header['obstype']
	except:
	    sendmsg( 'WARNING', 'Obstype keyword not found in header',
		     '', 'VRFY', '' )
	else:
	    obstype = obstype.lower()
	    if (obstype=='zero' or obstype=='dark'):
		# Use blank filter.
		msg = sendmsg( 'WARNING',
		    'Filter name set to blank for zero/dark',
		    '', 'VRFY', '' )
		return( True, 'blank', 'str' )

        sendmsg( 'WARNING', 'Filter keyword not found or misformated in header',
                 '', 'VRFY', '' )
        #logrejects( thisfile, msg )
        return( False, '', 'str' )

    # Extract filter ID from full name
    if (verbose>=2):
	print "FILTER %s" % (filter)
    m = re.search( '([ckCK#][ #-]*\d{4})', filter )
    try:
	filterid = m.group(1).replace ( ' ', '' ).replace ( '-', '')
    except:
    	filterid = None

    # If filter ID is missing check for a FILTERSN keyword.
    if not filterid:
        try:
	    filterid = hdu[subset].header['filtersn']
	except:
	    filterid = None
	else:
	    m = re.search( '([ckCK#][ #-]*\d{4})', filterid )
	    try:
		filterid = m.group(1).replace ( ' ', '' ).replace ( '-', '')
	    except:
		filterid = None

    if filterid:
	# Retrieve filter name for this filterid from database
	sql  = "SELECT filter FROM catalog "
	sql += "WHERE class='photindx' "
	sql += "AND filter LIKE '%"+filterid+"%'"
	try:
	    fields, data = execute_sql( sql, dmhost, dmport )
	except:
	    num_rows = 0
	else:
	    # Loop over all the results
	    num_rows = 0
	    while 1:
		if num_rows>=1:
		    prow = row
		row = fetchrow( fields, data )
		if not row:
		    break
		num_rows += 1
	if num_rows == 0:
	    # Don't care about the filter for zeros or darks.
	    try:
		obstype = hdu[subset].header['obstype']
	    except:
		sendmsg( 'WARNING', 'Obstype keyword not found in header',
			 '', 'VRFY', '' )
	    else:
		obstype = obstype.lower()
		if (obstype=='zero' or obstype=='dark'):
		    # Use blank filter.
		    msg = sendmsg( 'WARNING',
			'Filter name corrected to blank for zero/dark',
			filterid, 'VRFY', '' )
		    return( True, 'blank', 'str' )

	    msg = sendmsg( 'WARNING', 'Filter id not found in data base',
			   filterid, 'VRFY', '' )
	    #logrejects( thisfile, msg )
	    #return( False, '', 'str' )
	elif num_rows == 1:
	    # filterid has been found in the database, check whether it
	    # matches the name from the header, otherwise update the header
	    # and give a warning
	    if not (prow['filter'] == filter):
		#hdu[subset].header['filter'] = prow['filter']
		sendmsg( 'WARNING', 'Filter name corrected', filter,
			 'VRFY', '' )
		return( True, prow['filter'], 'str' )
	    else:
		# No remediation is necessary
		return( -1, '', 'str' )
	else:
	    sendmsg( 'ERROR', 'Filter id more than once in the database',
		     filterid, 'VRFY', '' )
	    return( False, '', 'str' )

    # Check for filter in filter alias database.  This requires
    # that the DETECTOR keyword be defined to allow for the same
    # alias being used for different detectors.
    try:
	detector = hdu[subset].header['detector']
    except:
	try:
	    detector = hdu[subset].header['instrume']
	except:
	    msg = sendmsg( 'WARNING', 'Detector keyword not in header',
			   filter, 'VRFY', '' )
	    #logrejects( thisfile, msg )
	    return( False, '', 'str' )
    sql  = "SELECT value FROM catalog "
    sql += "WHERE class='filtalias' "
    sql += "AND detector='%s' " % detector
    sql += "AND filter='%s' " % filter
    try:
	fields, data = execute_sql( sql, dmhost, dmport )
    except:
        num_rows = 0
    else:
	# Loop over all the results
	num_rows = 0
	while 1:
	    if num_rows>=1:
		prow = row
	    row = fetchrow( fields, data )
	    if not row:
		break
	    num_rows += 1
    if num_rows == 0:
	msg = sendmsg( 'WARNING', 'Filter not found in alias database',
		       filter, 'VRFY', '' )
	#logrejects( thisfile, msg )
	#return( False, '', 'str' )
	# Let modules figure out what to do.
	return( -1, '', 'str' )
    elif num_rows == 1:
	#hdu[subset].header['filter'] = prow['value']
	sendmsg( 'WARNING', 'Alias filter name corrected', filter,
		 'VRFY', '' )
	return( True, prow['value'], 'str' )
    else:
	sendmsg( 'ERROR', 'Filter more than once in the alias database',
		 filter, 'VRFY', '' )
	return( False, '', 'str' )

# ------------------------------------------------------------------- #
#                           Helper methods                            #
# ------------------------------------------------------------------- #

def dogetcal( hdu, subset, keyword ):
    # Retrieve a keyword from the 'catalog' catalog in the data manager

    success = True
    try:
        ampname = hdu[subset].header['ampname']
    except:
        success = False
    # Get the mjd from the header
    if success:
        success,mjd = getkeyword( hdu, subset, 'mjd-obs' )

    if success and DMConnected:
        # Set up the SQL query
        sql  = "SELECT value, '%s' AS mjd, " % str(mjd)
        sql += "'%s' - (mjdstart + mjdend) / 2 AS dmjd " % str(mjd)
	sql += "FROM catalog WHERE class='%s' " % keyword
	sql += "AND imageid='%s' " % ampname
	sql += "AND mjd >= mjdstart AND mjd < mjdend "
	sql += "ORDER BY ABS (dmjd), dmjd "
	sql += "LIMIT 1"
        # Execute the query
	try:
	    fields, data = execute_sql( sql, dmhost, dmport )
	except:
	    success = false
	else:
	    # Because of the LIMIT 1 in the select statement, there should only be 0 or 1 lines
	    try:
		row = fetchrow( fields, data )
	    except:
		success = false

    if success:
        return( True, row['value'] )
    else:
        return( False, 0 )

def getkeyword( hdu, subset, keyword ):
    # Get the keyword from the header. If it fails at the subset level,
    # try again at the global level because it may be an inherited keyword.
    # The value is returned as a string, and should be converted to the
    # desired format by the calling method.
    success = True
    answer = ''
    try:
        answer = str(hdu[subset].header[keyword])
    except:
        success = False
        if subset>0:
            try:
                answer = str(hdu[0].header[keyword])
            except:
                success = False
            else:
                success = True
    return( success, answer )

def getmjdut( year, month, day, mst, tel ):
    # Convert date to MJD at UT1=0. Assume longitude fixed at Kitt Peak:
    # west longitude (h.m.s) =   7 26 28 = 7.4411111h = 111.616667
    # Longitude for CTIO = 70.815
    # This method is based on the code in IRAF's asttimes.x

    success = True
    if tel=='KPNO 4.0 meter telescope':
        longitude = 111.616667
        timeoffset = -7
    elif tel=='CTIO 4.0 meter telescope':
        longitude = 70.815
        timeoffset = -4
    else:
        success = False

    if success:
        if (year < 100):
            y = 1900 + year
        else:
            y = year
        
        if (month > 2):
            m = month + 1
        else:
            m = month + 13
            y = y - 1

        jd = int (365.25 * y) + int (30.6001 * m) + day + 1720995
        if (day + 31 * (m + 12 * y) >= 588829):
            d = int (y / 100)
            m = int (y / 400)
            jd = jd + 2 - d + m
        jd = jd + 0.5

        # Compute GMST.
        t = (jd - 2451545) / 36525
        gmst = 24110.54841 + t * (8640184.812866 + t * (0.093104 - t * 6.2E-6))
        gmst = gmst / 3600

        # Compute UT1 from MST and GMST.
        ut = mod( (mst - (gmst - longitude/15)), 24 )
        # Because we submit dtcaldat to getmjdut, the date change happens in the local
        # time zone, not in the UT timezone, so we need to offset by 8 hours, the time
        # difference between GMT and MST.
        if (ut < timeoffset): 
            ut += 24
        # Compute MJD.
        mjd = jd - 2400000.5 + ut / 24
        # Correct for the difference in duration between sidereal and solar days
        ut = ut/1.00273791
        # Add 24h to negative hours
        if (ut<0):
            ut += 24
    # Return the results
    if success:
        return( True, mjd, ut )
    else:
        return( False, 0, 0 )

def mjd2date( mjd ):
    # Convert an MJD to a calendar date. The code in this method is adopted
    # from ast_julday_to_date in IRAF's asttimes.x

    jd = mjd+2400000.5
    ja = nint( jd )
    t = 24*(jd - ja + 0.5)
    
    if (ja >= 2299161):
        jb = int (((ja - 1867216) - 0.25) / 36524.25)
        ja = ja + 1 + jb - int (jb / 4)

    jb = ja + 1524
    jc = int (6680. + ((jb - 2439870) - 122.1) / 365.25)
    jd = 365 * jc + int (jc / 4)
    je = int ((jb - jd) / 30.6001)
    day = jb - jd - int (30.6001 * je)
    month = je - 1
    if (month > 12):
        month = month - 12
    year = jc - 4715
    if (month > 2):
        year = year - 1
    if (year < 0):
        year = year - 1

    syear = str(year)
    smonth = str(month)
    if len(smonth)==1:
        smonth = '0'+smonth
    sday = str(day)
    if len(sday)==1:
        sday = '0'+sday

    return( syear, smonth, sday )

def tim2str( tim ):
    hour = int(tim)
    min = (tim-hour)*60
    sec = (min-int(min))*60
    shour = str(int(hour))
    smin = str(int(min))
    ssec = str(int(sec))
    if len(shour)==1:
        shour = '0'+shour
    if len(smin)==1:
        smin = '0'+smin
    if len(ssec)==1:
        ssec = '0'+ssec
    return( shour, smin, ssec )

def mod( a, b ):
    if a>0:
        while a>b:
            a -= b
    if a<0:
        a=-a
        while a>b:
            a -= b
        a=-a
    return a

def nint( a ):
    if a>=0:
        return( int(a+0.5) )
    else:
        return( int(a-0.5) )
        
