#!/bin/env pipecl
#
# Linearize an input array

int status=1
real lincoeff
string infile, detector, lineq
string a, b

file    caldir = "MC$"

images
servers

if (fscan (cl.args, infile) < 1) {
    printf ("Usage: linearize.cl infile\n")
    logout 0
}
;

# Create short version of the filename
s1 = substr (infile, strldx("/",infile)+1, strlstr(".fits",infile)-1)

# Get the detector name
detector = ""
hselect( infile, "detector", yes ) | scan( detector )

# Default to no correction
lincoeff = 0

# Get the linearity coefficient and equation from the calibration database
getcal( infile, "lincoeff", cm, caldir, 
    obstype="", detector="!instrume", imageid="!detector",
    filter="", exptime="", mjd="!mjd-obs")
if ( getcal.statcode == 0 ) {
    getcal( infile, "lineq", cm, caldir, 
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="", mjd="!mjd-obs")
}
;
if (getcal.statcode>0) { # Getcal was not successful
    sendmsg( "ERROR",
        "Getcal failed - no nonlinearity correction performed",
        s1, "CAL" )
    status = 0
} else { # Getcal was successful
    # Get the linearity coefficient from the header
    lincoeff = INDEF
    lineq = ""
    hsel( infile, "lincoeff,lineq", yes ) | scan( lincoeff, lineq )
    if ( lincoeff == INDEF ) { # No coefficient in the header
        sendmsg( "ERROR",
            "No lincoeff in header - no nonlinearity correction performed",
            s1, "CAL" )
        status = 0
    } else {
	if ( lineq == "" ) {
            sendmsg( "ERROR",
                "No lineq in header - no nonlinearity correction performed",
                s1, "CAL" )
            status = 0
        } else {
            # Apply the nonlinearity correction using the 
            # linearity imexpression database
            imexpr( lineq, "output.fits", a=infile, b=lincoeff,
                exprdb="pipedata/linearity.db", outtype="real" )
            imdel( infile )
            imrename( "output.fits", infile )
            printf( "Linearized: coefficient=%10.4e\n", lincoeff ) | scan( line )
            hedit( infile, "LINEAR", line, add+, ver- )
        }
    }
    ;
}

logout( status )
