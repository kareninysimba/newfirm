# PSQL -- Execute SQL statement and return space separated values.
#
# This routine isolates the way the SQL statement is made and how the return
# data is formated so we can substitute various methods.  This might be
# using sqlite directly, using mysql on a remote node, etc.  This version
# uses the data manager which is another level of isolation.
#
# This version requires the SNDSERVER task is defined.

procedure psql (sql)

string	sql			{prompt="SQL statement"}

string	server = ""		{prompt="Server"}

begin
	printf ("COMMAND=sql_query\nSQL=""%s""\nFORMAT=rows\nEOF\n", sql) |
	    sndserver ("STDIN", server, "STDOUT") |
	    match ("ROW") | fields ("STDIN", "3-99x2")
end
