#!/bin/env pipecl
#
# MTDSTATS

int	status = 1
string	dataset, indir, datadir, ilist, lfile, flat, image

# Tasks and packages
images
servers

# get the dataset name
swcnames( envget("OSF_DATASET") )
dataset = swcnames.dataset

# Set paths and files.
indir = swcnames.indir
ilist = indir // dataset // ".swc"

datadir = swcnames.datadir
lfile = datadir // swcnames.lfile
set (uparm = swcnames.uparm)
set (pipedata = swcnames.pipedata)
cd( datadir )

# Log start of processing.
printf( "\nSWCFLATFIELD (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

# Create local copies of the data (this was part of swcace).
list = ilist
while ( fscan( list, s1 ) != EOF ) {

    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    swcnames( s2 )
    image = swcnames.image

    # Create a local copy
    if ( imaccess(image) == NO )
	imcopy( s1, image )
    ;
}
list = ""

if ( whenflatfield != "after" ) {
    print( "Flat field was applied BEFORE sky subtraction." )
    logout( 1 )
}

# Apply the flat field
list = ilist
while (fscan (list, s1) != EOF) {

    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    swcnames( s2 )
    image = swcnames.image

    # Retrieve the location of the flat field from the header, if available
    flat = "" ; hselect( image, "FLAT", yes ) | scan( flat )
    if (flat == "") {
        printf( "No flat field was applied!\n" ) | tee( lfile )
    } else {
        # Dome flat available, so divide data by flat
        imarith( image, "/", flat, image )
        printf( "Applied flat field: %s\n", flat ) | tee( lfile )
    }

}
list = ""

logout 1
