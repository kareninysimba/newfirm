#!/bin/env pipecl
#
# SWCACE -- First ACE pass.

file	obm, cat, sky, sig
string	module, indir, datadir, dataset, image, rtnfile, rfile, lfile
string  ilist, olist, defmeth, fourqmeth, offmeth, method
int     ir, ix, iy, rad
int	deforder, fourqorder, offorder, order
real    mjd, x1, x2, skymode, photapd
string	nocmpat, nocdpat, fsobject

# Define packages and tasks.
fitsutil
servers
utilities
proto
tables
ttools
images
noao
nproto
nfextern
ace
dataqual
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"
task $swcace_ace_proc = "$swcace_ace_proc.py $1 $2"
task parseresbck = "NHPPS_PIPESRC$/NEWFIRM/SKY/parseresbck.cl"

# Set directories and files.
swcnames (envget("OSF_DATASET"))
module = envget ("NHPPS_MODULE_NAME")
indir = swcnames.indir
datadir = swcnames.datadir
dataset = swcnames.dataset
ilist = indir // dataset // ".swc"
olist = "swcace1.tmp"
lfile = datadir // swcnames.lfile
set (uparm = swcnames.uparm)
set (pipedata = swcnames.pipedata)

cd (datadir)

# Log start of processing.
printf( "\n%s (%s): ", strupr(module), dataset ) | tee (lfile)
time | tee( lfile )

# Parse the contents of skyresbck.
parseresbck( sky_resbck ) |
    scan( defmeth, deforder, fourqmeth, fourqorder, offmeth, offorder )
print( sky_resbck )
printf( "%s %d %s %d %s %d\n", defmeth, deforder, fourqmeth, fourqorder, offmeth, offorder )

list = ilist
count( ilist ) | scan( i )
if ( i == 0 )
    touch( olist )
;
while (fscan(list,s1) != EOF) {
    flpr
    # Create a short version of the dataset name.
    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    swcnames( s2 )
    image = swcnames.image
    sky   = swcnames.sky
    sig   = swcnames.sig
    obm   = swcnames.obm
    cat   = swcnames.cat

    imdelete (sky, >& "dev$null")
    imdelete (sig, >& "dev$null")
    imdelete (obm, >& "dev$null")
    imdelete ("swcaceimg", >& "dev$null")
    delete (cat, >& "dev$null")

    # Create a local copy of the image.
    if (imaccess(image) == NO)
	imcopy( s1, image )
    ;

    # Record the image.
    hsel( image, "MJD-OBS", yes ) | scan( mjd )
    newDataProduct( image, mjd, "objectimage", "" )
    storekeywords( class="objectimage", id=s1, sid=image, dm=dm )

    # Do ACE DQ on the image.
    imacedq (image, "!CBPM", "swcaceimg", sky, sig, obm, cat, lfile, dm, module,
	convolve="", hsigma=4, minpix=6, ngrow=2, agrow=2.,
	catdefs="pipedata$swcace.def", magzero="!MAGZREF", verbose=2, mosaic+,
	cafwhm=3.)
    if (imacedq.status != 1) {
        # Current workaround: delete the local copy so that it will not be
	# passed on to downstream pipelines.
	imdelete (image)
	next
    }

    # Set the default residual background removal method
    method = defmeth
    order = deforder

    # In four-shooter mode (or 4Q or Q mode) ACE's sky image will be
    # contaminated with emission from the extended source. To get around
    # this, sky is only considered in the area outside a circle with a
    # radius given by sky_4qradius (a percentage of half the diagonal).
    nocmpat = "" ; hsel( image, "NOCMPAT", yes ) | scan( nocmpat )
    nocdpat = "" ; hsel( image, "NOCDPAT", yes ) | scan( nocdpat )
    fsobject = "" ; hsel( image, "FSOBJECT", yes ) | scan( fsobject )
    if ( ( (nocmpat=="4Q") || (nocdpat=="4Q") || (nocdpat=="Q") || (nocmpat=="Q") ) && (fsobject=="yes") ) {
	method = fourqmeth
	order = fourqorder
	if ( fourqmeth == "4Q" ) {
	    # Get the size of the sky image and determine the radius of the
	    # circle in pixels.
	    hsel( sky, "NAXIS1,NAXIS2", yes ) | scan( ix, iy )
	    ir = ix
	    if ( iy < ir )
	        ir = iy
	    ;
	    # Divide by 2 (diameter to radius) and 100 (percent to fraction)
	    rad = ir*sky_4qradius/200
	    ix = ix/2
	    iy = iy/2
	    printf( "%d %d %d\n", ix, iy, rad ) | scan( line )
	    print( line )
	    # Fit linear surface to the region outside that circle
	    imsurfit( sky, "fitsky", xorder=2, yorder=2, cross_terms+,
	        function="legendre", type_output="fit",
	        regions="invcircle", circle=line )
	    # Subtract it from the original image.
	    imdel ("swcaceimg")
	    printf( "Run aceall for sky subtraction\n", >> lfile )
	    aceall (image, skyotype="subsky", skyimages="swcaceimg",
	        skies="fitsky", masks="", exps="", gains="", scales="",
	        extnames="", logfiles=lfile, verbose=2, rimages="",
	        rmasks="", rexps="", rgains="", rscales="", roffset="",
	        rcatalogs="", catalogs="", catdefs="", catfilter="",
	        order="", nmaxrec=INDEF, magzero="INDEF", gwtsig=INDEF,
	        gwtnsig=INDEF, objmasks="+_obm", omtype="all",
	        sigimages="", sigmas="", rskies="", rsigmas="",
	        fitstep=100, fitblk1d=10, fithclip=2., fitlclip=3.,
	        fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
	        blksize=-10, blknsubblks=2, hdetect=yes, ldetect=no,
	        updatesky=yes, bpdetect="1-100", bpflag="1-100",
	        convolve="bilinear 3 3", hsigma=3., lsigma=10.,
	        neighbors="8", minpix=6, sigavg=4., sigmax=4.,
	        bpval=INDEF, rfrac=0.5, splitstep=0.4, splitmax=INDEF,
	        splitthresh=5., sminpix=8, ssigavg=10., ssigmax=5.,
	        ngrow=2, agrow=2., cafwhm=3.)
	    imdel( "fitsky" )
	}
	;
    }
    ;

    # Test whether offset sky exposures were used to subtract the sky.
    line = ""; hselect( image, "SSUBINFO", yes ) | scan( line )
    if ( strstr("ffset",line) > 0 ) {
	method = offmeth
	order = offorder
    }

    printf( "Residual sky removal: %s %d\n", method, order )
    # Remove the residual background
    if ( method == "none" ) {
	imdel( "swcaceimg" )
    } else if ( method == "poly" ) {
	# Subtract low-order polynomial
        hedit( image, "OBM", image//"_obm[pl]", add+, ver- )
	iferr {
            mscskysub( image, image//"_bckfit", type_output="fit",
                xorder=order, yorder=order, function="leg",
		upper=5, lower=5,
	        cross_terms-, regions="mask", mask="!OBM" )
	} then {
	    # mscskysub failed, use ace background instead
            sendmsg( "WARNING", "mscskysub failed", image, "PROC" )
            imdel( image ); imrename( "swcaceimg", image )
        } else {
            # Subtract the fit. This is done outside of mscskysub because
	    # mscskysub modifies the fit so that the average background is not
	    # changed. But here the goal is to change the background to zero.
            imarith( image, "-", image//"_bckfit",  image//"_bcksub" )
            imdel( image ); imrename( image//"_bcksub", image )
	    imdel( image//"_bckfit" )
	}
        hedit( image, "OBM", del+, ver- )
    } else {
        # Replace image with sky subtracted version calculated above.
	# This is done when skyresbck is "4Q" or "ace". "ace" is the
	# default behavior
        imdel( image ); imrename( "swcaceimg", image )
    }

    if (cat != "") {
	photapd = INDEF
        thselect( cat, "CARADIUS", yes ) | scan( photapd )
        if ( isindef( photapd ) == NO ) {
            nhedit( image, "PHOTAPD", 2.*photapd,
                "[pix] Aperture diameter for MAGZERO", add+ )
        }
        ;
    }
    ;

    # Add to new list.
    print( s1, >> olist )
}

# Check for (partial) success.
if (access(olist) == NO)
    logout 0
;

# Reset input list.
rename( olist, ilist )

# Return results.
rtnfile = indir // "return/" // dataset // ".ace"
if (access (rtnfile)) {
    printf ("%s\n", rtnfile)
    type (rtnfile)
    head (rtnfile, nlines=1) | scan (rfile)
    pathnames (indir//dataset//".hdrtrig", > dataset//".ace")
    pathnames ("*cat*,*sky*,*sig*,*obm*", sort-, >> dataset//".ace")
    copy (dataset//".ace", rfile); delete (dataset//".ace")
    touch (rfile//"trig")
    delete (rtnfile)
}
;

logout 1
