#!/bin/env pipecl
#
# SWCHDR -- Set SIF header with information from the MEF WCS solutions.

string  dataset, indir, datadir, ilist, lfile
string  image, gclimage, bpm, obm, cat

# Load packages.
images
servers
dataqual

# Set file and path names.
swcnames (envget("OSF_DATASET"))
dataset = swcnames.dataset
indir = swcnames.indir
datadir = swcnames.datadir
ilist = indir // dataset // ".swc"
lfile = datadir // swcnames.lfile
set (uparm = swcnames.uparm)
cd (datadir)

# Log start of processing.
printf( "\nSWCHDR (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

# Loop through input list.
list = ilist
while (fscan(list,s1) != EOF) {

    # Create a short version of the dataset name
    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    swcnames( s2 )
    print( s2 )

    # Files to be updated.
    image = swcnames.image
    gclimage = ""
    bpm = swcnames.cbpm
    obm = swcnames.obm // "[pl]"
    cat = swcnames.cat

    # Check access.
    if (imaccess(image)==NO)
        next
    ;
    if (sciencepipeline) { 
        s3 = substr( image, 1, strlstr("_ss",image)-1 )
        match( "_OGC?*"//s3, "wcsgcl" ) | scan( gclimage )
	if (gclimage!="" && imaccess(gclimage)==NO)
	    gclimage = ""
	;
    }
    ;
    if (imaccess(bpm)==NO)
        bpm = ""
    ;
    if (imaccess(obm)==NO)
        obm = ""
    ;
    if (imaccess(cat)==NO)
        cat = ""
    ;

    # Merge the keywords.
    touch ("swchdr.tmp")
    if (access(swcnames.wcshdr)) {
        concat (swcnames.wcshdr, "swchdr.tmp", append+)
	delete (swcnames.wcshdr)
    }
    ;
    if (access(swcnames.dqhdr)) {
        concat (swcnames.dqhdr, "swchdr.tmp", append+)
	delete (swcnames.dqhdr)
    }
    ;

    flpr
    # Enter the information in the images and catalog.
    dqsifhdr (image, gclimage, bpm, obm, cat, "swchdr.tmp",
        swcnames.wcsdb, lfile)

    # Clean up.
    delete ("swchdr.tmp")

}
list = ""

# Copy the CBPM from this pipeline to the CBPM filename, as 
# retrieved by getcal in swcsetup.  Also, revise the CBPM
# keyword in each image to be consistent with the value 
# retrieved by getcal.
list = "swcsetup-cbpms.list"
if ( access( "swcsetup-cbpms.list" ) ) {
    while ( fscan( list, s1, s2, s3 ) != EOF ) {
        # Extract the file name from the full path
        hedit( s1, "CBPM", substr(s3,strldx("/",s3)+1,999),
	    add+, update+, ver- )
        imdelete( s3 )
        imcopy( s2, s3 )
    }
    list = ""
    delete( "swcsetup-cbpms.list", verify- )
}
;

logout 1
