# SWCNAMES -- Directory and filenames for the SWC pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure swcnames (name)

string  name                    {prompt = "Name"}

string  dataset                 {prompt = "Dataset name"}
string  pipe                    {prompt = "Pipeline name"}
string  shortname               {prompt = "Short name"}
file    rootdir                 {prompt = "Root directory"}
file    indir                   {prompt = "Input directory"}
file    datadir                 {prompt = "Dataset directory"}
file    uparm                   {prompt = "Uparm directory"}
file    pipedata                {prompt = "Pipeline data directory"}
file    parent                  {prompt = "Parent part of dataset"}
file    child                   {prompt = "Child part of dataset"}
file    lfile                   {prompt = "Log file"}
file    image                   {prompt = "Primary image name"}
file    wcshdr                  {prompt = "WCS header return file name"}
file    dqhdr                   {prompt = "DQ header return file name"}
file	sky			{prompt = "Sky map"}
file	sig			{prompt = "Sigma map"}
file	obm			{prompt = "Object mask"}
file	cat			{prompt = "Catalog"}
file    mcat                    {prompt = "USNO matched catalog"}
file    wcsdb                   {prompt = "WCS database"}
file    cbpm                    {prompt = "Cumulative bad pixel mask"}
string  pattern = ""            {prompt = "Pattern"}
bool    base = no               {prompt = "Child part of dataset"}

begin
        # Set generic names.
        names ("swc", name, pattern=pattern, base=base)
        dataset = names.dataset
        pipe = names.pipe
        shortname = names.shortname
        rootdir = names.rootdir
        indir = names.indir
        datadir = names.datadir
        uparm = names.uparm
        pipedata = names.pipedata
        parent = names.parent
        child = names.child
        lfile = names.lfile

        # Set pipeline specific names.
        image = shortname
        wcshdr = shortname // "_wcshdr"
        dqhdr = shortname // "_dqhdr"
	sky = shortname // "_sky"
	sig = shortname // "_sig"
	obm = shortname // "_obm"
	cat = shortname // "_cat"
        mcat = shortname // "_mcat"
        wcsdb = shortname // ".wcsdb"
	cbpm = shortname // "_cbpm"//".pl"

        if (pattern != "") {
            image += ".fits"
        }
end
