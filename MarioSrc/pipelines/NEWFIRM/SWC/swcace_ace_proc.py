#!/bin/env python

"""
Capture data quality information from the output of ace and create
a .cl script to update the dataset and PMAS with.
"""

import re
import sys

fitsfile = sys.argv[1] + '.fits'
catfile = sys.argv[2]

infile = file( catfile, 'r')
outfile = open( 'swcace_ace_proc.cl', 'w' )

print >> outfile, "hsel( \"",fitsfile,"\", \"mjd-obs\", yes ) | scan( mjd )"
print >> outfile, "printf(\"%f %s\", mjd, dm)"

for line in infile.readlines():
    thisline = line.strip()
    m = re.match( '^#', thisline )
    if m:
        m2 = re.match( '^#k FWHM\s+\= (.*)', thisline )
        if m2:
            print >> outfile, "setkeyval( class=\"objectimage\", id=image, dm=dm,"
            print >> outfile, "    keyword=\"dqseamp\", value=\"%s\" )" % (m2.group(1))
            print >> outfile, "hedit( \"",fitsfile,"\", \"seeing1\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m2.group(1))
            print >> outfile, "hedit( \"swcaceimg\", \"seeing1\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m2.group(1))

infile.close()
outfile.close()
