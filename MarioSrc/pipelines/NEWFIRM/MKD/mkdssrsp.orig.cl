#!/bin/env pipecl
# 
# MKDSSRSP -- Make resampled data products.
#
# Description:
#
#   This module combines the four sky-subtracted resampled SIFs into a
#   single composite image.  It also constructs the DQ mask, and PNGs.
#
# Exit Status Values:
#
#   0 = Unsuccessful, input lists not found
#   1 = Successful

# Declare variables.
string  module, dataset, datadir, lfile, lst, lstbpm, nhscript
string	imOUT, bpmOUT, pngOUT, obstype, proctype

# Define tasks and packages.
dppkg
servers

# Set names and directories.
module = envget ("NHPPS_MODULE_NAME")
mkdnames (envget("OSF_DATASET"))
dataset = mkdnames.dataset
datadir = mkdnames.datadir
lfile = mkdnames.lfile
set (srcdir = "NHPPS_PIPESRC$/NEWFIRM/MKD/pipedata/")
set (uparm = mkdnames.uparm)
set (pipedata = mkdnames.pipedata)
cd (datadir)

# Log start of module.
printf ("\n%s (%s): ", strupr(module), dataset) | tee (lfile)
time | tee (lfile)

# Set input list filenames and check that they are present.  Normally,
# when the pipeline is run in production, products will be constructed
# from second-pass files.  To distinguish these products from those that
# happen to be made from first-pass files, the "_r" is replaced with "_r1"
# in the output filenames.

lst      = "mkdsetup-ssrsp.list"
lstbpm   = "mkdsetup-ssrspBPM.list"
if (mkd_ssrsp) {
    imOUT  = mkdnames.rsp
    bpmOUT = mkdnames.rspbpm
} else {
    imOUT  = ""
    bpmOUT = ""
}
pngOUT   = mkdnames.rsp
obstype  = "object"
proctype = "Resampled"
nhscript = "srcdir$" // module // ".nhedit"

# Look for first pass data if necessary.
if ((access(lst) && access(lstbpm)) == NO) {
    lst    = "mkdsetup-ss1rsp.list"
    lstbpm = "mkdsetup-ss1rspBPM.list"
    if (mkd_ssrsp) {
	imOUT  = substr(imOUT,1,strlstr("_r",imOUT)+1)//"1"//
	    substr(imOUT,strlstr("_r",imOUT)+2,999)
	bpmOUT = substr(bpmOUT,1,strlstr("_r",bpmOUT)+1)//"1"//
	    substr(bpmOUT,strlstr("_r",bpmOUT)+2,999)
    } else {
	imOUT  = ""
	bpmOUT = ""
    }
    pngOUT = substr(pngOUT,1,strlstr("_r",pngOUT)+1)//"1"//
        substr(pngOUT,strlstr("_r",pngOUT)+2,999)
}
;  

# Check files.
if ((access(lst) && access(lstbpm))==NO) {
#    sendmsg("ERROR","Input lists not found.","","VRFY")
    print "Input list not found"
    logout 1
}
;
count (lst) | scan (i)
if (i == 0)
    logout 1
;

# We can skip making archive data product if imOUT is not specified and still
# make PNGs for viewing.

if (imOUT == "") {
    concat (lst) | scan (s1)
    dppng ("@"//lst, s1, pngOUT, png_blk, png_pix, png_pixm, module//"-png",
        zscale="'i1!=0'")
    delete (lst)
    delete (lstbpm)
    logout 1
}
;

# Image data -------------------------------------------------------------------

# Make the global header from the intersection of common keywords.
mkglbhdr ("@"//lst, module//".fits", exclude="@srcdir$mkglbhdr.exclude")

# Make a version of the first SIF using the global header so that when
# we combine this becomes the header for the composite image.

head (lst) | scan (s1)
tail (lst, nl=-1, >> module//".tmp")
imcopy (s1, module//".fits[global,append,inherit]", verbose-)
imcombine (module//".fits[global],@"//module//".tmp", imOUT, headers="",
    bpmasks="", rejmasks="", nrejmasks="", expmasks="", sigmas="",
    logfile=lfile, combine="average", reject="none", project=no,
    outtype="real", outlimits="", offsets="wcs", masktype="none",
    blank=0., scale="none", zero="none", weight="none", statsec="",
    expname="oexptime", rdnoise="0.", gain="1.", snoise="0.", grow=0.)
imdelete (module//".fits")
delete (module//".tmp")

# Update the header.
nhedit (imOUT, "BPM", bpmOUT//"[pl]", ".", add+)
dphdr (imOUT, proctype, obstype, "")
nhedit (imOUT, comfile=nhscript); flpr
dprwcs (imOUT)
hfix (imOUT, command = "concat $fname | unique > $fname")

# Data quality mask ------------------------------------------------------------

imcombine ("@"//lstbpm, module//".pl", headers="", bpmasks="", rejmasks="",
    nrejmasks="", expmasks="", sigmas="", logfile=lfile, combine="average",
    reject="none", project=no, outtype="real", outlimits="", offsets="wcs",
    masktype="none", blank=2., scale="none", zero="none", weight="none",
    statsec="", expname="oexptime", rdnoise="0.", gain="1.", snoise="0.",
    grow=0.)
dpdqmask (imOUT, module//".pl", bpmOUT, module//"-bpm", mosaic-)
imdelete (module//".pl")

# Graphics file ----------------------------------------------------------------

dppng (imOUT, imOUT, pngOUT, png_blk, png_pix, png_pixm,
    module//"-png", zscale="'i1!=0'")

# Set the data products list.
if (access(imOUT//".fits"))
    print (imOUT//".fits", > module//".dps")
;
if (access(bpmOUT//".fits"))
    print (bpmOUT//".fits", >> module//".dps")
;
if (access(pngOUT//"_png.fits"))
    print (pngOUT//"_png.fits", >> module//".dps")
;

delete (lst)
delete (lstbpm)

logout 1
