#!/bin/env pipecl
# 
# MKDMEF -- Make MEF data products.
#
# Description:
#
#   This module combines SIFs (raw, pre, and post sky-subtracted) into
#   final data products.  It constructs MEF images, DQ masks, and PNGs.
#   This is a generic routine called by different modules.  Because it
#   may be called by different mdules in parallel be sure no filenames,
#   such as temporary working files, are the same.

# Declare variables.
real	quality
string  module, dataset, datadir, lfile, lst, lstbpm, nhscript
string	imOUT, bpmOUT, pngOUT, obstype, proctype, extn

# Define tasks and packages.
dppkg
servers

# Set names and directories.
module = envget ("NHPPS_MODULE_NAME")
mkdnames (envget("OSF_DATASET"))
dataset = mkdnames.dataset
datadir = mkdnames.datadir
lfile = mkdnames.lfile
set (srcdir = "NHPPS_PIPESRC$/NEWFIRM/MKD/pipedata/")
set (uparm = mkdnames.uparm)
set (pipedata = mkdnames.pipedata)
cd (datadir)

# Log start of module.
printf ("\n%s (%s): ", strupr(module), dataset) | tee (lfile)
time | tee (lfile)

# Set filenames.
if ( module == "mkdraw" ) {
    lst      = "mkdsetup-raw.list"
    # In rare cases, image data consists of only zeros, or there may
    # be incomplete or corrupt headers in the raw data. In these cases,
    # the ogcsetup.cl stage set the keyword NOPNG. Check for its 
    # presence, and skip making PNGs if needed.
    list = lst
    s2 = ""
    while ( fscan( list, s1 ) != EOF ) {
	hselect( s1, "NOPNG", yes ) | scan( s2 )
    }
    if ( s2 != "" ) {
	sendmsg( "WARNING", "No PNG created for possibly corrupt raw data", 
	    dataset, "PROC" )
	# An empty list will cause normal logout below
	delete( lst )
	touch( lst )
    }
    ;
    list = ""
    lstbpm   = "mkdsetup-rawBPM.list"
    imOUT    = ""
    bpmOUT   = ""
    pngOUT   = mkdnames.rawpng
    obstype  = "object"
    proctype = "Raw"
    nhscript = "srcdir$" // module // "-" // obstype // ".nhedit"
} else if ( module == "mkdpress" ) {
    lst      = "mkdsetup-press.list"
    lstbpm   = "mkdsetup-pressBPM.list"
    if ( mkd_press || mkdnames.calledby != "object" ) {
	imOUT  = mkdnames.press
	bpmOUT = mkdnames.pressbpm
    } else {
	imOUT  = ""
	bpmOUT = ""
    }
    pngOUT   = mkdnames.presspng
    obstype  = mkdnames.calledby
    # The output file names for dark and flat are not created
    # correctly. This might be fixed by changing names.cl (or mkdnames.cl),
    # but the underlying cause may be the naming convention chosen for the
    # mkd pipeline and the convention for naming the child. Here,we 
    # simply make an ad-hoc change to the file name so the rest of
    # the code can remain unchanged. Note the suffix _press as given by
    # mkdnames.press is both incorrect (not needed for dark and flat
    # because there is no sky subtraction) and unnecessary (mkdmef.cl
    # is only called once for darks and flats).
print "A1"
print( pngOUT )
    if ( mkdnames.calledby == "dark" ) {
	imOUT = substr( imOUT, 1, strldx("-",imOUT)-1 )
 	pngOUT = imOUT
	bpmOUT = imOUT//"_bpm"
    }
    ;
    if (mkdnames.calledby == "flat") {
        obstype = "dome flat"
	imOUT = substr( imOUT, 1, strldx("-",imOUT)-1 )
 	pngOUT = imOUT
	bpmOUT = imOUT//"_bpm"
    }
    ;
print "A2"
print( pngOUT )
    if (obstype == "object")
	proctype = "NoSS"
    else
        proctype = "MasterCal"
    nhscript = "srcdir$" // module // "-" // obstype // ".nhedit"
} else {
    lst      = "mkdsetup-ss.list"
    lstbpm   = "mkdsetup-ssBPM.list"
    if ( mkd_ss ) {
	imOUT  = mkdnames.ss
	bpmOUT = mkdnames.bpm
    } else {
	imOUT  = ""
	bpmOUT = ""
    }
    pngOUT   = mkdnames.sspng
    obstype  = "object"
    proctype = "InstCal"
    nhscript = "srcdir$" // module // "-" // obstype // ".nhedit"
}

# Check files.
if ( (access(lst)&&access(lstbpm)) == NO ) {
#    sendmsg ("ERROR", "Input lists not found.", "", "VRFY")
    print "Input list not found"
    logout 1
}
;  
count( lst ) | scan( i )
if ( i == 0 )
    logout 1
;

# We can skip making archive data products if imOUT is not specified and still
# make PNGs for viewing.

if ( imOUT == "" ) {
    concat( lst ) | scan( s1 )
    dppng( "@"//lst, s1, pngOUT, png_blk, png_pix, png_pixm, module//"-png" )
    delete( lst )
    delete( lstbpm )
    logout 1
}
;

# Make global header from the intersection of common keywords.
mkglbhdr( "@"//lst, imOUT, exclude = "@srcdir$mkglbhdr.exclude" )

# Define standard keywords and remove blocks of blank lines.
dphdr( imOUT, proctype, obstype, "" )
nhedit( imOUT, comfile = nhscript, ver- ); flpr
hfix( imOUT, command = "concat $fname | unique > $fname" )

# Append the extensions.
list = lst; quality = INDEF
for (j = 0; fscan(list,s1) != EOF; j += 1) {
print "B1"
    if (imaccess(s1) != YES) {
	j -= 1
	next
    }
    ;

    # Set the extension name.
    if (obstype == "object")
	extn = substr(s1, strlstr("_im", s1)+1, strlstr("_im", s1)+3)
    else
	extn = substr(s1, strlstr("-im", s1)+1, strlstr("-im", s1)+3)

    # Make a simple image because header manipulation works better.
    printf ("%s_%s\n", imOUT, extn) | scan (s2)
    imcopy (s1, s2, verbose-)
print "B2"

    # Set standard keywords, remove blocks of blank lines, and
    # accumulate information for global header..
    hselect (s2, "$quality", yes) | scan (x)
    if (isindef(x)==NO) {
	if (isindef(quality))
	    quality = x
	else
	    quality = min (quality, x)
    }
    ;
print "B3"
    nhedit (s2, "BPM", bpmOUT//"-"//extn//".pl", ".", add+, ver-)
    dphdr (s2, proctype, obstype, extn)
    nhedit (s2, comfile = nhscript, ver-); flpr
    dpwcs (s2, obstype, module//"_allwcs.tmp", mosaic+)
    hfix (s2, command = "concat $fname | unique > $fname")

    # Append extension.
print "B4"
print "-----"
    printf ("%s[%s,append,inherit]\n", imOUT, extn) | scan (s3)
print(s2)
print(s3)
print "-----"
    imcopy (s2, s3, verbose-)
    imdelete (s2)
}
list = ""

# Set global values derived from extensions.
nhedit (imOUT//"[0]", "NEXTEND", j, "Number of extensions", ver-)
if (isindef(quality)==NO)
    nhedit (imOUT//"[0]", "QUALITY", quality, "PL quality indicator", add+, ver-)
;
if (obstype == "object")
    dpgwcs (imOUT, module//"_allwcs.tmp")
else
    ;
delete (module//"_allwcs.tmp")

# Data quality mask --------------------------------------------------------

dpdqmask (imOUT, "@"//lstbpm, bpmOUT, module//"-bpm", mosaic+)

# Graphic files ----------------------------------------------------------------

fields ("pipedata$offsets11.dat", "2-3", > module//"-offsets.tmp")
print "A3"
dppng ("@"//lst, imOUT//"[1]", pngOUT, png_blk, png_pix, png_pixm, 
    module//"-png", offsets=module//"-offsets.tmp")
delete (module//"-offsets.tmp")

# Set the data products files.
if (access(imOUT//".fits"))
    print (imOUT//".fits", > module//".dps")
;
if (access(bpmOUT//".fits"))
    print (bpmOUT//".fits", >> module//".dps")
;
if (access(pngOUT//"_png.fits"))
    print (pngOUT//"_png.fits", >> module//".dps")
;

delete (lst)
delete (lstbpm)

logout 1
