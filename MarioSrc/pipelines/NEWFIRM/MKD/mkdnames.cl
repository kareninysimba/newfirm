# MKDNAMES -- Directory and filenames for the MKD pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.
#
#
# History
#
#   T. Huard 20090313   Included "calledby".  Ensured that formatting conforms
#                       to convention.
#   T. Huard 20090325   Added names for pre-sky-subtracted (Pre-SS)
#                       non-resampled MEFs.  Reorganized code somewhat.
#   Last Revised:   T. Huard    20090326    7:45AM

procedure mkdnames (name)

string  name            {prompt = "Name"}

string  dataset         {prompt = "Dataset name"}
string  pipe            {prompt = "Pipeline name"}
string  shortname       {prompt = "Short name"}
string  calledby        {prompt = "Calling pipeline name"}
file    rootdir         {prompt = "Root directory"}
file    indir           {prompt = "Input directory"}
file    datadir         {prompt = "Dataset directory"}
file    uparm           {prompt = "Uparm directory"}
file    pipedata        {prompt = "Pipeline data directory"}
file    parent          {prompt = "Parent part of dataset"}
file    child           {prompt = "Child part of dataset"}
file    lfile           {prompt = "Log file"}
file	raw		{prompt = "MEF raw name"}
file	rawbpm		{prompt = "MEF raw BPM name"}
file    rawpng          {prompt = "MEF png FITS file"}
file    ss              {prompt = "MEF name (sky-sub non-resamp)"}
file    bpm             {prompt = "MEF BPM name"}
file    sspng           {prompt = "MEF png FITS file"}
file    press           {prompt = "Pre-SS MEF name"}
file    pressbpm        {prompt = "Pre-SS MEF BPM name"}
file    presspng        {prompt = "Pre-SS MEF png FITS file"}
file    rsp             {prompt = "Resampled image"}
file    rspbpm          {prompt = "BPM for resampled image"}
file    rsppng          {prompt = "Resampled png FITS file"}
file    stk             {prompt = "Stacked image"}
file    stkpng          {prompt = "Stacked png FITS file"}
bool    base = no       {prompt = "Child part of dataset"}
string  pattern = ""    {prompt = "Pattern"}

begin
    file tmp

    # Set generic names.
    names ("mkd", name, pattern=pattern, base=base)
    dataset = names.dataset
    pipe = names.pipe
    shortname = names.shortname
    rootdir = names.rootdir
    indir = names.indir
    datadir = names.datadir
    uparm = names.uparm
    pipedata = names.pipedata
    parent = names.parent
    child = names.child
    lfile = names.lfile

    # Set pipeline specific names.
    
    # Determine which pipeline called the NDP/MKD pipeline.  This is done by
    # checking the dataset.  If dataset includes "-drk" or "-flt", then it is
    # a DARK or FLAT dataset and called by DRK and FLT, respectively.
    # Otherwise, the dataset represents an OBJECT, and presumably NDP/MKD was
    # called by GOS.
#    cd(datadir)
#    tmp = "mkdnames_" // envget ("NHPPS_MODULE_NAME") // ".tmp"
    calledby = ""
#    print(dataset,>tmp)
#    s1="" ; match("-drk",tmp) | scan(s1)
    s1 = "" ; print( dataset ) | match( "-drk", "STDIN" ) | scan( s1 )
    if (strlen(s1)>0) {
        calledby="dark"
    } else {
        s1 = "" ; print( dataset ) | match( "-flt", "STDIN" ) | scan( s1 )
        if (strlen(s1)>0) {
            calledby="flat"
        } else {
            calledby="object"
        }
    }
#    delete(tmp,verify-)
       
    # Raw MEF products
    raw = shortname//"_raw"
    rawbpm = raw//"_bpm"
    rawpng = raw
       
    # Sky-subtracted non-resampled MEF products
    ss = shortname // "_ss"
    bpm = ss // "_bpm"
    sspng = ss

    # Pre-sky-subtracted non-resampled MEF products    
    press=shortname//"_press"
    pressbpm=press//"_bpm"
    presspng=press
    
    # Sky-subtracted resampled composite image products   
    rsp = shortname // "_r"
    rspbpm = rsp // "_bpm"
    rsppng = rsp

    # Stack products
    stk = shortname // "_a"
    stkpng = stk

    if (pattern != "") {
        ss += ".fits"
        bpm += ".fits"
        press += ".fits"
        pressbpm += ".fits"
        rsp += ".fits"
        rspbpm += ".fits"
        stk += ".fits"
    }
end
