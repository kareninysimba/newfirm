#!/bin/env pipecl
# 
# MKDSTK -- Make stacked files.
#
# Currently, everything is done in STK/SPK and the only thing here is to
# make the file for mkd2stb.

string  dataset, datadir, ifile, lfile

# Load tasks and packages

# Set names and directories.
mkdnames (envget ("OSF_DATASET"))
dataset = mkdnames.dataset
datadir = mkdnames.datadir
ifile = mkdnames.indir // dataset // ".mkd"
lfile = datadir // mkdnames.lfile
set (uparm = mkdnames.uparm)
cd (datadir)

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Collect the pieces.
if (mkd_stk) {
    if (sciencepipeline) {
	match ("_spk.fits", ifile, > "mkdstk.dps")
	match ("_spk_bpm.fits", ifile, >> "mkdstk.dps")
	match ("_spk_expmap.fits", ifile, >> "mkdstk.dps")
	# match ("_spk_png.fits", ifile, >> "mkdstk.dps")
	# match ("_stk_png.fits", ifile, >> dataset//".list")
	match ("_spk_png1.fits", ifile, >> "mkdstk.dps")
	match ("_spk_png2.fits", ifile, >> "mkdstk.dps")
	match ("_stk_png1.fits", ifile, >> dataset//".list")
	match ("_stk_png2.fits", ifile, >> dataset//".list")
	concat ("mkdstk.dps", dataset//".list", append+)
    } else {
	match ("_stk.fits", ifile, > "mkdstk.dps")
	match ("_stk_bpm.fits", ifile, >> "mkdstk.dps")
	match ("_stk_expmap.fits", ifile, >> "mkdstk.dps")
	# match ("_stk_png.fits", ifile, >> "mkdstk.dps")
	match ("_stk_png1.fits", ifile, >> "mkdstk.dps")
	match ("_stk_png2.fits", ifile, >> "mkdstk.dps")
	concat ("mkdstk.dps", dataset//".list", append+)
    }
} else {
    if (sciencepipeline) {
	# match ("_spk_png.fits", ifile, >> dataset//".list")
	# match ("_stk_png.fits", ifile, >> dataset//".list")
	match ("_spk_png1.fits", ifile, >> dataset//".list")
	match ("_stk_png2.fits", ifile, >> dataset//".list")
	match ("_spk_png1.fits", ifile, >> dataset//".list")
	match ("_stk_png2.fits", ifile, >> dataset//".list")
    } else
	# match ("_stk_png.fits", ifile, >> dataset//".list")
	match ("_stk_png1.fits", ifile, >> dataset//".list")
	match ("_stk_png2.fits", ifile, >> dataset//".list")
}

logout 1
