#!/bin/env pipecl
#
# MKDSTK -- Make stack data products.
#
# This routine can be used for first and second pass stacks based on the
# NHPPS_SUBPIPE_NAME.  We have to keep in mind that the stacks are
# used in subsequent processing so there may still be a final cleanup later.

# Declare variables.
string  subpipe, module, dataset, datadir, lfile
string  imOUT, bpmOUT, expOUT, pngOUT, obstype, proctype, nhscript

# Define packages and tasks.
dppkg
servers

# Set names and directories.
subpipe = envget ("NHPPS_SUBPIPE_NAME")
module = envget ("NHPPS_MODULE_NAME")
names (subpipe, envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
set (srcdir = "NHPPS_PIPESRC$/NEWFIRM/MKD/pipedata/")
set (uparm = names.uparm)
set (pipedata=names.pipedata)
cd (datadir)

# Log the operation.
printf ("\n%s (%s): ", strupr(module), dataset) | tee (lfile)
time | tee (lfile)

# Set input and output files and types.
imOUT    = names.shortname // "_" // subpipe
bpmOUT   = imOUT // "_bpm"
expOUT   = imOUT // "_expmap"
pngOUT   = imOUT
obstype  = "object"
proctype = "Stacked"
nhscript = "srcdir$mkdstk.nhedit"

# Check for primary data product.
if (imaccess(imOUT) == NO)
    logout 0
;

# Primary science image --------------------------------------------------------

dphdr  (imOUT, proctype, obstype, "")
dprwcs (imOUT)
nhedit (imOUT, "DQMASK", bpmOUT//"[pl]", "Data quality mask", add+)
nhedit (imOUT, "EXPMAP", expOUT//"[pl]", "Exposure mask", add+)
nhedit (imOUT, comfile=nhscript); flpr
hfix (imOUT, command = "concat $fname | unique > $fname")

# Data quality mask ------------------------------------------------------------

dpdqmask (imOUT, bpmOUT, bpmOUT, "mkdstk-bpm", mosaic-)

# Exposure map -----------------------------------------------------------------

dpexpmap (imOUT, expOUT, expOUT, "mkdstk-expmap", mosaic-)
delete (expOUT//".pl")

# Graphic files ----------------------------------------------------------------

dppng (imOUT, imOUT, pngOUT, png_blk, png_pix, png_pixm,
    "stkmkd-png", zscale="'i1!=0'")

logout 1
