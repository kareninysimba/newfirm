# SPINAMES -- Directory and filenames for the SPI pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure spinames (name)

string  name                    {prompt = "Name"}

string  dataset                 {prompt = "Dataset name"}
string  pipe                    {prompt = "Pipeline name"}
string  shortname               {prompt = "Short name"}
file    rootdir                 {prompt = "Root directory"}
file    indir                   {prompt = "Input directory"}
file    datadir                 {prompt = "Dataset directory"}
file    uparm                   {prompt = "Uparm directory"}
file    pipedata                {prompt = "Pipeline data directory"}
file    parent                  {prompt = "Parent part of dataset"}
file    child                   {prompt = "Child part of dataset"}
file    lfile                   {prompt = "Log file"}
file    image                   {prompt = "Primary image name"}
file    sky                     {prompt = "Sky map"}
file    sig                     {prompt = "Sigma map"}
file    obm                     {prompt = "Object mask"}
file    mask                    {prompt = "Object mask from stack"}
file    cat                     {prompt = "Catalog"}
file    croppedim               {prompt = "Cropped stack"}
file    croppedbpm              {prompt = "Cropped stack mask"}
file    resampled               {prompt = "Resampled image"}
file    presampled              {prompt = "Pre-resampled image"}
file    premasked               {prompt = "Pre-resampled image"}
string  pattern = ""            {prompt = "Pattern"}
bool    base = no               {prompt = "Child part of dataset"}

begin
        # Set generic names.
        names ("spi", name, pattern=pattern, base=base)
        dataset = names.dataset
        pipe = names.pipe
        shortname = names.shortname
        rootdir = names.rootdir
        indir = names.indir
        datadir = names.datadir
        uparm = names.uparm
        pipedata = names.pipedata
        parent = names.parent
        child = names.child
        lfile = names.lfile

        # Set pipeline specific names.
        image = shortname
        sky = shortname // "_sky"
        sig = shortname // "_sig"
        obm = shortname // "_obm"
        mask = substr( shortname, 1, strlstr("_ss2",shortname)-1 ) // "_mask"
        cat = shortname // "_cat"
	croppedim = shortname//"_crp"
	croppedbpm = shortname//"_crp_bpm.pl"
        resampled = shortname // "_rsp"
        presampled = shortname // "_psp"
        premasked = shortname // "_psp_m"

        if (pattern != "") {
            image += ".fits"
        }

end
