#!/bin/env pipecl
#
# spidestripe
#

# Declare variables
int     status = 1
string  dataset, datadir, lfile, image, fpstack, flat

# Load packages
images
proto
noao
nproto
imfit
dataqual

# Set file and path names
spinames( envget( "OSF_DATASET" ) )
dataset = spinames.dataset
datadir = spinames.datadir
lfile = spinames.lfile
set ( uparm = spinames.uparm )
set ( pipedata = spinames.pipedata )

print( datadir )
cd( datadir )

# Log start of processing.
printf( "\nSTRCROP (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
concat( "fpstack_p" ) | scan( fpstack )
spinames( image )

if ( stripe_removal == "smart" ) {
    s1 = ""; hselect( image, "RESBCKMT", yes ) | scan( s1 )
    if ( ( s1 == "ace" ) || ( s1 == "poly5" ) ) {
	stripe_removal = "yes"
    } else {
	stripe_removal = "no"
    }
}
;

if ( stripe_removal == "yes" ) {

    imcopy( image, image//"_destripe" )

    # Determine direction of fitting
    hselect( image, "IMAGEID", yes ) | scan( i )
    if (i==3)
        i = 2
    ;
    if (i==4)
        i = 1
    ;

    removestripes( image, mask=spinames.mask, axis=i,
	skymode=0, niter=3, low=3, high=3, pclip=0.5 )

    # Add keyword to the header indicating stripe removal was done
    hedit( image, "REMSTRIP", "yes", add+, ver- )


}
;

logout( status )
