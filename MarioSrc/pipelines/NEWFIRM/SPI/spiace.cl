#!/bin/env pipecl

int	status = 1
int     namps, ir, ix, iy, rad, continue
int	deforder, fourqorder, offorder, order
real    mjd, x1, x2, skymode
string  datadir, dataset, image, indir, lfile, rtnfile, rfile
string	sig, sky, obm, cat, nocmpat, nocdpat, fsobject, shortname
string  wcs, cast, defmeth, fourqmeth, offmeth, method
struct  *list2

# Load packages.
servers
utilities
proto
tables
ttools
images
noao
nproto
nfextern
ace
msctools
dataqual
task parseresbck = "NHPPS_PIPESRC$/NEWFIRM/SKY/parseresbck.cl"

# Set file and path names.
spinames (envget("OSF_DATASET"))
dataset = spinames.dataset

# Set filenames.
indir = spinames.indir
datadir = spinames.datadir
lfile = datadir // spinames.lfile
set (uparm = spinames.uparm)
set (pipedata = spinames.pipedata)

# Log start of processing.
printf( "\nSPIACE (%s): ", dataset ) | tee (lfile)
time | tee( lfile )

cd( datadir )

# Retrieve the names of the image and the stack being processed
# from the files containing their locations
concat( "image_p" ) | scan( image )
spinames( image )

sky   = spinames.sky
sig   = spinames.sig
obm   = spinames.obm
cat   = spinames.cat
imcopy( image, spinames.image )

delete (sky//"*")
delete (sig//"*")
delete (obm//"*")
delete (cat//"*")
delete ("spiaceimg.fits*,objmask.fits*")

# If the sky exposures of an object/sky sequence are being processed,
# use the residual background subtraction method set in sps_resoff
# for the offset exposures.
hselect( image, "OBSTYPE", yes ) | scan( s1 )
if ( ( s1 == "sky" ) || ( s1 == "SKY" ) ) {
    print "Not using pedestal for the sky expousres"
    s2 = sps_resoff
} else {
    s2 = sps_resbck
}

# Parse the contents of sps_resbck/sps_resoff
parseresbck( s2 ) |
    scan( defmeth, deforder, fourqmeth, fourqorder, offmeth, offorder )
print( s2 )
printf( "%s %d %s %d %s %d\n", defmeth, deforder, fourqmeth, fourqorder, offmeth, offorder )

imacedq (spinames.image, "!CBPM", "spiaceimg", sky, sig, obm, cat, lfile,
    "", "spiace", convolve="", hsigma=4, minpix=6, ngrow=2, agrow=2.,
    catdefs="pipedata$swcace.def", magzero="!MAGZERO", verbose=2, mosaic+,
    cafwhm=3.)
if (imacedq.status != 1) {
    # Delete the local copy so that it will not be passed on
    # to downstream pipelines if ACE failed
    # TODO downstream such that deletion here is not needed
#    imdel( image )
    logout 0
}
;

# Analyze results and write to header.
s3 = mktemp ("spiace.")
if (imaccess(sky)) {

    # Set the default residual background removal method
    method = defmeth
    order = deforder

    # In four-shooter mode (or 4Q or Q mode) ACE's sky image will be
    # contaminated with emission from the extended source. To get around
    # this, sky is only considered in the area outside a circle with a
    # radius given by sky_4qradius (a percentage of half the diagonal).
    nocmpat = "" ; hsel( image, "NOCMPAT", yes ) | scan( nocmpat )
    nocdpat = "" ; hsel( image, "NOCDPAT", yes ) | scan( nocdpat )
    fsobject = "" ; hsel( image, "FSOBJECT", yes ) | scan( fsobject )
    if ( ( (nocmpat=="4Q") || (nocdpat=="4Q") || (nocdpat=="Q") || ( nocmpat=="Q") ) && (fsobject=="yes") ) {
	method = fourqmeth
	order = fourqorder
	if ( fourqmeth == "4Q" ) {
	    # Get the size of the sky image and determine the radius of the
	    # circle in pixels.
	    hsel( sky, "NAXIS1,NAXIS2", yes ) | scan( ix, iy )
	    ir = ix
	    if ( iy < ir )
	        ir = iy
	    ;
	    # Divide by 2 (diameter to radius) and 100 (percent to fraction)
	    rad = ir*sky_4qradius/200
	    ix = ix/2
	    iy = iy/2
	    printf( "%d %d %d\n", ix, iy, rad ) | scan( line )
	    print( line )
	    # Fit linear surface to the region outside that circle
	    imsurfit( sky, "fitsky", xorder=2, yorder=2, cross_terms+,
	        function="legendre", type_output="fit",
	        regions="invcircle", circle=line )
	    # Subtract it from the original image.
	    imdel ("spiaceimg")
	    printf( "Run aceall for sky subtraction\n", >> lfile )
	    aceall (image, skyotype="subsky", skyimages="spiaceimg",
	        skies="fitsky", masks="", exps="", gains="", scales="",
	        extnames="", logfiles=lfile, verbose=2, rimages="",
	        rmasks="", rexps="", rgains="", rscales="", roffset="",
	        rcatalogs="", catalogs="", catdefs="", catfilter="",
	        order="", nmaxrec=INDEF, magzero="INDEF", gwtsig=INDEF,
	        gwtnsig=INDEF, objmasks="+_obm", omtype="all",
	        sigimages="", sigmas="", rskies="", rsigmas="",
	        fitstep=100, fitblk1d=10, fithclip=2., fitlclip=3.,
	        fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
	        blksize=-10, blknsubblks=2, hdetect=yes, ldetect=no,
	        updatesky=yes, bpdetect="1-100", bpflag="1-100",
	        convolve="bilinear 3 3", hsigma=3., lsigma=10.,
	        neighbors="8", minpix=6, sigavg=4., sigmax=4.,
	        bpval=INDEF, rfrac=0.5, splitstep=0.4, splitmax=INDEF,
	        splitthresh=5., sminpix=8, ssigavg=10., ssigmax=5.,
	        ngrow=2, agrow=2., cafwhm=3.)
	    imdel( "fitsky" )
	}
	;
    }
    ;

    # Test whether offset sky exposures were used to subtract the sky.
    line = ""; hselect( image, "SSUBINFO", yes ) | scan( line )
    if ( strstr("ffset",line) > 0 ) {
	method = offmeth
	order = offorder
    }

    printf( "Residual sky removal: %s %d\n", method, order )
    # Remove the residual background
    if ( ( method == "pedestal" ) || ( method == "none" ) ) {
	imdel( "spiaceimg" )
	# Use object mask derived from first-pass stack to mask
	# out objects, then determine mode of the remainder, and
	# subtract that. The mode is used to help avoid a skew
	# towards higher values because of the extended emission.
	mimstat( image, fields="mode", nclip=3, lsigma=2, usigma=3,
	    format-, imasks="!STKBPM" ) | scan( skymode )
	imarith( image, "-", skymode, image )
    } else if ( method == "poly" ) {
	# Subtract low-order polynomial
	iferr {
            mscskysub( image, image//"_bckfit", type_output="fit",
                xorder=order, yorder=order, function="leg",
		upper=5, lower=5,
	        cross_terms+, regions="mask", mask="!STKBPM" )
	} then {
	    # mscskysub failed, use ace background instead
	    sendmsg( "WARNING", "mscskysub failed", image, "PROC" )
	    imdel( image ); imrename( "spiaceimg", image )
	} else {
	    # Subtract the fit. This is done outside of mscskysub because
	    # mscskysub modifies the fit so that the average background is not
	    # changed. But here the goal is to change the background to zero.
            imarith( image, "-", image//"_bckfit",  image//"_bcksub" )
            imdel( image ); imrename( image//"_bcksub", image )
	    imdel( image//"_bckfit" )
	}
    } else {
        # Replace image with sky subtracted version calculated above.
	# This is done when spsresbck is "4Q" or "ace". "ace" is the
	# default behavior
        imdel( image ); imrename( "spiaceimg", image )
    }
    if ( method == "poly" ) {
	method = method // str(order)
    }
    ;
    nhedit( image, "RESBCKMT", method, "Residual sky subtraction method",
	add+, ver- )

}
;

logout 1
