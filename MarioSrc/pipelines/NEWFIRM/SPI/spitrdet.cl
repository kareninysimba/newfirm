#!/bin/env pipecl
#
# SPITRDET - Transient Detection
#
# Description:
#
# This module makes use of the IRAF task acediff to compare a
# non-resampled SIF to a reference stack -- presumably clean of cosmic
# #rays, artifacts, and transients -- of the same region to identify
# pixels affected by these.  Affected pixels in the non-resampled SIF
# are "fixed" by acediff, and these pixels are identified in the
# object mask output from acediff.  This object mask is merged with
# the bad pixel mask of the non-resampled SIF.  Pixels flagged by
# acediff are identified in the bad pixel mask by value 8, and
# previously identified bad pixels retain their values in the merged
# mask.  In cases when a previously bad pixel is also flagged by
# acediff, that pixel retains its original value in the merged bad
# pixel mask.

# Declare variables
int	status = 1
real	scale
string 	dataset, datadir, lfile, cbpmFULL
string	cbpm, image, cbpmORIG
file	caldir = "MC$"

# Load packages
redefine mscred = mscred$mscred.cl
noao
nproto
nfextern
ace
#mscred
servers

# Set file and path names
spinames(envget("OSF_DATASET"))
dataset=spinames.dataset
datadir=spinames.datadir
lfile=spinames.lfile
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=spinames.uparm)
set (pipedata=spinames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSTRTRDET (%s): ", dataset) | tee (lfile)
time | tee (lfile)

if ( spi_trrej == "none" )
    logout 1
;

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
concat( "cbpm_p" ) | scan( cbpmFULL )
spinames( image )

hselect( spinames.croppedim, "CRSCALE", yes ) | scan( scale )

# Do not remove transients from image that have a magzero more than 
# 2 mag (factor of 6.25) different from the reference stack.
x = 2.5**spi_phvarcut
y = 2.5**(-spi_phvarcut)
if ( (scale<y) || (scale>x) ) {
    printf( "No transient rejection because of large scale factor %f\n",
	scale )
    logout 1
}
;

# For the purpose of inputting an image and mask without any transients
# fixpix'd or masked, replace the CBPM with one that does not mask these
# transients detected in the first pass.  Save the original CBPM
# as *-ORIG.pl. Also, save a mask ("tmp0.pl") that includes only
# the TR-flagged pixels from the first pass.
hselect( image, "CBPM", yes ) | scan( cbpm )
cbpmORIG = substr( cbpm, 1, strlstr(".pl",cbpm)-1 ) // "-ORIG.pl"
imrename( cbpm, cbpmORIG )
mskexpr( "i>7? 1: 0", "tmp0.pl", cbpmORIG)
mskexpr( "i>7? 0: i", cbpm, cbpmORIG)

# Use acediff, with the cropped stack as a reference that is assumed
# to be free of artifacts, to identify cosmic rays, artifacts, and
# transients that may be present in the non-resampled SIF.  acediff
# will then create a mask identifying the affected pixels.
acediff( image, masks="!CBPM", rimages=spinames.croppedim, rmasks="!BPM",
    objmask="objmask", hsigma=3, lsigma=3, ldetect+, rfrac=1.5,
    cafwhm=3 )

# Convert the acediff objmask to a BPM (tmp1.pl).  Merge this BPM with
# the original CBPM (cbpmORIG) that includes the bad pixels and
# first-pass TR-flagged pixels.  Thus, the merged mask (tmpALL.pl)
# will include all bad pixels and TR-flagged (1st and 2nd pass)
# pixels.  Copy the non-fixpix'd image to "prefix.fits" (this image
# has only been fixpix'd previously for bad pixels).  Fixpix the image
# (image) across ALL bad and TR-flagged pixels (tmpALL.pl).  Finally,
# construct the final properly fixpix'd image: 1) for the previously
# determined bad pixels, use the pixel values from the previous image
# (prefix.fits), which may have been fixpix'd prior to this module, 2)
# for the TR-flagged pixels (1st and 2nd pass), use the pixel values
# from the image fixpix'd here.  The net result is an image with bad
# and TR-flagged pixels all only fixpix'd once, which optionally does
# not fixpix persistence and/or saturation artifacts.
mskexpr( "i>10? 1: 0", "tmp1.pl", "objmask[pl]" )
imexpr( "(a>0 || b>0)?1: 0","tmpTR.pl", "tmp0.pl", "tmp1.pl" )
imexpr( "(a>0 || b>0)?1: 0","tmpALL.pl", cbpmORIG, "tmp1.pl" )
imcopy( image,"prefix.fits" )
fixpix( image, "tmpALL.pl", verbose-, pixels- )
# Comment out the following four lines IF you want to have the 
# image fixpix'd over ALL bad+TR pixels in this module.
imexpr( "c>0? a: b", "merged.fits", "prefix.fits", image, cbpm )
imdelete( "prefix.fits", verify- )
imdelete( image, verify- )
imrename( "merged.fits", image )

# Merge the object pixel mask created by acediff with the previous
# cumulative pixel mask for the image.  A pixel identified by ace is
# flagged with a value of 8 in the cumulative bad pixel mask, unless
# it was previously identified as a bad pixel, in which case it
# retains its original value in the mask.  Finally, update the BPM
# value in the header of the acediff-corrected non-resampled SIF to
# reflect the name of the merged bad pixel mask.
mskexpr( "m>0?m:(i>0? 8: 0)", "tmpmask.pl", "tmpTR.pl", refmask=cbpm )

# Replace the existing cumulative bad pixel mask with the new one
imdel( cbpm )
imrename( "tmpmask.pl", cbpm )

# Store the updated cumulative mask in the same location from 
# where the original mask was retrieved using getcal in spisetup.cl.
delete(cbpmFULL,verify-)
imcopy(cbpm,cbpmFULL)

# Clean up.
delete("strmask_*",verify-)
delete("*dcat*",verify-)
delete(cbpmORIG,verify-)
delete("tmp*.pl",verify-)

logout(status)
