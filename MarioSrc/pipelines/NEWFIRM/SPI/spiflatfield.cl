#!/bin/env pipecl
#
# spiflatfield

# Declare variables
int     status = 1
string  dataset, datadir, lfile, image, fpstack, flat

# Load packages
noao
nproto

# Set file and path names
spinames( envget( "OSF_DATASET" ) )
dataset = spinames.dataset
datadir = spinames.datadir
lfile = spinames.lfile
set ( uparm = spinames.uparm )
set ( pipedata = spinames.pipedata )

print( datadir )
cd( datadir )

# Log start of processing.
printf( "\nSTRFLATFIELD (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
concat( "fpstack_p" ) | scan( fpstack )
spinames( image )

imcopy( image, image//"_flatfield" )

if ( whenflatfield == "after" ) {
    # Retrieve the location of the flat field from the header, if available
    flat = "" ; hselect( image, "FLAT", yes ) | scan( flat )
    if (flat == "") {
        printf( "No flat field was applied!\n" ) | tee( lfile )
    } else {
        # Dome flat available, so divide data by flat
        imarith( image, "/", flat, image )
        printf( "Applied flat field: %s\n", flat ) | tee( lfile )
    }
}
;

logout( status )
