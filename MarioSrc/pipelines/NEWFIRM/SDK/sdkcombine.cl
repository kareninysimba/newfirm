#!/bin/env pipecl
#
# SDKPROC

int     status = 1
int     namps
string  datadir, dataset, ifile, image, indir, lfile, trimsec, cal, bpm

file    caldir = "MC$"

# Tasks and packages
images
mscred
proto

# Set file and path names.
sdknames (envget("OSF_DATASET"))
dataset = sdknames.dataset

# Set filenames.
indir = sdknames.indir
datadir = sdknames.datadir
ifile = indir // dataset // ".sdk"
lfile = datadir // sdknames.lfile
set (uparm = sdknames.uparm)
set (pipedata = sdknames.pipedata)
cal = sdknames.cal

# Log start of processing.
printf ("\nSDKCOMBINE (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# The code below does not distinguish between static bad pixels and
# pixels affected by persistence (see sdkproc.cl). Thus, because the
# static bad pixels are always present, the code below will always
# report that there are undefined pixels. However, as long as those
# undefined pixels are always coincident with the bad pixels, this is
# not a problem. Only if persistence were to cause undefined pixels
# should the alert be made. For the time being, this code is 
# commented out.
# First check whether persistence may have affected the darks
# in the input list.
#if (do_persistence) {
#    imcombine( "@sdkmasks.list", "sdkmasks", offset="physical",
#        combine="sum" )
#    # If there are any pixels in the stack to which no darks contributed
#    # valid data, then reject this stacked dark.
#    imstat( "sdkmasks", upper=0, format-, fields="npix" ) | scan( i )
#    if (i>0)
#        sendmsg( "WARNING", "Found undefined pixels in dark", dataset, "VRFY" )
#        #status = 0
#    ;
#    delete( "sdkmasks.list" )
#    imdel( "sdkmasks" )
#}
#;

count (ifile) | scan (i)
if ( i == 0 ) {
    print( "The input file list is empty. All rejected?" )
    logout 1
}
;

if ( status>0 ) {
    if (i > 3)
	imcombine( "@"//ifile, cal, offset="physical", reject="minmax", nlow=0 )
    else
	imcombine( "@"//ifile, cal, offset="physical", reject="none" )

    # Retrieve the BPM keyword from the first image in the list ...
    list = ifile
    i = fscan( list, image )
    list = ""
    hselect( image, "BPM", yes ) | scan( bpm )
    # ... and add it to the combined calibration 
    hedit( cal, "BPM", bpm, add+, ver- )

    # Interpolate over all bad pixels to produce cosmetically nice
    # images.
    fixpix( cal, "BPM" )

    # Remove image extensions from the IMCMB keywords, and add .fits.
    imhead( cal, long+ ) | match( "IMCMB" ) |
	fields( "STDIN", "2", > "sdkcombine1.tmp" )
    nhedit( cal, "IMCMB*", ".", ".", update+, delete+, show+ )
    list = "sdkcombine1.tmp"
    i = 1
    while ( fscan( list, s1 ) != EOF ) {
        j = stridx( "_", s1 )
        if ( j > 0 )
	    s1 = substr( s1, 1, j-1 )
        ;
        s1 = s1 // ".fits"
        printf( "%03d\n", i ) | scan( s2 )
        s2 = "IMCMB" // s2
        hedit( cal, s2, s1, add+, ver-, show+ )
        i = i+1
    }
    list = ""

}
;

logout 1
