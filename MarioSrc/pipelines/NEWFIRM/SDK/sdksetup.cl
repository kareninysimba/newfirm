#!/bin/env pipecl
#
# SDKSETUP -- Setup SDK processing data including calibrations.

int	status = 1
struct	statusstr, mask_name
string	dataset, indir, datadir, ifile, im, lfile

string	subdir
file	caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images

# get the dataset name
sdknames( envget("OSF_DATASET") )
dataset = sdknames.dataset

# Set paths and files.
indir = sdknames.indir
ifile = indir // dataset // ".sdk"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
	caldir = caldir // subdir

datadir = sdknames.datadir
lfile = datadir // sdknames.lfile
set (uparm = sdknames.uparm)
set (pipedata = sdknames.pipedata)
cd (datadir)
	
# Log start of processing.
printf( "\nSDKSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Check if input files are accessible.
if (defvar( "PIPEDEBUG" )) {
    type( ifile )
    list = ifile
    while( fscan( list, s1 ) != EOF ) {
        if ( !imaccess( s1 ) ) {
	    sendmsg ("ERROR", "Can't access image", s1, "CAL")
	    status = 0
	}
	;
    }
    list = ""
    if (status == 0)
        logout (status)
    ;
}
;

# Setup uparm and pipedata
delete( substr( sdknames.uparm, 1, strlen(sdknames.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

# Get the calibrations.
list = ifile
while (fscan (list, s1) != EOF) {

    getcal (s1, "bpm", cm, caldir,
	obstype="", detector="!INSTRUME", imageid="!detector",
	filter="", exptime="", mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;

}
list = ""


logout 1
