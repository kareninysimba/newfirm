#!/bin/env pipecl
#
# SDKPROC

int	status = 1
int     namps
real    mjd, mean, sigma
string  datadir, dataset, ilist, olist, image, indir, lfile
string  obpm, trimsec

file    caldir = "MC$"

# Tasks and packages
images
servers
proto
dataio

# Set file and path names.
sdknames (envget("OSF_DATASET"))
dataset = sdknames.dataset

# Set filenames.
indir = sdknames.indir
datadir = sdknames.datadir
ilist = indir // dataset // ".sdk"
lfile = datadir // sdknames.lfile
set (uparm = sdknames.uparm)
set (pipedata = sdknames.pipedata)

# Log start of processing.
printf ("\nSDKDQ (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Start with a clean slate
if ( access( "sdkdq1.tmp" ) ) 
    delete( "sdkdq1.tmp" )
;
# Start with a clean slate
if ( access( "sdkdq2.tmp" ) ) {
    delete( "sdkdq2.tmp" )
}
;
if ( imaccess( "dqmean" ) )
    imdel( "dqmean" )
;

# Count the number of darks
count( ilist ) | scan( i )
# Set the name for the new list
olist = indir // dataset // "1.tmp"
if (access(olist))
    delete (olist)
;

if (i<3) {
    copy( ilist, olist )
} else {

    mimstat( "@"//ilist, imasks="!BPM", fields="image,mean",
        nclip=3, lsigma=3, usigma=3, format-, > "sdkdq1.tmp" )
    # Show the statistics
    print( "DARK STATISTICS:" )
    concat( "sdkdq1.tmp" )
    print( "----------" )
    
    # Reject darks with a mean above sdk_maxdarkval
    list = "sdkdq1.tmp"
    touch( "sdkdq2.tmp" )
    while ( fscan( list, s1, x ) != EOF ) {
	if ( x < sdk_maxdarkval ) 
	    printf( "%s %f\n", s1, x, >> "sdkdq2.tmp" )
	else
	    printf( "Rejected high value %s: %f\n", s1, x )
    }
    list = ""

    # Count the remaining darks
    count( "sdkdq2.tmp" ) | scan( i )
    if ( i == 0 ) {
        status = 2
        touch( olist )
	sendmsg( "WARNING", "No darks to combine. All rejected?",
            dataset, "PROC" )
    }
    ;

    if ( status == 1 ) {
        if ( i<3 ) {
            fields( "sdkdq2.tmp", "1", > olist )
        } else {
            fields( "sdkdq2.tmp", "2" ) |
                rtextimage( "STDIN", "dqmean", header-, dim="1,"//str(i) )
            imstat( "dqmean", nclip=3, fields="mean,stddev",
                lsigma=2, usigma=2, format- ) | scan( mean, sigma )

            # Now loop over the input list and reject outliers
            list = "sdkdq2.tmp"
            printf( "Mean: %f\n", mean )
            printf( "Sigma: %f\n", sigma )
            while ( fscan( list, s1, x ) != EOF ) {
                if ( (x>mean-5*sigma) && (x<mean+5*sigma) )
	            print( s1, >> olist )
                else
	            printf( "Rejected outlier %s: %f\n", s1, x )
            }
            list = ""
        }
    }
    ;
}

# Replace the original input list
delete( ilist )
pathnames ("@"//olist, > ilist)
delete (olist)
print "=================="
concat( ilist )
print "=================="

logout( status )
