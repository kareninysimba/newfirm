#!/bin/env pipecl
#
# SDKPROC

int	status = 1
int     namps
real    mjd
string  datadir, dataset, ilist, olist, image, indir, lfile
string  obpm, trimsec

file    caldir = "MC$"

# Tasks and packages
images
servers
proto

# Set file and path names.
sdknames (envget("OSF_DATASET"))
dataset = sdknames.dataset

# Set filenames.
indir = sdknames.indir
datadir = sdknames.datadir
ilist = indir // dataset // ".sdk"
lfile = datadir // sdknames.lfile
set (uparm = sdknames.uparm)
set (pipedata = sdknames.pipedata)

# Log start of processing.
printf ("\nSDKPROC (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Make sure no persistence masking is done if not in sciencepipeline
if ( sciencepipeline == NO )
    do_persistence = NO
;

list = ilist
olist = indir // dataset // "1.tmp"
if (access(olist))
    delete (olist)
;
for (namps=1; fscan(list,s1)!=EOF; namps+=1) {
    # Get the part of the filename after the last /, stripping the .fits
    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    # Set the sdk variables for this image
    sdknames( s2 )
    # Set the name of the image
    image = sdknames.image
    # Retrieve the trim section from the header
    hsel( s1, "TRIMSEC", yes ) | scan( trimsec )
    # Copy the trimsec only, i.e., remove the reference pixels and
    # delete the section keywords.
    imcopy( s1//trimsec, image )
    hedit (image, "DETSEC,DATASEC,TRIMSEC,BIASSEC", del+, verify-, show-)

    if (do_persistence) {

        # Read the original bad pixel mask from the header
        hselect( image, "BPM", yes ) | scan( obpm )
        # Read keywords needed needed for the getcal
        hselect( image, "DETECTOR,OBSID,MJD-OBS", yes ) | scan( s2, s3, mjd )
        # Retrieve the location of the cumulative mask
        getcal( image, "masks", cm, caldir, imageid=s2, mjd=mjd,
            match="%"//s3, obstype="", detector="", filter="" )
        if (getcal.statcode>0) {
            sendmsg( "ERROR", "Getcal failed with message: "//getcal.statstr,
	        s2, "PROC" )
            status = 0
            break
        }

        # The bad pixel masks should not be merged with the persistence
        # masks, because the former will always be present, and these are
        # masked in the output data. 

# TODO: the persistence mask should not be merged with the BPM
# because bad pixels are always present. Here the goal is to detect
# other bad pixels that may come and go. The masks are currently not
# used in sdkcombine.cl.
        # Merge the bad pixel mask with the persistence mask. A mask
        # with only 0s or 1s is created because that is what sdkcombine.cl
        # needs. Note that the meaning of 0 and 1 is reversed, i.e., 
        # 0 means bad data, 1 means good data.
        mskexpr( "(i>0)? 0 : ((m>0)? 0 : 1)", sdknames.mask, obpm,
            refmasks=getcal.value )

        # Add the current mask to the list of masks, for sdkcombine to use.
	print( sdknames.mask, >> "sdkmasks.list" )

        # There is no need to store the cumulative mask because it
        # is not used by any downstream pipelines

    }
    ;

    # Write name of output image to olist
    print( image, >> olist )
}
list = ""

# Replace the original input list with a list of processed data
concat( olist )
delete( ilist )
pathnames ("@"//olist, > ilist)
delete (olist)

logout 1
