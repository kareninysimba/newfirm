# MARIO.CL -- Pipeline configuration script.
# This may do any valid IRAF setup but it is mostly intended for defining
# global variables.  Note that these become package parameters.

bool    sciencepipeline = yes   {prompt="Science pipeline mode?"}
bool	do_cleanup = yes        {prompt="Clean up intermediate results?"}
bool	do_persistence = yes    {prompt="Apply persistence masks?"}
bool	do_saturation = yes	{prompt="Mask saturation?"}
bool	do_dark = yes           {prompt="Derive dark calibrations?"}
bool	do_flat = yes           {prompt="Derive flat calibrations?"}
bool    debug_mode = no         {prompt="Run pipeline in debug mode?"}
bool    save_for_sft = no       {prompt="Save preflatfield for sft creation?"}

string  lincoeftype = "linimage" {prompt="Linearity type (linconst|linimage)"}

string  whenflatfield = "before" {prompt="Flatten before|after sky sub?"}
string  stripe_removal = "smart" {prompt="Do stripe removal?",enum="yes|no|smart"}
bool	ring_removal = no       {prompt="Perform ring removal?"}

bool	dir_stkgrp = yes	{prompt="Group sequences for stacking?"}

real	sdk_maxdarkval = 3000   {prompt="Maximum allowed mean for a dark"}

bool	sfl_nodarkcont = yes    {prompt="Continue if no dark available?"}
bool	sfl_nolincont = yes     {prompt="Continue if no linearity available?"}
real    sfl_mincounts = 500     {prompt="Minimum count in (difference) flat"}

bool	ogc_fixchannels = yes   {prompt="Fix noisy channels?"}
bool	ogc_nodarkcont = no     {prompt="Continue if no dark available?"}
bool	ogc_nolincont = no      {prompt="Continue if no linearity available?"}
bool	ogc_noflatcont = no     {prompt="Continue if no dome flat available?"}

int     per_numexps = 5         {prompt="Number of exposures to mask persistence"}

int     sky_minrunmedwin = 3    {prompt="Min window size for running median"}
int     sky_maxrunmedwin = 9    {prompt="Max window size for running median"}
bool	sky_pairwiseskysub = no {prompt="Do pairwise sky subtraction?"}
string	sky_resbck = "ace,4Q,none"  {prompt="Sky subtraction methods"}
real	sky_4qradius = 80       {prompt="4Q mask circle radius in percent"}

string	sps_resbck = "poly5,poly2,pedestal"  {prompt="2nd-pass sky subtraction methods"}
string  sps_resoff = "poly5,poly2,poly5"    {prompt="2nd-pass sky sub for offsets"}

real    wcs_fracmatch = 0.5     {prompt="WCS matching fraction"}
bool	wcs_wcsglobal = yes     {prompt="Do only global WCS update"}
real    gos_ditherstep = 1200   {prompt="Maximum dither step for grouping"}
bool    gos_byarray = no        {prompt="Stack data per array"}

string	psq_order = "asc"       {prompt="PSQ selection order"}
int     psq_limit = 999999      {prompt="PSQ data limit"}
string  ds_cache = "/data1/newfirm/DSCache/" {prompt="Data service cache"}
string  psq_redo = "no"         {prompt="Redo previous processing?",enum="yes|no|cal|obj"}

string	cal_skyflat = "flat"	{prompt="Sky flat type (ignore|flat|object)"}
int     cal_nzero = 3           {prompt="Min zero combine for putcal"}
int     cal_nflat = 3           {prompt="Min flat combine for putcal"}

# NOTE: THE ABSOLUTE VALUE FOR cal_mjdmin and cal_mjdmax MUST BE THE SAME!!
real	cal_mjdmin = -180         {prompt="Min MJD for putcal"}
real	cal_mjdmax = 180         {prompt="Max MJD for putcal"}

string  stk_reject = "badbmagzero,1.0" {prompt="Type of STK rejection"}
string  stk_weight = "default"  {prompt="Type of STK weighting"}
string  spk_reject = "badbmagzero,1.0" {prompt="Type of SPK rejection"}
string  spk_weight = "default"  {prompt="Type of SPK weighting"}
string	sti_trrej = "craverage"	{prompt="1st-pass CR rejection (none|craverage)"}
string	spi_trrej = "acediff"	{prompt="2nd-pass CR rejection (none|acediff)"}
real    spi_phvarcut = 2        {prompt="Cutoff above which no trrej?"}

real	rsp_grid = 1            {prompt="Tangent point grid (deg)"}
real	rsp_scale = 0.4         {prompt="Resampling scale"}
string	rsp_interp = "lsinc17"  {prompt="Resampling interpolant"}
int     png_blk = 2             {prompt="PNG version block size"}
int     png_pix = 120           {prompt="PNG preview pixel size"}
int     png_pixm = 800          {prompt="PNG medium preview pixel size"}
int     verbose = 0             {prompt="Verbosity level"}

string	email = "operator"      {prompt="Address to send email"}
string	dps_archive = "no"      {prompt="Archive data products?"}
string	dts_ftp = "no"          {prompt="FTP data products (yes|no|proposal)"}
string  dts_save = ".dps MKD?*fits S[PT]K?*_s[pt]k?*fits _psp log enids cat" {prompt="Pattern to save"}
string	all_erract = "return"   {prompt="Error action (none|return)"}

bool	mkd_press = yes		{prompt="Make pre-ss data product?"}
bool	mkd_ss = yes		{prompt="Make ss data product?"}
bool	mkd_ssrsp = yes		{prompt="Make ss resampled data product?"}
bool	mkd_stk = yes		{prompt="Make stacked data product?"}

bool	stb_verify = yes	{prompt="Check archive before deleting?"}
string	stb_verifypath = "/data1/newfirm/stbcheck/" {prompt="Path for data to be checked against archive"}

keep
