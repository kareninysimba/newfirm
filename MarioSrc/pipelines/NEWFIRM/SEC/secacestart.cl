#!/bin/env pipecl
#
# MTDSTATS

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ifile, lfile, stacked, listoflists
string  temp1, temp2, bpm, s4

string  subdir

# Tasks and packages
images
servers
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# get the dataset name
names( envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET") )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ifile = indir // dataset // ".sec"
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Log start of processing.
printf( "\nSECACESTART (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

cd (datadir)

stacked = dataset // ".stacked"
listoflists = indir // dataset // "ace.lol"
s2 = "secacestart.tmp"

# Find the stacked images in the input list.  Return no data.
match( "_STK?*_stk.fits", ifile, >> stacked )
list = stacked
i = 1
while ( fscan( list, s1 ) != EOF ) {

    # Create the output file name for the current list in s3
    s3 = str(i)
    if (strlen(s3)==1)
        s3 = "0"//s3
    ;
    s3 = indir // s3

    # Find the mask.
    bpm=""; hselect (s1, "BPM", yes) | scan (bpm)
    if (bpm != "") {
	j = stridx ("[", bpm)
	if (j == 0)
	    match( bpm, ifile) | scan (s4)
	else {
	    match( substr(bpm,1,j-1), ifile) | scan (s4)
	    s4 += substr (bpm,j,999)
	}
    }
    ;
    # Write the current stack and the corresponding mask to the current list
    printf ("%s %s\n", s1, s4, >> s3)
    # Append the name of the current list to the list of lists in s2
    print( s3, >> s2 )
    i = i+1
}
list = ""

if (access(s2) == NO)
    logout (1)
;

pathnames( "@"//s2, >> listoflists )

temp1 = "secace1.tmp"
temp2 = "secace2.tmp"
# Call the ACE pipeline
callpipe( "sec", "ace", "split", listoflists, temp1, temp2 )
concat (temp2) | scan (status); delete (temp2)
concat (temp1); delete (temp1)
printf("Callpipe returned %d\n", status)

logout( status )
