#!/bin/env pipecl
#
# MTDSTATS

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ifile, im, lfile, nfile

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
names( envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET") )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ifile = indir // dataset // ".sec"
nfile = dataset // ".needed"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
        caldir = caldir // subdir

set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Log start of processing.
printf( "\nSECSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

cd (datadir)

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( nfile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

logout( status )
