#!/bin/env pipecl
#
# MTDSTATS

int	nfound, pos1, pos2
real	ra, dec, equinox, x1, x2, x3
string	lfile, mcat, sourcefiles, targetfiles, obstype

# Load packages.
proto
images
noao
astutil
servers
task	wcsgetcat = "NHPPS_PIPESRC$/NEWFIRM/WCS/wcsgetcat.cl"

# Set the file names
wcsnames( envget ("OSF_DATASET") )

# Set dataset directory.
lfile = wcsnames.lfile
set (uparm = wcsnames.uparm)

cd( wcsnames.datadir )

# Log start of processing.
printf ("\n%s (%s): ", strupr( envget("NHPPS_MODULE_NAME") ),
    wcsnames.dataset) | tee( lfile )
time | tee( lfile )

# Get the global headers. These were created when the MEF files were
# split, i.e., when the GCL pipeline was started. These were returned
# in the GCL return file, the contents of which were put in the file
# "wcsgcl" by wcssetup.cl.

# Create the name of the files to contain the source and target files
sourcefiles = "wcsgcat1.tmp"
targetfiles = "wcsgcat2.tmp"

# Select the source files from wcsgcl
match( "_00.fits", "wcsgcl", >> sourcefiles )
# Create the target file names
list = sourcefiles
while ( fscan (list, s1) != EOF ) { # Loop over all global headers
    hselect( s1, "OBSTYPE", yes ) | scan( obstype )
    # Get the part of the filename after the last /, stripping the .fits
    s2 = substr( s1, strldx ("/",s1)+1, strlstr (".fits",s1)-1 )
    print( s2, >> targetfiles )
}
list = ""

# Copy the files
imcopy( "@"//sourcefiles, "@"//targetfiles )

list = targetfiles
while ( fscan(list, s1) != EOF ) { # Loop over all global headers

    wcsnames( s1 ) # set WCS names for s1
    print( s1 )	

    # Set defaults for the ra, dec, and equinox variables
    ra = INDEF; dec = INDEF; equinox = 2000.

    if (isindef(ra) || isindef(dec) || isindef(equinox)) {
        hselect (s1, "RA,DEC,RADECEQ", yes) | scan (ra, dec, equinox)
    }
    ;

    if (isindef(ra) || isindef(dec) || isindef(equinox)) {
        hselect (s1, "CRVAL1,CRVAL2", yes) | scan (ra, dec)
        ra /= 15
    }
    ;

    if (isindef(ra) || isindef(dec) || isindef(equinox)) {
        sendmsg ("ERROR", "Pointing information not found", "", "VRFY")
        logout 0
    }
    ;

    if (abs(equinox-2000.) > 0.01) {
	printf ("Precess from %.1f to 2000.\n", equinox) | tee (lfile)
	printf ("%g %g\n", ra, dec) |
	precess ("STDIN", equinox, 2000.) |
	scan (ra, dec)
    }
    ;

    wcsgetcat( ra, dec, 0:30:00, wcsnames.mcat, jmax=17 )

    # Check if a reasonable catalog returned.
    if (access (wcsnames.mcat) == NO) {
        sendmsg ("ERROR", "No USNO match catalog returned", "", "PROC")
        logout 0
    } else
        ;
    nfound = 0
    count (wcsnames.mcat) | scan (nfound)
    if (nfound<10) {
        sendmsg ("ERROR", "Too few entries in USNO match catalog", str(nfound),
            "PROC")
        delete (wcsnames.mcat)
        logout 0
    }

    # Log something.
    printf ("    Retrieved %d reference objects from USNO-B1.0\n     \
        centered at %.2h %.1h\n", nfound, ra, dec) | tee (lfile)
}

# Clean up
delete( sourcefiles )
delete( targetfiles )
delete( "wcsgcat*.tmp" )

logout 1
