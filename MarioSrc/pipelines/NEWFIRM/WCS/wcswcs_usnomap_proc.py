#!/bin/env python

"""
Capture data quality information from the output of usnomatch and create
a .cl script to update the dataset and PMAS with.
"""

import re
import sys

outfile = open( 'wcswcs_usnomap_proc.cl', 'w' )

infile = file( sys.argv[1], 'r')
for line in infile.readlines():
    thisline = line.strip()
    m = re.match( 'tangent point shift = \((.*), (.*)\) arcsec', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcuptx\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupty\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcuptx\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcupty\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))
    m = re.match( 'fractional scale change = \((.*), (.*)\)', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupsx\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupsy\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcupsx\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcupsy\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))
    m = re.match( 'axis rotation = \((.*), (.*)\) degrees', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupax\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupay\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcupax\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcupay\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))
    m = re.match( 'rms = \((.*), (.*)\) arcsec', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcuprx\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupry\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcuprx\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcupry\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))


infile.close()
outfile.close()


