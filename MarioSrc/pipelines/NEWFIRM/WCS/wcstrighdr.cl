#!/bin/env pipecl
#
# MTDSTATS

string dataset, lfile

# Load packages.
images
servers

# Directories and files.
wcsnames( envget("OSF_DATASET") )
dataset = wcsnames.dataset
lfile = wcsnames.datadir // wcsnames.lfile
set (uparm = wcsnames.uparm)
cd (wcsnames.datadir)

# Log start of processing.
printf ("\nWCSTRIGHDR (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Trigger the SIF pipelines.
match ("hdrtrig", "*.ace", print-, > "wcstrighdr.tmp")
concat ("wcstrighdr.tmp")
touch ("@wcstrighdr.tmp", verbose+)
delete ("wcstrighdr.tmp")

logout 1
