#!/bin/env pipecl
#
# MTDSTATS

int	num1, num2, num3, num4, num5, num6, num7, num8
real	seeing, x1, zp, ezp, skyval, eskyval, skynoise, skymag, depth
string	cast, dataset, lfile, glob, gclheader
struct	*fd

# Load packages.
task $ls = "$!ls"
images
servers

# Directories and files.
wcsnames( envget("OSF_DATASET") )
dataset = wcsnames.dataset
lfile = wcsnames.datadir // wcsnames.lfile
set (uparm = wcsnames.uparm)
cd (wcsnames.datadir)

# Log start of processing.
printf ("\nWCSHDR (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Get seeing from each of the amplifiers, calculate the average,
# and store in PMAS

# Create a list of files to match against
concat( "*.swc", > "wcshdr1.tmp" )
match( "_ss.fits", "wcshdr1.tmp", > "wcshdr2.tmp" )
delete( "wcshdr1.tmp" )

# Make sure the file exists
touch( "wcswcs1.tmp" )

list = "wcswcs1.tmp"
while (fscan (list, s1) != EOF) {
print "START of LOOP"
    # Extract the arrays for the current dataset, the _ is needed
    # to prevent matching the pattern in the directory name
    match( s1//"_", "wcshdr2.tmp", > "wcshdr3.tmp" )

    # Extract the global header for the current dataset
    glob = s1 // "_00"

    # Set wcsnames using the global header name
    wcsnames( glob )
    wcsnames.hdr = glob

    # Initialize the variables
    num1 = 0; num2 = 0; num3 = 0; num4 = 0; num5 = 0; num6 = 0;
    num7 = 0; num8 = 0
    seeing = 0; zp = 0; ezp = 0; skyval = 0; eskyval = 0
    skymag = 0; depth = 0; skynoise = 0

    # Loop over all arrays
    fd = "wcshdr3.tmp"
    while( fscan( fd, s2 ) != EOF ) {
	printf( "IMAGE %f\n", s2 )
        hselect( s2, "SEEING1", yes ) | scan( x )
        if ( nscan()==1 ) {
print "sssss"
print( x )
            num1 = num1+1
            seeing = seeing+x
        }
        hselect( s2, "DQPHZP", yes ) | scan( x )
        if ( nscan()==1 ) {
            num2 = num2+1
            zp = zp+x
        }
        hselect( s2, "DQPHEZP", yes ) | scan( x )
        if ( nscan()==1 ) {
            num3 = num3+1
            ezp = ezp+x
        }
        hselect( s2, "SKYBG1", yes ) | scan( x )
        if ( nscan()==1 ) {
            num4 = num4+1
            skyval = skyval+x
        }
        hselect( s2, "DQSKEAD1", yes ) | scan( x )
        if ( nscan()==1 ) {
            num5 = num5+1
            eskyval = eskyval+x
        }
        hselect( s2, "SKYMAG1", yes ) | scan( x )
        if ( nscan()==1 ) {
            num6 = num6+1
            skymag = skymag+x
        }
        hselect( s2, "PHOTDPTH", yes ) | scan( x )
        if ( nscan()==1 ) {
            num7 = num7+1
            depth = depth+x
        }
        hselect( s2, "SKYNOIS1", yes ) | scan( x )
        if ( nscan()==1 ) {
            num8 = num8+1
            skynoise = skynoise+x
        }
        
    }
    fd = ""

    # Compute global averages and update headers and PMAS as needed
    if ( num1 !=0 ) {
        seeing = seeing/num1
        hedit( glob, "SEEING", seeing, add+, ver- )
        printf("%12.5e\n", seeing ) | scan( cast )
        setkeyval( class="mefobjectimage", id=wcsnames.shortname, dm=dm,
            keyword="dqsemef", value=cast )
    } else {
	hedit( glob, "SEEING", INDEF, add+, ver- )
        print( "WARNING: Cannot calculate global seeing" )
    }
    if ( num2 !=0 ) {
        zp = zp/num2
        printf("%12.5e\n", zp ) | scan( cast )
        setkeyval( class="mefobjectimage", id=wcsnames.shortname, dm=dm,
            keyword="dqphazp", value=cast )
    } else {
        print( "WARNING: Cannot calculate global zero point" )
    }
    if ( num3 !=0 ) {
        ezp = ezp/num3
        printf("%12.5e\n", ezp ) | scan( cast )
        setkeyval( class="mefobjectimage", id=wcsnames.shortname, dm=dm,
            keyword="dqpheazp", value=cast )
    } else {
        print( "WARNING: Cannot calculate error on global zero point" )
    }
    if ( num4 !=0 ) {
        skyval = skyval/num4
        hedit( glob, "SKYBG", skyval, add+, ver- )
        printf("%12.5e\n", skyval ) | scan( cast )
        setkeyval( class="mefobjectimage", id=wcsnames.shortname, dm=dm,
            keyword="dqskaval", value=cast )
    } else {
	hedit( glob, "SKYBG", INDEF, add+, ver- )
        print( "WARNING: Cannot calculate global sky value" )
    }
    if ( num5 !=0 ) {
        eskyval = eskyval/num5
        printf("%12.5e\n", eskyval ) | scan( cast )
        setkeyval( class="mefobjectimage", id=wcsnames.shortname, dm=dm,
            keyword="dqskeavl", value=cast )
    } else {
        print( "WARNING: Cannot calculate error on global sky value" )
    }
    if ( num6 !=0 ) {
        skymag = skymag/num6
        hedit( glob, "SKYMAG", skymag, add+, ver- )
        printf("%12.5e\n", skymag ) | scan( cast )
        setkeyval( class="mefobjectimage", id=wcsnames.shortname, dm=dm,
            keyword="dqskamag", value=cast )
    } else {
	hedit( glob, "SKYMAG", INDEF, add+, ver- )
        print( "WARNING: Cannot calculate global sky brightness" )
    }
    if ( num7 !=0 ) {
        depth = depth/num7
        printf("%12.5e\n", depth ) | scan( cast )
        setkeyval( class="mefobjectimage", id=wcsnames.shortname, dm=dm,
            keyword="dqphadps", value=cast )
    } else {
        print( "WARNING: Cannot calculate average photometric depth" )
    }
    if ( num8 !=0 ) {
        skynoise = skynoise/num8
        hedit( glob, "SKYNOISE", skynoise, add+, ver- )
        printf("%12.5e\n", skynoise ) | scan( cast )
        setkeyval( class="mefobjectimage", id=wcsnames.shortname, dm=dm,
            keyword="dqskasig", value=cast )
    } else {
	hedit( glob, "SKYNOISE", INDEF, add+, ver- )
        print( "WARNING: Cannot calculate global noise in sky level" )
    }

    # Clean up
    delete( "wcshdr3.tmp" )

}
list = ""

# Comment the line below if wcshdr.cl is to be rerun
#delete( "wcswcs1.tmp" )
delete( "wcshdr2.tmp" )

logout 1
