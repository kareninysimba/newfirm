#!/bin/env pipecl
#
# WCSACEDQ -- Take ACE results and merge them.

int	status = 1
real	seeingp, skybg, skynoise, dpthadu, skymean, sigmean
string	lfile, mef, swcdir
struct	lfile, sky, sig, *meflist, *siflist

# Tasks and  packages.
servers
images
utilities
proto
noao
artdata
tables
ttools
dataqual
cache	sections

# Set directories and files.
wcsnames (envget ("OSF_DATASET"))
lfile = wcsnames.lfile
set (pipedata = wcsnames.pipedata)
set (uparm = wcsnames.uparm)
cd (wcsnames.datadir)

# Log start of processing.
printf ("\nWCSACEDQ (%s): ", wcsnames.dataset) | tee (lfile)
time | tee (lfile)

# Loop over all the exposures.
sections ("*%_00.fits%%", > "meflist.tmp")
meflist = "meflist.tmp"
while (fscan (meflist, mef) != EOF) {

    # Get matched catalogs for an MEF.  This also defines the SIFs.
    sections (mef//"*%_mcat%%", > "siflist.tmp")

    # Check if a matched catalog was produced.
    count ("siflist.tmp") | scan (i)
    if (i == 0) {
        delete ("siflist.tmp")
	next
    }
    ;

    # Compute global (average) values.
    thselect (mef//"*_mcat", "SEEINGP1", yes) | average | scan (seeingp)
    thselect (mef//"*_mcat", "SKYBG1", yes) | average | scan (skybg)
    thselect (mef//"*_mcat", "SKYNOIS1", yes) | average | scan (skynoise)
    thselect (mef//"*_mcat", "DPTHADU1", yes) | average | scan (dpthadu)
    delete (mef//"*_mcat")

    # Analyze global sky maps.
    wcsnames (mef)
    match (mef//"?*_sky", "*.ace", print-, > "wcsacedq.tmp")
    count ("wcsacedq.tmp") | scan (i)
    if (i < 1) {
	sendmsg( "ERROR", "No sky maps to work on", mef, "VRFY")
	sky = ""; skymean = INDEF
	status = 2
    } else {
	dqgsky ("wcsacedq.tmp", wcsnames.sky, wcsnames.hdr,
	    "pipedata$dqgsky.dat", blank=-1000, verbose-)
	imstat (wcsnames.sky, fields="mean,stddev", lower=-999, format-) |
	    scan (sky)
	print (sky) | scan (skymean)
    }
    delete ("wcsacedq.tmp")

    # Analyze global sigma maps.
    match (mef//"?*_sig", "*.ace", print-, > "wcsacedq.tmp")
    count ("wcsacedq.tmp") | scan (i)
    if (i < 1) {
	sendmsg ("ERROR", "No sky sigma maps to work on", mef, "VRFY")
	sig = ""; sigmean = INDEF
	status = 2
    } else {
	dqgsky ("wcsacedq.tmp", wcsnames.sig, wcsnames.hdr,
	    "pipedata$dqgsky.dat", blank=-1000, verbose-)
	imstat (wcsnames.sig, fields="mean,stddev", lower=-999, format-) |
	    scan (sig)
	print (sig) | scan (sigmean)
    }
    delete ("wcsacedq.tmp")

    # Write information to MEF header, to SIF header, and to PMASS.
    siflist = "siflist.tmp"
    for (j=1; fscan(siflist,s1)!=EOF; j+=1) {
	if (j == 1) {
	    if (sky != "") {
		hedit (mef//"_00.fits", "SKY", sky, add+, show+, >> lfile)
		hedit (mef//"_00.fits", "SKYMEAN", skymean,
		    add+, show+, >> lfile)
	    }
	    ;
	    if (sig != "") {
		hedit (mef//"_00.fits", "SIG", sig, add+, show+, >> lfile)
		hedit (mef//"_00.fits", "SIGMEAN", sigmean,
		    add+, show+, >> lfile)
	    }
	    ;
	}
	;

	# Set information for SIF headers.
        swcnames (s1)
	if (isindef(seeingp)==NO)
	    printf ("SEEINGP %.2f\n", seeingp, >> swcnames.dqhdr)
	;
	if (isindef(skybg)==NO)
	    printf ("SKYBG %.5g\n", skybg, >> swcnames.dqhdr)
	;
	if (isindef(skynoise)==NO)
	    printf ("SKYNOISE %.5g\n", skynoise, >> swcnames.dqhdr)
	;
	if (isindef(dpthadu)==NO)
	    printf ("DPTHADU %.2f\n", dpthadu, >> swcnames.dqhdr)
	;
	if (isindef(skymean)==NO)
	    printf ("SKYMEAN %.5g\n", skymean, >> swcnames.dqhdr)
	;
	if (isindef(sigmean)==NO)
	    printf ("SIGMEAN %.5g\n", sigmean, >> swcnames.dqhdr)
	;

	# Move to SWC directory.
        match (s1//"_cat", "*.ace", print-) | scan (s2)
	move (swcnames.dqhdr, substr(s2,1,strldx("/",s2))
    }
    siflist = ""; delete ("siflist.tmp")

    # Update sky and sig headers.
    if (imaccess(wcsnames.sky))
	mkheader (wcsnames.sky, wcsnames.hdr, append-, verbose-)
    ;
    if (imaccess(wcsnames.sig))
	mkheader (wcsnames.sig, wcsnames.hdr, append-, verbose-)
    ;
}
meflist = ""; delete ("meflist.tmp")

logout (status)
