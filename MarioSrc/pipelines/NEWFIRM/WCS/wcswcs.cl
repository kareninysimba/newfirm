#!/bin/env pipecl
#
# WCSWCS -- Update the WCS.

int	status = 1
int	nprob, nsets, ntot
real    axs, ays, axr, ayr, x1, x2, x3, x4, x5, mjd
string	dataset, lfile, tlfile, procid, cast, s4, glob, globhdr
string	swcdataset, swcdir, swccat, swcmcat, swcwcsdb, swcshort
string  swcwcshdr, shortdir, proj
struct	*list2

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"
task $wcswcs_usnomatch_proc = "$wcswcs_usnomatch_proc.py $1"
task $wcswcs_usnomap_proc = "$wcswcs_usnomap_proc.py $1"
task $wcswcs_ccmap_proc = "$wcswcs_ccmap_proc.py $1"

# Tasks and packages.
utilities
images
imcoords
mario
tables
ttools
servers
dataqual

# Set dataset directory and logfile.
wcsnames( envget ("OSF_DATASET") )
dataset = wcsnames.dataset
set (uparm = wcsnames.uparm)
set (pipedata = wcsnames.pipedata)
lfile = wcsnames.lfile
tlfile = "wcswcs.lfile"
cd( wcsnames.datadir )

# Log start of processing.
printf ("\nWCSWCS (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# TODO: move code below to setup to make it available to all modules if needed

# Create a list of MEF files to process, by selecting all the 
# global header files
if (access("wcswcs.tmp"))
    delete ("wcswcs.tmp")
;
files( "*_00.fits", > "wcswcs.tmp" )
if (access("wcswcs1.tmp"))
    delete ("wcswcs1.tmp")
;
list = "wcswcs.tmp"; touch ( "wcswcs1.tmp" )
while( fscan( list, s1 ) != EOF ) {
    # Strip the _00.fits from the end
    s2 = substr( s1, 1, strstr("_00.fits",s1)-1 )
    print( s2, >> "wcswcs1.tmp" )
}
list = ""
delete( "wcswcs.tmp" )

# Check for the existence of any temporary files. These should
# not exist, but these checks have been added during a round
# of thorough and rigorous "let's eliminate all possible potential
# problems" code checking.
if ( access("wcswcscats.tmp") ) {
    delete( "wcswcscats.tmp" )
}
;
if ( access("wcswcsmcats.tmp") ) {
    delete( "wcswcsmcats.tmp" )
}
;
if ( access("wcswcsdb.tmp") ) {
    delete( "wcswcsdb.tmp" )
}
;

# Loop over all MEF files to process
ntot = 0
nprob = 0
print "DEBUG: wcswcs1.tmp - start"
concat( "wcswcs1.tmp" )
print "DEBUG: wcswcs1.tmp - end"
list = "wcswcs1.tmp"
while( fscan( list, glob ) != EOF ) {

    printf( "DEBUG: loop - working on %s\n", glob )
    # Make sure each entry in the list starts with a clean
    # slate, so that previous problems are not passed on to 
    # the next
    status = 1
    # Append "_" to the contents of glob to avoid matching
    # directory names in the path
    glob = glob//"_"
    if ( access( "wcswcs2.tmp" ) ) {
        delete( "wcswcs2.tmp" )
    }
    ;
    if ( access( "wcswcs.tmp" ) ) {
        delete( "wcswcs.tmp" )
    }
    ;
    # Extract all files matching the current dataset from the 
    # ace return lists ...
    match( glob, "*.ace", print-, > "wcswcs2.tmp" )
    # ... and select the _cat files from the output, which
    # should select one instance of each extension
    match( "_cat", "wcswcs2.tmp", print-, > "wcswcs.tmp" )
    # Loop over all extensions to create input files needed below
    print "DEBUG: wcswcs.tmp - start"
    concat( "wcswcs.tmp" )
    print "DEBUG: wcswcs.tmp - end"
    list2 = "wcswcs.tmp"
    while( fscan( list2, s2 ) != EOF ) {
	# Strip the _cat from the end
        s3 = substr( s2, 1, strlstr("_cat",s2)-1 )
	# Extract the dataset name
        swcdataset = substr( s3, strlstr("/",s3)+1, 999 )
	# And the path name
        swcdir = substr( s3, 1, strlstr("/",s3) )
	# Set the swc names for this dataset
	swcnames( swcdataset )
        # Create input/output files needed below
        print( s2, >> "wcswcscats.tmp" )
        swccat = s2
        print( swcnames.mcat, >> "wcswcsmcats.tmp" )
        printf( "%s %s %s %s %s\n", swcdir, swcnames.mcat, swcnames.wcsdb,
            swcnames.shortname, swcnames.wcshdr, >> "wcswcsdb.tmp" )
    }
    list2 = ""
    delete( "wcswcs2.tmp,wcswcs.tmp" )
    print "DEBUG: wcswcsmcats.tmp - start"
    if ( access( "wcswcsmcats.tmp" ) ) {
        concat( "wcswcsmcats.tmp" )
    }
    ;
    print "DEBUG: wcswcsmcats.tmp - end"
    print "DEBUG: wcswcsdb.tmp - start"
    if ( access( "wcswcsdb.tmp" ) ) {
        concat( "wcswcsdb.tmp" )
    }
    ;
    print "DEBUG: wcswcsdb.tmp - end"

    # Construct the file name of the global header as set in wcsgcat.cl
    s3 = substr( s2, strldx("/",s2)+1, strlstr("_im",s2)-1 ) // "_00"

    # Set wcsnames using the global header name
    wcsnames( s3 )
    wcsnames.hdr = s3

    # Insert new data product.
    mjd = INDEF ; hsel( wcsnames.hdr, "MJD-OBS", yes ) | scan( mjd )
    newDataProduct( wcsnames.shortname, mjd, "mefobjectimage", "")
    storekeywords( class="mefobjectimage", id=wcsnames.hdr,
        sid=wcsnames.shortname, dm=dm )

    # Match the catalogs produced by swcace.cl against the reference catalog
    # produced by wcsgcat.cl
    if (access(wcsnames.mcat) && access("wcswcscats.tmp")) {
        # Delete the output files
	if (access("wcswcsmcats.tmp"))
	    delete( "@wcswcsmcats.tmp" )
	;
        iferr {
        print "DEBUG: USNOMATCH starting"
            usnomatch ("@wcswcscats.tmp", wcsnames.mcat, matchcat="@wcswcsmcats.tmp",
	        imcatdef="@pipedata$catmatch.def", fracmatch=wcs_fracmatch,
		xi=0, eta=0, theta=0, match=3,
	        logfile=tlfile)
#DAWN:		xi=0, eta=0, theta=0, match=3, nmin=40, 
        } then {
            printf( "USNOMATCH failed on %s\n", wcsnames.mcat )
            line = "Warning"
        } else {
            printf( "DEBUG: USNOMATCH completed\n" )
            concatenate (tlfile) | tee (lfile)
        }
    } else {
        printf ("Warning: No reference catalog (%s)\n", wcsnames.mcat) | scan (line)
    }

    if (substr (line, 1, 7) == "Warning") {
        sendmsg ("ERROR", substr(line,9,1000), "", "PROC")
        printf ("ERROR: %s\n", substr(line,9,1000), >> lfile)
        status = 2
    } else {
        # Merge matched catalogs.
        delete (wcsnames.mcat)
        # TODO: determine appropriate value for filter=
        iferr {
            mefmerge( "@wcswcsmcats.tmp", wcsnames.mcat, catdef="",
                filter="G<23.", append+, verbose+ )
        } then {
            print "MEFMERGE FAILED"
            status = 2
        } else { 
            print "MEFMERGE OK"
        }
        delete ("wcswcs_usnomatch_proc.cl")
    }
    delete( tlfile )

    # Compute a global correction and add a constraining grid.
    flpr
    if (status == 1) {
        s1 = "none"
        head( "wcswcsmcats.tmp", nl=1 ) | scan( s1 )
        if ( access( s1 ) ) {
            usnomap( "@wcswcsmcats.tmp", ngrid=250, logfile=tlfile ) |& scan (line)
            concatenate (tlfile) | tee (lfile)
            if (substr (line, 1, 7) == "Warning") {
                sendmsg ("ERROR", substr(line,9,1000), "", "PROC")
	        printf ("ERROR: %s\n", substr(line,9,1000), >> lfile)
                status = 2
            } else {
                wcswcs_usnomap_proc( tlfile )
                cl < wcswcs_usnomap_proc.cl
	        delete ("wcswcs_usnomap_proc.cl")
            }
        } else {
            print "USNOMAP FAILED"
            status = 2
        }
    }
    ;
    delete( "wcswcscats.tmp,wcswcsmcats.tmp" )
    # Do WCS solutions.  Get the shifted tangent point from the logfile.
    if (status == 1) {
        axr = 0
        ayr = 0
        axs = 0
        ays = 0
        nsets = 0
        match ("new tangent point", tlfile) | translit ("STDIN", "(,)", del+) |
	    scan (s1, s1, s1, s1, s1, s2)
        delete( tlfile )
	x = real (s1); y = real (s2)
	hedit (wcsnames.hdr, "OLDRA", "(RA)", add+, show-, >> lfile)
	hedit (wcsnames.hdr, "OLDDEC", "(DEC)", add+, show-, >> lfile)
	hedit (wcsnames.hdr, "RA", s1, show+, >> lfile)
	hedit (wcsnames.hdr, "DEC", s2, show+, >> lfile)

        list2 = "wcswcsdb.tmp"
	i = 1
        while (fscan (list2, swcdir, swcmcat, swcwcsdb, swcshort, swcwcshdr) != EOF) {
	    if ( access( "mefwcsproj.tmp" ) )
	        delete( "mefwcsproj.tmp" )
 	    ;
            # Set projection info from WCSDB file.
            proj = ""
            s1 = ""; match ("WCSDB", swccat) | scan (s2, s2, s2, s1)
            if (s1 != "") {
                match ("projection", s1) | scan (s2, s3)
                if (s3 == "zpx") {
                    proj = "zpx"
                    print (proj, > "mefwcsproj.tmp")
                    match ("projp", s1) | sort | uniq (>> "mefwcsproj.tmp")
                }
                ;
            }
            ;
	    i = i+1

            if ( access(swcwcsdb) ) {
                delete( swcwcsdb )
            }
            ;
	    # If wcs_wcsglobal = yes, then only the lower order corrections
	    # ("global") are made. If it is no, then the constraining grid
	    # of "fake" stars is combined with the real sources to determine
	    # an updated, higher-order correction. This means that if there
	    # are few stars available, the solution will tend to the 
	    # lower order solution set by the constraining grid. If a large
	    # number of stars is available, it will tend to the solution
	    # given by the data. In both cases, the WCS is determined on
	    # a per-extension basis.
            if (wcs_wcsglobal) {
                # Following should not be needed, but in the interest 
                # of being overly cautious it is included
	        if ( access( "wcswcsgrid.tmp" ) ) {
                    delete ("wcswcsgrid.tmp")
                }
                ;
	        match ("INDEF", swcmcat, > "wcswcsgrid.tmp")
		if ( proj == "zpx" ) {
		    ccmap( "wcswcsgrid.tmp", swcwcsdb, solution="wcs",
			lngref=x, latref=y, projection="mefwcsproj.tmp",
			xxorder=4, xyorder=4, yxorder=4, yyorder=4,
			xxterms="half", yxterms="half",
			pixsystem="logical", > tlfile )
		} else {
	            ccmap( "wcswcsgrid.tmp", swcwcsdb, solution="wcs",
	                lngref=x, latref=y, > tlfile )
		}
	        delete ("wcswcsgrid.tmp")
	    } else {
		if ( proj == "zpx" ) {
		    ccmap (swcmcat, swcwcsdb, solution="wcs",
			lngref=x, latref=y, projection="mefwcsproj.tmp",
			pixsystem="logical", > tlfile )
		} else {
	            ccmap (swcmcat, swcwcsdb, solution="wcs",
	                lngref=x, latref=y, > tlfile)
		}
	    }
            concatenate (tlfile) | tee (lfile)
	    # Redirect input parameters for wcswcs_ccmap_proc to file.
	    # wcswcs_ccmap_proc will read the parameters from file.
            # This is a way to avoid escaping the "!" in swcdir.
            if ( access( "wcswcs.inp" ) ) {
                delete( "wcswcs.inp" )
            }
            ;
            printf("%s %s %s\n", tlfile, swcdir, swcshort, >> "wcswcs.inp" )
            wcswcs_ccmap_proc( "wcswcs.inp" )
            delete( "wcswcs.inp" )
            cl < wcswcs_ccmap_proc.cl
	    delete ("wcswcs_ccmap_proc.cl")
	    # Get the scale and the rms from the header to calculate the 
	    # average over all extensions
	    s1 = swcdir // swcshort
	    hselect( s1, "DQWCCCXR,DQWCCCYR,DQWCCCXS,DQWCCCYS", yes ) |
	        scan( x1, x2, x3, x4 )
	    if (nscan()==4) {
	        axr = axr+x1
	        ayr = ayr+x2
	        axs = axs+x3
	        ays = ays+x4
                nsets = nsets+1
            }
	    ;
            delete( tlfile )
        }
        list2 = ""
        if (nsets>0) {
            axr = axr/nsets
            ayr = ayr/nsets
            axs = axs/nsets
            ays = ays/nsets
	    x1 = (axs + ays) / 2

	    # Update global header.
            hedit( wcsnames.hdr, "PIXSCALE", x1, add+, show+, >> lfile)
            hedit( wcsnames.hdr, "PIXSCAL1", axs, add+, show+, >> lfile)
            hedit( wcsnames.hdr, "PIXSCAL2", ays, add+, show+, >> lfile)
            hedit( wcsnames.hdr, "WCSXRMS", axr, add+, show+, >> lfile)
            hedit( wcsnames.hdr, "WCSYRMS", ayr, add+, show+, >> lfile)

	    # Update SIF headers.
	    print ("PIXSCALE ", x1, >> "wcsgwcs.hdr")
	    print ("PIXSCAL1 ", axs, >> "wcsgwcs.hdr")
	    print ("PIXSCAL2 ", ays, >> "wcsgwcs.hdr")
	    print ("WCSXRMS ", axr, >> "wcsgwcs.hdr")
	    print ("WCSYRMS ", ayr, >> "wcsgwcs.hdr")

	    # Update PMAS
    	    printf("%12.5e\n", axr ) | scan( cast )
            setkeyval( dm, "mefobjectimage", wcsnames.shortname,
	        "dqwccaxr", cast )
            printf("%12.5e\n", ayr ) | scan( cast )
            setkeyval( dm, "mefobjectimage", wcsnames.shortname,
                "dqwccayr", cast )
            printf("%12.5e\n", axs ) | scan( cast )
            setkeyval( dm, "mefobjectimage", wcsnames.shortname,
                "dqwccaxs", cast )
            printf("%12.5e\n", ays ) | scan( cast )
            setkeyval( dm, "mefobjectimage", wcsnames.shortname,
                "dqwccays", cast )
        } else 
	    sendmsg("WARNING", "Could not determine average WCS info", "", "DQ" )

    }
    ;
    # Update headers.
    globhdr = glob // "00.fits" # Note that glob already ends in a _
    if (status == 1) {
        # Trigger results back to SWC pipeline.
        list2 = "wcswcsdb.tmp"
        for (i=1; fscan(list2,swcdir,swcmcat,swcwcsdb,swcshort,swcwcshdr)!=EOF; i+=1) {
            if ( access( swcdir//swcwcshdr ) ) {
                delete( swcdir//swcwcshdr )
            }
            ;

	    # Update global parameters.
	    if (i == 1) {
		x1 = INDEF; thselect (swcmcat, "MAGZERO", yes) | scan (x1)
		if (isindef(x1)==NO)
		    printf ("MAGZERO %g\n", x1, >> "wcsgwcs.hdr")
		;
		x2 = INDEF; thselect (swcmcat, "MAGZSIG", yes) | scan (x2)
		if (isindef(x2)==NO)
		    printf ("MAGZSIG %g\n", x2, >> "wcsgwcs.hdr")
		;
		x3 = INDEF; thselect (swcmcat, "MAGZERR", yes) | scan (x3)
		if (isindef(x3)==NO)
		    printf ("MAGZERR %g\n", x3, >> "wcsgwcs.hdr")
		;
		x4 = INDEF; thselect (swcmcat, "MAGZNAV", yes) | scan (x4)
		if (isindef(x4)==NO)
		    printf ("MAGZNAV %d\n", x4, >> "wcsgwcs.hdr")
		;

		if (isindef(x1)==NO)
		    hedit (globhdr, "MAGZERO", x1, add+, show+, >> lfile)
		;
		if (isindef(x2)==NO)
		    hedit (globhdr, "MAGZSIG", x2, add+, show+, >> lfile)
		;
		if (isindef(x3)==NO)
		    hedit (globhdr, "MAGZERR", x3, add+, show+, >> lfile)
		;
		j = x4
		if (isindef(j)==NO)
		    hedit (globhdr, "MAGZNAV", j, add+, show+, >> lfile)
		;
		hedit (globhdr, "WCSCAL", "(1==1)", add+, show+, >> lfile)
	    }
	    ;
	    copy ("wcsgwcs.hdr", swcwcshdr)
	    x1 = INDEF; thselect (swcmcat, "MAGZERO1", yes) | scan (x1)
	    if (isindef(x1)==NO)
		printf ("MAGZERO1 %g\n", x1, >> swcwcshdr)
	    ;
	    x2 = INDEF; thselect (swcmcat, "MAGZSIG1", yes) | scan (x2)
	    if (isindef(x2)==NO)
		printf ("MAGZSIG1 %g\n", x2, >> swcwcshdr)
	    ;
	    x3 = INDEF; thselect (swcmcat, "MAGZERR1", yes) | scan (x3)
	    if (isindef(x3)==NO)
		printf ("MAGZERR1 %g\n", x3, >> swcwcshdr)
	    ;
	    j = INDEF; thselect (swcmcat, "MAGZNAV1", yes) | scan (j)
	    if (isindef(j)==NO)
		printf ("MAGZNAV1 %d\n", j, >> swcwcshdr)
	    ;

	    move (swcwcshdr//","//swcwcsdb, swcdir)
	    copy (swcmcat, swcdir)
        }
        list2 = ""
    } else
        hedit (globhdr, "WCSCAL", "(1==2)", add+, show+, >> lfile)
    delete( "wcsgwcs.hdr,wcswcs*.tmp" )

    ntot = ntot+1
    if ( status > 1 ) {
	nprob = nprob+1
	nhedit( globhdr, "QUALINFO", "WCS solution failed",
	    "Quality information", add+, ver- )

    }
    ;
}

# It is possible that for a (small) fraction the WCS solution fails, 
# e.g., because of occasional poor transparency because of clouds.
# If the fraction of WCS solution failures is too large, this likely
# is a different problem.

status = 1
x = nprob/ntot

if ( nprob == ntot ) {
    status = 2
    sendmsg( "ERROR", "WCS failed for all images", dataset, "PROC" )
} else {
    if ( x > 0.5 ) {
	status = 4
        sendmsg( "WARNING", "WCS failed for more that half of  the images",
	    dataset, "PROC" )
    } else {
        if ( nprob > 0 ) {
            status = 3
            printf( "WCS failure in %d out of %d images\n",
	        nprob, ntot ) | scan( line )
            sendmsg( "WARNING", line, dataset, "PROC" )
	}
	;
    }
}

logout( status )
