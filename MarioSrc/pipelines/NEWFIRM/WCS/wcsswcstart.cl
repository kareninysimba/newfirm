#!/bin/env pipecl

int	status
string	datadir, dataset, extname
string	ilist, image, indir, lfile, listoflists, olist, temp1, temp2

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' $5 2>&1 > $6; echo $? > $7"

# Set file and path names.
wcsnames (envget("OSF_DATASET"))
dataset = wcsnames.dataset

# Set filenames.
datadir = wcsnames.datadir
indir = wcsnames.indir
lfile = datadir // wcsnames.lfile
set (uparm = wcsnames.uparm)
set (pipedata = wcsnames.pipedata)

# Log start of processing.
printf ("\nWCSSWCSTART (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

ilist = indir//dataset//".wcs"

list = ilist

# Create a list of lists for call.cl to use to start the SWC pipelines
olist = indir//dataset//"ext.wcs"
while( fscan(list, s1) != EOF ) {
    # Create a shorter name version (only the extension)
    s2 = substr( s1, 1, strldx(".",s1)-1 )
    s3 = substr( s2, strlstr("sky",s2)+3, 999 )
    s2 = indir//s3//".sky"
    # Only copy the fits files
    match( ".fits", s1, >> s3 )
    # Add "wcsgcl", which is needed by the SWC pipeline
    pathnames( "wcsgcl", >> s3 )
    copy( s3, s2 )
    # Create full IRAF path name
    pathnames( s2, >> olist )
}
type( olist )
status = 1

# Call the SWC pipelines 
temp1 = "wcsswc1.tmp"
temp2 = "wcsswc2.tmp"
callpipe( "wcs", "swc", "split", olist, 1000, temp1, temp2 )
concat (temp2) | scan (status)
concat (temp1)
printf("Callpipe returned %d\n", status)

delete( olist )

logout( status )
