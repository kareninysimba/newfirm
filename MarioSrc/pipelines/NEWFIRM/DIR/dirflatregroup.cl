#!/bin/env pipecl

real   flat_onoff_timegap = 3600

int    nocno, pnocno, reject, pair, numon, numoff, nfltgrp
int    natstart, keeponflat
real   midpoint, lomax, himin, nocid, pnocid, level, plevel
real   exptime
string lostring, histring, thisfile, pfile, outfile, preflatlist
string exposure, exposures, filter, hdrfile, statfile, flatlist
string prefix, sublist, pairfile, lampstat, noctyp, preflatflist
string dataset
struct pline
struct *list2, *list3

proto
tables

# Get arguments.
if (fscan( cl.args, dataset, hdrfile, statfile ) < 3 ) {
    printf ("Usage: flatregroup.cl dataset hdrfile statfile\n")
    logout 0
}
;

# Clip extra NOCIDs from hdrfile, and sort by NOCID
if ( access( "flatregroup1.tmp" ) )
    delete( "flatregroup1.tmp" )
;
fields( hdrfile, "1-6" ) | sort( "STDIN", column=6, numeric+,
    >> "flatregroup1.tmp" )

# Match hdrfile and statfile
list = "flatregroup1.tmp"
if ( access( "flatregroup2.tmp" ) )
    delete( "flatregroup2.tmp" )
;
while ( fscan( list, line ) != EOF ) {
    # Get the file name
    print( line ) | scan( s1 )
    s2 = substr( s1, strldx("/",s1)+1, strldx("[",s1)-1 )
    # Find the matching median value (3rd field)  in the statistics
    x = INDEF
    match( s2, statfile ) | fields( "STDIN", fields="3" ) | scan( x )
    # Print the line if a match is found, but drop NOCDHS
    if ( !isindef(x) ) {
	printf( "%s %f\n", line, x ) |
	    fields( "STDIN", fields="1,3-", >> "flatregroup2.tmp" )
    }
    ;
}
list = ""

# Create a list of all filters
if ( access( "filters.list" ) )
    delete( "filters.list" )
;
fields( "flatregroup2.tmp", "3" ) | sort( "STDIN" ) | unique > "filters.list"

# Set up some variables
nfltgrp = 1
preflatlist = "pre_flist.tmp"
#prefix = dataset//"_"
prefix = "regroup_"

# Make sure a new preflatlist is started
if ( access( preflatlist ) ) {
    delete( preflatlist )
}
;

# Loop over all filters
list = "filters.list"
while ( fscan( list, filter ) != EOF ) {

    # Select the entries for the current filter only
    if ( access( "flatregroup3.tmp" ) )
	delete( "flatregroup3.tmp" )
    ;
    list2 = "flatregroup2.tmp"
    while ( fscan( list2, line ) != EOF ) {
        print( line ) | fields( "STDIN", "3" ) | scan( s2 )
        if ( s2 == filter ) {
	    print( line, >> "flatregroup3.tmp" )
        }
    }

    # Create a list of all exposure times
    exposures = "exposures."//filter//".list"
    if ( access( exposures ) )
        delete( exposures )
    ;
    fields( "flatregroup3.tmp", 2 ) | sort( "STDIN", numeric+ ) |
	unique( > exposures )

    # Now loop over all exposure times
    list2 = exposures
    while( fscan( list2, exposure ) != EOF ) {

        # Remember nfltgrp at start at this filter/exposure time
        # combination in case the on-only list is deleted.
        natstart = nfltgrp
	# Clear the reject flag
	reject = 0

        # Select the entries for the current exposure time only
        if ( access( "flatregroup4.tmp" ) )
            delete( "flatregroup4.tmp" )
        ;
        list3 = "flatregroup3.tmp"
        while ( fscan( list3, line ) != EOF ) {
            print( line ) | fields( "STDIN", "2" ) | scan( s2 )
            if ( s2 == exposure ) {
	        print( line, >> "flatregroup4.tmp" )
            }
        }
	list3 = ""
        
	# Determine which flats were taken with the lamp on, and
	# which with the lamp off. This is done by looking for the
	# midpoint between minimum and maximum, and then dividing
	# into two groups, one below and one above the minumum.
	# Next, for each of these distrubutions the other extreme
	# is determined. If this is sufficiently far from the 
	# midpoint, then there are two peaks (one on, one off).
	# If the other extreme is very close to the midpoint,
	# apparently there is only one peak. In this case, it is
	# assumed that the lamp was on for all flats. The average
	# or median is not used because it is unknown how many
	# on and how many off flats have been taken.

	# Get the column with the statistics
        if ( access( "flatregroup5.tmp" ) )
            delete( "flatregroup5.tmp" )
        ;
	fields( "flatregroup4.tmp", "6", > "flatregroup5.tmp" )
	# Determine the midpoint
	tstat( "flatregroup5.tmp", "c1", >> "/dev/null" )
	midpoint = (tstat.vmin+tstat.vmax)/2
	# Determine the maximum of the low wing
	tstat( "flatregroup5.tmp", "c1", highlim=midpoint, >> "/dev/null" )
	lomax = tstat.vmax
	# Determine the minimum of the high wing
	tstat( "flatregroup5.tmp", "c1", lowlim=midpoint, >> "/dev/null" )
	himin = tstat.vmin
	# If the difference between himin and lomax is sufficiently
	# large, the low wing are off flats
	if ( himin-lomax>sfl_mincounts ) {

	    # Some flats are taken with lamps on, others with lamps
	    # off. The next step is to identify pairs. To keep matters
	    # simple for now, it is assumed that on and offs are
	    # observed in order, i.e., an on is followed by an off, or
	    # an off is followed by an on. Non-paired sequences are
	    # ignored. In the following sequence: 1ON 2OFF 3ON 4OFF
	    # 5OFF 6ON 7ON 8ON 9OFF there are four valid pairs: 12,
	    # 34, 56, and 89. 7 is a loose sequence and will be
	    # ignored.  Looking for a matching pair is restarted if
	    # there is a time gap between sequences of more than
	    # flat_onoff_timegap seconds. If the number of flats in an
	    # on and off sequence is off by more than a factor of two
	    # with respect to each other, the pair is rejected. If
	    # there are on/off changes within a sequence, that
	    # sequence is rejected.

	    # Read the contents of the first list
	    list3 = "flatregroup4.tmp"
	    i = fscan( list3, pline )
	    # Read the fields from the line
	    print( pline ) | scan( pfile, exptime, s1, pnocno, pnocid, plevel )
	    # Delete existing on/off files
	    if ( access( "onfile" ) )
	        delete( "onfile" )
	    ;
	    if ( access( "offfile" ) )
	        delete( "offfile" )
	    ;
	    # Determine whether the lamp is on or off
	    if ( plevel<midpoint ) {
		outfile = "offfile"
		lampstat = "B"
	    } else {
	        outfile = "onfile"
		lampstat = "A"
	    }
	    touch( outfile )
	    # Print the current entry to file
	    printf( "%s %s\n", pfile, lampstat, >> outfile )
	    keeponflat = 1
	    # Make sure the list of the current exptime/filter combination
	    # is empty.
	    if ( access( "thislist" ) ) {
	        delete( "thislist" )
            }
	    ;
	    touch( "thislist" )
	    # Loop over all entries for this exptime/filter combination
	    while ( fscan( list3, line ) != EOF ) {
print( line )
		
		# Read the relevant fields
		print( line ) | scan( thisfile, s1, s1, nocno, nocid, level )

		if (nocno<=pnocno) {
print( "Start of a new sequence" )
		    # A new sequence begins. Find out whether this is
		    # completes a pair, the start of a new pair, or
		    # something else, and handle each case accordingly:

		    if ( access("onfile") && access("offfile") ) {
print( "onfile and offfile exist" )
			# On and off both exists, so this completes
			# a pair.
			# Delete all previous on-only if needed, and
			# reset the counter.
			if ( keeponflat == 1 ) {
			    keeponflat = 0
			    if ( access( "thislist" ) ) {
				imdel( "@thislist" )
				delete( "thislist" )
			        nfltgrp = natstart
			    }
			    ;
			    touch( "thislist" )
print( "Deleting all on-only" )
			}
			;
			# Check whether the current pair has similar
			# numbers of on and off flats.
			count( "onfile" ) | scan( numon )
			count( "offfile" ) | scan( numoff )
			x = numon/numoff
			if ( (x>0.5) && (x<2) ) {
			    # Add the pair to the pair list
print( "Adding a pair to the list" )
			    printf( "%04s\n", nfltgrp ) | scan( s2 )
			    pairfile = prefix // s2 // ".tmp"
			    concat( "onfile,offfile", > pairfile )
			    print( pairfile, >> "thislist" )
			    # Increase the flat group counter
			    nfltgrp = nfltgrp+1
		        } else {
			    # Too few of either on of off, so pair
			    # is rejected
		        }
			delete( "onfile,offfile" )
		    } else {
			if ( access("onfile") ) { # On exists, no off
print( "onfile exists" )
			    if ( level > midpoint ) {
				# The current image is also an on. Keep
				# the previous on-only, as long as no
				# pairs are available
				if ( keeponflat == 1 ) {
				    # Add it to the flat list
				    printf( "%04s\n", nfltgrp ) | scan( s2 )
				    s2 = prefix // s2 // ".tmp"
				    rename( "onfile", s2 )
				    print( s2, >> "thislist" )
				    # Increase the flat group counter
				    nfltgrp = nfltgrp+1
print( "Adding on-only to the list" )
				} else {
				    # Delete the previous list ...
				    delete( "onfile" )
print( "Rejecting previous on-only" )
				}
			    }
			    ;
			} else {
			    if ( access("offfile") ) { # Off exists, no on
print( "offfile exists" )
				if ( level < midpoint ) {
				    # The current iamge is also an off.
				    # Delete the previous list.
				    delete( "offfile" )
print( "Rejecting previous off-only" )
				}
				;
			    } else {
				# Neither on nor off exists
			    }
		        }
		    }
		    
		    # Start the new sequence. This means that a check
		    # needs to be made to make sure the previous and
		    # the current sequence are eligible to be a pair.
		    # Specifically, this means that the time gap between
		    # the current and the previous exposure should be
		    # less than 5 minutes.

		    if ( nocid > pnocid+(flat_onoff_timegap+exptime)/86400 ) {
			# The time gap with respect to the first half
			# of this pair is too large. The previous
			# sequence is rejected, unless it is an on
			# flat and no valid pairs have been detected.
			if ( access("onfile") ) {
			    if ( (keeponflat==1) && (level>midpoint) ) {
				# Keep this list
			    } else {
print( "Large gap in time, deleting previous onfile" )
			        delete( "onfile" )
			    }
			} else {
			    if ( access( "offfile" ) ) {
print( "Large gap in time, deleting previous offfile" )
				delete( "offfile" )
			    }
			    ;
			}
		    }


		    # Set the name for the next sequence of flats
                    if ( level<midpoint ) {
			outfile = "offfile"
			lampstat = "B"
		    } else {
			outfile = "onfile"
			lampstat = "A"
		    }
		    # Print the entry to file
		    printf( "%s %s\n", thisfile, lampstat, >> outfile )

		    # Clear the reject flag
		    reject = 0

	        } else {
		    
		    # This sequence seems to continue, but make some
		    # more checks to make sure. There should not be
		    # a large gap in nocid, nor should the level change
		    # from on to off or the other way around.

print( "Continuing this sequence" )
                    if ( (outfile=="offfile") && (level>midpoint) ) {
			print "Found on in off sequence"
			reject = 1
		    }
                    ;
                    if ( (outfile=="onfile") && (level<midpoint) ) {
			print "Found off in on sequence"
			reject = 1
		    }
                    if ( nocid > pnocid+(flat_onoff_timegap+exptime)/86400 ) {
			print "Found too large of a time gap"
		        reject = 1
		    }
		    if ( reject == 1 ) {
print( "Rejecting the ongoing sequence" )
			if ( access( outfile ) )
			    delete( outfile )
			;
		    } else {
			# Print the current entry to file
		        printf( "%s %s\n", thisfile, lampstat, >> outfile )
		    }

	        }

	        # Set the new values for the variables describing
	        # the previous entry
	        pfile = thisfile
		pnocno = nocno
		pnocid = nocid
		plevel = level
	    }
	    list3 = ""

print( "Start of post loop" )
	    # Clean up the leftovers
	    if ( access( "onfile" ) ) {
		if ( access( "offfile" ) ) {
		    # A pair exists
		    # Delete previous on-only if needed
		    if ( keeponflat == 1 ) {
			if ( access( "thislist" ) ) {
			    imdel( "@thislist" )
			    delete( "thislist" )
			    nfltgrp = natstart
print( "Post loop: deleting all on-only" )
		        }
		    }
		    ;
		    # Compare numbers of on and off flats
		    count( "onfile" ) | scan( numon )
		    count( "offfile" ) | scan( numoff )
		    x = numon/numoff
		    if ( (x>0.5) && (x<2) ) {
			# Add the pair to the pair list
print( "Post loop: adding a pair to the list" )
                        printf( "%04s\n", nfltgrp ) | scan( s2 )
			pairfile = prefix // s2 // ".tmp"
			concat( "onfile,offfile", > pairfile )
			print( pairfile, >> "thislist" )
			# Increase the flat group counter
			nfltgrp = nfltgrp+1
		    } else {
			# Too few of either on of off, so pair
			# is rejected
		    }
		    delete( "onfile,offfile" )
		} else {
		    # An on-only exists
		    # Keep the previous on-only, as long as 
		    # pairs are available
		    if ( keeponflat == 1 ) {
			# Add it to the flat list
			printf( "%04s\n", nfltgrp ) | scan( s2 )
			s2 = prefix // s2 // ".tmp"
			rename( "onfile", s2 )
			print( s2, >> "thislist" )
			# Increase the flat group counter
			nfltgrp = nfltgrp+1
print( "Post loop: adding on-only to the list" )
		    } else {
			# Delete the previous list ...
			delete( "onfile" )
print( "Post loop: rejecting previous on-only" )
	            }
		}
	    } else {
		if ( access( "offfile" ) ) {
		    delete( "offfile" )
		}
		;
	    }
	    
	    
        } else {

            # No need to look for on/off pairs, all flats are with
	    # lamps on.
print( "All flats are with lamps on" )
	    
	    # Read the contents of the first list
	    list3 = "flatregroup4.tmp"
	    i = fscan( list3, pline )
	    # Read the fields from the line
	    print( pline ) | scan( pfile, exptime, s1, pnocno, pnocid, plevel )
	    outfile = "onfile"
	    touch( outfile )
	    # Print the current entry to file
	    printf( "%s A\n", pfile, >> outfile )
	    # Make sure the list of the current exptime/filter combination
	    # is empty.
	    if ( access( "thislist" ) ) {
	        delete( "thislist" )
            }
	    ;
	    touch( "thislist" )
	    # Loop over all entries for this exptime/filter combination
	    while ( fscan( list3, line ) != EOF ) {
print( line )
		# Read the relevant fields
		print( line ) | scan( thisfile, s1, s1, nocno, nocid, level )

		if (nocno<=pnocno) {
print( "Start of a new sequence" )
 		    # Add it to the flat list
                    printf( "%04s\n", nfltgrp ) | scan( s2 )
		    s2 = prefix // s2 // ".tmp"
		    rename( "onfile", s2 )
		    print( s2, >> "thislist" )
		    # Increase the flat group counter
		    nfltgrp = nfltgrp+1
	        } else {
print( "Continuing this sequence" )
		    printf( "%s A\n", thisfile, >> outfile )
	        }
	        # Set the new values for the variables describing
	        # the previous entry
	        pfile = thisfile
		pnocno = nocno
		pnocid = nocid
		plevel = level
                   
            }
	    list3 = ""
		
	    # Clean up the leftovers
	    if ( access( "onfile" ) ) {
		printf( "%04s\n", nfltgrp ) | scan( s2 )
		s2 = prefix // s2 // ".tmp"
		rename( "onfile", s2 )
		print( s2, >> "thislist" )
		# Increase the flat group counter
		nfltgrp = nfltgrp+1
	    }
	    
        }

	touch( "thislist" )
	concat( "thislist", >> preflatlist )
	delete( "thislist" )

    } # End loop over all exposure times
    list2 = ""
    
} # End loop over all filters
list = ""

# Now loop over all images in all lists, and update the headers
# so that the pipeline can recognize the new pairs. For lists
# that contain both A and B for lampstat, the noctyp will be
# set to DFLATS. If lampstat only has value A, noctyp will be
# set to DFLATON. There should be no files with only lampstat=B
# (i.e., DFLATOFF only).

# Make sure a new flatlist is started
flatlist = "end_flist.tmp"
if ( access( flatlist ) ) {
    delete( flatlist )
}
;
touch( flatlist )

list = preflatlist
while ( fscan( list, s1 ) != EOF ) {

    # See how many unique values of lampstat are present
    # in the input list. If only one value is present, make
    # sure it is A.
    if ( access( "flatregroup6.tmp" ) )
	delete( "flatregroup6.tmp" )
    ;
    fields( s1, "2" ) | sort( "STDIN" ) | unique > "flatregroup6.tmp"
    count( "flatregroup6.tmp" ) | scan( i )
    if ( i == 2 ) {
	# Two values are present, so this is a sequence. However,
	# NOCTYP is given the name DFLATONOFF, not DFLATS, do
	# distinguish between a pipeline-identified sequence
	# and a observer-specified sequence. The DFLATONOFF
	# also matches against the DFLATO in dirgroup2.cl, so
	# that, upon reprocessing, the new sequences are still
	# recoginized as non-DFLATS sequences.
	noctyp = "DFLATONOFF"
    } else {
	if ( i == 1 ) {
	    concat( "flatregroup6.tmp" ) | scan( s2 )
	    if ( s2 == "A" ) {
		noctyp = "DFLATON"
	    } else {
		# Whether B or something else, this is not valid
		next
	    }
	} else {
	    # Whether 0 or more than 2, this is not valid
	    next
	}
    }

    # Add the current list to the final list of lists
    print( s1, >> flatlist )

    # Loop over all entries in the current list, update the
    # headers, and write out the final list
    if ( access( "flatregroup7.tmp" ) ) 
        delete( "flatregroup7.tmp" )
    ;
    list2 = s1
    while ( fscan( list2, thisfile, lampstat ) != EOF ) {
	# Add to the global header, so replace [1] with [0]
	s2 = substr( thisfile, 1, strldx("[",thisfile)-1 ) // "[0]"
	# Store the original NOCTYP in ONOCTYP
	hedit( s2, "ONOCTYP", "(NOCTYP)", add+, ver- )
	# Set NOCTYP and LAMPSTAT according to the sequence
	# info derived here. The old LAMPSTAT is not kept,
	# because the new LAMPSTAT is more informative.
        hedit( s2, "NOCTYP", noctyp, add+, update+, ver- )
        hedit( s2, "LAMPSTAT", lampstat, add+, update+, ver- )
	# Remove the subset and add .fits
	s2 = substr( thisfile, 1, strldx("[",thisfile)-1 ) // ".fits"
	# Add this to the final list
	print( s2, >> "flatregroup7.tmp" )
    }
    list2 = ""
    delete( s1 )
    rename( "flatregroup7.tmp", s1 )

}
list = ""	

print "End of dirflatregroup.cl"
logout 1
