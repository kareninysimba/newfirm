#!/bin/env pipecl
#
# DIRGROUP3 -- Set list of objects for call to FTR pipeline.
#
# 	STATUS	MEANING
#	0	Untrapped or trapped error
#	1	Found data
#	2	Found no data

string dataset, indir, datadir, inlist, lfile, olist, pattern

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".dir"
olist = datadir // "olist.tmp"

# Log start of processing.
printf ("\nDIRGROUP3 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Find list of objects or return no data status.
if (access (olist)) {
    rename (olist, inlist)
    type (inlist)
    type ("@"//inlist)
    logout 1
} else
    logout 2
