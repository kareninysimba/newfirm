#!/bin/env pipecl
#
# DIRINSPECT

int    status
string dataset, indir, datadir, inlist, lfile, olist, osf_flag, class

servers
task $mkreview = "$mkndpreview --cal $1"

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".dir"
olist = datadir // "olist.tmp"

# Log start of processing.
printf ("\nDIRINSPECT (%s): ", dataset) | tee (lfile)
time | tee (lfile)

osf_flag = envget ("OSF_FLAG")

# This modules is called twice. In the first pass, it creates the
# review page. In the second, it processes the output of the review.

status = 0

if ( osf_flag == "_" ) {
    # Create the review page
    mkreview( dataset )
    status = 1
}
;

if ( osf_flag == "a" ) {
    # Process the results of the review
    s1 = "/tmp/caldel_"//dataset
    if ( access( s1 ) ) {
	list = s1
	while ( fscan( list, s2 ) != EOF ) {
	    # Determine calibration type
	    s3 = substr( s2, stridx("-",s2)+1, 999 )
	    if ( strlen(s3) <= 4 ) {
	        class = "dark"
	    } else {
		class = "dflat"
	    }
	    delcal( cm, class=class, value="like "//s2//"%",
		datatype="image" )
	}
    }
    ;
    status = 2
}
;

if ( status == 0 ) {
    sendmsg( ERROR, "OSF_FLAG value not recognized", dataset, "PROC" )
}
;

logout( status )
