#!/bin/env pipecl

int	status
string	datadir, dataset, extname
string	image, indir, lfile, listoflists, outfile
string	temp1, temp2

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set file and path names.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset

# Set filenames.
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Log start of processing.
printf ("\nDIRPER1 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

if (sciencepipeline) {

    # Collect all the return files from the persistence (PER)
    # pipeline and create a single list of persistent masks.
    match( "*per.pl$", "*.per", print_file_names-,
        >> "persistence_masks.list" )
    status = 1

} else {

    # No persistence masking is needed, so exit normally
    status = 1

}

logout( status )
