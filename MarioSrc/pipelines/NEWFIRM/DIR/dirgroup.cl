#!/bin/env pipecl
#
# DIRGROUP -- Group by observation types and sequence number.

int	l
bool	verb = yes
string	dataset, indir, datadir, id, dataset1
string	inlist, lfile, dlist, flist, olist, fname
string	seqnum, nocdhs, grp, dtcaldat, sname, s4
struct	filter
struct	*fd

int	ominseq = 2	# Minimum number in object sequence

# Define packages and tasks.
task setseqid = "NHPPS_PIPESRC$/NEWFIRM/DIR/setseqid.cl"
util
proto
images
servers
unlearn setseqid
cache	setseqid

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".dir"
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Set base dataset name (without time stamp), used to set
# time-independent ID used to store grouping names in database
# and retrieve that group name if the data are reprocessed.
dataset1 = dataset
if (strldx("_",dataset1)>0)
    dataset1 = substr (dataset1, 1, strldx("_",dataset1)-1)
;

#delete ("dirgroup*.tmp,[dfo]list.tmp")
delete ("*.tmp")
dlist = "dlist.tmp"
flist = "flist.tmp"
olist = "olist.tmp"

# Log start of processing.
printf ("\nDIRGROUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Append "[0]" to all files in the list.  We need to sort the files because
# the order retrieved from the archive may not be related to the actual
# order of the exposures.
list = inlist
while (fscan( list, fname) != EOF)
    print (fname//"[0]", >> "dirgroup1.tmp")

# Set the sequence IDs (if we trust the KTM sequences we could skip
# setting the IDs). If a previous sequence ID is available from the
# database, use that.
if (YES) {
    hselect ("@dirgroup1.tmp", "$I,NOCID", yes) |
	sort (col=2, num+) | fields ("STDIN", "1", > "dirgroup1.tmp")
    print (dataset) | translit ("STDIN", "_", " ") | scan (s2, s3)
    list = "dirgroup1.tmp"
    while (fscan (list, fname) != EOF) {
	hselect (fname, "NOCNO,NOCTOT", yes) | scan (i, j)
	if (nscan() < 2)
	    next
	;

	# Use previous sequence ID and number if available
	# Define id to look up previous data.
	id = dataset1 // "_" // substr( fname, strldx("/",fname)+1,
	    strlstr(".fits",fname)-1 )
	# See whether sequence name is available
	getkeyval( dm, "obsdataproduct", id, "URL" ) | scan( sname )
	if (sname != "None") {
	    printf( "Got sequence name %s from DB for %s\n", sname, id )
	    # Sequence name available from database, so use it
	    l = stridx( "_", sname )
	    s1 = substr( sname, 1, l-1 )
	    s4 = substr( sname, l+1, 1000 )
	} else {
	    printf( "No sequence name in DB for %s, setting new one:\n",
		id )
	    setseqid (s2, s3, i, j, "none")
	    s1 = setseqid.seqnum
	    s4 = setseqid.seqid
	    # Store seqid and seqnum in a single string
	    sname = s1//"_"//s4
	    # Store this string in database for possible reprocessing
	    setkeyval( dm, "obsdataproduct", id, "URL", sname )
	}

	hedit (fname, "SEQID", s4, add+, show=verb)
	hedit (fname, "SEQNUM", s1, add+, show-)
	print (fname, >> "dirgroup.tmp")
    }
    list = ""; delete ("dirgroup1.tmp")
} else
    rename ("dirgroup1.tmp", "dirgroup.tmp")

# Separate by obstype.
hselect ("@dirgroup.tmp", "$I", "(obstype='dark'||obstype='zero')", > "z.tmp")
hselect ("@dirgroup.tmp", "$I", "(obstype='flat')", > "f.tmp")
hselect ("@dirgroup.tmp", "$I", "(obstype='object'||obstype='sky')", > "o.tmp")
delete ("dirgroup.tmp")

print ("\nZeros/Darks:\n", >> lfile)
concat ("z.tmp", >> lfile)
print ("\nFlats:\n", >> lfile)
concat ("f.tmp", >> lfile)
print ("\nObjects/Skies:\n", >> lfile)
concat ("o.tmp", >> lfile)

# Skip calibration processing as requested
if ( do_dark == NO ) {
    delete( "z.tmp" )
    touch( "z.tmp" )
}
;
if ( do_flat == NO ) {
    delete( "f.tmp" )
    touch( "f.tmp" )
}
;

# Separate darks into sequence lists.
hselect ("@z.tmp", "$I,seqnum", yes) | sort (col=2, > "dirgroup.tmp")
delete ("z.tmp")
touch( dlist )
s1 = ""; list = "dirgroup.tmp"
while (fscan (list, fname, i) != EOF) {
    printf ("%04d\n", i) | scan (seqnum)
    if (seqnum != s1) {
	print (datadir // dataset // "_" // seqnum // ".tmp", >> dlist)
	s1 = seqnum
    }
    ;

    # Add to sequence file ...
    fname = substr (fname, 1, stridx("[",fname)-1)
    print (fname, >> dataset//"_"//seqnum//".tmp")
    # ... and the log
    printf ("%s -> %s\n", fname, dataset//"_"//seqnum//".tmp", >> lfile)

}
list = ""; delete ("dirgroup.tmp")
if (access(dlist)) {
    sort (dlist) | uniq (> "dirgroup.tmp")
    rename ("dirgroup.tmp", dlist)
}
;

# Separate flats into sequence lists.
hselect ("@f.tmp", "$I,seqnum,nocdhs", yes) | sort (col=2, > "dirgroup.tmp")
delete ("f.tmp")
touch( flist )
s1 = ""; list = "dirgroup.tmp"
while (fscan (list, fname, i, nocdhs) != EOF) {
    nocdhs = strupr (nocdhs)
    if (nocdhs=="SFLAT" || nocdhs=="SFLATO" || nocdhs=="TFLAT")
        next
    ;
    printf ("%04d\n", i) | scan (seqnum)
    if (seqnum != s1) {
	print (datadir // dataset // "_" // seqnum // ".tmp", >> flist)
	s1 = seqnum
    }
    ;

    # Add to sequence file ...
    fname = substr (fname, 1, stridx("[",fname)-1)
    print (fname, >> dataset//"_"//seqnum//".tmp")
    # ... and the log
    printf ("%s -> %s\n", fname, dataset//"_"//seqnum//".tmp", >> lfile)
}
list = ""; delete ("dirgroup.tmp")
if (access(flist)) {
    sort (flist) | uniq (> "dirgroup.tmp")
    rename ("dirgroup.tmp", flist)
}
;

# Separate objects into sequence lists.  The sequence lists may be
# merged by a group name.  A group name is a concatentation of all the
# grouping critera.  For reasons of possible large sequences we
# currently make the group name include the calendar date.  To allow
# sequences to cross nights this would be changed.

hselect ("@o.tmp", "$I,seqnum,nocdhs,rspgrp,dtcaldat,filter", yes) |
    sort (col=2, > "dirgroup.tmp")
delete ("o.tmp")

s1 = ""; s2 = ""; grp = ""
list = "dirgroup.tmp"
touch( "dirgroup1.tmp" )
touch( olist )
while (fscan (list, fname, i, nocdhs, grp, dtcaldat, filter) != EOF) {

    # Skip data that should not be processed by the pipeline
    nocdhs = strupr (nocdhs)
    if (nocdhs=="FOCUS" || nocdhs=="TEST" || nocdhs=="SFLATO")
        next
    ;

    # Ensure the filter name read from the header is known, i.e., present
    # in the subset file
    s3=""; match (filter, "pipedata$subsets.dat") |
        fields ("STDIN", "2") | scan (s3)
    if (s3 == "") {
	sendmsg ("ERROR", "Filter not in subset file", filter, "VRFY")
	next
    }
    ;
    filter = s3
    
    printf( "%04d%s\n", i, filter ) | scan( seqnum )
    
    # Reset counter and reference strings when sequence name changes
    if (seqnum != s1) {
	s1 = seqnum
	s2 = ""
	j = 0
    }
    ;

    # Add the sequence name once to the output list
    j += 1
    if (j == 1)
	print (datadir // dataset // "_" // seqnum // ".tmp", >> olist)
    ;

    # Add to sequence file ...
    fname = substr (fname, 1, stridx("[",fname)-1)
    print (fname, >> dataset//"_"//seqnum//".tmp")
    # ... and the log
    printf ("%s -> %s\n", fname, dataset//"_"//seqnum//".tmp", >> lfile)

    # Update the group name based on grp (i.e., rspgrp per fscan above),
    # dtcaldat, and filter
    printf ("%s%s%s\n", grp, dtcaldat, filter) |
        translit ("STDIN", " \t", delete+) | scan (grp)
    # Add new group names to grouping file, which is used if observing
    # sequences are to be merged.  This is only done if dir_stkgrp is
    # set to yes. 
    if (s2 != grp && dir_stkgrp) {
        s2 = grp
	printf ("%s %s\n", grp, dataset//"_"//seqnum//".tmp",
	    >> "dirgroup1.tmp")
    }
    ;
}
list = ""; delete ("dirgroup.tmp")

# Merge sequences with the same group.  Avoid making overly large sequences.
# 120 is a nice number because it has so many factors (1x2x3x4x5=120).
# Merging is only done if dir_stkgrp is set to yes directly above.
if (access("dirgroup1.tmp")) {
    sort ("dirgroup1.tmp", > "dirgroup.tmp"); delete ("dirgroup1.tmp")
    list = "dirgroup.tmp"
    i = fscan (list, grp, fname)
    count (fname) | scan (j)
    printf ("Group %s (%d)\n", fname, j) | tee (lfile)
    while (fscan (list, s2, s1) != EOF) {
	if (s1 == fname || access(s1)==NO)
	    next
	;
	count (s1) | scan (k)
        if (s2 == grp) {
	    count (fname) | scan (j)
	    l = j + k
	    if (l <= 120) {
		printf ("  Merge %s (%d) -> %s (%d)\n",
		    s1, k, fname, l) | tee (lfile)
		concat (s1, fname, append+)
		delete (s1)
	    } else {
		printf ("Group %s (%d)\n", s1, k) | tee (lfile)
	        fname = s1
	    }

	} else {
	    printf ("Group %s (%d)\n", s1, k) | tee (lfile)
	    fname = s1
	    grp = s2
	}
    }
    list = ""; delete ("dirgroup.tmp")

    rename (olist, "dirgroup.tmp")
    touch( olist )
    list = "dirgroup.tmp"
    while (fscan (list, fname) != EOF) {
        if (access(fname)) {
	    count( fname ) | scan( j )
	    if ( j > ominseq ) {
	        print (fname, >> olist)
	    }
	    ;
	}
	;
    }
    list = ""; delete ("dirgroup.tmp")
}
;

if (access(olist)) {
    sort (olist) | uniq (> "dirgroup.tmp")
    rename ("dirgroup.tmp", olist)
}
;

#logout 99
logout 1
