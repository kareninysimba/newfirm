#!/bin/env pipecl

int	status = 1
bool	do_per
string	datadir, dataset, extname
string	image, indir, ifile, lfile, outfile
string	temp1, temp2, listoflists
struct	*fd1

# Tasks and packages.
utilities
mscred
servers
task mscextensions = mscsrc$x_mscred.e
cache mscred
task $splitc = "$!splitc.cl $1 $2 $3 $4 2>&1 > $5; echo $? > $6"
task $callpipe = "$!call.cl $1 $2 $3 '$4' $5 2>&1 > $6; echo $? > $7"

# Set file and path names.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset

# Set filenames.
indir = names.indir
datadir = names.datadir
ifile = indir // dataset // ".dir"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Log start of processing.
printf ("\nDIRPER1 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Determine whether to do persistence.  Don't do it if not in sciencepipeline.
# Check if there is already at least one persistence mask and assume
# we can skip making the persistence masks.  This is a hack and it
# is up to other software to make sure the persistence files are cleaned
# when the pipeline is started or restarted.

do_per = do_persistence
if ( sciencepipeline == NO )
    do_per = NO
;

if (do_per) {
    s1 = dataset
    i = strldx ("_", s1)
    if (i > 0)
        s1 = substr (s1, 1, i-1)
    ;
    getcal( "", "masks", cm, "", obstype="", detector="", imageid="",
        filter="", exptime="", mjd="50000", match=s1//"%", >& "dev$null")
    if (getcal.statcode == 0) {
        if (access(getcal.value))
	    do_per = NO
	;
	print "Previous persistent masks found - skipping PER pipeline"
    }
    ;
}
;

if (do_per) {

    # TODO make this module such that it can split the input list
    # (after creating the individual lists for each of the extensions)
    # in N lists (keeping persist_dtime and persist_nreads into
    # account). The behavior could be made configurable through a
    # command line argument to this module, e.g.,
    # "dirper1.cl 1" means one list would be sent to the PER pipeline
    # "dirper1.cl 8" means that the input list will be broken up into
    #     8 lists of more or less equal length, and these lists will
    #     be farmed out across nodes.
    # In this scheme, there needs to be a module that accumulates the 
    # results and merges these into one list, which will be sent to 
    # downstream modules so these can find the appropriate persistence
    # mask.
    # Another issue that needs to be taken care of is avoiding creation
    # of duplicate bright object masks and persistence masks. If only
    # 1 list is given, the PER pipeline will have to treat the first
    # few images in a sequence in a special way (A), because no earlier
    # images are available. For all images later in the sequence, such
    # earlier images are available, even when the input list is split
    # up into smaller lists. One way to solve this is to supply those
    # earlier images in the input list as well (B), while at the same time
    # letting the PER pipeline know how the first few images need to 
    # be processed (i.e., method A vs. method B). This could be
    # accomplished by, e.g., a special first entry in the list that tells the
    # called module what to do, or by a special file that is created
    # in the input directory of the pipeline that is called.

    # Initially the farming out across nodes was done with splitc.cl.
    # However, this was very slow, primarily because of writing out
    # all the individual files. Instead, this module now creates four
    # lists, one for each extension, which are sent to the PER pipeline.

    # Remove existing mask entries from the database. This is needed
    # for pipeline restarts where the database has not been cleaned.
    # See also perbopem.cl for notes on reusing existing masks.
    # TODO: verify whether the starting % is needed.
    delcal( cm, class="masks", value="like %"//dataset//"%" )

    # Extract image extensions.
    s2 = ""
    if ( access( "pipedata$extpat.dat" ) )
        head( "pipedata$extpat.dat" ) | scan( s2 )
    ;
    head( ifile ) | scan (s1)
    i = strlstr( ".fits", s1 )
    if ( i > 0 )
        s1 = substr( s1, 1, i-1 )
    ;
    mscextensions( s1, extname=s2, lindex-, lname+, lver- ) |
        translit( "STDIN", "[]", " ", > "split.tmp" )
    
    # Set the list-of-lists file
    listoflists = indir // dataset // "per.lol"

    # Loop over all extensions
    list = "split.tmp"
    while ( fscan( list, s1, extname ) != EOF ) {
        fd1 = ifile
	# Loop over all entries in the ifile
        while ( fscan( fd1, s1 ) != EOF ) {
            # Remove the trailing .fits
            s2 = substr( s1, 1, strlstr(".fits",s1)-1 )
            # Write this extension to the extension list
            print( s2//"["//extname//"]", >> extname )
        }
        # Write the full path name of this extension list to the list of lists
	pathname( extname ) | scan( s2 )
	print( s2, >> listoflists )
        fd1 = ""
    }
    list = ""

    # Call the PER pipelines 
    temp1 = "dirper1.tmp"
    temp2 = "dirper2.tmp"
    call( "dir", "per", "split", listoflists, 1000, temp1, temp2 )
    concat (temp2) | scan (status)
    concat (temp1)
    printf("Callpipe returned %d\n", status)
    delete( "listoflists,temp1,temp2" )

}
;

logout( status )
