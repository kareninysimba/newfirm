#!/bin/env pipecl
#
# DIRSPECIAL -- Apply special setup and remediation actions.
#
# This just calls dataset tagged scripts in PipeConfig$.  This could be
# replaced by a csh script and call any special command but currently this
# is a CL script that runs an IRAF script in the same CL session.

#logout 0 # Temporary halt to be able to edit the input list of a process, to eliminate non-desired files

int	status = 1
string	dataset, datadir, ifile, lfile

# Packages and tasks.
images
task special = special.cl


# Set paths and files.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".dir"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Check for special processing script.
s3 = ""
s1 = dataset
for (i=stridx("_",dataset); i>0; i=stridx("_",s1)) {
    if (s3 == "")
	s3 = substr (s1, 1, i-1)
    else
	s3 += "_" // substr (s1, 1, i-1)
    s2 = "PipeConfig$/dir" // s3 // ".cl"
    s1 = substr (s1, i+1, 1000)
    if (access(s2) == NO)
        next
    ;

    copy (s2, "special.cl")
    cache special

    iferr {
	special (dataset, ifile, lfile) | tee (lfile)
	status = special.status
    } then {
        sendmsg ("ERROR", $errmsg, "", VRFY)
	status = 0
    }
    ;

    delete ("special.cl")

    if (status == 0)
        break
    ;
}

logout (status)
