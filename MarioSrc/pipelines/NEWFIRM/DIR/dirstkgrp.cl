#!/bin/env pipecl
#
# DIRSTKGRP -- Identify groups for stacking and the tangent points to use.
# Note that this is being done before the WCS is calibrated.
#
# The input is a list of MEF files.

int	nrspgrp
real	ra, dec
string	dataset, datadir, ilist, lfile, rspgrp, rsptgrp, rspord, bpm
struct	*fd

# Tasks and Packages.
images
task skysep	= "NHPPS_PIPESRC$/MOSAIC/RSP/skysep.cl"; cache skysep
task skygroup	= "NHPPS_PIPESRC$/MOSAIC/RSP/skygroup.cl"
task rspgrid	= "NHPPS_PIPESRC$/MOSAIC/RSP/rspgrid.cl"

# Set paths and files.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
ilist = names.indir // dataset // ".dir"
datadir = names.datadir
lfile = names.lfile
cd (datadir)

# Clean up from previous error.
delete ("dirstkgrp*temp")

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Get the pointing information.
list = ilist
touch( "dirstkgrp1.temp" )
while (fscan (list, s1) != EOF) {
    s2 = s1 // "[0]"
    hselect (s2, "RA,DEC,OBSTYPE", yes) | scan (x, y, s3)
    if ( (s3 != "object") && (s3 != "sky") )
	next
    printf ("%g %g %s\n", x, y, s2, >> "dirstkgrp1.temp")
}
list = ""

# Exit if no object data are available
count( "dirstkgrp1.temp" ) | scan( i )
if (i==0) {
    print "No object data available"
    logout 1
}
;

# Group the exposures.
skygroup ("dirstkgrp1.temp", "dirstkgrp", extn=".temp", sep=gos_ditherstep,
    raunit="hr", raformat="%.2h", decformat="%.1h", keepcoords+, >> lfile)
delete ("dirstkgrp1.temp")

# Go through each group and assign the group name and tangent point.
# Each group forms a list in a lists of lists.
# The group name is the first name in the sorted group list.

list = "dirstkgrp.temp"
while (fscan(list,s3) != EOF) {
    fd = s3
    while (fscan (fd, s1, x, y) != EOF) {
	s1 = substr (s1, 1, strldx("[",s1)-1)
	printf ("%s %g %g\n", s1, x, y, >> "dirstkgrp1.temp")
    }
    fd = ""; delete (s3)
    sort ("dirstkgrp1.temp", > s3)
    delete ("dirstkgrp1.temp")

    fd = s3; rspgrp = ""; count (s3) | scan (nrspgrp)
    for (i=1; fscan (fd, s1, x, y) != EOF; i+=1) {
	s2 = s1 // "[0]"
	# Set the tangent point and group name from the first entry.
        if (rspgrp == "") {
	    rspgrp = substr (s1, strldx("/",s1)+1, strstr(".fits",s1)-1)
	    rspgrid (x, y, grid=rsp_grid) | scan (ra, dec, rsptgrp)
            printf ("--- %.0h %0.h %s ---\n", ra, dec, s1, >> lfile)
	}
	;

	# Set the order string.
	printf ("%03d\n", i) | scan (rspord)

	# Set the header keywords and output file.  The grouping keyword
	# is used to for grouping and to define the name for the output
	# group image.  The order parameter defines the order for the
	# imcombine list.  The number in the group is used to decide
	# whether a stack should be created.  The RA and DEC values store
	# the tangent point for resampling.

	hedit (s2, "RSPTGRP", "dummy", add+, show-, ver-)
	hedit (s2, "RSPTGRP", rsptgrp, add+, show-, ver-)
	hedit (s2, "RSPGRP", rspgrp, add+, show-, ver-)
	hedit (s2, "RSPORD", "dummy", add+, show-, ver-)
	hedit (s2, "RSPORD", rspord, add+, show-, ver-)
	hedit (s2, "NRSPGRP", nrspgrp, add+, show-, ver-)
	hedit (s2, "RSPRA", ra, add+, show-, ver-)
	hedit (s2, "RSPDEC", dec, add+, show-, ver-)
    }
    fd = ""
    if ( do_cleanup ) {
	delete (s3)
    }
    ;
}
list = ""

if ( do_cleanup ) {
    delete ("dirstkgrp.temp")
}
;

logout 1
