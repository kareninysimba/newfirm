#!/bin/env pipecl
#
# DIRSETUP -- Setup DIR processing. 
# 
# This is to setup the link for pipedata which contains files used
# by dirverify. Dirverify will then fix things like the detector/instrument
# variants.

string	dataset, datadir, ifile, lfile, instrume

# Packages and tasks.
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Set paths and files.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".dir"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Find image with required keywords.
list = ifile; s2 = ""
while (fscan (list, s1) != EOF) {
    s2 = s1 // "[0]"
    hselect (s2, "$MJD-OBS", yes) | scan (x)
    if (isindef(x)==NO)
        break
    ;
}
list = ""
if (isindef(x))
    logout 1
else if (s2 == "")
    logout 0
;

# Set directories.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
iferr {
    setdirs (s2)
} then 
    logout 0
else
    ;

logout 1
