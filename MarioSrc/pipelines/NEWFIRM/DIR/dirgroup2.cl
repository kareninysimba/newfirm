#!/bin/env pipecl
#
# DIRGROUP2 -- Set list of flats for call to FTR pipeline.
#
# 	STATUS	MEANING
#	0	Untrapped or trapped error
#	1	Found data
#	2	Found no data

int	found, l
int	status = 1
string	dataset, indir, datadir, inlist, lfile, flist, pattern, bpm, s4
string  dataset1, id, sname
struct  statusstr
struct	*list2
file    caldir = "MC$"

images
servers
proto

task $sed = "$!sed -e s/\\\.fits/\\\[1\\\]/ $1 > $2"
task $dirflatregroup = "$!dirflatregroup.cl $1 $2 $3"

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".fdir"

# Set base dataset name (without time stamp), used to set
# time-independent ID used to store grouping names in database
# and retrieve that group name if the data are reprocessed.
dataset1 = dataset
if (strldx("_",dataset1)>0)
    dataset1 = substr (dataset1, 1, strldx("_",dataset1)-1)
;

# Log start of processing.
printf ("\nDIRGROUP2 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd( datadir )

# Check whether this is a rerun
if ( access( "flist.rerun" ) ) {
    # This is a rerun, so restore original flist.tmp
    delete( "flist.tmp" )
    rename( "flist.rerun", "flist.tmp" )
} else {
    # This is a first run, so save flist.tmp in case of a rerun
    copy( "flist.tmp", "flist.rerun" )
}

if ( access( "full_flist.tmp" ) )
    delete( "full_flist.tmp" )
;
if ( access( "full_flist1.tmp" ) )
    delete( "full_flist1.tmp" )
;
# Expand the list flist.tmp created by dirgroup.cl
concat( "@flist.tmp", > "full_flist.tmp" )
# Replace the .fits with the desired extension
sed( "full_flist.tmp", "full_flist1.tmp" )
flist = "full_flist1.tmp"
# Find list of flats or return no data status.
count( flist ) | scan( i )
if ( i == 0 )
    delete( flist )
;
if ( access( flist ) ) {

    # See whether there are any DFLATON or DFLATOFF recipes
    if ( access( "dirgroup21.tmp" ) )
	delete( "dirgroup21.tmp" )
    ;
    touch( "dirgroup21.tmp" )
    hselect( "@"//flist, "$I,NOCDHS,EXPTIME,FILTER,NOCNO,NOCID",
	yes, > "dirgroup21.tmp" )
    if ( access( "dirgroup22.tmp" ) )
	delete( "dirgroup22.tmp" )
    ;
    match( "DFLATO", "dirgroup21.tmp", > "dirgroup22.tmp" )
    count( "dirgroup22.tmp" ) | scan( i )
    if (i>0) {

        # Keep the DFLATS sequences as well. This is done by
	# selecting all sequences whose first entry is
	# NOCTYP=DFLATS.
     	list = "flist.tmp"
        if ( access( "dirgroup23.tmp" ) )
	    delete( "dirgroup23.tmp" )
        ;
	touch( "dirgroup23.tmp" )
	while ( fscan( list, s1 ) != EOF ) {
	    # Read the first line of this sequence
	    list2 = s1
	    i = fscan( list2, s2 )
	    # Replace .fits with [0]
	    s3 = substr( s2, 1, strlstr(".fits",s2)-1 ) // "[0]"
	    # Read the NOCTYP keyword
	    s4 = ""
	    hselect( s3, "NOCTYP", yes ) | scan( s4 )
	    if ( s4 == "DFLATS" ) {
		# Write this sequence to file
		print( s1, >> "dirgroup23.tmp" )
	    }
	    ;
	    list2 = ""
	}
	list = ""
	# Determine statistics for all DFLATON/OFF flats, but only
	# for one of the extensions in order to save time. 
	# Extract the first field from the output of hselect
        if ( access( "dirgroup24.tmp" ) )
	    delete( "dirgroup24.tmp" )
        ;
	fields( "dirgroup22.tmp", fields="1", > "dirgroup24.tmp" )
	# Find a BPM
	list = "dirgroup24.tmp"
	found = 0
	bpm = ""
	while ( ( fscan( list, s1 ) != EOF ) && ( found == 0 ) ) {
	    getcal( s1, "bpm", cm, caldir, obstype="", 
        	detector="!instrume", imageid="!detector",
		filter="", exptime="", mjd="!mjd-obs" ) |
		    scan( i, statusstr )
	    if ( i == 0 ) {
		# A BPM was found
		hselect( s1, "BPM", yes ) | scan( bpm )
		found = 1
	    }
	    ;

	}
	list = ""
        if ( access( "dirgroup25.tmp" ) )
	    delete( "dirgroup25.tmp" )
        ;
	mimstat( "@dirgroup24.tmp", imasks=bpm, fields="image,mean,midpt",
	    nclip=1, lsigma=3, usigma=3, format-, >> "dirgroup25.tmp" )
	# Regroup the DFLATON/OFF flats into DFLATS
	dirflatregroup( dataset, "dirgroup22.tmp", "dirgroup25.tmp" )
	# The sequence names provided by dirflatgroup start at 1,
	# and will likely not match the names assigned by dirgroup.
	# Here we rename the groups to the names assigned by dirgroup.
	if ( access( "end_flist.tmp2" ) )
	    delete( "end_flist.tmp2" )
	;
	touch( "end_flist.tmp2" )
	list = "end_flist.tmp"
	while ( fscan( list, s1 ) != EOF ) {
	    # Get the first entry in the list
	    head( s1, nlines=1 ) | scan( s2 )
            # Define id to look up previous data.
            id = dataset1 // "_" // substr( s2, strldx("/",s2)+1,
                strlstr(".fits",s2)-1 )
            # Retrieve the previous seqeunce ID
            getkeyval( dm, "obsdataproduct", id, "URL" ) | scan( sname )
	    if ( sname != "None" ) {
                printf( "Got sequence name %s from DB for %s\n", sname, id )
                # Sequence name available from database, so use it
		s4 = substr( sname, strldx("_",sname)+1, 999 )
		# Replace the sequence ID given by dirflatregroup
		# with the sequence ID, and rename to the final name
		s3 = dataset // "_" // s4 // ".tmp"
                printf( "Renaming %s to %s\n", s1, s3 )
		rename( s1, s3 )
		print( s3, >> "end_flist.tmp2" )
	    } else {
		# A sequence ID must have been entered in dirgroup,
		# so if none are found, it is an error
		status = 0
	    }
	}
	list = ""
	# Remove the old flist.tmp
	delete( "flist.tmp" )
	# Turn the contents of the output of dirflatregroup into
	# full path names, and put these in the new flist.tmp
	pathnames( "@end_flist.tmp2", > "flist.tmp" )
	# Add the DFLATS seqeunces back in
	concat( "dirgroup23.tmp", >> "flist.tmp" )
	flist = "flist.tmp"

    } else {
	
	# There are no DFLATON/OFF flats, so processing can continue
	# without regrouping
        flist = "flist.tmp"

    }

    rename (flist, inlist)
    type (inlist)
    type ("@"//inlist)
    logout( status )

} else
    logout 2
