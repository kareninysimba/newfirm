#!/bin/env pipecl
#
# OGCproc

# The saturation keyword as put in the header by the KTM is
# set as follows (from F. Valdes' 3/19/08 email):
# exptime = ncoadds * exptime for one readout,
# saturation = nfowler * ncoadds * saturation for one readout

string	dark, datadir, dataset, ilist
string	image, indir, lfile

# Tasks and packages
images
imutil
servers
proto
dataqual

# Set file and path names.
ogcnames( envget("OSF_DATASET") )
dataset = ogcnames.dataset

# Set filenames.
indir = ogcnames.indir
datadir = ogcnames.datadir
ilist = indir // dataset // ".ogc"
lfile = datadir // ogcnames.lfile
set (uparm = ogcnames.uparm)
set (pipedata = ogcnames.pipedata)

# Log start of processing.
printf( "\nOGCDARKSUB (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Make sure no persistence masking is done if not in sciencepipeline
if ( sciencepipeline == NO )
    do_persistence = NO
;

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
ogcnames( image )

# Retrieve the location of the dark from the header, if available. 
# Depending on the configuration, it is possible that no dark
# has been found but processing is to continue regardless (see
# ogcsetup.cl).
dark = "" ; hselect( image, "DARK", yes ) | scan( dark )
if ( dark != "" ) {
    # Dark available, subtract it
    imarith( image, "-", dark, image )
    printf( "Subtracted dark: %s\n", dark, >> lfile )
} else {
    printf( "No dark was subtracted!\n", >> lfile )
}

logout( 1 )
