# OGCNAMES -- Directory and filenames for the OGC pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure ogcnames (name)

string  name                    {prompt = "Name"}

string  dataset                 {prompt = "Dataset name"}
string  pipe                    {prompt = "Pipeline name"}
string  shortname               {prompt = "Short name"}
file    rootdir                 {prompt = "Root directory"}
file    indir                   {prompt = "Input directory"}
file    datadir                 {prompt = "Dataset directory"}
file    uparm                   {prompt = "Uparm directory"}
file    pipedata                {prompt = "Pipeline data directory"}
file    parent                  {prompt = "Parent part of dataset"}
file    child                   {prompt = "Child part of dataset"}
file    lfile                   {prompt = "Log file"}
file    image                   {prompt = "Primary image name"}
file    raw                     {prompt = "Raw image name"}
file    bpm                     {prompt = "Bad pixel mask"}
file    cm1                     {prompt = "Cumulative pixel mask1"}
file    cm2                     {prompt = "Cumulative pixel mask2"}
string  pattern = ""            {prompt = "Pattern"}
bool    base = no               {prompt = "Child part of dataset"}

begin
        # Set generic names.
        names ("ogc", name, pattern=pattern, base=base)
        dataset = names.dataset
        pipe = names.pipe
        shortname = names.shortname
        rootdir = names.rootdir
        indir = names.indir
        datadir = names.datadir
        uparm = names.uparm
        pipedata = names.pipedata
        parent = names.parent
        child = names.child
        lfile = names.lfile

        # Set pipeline specific names.
        image = shortname
        raw = shortname//"_raw"
        bpm = shortname//"_bpm"//".pl"
        cm1 = shortname//"_cm1"//".pl"
        cm2 = shortname//"_cm2"//".pl"

        if (pattern != "") {
            image += ".fits"
        }
end
