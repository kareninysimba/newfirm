#!/bin/env pipecl
#
# OGCproc

int	status = 1
string	datadir, dataset, ilist, obpm, image, indir, lfile, cbpm

# Tasks and packages
images
imutil
servers
proto
dataqual

# Set file and path names.
ogcnames( envget("OSF_DATASET") )
dataset = ogcnames.dataset

# Set filenames.
indir = ogcnames.indir
datadir = ogcnames.datadir
ilist = indir // dataset // ".ogc"
lfile = datadir // ogcnames.lfile
set (uparm = ogcnames.uparm)
set (pipedata = ogcnames.pipedata)

# Log start of processing.
printf( "\nOGCMASK (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Make sure no persistence masking is done if not in sciencepipeline
if ( sciencepipeline == NO )
    do_persistence = NO
;

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
ogcnames( image )

if (do_persistence) {
    # Retrieve path of cumulative pixel mask
    concat( "cbpm_p" ) | scan( cbpm )
    # Store the updated cumulative mask in the same location 
    imdel( cbpm )
    imcopy( "cummask2.pl", cbpm )
} else {
    # Create the name for the cumulative pixel mask. Note that
    # "sat" is appended rather than "per" to indicate that
    # this mask does not include persistence flags.
    s2 = image // "sat.pl"
    # Copy the temporary file to its final name
    imcopy( "cummask2.pl", s2 )
    # Get the full location hostname and path
    pathname( s2 ) | scan( s3 )
    # Because the persistence pipeline has not been run, the
    # cumulative mask has not yet been stored in the calibration
    # manager. This is done here.
    hsel( image, "OBSID,DETECTOR", yes ) | scan( s2, s1 )
    putcal( "", "keyword", cm, value=s3, class="masks",
        imageid=s1, match=ogcnames.parent//" "//s2,
        detector="", filter="", exptime="" )
}


s1 = ""
hsel( image, "QUALINFO", yes ) | scan( s1 )
printf( "INTERPOLATION: "
if ( s1 == "" ) {

    # Interpolate over all bad pixels to produce cosmetically nice
    # images.
    fixpix( image, "cummask2.pl" )
    printf( "cummask2.pl (all bad/flagged pixels)\n" )

} else {

    # If QUALINFO is set, a very large fraction of pixels is masked,
    # possibly all. In this case, there is little point in interpolating
    # over saturated or persistent pixels, so only the bad pixels
    # are interpolated over. (In fact, one could consider never 
    # interpolating over persistent or saturated pixels. However, it is
    # not verified whether the code works as expected. For the fully
    # masked data, it is okay if there is a problem, because the data
    # are masked anyway.)

    hsel( image, "BPM", yes ) | scan( obpm )
    if (ogc_fixchannels) {
#	# tmpmask.pl is created by ogcbadchans.cl
#        fixpix( "tmpmask.pl", obpm )
#        imdel( "tmpmask.pl" )
	# ogcbadchans.cl overwrote BPM with merger of original
	# BPM and bad channel.
        fixpix( image, obpm )
        printf( "obpm %s (only original BPM and bad channels)\n", obpm )
    } else {
        fixpix( image, obpm )
        printf( "obpm %s (original BPM only)\n", obpm )
    }

}

logout( status )
