#!/bin/env pipecl
#
# GCLCLEAN

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ifile, im, lfile, maskname

# Tasks and packages
images
servers

# get the dataset name
names( "gcl", envget("OSF_DATASET") )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ifile = indir // dataset // ".gcl"

cd (datadir)

# Log start of processing.
printf( "\nSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Loop over all _00 files, and delete those that do not have a
# matching entry in any of the SGC return files. If no entry is
# available, that means the exposure was rejected by the SGC pipeline
# because it was completely masked (due to saturation or persistence).
# Create a list of all _00 files
files( "*_00.fits", > "gclclean1.tmp" )
# Remove the trailing _00.fits
list = "gclclean1.tmp"; touch ("gclclean2.tmp")
while ( fscan( list, s1 ) != EOF ) {
    s2 = substr( s1, 1, strldx("_",s1)-1 )
    printf( "%s\n", s2, >> "gclclean2.tmp" )
}
list = ""
# Create a list of all the fits files in the SGC return files
match( "*.fits", "*.sgc", > "gclclean3.tmp" )
# Extract only the base file name
list = "gclclean3.tmp"; touch ("gclclean4.tmp")
while ( fscan( list, s1 ) != EOF ) {
    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    printf( "%s\n", s2, >> "gclclean4.tmp" )
}
list = ""
# Now do the loop over all _00 and match against SGC return files
list = "gclclean2.tmp"
while ( fscan( list, s1 ) != EOF ) {
    i = 0
    match( s1, "gclclean4.tmp" ) | count | scan( i )
    if (i==0) {
        imdel( s1//"_00" )
    }
    ;
}
list = ""

# The original, unprocessed fits files are not needed anymore, because
# further processing has been done in the SGC pipeline, so delete these.
imdel( "*_im*" )

delete( "gclclean*.tmp" )

logout( status )
