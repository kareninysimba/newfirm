#!/bin/env pipecl

int     status
string	datadir, dataset, extname, gcldir, ifile
string	image, indir, lfile, listoflists, outfile, gclfiles
string  groupname, grouplist
struct	*fd

# Tasks and packages
images
proto
task $findgroups = "$!gosfindgroups.cl $1 $2"

# Set file and path names.
gosnames (envget("OSF_DATASET"))
dataset = gosnames.dataset

# Set filenames.
indir = gosnames.indir
datadir = gosnames.datadir
ifile = indir // dataset // ".gos"
lfile = datadir // gosnames.lfile
set (uparm = gosnames.uparm)
set (pipedata = gosnames.pipedata)

# Log start of processing.
printf ("\nGOSWHICHSKYSUB (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Retrieve the extension names from the file "split.ext", which
# was created when the GCL pipeline called the SGC pipeline with
# splitc.cl
line = ""
if (access (dataset//".gcl")) {
    list = dataset // ".gcl"
    while (fscan (list, s1) != EOF) {
        if (access (s1))
	    match( "split.ext", s1 ) | scan( line )
	;
	break
    }
    list = ""
}
;
if (line == "") {
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 3
}
;

# Copy the file to the current directory for later use by gosstkgroup.cl
copy( line, "split.ext" )

# Create the list of files to work on from the output of the SGC 
# pipeline, so we look for .sgc files in the relevant $GCLDATA/data
# directory.
# TODO: the part below is a bit of a hack. If possible, this should
# be changed so that the extracting the GCL host and the pathnames is
# not necessary, perhaps it can be retrieved from the .gcl file, or the
# .sgc files mentioned in the .gcl file?
# Determine the host on which the GCL pipeline ran
s1 = substr( line, 1, stridx( "!", line )-1 )
# Construct the GCL path
gcldir = s1 // "!" // envget("NEWFIRM_GCL") // "/data/" // dataset // "-gcl/"
# Select the files from the SGC return lists to GCL
match( ".fits", gcldir//"*.sgc", print-, >> "goswhichskysub1.tmp" )

# Make sure the files are in order of observation
hselect( "@goswhichskysub1.tmp", "$I,NOCID", yes ) |
    sort( "STDIN", column=2, num+ ) | fields( "STDIN", 1, 
        >> "goswhichskysub5.tmp" )

# Logout with status=3 if there are no files in the SGC return lists
i = 0
count( "goswhichskysub5.tmp" ) | scan( i )
if ( i ==0 ) {
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 3
}
;

# Now loop over all extensions, and create a list of files for SKY
# pipeline from goswhichskysub5.tmp
list = line # line contains path to split.ext, which contains all extensions
listoflists = indir//dataset//"sky.lol"
s1 = "goswhichskysub.tmp"
touch( s1 )
gclfiles = "goswhichskysub4.tmp"
while ( fscan( list, s2, extname ) != EOF) { # loop over all extensions

    # Make sure the output file exists
    touch( gclfiles )
    # Extract the relevant fits files from the GCL return list
    match( extname//".fits", "goswhichskysub5.tmp", > gclfiles )
    # Determine the number of lines in gclfiles
    count( gclfiles ) | scan( i )
    # Continue if gclfiles has contents
    if ( i > 0 ) {

        # Find groups within the current input list.
        findgroups( gclfiles, "goswhichskysub3.tmp" )
	# Loop over all groups
	fd = "goswhichskysub3.tmp"
	while ( fscan( fd, groupname, grouplist ) != EOF ) {

            # Create the output file name. Note that the groupname as
            # created in findgroups already contains the extension name.
            outfile = indir//groupname//".gos"
            # Rename grouplist to the desired name
            rename( grouplist, outfile )
            # Add the output file to the precursor of listoflists
            print( outfile, >> s1 )

	}
        fd = ""

    }
    ;

}

count( s1 ) | scan( i )

if ( i > 0 ) {
    list = ""
    # Convert the file names in s1 into full IRAF pathnames
    pathnames( "@"//s1, >> listoflists )
    delete( s1 )
    status = 1
} else {
    # s1 may be empty if all files in OGC are renamed to _mkd, which
    # happens if (almost) all pixels have been masked, e.g., due to
    # saturation or persistence masking.
    status = 2
}

logout( status )
