#!/bin/env pipecl

int     status, foundmatch
string  datadir, dataset, extname, firstext, s4, group
string  image, indir, lfile, listoflists, outfile, wcsreturn
struct  *fd

images
proto

task gosskyangle   = "NHPPS_PIPESRC$/NEWFIRM/GOS/gosskyangle.cl"
task gosskygroup   = "NHPPS_PIPESRC$/NEWFIRM/GOS/gosskygroup.cl"
task $sed = "$!sed -e s/$(1)/$(2)/ $(3) >> $(4)"
task $grep = "$!grep $(1) $(2) >> $(3)"

# Set file and path names.
gosnames (envget("OSF_DATASET"))
dataset = gosnames.dataset

# Set filenames.
indir = gosnames.indir
datadir = gosnames.datadir
lfile = datadir // gosnames.lfile
set (uparm = gosnames.uparm)
set (pipedata = gosnames.pipedata)

# Log start of processing.
printf ("\nGOSSTKGROUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Retrieve the results from the WCS pipeline. WCS returned a list of
# files. The name of this list is given in dataset//".wcs"
if (access (dataset//".wcs")) {
    list = dataset//".wcs"
    status = fscan( list, wcsreturn )
    list = ""
} else {
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 0
}
# The file whose name is stored in 'wcsreturn' contains the files
# produced by the WCS pipeline, which includes the return lists
# the SWC pipeline generated and returned to the WCS pipeline.
# Select the SWC return lists.
match( ".swc", wcsreturn, >> "gosstkgroup1.tmp" )
# gosstkgroup1.tmp now contains all the lists returned by SWC,
# expand and concatenate those into one file
concat( "@gosstkgroup1.tmp", > "gosstkgroup2.tmp" )
# Go through gosstkgroup2.tmp to select the required files. There are
# two possibilities: 1) data are stacked per exposure (this is the
# default), and 2) data are stacked per array (as long as
# gos_ditherstep is set to be smaller than the array separation).
if ( gos_byarray == NO ) { # data stacked by exposure
    # split.ext contains the list of extensions, copied here by
    # goswhichskysub.cl (from the GCL pipeline)
    # Extract the first extension for which data have been returned
    # (this saveguards against the possibility that there is no data
    # for extension nuber 1).
    list = "split.ext"
    foundmatch = 0
    while ( (fscan( list, s1, s2 ) != EOF) && (foundmatch==0) ) {
        # Create the matching string
        s3 = "*"//s2//"_ss.fits"
        # Select the matches from gosstkgroup2.tmp
        match( s3, "gosstkgroup2.tmp", print-, >> "gosstkgroup3.tmp" )
        i = 0
        if ( access( "gosstkgroup3.tmp" ) ) 
            count( "gosstkgroup3.tmp" ) | scan( i )
            if ( i>0 ) {
                foundmatch = 1
                firstext = s2
            } else
                delete( "gosstkgroup3.tmp" )
        ;
    }
    list = ""
} else { # data are stacked per array
    # Select the matches from gosstkgroup2.tmp
    match( "*_ss.fits", "gosstkgroup2.tmp", print-, >> "gosstkgroup3.tmp" )
}

list = "gosstkgroup3.tmp"
if (access("gosstkgroup3.tmp")) {
    print "OK"
} else {
    print "NOT OK"
    logout 0
}
while( fscan( list, s1 ) != EOF ) {
    printf("%d %d\n", 1, 1, > "gosstkgroup5.tmp" )
    wcsctran( "gosstkgroup5.tmp", "gosstkgroup6.tmp", s1, inwcs="logical",
        outwcs="world", verbose- )
    type( "gosstkgroup6.tmp" ) | scan( line )
    printf(" %s %s\n", line, s1, >> "gosstkgroup4.tmp" )
    delete( "gosstkgroup6.tmp" )
}
list = ""

# Group the pointings in the current sequence by mapping position.
# Pointings that are closer together than gos_ditherstep will
# end up in the same group.
# TODO: it appears that gosskygroup does not work correctly if
# keepcoords is set to NO.
gosskygroup( "gosstkgroup4.tmp", "stack", extn=".grp", sep=gos_ditherstep,
    raunit="deg", raformat="%.2H", decformat="%.1h", keepcoords+ )
delete( "gosstkgroup4.tmp" )

if ( gos_byarray == NO ) { # data stacked by exposure
    # Use the list of groups just created for one of the
    # extensions to add in the others extensions to each
    # of the groups. Note that we cannot simply replace
    # one extension with another in the files names in
    # the input list, because the file names also include
    # the nodes, which may be different.

    # First create a list of the extensions
    fields( "split.ext", fields=2, >> "ext" )
    # Create the strings that needs to be substituted
    s3 = "_"//firstext//"_"

    # Loop over all the groups in stack.grp
    fd = "stack.grp"
    while( fscan( fd, group ) != EOF ) {
	# Loop over all extensions
        list = "ext"
        while( fscan( list, s1 ) != EOF ) {
            # Create the first replacement string
            s2 = "_"//s1//"_"
	    # Replace with the first replacement string
	    sed( s3, s2, group, "gosstkgroup6.tmp" )
        }
        list = ""
        # gosstkgroup6.tmp now contain the complete list,
        # including all extensions, of the exposures to be 
        # stacked, but the node names may be incorrect

	# Set group name to name of first images.
	fields (group, "3") | sort | head (nl=1) | scan (s1)
	s1 = substr (s1, strldx("/",s1)+1, 1000)
	s1 = substr (s1, 1, stridx("_",s1)-1)
	s1 = "stack_" // s1 // ".grp"

        # Delete the original group
        delete( group )
        # Set the new group name to the name just created
	group = s1
        # Remove the RA and Dec and only keep the file name
        fields( "gosstkgroup6.tmp", fields=3, > "gosstkgroup4.tmp" )
        delete( "gosstkgroup6.tmp" )
        # Add the new name to the new list of groups
	print (group, >> "stack1.grp")

        # Loop over the list with correct extensions, retrieve
        # the file name from each line, and use that to find 
        # the matching entry in the expanded SWC return lists.
        list = "gosstkgroup4.tmp"
        # Loop over all entries in gosstkgroup4.tmp
        while ( fscan( list, s1 ) != EOF ) {
            # Extract only the file name
            s2 = substr( s1, strldx("/",s1)+1, 999 )
            # Retrieve the matching entry from gosstkgroup2.tmp
            # (containing the expanded SWC return files) and
            # and append it to gosstkgroup6.tmp
            grep( s2, "gosstkgroup2.tmp", "gosstkgroup6.tmp" )
        }
        list = ""

        # gosstkgroup6.tmp now contains the list of all extensions
        # to be stacked, with the correct node names. Rename it
        # to the group name created above
        rename( "gosstkgroup6.tmp", group )
        # Clean up
        delete( "gosstkgroup6.tmp" )

    } # End loop over all the groups
    fd = ""
    rename ("stack1.grp", "stack.grp")
    delete( "ext" )
   
}
;

if (1==0) {
# Append the saturated/bad pixel masks to each of the groups
# in stack.grp
list = "stack.grp"
while ( fscan( list, s1 ) != EOF ) {
    fd = s1
    while( fscan( fd, s2 ) != EOF ) {
        # Extract the string between the last / and the last _
        s3 = substr( s2, strldx("/",s2)+1, strldx("_",s2)-1 )
        # Match this string against the contents of gosstkgroup2.tmp
        match( s3, "gosstkgroup2.tmp", > "gosstkgroup7.tmp" )
        # Set default value in case no match is found
        s2 = ""
        # Look for "bpm" in the output from the previous match
        match( "bpm", "gosstkgroup7.tmp" ) | scan( s2 )
        # If a match is found (i.e., s2!=""), then append the 
        # bad pixel mask to a list. The case of the saturated/bad
        # pixel mask not being found is handled by the downstream
        # pipeline (where it will be substituted by the default
        # bad pixel mask).
        if ( s2 != "" ) 
           print( s2, >> "gosstkgroup8.tmp" )
        ;
        # Clean up
        delete( "gosstkgroup7.tmp" )
    }
    fd = ""
    # gosstkgroup8.tmp now contains all the saturated/bad pixel masks.
    # Create full IRAF path names for each and append them to the
    # list of exposures to be stacked
    pathnames( "@gosstkgroup8.tmp", >> s1 )
    # Clean up
    delete( "gosstkgroup8.tmp" )
}
list = ""
}
;

# stack.grp is a list of lists, each list in it defining a single group.
# This list of lists is also needed by secspsstart.cl. However, the 
# entries in this list of lists are renamed by the call.cl in
# gosstkstart.cl. To make sure that the information that secspsstart.cl
# needs is preserved, it is copied here.
list = "stack.grp"
while( fscan( list, s1 ) != EOF ) {
    s2 = "keep"//s1
    copy( s1, s2 )
    pathname( s2, >> "stack_secspsstart.grp" )
}
list = ""

# Create the list of lists
listoflists = indir//dataset//"stk.lol"
# Expand the file names in stack.grp to fully qualified IRAF names
# and store these in listoflists
concat("stack.grp")
pathname( "@stack.grp", >> listoflists )
# Clean up. 
delete( "gosstkgroup*.tmp,stack.grp" )

logout 1
