#!/bin/env pipecl

int	status
string	datadir, dataset, extname
string	image, indir, ifile, lfile, listoflists, outfile
string	temp1, temp2

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set file and path names.
gosnames (envget("OSF_DATASET"))
dataset = gosnames.dataset

# Set filenames.
indir = gosnames.indir
datadir = gosnames.datadir
lfile = datadir // gosnames.lfile
set (uparm = gosnames.uparm)
set (pipedata = gosnames.pipedata)

# Log start of processing.
printf ("\nGOSSECSTART (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

if (sciencepipeline) {

    # Initially, the SEC pipeline was called with a simple call.cl in the
    # xml file. This was changed because the stack.grp is also needed
    # by the SEC pipeline. So now a customized list is created and sent
    # to the SEC pipeline here.

    ifile = datadir // dataset // ".list"
    pathname( "stack_secspsstart.grp", >> ifile )

    # Pass on the GCL return file to the SEC pipeline
    head( dataset//".gcl" ) | scan( s1 )
    pathnames( s1, >> ifile )

    # Call the SEC pipelines 
    temp1 = "gosstk1.tmp"
    temp2 = "gosstk2.tmp"
    callpipe( "gos", "sec", "copy", ifile, temp1, temp2 )
    concat (temp2) | scan (status)
    concat (temp1)
    printf("Callpipe returned %d\n", status)

} else {

    # The SEC pipeline may or may not be running but no second pass
    # sky subtraction is desired, so produce an exit code indicating
    # the SEC pipeline need not be run.
    status = 1

}

logout( status )
