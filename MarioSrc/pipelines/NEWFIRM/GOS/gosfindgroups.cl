#!/bin/env pipecl

real    exptime, nocid, pexptime, pnocid, dnocid
string  inlist, outlist, filter, groupname, pfilter, grouplist
string  seqid, pseqid
struct  *list2

images
proto

# Get arguments.
if ( fscan( cl.args, inlist, outlist ) != 2 ) {
    printf( "Usage: gosfindgroups.cl inlist outlist\n" )
    logout 0
}
;

# Exit if the input list is empty
count( inlist ) | scan( i )
if ( i == 0 ) {
    printf( "Input list %s is empty\n", inlist )
    logout 1
}
;

# Make sure only first entry on each line is used
fields( inlist, "1", > "gosfindgroups2.tmp" )
# Read the keywords needed for group determination and write them to file
hselect( "@gosfindgroups2.tmp", "$I,FILTER,OEXPTIME,NOCID,SEQID", yes,
    > "gosfindgroups1.tmp" )

# When reading from file below, both the inlist and gosfindgroups1.tmp
# are read, so that any additional contents in the inlist can be written
# out as well.

# Read the first line from the file
list = "gosfindgroups1.tmp"
list2 = inlist
i = fscan( list, s1, pfilter, pexptime, pnocid, pseqid )
i = fscan( list2, line )

# Set groupname and grouplist
s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
# Remove the _ from the groupname
groupname = substr( s2, 1, strldx("_",s2)-1 ) //
            substr( s2, strldx("_",s2)+1, 999 )
grouplist = groupname//".list"

# Write the groupname and grouplist to the output list
printf( "%s %s\n", groupname, grouplist, > outlist )
# Write the first entry "line" to the grouplist ("line" contains
# s1 and whatever else was present on the rest of the input line 
# from the inlist).
printf( "%s\n", line, >> grouplist )

# Loop over all entries
while( fscan( list, s1, filter, exptime, nocid, seqid ) != EOF ) {

    # Read the matching line from the original inlist
    i = fscan( list2, line )
    # Calculate dnocid, the most time allowed to pass without breaking
    # a sequence
    dnocid = (exptime+400)/86400
    # Detect breaks in group, but only outside of observing sequences
    if ( pseqid != seqid ) {
        if ( (filter!=pfilter) || (exptime!=pexptime) || (nocid>pnocid+dnocid) ) {
            printf( "Break detected - new group starts at %s\n", s1 )
            # Set the new groupname and grouplist
            s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
            groupname = substr( s2, 1, strldx("_",s2)-1 ) // substr( s2, strldx("_",s2)+1, 999 )
            grouplist = groupname // ".list"
            # Write the new groupname and grouplist to the output list
            printf( "%s %s\n", groupname, grouplist, >> outlist )
        }
        ;
    }
    ;
    # Write the current contents of "line" to the current grouplist
    printf( "%s\n", line, >> grouplist )
    # Update the previous values
    pfilter = filter ; pexptime = exptime ; pnocid = nocid; pseqid = seqid
    
}
list = ""

# Clean up
#delete( "gosfindgroups?.tmp" )

logout 1
