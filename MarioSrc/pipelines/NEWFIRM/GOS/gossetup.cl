#!/bin/env pipecl
#
# GOSSETUP

string  datadir, dataset
string  image, indir, lfile, ifile

images
proto
util
servers
task $psqlog = "$!psqlog"
task $sed = "$!sed -e s/\\\[0\\\]// $1 > $2"
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"

# Set file and path names.
gosnames( envget("OSF_DATASET") )
dataset = gosnames.dataset

# Set filenames.
indir = gosnames.indir
datadir = gosnames.datadir
lfile = datadir // gosnames.lfile
ifile = indir // dataset // ".gos"
set (uparm = gosnames.uparm)
set (pipedata = gosnames.pipedata)

# Log start of processing.
printf ("\nGOSSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Record in PSQ.
psqlog (names.shortname, "submitted")

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1//"[0]" )
} then {
    logout 0
} else
    ;

# Append "[0]" to all files in the list.
list = ifile
while( fscan( list, s1 ) != EOF )
    print( s1//"[0]", >> "gossetup1.tmp" )

# Record the ENIDs.
hselect ("@gossetup1.tmp", "ENID*", yes) | words (>> dataset//".enids")

# Make sure the input list is in the same order as the observations
# were taken in. 
hselect( "@gossetup1.tmp", "$I,NOCID", yes, >> "gossetup2.tmp" )
# Remove the original list
#delete( ifile )
# Sort by NOCID, output the first column to the original ifile
sort( "gossetup2.tmp", column=2, numeric=yes ) |
    fields( "STDIN", 1, >> "gossetup3.tmp" ) 
# Remove the trailing [0]
sed( "gossetup3.tmp", "gossetup4.tmp" )

# Clean up
delete( "gossetup?.tmp" )

logout 1
