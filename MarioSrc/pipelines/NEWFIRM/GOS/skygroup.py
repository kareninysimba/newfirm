#!/usr/bin/python

import sys
from math import *

sep = 60
raunit = "hr"
RAD = 57.29578
out = "sub"
extn = ''

def sexa2dec( val ):
    ''' Convert sexagecimal numbers (in string format) to decimal
        numbers (as floats).'''
    hms = val.split( ':' )
    return( float(hms[0]) + float(hms[1])/60 + float(hms[2])/3600 )

def dec2sexa( val ):
    ''' Convert a decimal number into a sexagicimal one (in string
        format. '''
    hms = []
    hms.append( int( val ) )
    r = val-hms[0]
    hms.append( int( r*60 ) )
    r = 60*r-hms[1]
    hms.append( int(6000*r+0.5)/100 )
    return( '%02i:%02i:%05.2f' % ( hms[0], hms[1], hms[2] ) )

def dcos( val ):
    ''' Return the cosine of a number given in degrees. '''
    return cos( val/RAD )

def dsin( val ):
    ''' Return the sine of a number given in degrees. '''
    return sin( val/RAD )

def datan2( val1, val2 ):
    ''' Return the atan2 of two numbers given in degrees.'''
    return atan2( val1/RAD, val2/RAD )

def skyangle( ra1, dec1, ra2, dec2, raunit ):
    ''' Determine the angle between two points on the sky (in
        arcseconds. '''
    if raunit == "hr":
        ra1 *= 15
        ra2 *= 15
    c1 = dcos(dec1)
    c2 = dcos(dec2)
    x = dcos(ra1) * c1 - dcos(ra2) * c2
    y = dsin(ra1) * c1 - dsin(ra2) * c2
    z = dsin(dec1) - dsin(dec2)
    c1 = (x*x + y*y + z*z) / 4.
    c2 = max (0., 1.-c1)
    return( 2 * datan2(sqrt(c1),sqrt(c2)) * 3600 * RAD )

# Set some initial values.
if raunit == "hr":
    dra = 24
else:
    dra = 360
decgroup = []
sorted_decgroup = []
grouped_list = []
sorted_groupted_list = []
n = 1

# Read the input file, which is expected to have ra, dec, and a name.
f = open( sys.argv[1], 'r' )
coords = []
for line in f:
    fields = line.split( ' ' )
    coords.append( ( fields[0], fields[1], fields[2] ) )

# Start by sorting in Dec.
coords.sort( key=lambda x: float(sexa2dec(x[1])) ) # sort by Dec

# Get the values for the first entry from the coordinate list
r1 = coords[0][0]
d1 = coords[0][1]
data1 = coords[0][2]

# Loop over all remaining entries in the coordinate list.
for i in range(1,len(coords)):

    # Get the values for the current entry in the coordinate list.
    r2 = coords[i][0]
    d2 = coords[i][1]
    data2 = coords[i][2]

    # Add the current entry to the current Dec-group.
    decgroup.append( (r1, d1, data1 ) )
    # If the previous RA is only 'sep' larger than 0, add dra and add
    # it to the Dec-group again, so that groups around RA=0h can be
    # found correctly (without this, such groups would be split 
    # in two).
    if 3600*sexa2dec(r1)/max( 0.001, dcos(sexa2dec(d1)) ) <= sep:
         r4 = sexa2dec(r1) + dra
         decgroup.append( (dec2sexa(r4), d1, data1 ) )

    # If the difference in Dec between the current entry and the
    # previous is smaller than sep, then this point belongs to the
    # current group. Set the r1 (previous entry) to the current and
    # move on to the next entry in the input list.
    if abs(sexa2dec(d2)-sexa2dec(d1))*3600 <= sep:
        r1 = r2; d1 = d2; data1 = data2
        continue

    # If this point is reached, there was a jump in Dec.

    # Store the entry that was most recently read from the Dec sorted
    # list.
    r3 = r2
    d3 = d2
    data3 = data2

    # Sort the Dec-group in RA.
    sorted_decgroup = decgroup
    sorted_decgroup.sort(key=lambda x: float(sexa2dec(x[0])) )
    # Reset the unsorted array.
    decgroup = []

    # Get the values for the first entry in the sorted Dec-group.
    r1 = sorted_decgroup[0][0]
    d1 = sorted_decgroup[0][1]
    data1 = sorted_decgroup[0][2]
    # Store the Dec of the first entry in the list.
    d4 = d1

    # Loop over all entries in the sorted Dec-group
    for i in range(1,len(sorted_decgroup)):

        # Read the values for the current entries in the sorted Dec-group.
        r2 = sorted_decgroup[i][0]
        d2 = sorted_decgroup[i][1]
        data2 = sorted_decgroup[i][2]
        # Store the previous entry, along with the group number.
        grouped_list.append( ( n, r1, d1, data1 ) )

        # Calculate the separation between the current and the
        # previous point. Note that the declination is fixed to d4
        # because all entries in this list are in the same Dec group.
        # If the actual declinations are used, the separation between
        # the points might be larger than 'sep', which would result
        # in the current Dec-group being broken up again.
        gossep = skyangle( sexa2dec(r1), sexa2dec(d4), sexa2dec(r2),
                           sexa2dec(d4), raunit )

        # If the difference in RA between the current entry and the
        # previous one smaller than sep, then this point belongs to
        # the current group. Set the r1 (previous entry) to the
        # current and move on to the next entry.
        if gossep <= sep:
            r1 = r2; d1 = d2; data1 = data2
            continue

        # If we reach this point, there has been a jump in RA, so the
        # group counter is increased...
        n += 1
        # ... and r1 is reset to contain the previous entry at the
        # start of the next iteration.
        r1 = r2
        d1 = d2
        data1 = data2

    # Close the input file and reset the sorted list.
    sorted_decgroup = []
    # Make sure the last point also gets written.
    grouped_list.append( ( n, r1, d1, data1 ) )
    n += 1

    # Reset r1 to contain the previous entry of the Dec-grouped list
    # at the start of the next iteration.
    r1 = r3; d1 = d3; data1 = data3

# Finish up the last Dec-group. First, add the current entry to the list.
decgroup.append( ( r1, d1, data1 ) )
# If the current RA is only 'sep' larger than 0, add dra and add it to
# the Dec-group again but with 'dra' added.
if 3600*sexa2dec(r1)/max( 0.001, dcos(sexa2dec(d1)) ) <= sep:
    r4 = sexa2dec(r1) + dra
    decgroup.append( (dex2sexa(r4), d1, data1 ) )

# Sort the Dec-group in RA.
sorted_decgroup = decgroup
sorted_decgroup.sort( key=lambda x: float(sexa2dec(x[0])) )

# Get the first entry from the sorted Dec list.
r1 = sorted_decgroup[0][0]
d1 = sorted_decgroup[0][1]
data1 = sorted_decgroup[0][2]

# Keep the Dec of the first entry in the list.
d4 = d1

# Loop over the remaining entries in the Dec-sorted group list.
for i in range( 1, len(sorted_decgroup) ):

    # Get the next entry from the Dec-sorted group list.
    r2 = sorted_decgroup[i][0]
    d2 = sorted_decgroup[i][1]
    data2 = sorted_decgroup[i][2]

    # Store the previous entry, along with the group number.
    grouped_list.append( ( n, r1, d1, data1 ) )

    # Calculate the separation between the current and the previous
    # point. Note that the declination is fixed to d4 because all
    # entries in this list are in the same Dec group.
    gossep = skyangle( sexa2dec(r1), sexa2dec(d4), sexa2dec(r2),
                       sexa2dec(d4), raunit )

    # If the difference in RA between the current entry and the
    # previous one smaller than sep, then this point belongs to the
    # current group. Set the r1 (previous entry) to the current and
    # move on to the next entry.
    if (gossep <= sep):
        r1 = r2
        d1 = d2
        data1 = data2
        continue

    # If this point is reached, there has been a jump in RA, so the
    # output file counter is increased ...
    n += 1
    # ... and r1 is reset to contain the previous entry at the start
    # of the next iteration.
    r1 = r2; d1 = d2; data1 = data2

# Make sure the last point also gets written.
grouped_list.append( ( n, r1, d1, data1 ) )

# Sort the file containing the grouping information by the output file
# counter, then by name, and finally by RA. Sorting in this way
# ensures that duplicate entries (added for points near RA=0h) will be
# adjacent to each other in the list. This makes it easy to remove
# duplicates.
sorted_grouped_list = grouped_list
sorted_grouped_list.sort( key=lambda x: (float(x[0]),x[3],x[1]) )

# Initialize the dictionary to contain the output groups
output = {}
# Read the first entry of the sorted grouped list
i = sorted_grouped_list[0][0]
data1 = sorted_grouped_list[0][1:]
name1 = sorted_grouped_list[0][3].rstrip()

# Loop over the rest of the entries
for k in range( 1, len(sorted_grouped_list) ):

    # Read the next entry from the sorted grouped list
    j = sorted_grouped_list[k][0]
    data2 = sorted_grouped_list[k][1:]
    name2 = sorted_grouped_list[k][3].rstrip()

    # If the names of the current and the previous entry match,
    # the entry is a duplicate and the current one should be
    # dropped. The sorting done above ensures that the current
    # one is the one that has RA>dra.
    if name1 == name2:
###        temp2.append( ( j, i ) )
        continue

    # If this point is reached, then the entry is not a duplicate.
    # Set the name of this group:
    fname = '%s_%03d%s' % ( out, i, extn )
    # Check whether the name is already in the dictionary. If so,
    # append the previous entry to the corresponding list. If not,
    # create an entry in the dictionary with the value of the previous
    # entry.
    if fname in output:
        output[fname].append( [ data1 ] )
    else:
        output[fname] = [ data1 ]

    # Set the previous value to the current value.
    i = j
    data1 = data2
    name1 = name2

# Make sure the last point also gets written
fname = '%s_%03d%s' % ( out, i, extn )
if fname in output:
    output[fname].append( [ data1 ] )
else:
    output[fname] = [ data1 ]
print output

sys.exit(1)

###temp1 = dict(map(lambda i: (i,1),temp2)).keys()
###print temp1
###temp2 = []

#### Merge the lists.
###n1 = n
###for k in range(0,len(temp1)):
###    j = temp1[k][0]
###    i = temp1[k][1]
###    fname = '%s_%03d%s' % ( out, j, extn )
###    # "if access(fname)":
###    if fname in output:
###        inp = '%s_%03d%s\n' % ( out, i, extn )
###        for line in inp:
###            output[fname].append( [ line ] )
###        n1 -= 1
###temp1 = []

## Renumber if needed.
###if n1 != n:
###    i = 1
###    j = 1
###    while j<=n:
###        fname = '%s_%03d%s' % ( out, j, extn )
###        if fname in output:
###	    if i != j:
###                inp = '%s_%03d%s\n' % ( out, i, extn )
###                del output[fname]
###                output[fname] = output[inp]
###                del output[inp]
###	    i += 1
###        j += 1

### Create the final output list of lists.
###files (out//"_[0-9]*", > out//extn)



