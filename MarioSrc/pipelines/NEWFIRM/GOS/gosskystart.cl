#!/bin/env pipecl

int status
string datadir, dataset, indir, lfile, listoflists, temp1, temp2

# Set file and path names.
gosnames (envget("OSF_DATASET"))
datadir = gosnames.datadir
dataset = gosnames.dataset
indir = gosnames.indir
lfile = datadir // gosnames.lfile

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Log start of processing.
printf ("\nGOSSKYSTART (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Call the SKY pipeline
listoflists = indir//dataset//"sky.lol"
temp1 = "gossky1.tmp"
temp2 = "gossky2.tmp"
callpipe( "gos", "sky", "split", listoflists, temp1, temp2 )
concat (temp2) | scan (status)
concat (temp1)
printf("Callpipe returned %d\n", status)

logout( status )
