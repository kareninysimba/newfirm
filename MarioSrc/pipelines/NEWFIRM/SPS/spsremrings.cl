#!/bin/env pipecl
#
# SPSREMRINGS

int     status = 1
int     xcen, ycen
string  dataset, indir, datadir, ilist, lfile, olist, bpm, image, area
real	skymode

# Tasks and packages
images
servers
dataqual
proto

# get the dataset name
spsnames( envget("OSF_DATASET") )

# Set paths and files.
indir = spsnames.indir
dataset = spsnames.dataset
datadir = spsnames.datadir
lfile = datadir // spsnames.lfile
ilist = indir // dataset // ".sps"
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Log start of processing.
printf( "\nSPSREMRINGS (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

cd (datadir)

list = ilist
while ( fscan( list, s1, s2 ) != EOF ) {

    # Set the output name for this exposure
    s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    spsnames( s3 )

    # Delete the mask if it already exists
    if ( imaccess( spsnames.mask ) ) 
        imdel( spsnames.mask )
    ;

    # Switch to WCS matching
    printf ("reset pmmatch = world\nkeep\n") | cl 
    # Match the stacked mask to the image in WCS space
    # mskexpr syntax: expression, output_mask ref_img ref_mask
    flpr
    if ( imaccess( spsnames.obm ) ) {
        imdel( spsnames.obm )
    }
    ;
    mskexpr( "m", spsnames.obm, s1, refmask=s2//"[pl]" )
    # Retrieve the bad pixel mask from the header
    hsel( s1, "CBPM", yes ) | scan( bpm )
    # Combine the masks
    imexpr( "a+b>=1", spsnames.mask//".pl", bpm, spsnames.obm )
    # Remove intermediate step
#    imdel( spsnames.obm )
    # Add the name of the combined mask to the header
    hedit( s1, "STKBPM", spsnames.mask, add+, ver- )
    # And add the object mask as well
    hedit( s1, "OBJBPM", spsnames.obm, add+, ver- )

}

# In this module, the files are copied from their remote location (as
# given in the original input list) to here. To make sure skysub.cl
# uses the correct files, a new one with the local paths is created,
# and written over the original input list.
olist = "skyremrings1.tmp"

if ( ring_removal == yes ) {

    # Get first file from list to determine which array is being used
    head( ilist ) | scan( s1 )
    hselect( s1, "IMAGEID", yes ) | scan( i )
    if ( i == 1 ) {
        xcen = 2046; ycen = 2046;
	area = "[1024:2046,1024:2046]"
    } else if ( i == 2 ) {
        xcen = 0; ycen = 2046;
	area = "[1:1023,1024:2046]"
    } else if ( i == 3 ) {
        xcen = 2046; ycen = 0;
	area = "[1024:2046,1:1023]"
    } else {
        xcen = 0; ycen = 0;
	area = "[1:1023,1:1023]"
    }

    list = ilist
    while ( fscan( list, s1, s2 ) != EOF ) {

        # Set the output name for this exposure
        s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
	spsnames( s3 )
	# Copy file locally
	image = spsnames.image
	imcopy( s1, image )
	# Print full IRAF path to olist
	pathnames( image//".fits" ) | scan( s3 )
	printf( "%s %s\n", s3, s2, >> olist )

imcopy( image, image//"_ringsub" )

	# Calculate the mode, masking out bad pixels, in the
        # approximate are where the rings are not present (or at
	# least not strong)
        mimstat( s1//area, fields="mode", nclip=3, lsigma=2, 
            usigma=2, format-, imasks=spsnames.mask//area ) | scan( skymode )

	# Perform ring removal
        removerings( spsnames.image, mask=spsnames.mask,
            xcenter=xcen, ycenter=ycen, nrings=500, inner=50,
	    skymode=skymode, niter=3, low=3, high=3, pclip=0.5 )

	# Add keyword to the header indicating ring removal was done
	hedit( spsnames.image, "REMRINGS", "yes", add+, ver- )

    }
    list = ""

    # Write the new list over the original input list
    delete( ilist )
    rename( olist, ilist )
}
;

logout( status )
