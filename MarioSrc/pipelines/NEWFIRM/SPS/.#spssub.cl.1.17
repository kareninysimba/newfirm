#!/bin/env pipecl
#
# SPSSUB - second pass sky subtraction

int	status = 1
int	offsetsky, num, runmedwindow, nimages, nimgs
int	objectpos, imageid, pointpos, ir, ix, iy, rad
real	stkscale, skymode, skymode2, seqno
string	beststk, stkbpm, obstype, ut, dataset, indir, lfile, datadir
string  match, bpm, ilist, sortedlist
string  prevout, previn, nocmpat, nocdpat

# Tasks and packages
images
tv
proto
imfilter
noao
nproto
task $findmatch = "$!findmatch $1 $2"
task $paste = "$!paste -d\  $1 $2 > $3"

# Set file and path names.
spsnames( envget("OSF_DATASET") )
dataset = spsnames.dataset

# Set filenames.
indir = spsnames.indir
datadir = spsnames.datadir
ilist = indir // dataset // ".sps"
lfile = datadir // spsnames.lfile
set (uparm = spsnames.uparm)
set (pipedata = spsnames.pipedata)

# Log start of processing.
printf( "\nSPSSUB (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Make sure the list is properly sorted
sortedlist = "sortedlist"
fields( ilist, "1", >> "spssub1.tmp" )
hsel( "@spssub1.tmp", "$I,NOCID", yes, >> "spssub2.tmp" )
# Paste the input list and the file just created into one
# file. osfn converts ilist into a format readable by the OS
paste( osfn(ilist), "spssub2.tmp", "spssub3.tmp" )
# Sort by the fourth column, containing NOCID
sort( "spssub3.tmp", column=4, numeric=yes, >> "spssub4.tmp" )
# Extract the first two columns
fields( "spssub4.tmp", "1-2", > sortedlist )
# Store the sorted list in the original input list
delete( ilist )
rename( sortedlist, ilist )
# Clean up
delete( "spssub*.tmp" )

# Go through the input list and add or update the STKBPM 
# keyword. Also, calculate the mode for all images if not
# previously determined.
list = ilist
count( ilist ) | scan( num )
offsetsky = 0
i = 0
while ( fscan( list, s1, s2 ) != EOF ) {

    # Inform user about the progress (mostly useful when run
    # from the command line)
    i = i+1
    printf( "Processing %d out of %d.\n", i, num )
    printf( "Image name: %s\n", s1 )

    # Read the obstype from the header, increase offsetsky
    # counter if obstype==sky
    obstype = ""
    hsel( s1, "OBSTYPE", yes ) | scan( obstype )
    if ( obstype == "sky" ) {
        offsetsky = offsetsky+1
    }

    # Set the output name for this exposure
    s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    spsnames( s3 )

    # Delete the mask if it already exists
    if ( imaccess( spsnames.mask ) ) 
        imdel( spsnames.mask )
    ;

    # Switch to WCS matching
    printf ("reset pmmatch = world\nkeep\n") | cl 
    # Match the stacked mask to the image in WCS space
    # mskexpr syntax: expression, output_mask ref_img ref_mask
    flpr
    mskexpr( "m", spsnames.obm, s1, refmask=s2//"[pl]" )
    # Retrieve the bad pixel mask from the header
    hsel( s1, "CBPM", yes ) | scan( bpm )
    # Combine the masks
    imexpr( "a+b>=1", spsnames.mask//".pl", bpm, spsnames.obm )
    # Remove intermediate step
#    imdel( spsnames.obm )
    # Add the name of the combined mask to the header
    hedit( s1, "STKBPM", spsnames.mask, add+, ver- )
    # And add the object mask as well
    hedit( s1, "OBJBPM", spsnames.obm, add+, ver- )

    # Switch to logical matching
    printf ("reset pmmatch = logical\nkeep\n") | cl 
    # Calculate the mean, masking out bad pixels
    # In the first pass, the mode is used, but now the objects
    # are masked, so the mean may give a better measurement
    # of the sky.
    mimstat( s1, fields="mean", nclip=3, lsigma=2, 
        usigma=2, format-, imasks="!STKBPM" ) | scan( skymode )
    # Store the mode in the header
    hedit( s1, "SKYMODE", skymode, add+, ver- )

}
list = ""

set pmmatch = "logical"   # Ensure pixel matching

# Read NOCMPAT, NOCDPAT, and IMAGEID from the last exposure in the
# sequence. These keywords should be the same for the entire sequence.
nocmpat = "" ; hsel( s1, "NOCMPAT", yes ) | scan( nocmpat )
nocdpat = "" ; hsel( s1, "NOCDPAT", yes ) | scan( nocdpat )
imageid = 0 ; hsel( s1, "IMAGEID", yes ) | scan( imageid )

# If NOCMPAT or NOCDPAT=Q (four-shooter mode), ignore offset sky exposures.
if ( ( nocmpat == "4Q" ) || ( nocdpat == "4Q" ) || ( nocdpat == "Q" ) ) {
    offsetsky = 0
}
;

if ( offsetsky == 0 ) {
    # Do sky subtraction by one-pass, in-field dithering. 
    print( "SKY SUBTRACTION WITH RUNMED (i.e., in-field dithering)" )
    match = "object"
} else {
    # Use offset exposures for sky subtraction.
    print( "SKY SUBTRACTION WITH OFFSET SKY EXPOSURES" )
    match = "sky"
}

if ( access( "skymode.tmp" ) )
    delete( "skymode.tmp" )
;
 
# First, separate object and sky exposures.
list = ilist
touch ("stksub1.tmp,skymode.tmp")
while( fscan( list, s1, s2 ) != EOF ) {

    # Set the output file names for this exposure
    s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    spsnames( s3 )

    # Read the obstype from the header
    obstype = ""
    hsel( s1, "OBSTYPE", yes ) | scan( obstype )

    if ( obstype == match ) {
        print( s1, >> "stksub1.tmp" )
        # Delete the output files if they already exists
        if ( imaccess( spsnames.sky ) )
            imdel( spsnames.sky )
        ;
	# Write the output file names to their lists	
	print( spsnames.sky, >> "stksub3.tmp" ) # sky images
	# Write the output file names to their lists	
	print( spsnames.holes//".pl", >> "stksub4.tmp" ) # sky images
        # Write the inverse of the mode to file for
        # use as scaling by runmed
        skymode = INDEF ; hsel( s1, "SKYMODE", yes ) | scan( skymode )
	if (isindef(skymode)) {
            printf( "ERROR: could not retrieve skymode for %s\n", s1 )
            status = 2
        }
	;
        x = 1/skymode
	print( x, >> "skymode.tmp" )
    }
    ;
    if ( obstype == "object" ) {
	print( s1, >> "stksub2.tmp" )
    } else {
	if ( obstype == "sky" )
            ;
        else {
	    print( "ERROR: Unknown value for obstype" )
            status = 3
	}
        ;
    }
}
list = ""

# If NOCMPAT=Q (four-shooter mode), then the exposures without the
# extended source in them become "offset" exposures, and the ones with
# the extended sky in them become the obejct exposures. Note that any
# true "offset" exposures have been removed from the list.
if ( ( nocmpat == "4Q" ) || ( nocdpat == "4Q" ) || ( nocdpat == "Q" ) ) {
    # Extended object moves through the extensions as follows:
    # first im2, then im1, then im3, and finally im4.
    if ( imageid == 1 )
        objectpos = 2
    else 
        if ( imageid == 2 )
            objectpos = 1
        else
            objectpos = imageid
    # Loop over the list of all object exposures
    list = "stksub2.tmp"
    while ( fscan( list, s1 ) != EOF ) {

        # Determine whether the extended source is in this exposure
        # First read nocmpos/nocdpos, which indicate the position of the
        # current exposure within the mapping pattern.
        if ( nocmpat == "4Q" ) {
            pointpos = 0 ; hsel( s1, "NOCMPOS", yes ) | scan( pointpos )
        } else {
            pointpos = 0 ; hsel( s1, "NOCDPOS", yes ) | scan( pointpos )
        }
        if ( pointpos == objectpos ) {
            # This exposure has an extended object, so add it to
            # the object list.
            print( s1, >> "_stksub2.tmp" )
            hedit( s1, "FSOBJECT", "yes", add+, ver- )

            # The skylevel is redetermined, because the value
            # derived above was based on the entire image. In 4Q mode,
            # there may be a large object present, which would bias
            # the sky level to higher values, leading to too low
            # sky levels in the sky-subtracted data.

            # Get the size of the sky image
            hsel( s1, "NAXIS1,NAXIS2", yes ) | scan( ix, iy )
            # Determine the radius of the circle in pixels
            ir = ix
            if ( iy < ir )
                ir = iy
            ;
            # Divide by 2 (diameter to radius) and 100 (percent to fraction)
            rad = ir*sky_4qradius/200
            ix = ix/2
            iy = iy/2
            if ( access( "skysub1.inp" ) )
                delete( "skysub1.inp" )
            ;
            printf( "%d %d 0 e\n", ix, iy, > "skysub1.inp" )
            # Create a mask with good values outside of this circle
            # centered on the middle of the image. First get the
            # existing mask
            hsel( s1, "STKBPM", yes ) | scan( s2 )
            if ( imaccess( "tmpmask.pl" ) )
                imdel( "tmpmask.pl" )
            ;
            # Mark the circle as bad
            imedit( s2, "tmpmask.pl", cursor="skysub1.inp", radius=rad,
                value=1, display- )
            delete( "skysub1.inp" )
            # Calculate the mean, masking out bad pixels
            mimstat( s1, fields="mean", nclip=3, lsigma=2, 
                usigma=2, format-, imasks="tmpmask.pl" ) | scan( skymode )
            imdel( "tmpmask.pl" )
            # Update the mode in the header
            hedit( s1, "SKYMODE", skymode, update+, ver- )
 
        } else {
            # This exposure does not have the extended object, so add
            # it to the runmed list, and the output filename list.
            print( s1,  >> "_stksub1.tmp" )
            # Set the output file names for this exposure
            s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
            spsnames( s3 )
            # Write the output file names to their lists	
            print( spsnames.sky, >> "_stksub3.tmp" )
            # Write the hole mask file names to their lists	
            print( spsnames.holes//".pl", >> "_stksub4.tmp" )
            # Write the inverse of the mode to file for
            # use as scaling by runmed
            skymode = INDEF ; hsel( s1, "SKYMODE", yes ) | scan( skymode )
            x = 1/skymode
	    print( x, >> "_skymode.tmp" )
            # The sky should also be subtracted from the extensions
            # without the extended object, so add it to the object
            # list as well.
            print( s1, >> "_stksub2.tmp" )
            hedit( s1, "FSOBJECT", "no", add+, ver- )
        }

    }
    list = ""

    # Reset the lists to newly created ones
    delete( "stksub1.tmp,stksub2.tmp,stksub3.tmp,stksub4.tmp,skymode.tmp" )
    rename( "_stksub1.tmp", "stksub1.tmp" )
    rename( "_stksub2.tmp", "stksub2.tmp" )
    rename( "_stksub3.tmp", "stksub3.tmp" )
    rename( "_stksub4.tmp", "stksub4.tmp" )
    rename( "_skymode.tmp", "skymode.tmp" )

}
;

# Get the number of images
nimages = 0; count( "stksub1.tmp" ) | scan( nimages )

# Check whether enough images are available, for the case of in-field
# dithering.
if ((nimages<sky_minrunmedwin)&&(offsetsky==0)) {

    if (nimages<2) {

        # Sky subtraction cannot be done, but produce output anyway so that
        # the downstream pipelines can continue. A rough sky subtraction
        # will be done in the SPI pipeline where the ace sky image is 
        # subtracted.
        sendmsg( "WARNING",
	    "Too few exposures, cannot do sky subtraction",
    	    "int(nimages)", "VRFY" )
        print( "TOO FEW EXPOSURES (%d), NO SKY SUBTRACTION", nimages )
        list = ilist
        if ( fscan( list, s1 ) != EOF ) {
            # Get the part of the filename after the last /, strip the .fits
            s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
            # Set the sky variables for this image
            spsnames( s2 )
            # Copy the unskysubtracted image
	    imcopy( s1, spsnames.skysub )
        }
        list = ""

    } else {

        sendmsg( "WARNING",
	    "Too few images for running median, using pairwise skysubtraction",
    	    "", "VRFY" )
        print( "PAIRWISE SKY SUBTRACTION" )
        list = ilist
        for( nimgs=1; fscan(list,s1)!=EOF; nimgs+=1 ) {
            # Get part of the filename after the last /, stripping the .fits
            s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
            # Set the sky variables for this image
            spsnames( s2 )
            # If nimgs>1, i.e., we have a pair, subtract them
            print( nimgs )
            if (nimgs>1) {
	        imarith( previn, "-", s1, prevout )
            }
            # Store the output file name for the next item on the list. This
            # has to be done here so that prevout is set to the current item
            # for the "reverse" subtraction for the last item in the list.
            prevout = spsnames.skysub
            # Do a reverse subtraction for the last image in the list
            if (nimgs==nimages) {
                imarith( s1, "-", previn, prevout )
            }
            # Store the current input file name to create the second half
            # of the pair for the next item on the list.
            previn = s1
        }
        list = ""
        delete( "skysub1.tmp,skysub2.tmp" )

    }

    logout 1

}
;

# Do the sky subtraction for offset sky exposures and there are too
# few images for runmed
if ((nimages<sky_minrunmedwin)&&(offsetsky>0)) {

    if (nimages<2) {

        # With only one image, the image itself will have to be the
        # sky image.
        imcopy( "@stksub1.tmp", "@stksub3.tmp" )

    } else {

        # Only a few images are available, combine them to create
        # a somewhat source-free image.
	# Use the name of the first image in the list as the output
        # file name.
        head( "@stksub3.tmp", nlines=1 ) | scan( s1 )
        imcombine( "@stksub1.tmp", s1, imcmb="$I",
            logfile="STDOUT", combine="average", reject="minmax", 
            offsets="none", weight="!oexptime", expname="oexptime",
            nlow=0, nhigh=1, nkeep=1, grow=1. )
        delete( "stksub3.tmp" )
        printf( "%s\n", s1, >> "stksub3.tmp" )

    }

}
;

# Limit size of runmedwindow
runmedwindow = nimages
if (runmedwindow>sky_maxrunmedwin)
    runmedwindow = sky_maxrunmedwin
;

if (nimages>=sky_minrunmedwin) {
    # Create the sky images from the offset exposures from the offset
    # exposures with the objects in the stacked image masked out. Note
    # that the mask is not applied here because the "fixpixed" bad
    # pixels should also be sky subtracted.
    set pmmatch = "logical"
    runmed( "@stksub1.tmp", "@stksub3.tmp", window=runmedwindow,
        inmaskkey="OBJBPM", scale="@skymode.tmp", outtype="filter",
        outscale-, normscale-, exclude+, nclip=2., masks="@stksub4.tmp",
        outmaskkey="HOLES" )
}
;

# Calculate the mode in the newly created sky images
list = "stksub3.tmp"
set pmmatch = "physical"
while( fscan( list, s1 ) != EOF ) {
    # Calculate the mode, masking out bad pixels in WCS space
    mimstat( s1, fields="midpt", nclip=3, lsigma=2, 
        usigma=2, format-, imasks="!STKBPM" ) | scan( skymode )
    # Store the mode in the header
    hedit( s1, "SKYMODE", skymode, add+, ver- )
    }
list = ""
set pmmatch = "logical"

# Create a list with UTs of the offset sky exposures.
# This is used below to find the best matching sky exposure
hselect( "@stksub3.tmp", "$I,NOCID", yes, >> "stksub5.tmp" )

# Now loop over all object exposures and subtract the 
# relevant sky image
list = "stksub2.tmp"
while( fscan( list, s1 ) != EOF ) {

    # Set the output file names for this exposure
    s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    spsnames( s3 )

    # Retrieve sequence number from the header ...
    hselect( s1, "NOCID", yes ) | scan( seqno )
    # ... and find the sky closest to the current sequence number
print "---"
findmatch( "stksub5.tmp", seqno )
    findmatch( "stksub5.tmp", seqno ) | scan( beststk )
print( s1 )
print( seqno )
print( beststk )
print "---"

    # Retrieve the sky level in the object and sky images
    # from the headers
    hselect( s1, "SKYMODE", yes ) | scan( skymode )
    hselect( beststk, "SKYMODE", yes ) | scan( skymode2 )

    # Scale the sky image to match the mode of the object image
    stkscale = skymode/skymode2
    imarith( beststk, "*", stkscale, "tmpscaledsky" )
    # Delete the output sky subtracted file if it already exists
    if ( imaccess( spsnames.skysub ) ) {
        imdel( spsnames.skysub )
    }
    ;
    # Subtract the selected sky image from the current image
    imarith( s1, "-", "tmpscaledsky", spsnames.skysub  )
    # Report how the sky was subtracted:
    printf( "Sky:    %s with mode %f\n", beststk, skymode2 )
    printf( "Object: %s with mode %f\n", s1, skymode )
    # Copy the "holes" mask from the sky image to the sky subtracted
    # image
    hselect( beststk, "HOLES", yes ) | scan( s3 )
    if (nscan()==1)
        hedit( spsnames.skysub, "HOLES", s3, add+, ver- )
    ;
    # Clean up
    imdel( "tmpscaledsky" )

}

if (status>1) {
    printf( "An error occurred, the exit code is %i.\n", status )
}
;

# Clean up sky images
imdel( "@stksub3.tmp" )

# Restore default behavior for mask matching
set     pmmatch         = "logical"

logout( 1 )

#TODO: sky subtraction output files should not be in SGC pipeline
