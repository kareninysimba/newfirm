#!/bin/env pipecl
#
# MTDSTATS

int	status = 1
int     namps,numon,numoff
real	mean, x1, x2, diff
real    dqavfsat,dqavfsat1,dqavfsat2,dqmxfsat,dqmxfsat1,dqmxfsat2
string  datadir, dataset, iliston, ilistoff, image, indir, lfile, ifile
string	cal, calon, caloff, ilistb, bpm, flatinfo

# Tasks and packages
images
mscred
tables
proto

# Set file and path names.
sflnames (envget("OSF_DATASET"))
dataset = sflnames.dataset

# Set filenames.
indir = sflnames.indir
datadir = sflnames.datadir
iliston = indir // dataset // "on.tmp"
ilistoff = indir // dataset // "off.tmp"
ilistb = indir // dataset // "both.tmp"
lfile = datadir // sflnames.lfile
set (uparm = sflnames.uparm)
set (pipedata = sflnames.pipedata)
cal = sflnames.cal
calon = sflnames.calon
caloff = sflnames.caloff

# Log start of processing.
printf ("\nSFLCOMBINE (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

numon = 0
if ( access(iliston) ) {
    count( iliston ) | scan(numon)
    if (numon>0) 
        imcombine( "@"//iliston, calon, offset="physical",
            reject="avsigclip" )
    ;
    hsel( "@"//iliston, "DQARFSAT", yes, >> "sflcombine1.tmp" )
    tstat( "sflcombine1.tmp", "c1", low=INDEF, high=INDEF )
    dqavfsat1 = tstat.mean
    dqmxfsat1 = tstat.vmax
}
;
numoff = 0
if ( access(ilistoff) ) {
    count( ilistoff ) | scan(numoff)
    if (numoff>0)
        imcombine( "@"//ilistoff, caloff, offset="physical",
            reject="avsigclip" )
    ;
    hsel( "@"//ilistoff, "DQARFSAT", yes, >> "sflcombine2.tmp" )
    tstat( "sflcombine2.tmp", "c1", low=INDEF, high=INDEF )
    dqavfsat2 = tstat.mean
    dqmxfsat2 = tstat.vmax
}
;
if ( numon>0 && numoff>0 ) {
    print "DATA FOR BOTH"
    x1 = 0
    x2 = 0
    imstat( calon, fields="mode", nclip=3, lsigma=2,
        usigma=2, format- ) | scan( x1 )
    imstat( caloff, fields="mode", nclip=3, lsigma=2,
        usigma=2, format- ) | scan( x2 )
    diff = abs(x1 - x2)
    if ( diff<sfl_mincounts ) {
        printf( "DATA ARE TOO SIMILAR, difference is: %f\n", diff )
	sendmsg( "WARNING", "Difference too small (" // str(diff) // ")",
	    dataset, "CAL" )
        status = 2
    } else {
        if ( x1>x2 ) {
            imarith( calon, "-", caloff, cal )
            dqavfsat = dqavfsat1
            dqmxfsat = dqmxfsat1
        } else {
            imarith( caloff, "-", calon, cal )
            dqavfsat = dqavfsat2
            dqmxfsat = dqmxfsat2
        }
	# The 'cal' image now only has the IMCMB keywords for either
	# the on or the off, but it needs to have both.
	# Delete the existing IMCMB keywords
	nhedit( cal, "IMCMB*", ".", ".", update+, delete+, show+ )
	# Concatenate on and off lists
	concat( iliston, > "sflcombine3.tmp" )
	concat( ilistoff, >> "sflcombine3.tmp" )
	# Loop over this list
	i = 1
	list = "sflcombine3.tmp"
	while ( fscan( list, s1 ) != EOF ) {
	    j = stridx( "_", s1 )
	    if ( j > 0 )
		s1 = substr( s1, 1, j-1 )
	    ;
	    s1 = s1 // ".fits"
	    printf( "%03d\n", i ) | scan( s2 )
	    s2 = "IMCMB" // s2
	    hedit( cal, s2, s1, add+, ver-, show+ )
	    i = i+1
	}
	list = ""
    }
    flatinfo = "Difference between on and off domeflat"
} else {
    if (numon>0) {
	print "CALON only"
        dqavfsat = dqavfsat1
        dqmxfsat = dqmxfsat1
        x1 = 0
        imstat( calon, fields="mode", nclip=3, lsigma=2,
            usigma=2, format- ) | scan( x1 )
        if (x1<sfl_mincounts) {
            print "INSUFFICIENT COUNTS IN ON-ONLY FLAT"
	    imdel( "@"//iliston )
            imdel( calon )
            status = 2
        } else {
            imcopy( calon, cal )
        }
	flatinfo = "Not a difference flat / lamp status unknown"
    }
    ;
    if (numoff>0) {
	print "CALOFF only"
        dqavfsat = dqavfsat2
        dqmxfsat = dqmxfsat2
        x1 = 0
        imstat( caloff, fields="mode", nclip=3, lsigma=2,
            usigma=2, format- ) | scan( x1 )
        if (x1<sfl_mincounts) {
            print "INSUFFICIENT COUNTS IN OFF-ONLY FLAT"
	    imdel( "@"//ilistoff )
	    imdel( caloff )
            status = 2
        } else {
            imcopy( caloff, cal )
        }
	flatinfo = "Not a difference flat / lamp status unknown"
    }
    ;
    if ( numon==0 && numoff==0 ) {
	status = 2
	print "NO DATA"
    }
    ;
}

if (status==1) {

    hedit( cal, "DQAVFSAT", dqavfsat, add+, ver- )
    hedit( cal, "DQMXFSAT", dqmxfsat, add+, ver- )

    # Combine the on and off lists to make sure there is a valid entry
    copy( iliston, ilistb )
    concat( ilistoff, ilistb, append+ )
    # Retrieve the BPM keyword from the first image in the list ...
    list = ilistb
    i = fscan( list, image )
    list = ""
    hselect( image, "BPM", yes ) | scan( bpm )
    # ... and add it to the combined calibration 
    hedit( cal, "BPM", bpm, add+, ver- )

    # Add to the header how the flat was derived
    hedit( cal, "FLATINFO", flatinfo, add+, ver- )

    # Interpolate over all bad pixels to produce cosmetically nice
    # images.
    fixpix( cal, "BPM" )

}
;

logout( status )
