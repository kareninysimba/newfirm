#!/bin/env pipecl
#
# SFLCALCHECK -- Check for calibrations in waiting datasets.
#
# This module finds datasets with the start flag set to 'w'.  It then
# checks if the dataset has calibrations available.  If the calibrations
# are available it retriggers the dataset.  This routine runs
# under a timer.

int     status = 1
int     num
struct  statusstr, mask_name
string  dataset, indir, datadir, ifile, im, lfile, host, dset

string	subdir
file	caldir = "MC$"

# Tasks and packages
task $osf_update = "$!osf_update -a $6 -h $1 -p $2 -f $3 -c $4 -s $5"
task $osf_test = "$osf_test -a $4 -p $1 -c $2 -s $3 > $5"

# get the dataset name
sflnames( envget("OSF_DATASET") )
dataset = sflnames.dataset
images
servers

# Set paths and files.
indir = sflnames.indir
ifile = indir // dataset // ".sfl"
datadir = sflnames.datadir
lfile = datadir // sflnames.lfile
set (uparm = sflnames.uparm)
set (pipedata = sflnames.pipedata)
cd (datadir)

# Log start of processing.
printf( "\nSFLCALCHECK (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

osf_test( "sfl", 1, "w", "NEWFIRM", "sflcalcheck.tmp" )
list = "sflcalcheck.tmp"
num = 0
while ( fscan( list, s1 ) != EOF ) {
    dset = substr (s1, stridx(".",s1)+1, 1000)
    dset = substr (dset, 1, strstr("__", dset)-1)
    # Extract the host name
    host = substr( s1, strldx( ":", s1 )+1, 1000 )
    host = substr( host, 1, stridx( "_", host)-1 )
    # Create the path to the input file 
    s2 = host//"!NEWFIRM_SFL$/input/"//dset//".sfl"
    # Get the first line of the input file
    head( s2 ) | scan( s3 )
    # Run the getcal
    getcal( s3, "dark", cm, caldir,
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="!exptime", dexptime=0.05,
	mjd="!mjd-obs" ) | scan( i, statusstr )
    if ( i==0 ) {
        # Calibration data have become available, so retrigger
        # the relevant dataset, which is done by setting the
        # OSF flag for sflstart to p and the one for sflsetup
        # to _.
        printf( "UPDATING: %s %s\n", host, dset )
        osf_update( host, "sfl", dset, 1, "p", "NEWFIRM" )
        osf_update( host, "sfl", dset, 2, "_", "NEWFIRM" )
    } else {
        # Increase the counter of pending datasets
        num = num+1
    }
}
list = ""
delete( "sflcalcheck.tmp" )

if (num==0) { # No entries are waiting, so exit with success 
    status = 1
} else {
    # There are still entries waiting, so keep going. One
    # complication is that the calibration data may have been
    # found for this dataset. If that is the case, this particular
    # instance of sflcalcheck.cl will stop being time-triggered
    # when the sflsetup.cl completes successfully. However, other 
    # datasets that are still pending will then have their
    # sflcalcheck.cl running periodically, so checking for 
    # pending datasets will continue until all have been processed.
    status = 4
}

logout( status )
