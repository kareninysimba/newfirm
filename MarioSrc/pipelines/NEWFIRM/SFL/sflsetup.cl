#!/bin/env pipecl
#
# SFLsetup

int     status = 1
real	exptime, dexptime
struct  statusstr, mask_name
string  dataset, indir, datadir, ifile, im, lfile, host, mjd
string	subdir
file	caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
sflnames( envget("OSF_DATASET") )
dataset = sflnames.dataset

# Set paths and files.
indir = sflnames.indir
ifile = indir // dataset // ".sfl"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
	caldir = caldir // subdir

datadir = sflnames.datadir
lfile = datadir // sflnames.lfile
set (uparm = sflnames.uparm)
set (pipedata = sflnames.pipedata)
cd (datadir)

# Log start of processing.
printf( "\nSFLSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Setup uparm and pipedata
delete( substr( sflnames.uparm, 1, strlen(sflnames.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;
    
# Check type of linearization to be done.  ltype="linconst" for
# constant linearity coefficients, while ltype="linimage" for 2D linearity
# coefficients.
if ( (lincoeftype != "linconst") && (lincoeftype != "linimage") ) {
    s2 = "lincoeftype "//lincoeftype//" not recognized, using linimage"
    sendmsg( "WARNING", s2, dataset, "VRFY" )
    lincoeftype = "linimage"
} else {
    if ( lincoeftype == "linconst" ) {
        printf( "\nUsing constant linearity coefficient.\n" ) | tee( lfile )
    } else {
        printf( "\nUsing 2D linearity coefficient.\n" ) | tee( lfile )
    }
}

# Get the calibrations (i.e., darks and linearity coefficients) from the
# calibration database
list = ifile
while ( fscan (list, s1) != EOF ) {

    mjd = "54500"
    hselect (s1, "MJD-OBS", yes) | scan (mjd)
    getcal (s1, "bpm", cm, caldir,
	obstype="", detector="!instrume", imageid="!detector",
	filter="", exptime="", mjd=mjd) | scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;

    # Try to retrieve the required dark from the database
    if (sfl_nodarkcont == NO) {
        # Use default dexptime if darks are required
        dexptime = 0.05
    } else {
        # If darks are not required, use larger range to increase
        # chances of finding any dark
        dexptime = 1.0
    }
    getcal( s1, "dark", cm, caldir,
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="!exptime", dexptime=0.05,
	mjd=mjd ) | scan( i, statusstr )
    if (i != 0) { # Desired dark was not found in the calibration database
        if (sfl_nodarkcont == NO) {
            sendmsg( "ERROR", "Getcal failed for dark", s1, "CAL" )
            status = 0
            break
        }
    }
    ;

    # Check whether the getcal works for retrieving linearity coefficient
    getcal( s1, lincoeftype, cm, caldir, obstype="", detector="!instrume",
        imageid="!detector", filter="", exptime="", mjd=mjd )
    if ( getcal.statcode > 0 ) {
        # Getcal was not successful
        s2 = "Getcal failed - no linearity coefficient retrieved"
        sendmsg( "ERROR", s2, s1, "CAL" )
        status = 0
        break
    } else {
        # Getcal was successful
        # If constant linearity coefficient is used, write value of LINCONST
        # keyword to LINCOEFF keyword.  If 2D linearity coefficient is used,
        # write value of LINIMAGE keyword to LINCOEFF keyword.  
        # It is *IMPORTANT* that LINCOEFF has the appropriate value because
        # that keyword is read in sflproc.cl to help identify saturated
        # pixels.
        s2 = ""; hselect( s1, lincoeftype, yes ) | scan( s2 )
        if (s2 != "") {
            hedit( s1, "LINCOEFF", s2, add+, addonly-, delete-, verify-,
		show+, update+ )
        } else {
            s2 = lincoeftype//" keyword not found"
            sendmsg( "ERROR", s2, s1, "CAL" )
            status = 0
            break
        }
    }

    getcal( s1, "saturate", cm, caldir, 
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="", mjd=mjd )
    if ( getcal.statcode == 0 ) {
        getcal( s1, "sateq", cm, caldir, 
            obstype="", detector="!instrume", imageid="!detector",
            filter="", exptime="", mjd=mjd)
    }
    ;
    if ( getcal.statcode > 0 ) { # Getcal was not successful
        sendmsg( "ERROR",
            "Getcal failed - no saturation information retrieved",
            s1, "CAL" )
        status = 0
        break
    }
    ;

}
list = ""

printf( "Exit status: %d\n", status )
logout( status )
