#!/bin/env python

"""
This script can be run in one of three ways. If procmode==add, then a
new exposure is added to the current list. If the new exposure
completes a sequence, the relevant pipeline is triggered with the
list that has been built, without adding the current exposure. If
procmode==end, this means that a 'end sequence' trigger has been
received, in which case the relevant pipeline will be triggered and
no exposure is added to the current list. If procmode==flush, no
end-of-sequence trigger has been received but processing is forced.
This should only be done when the pipeline is not receiving data, e.g.,
at the end of a night or when the telescope closes due to weather.

For debugging purposes, a third option is also recognized. If
procmode==nothing, then mtdgroup immediately returns exit status 0,
indicating a normal completion.

                        ===== TODO =====

- This module needs to be time triggered occasionally, to detect
  sequences that are incomplete due to aborted observations
- What to do if data comes in out of sequence? Unlikely to happen?
  If this happens in a list of observations, these can be sorted
  using the observation time from the header. One can also check
  to see whether the sequence id (does this exist?) has changed.
  In that case, the sequence can continue to be built until the
  sequence id changes, and the list can be time sorted before
  triggering a pipeline.
"""

# Import python modules
import datetime
import time
import os
import sys
import pyfits as p
#import re
#import sqlite
#from sqlutil import *
#from sendmsg import *

DATASET_PREV = 'mtddataset.prev'
VERBOSE      = True

# Wrapper function around send_message
def sendmsg( status, message, submsg, id, dset ):
    omsg = message
    if (len(submsg)>0):
        message += ' ('+submsg+')'
    print message
    return omsg

def callpipe( obs, indir, host, dataset ):
    os.environ ['OSF_DATASET'] = obs.first
    # Make sure the relevant directories exist. If mtdgroup.py
    # is used under normal operations, i.e., if it is triggered
    # with one exposure at a time, the directories will have been
    # created by mtdstart.cl. However, if mtdgroup.cl is triggered
    # with a list of observations, it is possible the relevant
    # mtdstart.cl was not run. Here, we check whether what is
    # needed is present, and we create it if it is not. This
    # is done with init.cl, a modified version of start.cl.
    os.system( 'init.cl mtd' )

    cmdline = 'call.cl mtd %s split \"' + '%s!%s%s.lol\" 999' % ( host, indir, obs.dataset )
    if VERBOSE:
        print 'Callpipe command:',
        print cmdline

    move_to = ''
    if obs.obstyp == 'dark':
        os.system( cmdline % ('drk') )
        move_to = os.path.join( os.environ['NEWFIRM_DRK'], 'data',\
                              '%s-drk%s' % (dataset,obs.dataset) )
    elif obs.obstyp == 'dflaton':
        os.system( cmdline % ('fil') )
        move_to = os.path.join( os.environ['NEWFIRM_FIL'], 'data',\
                              '%s-fil%s' % (dataset,obs.dataset) )
    elif obs.obstyp == 'object':
        os.system( cmdline % ('gos') )
        move_to = os.path.join( os.environ['NEWFIRM_GOS'], 'data',\
                              '%s-gos%s' % (dataset,obs.dataset) )
#    elif obs.obstyp == 'focus':
#        os.system( cmdline % ('gos') )
#        move_to = os.path.join( os.environ['NEWFIRM_GOS'], 'data',\
#                              '%s-gos%s' % (dataset,obs.dataset) )

    # Move the return file
    move_to = os.path.join( os.environ['NEWFIRM_DTS'], 'input',\
                            '%s-gos%s-dts.odir' % (dataset,obs.dataset) )
    print( move_to )
    if len(move_to)>0:
        move_from = '/tmp/%s.URL' % (dataset)
        if VERBOSE:
            print 'Move from: %s' % (move_from)
            print 'Move to: %s' % (move_to)
#        while not os.path.exists( move_to ):
        time.sleep( 1 )
        os.system( 'sleep 0 ; mv %s %s' % (move_from, move_to) )

    return

"""
# define a class for simultaneous output to file and stdout
# TODO: make this importable, so that one can do 'import pipeline' above
class DualOut:
    def __init__(self):
        self.content = []
    def write(self, string):
        ofile1 = open( lfile, 'a', 0 )
        ofile1.write(string)
        sys.stdout.write(string)
        ofile1.close()
tee = DualOut()

# Print standard start of the processing
#print >> tee, '\nMTDGROUP %s: ' % ( dataset )
#print >> tee, datetime.datetime.today () # TODO: change format to match cl

# Go to the relevant directory
os.chdir( indir+'data' )
"""

class obsInfo:
    def __init__( self, fnPrev=None ):
        self.reset()
        if fnPrev:
            self.initFromFile( fnPrev )
        return
    
    def initFromFile( self, fnPrev ):
        try:
            infile = open( fnPrev, 'r' )
        except:
            if VERBOSE:
                print 'File %s does not exist yet' % (fnPrev)
            return
        try:
            self.dataset = infile.readline().strip()
            self.seqno   = int( infile.readline().strip() )
            self.first   = infile.readline().strip()
            self.prev    = infile.readline().strip()
            self.obsid   = infile.readline().strip()
            self.obstyp  = infile.readline().strip()
            self.obstot  = int( infile.readline().strip() )
            self.obsno   = int( infile.readline().strip() )
        except:
            print 'Unable to read contents of (%s)' % fnPrev
        return
    
    def reset( self ):
        self.dataset = 'none'
        self.seqno   = -1
        self.first   = 'none'
        self.prev    = 'none'
        self.obsid   = 'none'
        self.obstyp  = 'none'
        self.obstot  = -1
        self.obsno   = -1
        return
    
    def getContents( self ):
        return[ self.dataset,
                self.seqno,
                self.first,
                self.prev,
                self.obsid,
                self.obstyp,
                self.obstot,
                self.obsno ]
    
    def writeToFile( self, fnPrev ):
        outfile = open( fnPrev, 'w' )
        for value in self.getContents():
            outfile.write( '%s\n' % (value) )
        outfile.flush()
        outfile.close()
        return

    def output( self ):
        for value in self.getContents():
            print value
        return        

def add( prevObs, indir, dataset, hostname, trigger ):
    
    print '\nMTDGROUP %s: ' % ( dataset ),
    print datetime.datetime.today()

    # Add one or more exposures to the current list, trigger when necessary
    
    # Open the input file list
    fName  = os.path.join( indir, '%s.mtd' % (dataset) )
    infile = open( fName, 'r' )
    # Read the file into memory...
    lines = infile.readlines()
    # Close the input file...
    infile.close()

    # Read the first line to get the name for the current list. This
    # will be used as the name for the dataset that will be sent
    # to child pipelines. It is stored in p_dataset as well.
    firstfile = os.path.basename( lines[0].strip() )
    if '.fits' == firstfile[-5:]:
        firstfile = os.path.splitext(firstfile)[0]
    
    # Split the dataset around the '-' into a sequence 
    pos   = dataset.rfind( '-' )
    seqid = dataset[:pos]
    expid = dataset[pos+1:]
    
    # Move the return file - URL stands for User Requested Location :)
    move_from = os.path.join( os.environ['NEWFIRM_MTD'], 'input/return',\
                              '%s.return' % (dataset) )
    move_to = '/tmp/%s.URL' % (seqid)
    os.system( 'mv %s %s' % (move_from, move_to) )

    if VERBOSE:
        print "Sequence ID: %s" % seqid
        print "Exposure ID: %s" % expid
    
    if prevObs.dataset == 'none':
        # The name of the dataset is not yet set...
        prevObs.seqno   = 1
        prevObs.dataset = expid
        prevObs.first   = seqid

    # Generate filenames...
    fnCurr = os.path.join( indir, '%s.cur' % (prevObs.dataset) )
    fnLol  = os.path.join( indir, '%s.lol' % (prevObs.dataset) )
    
    # Open the current list
    curlist = open( fnCurr, 'a', 0 )
    
    # If it does not yet exist, open the list of lists and write
    # the filename of curlist to it
    if not os.path.exists( fnLol ):
        if VERBOSE:
            print 'The list of lists does not exist yet, creating...'
        lol = open( fnLol, 'w', 0 )
        lol.write( '%s!%s\n' % (hostname, fnCurr) )
        lol.flush()
        lol.close()
    
    # Loop over all files in the list
    # Note that it is assumed that all data sets are local
    for line in lines:
        datasetok = True
        thisline  = line.strip()
        if VERBOSE:
            print 'Input line from list: %s' % (thisline)
        
        # Make sure file is on local host
        fields = thisline.split('!')
        if len(fields) == 2:
            # The part before the ! is the hostname, the rest is the filename
            # TODO next line can cause trouble if there is a . in the filename
            filehost = fields[0].split( '.', 1)[0]
            thisfile = fields[1]
        elif len( fields ) == 1:
            thisfile = thisline
            filehost = hostname
        else:
            # More than one ! in the file name
            sendmsg( 'ERROR', 'Input file name garbled', thisline, 'VRFY',
                     dataset )
            sys.exit(1)
        
        if filehost != hostname:
            sendmsg( 'ERROR', 'Host name in input list is not this host',
                     filehost, 'VRFY', dataset )
            sys.exit(1)
        
        # Make sure the file ends in .fits
        thisfile, ext = os.path.splitext( thisfile )
        if ext != '.fits':
            ext = '.fits'
        thisfile += ext
        
        # Get the short file name
        shortfile = os.path.basename( thisfile )
        
        if VERBOSE:
            print "==== Processing %s ====" % (thisfile)
        
        # Read the relevant keywords from the header
        # Open the fits file
        try:
            hdu = p.open( thisfile, 'update' )
        except:
            msg = sendmsg( 'WARNING', 'Could not open fits file',
                           thisfile, 'VRFY', shortfile )
            datasetok = False
        else:
            try:
                # Read the header
                prihdr = hdu[0].header
            except:
                msg = sendmsg( 'WARNING', 'Could not open fits header',
                               thisfile, 'VRFY', shortfile )
                datasetok = False
                hdu.close( output_verify='ignore' )
        
        # noctot nocno        # total number of observations requested
        # obstype             # observation type
        # What breaks a sequence:
        # - nocno does not increase
        # - observation type changes
        # - number of expected observations (noctot) changes
        # To be flagged:
        # - incomplete sequence
        # TODO: filter change breaks sequence
        
        if datasetok:
            currObs = obsInfo()
            # Get the relevant keywords from the header
            currObs.obstyp = prihdr['noctyp'].lower()
            currObs.obstot = prihdr['noctot']
            currObs.obsno  = prihdr['nocno']
            # Remember the current expid
            currObs.prev = expid

            if currObs.obstyp == 'dflaton':
                pass
            elif currObs.obstyp == 'dflats':
                # dflats is a sequence of dflaton and dflatoff
                currObs.obstyp = 'dflaton'
            elif currObs.obstyp == 'dflatoff':
                # dflaton and dflatoff are part of one sequence,
                currObs.obstyp = 'dflaton'
            elif currObs.obstyp == 'tflat':
                # twilights exposures are treated as dome flats for now
                currObs.obstyp = 'dflaton'
            elif currObs.obstyp == 'sky':
                # sky is part of an on/off sky/object sequence
                currObs.obstyp = 'object'
            elif currObs.obstyp == 'dark':
                pass
            elif currObs.obstyp == 'focus':
                pass
            else:
                # anything else is an object
                currObs.obstyp = 'object'

            # Assume the sequence is OK unless proven otherwise
            status = 1
            
            if VERBOSE:
                print 'BEGIN contents of current image'
                print currObs.first,  prevObs.first
                print currObs.obstot, prevObs.obstot
                print currObs.obsno,  prevObs.obsno
                print currObs.obstyp, prevObs.obstyp
                print 'END contents of current image'
            
            # Compare current keywords to the previous ones, if
            # trigger is true
            
            if trigger:

                # Check whether prevObs has been set. Only the
                # presence of prevObs.prev is checked, the
                # others should be defined if this one is;
                # note that dataset, seqno, and first are
                # defined earlier
                if prevObs.prev != 'none':

                    # If the seqid has changed, the sequence is broken
                    if prevObs.first != seqid:
                        status = 0
                        print "Break found: seqid changed"

                    # prevObs.obstot must be the same as currObs.obstot,
                    # otherwise the sequence is broken
                    if currObs.obstot != prevObs.obstot:
                        status = 0
                        print "Break found: obstot changed"
            
#                    if currObs.obsno <= prevObs.obsno:
                    if (currObs.obsno<=prevObs.obsno) and (currObs.obstyp=='object'):
                        # if obsno did not increase
                        # then the sequence is broken
#                        if currObs.obsno == prevObs.obsno and currObs.obsid == prevObs.obsid:
#                            # unless it is the same observation that
#                            # was mistakenly fed to the pipeline
#                            status = -1 # set a flag to ignore this observation
#                            msg = sendmsg( 'WARNING', 'Duplicate observation will be ignored',
#                                           thisfile, 'VRFY', shortfile )
#                        else:
                        status = 0
                        print "Break found: obsno did not increase"
                    elif currObs.obsno - prevObs.obsno > 1:
                        # if obsno increased by more than 1
                        # set flag to indicate missing observation
                        status = 2
                        msg = sendmsg( 'WARNING', 'Observations missing', '', 'VRFY', shortfile )
            
                    # if the obstyp is not the same
                    # then the sequence is broken
                    if currObs.obstyp != prevObs.obstyp:
                        status = 0
                        print "Break found: obstype changed"

            # End of "if trigger"

            # Reinitialize the current list and trigger the relevant pipeline
            if status == 0:
	        print 'WARNING: break detected'

                # the sequence has been terminated
                if VERBOSE:
                    print 'Reinitializing the list'
                
                curlist.close() # close the current list
                
                # Trigger the relevant pipeline, based on prevObs.obstyp
                print 'Now the %s pipeline will be triggered' % prevObs.obstyp

                cmdline = 'osf_update -a NEWFIRM -p mtd -s b -c 8 -f %s-%s' % ( prevObs.first, prevObs.prev )
                print cmdline
                os.system( cmdline )

                # There is no guarantee that mtdendseq (which will
                # executes "mtdgroup.py end") will run immediately
                # following the osf_update directly above. This means
                # that it is possible that mtdgroup.py will run again
                # on other datasets before the relevant pipeline is
                # triggered by mtdendseq. In that case, the contents
                # of the DATASET_PREV file will be incorrect. To avoid
                # that, the DATASET_PREV is copied to a unique name
                # that can be recognized when mtdendseq runs.
                fnNPrev = '%s/%s-%s.prev' % ( indir, prevObs.first, prevObs.prev )
                os.system( 'cp %s/%s %s' % ( indir, DATASET_PREV, fnNPrev) )

                # Reset the contents of prevObs
#                prevObs.reset()
#                seqnum = prevObs.seqno + 1

                # Build p_dataset based on the current sequence number
                prevObs.seqno  += 1
                prevObs.dataset = expid
                
                # Keep the filename of the first exposure in the new sequence
                fnCurr = os.path.join( indir, '%s.cur' % (prevObs.dataset) )
                
                prevObs.first = seqid
                curlist       = open( fnCurr, 'a', 0 ) # and start a new one
                                
                # Open the list of lists, and write the filename of curlist
                fnLol = os.path.join( indir, '%s.lol' % (prevObs.dataset) )
                lol   = open( fnLol, 'w', 0 )
                lol.write( '%s!%s\n' % (hostname, fnCurr) )
                lol.flush()
                lol.close()
            
            if status >= 0:
                if VERBOSE:
                    print 'Adding file %s to the list' % thisfile
                # add the current file to the list
                curlist.write( '%s!%s\n' % (hostname, thisfile) )
                curlist.flush()

            # Set the previous values to the current values
            prevObs.obsid  = currObs.obsid
            prevObs.obstyp = currObs.obstyp
            prevObs.obstot = currObs.obstot
            prevObs.obsno  = currObs.obsno
            prevObs.prev   = currObs.prev
        
    # End loop over all observations in input list
    
    # Close the current list
    curlist.close()
    
    # Write previous values to file
    fnPrev = os.path.join( indir, DATASET_PREV )
    try:
        os.remove( fnPrev )
    except:
        pass
    prevObs.writeToFile( fnPrev )

    # Check whether a .mtdend file exists for the last dataset. If
    # so, trigger processing. Note that mtdgroup can also be called
    # with a list of more than one exposures. The code below has only
    # been tested against the case where the list contains one file.
    fName  = os.path.join( indir, '%s.mtdend' % (dataset) )
    print "++++"
    print fName
    if ( os.path.exists( fName ) ):
        fnNPrev = '%s/%s.prev' % ( indir, dataset )
        os.system( 'cp %s/%s %s' % ( indir, DATASET_PREV, fnNPrev) )
        cmdline = 'osf_update -a NEWFIRM -p mtd -s s -c 8 -f %s' % ( dataset )
        print cmdline
        os.system( cmdline )

    return( 0 )

def end( prevObs, indir, dataset, hostname ):

    # Terminate current list and trigger pipeline
    print 'Now the %s pipeline will be triggered' % prevObs.obstyp

    rootdataset = dataset.split('-')[0]

    # If the lock file exists, this dataset has already been triggered
    fnLock = os.path.join( indir, '%s.lock' % (dataset) )
    
    if not os.path.exists( fnLock ):
        # Write a lock file so that the first observation from the next
        # sequence will not trigger processing of the dataset as well
        lockfile = open( fnLock, 'w' )
        lockfile.write( 'locked\n' )
        lockfile.flush()
        lockfile.close()
        
        # Trigger the relevant pipeline
	callpipe( prevObs, indir, hostname, rootdataset )

        # Reinitialize file with information on previous observation,
        # but increase the sequence number by 1. Reinitializing the
        # prevfile will also prevent detection of a break in the
        # sequence. This is needed because otherwise the dataset
        # would be triggered again upon the next call to mtdgroup.py
        # with procmode==add.
#        fnPrev = os.path.join( indir, DATASET_PREV )
#        seqnum = prevObs.seqno + 1
        
#        prevObs.reset()
#        prevObs.seqno = seqnum
#        fnPrev = os.path.join( indir, DATASET_PREV )
#        prevObs.writeToFile( fnPrev )
    else:
        print 'Dataset %s has already been triggered' % dataset
        print 'The lock file is %s' % fnLock
    return (0)

if __name__ == '__main__':
    
    procmode = sys.argv[1]

    print "============ START OF MTDGROUP.PY ============"

    if procmode == 'nothing':
        sys.exit(0)

    # Set filenames and directories
    if procmode == 'flush':
        dataset = 'flush'
    else:
        dataset = os.environ['OSF_DATASET']
    mtddata = os.environ['NEWFIRM_MTD']
    indir   = os.path.join( mtddata, 'input' ) + os.path.sep
    datadir = os.path.join( mtddata, 'data', dataset ) + os.path.sep

    # Get the node name
    thishost = os.environ['HOST'].split('.')[0]
    
    # Because mtdgroup.py can be called by the mtdendseq as well, and
    # because a call to mtdendseq will bypass the mtdstart module that
    # sets up the relevant directory for the dataset, the relevant
    # directories still need to be created
    #if procmode == 'end':
    #    os.system( 'init.cl mtd' )
    
    ## Log start of processing
    #lfile   = os.path.join( datadir, '%s.log' % (dataset) )
    #logfile = open( lfile, 'a', 0 )
    
    # Read the contents of the previous processing file...
    if procmode == 'end':
        fnNPrev = '%s/%s.prev' % ( indir, dataset )
        print 'Reading from file: %s' % ( fnNPrev )
        prevObs = obsInfo( os.path.join( indir, fnNPrev ) )
    else:
        prevObs = obsInfo( os.path.join( indir, DATASET_PREV ) )
        if procmode == 'flush':
            # If a flush is requested (e.g., at the end of a night or
            # in the middle of a night when clouds have rolled in), no
            # break will have been detected nor will a .mtd file have
            # been created. This means that the DATASET_PREV file
            # should reflect the correct current situation. This file
            # is now used to recreate the original dataset name.
            dataset = '%s-%s' % ( prevObs.first, prevObs.prev )

    print "Working on dataset: %s" % (dataset)

    if VERBOSE:
        print 'BEGIN contents of', DATASET_PREV
        prevObs.output()
        print 'END contents of', DATASET_PREV
    
    if procmode == 'add':
        status = add( prevObs, indir, dataset, thishost, 1 )
    elif procmode == 'addonly':
        status = add( prevObs, indir, dataset, thishost, 0 )
    elif procmode == 'end':
        status = end( prevObs, indir, dataset, thishost )
    elif procmode == 'flush':
        status = end( prevObs, indir, dataset, thishost )
        # Reset the contents of DATASET_PREV and write to disk
        prevObs.reset()
        prevObs.writeToFile( DATASET_PREV )

    sys.exit(status)
