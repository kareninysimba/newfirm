#!/usr/bin/env python

# This simple version of dirverify simply rejects exposures with
# nocdhs=="test" from the input list. If the output list has
# zero length, this module exits with exitcode=2.

# It also translates the detector/detname keyword pair (when present)
# to instrume/detector, which are the prefered values starting 2008B.

# Import python modules
import datetime
import time
import os
import sys
import pyfits as p

VERBOSE      = True

# Wrapper function around send_message
def sendmsg( status, message, submsg, id, dset ):
    omsg = message
    if (len(submsg)>0):
        message += ' ('+submsg+')'
    print message
    return omsg

def proc( indir, dataset, hostname ):

    print '\nMTDVERIFY %s: ' % ( dataset ),
    print datetime.datetime.today()

    # Open the input file list
    fName  = os.path.join( indir, '%s.mtd' % (dataset) )
    infile = open( fName, 'r' )
    # Read the file into memory...
    lines = infile.readlines()
    # Close the input file...
    infile.close()

    # Open the output file for writing
    fnOutfile = "%s.out"
    outfile = open( fnOutfile, 'w' )

    rejected = 0

    # Loop over all files in the list
    # Note that it is assumed that all data sets are local
    for line in lines:

        datasetok = 1

        thisline  = line.strip()
        if VERBOSE:
            print '=== Processing: %s' % (thisline)
        
        # Make sure file is on local host
        fields = thisline.split('!')
        if len(fields) == 2:
            # The part before the ! is the hostname, the rest is the filename
            # TODO next line can cause trouble if there is a . in the filename
            filehost = fields[0].split( '.', 1)[0]
            thisfile = fields[1]
        elif len( fields ) == 1:
            thisfile = thisline
            filehost = hostname
        else:
            # More than one ! in the file name
            sendmsg( 'ERROR', 'Input file name garbled', thisline, 'VRFY',
                     dataset )
            sys.exit(1)
        
        if filehost != hostname:
            sendmsg( 'ERROR', 'Host name in input list is not this host',
                     filehost, 'VRFY', dataset )
            sys.exit(1)
        
        # Make sure the file ends in .fits
        thisfile, ext = os.path.splitext( thisfile )
        if ext != '.fits':
            ext = '.fits'
        thisfile += ext
        
        # Get the short file name
        shortfile = os.path.basename( thisfile )
        
        # Read the relevant keywords from the header
        # Open the fits file
        try:
            hdu = p.open( thisfile, 'update' )
        except:
            msg = sendmsg( 'WARNING', 'Could not open fits file',
                           thisfile, 'VRFY', shortfile )
            datasetok = False
        else:
            try:
                # Read the header
                prihdr = hdu[0].header
            except:
                msg = sendmsg( 'WARNING', 'Could not open fits header',
                               thisfile, 'VRFY', shortfile )
                datasetok = False
    
        if datasetok:
            nocdhs  = prihdr['nocdhs']
            mjdobs  = prihdr['mjd-obs']

        if datasetok:
            writeout = 1
            if nocdhs.lower() == 'test':
                writeout = 0
                msg = "Test"
            if nocdhs.lower() == 'focus':
                writeout = 0
                msg = "Focus"
            if nocdhs.lower() == 'sflat' or nocdhs.lower() == 'sflato':
                writeout = 0
                msg = "Sky flat"
            if nocdhs.lower() == 'tflat':
                writeout = 0
                msg = "Twilight flat"
            if mjdobs<0:
                writeout = 0
                msg = "Negative MJD-OBS"
        else:
            writeout = 0

        if writeout:
            outfile.write( '%s\n' % ( line ) )
        else:
            rejected += 1
            print "%s exposure %s is rejected" % (msg,shortfile)

        # Check for the presence of the detname/detector keywords.
        # If they are present switch them out for the detector/instrume
        # keywords.
        if datasetok:
            try:
                detector = prihdr['detector']
            except:
                # No detector keyword in the primary header, meaning
                # the old detname/detector keyword pair is used.
                pass
            else:
                if detector == 'NEWFIRM':
                    # This is the old format, change the keyword values.
                    # First, read the old detector names
                    name1 = hdu[1].header['detname']
                    name2 = hdu[2].header['detname']
                    name3 = hdu[3].header['detname']
                    name4 = hdu[4].header['detname']
                    # In some cases, pyfits capitalizes the extname
                    # keyword, which causes problems in the pipeline.
                    # So the keywords are read, converted to lowercase
                    # and then written again.
                    for i in range(1,5):
                        ext = hdu[i].header['extname'].lower()
                        hdu[i].header.update( 'extname', ext )
                    # Delete the old keywords
                    head1 = hdu[1].header
                    del head1['DETNAME']
                    head2 = hdu[2].header
                    del head2['DETNAME']
                    head3 = hdu[3].header
                    del head3['DETNAME']
                    head4 = hdu[4].header
                    del head4['DETNAME']
                    # Also delete top level detector keyword
                    del prihdr['detector']
                    # Now write the new keywords
                    prihdr.update( 'instrume', 'NEWFIRM' )
                    head1.update( 'detector', name1 )
                    head2.update( 'detector', name2 )
                    head3.update( 'detector', name3 )
                    head4.update( 'detector', name4 )
                    # Write the updated header to disk
                    hdu.close( output_verify='ignore' )

    # Close the output file
    outfile.flush()
    outfile.close()

    if (rejected>0):
        status = 2
        # The list has changed, so rename the new list to the old list
        os.system( 'mv %s %s' % (fnOutfile, fName) )
    else:
        status = 0

    return( status )

if __name__ == '__main__':
    
    print "============ START OF MTDVERIFY.PY ============"
    
    # Set filenames and directories
    dataset = os.environ['OSF_DATASET']
    mtddata = os.environ['NEWFIRM_MTD']
    indir   = os.path.join( mtddata, 'input' ) + os.path.sep
    datadir = os.path.join( mtddata, 'data', dataset ) + os.path.sep

    print "Working on dataset: %s" % (dataset)

    # Get the node name
    thishost = os.environ['HOST'].split('.')[0]

    # Get the node name
    thishost = os.environ['HOST'].split('.')[0]
    
    status = proc( indir, dataset, thishost )

    sys.exit( status )
