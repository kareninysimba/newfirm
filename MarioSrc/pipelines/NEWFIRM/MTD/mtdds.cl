#!/bin/env pipecl
#
# MTDSTATS

string host,localpath,remotepath, s4
string ifile,datadir,ofile,dataset,lfile

images

task $hostname = "$!hostname"
task $remdots = "$!echo $1 | $!sed -e s/\\\.//g"
task $remchars = "$!echo $1 | $!sed -e s/_//g -e s/-//g"

# Set paths and files.
names ("mtd", envget("OSF_DATASET"))
dataset = names.dataset
ifile = names.indir // dataset // ".mtd"
datadir = names.datadir
lfile = datadir // names.lfile

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

print( datadir )
cd (datadir)

# Get the current hostname
hostname | scan (host)
print( host )
# Set the output file name
ofile = "mtdstats1.tmp"
list = ifile
while ( fscan( list, remotepath ) != EOF ) {

    # Get the hostname from the remote path
    s2 = substr( remotepath, 1, stridx("!",remotepath)-1 )
    # If the current host is named "localhost", assume
    # everything is done locally.
    if ( host == "localhost" ) {
        s2 = "localhost"
    }
    ;
    if ( host != s2 ) { # The file is not local
        # Create a local filename
        s3 = substr( remotepath, strldx("/",remotepath)+1, 999 )
        # Remove any "." (other than the extension) from the file name
        s4 = substr( s3, 1, strlstr( ".fits", s3 )-1 )
        remdots( s4 ) | scan( s3 )
        # Remove "_" and "-" from the file name
        remchars( s3 ) | scan( s4 )
        # If the file name starts with a digit, prepend an "x"
        s3 = ""
        print( s4 ) | match( "^[0-9]", "STDIN" ) | scan( s3 )
        if ( s3 != "" )
            s4 = "x"//s4
        ;
        # Create the local fully qualified IRAF path
        localpath = host//"!/data/staging/"//s4//".fits"
        # Copy the remote data to here if it does not exist yet
        if ( access( localpath ) == NO ) {
            copy( remotepath, localpath )
        }
        ;
        # Append the current localpath to the list
        print( localpath, >> ofile )
    } else { # The file is local
        # Add the path just read to ensure the list remains complete
        print( remotepath, >> ofile )
    }

}
list = ""

# Remove the original list
delete( lfile )
# Rename the new list to the original list name
rename( ofile, ifile )
type( ifile )

logout 1
