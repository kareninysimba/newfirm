#!/bin/env pipecl
#
# MTDCLEANUP -- Clean up staged files.

string  dataset
file    indir, datadir, ilist, lfile

# Set filenames.
names ("mtd", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir // dataset // ".ds"
lfile = datadir // names.lfile
if (access (ilist) == NO)
    logout 1
;

# Log start of processing.
printf ("\nMTDCLEANUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean up staged files.
delete ("@"//ilist)

# Remove tmp files from data directory.
cd (datadir)
delete ("*.tmp")

# Remove files from indir.  This is also done in the done stage but
# we can call this module for error recover.
cd (indir)
delete (dataset//".*")

logout 1
