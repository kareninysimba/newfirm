# DSPSUMMARY -- Make HTML summary info from an image header.
# This is an HTML fragment written to the STDOUT to be used by
# the calling program.  The image argument may be  wildcard but only
# the first image will be processed.

procedure dspsummary (image)

file	image				{prompt="Image"}

begin
	file	im
	int	ival, rejected
	real	rval, rval2
	string	strval, srval, srval2, filt
	struct	sval, sval2, obstype, proctype
	bool    green = YES

	# Tasks and packages.
	images
	utilities

	im = image

	# Observation type and processing type.
	obstype = ""; hselect (im, "OBSTYPE", yes) | scan (obstype)
	proctype = ""; hselect (im, "PROCTYPE", yes) | scan (proctype)

	# Is this image rejected?
	sval=""; hselect (im, "WCSCAL", yes) | scan (sval)
        if ( sval != "T" ) {
	    green = NO
	} else {
	    sval=""; hselect( im, "REJECT", yes ) |
	        translit( "STDIN", '"', delete+) | scan( sval )
	    if ( substr( sval, 1, 1 ) == "Y" )
		green = NO
	    ;
	}
	if ( green )
	    printf( "<font color=\"green\" size=\"-2\">\n" )
        else 
	    printf( "<font color=\"red\" size=\"-2\">\n" )
	
	# Date of observation
	sval=""; hselect (im, "DATE-OBS", yes) |
	    translit ("STDIN", '"', delete+) | scan (sval)
	if (sval != "")
	    printf ("%s<BR>\n", sval)

#	# Image Title
#	if ( obstype != "object ) {
#	    sval=""; hselect (im, "TITLE", yes) |
#	        translit ("STDIN", '"', delete+) | scan (sval)
#	    ival = strstr ("- DelRA",sval)
#	    if (ival > 0)
#	        sval = substr (sval, 1, ival-1)
#	    printf ("%s<BR>\n", sval)
#	}
#	;

	# Original filename or number combined.
	ival=INDEF; hselect (im, "NCOMBINE", yes) | scan (ival)
	if (isindef(ival)) {
	    sval=""; hselect (im, "PLOFNAME", yes) |
		translit ("STDIN", '"', delete+) | scan (sval)
	    if (sval != "") {
	        ival = strstr ("_pl_png", sval)
		if (ival > 0)
		    sval = substr (sval, 1, ival-1)
		printf ("RN: %s<BR>\n", sval)
	    }
	} else
	    printf ("NCOMBINE = %d<BR>\n", ival)

	if (proctype == "MasterCal") {
	    rval=INDEF; hselect (im, "QUALITY", yes) | scan (rval)
	    if (!isindef(rval)) {
	        printf ("QUALITY = %g<BR>\n", rval)
		if (rval < 0.)
		    printf ("<B>For Evaluation Only</B><BR>\n")
	    }
	}
	
	if (obstype == "zero")
	    return

	# Filter
	filt = ""; hselect( im, "FILTER", yes ) |
	    translit( "STDIN", '"', delete+ ) | scan( filt )

	if (proctype == "MasterCal") {
	    if (filt != "")
	        printf ("%s<BR>\n", filt)
	    return
	}
	;

	# Observing recipe info
	sval=""; hselect (im, "NOCDHS", yes) | scan (sval)
	printf( "OR: %s<BR>\n", sval )
	sval = ""; hselect( im, "NOCDPAT", yes ) | scan( sval )
	sval2 = ""; hselect( im, "NOCMPAT", yes ) | scan( sval2 )
	printf( "dithpath: %s<BR>\nmappat: %s<BR>\n", sval, sval2 )

	# Original exposure time.
	srval = ""
	rval=INDEF; hselect (im, "OEXPTIME", yes) | scan (srval)
	if (isindef(rval)) {
	    rval=INDEF; hselect (im, "EXPTIME", yes) | scan (srval)
	}
	printf( "Filter:%s<BR>\nExptime: %d<BR>\n", filt, srval )

	# Sky level
	srval = ""
	rval=INDEF; hselect (im, "RAWBCKLV", yes) | scan (rval)

	# Residual sky subtraction method
	sval = ""; hselect (im, "RESBCKMT", yes) | scan (sval)
	printf( "Rawbck: %d<BR>\nResbck: %s<BR>\n", rval, sval )

	# Airmass.
	srval2 = ""
	rval=INDEF; hselect (im, "AIRMASS", yes) | scan (srval2)
	if (!isindef(rval))
	    printf ("AIRMASS = %.2g<BR>\n", rval)

	# WCS and Photometry.
	sval="F"; hselect (im, "WCSCAL", yes) | scan (sval)
	if (sval == "T") { 
	    # Magnitude zero point
	    rval=INDEF; hselect (im, "MAGZERO", yes) | scan (rval)
	    srval = ""
	    if (!isindef(rval))
		printf ("%.2f\n", rval) | scan( srval )

	    # Magnitude depth.
	    rval=INDEF; hselect (im, "PHOTDPTH", yes) | scan (rval)
	    srval2 = ""
	    if (!isindef(rval))
		printf ("%.2f\n", rval) | scan( srval2 )

	    printf( "Magzero:%s<BR>\nPhotdp: %s<BR>\n", srval, srval2 )

	    # Seeing
	    rval=INDEF; hselect (im, "SEEING", yes) | scan (srval)
#	    if (!isindef(rval))
#		printf ("FWHM = %.2f<BR>\n", rval)
	    printf( "Seeing:%s<BR>\n", srval )
	} else {
	    hselect (im, "TELRA,TELDEC", yes) | scan (rval, rval)
	    if (nscan() < 2)
		printf ("<B>Telescope Coordinates Missing</B><BR>\n")
	    else
		printf ("<B>WCS/Phot Calibration Failed</B><BR>\n")
	}
	printf( "</font>\n" )
end
