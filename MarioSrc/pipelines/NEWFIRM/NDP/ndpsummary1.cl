# DSPSUMMARY1 -- Make HTML summary info from an image header.
# This is an HTML fragment written to the STDOUT to be used by
# the calling program.  The image argument may be  wildcard but only
# the first image will be processed.

procedure dspsummary1 (image)

file	image				{prompt="Image"}

begin
	file	im
	int	ival
	real	rval
	string	strval
	struct	sval, obstype, proctype

	# Tasks and packages.
	images
	utilities

	im = image

	# Observation type and processing type.
	obstype = ""; hselect (im, "OBSTYPE", yes) | scan (obstype)
	proctype = ""; hselect (im, "PROCTYPE", yes) | scan (proctype)

	if (proctype == "MasterCal")
	    return

	# Maximum object area.
	ival=INDEF; hselect (im, "DQOBMAXA", yes) | scan (ival)
	if (!isindef(ival))
	    printf ("MAXOBJAREA = %d<BR>\n", ival)

	# Sky level
	rval=INDEF; hselect (im, "DQSKFRAC", yes) | scan (rval)
	if (!isindef(rval))
	    printf ("SKYFRAC = %.3g<BR>\n", rval)

	# WCS and Photometry.
	sval = "F"; hselect (im, "WCSCAL", yes) | scan (sval)
	if (sval == "T") {
	    # Number of matched 2MASS sources.
	    printf ("2MASS Match = %d<BR>\n", ival)
	} else {
	    hselect (im, "TELRA,TELDEC", yes) | scan (rval, rval)
	    if (nscan() < 2)
		printf ("<B>Telescope Coordinates Missing</B><BR>\n")
	    else
		printf ("<B>WCS/Phot Calibration Failed</B><BR>\n")
	}
end
