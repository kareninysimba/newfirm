#!/bin/env pipecl
#
# NDP2MKD -- Call multiple instances of the MKD pipeline.
# 
# The input is a list of files for the MKD pipeline.

string	cpipe				# Calling pipeline
string	tpipe 				# Target pipeline
int	ntrig				# Number to trigger here

string	dataset, indir, datadir, ilist, lfile, spool, pipes, odir
string	tname, cname, textn, rextn, calledby, s4
file	temp1, temp2, temp3, temp4
struct	*fd1

# Tasks and packages.
task $pipeselect = "$!pipeselect"
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# The calling pipeline is set from the environment and the target pipeline
# is set by an argument.  The trigger and return extensions are set by
# the target pipeline.

names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))

cpipe = names.pipe
ntrig = envget ("CoresPerNode" )
if (fscan (cl.args, tpipe, ntrig) < 1)
    logout 0
;
textn = "." // tpipe
rextn = "." // tpipe

if ( ntrig == 0 ) {
    ntrig = num_trigger;
}
;

# Files and directories.
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir // dataset // ".ndp"
spool = indir // "spool/"
lfile = datadir // names.lfile
set (uparm = names.uparm)
temp1 = envget ("NHPPS_MODULE_NAME") // "1.tmp"
temp2 = envget ("NHPPS_MODULE_NAME") // "2.tmp"
temp3 = envget ("NHPPS_MODULE_NAME") // "3.tmp"

# Work in data directory.
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Check for pipelines.
pipes = spool // dataset // rextn
touch (pipes)
if (defvar ("NHPPS_MIN_DISK"))
    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0,
        envget("NHPPS_MIN_DISK"), > pipes)
else
    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, > pipes)
count (pipes) | scan (i)
if (i < min(1,ntrig)) {
    sendmsg ("ERROR", "Pipeline not running", tpipe, "PIPE")
    delete (pipes)
    logout 2
}
;

# Determine who called the NDP pipeline
calledby = ""
print( dataset, > temp1 )
concat( temp1 )
print "---"
s1 = ""
match( "-drk", temp1 ) | scan( s1 )
if ( strlen(s1)>0 ) {
    calledby = "dark"
} else {
    match( "-flt", temp1, meta- ) | scan( s1 )
    if( strlen(s1)>0 ) {
        calledby = "flat"
    } else {
        calledby = "object"
    }
}
delete( temp1 )
print( dataset )
print( calledby )

#logout 1

# Make subset name and pattern. The point here is to select only one 
# instance of each set of extensions
touch (temp1)
if ( calledby == "dark" ) { # Called by the DRK pipeline
    # TODO use list of extensions to set _imX, because 1 could be missing
    match ("_SDK?*-im1.fits", ilist, > temp2)
} else {
    if ( calledby == "flat" ) { # Called by the FLT pipeline
        # TODO use list of extensions to set _imX, because 1 could be missing
        match ("_SFL?*-im1.fits", ilist, > temp2)
    } else { # Assume called by GOS pipeline
#        match ("_WCS?*_00.fits", ilist, > temp2)
        match ("_OGC?*im1_raw.fits", ilist, > temp2)
    }
}

list = temp2
# TODO move while within if statement
while (fscan (list, s1) != EOF) {
    if ( calledby == "dark" ) { # Called by the DRK pipeline
        # Break around the hyphen in the pattern, and replace
        # it with a question mark. Only use the exposure ID
        # as a name when calling the MKD pipeline
        s2 = substr( s1, strldx("/",s1)+1, strlstr("-im1",s1)-1)
        s3 = substr( s2, 1, strldx("-",s2)-1 )
        s4 = substr( s2, strldx("-",s2)+1, 999 )
        s2 = s3//"?"//s4
        printf ("%s _SDK/data/?*/?*%s?*.fits$\n", s4, s2, >> temp1)
    } else {
        if ( calledby == "flat" ) { # Called by the FLT pipeline
            s2 = substr( s1, strldx("/",s1)+1, strlstr("-im1",s1)-1)
#            # Strip the filter name
#            s2 = substr( s2, 1, strldx("-",s2)-1 )
            # Break around the hyphen in the pattern, and replace
            # it with a question mark. Only use the exposure ID
            # as a name when calling the MKD pipeline
            s3 = substr( s2, 1, strldx("-",s2)-1 )
            s4 = substr( s2, strldx("-",s2)+1, 999 )
            s2 = s3//"?"//s4
            printf ("%s _SFL/data/?*/?*%s?*[0-9].fits$\n", s4, s2, >> temp1)
        } else { # Assume called by GOS pipeline
#            s2 = substr (s1, strldx("/",s1)+1, strlstr("_00.fits",s1)-1)
	    s2 = substr (s1, strldx("/",s1)+1, 999 )
	    s2 = substr( s2, 1, stridx("_",s2)-1 )
            printf ("%s _[OWS][CWTPG]?*%s_?*[ls]$\n", s2, s2, >> temp1)
        }
    }
print "+++++++++++++++++++++++++++++++++++++++++++++++++++"
print( s1 )
print( s2 )
}

list = ""; delete (temp2)

count (temp1) | scan (i)
if (i == 0) {
    sendmsg ("WARNING", "No data found", "", "VRFY")
    delete (pipes)
    delete (temp1)
    logout 3
}
;

# We want to assign datasets here in the precedence from pipeselect.
# But for return.cl we need the pipeline list sourted.

temp4 = spool // dataset // rextn // ".tmp"
rename (pipes, temp4)
sort (temp4, > pipes)

# Loop over each pattern and setup triggers and returns for calling pipeline.
# Trigger a few pipeline instances.  The rest will be triggered by the
# returns.

i = 1
copy (ilist, temp2)
list = temp1; fd1 = temp4
while (fscan (list, s1, s2) != EOF) {

    # Set names.
    tname = dataset // "-" // tpipe // s1
    cname = dataset // "-" // tpipe // s1

    # Create @file.
    printf ("match (%s)\n", s2)
print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
print( s1 )
print( s2 )
print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    match (s2, temp2 )
    match (s2, temp2, >> indir//"data/"//tname//textn)
    match (s2, temp2, stop+, > temp3); rename (temp3, temp2)

    # Setup files for triggering and return.
    print (tname, >> spool//cname//rextn)
    print (cname//rextn, >> dataset//textn)

    # Trigger a certain number of instances of the target pipeline.
    if (i > ntrig)
        next
    ;
    if (fscan (fd1, odir) == EOF) {
        i += 1
	fd1 = pipes
	if (fscan (fd1, odir) == EOF)
	    next
	;
    }
    ;
    if (i > ntrig)
        next
    ;

    # Trigger the target pipeline.
    copy (indir//"data/"//tname//textn, odir//tname//textn)
    pathname (indir//cname//rextn, >> odir//"return/"//tname//textn)
    print (substr(odir,1,stridx("!",odir)-1), > spool//cname//rextn//".node")
    touch (odir//tname//textn//"trig")
}
list =""; fd1 = ""
delete (temp1)
delete (temp2)
delete (temp4)

logout 1
