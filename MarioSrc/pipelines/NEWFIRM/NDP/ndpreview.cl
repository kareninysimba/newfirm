#!/bin/env pipecl
# 
# NDPREVIEW -- Review data products.
#
# This routine makes a review HTML.

string  dataset, datadir, ilist, lfile, pngdir, pngfits, html, prophtml
string	root, lastroot, propid, obstype, abspngdir

# Load tasks and packages
task $resize = "$!convert -resize"
task $ln = "$!ln -sf"
task $readlink = "$!readlink"
task ndpprophdr = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndpprophdr.cl"
task ndppropseq = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndppropseq.cl"
task ndpsummary = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndpsummary.cl"
task ndpsummary1 = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndpsummary1.cl"
task rmdir = "$!rm -r $1"
task $getps = "$!ls -rt1 *eps | tail -1"
task $convert = "$!convert $1 -rotate -90 -crop 696x250+0+250 $2; rm $1"
task $indef2star = "$!sed -e s/INDEF/\*/g $1 | awk '{print NR,\$0}' > res.tmp ; cp $1 bck ; mv res.tmp $1"
task $mkreview = "$mkndpreview --plqueue $1"
cache ndppropseq
unlearn ndppropseq
dppkg
tables
tbplot
images
tv
util

# Set names and directories.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ilist = names.indir // dataset // ".ndp"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (srcdir = "NHPPS_PIPESRC$/NEWFIRM/NDP/pipedata/")

cd( datadir )

# Log start of module.
printf ("\n%s (%s): ", strupr(envget("NHPPS_MODULE_NAME")), dataset) |
    tee (lfile)
time | tee (lfile)

delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
if ( access( "uparm" ) )
    rmdir( "uparm" )
;
mkdir( "uparm" )

# Create data subdirectories.  Note that this may be called more than once
# so we delete any old data.
cd (names.rootdir//"output")
if (access("html")==NO)
    mkdir ("html")
;
if (access("png")==NO)
    mkdir ("png")
;
if (access("observer")==NO)
    mkdir ("observer")
;
if (access("logs")==NO)
    mkdir ("logs")
;
cd ("png")
pngdir = names.shortname//"/"
if (access(pngdir))
    delete (pngdir//"*")
else
    mkdir (pngdir)
cd (pngdir)
path | scan( s1 )
abspngdir = substr( s1, stridx("!",s1)+1, 999 )

pngdir = "../png/"//names.shortname//"/"
html = "../../html/"//names.shortname // ".html"
prophtml = "dummy"
if (access(html))
    delete (html)
;

# Extract lists of FITS-encapsulated PNGs.
concat (datadir//"*.mkd") | match ("_png1", > "ndprev.tmp")
match ("_raw_png1", "ndprev.tmp", > "ndprev_raw.tmp")
match ("_press_png1", "ndprev.tmp", > "ndprev_press.tmp")
# Because "_press" was removed from the names of the darks and the
# flats, those FITS-encapsulated PNGs need to be found in a 
# different way, while not matching other sets when this module
# is running on a different set of data:
list = "ndprev.tmp"
while ( fscan( list, s1 ) != EOF ) {
    hselect( s1//"[0]", "OBSTYPE", yes ) | scan( obstype )
    if ( (obstype=="flat") || (obstype=="dark" ) )
        printf( "%s\n", s1, >> "ndprev_press.tmp" )
    ;
}
list = ""
match ("_ss_png1", "ndprev.tmp", > "ndprev_ss.tmp")
match ("_r_png1", "ndprev.tmp", > "ndprev_rsp.tmp")
match ("_stk_png1", "ndprev.tmp", > "ndprev_stk.tmp")
match ("_spk_png1", "ndprev.tmp", > "ndprev_spk.tmp")

# Eliminate unpopulated columns.
count ("ndprev_ss.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_ss.tmp")
;
count ("ndprev_press.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_press.tmp")
;
count ("ndprev_raw.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_raw.tmp")
;
count ("ndprev_rsp.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_rsp.tmp")
;
count ("ndprev_stk.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_stk.tmp")
;
count ("ndprev_spk.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_spk.tmp")
;

# Create list of datasets.
list = "ndprev.tmp"; lastroot = ""
while (fscan (list, s1) != EOF) {
    root = substr (s1, strldx("/",s1)+1, strstr("_png1.fits",s1)-1)
    root = substr (root, 1, strldx("_",root)-1)
    s2 = ""
    match (root//"_ss", "ndprev.tmp") | scan (s2)
    if (s2 == "")
	match (root//"_press", "ndprev.tmp") | scan (s2)
    ;
    if (s2 == "")
	match (root//"_r", "ndprev.tmp") | scan (s2)
    ;
    if (s2 == "")
        match (root//"_raw", "ndprev.tmp") | scan (s2)
    ;
    if (s2 == "")
	match (root//"_spk", "ndprev.tmp") | scan (s2)
    ;
    if (s2 == "")
	match (root//"_stk", "ndprev.tmp") | scan (s2)
    ;
    if (s2 != "")
        s1 = s2
    ;
    if (root == lastroot)
        next
    ;
    lastroot = root

    # Get PNG locally for more efficient header access.
    copy (s1, ".", verbose-)
    s1 = substr (s1, strldx("/",s1)+1, 1000)
    s2 = s1//"[0]"

    # Set the PROPID so we can later separate by it.
    propid = ""
    hselect (s2, "DTPROPID,PROPID,PLPROPID", yes) | scan (propid)
    if (propid == "")
       propid = "unknown"
    ;

    # Get obstype
    obstype = ""
    hselect( s2, "OBSTYPE", yes ) | scan( obstype )

    # Create the HTML file and initialize the table.
    if (access(html)==NO) {
	printf( '<HTML>', > html )
	printf( '<style type="text/css"> table.ndp {', >> html )
	printf( 'border-width: 1px 1px 1px 1px; border-spacing: 0px;', >> html )
	printf( 'border-style: none none none none;', >> html )
	printf( 'border-color: black black black black;', >> html )
	printf( 'border-collapse: collapse; background-color: white; }\n', >> html )
	printf( 'table.ndp th {', >> html )
	printf( 'border-width: 1px 1px 1px 1px; padding: 5px 5px 5px 5px;', >> html )
	printf( 'border-style: inset inset inset inset;', >> html )
	printf( 'border-color: black black black black;', >> html )
	printf( 'background-color: rgb(220, 220, 255);', >> html )
	printf( '-moz-border-radius: 0px 0px 0px 0px; }\n', >> html )
	printf( 'table.ndp td {', >> html )
	printf( 'border-width: 1px 1px 1px 1px; padding: 5px 5px 5px 5px;', >> html )
	printf( 'border-style: inset inset inset inset;', >> html )
	printf( 'border-color: black black black black;', >> html )
	printf( 'background-color: rgb(240, 240, 255);', >> html )
	printf( '-moz-border-radius: 0px 0px 0px 0px; }\n', >> html )
	printf( 'table.plain td {border-width:0px 0px 0px 0px;', >> html )
	printf( 'border-style: none none none none; }\n', >> html )
	printf( 'p { margin: 0; padding: 0; }\n ', >> html )
	printf( '.alignleft { float: left; }\n', >> html )
	printf( '.alignright { float: right; }\n', >> html )
	printf( '</style>\n', >> html )
	printf( '<TITLE>%s</TITLE><BODY style="font-family:verdana"><CENTER><H1>%s</H1></CENTER>\n',
	    names.shortname, names.shortname, >> html)
	if ( ( obstype == "OBJECT" ) || ( obstype == "object" ) ) {
	    s3 = pngdir//"obscondit.png"
	    printf( '<img src="%s">', s3, >> html )
	}
	;
	printf ('\n<TABLE class="ndp">\n', >> html)
    } else
	printf ('</TR>\n', >> html)

    # Create the proposal HTML file.
    prophtml = "../../logs/"// propid // ".html"
    if (access(prophtml) == NO)
        ndpprophdr (s2, > prophtml)
    ;
    ndppropseq (s2)

    # Start a row.  Put in any comments.
    printf ("<!-- PROPID %s -->\n", propid, >> html)
    printf ("<TR>\n", >> html)

    # Put summary information.
    printf ('<TD ALIGN="LEFT" VALIGN="CENTER">\n', >> html)
    ndpsummary (s2, >> html)
    printf ('</A></TD>\n', >> html)
    #printf ('<TD ALIGN="LEFT" VALIGN="CENTER">\n', >> html)
    #ndpsummary1 (s2, >> html)
    #printf ('</A></TD>\n', >> html)

    hselect (s2, "$MJD-OBS,$RAWBCKLV,$SEEING,$MAGZERO,$PHOTDPTH", yes,
	>> datadir//"obscondit.dat" )

    if (access("ndprev_raw.tmp")) {
        pngfits = ""
	match (root//"_raw_png1.fits", "ndprev_raw.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, large-, >> html)
    }
    ;
    if (access("ndprev_press.tmp")) {
        pngfits = ""
#	match (root//"_press_png1.fits", "ndprev_press.tmp") | scan (pngfits)
	match (root//"?*_png1.fits", "ndprev_press.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, large-, >> html)
    }
    ;
    if (access("ndprev_ss.tmp")) {
	pngfits = ""
	match (root//"_ss_png1.fits", "ndprev_ss.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, >> html)
    }
    ;
    if (access("ndprev_rsp.tmp")) {
	pngfits = ""
	match (root//"_r_png1.fits", "ndprev_rsp.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, large-, >> html)
    }
    ;
    if (access("ndprev_stk.tmp")) {
	pngfits = ""
	match (root//"_stk_png1.fits", "ndprev_stk.tmp") | scan (pngfits)
	if (access("ndprev_spk.tmp"))
	    dpcol (pngfits, pngdir, propid, >> html)
	else
	    dpcol (pngfits, pngdir, propid, >> html)
    }
    ;
    if (access("ndprev_spk.tmp")) {
	pngfits = ""
	match (root//"_spk_png1.fits", "ndprev_spk.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, >> html)
    }
    ;

    # Finish row.
    printf ('</TD>\n', >> html)
}
list = ""
#delete ("ndprev*.tmp")

# Finish table and HTML.
if (access(html)) {
    printf ('</TR>\n', >> html)
    printf ('\n</TABLE></BODY></HTML>\n', >> html)
}
;

# Finish prop table.
if (access(prophtml)) {
    cd ("../../observer")
    html = "../html/" // names.shortname // ".html"
    prophtml = "../logs/" // propid // ".html"
    ndppropseq (html)
    ndppropseq (html, >> prophtml)
    if (access("index.html")) {
        readlink ("index.html") | scan (s1)
	if (s1 != prophtml)
	    ln (prophtml, "index.html")
        ;
    } else {
	ln (prophtml, "index.html")
    }
}
;

if ( ( obstype == "OBJECT" ) || ( obstype == "object" ) ) {
    # Create figure with observing conditions trends
    cd( datadir )
    indef2star( "obscondit.dat" )
    set stdgraph = "epsfl"
    iferr {
        set tmp = "/tmp/"
        igi < "srcdir$obscondit.igi"
    } then {
	print "IGI ERROR"
    } else {
dir
        # Flush the graphics buffer to ensure the plot is created now
	sleep( 1)
        gflush
        # Find the most recently created postscript file ...
        getps | scan( s1 )
        # ... and convert it to png
        convert( s1, abspngdir//"obscondit.png" )
    }
}
;

s1 = substr( dataset, 1, stridx("-",dataset)-1 )
mkreview( s1 )

logout 1
