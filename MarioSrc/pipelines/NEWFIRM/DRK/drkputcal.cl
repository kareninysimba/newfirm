#!/bin/env pipecl
#
# MTDSTATS

int	status = 1
int	fsample, digavgs, ncoadd
real	exptime, mjd, mjdstart, mjdend
string	datadir, dataset, lfile, obstype, match

# Load the packages.
images
lists
servers
task $psqlog = "$!psqlog"

# Set the dataset name and uparm.
names( "drk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)

# Log the operation.
printf( "\nDRKPUTCAL (%s):\n", dataset )

# Go to the relevant directory
cd( datadir )

# Get the list of darks images by selecting images with the string
# -imX.fits in the return files from the SDK pipeline.  Note that the
# original flat files have _imX.fits. Also, require that all SDK
# return successfully; this is done by ensuring that each of the files
# in the return file has the expected contents.
list = dataset//".sdk"; touch( "calpcal.lis" )
while ( fscan( list, s1 ) != EOF ) {
    s2 = ""
    match( "-im[0-9]*.fits", s1 ) | scan( s2 )
    if (s2=="") {
        # This file does not have the expected contents, therefore
        # not all data are returned, a complete flat cannot
        # be put in the database, and hence this set is rejected,
        # which is accomplished by deleting putcal.list
	delete( "calpcal.lis" )
	touch( "calpcal.lis" )
        break
    } else {
        # This file does have the expected contents, so add this
        # file to calpcal.lis
        print( s2, >> "calpcal.lis" )
    }
}
list = ""

# Get the first file from the list
s1 = ''; head ("calpcal.lis", nl=1) | scan (s1)
if (s1 == "") { # Error if there are no files
    sendmsg( "WARNING", "No calibration file to put", "", "CAL" )
    logout 2
} else {
    hselect( s1, "MJD-OBS,OBSTYPE,EXPTIME", yes ) |
	scan( mjd, obstype, exptime )
    mjdstart = mjd + cal_mjdmin
    mjdend = mjd + cal_mjdmax
    i = 1; hselect( s1, "NCOMBINE", yes ) | scan( i )
    hselect( s1, "FSAMPLE,DIGAVGS,NCOADD", yes ) |
        scan( fsample, digavgs, ncoadd )
    if (nscan()!=3) {
        sendmsg( "WARNING", "Fowler/coadd/digavg info missing",
	"not put in DM", "CAL" )
        i = 0
    }
    ;
}

# Put the calibration files in the Data Manager.
if (i < cal_nzero) {
    sendmsg( "WARNING", "Too few zero/dark exposures",
	str(i) // " - not put in DM", "CAL" )
    psqlog( names.shortname, "archived", "nodata" )
} else {
    match = "CO="//ncoadd//" DA="//digavgs//" FS="//fsample
    hedit ( "@calpcal.lis", "PROCTYPE", "MasterCal", add+, ver-)
    putcal( "@calpcal.lis", "image", dm=cm, class=obstype,
        detector="!instrume", imageid="!detector", filter="!filter",
        exptime="!exptime", mjdstart=mjdstart, mjdend=mjdend,
            match=match ) | scan( i, line )
    if (i != 0)
	status = 0
    ;
}

if (access ("calpcal.lis") == YES) {
    delete ("calpcal.lis")
}
;

if (status == 0)
    sendmsg ("ERROR", "Putcal failed", line, "CAL")
;

logout 1
