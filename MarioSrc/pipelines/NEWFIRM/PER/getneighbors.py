#!/usr/bin/python

import sys
import os

# Get the command line arguments
fName = sys.argv[1]
num = int(sys.argv[2])

# Read the input file and delete it
infile = open( fName, 'r' )
lines = infile.readlines()
infile.close()
os.unlink( fName )

# Store the first two columns in arrays
files = []
flag = []
location = []
selected = []
for line in lines:
    thisline = line.strip()
    fields = thisline.split(' ')
    files.append( fields[0] )
    flag.append( int(fields[1]) )
    location.append( fields[2] )
    selected.append( 1 )

# Loop over all entries in the array
lngth = len(files)
for i in range( 0, lngth ):
    if flag[i] == 0:
        jmin = max( i-num, 0 )
        jmax = min( i+num+1, lngth )
        for j in range( jmin, jmax ):
            selected[j] = 0
            
# Loop over all entries in the array
#for i in range( 0, lngth ):
#    print '%s %s %s' % ( files[i], flag[i], selected[i] )

# Write the selected entries to the output list
fOutname = fName
outfile = open( fOutname, 'w' )
for i in range( 0, lngth ):
#    if not selected[i]:
#        outfile.write( '%s\n' % files[i] )
    outfile.write( '%s %i %s\n' % ( files[i], selected[i], location[i] ) )
outfile.close()
                                                                               
