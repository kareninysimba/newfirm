#!/bin/env pipecl
#
# PERBOPEM - Bright Object and PErsistence Masks

int status = 1
int founddir,nlines,prevavail
real scale,fowler,coadd,bright,mjd
string dataset,datadir,ilist,lfile,prevmask,s4,trimsec,headerfile,extname
string shortname,orig
struct *fd1

file    caldir = "MC$"

# Tasks and packages.
images
lists
noao
imred
crutil
proto
servers
task $getneighbors = "$!getneighbors.py $1 $2"
task $select = "$!awk \'{if(\$2==$2){print \$1,\$3}}\' $1 > $3"

# Set paths and files.
names ("per", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ilist = names.indir // dataset // ".per"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Work in data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Start with the simplest possible implementation for persistence 
# masking: simply mask object brighter than a certain threshold in the
# N preceding images.

# Retore the original input list if it exists, store current one otherwise
if ( access( "list.rerun" ) ) {
    delete( ilist )
    copy( "list.rerun", ilist )
} else {
    copy( ilist, "list.rerun" )
}

# Loop through the input list to find any pre-existing persistence masks.
list = ilist
while ( fscan( list, s1 ) != EOF ) {
    hsel( s1, "DETECTOR,OBSID,MJD-OBS", yes ) | scan( s2, s3, mjd )
    # Retrieve the location of the previously produced mask
    getcal( s1, "masks", cm, caldir, imageid=s2, mjd=mjd,
        match="%"//s3, obstype="", detector="", filter="" )
    if (getcal.statcode>0) {
	# No mask found
	printf( "%s 0 none\n", s1, >> "perbopem6.tmp" )
    } else {
	# Mask found, make sure the matching orginal persistence mask
	# exists. If not, add this file to the list of files with
	# no masks.
	# Create file name of original from the cumulative mask name.
	s2 = getcal.value
	s3 = substr( s2, 1, strlstr("per",s2)-1 ) // "orig.pl"
	if ( imaccess( s3 ) ) {
	    printf( "%s 1 %s\n", s1, getcal.value, >> "perbopem6.tmp" )
	} else {
	    printf( "Could not find original persistence mask for %s\n", s1 )
	    printf( "%s 0 none\n", s1, >> "perbopem6.tmp" )
	}
    }
}
list = ""
print "================================================="
concat( "perbopem6.tmp" )
print "================================================="

# If masks are found missing, set per_numexps neighbors as missing as well.
# The file given to getneighbors is replaced, and contains only those
# files that need to be processed.
getneighbors( "perbopem6.tmp", 0, per_numexps )

# Select the data with existing masks. For these data, the original
# persistence masks should be copied to the location of the cumulative
# masks in the database.
select( "perbopem6.tmp", 1, "perbopem7.tmp" )

# Select data without existing masks, or data selected as neighbor. The
# latter may have existing masks, and those should be deleted, as well
# as the matching database entry.
select( "perbopem6.tmp", 0, "perbopem8.tmp" )

count( "perbopem7.tmp" ) | scan( i )
if ( i > 0 ) {
    # Loop over existing masks, retrieve the original masks and copy
    # them the location pointed to in the database.
    list = "perbopem7.tmp"
    while ( fscan( list, s1, s2 ) != EOF ) {
	# Create file name of original from the cumulative mask name
	s3 = substr( s2, 1, strlstr("per",s2)-1 ) // "orig.pl"
	# Replace the cumulative mask with the original persistence mask
	if ( imaccess( s2 ) ) 
	    imdel( s2 )
	;
	imcopy( s3, s2 )
    }
    list = ""
}
;

count( "perbopem8.tmp" ) | scan( i )
if ( i == 0 ) {
    print "Existing masks are available for all data. Exiting."
    logout 1
} else {
    # Loop over all data and remove any existing database entries
    list = "perbopem8.tmp"
    while ( fscan( list, s1, s2 ) != EOF ) {
	hsel( s1, "OBSID,DETECTOR", yes ) | scan( s2, s3 )
	delcal( cm, class="masks", imageid=s3, match="like %"//s2//"" )
    }
    list = ""
    # Create the new ilist
    delete( ilist )
    fields( "perbopem8.tmp", "1", > ilist )
}

# Create bright object masks for all images. Adjust counts for
# digital averages, coadds, and Fowler sampling
list = ilist
while ( fscan( list, s1 ) != EOF ) {

    extname = substr( s1, strlstr("[",s1)+1, strlstr("]",s1)-1 )
    s2 = substr(s1,strldx("/",s1)+1,strlstr("[im",s1)-1 )//"_"//extname//"bom"
    s3 = substr(s1,strldx("/",s1)+1,strlstr("[im",s1)-1 )//"_"//extname//"per"
    shortname = substr(s1,strldx("/",s1)+1,999 )

    # Determine the scale factor
    fowler = 0
    hselect( s1, "FSAMPLE", yes ) | scan( fowler )
    coadd = 0
    hselect( s1, "NCOADD", yes ) | scan( coadd )
    scale = 1.*fowler*coadd

    # Detect bright objects
    bright = 8000.*scale
    printf( "i>%f", bright, > "perbopem1.tmp" )
    mskexpr( "@perbopem1.tmp", s2//".pl", s1, refmask="" )
    delete( "perbopem1.tmp" )
    prevmask = s2

    # Grow the mask
    crgrow( s2, "crgrow.pl", radius=5 )

    # Retrieve the trim section from the header
    hsel( s1, "TRIMSEC", yes ) | scan( trimsec )
    if ( nscan()!=1 ) {
        sendmsg( "ERROR", "Could not retrieve trimsec",
	    shortname, "VRFY" )
        status = 0
        break
    }
    ;
    # Copy the trimsec only, i.e., remove the reference pixels and
    # delete the section keywords. Reuse the old file name (s2).
    imdel( s2 )
    imcopy( "crgrow.pl"//trimsec, s2//".pl" )
    # Delete temporary file
    imdel( "crgrow.pl" )
    # Remove the various "section" keywords
    hedit( s2, "DETSEC,DATASEC,TRIMSEC,BIASSEC", del+, verify-, show- )

    # Write the bright object and persistence mask file names to file
    print( s2, >> "perbopem2.tmp" )
    print( s3, >> "perbopem3.tmp" )

}
list = ""

# Create the persistence masks
fd1 = "perbopem2.tmp"
list = "perbopem3.tmp"
i = 1
while ( fscan( list, s1 ) != EOF ) {

    # Also read the bright object mask, because that file already
    # exists, whereas the persistence mask is created here and does
    # not have the correct keywords. Before this change, the information
    # putcalled into the calibration manager was incorrect: the
    # persistence mask for an image should be the sum over the
    # previous per_numexps exposures. However, because the
    # information for the putcal was read from the persistence
    # mask created with imsum (see below), the putcal used
    # the header from the last image in the sequence, thus effectively
    # making the persistence mask include the current image as well.
    if ( fscan( fd1, headerfile ) == EOF ) {
        sendmsg( "ERROR", "Mismatch in number of files", headerfile, "VRFY" )
        status = 0
        break
    } 

    # Extract the relevant bright object masks from the input list
    if (i>per_numexps) {
        nlines = per_numexps
    } else {
        nlines = i-1
    }
    if (i==1) {

        # Make an all ok mask (because there was no previous exposure
        # to create a persistence mask from)
        mskexpr( "1==0", s1//".pl", prevmask )

    } else {

        # Extract the relevant lines from the list of bright object masks
        # First, get all the previous masks
        j = i-1
	head( "perbopem2.tmp", nlines=j, > "perbopem4.tmp" )
        # And now only get the last nlines
        tail( "perbopem4.tmp", nlines=nlines, > "perbopem5.tmp" )
        # Create the persistence mask. First sum over all the
	# contributing bright object masks ...
        imsum( "@perbopem5.tmp", s1//"tmp.pl" )
	# ... and then set those that have a positive value to 6,
	# which designates pixels affected by persistence
        mskexpr( "i>0 ? 6 : 0", s1//".pl", s1//"tmp.pl" )

    }

    # Create a copy of the newly persistence mask. This copy
    # will remain unchanged and can be used if this dataset will be
    # reprocessed.
    orig = substr( s1, 1, strlstr("per",s1)-1 ) // "orig"
    imcopy( s1//".pl", orig//".pl" )

    # Store the location of this mask in the calibration manager
    # First get the full location hostname and path
    pathname( s1 ) | scan( s2 )
    # In the putcal below the !keyword syntax cannot be used
    # because the "value=" keyword is used, so the keywords 
    # have to be read separately and supplied to putcal.
    hsel( headerfile, "OBSID,DETECTOR", yes ) | scan( s3, s4 )
    putcal( "", "keyword", cm, value=s2//".pl", class="masks",
        imageid=s4, match=names.parent//" "//s3,
        detector="", filter="", exptime="" )

    # Clean up
    imdel( s1//"tmp.pl" )
    delete( "perbopem4.tmp,perbopem5.tmp" )

    i = i+1

}
list = ""

logout( status )
