#!/bin/env pipecl
#
# PERSETUP

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ifile, im, lfile, maskname

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers
proto

# get the dataset name
names( "per", envget( "OSF_DATASET" ) )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ifile = indir // dataset // ".per"

subdir = "/" // envget( "NHPPS_SYS_NAME" ) // "/"
if (strstr( subdir, osfn( caldir ) ) == 0)
        caldir = caldir // subdir

set (uparm = names.uparm)
set (pipedata = names.pipedata)

cd( datadir )

# Log start of processing.
printf( "\nSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Make sure the input list is properly sorted
hselect( "@"//ifile, "$I,NOCID", yes, > "persetup1.tmp" )
delete( ifile )
sort( "persetup1.tmp", column=2, numeric+ ) | fields( "STDIN", "1", > ifile )
delete( "persetup1.tmp" )

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( "@"//ifile )
} then {
    logout 0
} else
    ;

logout( status )
