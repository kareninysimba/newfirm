#!/bin/env pipecl
#
# STImask
#
# Description:
#
#   
#
# Exit Status Values:
#
#   0 = Unsuccessful (set for error in reading SIFnamefile or CBPMnamefile,
#                   or for error in retrieving the reprojection center)
#	1 = Successful
#
# History:
#
#   R. Swaters  --------    Created.
#   T. Huard    20090406    IMPORTED TO STI PIPELINE, which works on an
#                           individual SIF instead of a list of SIFs.  Renamed
#                           stkmask.cl to stimask.cl, and revised accordingly.
#   Last Revised:  T. Huard  20090406  11:40am

int     status = 1
string  datadir,dataset,shortname,lfile,SIFnamefile,CBPMnamefile
string  msg,sifFULL,sif,cbpmFULL,cbpm,cbpmR,cbpmTMP

# Load the packages.
redefine mscred=mscred$mscred.cl
images
lists
noao
nproto
mscred
#cache mscred
#Why is the "cache mscred" necessary?

# Set the dataset name and uparm.
names( "sti", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
shortname=names.shortname
lfile = datadir // names.lfile
SIFnamefile="stisetup-SIF.tmp"
CBPMnamefile="stisetup-CBPM.tmp"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSTIMASK (%s):\n", dataset )

# Go to the relevant directory
cd( datadir )

if ( sciencepipeline == NO )
    logout 1
;

# Read sifnamefile to get full pathname of SIF (sifFULL).
# Extract basename (sif).
sifFULL=INDEF ; fields(SIFnamefile,"1",lines="1") | scan(sifFULL)
if (sifFULL == "INDEF") {
    msg="No SIF listed in SIFnamefile."
    sendmsg("ERROR",msg,shortname,"PROC")
    status=0
    logout(status)
#    break
}
;
sif=substr(sifFULL,strldx("/",sifFULL)+1,strlstr(".fits",sifFULL)-1)

# Set the name of the mask created by mscimage in stiresample.cl.
# This mask is single-valued.  Call it cbpmR.
cbpmR=sif//"_rsp_cbpm.pl"

# Read CBPMnamefile to obtain the CBPM filename of the original (not
# reprojected) SIF.  This mask is multiple-valued.  Call it cbpm.
fields(CBPMnamefile,"1",lines="1") | scan(cbpmFULL)
cbpm=substr(cbpmFULL,strldx("/",cbpmFULL)+1,strlstr(".pl",cbpmFULL)-1)

# Set a temporary name (cbpmTMP) and rename the mscimage-created mask
# from stiresample.cl so that cbpmTMP now contains the mask created
# by mscimage.
cbpmTMP = "stimask_tmp.pl"
imrename( cbpmR, cbpmTMP )

# Define how mskexpr should work
printf ("reset pmmatch = \"world 9\"\nkeep\n") | cl

# Match the original non-resampled multiple-valued mask (cbpm) to the
# reprojected mask (cbpmTMP after rename), and write the result to the
# original mscimage-created mask name (cbpmR; which no longer exists
# since the rename).
mskexpr( "m>0?m:(i>0?i: 0)", cbpmR, cbpmTMP, refmask=cbpm )

# No need to update the the value of BPM keyword in the resampled SIF
# since it is the same filename.
#hedit( sif//"_rsp", "BPM", cbpmR, add+, ver-, update+ )

# Clean up
# imdelete(cbpm,verify-)
# imdelete(cbpmTMP,verify-)


logout( status )
