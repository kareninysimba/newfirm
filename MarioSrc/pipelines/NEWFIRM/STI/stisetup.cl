#!/bin/env pipecl
#
# STIsetup
#
# Description:
#
# 	This module copies the cumulative mask for the SIF to the current
#   directory.  It sets up SIFnamefile ("stisetup-SIF.tmp"), which is a copy
#   of the first column of ifile, containing the full pathname of the SIF to
#   be processed by STI.  The module also sets up REFnamefile
#   ("stisetup-REF.tmp"), which is a copy of the second column of ifile, 
#   containing the full pathname of the reference SIF.  The module also sets
#   up CBPMnamefile ("stisetup-CBPM.tmp") containing the full pathname of the
#   CBPM associated with the SIF, as saved in the calibration manager.
#
# Exit Status Values:
#
#   0 = Unsuccessful (set for a getcal failure or error in reading ifile)
#	1 = Successful

# Declare variables
int		status
string	dataset,datadir,shortname,ifile,lfile
string  SIFnamefile,REFnamefile,CBPMnamefile
string	sifFULL,sifREFFULL,msg,obsid,cbpmFULL,cbpm,sif
file	caldir = "MC$"

# Load packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
proto
servers
images

# Set file and path names
names( "sti", envget( "OSF_DATASET" ) )
dataset = names.dataset
datadir = names.datadir
shortname = names.shortname
ifile = names.indir//dataset//".sti"
lfile= datadir//names.lfile
SIFnamefile = "stisetup-SIF.tmp"
REFnamefile = "stisetup-REF.tmp"
CBPMnamefile = "stisetup-CBPM.tmp"
set( uparm=names.uparm )
set( pipedata=names.pipedata )
cd( datadir )

# Log start of processing.
printf("\nSTIsetup (%s): ",dataset) | tee(lfile)
time | tee(lfile)

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1//"[0]" )
} then {
    logout 0
} else
    ;

# Initialize exit status to 1 (successful)
status = 1

# Read full pathname of SIF and reference SIF from ifile.
sifFULL = INDEF ; fields( ifile, "1,2", lines="1" ) | 
    scan( sifFULL, sifREFFULL )
# Copy the file to here
sif = substr( sifFULL, strldx("/",sifFULL)+1, strlstr(".fits",sifFULL)-1 )
if ( imaccess( sif ) ) {
    imdel( sif )
}
;
imcopy( sifFULL, sif )

# Set up SIFnamefile (stisetup-SIF.tmp) and REFnamefile with full pathname
# of SIF and reference SIF (for purpose of resampling in stiresample.cl),
# respectively, SIF (for purpose of resampling in stiresample.cl) so that
# ifile is ONLY read by stisetup.  In this way, ifile is preserved, but the
# STI modules can read and update SIFnamefile as necessary.  With this setup,
# stisetup-SIF.tmp can be thought of as containing the name of the *working*
# copy of the SIF for STI.
printf( "%s\n", sifFULL, > SIFnamefile )
printf( "%s\n", sifREFFULL, > REFnamefile )

# Retrieve location of current bad pixel mask for SIF
getcal( sif, "bpm", cm, caldir, obstype="", detector="!instrume",
    imageid="!detector", filter="", exptime="", mjd="!mjd-obs" )
if ( getcal.statcode > 0 ) {
    msg = str(getcal.statcode)//" "//getcal.statstr
    sendmsg( "ERROR", "Getcal failed for bpm", msg, "CAL" )
    status = 0
}
;

# Retrieve location of current cumulative mask for SIF
hselect( sif, "OBSID",yes) | scan(obsid)
getcal( sif, "masks", cm, caldir, imageid="!detector", mjd="!mjd-obs",
    match="%"//obsid, obstype="", detector="", filter="" )
i = 0
while ( (getcal.statcode>0) && (i<=5) ) {
    # Try again a bit later
    print "Retrying..."
    sleep( 300 )
    getcal( sif, "masks", cm, caldir, imageid="!detector", mjd="!mjd-obs",
        match="%"//obsid, obstype="", detector="", filter="" )
    i = i+1
}
if ( getcal.statcode > 0 ) {
    msg = str(getcal.statcode)//" "//getcal.statstr
    sendmsg( "ERROR", "Getcal failed for masks", msg, "PROC" )
    status = 0
}
;

# Copy cumulative mask for SIF to the current directory
cbpmFULL = getcal.value
cbpm = substr( cbpmFULL, strldx("/",cbpmFULL)+1, 999 )
if ( access(cbpm) ) delete( cbpm, verify- )
;
imcopy( cbpmFULL, cbpm, verbose- )

# Set up file (stisetup-CBPM.tmp) with full pathname of CBPM for SIF, as
# stored in the calibration manager.  In this way, only the stisetup module
# in STI needs to do a getcal to retrieve the CBPM.  Since masks are always
# copied over to the working directories, stisetup-CBPM.tmp does not 
# necessarily mean that it contains the working copy of the CBPM file.
# In STI, we assume that the working copy of the CBPM is within the
# current directory (datadir), but each module that updates CBPM copies
# the revised CBPM back to the saved location in the calibration manager.
# In this way, the calibration manager is always up to date, in case later
# the STI pipeline calls another pipeline.  In that case, however, it will
# be necessary to copy over the CBPM to the working directory (datadir) 
# again upond return to STI in order to ensure that the most up to date
# CBPM is found in the working directory.  One could avoid this by copying
# over the CBPM at the start of each module that will make use of the CBPM,
# but that would be overkill.
printf( "%s\n", cbpmFULL, > CBPMnamefile )

logout( status )
