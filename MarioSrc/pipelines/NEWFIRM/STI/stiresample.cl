#!/bin/env pipecl
#
# STIresample

int     status = 1
real    ra, dec
string  dataset, datadir, shortname, lfile
string  SIFnamefile, REFnamefile, CBPMnamefile
string	sifFULL, sifREFFULL, sif, interpolant

# Load the packages.
redefine mscred=mscred$mscred.cl
images
lists
noao
nproto
mscred
cache mscred

# Set the dataset name and uparm.
names( "sti", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
shortname = names.shortname
lfile = datadir // names.lfile
SIFnamefile = "stisetup-SIF.tmp"
REFnamefile = "stisetup-REF.tmp"
CBPMnamefile = "stisetup-CBPM.tmp"
set (uparm = names.uparm)

# Log start of processing.
printf ("\n%s (%s): ",envget("NHPPS_MODULE_NAME"),dataset) | tee( lfile )
time | tee( lfile )

# Go to the relevant directory
cd( datadir )

# Read SIFnamefile and REFnamefile to get full pathname of SIF (sifFULL) and
# the reference SIF (sifREFFULL).  Extract basename (sif).
fields( SIFnamefile, "1", lines="1" ) | scan( sifFULL )
fields( REFnamefile, "1", lines="1" ) | scan( sifREFFULL )
sif = substr( sifFULL, strldx("/",sifFULL)+1, strlstr(".fits",sifFULL)-1 )

# Resample the SIF
if ( sciencepipeline ) {

    hselect( sifREFFULL, "RSPRA,RSPDEC", yes ) | scan( ra, dec )
    if ( nscan() != 2 ) {
        sendmsg( "ERROR", "Could not retrieve reprojection center", 
            shortname, "PROC" )
        status = 0
    } else {
        interpolant = "linear"
        mscimage( sif, sif//"_rsp", format="image", verbose+,
            ra=ra, dec=dec, scale=rsp_scale, rotation=0,
            wcssource="parameters", interpolant=interpolant )
    }
    
} else {

    interpolant = "linear"
    mscimage( sif, sif//"_rsp", format="image", verbose+,
        scale=rsp_scale, rotation=0, wcssource="parameters",
        reference=sifREFFULL, interpolant=interpolant )
        
}

hedit(sif//"_rsp","BPM",sif//"_rsp_cbpm.pl",add+,verify-,update+)
imrename(sif//"_rsp_bpm.pl",sif//"_rsp_cbpm.pl")
nhedit( sif//"_rsp", "PIXSCALE", rsp_scale, "[arc/pix] Pixel scale",
    add+, ver-, update+ )
nhedit( sif//"_rsp", "PIXSCAL1", rsp_scale, "[arc/pix] Pixel scale",
    add+, ver-, update+ )
nhedit( sif//"_rsp", "PIXSCAL2", rsp_scale, "[arc/pix] Pixel scale",
    add+, ver-, update+ )

logout( status )
