  �V  #I  /$TITLE = "OBJECT"
$CTIME = 887396597
$MTIME = 887396597
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
NEXTEND =                    4 / Number of extensions
FILENAME= 'dflatJ0461.fits'    / Original host filename
OBSTYPE = 'object  '           / Observation type
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '5:26:51.79'         / RA of observation (hr)
DEC     = '-17:00:00.0'        / DEC of observation (deg)
OBJRA   = '5:26:51.79'         / right ascension [HH:MM:SS.SS]
OBJDEC  = '-17:00:00.0'        / declination [DD:MM:SS.S]
OBJEPOCH=               2007.8 / epoch [YYYY]

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2007-10-31T10:15:55.0' / Date of observation start (UTC)
TIME-OBS= '10:15:55'           / universal time [HH:MM:SS]
MJD-OBS =       54404.42771991 / MJD of observation start
ST      = '5:26:53 '           / sidereal time [HH:MM:SS]

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2007.8 / Equinox of tel coords
TELRA   = '5:26:51.79'         / RA of telescope (hr)
TELDEC  = '-17:00:00.0'        / DEC of telescope (deg)
HA      = '0:00:00.13'         / telescope ha [No Units]
ZD      =              48.9625 / Zenith distance
AIRMASS =                1.521 / Airmass
TELFOCUS=                10975 / Telescope focus

DETECTOR= 'NEWFIRM '           / Mosaic detector
MOSSIZE = '[1:4096,1:4096]'    / Mosaic detector size
NDETS   =                    4 / Number of detectors in mosaic
FILTER  = 'J       '           / Filter name(s)

OBSERVER= 'Hal_Halbedel'       / Observer(s)
PROPOSER= 'Ron_Probst'         / Proposer(s)
PROPID  = '2007A-TnE'          / Proposal identification
OBSID   = 'kp4m.20071031T101555' / Observation ID
SEQID   = 'NFQR_2007ATnE_0027' / Sequence ID
SEQNUM  =                   27 / Sequence Number
EXPID   =                    0 / Monsoon exposure ID
NOCID   =      2454405.1359522 / NEWFIRM ID

NOHS    = '1.1.1   '           / NOHS ID [No Units]
NOCNO   =                    1 / observation number in this sequence [No Units]
NOCUTC  =             20071031 / NOCUTC ID [YYYYMMDD]
NOCRSD  =      2454405.1215835 / recipe date [MSD]
NOCORA  =                    0 / RA offset [Arcseconds]
NOCDITER=                    0 / dither iteration count [No Units]
NOCSKY  =                    0 / sky offset modulus [No Units]
NOCDPAT = '5PX     '           / dither pattern [No Units]
NOCGID  =      2454405.1215835 / group identification number [MSD]
NOCDROF =                    0 / dither RA offset [Arcseconds]
NOCDHS  = 'STARE   '           / DHS script name [No Units]
NOCNAME = 'dflatJ5s.fits'      / file name for data set [No Units]
NOCMPOS =                    0 / map position [No Units]
NOCTIM  =                    5 / requested integration time [Seconds]
NOCMDOF =                    0 / map Dec offset [Arcminutes]
NOCMREP =                    0 / map repetition count [No Units]
NOCTOT  =                    5 / total number of observations in set [No Units]
NOCFOCUS=                11200 / ntcs_focus value [micron]
NOCSYS  = 'kpno_4m '           / system ID [No Units]
NOCLAMP = 'off     '           / dome flat lamp status (on|off)
NOCDPOS =                    0 / dither position [No Units]
NOCMITER= 'arcminutes'         / map iteration count [No Units]
NOCID   =      2454405.1359522 / observation ID a.k.a expID [MSD]
NOCODEC =                    0 / Dec offset [Arcseconds]
NOCMPAT = '5PX     '           / map pattern [No Units]
NOCDDOF =                    0 / dither Dec offset [Arcseconds]
NOCNUM  =                    5 / observation number request [No Units]
NOCMROF =                    0 / map RA offset [Arcminutes]
NOCTYP  = 'OBJECT  '           / observation type [No Units]
NOCDREP =                    0 / dither repetition count [No Units]

NFOSSTMP=            64.988998 / oss temp measured [K]
NFDETTMP=                   30 / detector array temp measured [K]

NFFILPOS= 'J       '           / detected position name [No Units]
NFFW2POS=                    1 / wheel 2 actual position (0|1|2|3|4|5|6|7|8) [No
NFFW1POS=                    8 / wheel 1 actual position (0|1|2|3|4|5|6|7|8) [No

DECDIFF =                    0 / dec diff [Arcsecond]
AZ      = '180:00:02.4'        / telescope azimuth [DD:MM:SS]
DECOFF  =                    0 / dec offset [Arcsecond]
RAOFF   =                    0 / ra offset [Arcsecond]
RAINDEX =              -221.89 / ra index [Arcsecond]
ALT     = '41:02:15.0'         / telescope altitude [HH:MM:SS]
RAZERO  =                33.62 / ra zero [Arcsecond]
RADIFF  =                    0 / ra diff [Arcsecond]
DECZERO =                91.12 / dec zero [Arcsecond]
DECINDEX=                   35 / dec index [Arcsecond]

DOMEERR =              -135.32 / dome error as distance from target [Degrees]
DOMEAZ  =                359.3 / dome position [Degrees]

NFC1POS = '00:00:00.00 00:00:00.0 2007 [s]' / camera 1 target in RA Dec Epoch [H
NFC1GDR = 'off [s] '           / camera 1 guider mode [No Units]
NFC1FILT= '0 [s]   '           / camera 1 filter [No Units]

TCPGDR  = 'off     '           / guider status (on|off) [No Units]

NFC2FILT= '0 [s]   '           / camera 2 filter [No Units]
NFC2POS = '00:00:00.00 00:00:00.0 2007 [s]' / camera 2 target in RA Dec Epoch [H
NFC2GDR = 'off [s] '           / camera 2 guider mode [No Units]

NFECPOS = 'open    '           / detected position (open|close|between) [No Unit
PROCMEAN=             1973.472
PCOUNT  =                 2766 / Heap size in bytes
GCOUNT  =                    1 / Only one group
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
EXTNAME = 'im4     '           / Extension name
EXTVER  =                    4 / Extension version
INHERIT =                    T / Inherits global header
DATE    = '2008-02-13T19:49:14' / Date FITS file was generated
IRAF-TLM= '12:49:14 (13/02/2008)' / Time of last modification
OBJECT  = 'OBJECT  '           / Name of the object observed
IMAGEID =                    4 / Image identification
EXPTIME =                    5 / Actual integration time in seconds
MJDSTART=        54404.6359817 / MJD of observation start
MJDEND  =        54404.6360396 / MJD of observation end

DETNAME = 'Raytheon InSb #4 (NOAO 4)' / Array name
DETSIZE = '[1:2046,1:2046]'    / Detector size
DETSEC  = '[1:2046,1:2046]'    / Detector section

LTM1_1  =                  1.0 / Detector to image transformation
LTM2_2  =                  1.0 / Detector to image transformation

WCSASTRM= 'image1399 (USNO N J) by F. Valdes 2007-06-29' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =      81.715791666667 / Coordinate reference value
CRVAL2  =                -17.0 / Coordinate reference value
CRPIX1  =      2117.6506869816 / Coordinate reference pixel
CRPIX2  =      2159.6650113076 / Coordinate reference pixel
CD1_1   =   3.402936272544e-07 / Coordinate matrix
CD2_1   =  0.00010985985406231 / Coordinate matrix
CD1_2   = -0.00010981888840317 / Coordinate matrix
CD2_2   = -4.7043134752964e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 1. 0.01176228245223173 0.2370'
WAT1_002= '378097044547 -0.2324814639882445 -0.006856030325125704 -1.4027357063'
WAT1_003= '75256E-4 -3.303816084108529E-6 -2.940697333784130E-4 4.4286010870610'
WAT1_004= '29E-6 -4.581573966225916E-5 2.045805513099013E-4 -1.412689254225243E'
WAT1_005= '-4 -7.954678764107602E-6 -3.787594499318694E-5 4.464822324695919E-6 '
WAT1_006= '4.198903381990655E-5 -6.837145271843302E-6 -2.883894074501925E-5 -2.'
WAT1_007= '461836066454554E-5 3.019921630429422E-6 -4.252529188930910E-6 "'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 1. 0.01176228245223173 0.237'
WAT2_002= '0378097044547 -0.2324814639882445 -0.006856030325125704 1.2742799259'
WAT2_003= '31010E-4 -4.349480200878276E-5 4.148566859753761E-5 -2.6052063334579'
WAT2_004= '81E-5 -3.645675489681360E-5 -2.118608567066150E-4 1.118190220558664E'
WAT2_005= '-5 2.174328553083507E-5 2.864496746133038E-4 -1.611382572045497E-4 -'
WAT2_006= '2.625062187158997E-5 -8.310254885851297E-7 -1.074261841307059E-6 9.6'
WAT2_007= '68190182693476E-6 5.338680619817492E-6 -5.087574839828746E-6 "'

PROC0001= 'Oct 31 12:57 Trim $I'
PROC0002= 'Oct 31 12:57 trimsec = [2:2047,2:2047]'
PROCDONE= 'T       '
PROCAVG =             2038.685
PROCSIG =             142.0703
PROCID  = 'kp4m.20071031T101555V1'
PROCID01= 'kp4m.20071031T101555'
PROCID02= 'kp4m.20071031T101604'
PROCID03= 'kp4m.20071031T101613'
PROCID04= 'kp4m.20071031T101622'
PROCID05= 'kp4m.20071031T101634'
IMCMB001= '        '
IMCMB002= '        '
IMCMB003= '        '
IMCMB004= '        '
IMCMB005= '        '
NCOMBINE=                    5
BPM     = 'bpm071031_im4.pl'
    �     �  �                 o  o  �   �  /      ���               �� �       @�``````` @ @ @`` @```````````` AV``````` @ @ @` @ @` @ @ @``````` @7```` @ @ @ @ @```` @�``````` @`` @``````` AJ```` @ @` @ @```` B4````` @` @````` A.   ��      � � ��      P[R�S� �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@  �  ��      P[R��@" �  ��      P[R��@! �  ��      P[R��@  �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �  ��      P[R��@ �V ��      P[R�S� �  ��       X@R�S� �  ��       S@R�S� �  ��       Q@R�S� �  ��       O@R�S� �  ��       N@R�S� �  ��       M@R�S� �  ��       L@R�S� �  ��       K@R�S� �  ��       J@R�S� �  ��       I@ R�S� �  ��       H@"R�S� �  ��       I@!R�S� �  ��      G�  ��       I@ R�S� �  ��       J@R�S� �  ��       K@R�S� �  ��       L@R�S� �  ��       M@R�S� �  ��       N@R�S� �  ��       O@R�S� �  ��       P@R�S� �  ��       R@R�S� �  ��       T@R�S� �  ��       Y@R�S� � 7 ��      P[R�S� �  ��      P[Q�P�S� �  ��      P[�@	P�S� �  ��      P[�@P�S� �  ��      P[�@P�S� �  ��      P[�@P�S� �  ��      P[�@P�S� �  ��      P[�@P�S� �  ��      P[�@P�S� �  ��      P[�@P�S� �  ��      P[�@P�S� �  ��      P[�@P�S� �  ��      P[�@	P�S� �  ��      P[Q�P�S� � � ��      P[R�S� �  ��      P[QAQ�S� �  ��      P[<@	Q�S� �  ��      P[:@Q�S� �  ��      P[9@Q�S� �  ��      P[8@Q�S� �  ��      P[8@Q�S� �  ��      P[7@Q�S� �  ��      P[7@Q�S� �  ��      P[6@Q�S� �  ��      P[7@Q�S� �  ��      P[7@Q�S� �  ��      P[8@Q�S� �  ��      P[8@Q�S� �  ��      P[9@Q�S� �  ��      P[:@Q�S� �  ��      P[;@Q�S� �  ��      P[=@	Q�S� �  ��      P[QBQ�S� �J ��      P[R�S� �  ��      P[RP�S� �  ��      P[@	P�S� �  ��      P[@P�S� �  ��      P[@P�S� �  ��      P[@P�S� �  ��      P[@P�S� �  ��      P[@P�S� �  ��      P[@P�S� �  ��      P[@P�S� �  ��      P[@P�S� �  ��      P[@P�S� �  ��      P[@	P�S� �  ��      P[RP�S� �4 ��      P[R�S� �  ��      P[�@S� �  ��      P[�@
S� �  ��      P[�@S� �  ��      P[�@S� �  ��      P[�@S� �  ��      P[�@S� �  ��      P[�@S� �  ��      P[�@S� �  ��      P[�@S� �  ��      P[�@S� �  ��      P[�@S� �  ��      P[�@
S� �  ��      P[�@S� �. ��      P[R�S� �