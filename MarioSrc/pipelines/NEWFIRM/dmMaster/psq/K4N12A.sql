REPLACE INTO PSQ VALUES('K4N12A', 'K4N12A', 'K4N12AD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N12A (dataset) VALUES ('20120227');
REPLACE INTO K4N12A (dataset) VALUES ('20120302');
REPLACE INTO K4N12A (dataset) VALUES ('20120306');
REPLACE INTO K4N12A (dataset) VALUES ('20120308');
REPLACE INTO K4N12A (dataset) VALUES ('20120311');
REPLACE INTO K4N12A (dataset) VALUES ('20120329');
REPLACE INTO K4N12A (dataset) VALUES ('20120401');
REPLACE INTO K4N12A (dataset) VALUES ('20120405');
REPLACE INTO K4N12A (dataset) VALUES ('20120409');
REPLACE INTO K4N12A (dataset) VALUES ('20120627');
REPLACE INTO K4N12A (dataset) VALUES ('20120628');
REPLACE INTO K4N12A (dataset) VALUES ('20120701');
REPLACE INTO K4N12A (dataset) VALUES ('20120703');
REPLACE INTO K4N12A (dataset) VALUES ('20120705');
REPLACE INTO K4N12A (dataset) VALUES ('20120709');

REPLACE INTO K4N12AD VALUES ('20120227', '20120227', '20120301',
  '"start_date" between timestamp \'2012-02-27\' and timestamp \'2012-03-01\'');
REPLACE INTO K4N12AD VALUES ('20120302', '20120302', '20120305',
  '"start_date" between timestamp \'2012-03-02\' and timestamp \'2012-03-05\'');
REPLACE INTO K4N12AD VALUES ('20120306', '20120306', '20120307',
  '"start_date" between timestamp \'2012-03-06\' and timestamp \'2012-03-07\'');
REPLACE INTO K4N12AD VALUES ('20120308', '20120308', '20120310',
  '"start_date" between timestamp \'2012-03-08\' and timestamp \'2012-03-10\'');
REPLACE INTO K4N12AD VALUES ('20120311', '20120311', '20120312',
  '"start_date" between timestamp \'2012-03-11\' and timestamp \'2012-03-12\'');
REPLACE INTO K4N12AD VALUES ('20120329', '20120329', '20120331',
  '"start_date" between timestamp \'2012-03-29\' and timestamp \'2012-03-31\'');
REPLACE INTO K4N12AD VALUES ('20120401', '20120401', '20120404',
  '"start_date" between timestamp \'2012-04-01\' and timestamp \'2012-04-04\'');
REPLACE INTO K4N12AD VALUES ('20120405', '20120405', '20120408',
  '"start_date" between timestamp \'2012-04-05\' and timestamp \'2012-04-08\'');
REPLACE INTO K4N12AD VALUES ('20120409', '20120409', '20120410',
  '"start_date" between timestamp \'2012-04-09\' and timestamp \'2012-04-10\'');
REPLACE INTO K4N12AD VALUES ('20120627', '20120627', '20120627',
  '"start_date" between timestamp \'2012-06-27\' and timestamp \'2012-06-27\'');
REPLACE INTO K4N12AD VALUES ('20120628', '20120628', '20120630',
  '"start_date" between timestamp \'2012-06-28\' and timestamp \'2012-06-30\'');
REPLACE INTO K4N12AD VALUES ('20120701', '20120701', '20120702',
  '"start_date" between timestamp \'2012-07-01\' and timestamp \'2012-07-02\'');
REPLACE INTO K4N12AD VALUES ('20120703', '20120703', '20120704',
  '"start_date" between timestamp \'2012-07-03\' and timestamp \'2012-07-04\'');
REPLACE INTO K4N12AD VALUES ('20120705', '20120705', '20120708',
  '"start_date" between timestamp \'2012-07-05\' and timestamp \'2012-07-08\'');
REPLACE INTO K4N12AD VALUES ('20120709', '20120709', '20120709',
  '"start_date" between timestamp \'2012-07-09\' and timestamp \'2012-07-09\'');
