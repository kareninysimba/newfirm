# MKPSQSQL -- Convert getrun data into sql data.

procedure mkpsqsql(queue)

string	queue
string	instrument = "NEWFIRM"
string	query = "instrument=''newfirm''"

begin
	string	q, d, a, b
	a="";b="";d="";

	q = queue

	printf ("REPLACE INTO PSQ VALUES('%s', '%s', '%sD', '%s',\n",
	    q, q, q, instrument)
	printf ("'dir', 'disabled', '%s');\n\n", query)

	list = q//"QD.out"
	while (fscan (list, d) != EOF) {
	    printf ("REPLACE INTO %s (dataset) VALUES ('%s');\n", q, d)
	}
	list = ""
	printf ("\n")

	list = q//"QD.out"
	while (fscan (list, d, a, b) != EOF) {
	    printf ("REPLACE INTO %sD VALUES ('%s', '%s', '%s',\n", q, d, a, b)
#	    printf ("  'start_date between timestamp ''%s-%s-%s'' and timestamp ''%s-%s-%s''');\n",
	    printf ("  \'\"start_date\" between timestamp \\\\\'%s-%s-%s\\\\\' and timestamp \\\\\'%s-%s-%s\\\\\'\');\n",
		substr(a,1,4), substr(a,5,6), substr(a,7,8),
		substr(b,1,4), substr(b,5,6), substr(b,7,8))
	}
	list = ""
end
