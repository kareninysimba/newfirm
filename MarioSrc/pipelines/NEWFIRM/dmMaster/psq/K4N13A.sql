REPLACE INTO PSQ VALUES('K4N13A', 'K4N13A', 'K4N13AD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N13A (dataset) VALUES ('20130201');
REPLACE INTO K4N13A (dataset) VALUES ('20130325');
REPLACE INTO K4N13A (dataset) VALUES ('20130327');
REPLACE INTO K4N13A (dataset) VALUES ('20130331');
REPLACE INTO K4N13A (dataset) VALUES ('20130403');
REPLACE INTO K4N13A (dataset) VALUES ('20130405');
REPLACE INTO K4N13A (dataset) VALUES ('20130409');

REPLACE INTO K4N13AD VALUES ('20130201', '20130201', '20130203',
  '"start_date" between timestamp \'2013-02-01\' and timestamp \'2013-02-03\'');
REPLACE INTO K4N13AD VALUES ('20130325', '20130325', '20130326',
  '"start_date" between timestamp \'2013-03-25\' and timestamp \'2013-03-26\'');
REPLACE INTO K4N13AD VALUES ('20130327', '20130327', '20130330',
  '"start_date" between timestamp \'2013-03-27\' and timestamp \'2013-03-30\'');
REPLACE INTO K4N13AD VALUES ('20130331', '20130331', '20130402',
  '"start_date" between timestamp \'2013-03-31\' and timestamp \'2013-04-02\'');
REPLACE INTO K4N13AD VALUES ('20130403', '20130403', '20130404',
  '"start_date" between timestamp \'2013-04-03\' and timestamp \'2013-04-04\'');
REPLACE INTO K4N13AD VALUES ('20130405', '20130405', '20130408',
  '"start_date" between timestamp \'2013-04-05\' and timestamp \'2013-04-08\'');
REPLACE INTO K4N13AD VALUES ('20130409', '20130409', '20130410',
  '"start_date" between timestamp \'2013-04-09\' and timestamp \'2013-04-10\'');
