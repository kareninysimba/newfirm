REPLACE INTO PSQ VALUES('K4N13B', 'K4N13B', 'K4N13BD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N13B (dataset) VALUES ('20131014');
REPLACE INTO K4N13B (dataset) VALUES ('20131016');
REPLACE INTO K4N13B (dataset) VALUES ('20131020');
REPLACE INTO K4N13B (dataset) VALUES ('20131024');
REPLACE INTO K4N13B (dataset) VALUES ('20131028');
REPLACE INTO K4N13B (dataset) VALUES ('20131031');
REPLACE INTO K4N13B (dataset) VALUES ('20131104');
REPLACE INTO K4N13B (dataset) VALUES ('20131105');
REPLACE INTO K4N13B (dataset) VALUES ('20131108');
REPLACE INTO K4N13B (dataset) VALUES ('20131112');
REPLACE INTO K4N13B (dataset) VALUES ('20131114');
REPLACE INTO K4N13B (dataset) VALUES ('20131118');
REPLACE INTO K4N13B (dataset) VALUES ('20131121');
REPLACE INTO K4N13B (dataset) VALUES ('20131125');
REPLACE INTO K4N13B (dataset) VALUES ('20131205');
REPLACE INTO K4N13B (dataset) VALUES ('20131209');

REPLACE INTO K4N13BD VALUES ('20131014', '20131014', '20131015',
  '"start_date" between timestamp \'2013-10-14\' and timestamp \'2013-10-15\'');
REPLACE INTO K4N13BD VALUES ('20131016', '20131016', '20131019',
  '"start_date" between timestamp \'2013-10-16\' and timestamp \'2013-10-19\'');
REPLACE INTO K4N13BD VALUES ('20131020', '20131020', '20131023',
  '"start_date" between timestamp \'2013-10-20\' and timestamp \'2013-10-23\'');
REPLACE INTO K4N13BD VALUES ('20131024', '20131024', '20131027',
  '"start_date" between timestamp \'2013-10-24\' and timestamp \'2013-10-27\'');
REPLACE INTO K4N13BD VALUES ('20131028', '20131028', '20131030',
  '"start_date" between timestamp \'2013-10-28\' and timestamp \'2013-10-30\'');
REPLACE INTO K4N13BD VALUES ('20131031', '20131031', '20131103',
  '"start_date" between timestamp \'2013-10-31\' and timestamp \'2013-11-03\'');
REPLACE INTO K4N13BD VALUES ('20131104', '20131104', '20131104',
  '"start_date" between timestamp \'2013-11-04\' and timestamp \'2013-11-04\'');
REPLACE INTO K4N13BD VALUES ('20131105', '20131105', '20131107',
  '"start_date" between timestamp \'2013-11-05\' and timestamp \'2013-11-07\'');
REPLACE INTO K4N13BD VALUES ('20131108', '20131108', '20131111',
  '"start_date" between timestamp \'2013-11-08\' and timestamp \'2013-11-11\'');
REPLACE INTO K4N13BD VALUES ('20131112', '20131112', '20131113',
  '"start_date" between timestamp \'2013-11-12\' and timestamp \'2013-11-13\'');
REPLACE INTO K4N13BD VALUES ('20131114', '20131114', '20131117',
  '"start_date" between timestamp \'2013-11-14\' and timestamp \'2013-11-17\'');
REPLACE INTO K4N13BD VALUES ('20131118', '20131118', '20131120',
  '"start_date" between timestamp \'2013-11-18\' and timestamp \'2013-11-20\'');
REPLACE INTO K4N13BD VALUES ('20131121', '20131121', '20131124',
  '"start_date" between timestamp \'2013-11-21\' and timestamp \'2013-11-24\'');
REPLACE INTO K4N13BD VALUES ('20131125', '20131125', '20131125',
  '"start_date" between timestamp \'2013-11-25\' and timestamp \'2013-11-25\'');
REPLACE INTO K4N13BD VALUES ('20131205', '20131205', '20131208',
  '"start_date" between timestamp \'2013-12-05\' and timestamp \'2013-12-08\'');
REPLACE INTO K4N13BD VALUES ('20131209', '20131209', '20131210',
  '"start_date" between timestamp \'2013-12-09\' and timestamp \'2013-12-10\'');
