REPLACE INTO PSQ VALUES('K4N14A', 'K4N14A', 'K4N14AD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N14A (dataset) VALUES ('20140304');
REPLACE INTO K4N14A (dataset) VALUES ('20140305');
REPLACE INTO K4N14A (dataset) VALUES ('20140309');
REPLACE INTO K4N14A (dataset) VALUES ('20140310');
REPLACE INTO K4N14A (dataset) VALUES ('20140312');
REPLACE INTO K4N14A (dataset) VALUES ('20140314');
REPLACE INTO K4N14A (dataset) VALUES ('20140317');
REPLACE INTO K4N14A (dataset) VALUES ('20140321');
REPLACE INTO K4N14A (dataset) VALUES ('20140325');

REPLACE INTO K4N14AD VALUES ('20140304', '20140304', '20140304',
  '"start_date" between timestamp \'2014-03-04\' and timestamp \'2014-03-04\'');
REPLACE INTO K4N14AD VALUES ('20140305', '20140305', '20140308',
  '"start_date" between timestamp \'2014-03-05\' and timestamp \'2014-03-08\'');
REPLACE INTO K4N14AD VALUES ('20140309', '20140309', '20140309',
  '"start_date" between timestamp \'2014-03-09\' and timestamp \'2014-03-09\'');
REPLACE INTO K4N14AD VALUES ('20140310', '20140310', '20140311',
  '"start_date" between timestamp \'2014-03-10\' and timestamp \'2014-03-11\'');
REPLACE INTO K4N14AD VALUES ('20140312', '20140312', '20140313',
  '"start_date" between timestamp \'2014-03-12\' and timestamp \'2014-03-13\'');
REPLACE INTO K4N14AD VALUES ('20140314', '20140314', '20140316',
  '"start_date" between timestamp \'2014-03-14\' and timestamp \'2014-03-16\'');
REPLACE INTO K4N14AD VALUES ('20140317', '20140317', '20140318',
  '"start_date" between timestamp \'2014-03-17\' and timestamp \'2014-03-18\'');
REPLACE INTO K4N14AD VALUES ('20140321', '20140321', '20140324',
  '"start_date" between timestamp \'2014-03-21\' and timestamp \'2014-03-24\'');
REPLACE INTO K4N14AD VALUES ('20140325', '20140325', '20140326',
  '"start_date" between timestamp \'2014-03-25\' and timestamp \'2014-03-26\'');
