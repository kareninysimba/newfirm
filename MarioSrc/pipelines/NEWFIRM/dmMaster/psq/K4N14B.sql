REPLACE INTO PSQ VALUES('K4N14B', 'K4N14B', 'K4N14BD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N14B (dataset) VALUES ('20141001');
REPLACE INTO K4N14B (dataset) VALUES ('20141003');
REPLACE INTO K4N14B (dataset) VALUES ('20141007');
REPLACE INTO K4N14B (dataset) VALUES ('20141010');
REPLACE INTO K4N14B (dataset) VALUES ('20141014');
REPLACE INTO K4N14B (dataset) VALUES ('20141017');
REPLACE INTO K4N14B (dataset) VALUES ('20141028');
REPLACE INTO K4N14B (dataset) VALUES ('20141101');
REPLACE INTO K4N14B (dataset) VALUES ('20141103');
REPLACE INTO K4N14B (dataset) VALUES ('20141106');
REPLACE INTO K4N14B (dataset) VALUES ('20141110');
REPLACE INTO K4N14B (dataset) VALUES ('20141113');
REPLACE INTO K4N14B (dataset) VALUES ('20141117');
REPLACE INTO K4N14B (dataset) VALUES ('20141121');
REPLACE INTO K4N14B (dataset) VALUES ('20141125');

REPLACE INTO K4N14BD VALUES ('20141001', '20141001', '20141002',
  '"start_date" between timestamp \'2014-10-01\' and timestamp \'2014-10-02\'');
REPLACE INTO K4N14BD VALUES ('20141003', '20141003', '20141006',
  '"start_date" between timestamp \'2014-10-03\' and timestamp \'2014-10-06\'');
REPLACE INTO K4N14BD VALUES ('20141007', '20141007', '20141009',
  '"start_date" between timestamp \'2014-10-07\' and timestamp \'2014-10-09\'');
REPLACE INTO K4N14BD VALUES ('20141010', '20141010', '20141013',
  '"start_date" between timestamp \'2014-10-10\' and timestamp \'2014-10-13\'');
REPLACE INTO K4N14BD VALUES ('20141014', '20141014', '20141016',
  '"start_date" between timestamp \'2014-10-14\' and timestamp \'2014-10-16\'');
REPLACE INTO K4N14BD VALUES ('20141017', '20141017', '20141020',
  '"start_date" between timestamp \'2014-10-17\' and timestamp \'2014-10-20\'');
REPLACE INTO K4N14BD VALUES ('20141028', '20141028', '20141031',
  '"start_date" between timestamp \'2014-10-28\' and timestamp \'2014-10-31\'');
REPLACE INTO K4N14BD VALUES ('20141101', '20141101', '20141102',
  '"start_date" between timestamp \'2014-11-01\' and timestamp \'2014-11-02\'');
REPLACE INTO K4N14BD VALUES ('20141103', '20141103', '20141104',
  '"start_date" between timestamp \'2014-11-03\' and timestamp \'2014-11-04\'');
REPLACE INTO K4N14BD VALUES ('20141106', '20141106', '20141109',
  '"start_date" between timestamp \'2014-11-06\' and timestamp \'2014-11-09\'');
REPLACE INTO K4N14BD VALUES ('20141110', '20141110', '20141112',
  '"start_date" between timestamp \'2014-11-10\' and timestamp \'2014-11-12\'');
REPLACE INTO K4N14BD VALUES ('20141113', '20141113', '20141116',
  '"start_date" between timestamp \'2014-11-13\' and timestamp \'2014-11-16\'');
REPLACE INTO K4N14BD VALUES ('20141117', '20141117', '20141120',
  '"start_date" between timestamp \'2014-11-17\' and timestamp \'2014-11-20\'');
REPLACE INTO K4N14BD VALUES ('20141121', '20141121', '20141124',
  '"start_date" between timestamp \'2014-11-21\' and timestamp \'2014-11-24\'');
REPLACE INTO K4N14BD VALUES ('20141125', '20141125', '20141125',
  '"start_date" between timestamp \'2014-11-25\' and timestamp \'2014-11-25\'');
