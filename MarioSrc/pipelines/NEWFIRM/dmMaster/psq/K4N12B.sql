REPLACE INTO PSQ VALUES('K4N12B', 'K4N12B', 'K4N12BD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N12B (dataset) VALUES ('20121025');
REPLACE INTO K4N12B (dataset) VALUES ('20121027');
REPLACE INTO K4N12B (dataset) VALUES ('20121029');
REPLACE INTO K4N12B (dataset) VALUES ('20121030');
REPLACE INTO K4N12B (dataset) VALUES ('20121101');
REPLACE INTO K4N12B (dataset) VALUES ('20121102');
REPLACE INTO K4N12B (dataset) VALUES ('20121106');
REPLACE INTO K4N12B (dataset) VALUES ('20121107');
REPLACE INTO K4N12B (dataset) VALUES ('20121109');
REPLACE INTO K4N12B (dataset) VALUES ('20121113');
REPLACE INTO K4N12B (dataset) VALUES ('20121114');
REPLACE INTO K4N12B (dataset) VALUES ('20121115');
REPLACE INTO K4N12B (dataset) VALUES ('20121119');
REPLACE INTO K4N12B (dataset) VALUES ('20121121');
REPLACE INTO K4N12B (dataset) VALUES ('20121125');
REPLACE INTO K4N12B (dataset) VALUES ('20130122');
REPLACE INTO K4N12B (dataset) VALUES ('20130124');
REPLACE INTO K4N12B (dataset) VALUES ('20130128');

REPLACE INTO K4N12BD VALUES ('20121025', '20121025', '20121026',
  '"start_date" between timestamp \'2012-10-25\' and timestamp \'2012-10-26\'');
REPLACE INTO K4N12BD VALUES ('20121027', '20121027', '20121028',
  '"start_date" between timestamp \'2012-10-27\' and timestamp \'2012-10-28\'');
REPLACE INTO K4N12BD VALUES ('20121029', '20121029', '20121029',
  '"start_date" between timestamp \'2012-10-29\' and timestamp \'2012-10-29\'');
REPLACE INTO K4N12BD VALUES ('20121030', '20121030', '20121031',
  '"start_date" between timestamp \'2012-10-30\' and timestamp \'2012-10-31\'');
REPLACE INTO K4N12BD VALUES ('20121101', '20121101', '20121101',
  '"start_date" between timestamp \'2012-11-01\' and timestamp \'2012-11-01\'');
REPLACE INTO K4N12BD VALUES ('20121102', '20121102', '20121105',
  '"start_date" between timestamp \'2012-11-02\' and timestamp \'2012-11-05\'');
REPLACE INTO K4N12BD VALUES ('20121106', '20121106', '20121106',
  '"start_date" between timestamp \'2012-11-06\' and timestamp \'2012-11-06\'');
REPLACE INTO K4N12BD VALUES ('20121107', '20121107', '20121108',
  '"start_date" between timestamp \'2012-11-07\' and timestamp \'2012-11-08\'');
REPLACE INTO K4N12BD VALUES ('20121109', '20121109', '20121112',
  '"start_date" between timestamp \'2012-11-09\' and timestamp \'2012-11-12\'');
REPLACE INTO K4N12BD VALUES ('20121113', '20121113', '20121113',
  '"start_date" between timestamp \'2012-11-13\' and timestamp \'2012-11-13\'');
REPLACE INTO K4N12BD VALUES ('20121114', '20121114', '20121114',
  '"start_date" between timestamp \'2012-11-14\' and timestamp \'2012-11-14\'');
REPLACE INTO K4N12BD VALUES ('20121115', '20121115', '20121118',
  '"start_date" between timestamp \'2012-11-15\' and timestamp \'2012-11-18\'');
REPLACE INTO K4N12BD VALUES ('20121119', '20121119', '20121120',
  '"start_date" between timestamp \'2012-11-19\' and timestamp \'2012-11-20\'');
REPLACE INTO K4N12BD VALUES ('20121121', '20121121', '20121124',
  '"start_date" between timestamp \'2012-11-21\' and timestamp \'2012-11-24\'');
REPLACE INTO K4N12BD VALUES ('20121125', '20121125', '20121128',
  '"start_date" between timestamp \'2012-11-25\' and timestamp \'2012-11-28\'');
REPLACE INTO K4N12BD VALUES ('20130122', '20130122', '20130123',
  '"start_date" between timestamp \'2013-01-22\' and timestamp \'2013-01-23\'');
REPLACE INTO K4N12BD VALUES ('20130124', '20130124', '20130127',
  '"start_date" between timestamp \'2013-01-24\' and timestamp \'2013-01-27\'');
REPLACE INTO K4N12BD VALUES ('20130128', '20130128', '20130131',
  '"start_date" between timestamp \'2013-01-28\' and timestamp \'2013-01-31\'');
