REPLACE INTO PSQ VALUES('K4N16A', 'K4N16A', 'K4N16AD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N16A (dataset) VALUES ('20160220');
REPLACE INTO K4N16A (dataset) VALUES ('20160321');

REPLACE INTO K4N16AD VALUES ('20160220', '20160220', '20160223',
  '"start_date" between timestamp \'2016-02-20\' and timestamp \'2016-02-23\'');
REPLACE INTO K4N16AD VALUES ('20160321', '20160321', '20160324',
  '"start_date" between timestamp \'2016-03-21\' and timestamp \'2016-03-24\'');
