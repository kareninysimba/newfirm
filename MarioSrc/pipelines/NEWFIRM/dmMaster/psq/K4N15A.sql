REPLACE INTO PSQ VALUES('K4N15A', 'K4N15A', 'K4N15AD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N15A (dataset) VALUES ('20150304');
REPLACE INTO K4N15A (dataset) VALUES ('20150308');
REPLACE INTO K4N15A (dataset) VALUES ('20150310');
REPLACE INTO K4N15A (dataset) VALUES ('20150323');
REPLACE INTO K4N15A (dataset) VALUES ('20150327');
REPLACE INTO K4N15A (dataset) VALUES ('20150331');
REPLACE INTO K4N15A (dataset) VALUES ('20150402');
REPLACE INTO K4N15A (dataset) VALUES ('20150406');
REPLACE INTO K4N15A (dataset) VALUES ('20150408');
REPLACE INTO K4N15A (dataset) VALUES ('20150412');

REPLACE INTO K4N15AD VALUES ('20150304', '20150304', '20150307',
  '"start_date" between timestamp \'2015-03-04\' and timestamp \'2015-03-07\'');
REPLACE INTO K4N15AD VALUES ('20150308', '20150308', '20150309',
  '"start_date" between timestamp \'2015-03-08\' and timestamp \'2015-03-09\'');
REPLACE INTO K4N15AD VALUES ('20150310', '20150310', '20150312',
  '"start_date" between timestamp \'2015-03-10\' and timestamp \'2015-03-12\'');
REPLACE INTO K4N15AD VALUES ('20150323', '20150323', '20150326',
  '"start_date" between timestamp \'2015-03-23\' and timestamp \'2015-03-26\'');
REPLACE INTO K4N15AD VALUES ('20150327', '20150327', '20150330',
  '"start_date" between timestamp \'2015-03-27\' and timestamp \'2015-03-30\'');
REPLACE INTO K4N15AD VALUES ('20150331', '20150331', '20150401',
  '"start_date" between timestamp \'2015-03-31\' and timestamp \'2015-04-01\'');
REPLACE INTO K4N15AD VALUES ('20150402', '20150402', '20150405',
  '"start_date" between timestamp \'2015-04-02\' and timestamp \'2015-04-05\'');
REPLACE INTO K4N15AD VALUES ('20150406', '20150406', '20150407',
  '"start_date" between timestamp \'2015-04-06\' and timestamp \'2015-04-07\'');
REPLACE INTO K4N15AD VALUES ('20150408', '20150408', '20150411',
  '"start_date" between timestamp \'2015-04-08\' and timestamp \'2015-04-11\'');
REPLACE INTO K4N15AD VALUES ('20150412', '20150412', '20150412',
  '"start_date" between timestamp \'2015-04-12\' and timestamp \'2015-04-12\'');
