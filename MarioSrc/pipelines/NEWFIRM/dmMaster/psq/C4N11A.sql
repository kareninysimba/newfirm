REPLACE INTO PSQ VALUES('C4N11A', 'C4N11A', 'C4N11AD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO C4N11A (dataset) VALUES ('20110222');
REPLACE INTO C4N11A (dataset) VALUES ('20110223');
REPLACE INTO C4N11A (dataset) VALUES ('20110227');
REPLACE INTO C4N11A (dataset) VALUES ('20110303');
REPLACE INTO C4N11A (dataset) VALUES ('20110304');
REPLACE INTO C4N11A (dataset) VALUES ('20110308');
REPLACE INTO C4N11A (dataset) VALUES ('20110310');
REPLACE INTO C4N11A (dataset) VALUES ('20110314');
REPLACE INTO C4N11A (dataset) VALUES ('20110318');
REPLACE INTO C4N11A (dataset) VALUES ('20110320');
REPLACE INTO C4N11A (dataset) VALUES ('20110410');
REPLACE INTO C4N11A (dataset) VALUES ('20110411');
REPLACE INTO C4N11A (dataset) VALUES ('20110413');
REPLACE INTO C4N11A (dataset) VALUES ('20110416');
REPLACE INTO C4N11A (dataset) VALUES ('20110418');
REPLACE INTO C4N11A (dataset) VALUES ('20110421');
REPLACE INTO C4N11A (dataset) VALUES ('20110425');
REPLACE INTO C4N11A (dataset) VALUES ('20110503');
REPLACE INTO C4N11A (dataset) VALUES ('20110504');
REPLACE INTO C4N11A (dataset) VALUES ('20110506');
REPLACE INTO C4N11A (dataset) VALUES ('20110510');
REPLACE INTO C4N11A (dataset) VALUES ('20110514');
REPLACE INTO C4N11A (dataset) VALUES ('20110516');
REPLACE INTO C4N11A (dataset) VALUES ('20110712');
REPLACE INTO C4N11A (dataset) VALUES ('20110713');
REPLACE INTO C4N11A (dataset) VALUES ('20110717');
REPLACE INTO C4N11A (dataset) VALUES ('20110719');
REPLACE INTO C4N11A (dataset) VALUES ('20110720');
REPLACE INTO C4N11A (dataset) VALUES ('20110721');
REPLACE INTO C4N11A (dataset) VALUES ('20110724');
REPLACE INTO C4N11A (dataset) VALUES ('20110728');

REPLACE INTO C4N11AD VALUES ('20110222', '20110222', '20110222',
  '"start_date" between timestamp \'2011-02-22\' and \'2011-02-22\'');
REPLACE INTO C4N11AD VALUES ('20110223', '20110223', '20110226',
  '"start_date" between timestamp \'2011-02-23\' and \'2011-02-26\'');
REPLACE INTO C4N11AD VALUES ('20110227', '20110227', '20110302',
  '"start_date" between timestamp \'2011-02-27\' and \'2011-03-02\'');
REPLACE INTO C4N11AD VALUES ('20110303', '20110303', '20110303',
  '"start_date" between timestamp \'2011-03-03\' and \'2011-03-03\'');
REPLACE INTO C4N11AD VALUES ('20110304', '20110304', '20110307',
  '"start_date" between timestamp \'2011-03-04\' and \'2011-03-07\'');
REPLACE INTO C4N11AD VALUES ('20110308', '20110308', '20110309',
  '"start_date" between timestamp \'2011-03-08\' and \'2011-03-09\'');
REPLACE INTO C4N11AD VALUES ('20110310', '20110310', '20110313',
  '"start_date" between timestamp \'2011-03-10\' and \'2011-03-13\'');
REPLACE INTO C4N11AD VALUES ('20110314', '20110314', '20110317',
  '"start_date" between timestamp \'2011-03-14\' and \'2011-03-17\'');
REPLACE INTO C4N11AD VALUES ('20110318', '20110318', '20110319',
  '"start_date" between timestamp \'2011-03-18\' and \'2011-03-19\'');
REPLACE INTO C4N11AD VALUES ('20110320', '20110320', '20110320',
  '"start_date" between timestamp \'2011-03-20\' and \'2011-03-20\'');
REPLACE INTO C4N11AD VALUES ('20110410', '20110410', '20110410',
  '"start_date" between timestamp \'2011-04-10\' and \'2011-04-10\'');
REPLACE INTO C4N11AD VALUES ('20110411', '20110411', '20110412',
  '"start_date" between timestamp \'2011-04-11\' and \'2011-04-12\'');
REPLACE INTO C4N11AD VALUES ('20110413', '20110413', '20110415',
  '"start_date" between timestamp \'2011-04-13\' and \'2011-04-15\'');
REPLACE INTO C4N11AD VALUES ('20110416', '20110416', '20110417',
  '"start_date" between timestamp \'2011-04-16\' and \'2011-04-17\'');
REPLACE INTO C4N11AD VALUES ('20110418', '20110418', '20110420',
  '"start_date" between timestamp \'2011-04-18\' and \'2011-04-20\'');
REPLACE INTO C4N11AD VALUES ('20110421', '20110421', '20110424',
  '"start_date" between timestamp \'2011-04-21\' and \'2011-04-24\'');
REPLACE INTO C4N11AD VALUES ('20110425', '20110425', '20110427',
  '"start_date" between timestamp \'2011-04-25\' and \'2011-04-27\'');
REPLACE INTO C4N11AD VALUES ('20110503', '20110503', '20110503',
  '"start_date" between timestamp \'2011-05-03\' and \'2011-05-03\'');
REPLACE INTO C4N11AD VALUES ('20110504', '20110504', '20110505',
  '"start_date" between timestamp \'2011-05-04\' and \'2011-05-05\'');
REPLACE INTO C4N11AD VALUES ('20110506', '20110506', '20110509',
  '"start_date" between timestamp \'2011-05-06\' and \'2011-05-09\'');
REPLACE INTO C4N11AD VALUES ('20110510', '20110510', '20110513',
  '"start_date" between timestamp \'2011-05-10\' and \'2011-05-13\'');
REPLACE INTO C4N11AD VALUES ('20110514', '20110514', '20110515',
  '"start_date" between timestamp \'2011-05-14\' and \'2011-05-15\'');
REPLACE INTO C4N11AD VALUES ('20110516', '20110516', '20110518',
  '"start_date" between timestamp \'2011-05-16\' and \'2011-05-18\'');
REPLACE INTO C4N11AD VALUES ('20110712', '20110712', '20110712',
  '"start_date" between timestamp \'2011-07-12\' and \'2011-07-12\'');
REPLACE INTO C4N11AD VALUES ('20110713', '20110713', '20110716',
  '"start_date" between timestamp \'2011-07-13\' and \'2011-07-16\'');
REPLACE INTO C4N11AD VALUES ('20110717', '20110717', '20110718',
  '"start_date" between timestamp \'2011-07-17\' and \'2011-07-18\'');
REPLACE INTO C4N11AD VALUES ('20110719', '20110719', '20110719',
  '"start_date" between timestamp \'2011-07-19\' and \'2011-07-19\'');
REPLACE INTO C4N11AD VALUES ('20110720', '20110720', '20110720',
  '"start_date" between timestamp \'2011-07-20\' and \'2011-07-20\'');
REPLACE INTO C4N11AD VALUES ('20110721', '20110721', '20110723',
  '"start_date" between timestamp \'2011-07-21\' and \'2011-07-23\'');
REPLACE INTO C4N11AD VALUES ('20110724', '20110724', '20110727',
  '"start_date" between timestamp \'2011-07-24\' and \'2011-07-27\'');
REPLACE INTO C4N11AD VALUES ('20110728', '20110728', '20110731',
  '"start_date" between timestamp \'2011-07-28\' and \'2011-07-31\'');
