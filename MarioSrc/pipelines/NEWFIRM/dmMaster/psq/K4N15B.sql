REPLACE INTO PSQ VALUES('K4N15B', 'K4N15B', 'K4N15BD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N15B (dataset) VALUES ('20150929');
REPLACE INTO K4N15B (dataset) VALUES ('20151001');
REPLACE INTO K4N15B (dataset) VALUES ('20151015');
REPLACE INTO K4N15B (dataset) VALUES ('20151023');
REPLACE INTO K4N15B (dataset) VALUES ('20151030');
REPLACE INTO K4N15B (dataset) VALUES ('20151106');
REPLACE INTO K4N15B (dataset) VALUES ('20151110');
REPLACE INTO K4N15B (dataset) VALUES ('20151116');
REPLACE INTO K4N15B (dataset) VALUES ('20151124');
REPLACE INTO K4N15B (dataset) VALUES ('20151125');
REPLACE INTO K4N15B (dataset) VALUES ('20151201');
REPLACE INTO K4N15B (dataset) VALUES ('20151207');

REPLACE INTO K4N15BD VALUES ('20150929', '20150929', '20150930',
  '"start_date" between timestamp \'2015-09-29\' and timestamp \'2015-09-30\'');
REPLACE INTO K4N15BD VALUES ('20151001', '20151001', '20151004',
  '"start_date" between timestamp \'2015-10-01\' and timestamp \'2015-10-04\'');
REPLACE INTO K4N15BD VALUES ('20151015', '20151015', '20151019',
  '"start_date" between timestamp \'2015-10-15\' and timestamp \'2015-10-19\'');
REPLACE INTO K4N15BD VALUES ('20151023', '20151023', '20151025',
  '"start_date" between timestamp \'2015-10-23\' and timestamp \'2015-10-25\'');
REPLACE INTO K4N15BD VALUES ('20151030', '20151030', '20151105',
  '"start_date" between timestamp \'2015-10-30\' and timestamp \'2015-11-05\'');
REPLACE INTO K4N15BD VALUES ('20151106', '20151106', '20151109',
  '"start_date" between timestamp \'2015-11-06\' and timestamp \'2015-11-09\'');
REPLACE INTO K4N15BD VALUES ('20151110', '20151110', '20151115',
  '"start_date" between timestamp \'2015-11-10\' and timestamp \'2015-11-15\'');
REPLACE INTO K4N15BD VALUES ('20151116', '20151116', '20151117',
  '"start_date" between timestamp \'2015-11-16\' and timestamp \'2015-11-17\'');
REPLACE INTO K4N15BD VALUES ('20151124', '20151124', '20151124',
  '"start_date" between timestamp \'2015-11-24\' and timestamp \'2015-11-24\'');
REPLACE INTO K4N15BD VALUES ('20151125', '20151125', '20151130',
  '"start_date" between timestamp \'2015-11-25\' and timestamp \'2015-11-30\'');
REPLACE INTO K4N15BD VALUES ('20151201', '20151201', '20151206',
  '"start_date" between timestamp \'2015-12-01\' and timestamp \'2015-12-06\'');
REPLACE INTO K4N15BD VALUES ('20151207', '20151207', '20151210',
  '"start_date" between timestamp \'2015-12-07\' and timestamp \'2015-12-10\'');
