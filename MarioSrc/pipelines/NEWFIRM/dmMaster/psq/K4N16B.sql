REPLACE INTO PSQ VALUES('K4N16B', 'K4N16B', 'K4N16BD', 'NEWFIRM',
'dir', 'disabled', 'instrument=''newfirm''');

REPLACE INTO K4N16B (dataset) VALUES ('20161005');
REPLACE INTO K4N16B (dataset) VALUES ('20161010');
REPLACE INTO K4N16B (dataset) VALUES ('20161017');
REPLACE INTO K4N16B (dataset) VALUES ('20161023');
REPLACE INTO K4N16B (dataset) VALUES ('20161029');
REPLACE INTO K4N16B (dataset) VALUES ('20161104');
REPLACE INTO K4N16B (dataset) VALUES ('20161111');
REPLACE INTO K4N16B (dataset) VALUES ('20161114');
REPLACE INTO K4N16B (dataset) VALUES ('20161117');
REPLACE INTO K4N16B (dataset) VALUES ('20170109');
REPLACE INTO K4N16B (dataset) VALUES ('20170112');

REPLACE INTO K4N16BD VALUES ('20161005', '20161005', '20161009',
  '"start_date" between timestamp \'2016-10-05\' and timestamp \'2016-10-09\'');
REPLACE INTO K4N16BD VALUES ('20161010', '20161010', '20161013',
  '"start_date" between timestamp \'2016-10-10\' and timestamp \'2016-10-13\'');
REPLACE INTO K4N16BD VALUES ('20161017', '20161017', '20161022',
  '"start_date" between timestamp \'2016-10-17\' and timestamp \'2016-10-22\'');
REPLACE INTO K4N16BD VALUES ('20161023', '20161023', '20161028',
  '"start_date" between timestamp \'2016-10-23\' and timestamp \'2016-10-28\'');
REPLACE INTO K4N16BD VALUES ('20161029', '20161029', '20161103',
  '"start_date" between timestamp \'2016-10-29\' and timestamp \'2016-11-03\'');
REPLACE INTO K4N16BD VALUES ('20161104', '20161104', '20161108',
  '"start_date" between timestamp \'2016-11-04\' and timestamp \'2016-11-08\'');
REPLACE INTO K4N16BD VALUES ('20161111', '20161111', '20161113',
  '"start_date" between timestamp \'2016-11-11\' and timestamp \'2016-11-13\'');
REPLACE INTO K4N16BD VALUES ('20161114', '20161114', '20161116',
  '"start_date" between timestamp \'2016-11-14\' and timestamp \'2016-11-16\'');
REPLACE INTO K4N16BD VALUES ('20161117', '20161117', '20161122',
  '"start_date" between timestamp \'2016-11-17\' and timestamp \'2016-11-22\'');
REPLACE INTO K4N16BD VALUES ('20170109', '20170109', '20170111',
  '"start_date" between timestamp \'2017-01-09\' and timestamp \'2017-01-11\'');
REPLACE INTO K4N16BD VALUES ('20170112', '20170112', '20170116',
  '"start_date" between timestamp \'2017-01-12\' and timestamp \'2017-01-16\'');
