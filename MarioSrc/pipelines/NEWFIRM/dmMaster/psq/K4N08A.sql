REPLACE INTO PSQ VALUES('K4N08A', 'K4N08A', 'K4N08AD', 'NEWFIRM',
'dir', 'disabled', 'instrument=\'newfirm\'');

REPLACE INTO K4N08A (dataset) VALUES ('20080212');
REPLACE INTO K4N08A (dataset) VALUES ('20080214');
REPLACE INTO K4N08A (dataset) VALUES ('20080218');
REPLACE INTO K4N08A (dataset) VALUES ('20080222');
REPLACE INTO K4N08A (dataset) VALUES ('20080226');
REPLACE INTO K4N08A (dataset) VALUES ('20080228');
REPLACE INTO K4N08A (dataset) VALUES ('20080303');
REPLACE INTO K4N08A (dataset) VALUES ('20080307');
REPLACE INTO K4N08A (dataset) VALUES ('20080311');
REPLACE INTO K4N08A (dataset) VALUES ('20080314');
REPLACE INTO K4N08A (dataset) VALUES ('20080318');
REPLACE INTO K4N08A (dataset) VALUES ('20080320');
REPLACE INTO K4N08A (dataset) VALUES ('20080324');
REPLACE INTO K4N08A (dataset) VALUES ('20080328');
REPLACE INTO K4N08A (dataset) VALUES ('20080401');
REPLACE INTO K4N08A (dataset) VALUES ('20080405');
REPLACE INTO K4N08A (dataset) VALUES ('20080409');
REPLACE INTO K4N08A (dataset) VALUES ('20080413');
REPLACE INTO K4N08A (dataset) VALUES ('20080417');
REPLACE INTO K4N08A (dataset) VALUES ('20080421');
REPLACE INTO K4N08A (dataset) VALUES ('20080509');
REPLACE INTO K4N08A (dataset) VALUES ('20080511');
REPLACE INTO K4N08A (dataset) VALUES ('20080513');
REPLACE INTO K4N08A (dataset) VALUES ('20080516');
REPLACE INTO K4N08A (dataset) VALUES ('20080519');
REPLACE INTO K4N08A (dataset) VALUES ('20080523');
REPLACE INTO K4N08A (dataset) VALUES ('20080527');

REPLACE INTO K4N08AD VALUES ('20080212', '20080212', '20080213',
  '"start_date" between timestamp \'2008-02-12\' and timestamp \'2008-02-13\'');
REPLACE INTO K4N08AD VALUES ('20080214', '20080214', '20080217',
  '"start_date" between timestamp \'2008-02-14\' and timestamp \'2008-02-17\'');
REPLACE INTO K4N08AD VALUES ('20080218', '20080218', '20080221',
  '"start_date" between timestamp \'2008-02-18\' and timestamp \'2008-02-21\'');
REPLACE INTO K4N08AD VALUES ('20080222', '20080222', '20080225',
  '"start_date" between timestamp \'2008-02-22\' and timestamp \'2008-02-25\'');
REPLACE INTO K4N08AD VALUES ('20080226', '20080226', '20080227',
  '"start_date" between timestamp \'2008-02-26\' and timestamp \'2008-02-27\'');
REPLACE INTO K4N08AD VALUES ('20080228', '20080228', '20080302',
  '"start_date" between timestamp \'2008-02-28\' and timestamp \'2008-03-02\'');
REPLACE INTO K4N08AD VALUES ('20080303', '20080303', '20080306',
  '"start_date" between timestamp \'2008-03-03\' and timestamp \'2008-03-06\'');
REPLACE INTO K4N08AD VALUES ('20080307', '20080307', '20080310',
  '"start_date" between timestamp \'2008-03-07\' and timestamp \'2008-03-10\'');
REPLACE INTO K4N08AD VALUES ('20080311', '20080311', '20080313',
  '"start_date" between timestamp \'2008-03-11\' and timestamp \'2008-03-13\'');
REPLACE INTO K4N08AD VALUES ('20080314', '20080314', '20080317',
  '"start_date" between timestamp \'2008-03-14\' and timestamp \'2008-03-17\'');
REPLACE INTO K4N08AD VALUES ('20080318', '20080318', '20080319',
  '"start_date" between timestamp \'2008-03-18\' and timestamp \'2008-03-19\'');
REPLACE INTO K4N08AD VALUES ('20080320', '20080320', '20080323',
  '"start_date" between timestamp \'2008-03-20\' and timestamp \'2008-03-23\'');
REPLACE INTO K4N08AD VALUES ('20080324', '20080324', '20080327',
  '"start_date" between timestamp \'2008-03-24\' and timestamp \'2008-03-27\'');
REPLACE INTO K4N08AD VALUES ('20080328', '20080328', '20080331',
  '"start_date" between timestamp \'2008-03-28\' and timestamp \'2008-03-31\'');
REPLACE INTO K4N08AD VALUES ('20080401', '20080401', '20080404',
  '"start_date" between timestamp \'2008-04-01\' and timestamp \'2008-04-04\'');
REPLACE INTO K4N08AD VALUES ('20080405', '20080405', '20080408',
  '"start_date" between timestamp \'2008-04-05\' and timestamp \'2008-04-08\'');
REPLACE INTO K4N08AD VALUES ('20080409', '20080409', '20080412',
  '"start_date" between timestamp \'2008-04-09\' and timestamp \'2008-04-12\'');
REPLACE INTO K4N08AD VALUES ('20080413', '20080413', '20080416',
  '"start_date" between timestamp \'2008-04-13\' and timestamp \'2008-04-16\'');
REPLACE INTO K4N08AD VALUES ('20080417', '20080417', '20080420',
  '"start_date" between timestamp \'2008-04-17\' and timestamp \'2008-04-20\'');
REPLACE INTO K4N08AD VALUES ('20080421', '20080421', '20080423',
  '"start_date" between timestamp \'2008-04-21\' and timestamp \'2008-04-23\'');
REPLACE INTO K4N08AD VALUES ('20080509', '20080509', '20080510',
  '"start_date" between timestamp \'2008-05-09\' and timestamp \'2008-05-10\'');
REPLACE INTO K4N08AD VALUES ('20080511', '20080511', '20080512',
  '"start_date" between timestamp \'2008-05-11\' and timestamp \'2008-05-12\'');
REPLACE INTO K4N08AD VALUES ('20080513', '20080513', '20080515',
  '"start_date" between timestamp \'2008-05-13\' and timestamp \'2008-05-15\'');
REPLACE INTO K4N08AD VALUES ('20080516', '20080516', '20080518',
  '"start_date" between timestamp \'2008-05-16\' and timestamp \'2008-05-18\'');
REPLACE INTO K4N08AD VALUES ('20080519', '20080519', '20080522',
  '"start_date" between timestamp \'2008-05-19\' and timestamp \'2008-05-22\'');
REPLACE INTO K4N08AD VALUES ('20080523', '20080523', '20080526',
  '"start_date" between timestamp \'2008-05-23\' and timestamp \'2008-05-26\'');
REPLACE INTO K4N08AD VALUES ('20080527', '20080527', '20080527',
  '"start_date" between timestamp \'2008-05-27\' and timestamp \'2008-05-27\'');
