-- 
-- catalog.sql
-- 
-- SQL script to create the catalog table
-- in the DataManagerDB database.
-- 
-- WARNING: the script simply DROPs any 
-- previous table called catalog without
-- warning.
--

-- USE DataManagerDB;

DROP TABLE catalog;
CREATE TABLE catalog (
    CLASS       VARCHAR(9) NOT NULL, 
    MJDSTART    FLOAT NULL, 
    MJDEND      FLOAT NULL, 
    DETECTOR    VARCHAR(71) NULL, 
    IMAGEID     VARCHAR(71) NULL, 
    FILTER      VARCHAR(71) NULL, 
    EXPTIME     FLOAT NULL, 
    NCOMBINE	INT NULL,
    QUALITY	FLOAT NULL,
    MATCH	VARCHAR(512) NULL,
    DATATYPE    VARCHAR(9) NULL, 
    VALUE       VARCHAR(127) NULL
);

DROP INDEX MJDIndx;
CREATE INDEX MJDIndx ON catalog (MJDSTART);

DROP INDEX ImageIdIndx;
CREATE INDEX ImageIdIndx ON catalog (IMAGEID);
