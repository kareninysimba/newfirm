#!/bin/env pipecl
#
# SGCOGCSTART

int	status = 1
string	datadir, dataset, indir, ilist, lfile
string  temp1, temp2, listoflists
struct  *list2

# Tasks and packages.
images
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set the dataset name and uparm.
names( "sgc", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".sgc"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSGCOGCSTART (%s):\n", dataset )

# Go to the relevant directory
cd( datadir )

# Create the list of lists.
listoflists = indir//dataset//"ogc.lol"
list = ilist
while ( fscan( list, s1 ) != EOF ) {
    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    s2 = substr( s2, 1, stridx("_",s2)-1 ) //
         substr( s2, stridx("_",s2)+1, 999 )
    print( s1, >> s2//".tmp" )
    print( s2//".tmp", >> "sgcogcstart1.tmp" )
}
list = ""
pathnames( "@sgcogcstart1.tmp", > listoflists )

# Call the OGC pipelines 
temp1 = "sgcogcstart2.tmp"
temp2 = "sgcogcstart3.tmp"
callpipe( "sgc", "ogc", "split", listoflists, temp1, temp2 )
concat( temp2 ) | scan( status )
concat( temp1 )
printf( "Callpipe returned %d\n", status )

delete( "sgcogcstart?.tmp" )

logout( status )
