#!/bin/env pipecl
#
# SGCPROC

# The saturation keyword as put in the header by the KTM is
# set as follows (from F. Valdes' 3/19/08 email):
# exptime = ncoadds * exptime for one readout,
# saturation = nfowler * ncoadds * saturation for one readout

int	status = 1
int	namps, linerr, dx, dy, npix
real	exptime, rdnoise, gain, fowler, saturate, mjd, dqfracok, coadds
real	lincoeff
string	dark, datadir, dataset, flat, ilist, obpm, s4, maskname
string	image, indir, lfile, trimsec, obstype, shortname, sateq

file    caldir = "MC$"

# Tasks and packages
images
imutil
servers
proto
task $linearize = "$linearize_db.cl $1"

# Set file and path names.
sgcnames( envget("OSF_DATASET") )
dataset = sgcnames.dataset

# Set filenames.
indir = sgcnames.indir
datadir = sgcnames.datadir
ilist = indir // dataset // ".sgc"
lfile = datadir // sgcnames.lfile
set (uparm = sgcnames.uparm)
set (pipedata = sgcnames.pipedata)

# Log start of processing.
printf( "\nSGCPROC (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Make sure no persistence masking is done if not in sciencepipeline
if ( sciencepipeline == NO )
    do_persistence = NO
;

list = ilist
for (namps=1; fscan(list,s1)!=EOF; namps+=1) {

    # Get the part of the filename after the last /, stripping the .fits
    shortname = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    # Set the sgc variables for this image
    sgcnames( shortname )

    # Retrieve the trim section and the obstype from the header
    hsel( s1, "TRIMSEC, obstype", yes ) | scan( trimsec, obstype )
    if ( nscan()!=2 ) {
        sendmsg( "ERROR", "Could not retrieve trimsec and obstype",
	    shortname, "VRFY" )
        status = 0
        break
    }
    ;

    # Set the name of the image
    image = sgcnames.image
    # Copy the trimsec only, i.e., remove the reference pixels
    imcopy( s1//trimsec, image )
    # Delete the section keywords
    hedit (image, "detsec,datasec,trimsec,biassec", del+, verify-, show-)

    # Get the exposure time, Fowler sampling, and number off
    # coadds from the header
    hselect( image, "exptime,fsample,ncoadd", yes ) |
        scan( exptime, fowler, coadds )
    # Read the original bad pixel mask from the header
    hsel( image, "bpm", yes ) | scan( obpm )

    # TODO: once move from sequence-based to exposure based is
    # implemented, these getcals need to be moved to sgcsetup.
    getcal( image, "lincoeff", cm, caldir, 
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="", mjd="!mjd-obs")
    if (getcal.statcode>0) { # Getcal was not successful
        sendmsg( "ERROR",
            "Getcal failed - no linearity coefficient retrieved",
            s1, "CAL" )
        status = 0
        break
    } else {
        # Get the linearity coefficient from the header
        lincoeff = INDEF
        hsel( image, "lincoeff", yes ) | scan( lincoeff )
    }
    getcal( image, "saturate", cm, caldir, 
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="", mjd="!mjd-obs")
    if ( getcal.statcode == 0 ) {
        getcal( image, "sateq", cm, caldir, 
            obstype="", detector="!instrume", imageid="!detector",
            filter="", exptime="", mjd="!mjd-obs")
    }
    ;
    if (getcal.statcode>0) { # Getcal was not successful
        sendmsg( "ERROR",
            "Getcal failed - no saturation equation retrieved",
            s1, "CAL" )
        status = 0
        break
    } else { # Getcal was successful
        sateq = ""
        hsel( image, "sateq", yes ) | scan( sateq )
	if ( sateq == "" ) {
            sendmsg( "ERROR",
                "No sateq in header - no saturation equation retrieved",
                s1, "CAL" )
            status = 0
            break
        } else {
            # Create a cumulative mask of saturated pixels and original bpm
            flpr
            imexpr( sateq, "cummask1.pl", obpm, image, lincoeff,
                exprdb="pipedata/saturation.db" )
#            imcopy( obpm, "cummask1.pl" )
        }
    }

    if (do_persistence) {

        # Read keywords needed needed for the getcal
        hsel( image, "detector,obsid,mjd-obs", yes ) | scan( s2, s3, mjd )
        # Retrieve the location of the cumulative mask
        getcal( image, "masks", cm, caldir, imageid=s2, mjd=mjd,
            match="%"//s3, obstype="", detector="", filter="" )
        if (getcal.statcode>0) {
            sendmsg( "ERROR", "Getcal failed with message: "//getcal.statstr,
	        shortname, "PROC" )
            status = 0
            break
        }

        # Merge the saturated/bad pixel mask with the persistence mask
        mskexpr( "(i>0)?i:m", "cummask2.pl", "cummask1.pl",
            refmasks=getcal.value )
# These are useful for debugging perposes
#imcopy( "cummask1.pl", image//"cm1.pl" )
#imcopy( "cummask2.pl", image//"cm2.pl" )
#imcopy( getcal.value, image//"gc.pl" )

    } else
        imcopy( "cummask1.pl", "cummask2.pl" )
    ;

    # Clean up intermediate results
    imdel( "cummask1.pl" )

    # Interpolate over all bad pixels to produce cosmetically nice
    # images. This is done before updating the cumulative mask so 
    # that the local mask file is read now.
#    fixpix( image, "cummask2.pl" , verbose+)

    if (do_persistence) {

        # Store the updated cumulative mask in the same location 
        imdel( getcal.value )
        imcopy( "cummask2.pl", getcal.value )

    } else {

        # Create the name for the cumulative pixel mask. Note that
        # "sat" is appended rather than "per" to indicate that
        # this mask does not include persistence flags.
        s2 = shortname // "sat.pl"
        # Copy the temporary file to its final name
        imcopy( "cummask2.pl", s2 )
	# Get the full location hostname and path
        pathname( s2 ) | scan( s3 )
        # Because the persistence pipeline has not been run, the
        # cumulative mask has not yet been stored in the calibration
        # manager. This is done here.
	hsel( s1, "obsid,detector", yes ) | scan( s2, s4 )
	putcal( "", "keyword", cm, value=s3, class="masks",
	    imageid=s4, match=sgcnames.parent//" "//s2,
            detector="", filter="", exptime="" )
    }

    # Reject this dataset from further processing if the mask
    # is mostly filled.
    # First get the number of pixels in the image
    hselect( image, "i_naxis[1],i_naxis[2]", yes ) | scan( dx, dy )
    npix = dx*dy
    # Now get the number of unmasked pixels
    i = 0
    mimstat( image, imasks="cummask2.pl", fields="npix", format- ) | scan( i )
    # Determine the fraction of unmasked pixels
    dqfracok = (1.*i)/(1.*npix)
    # Reject the image if the fraction of ok pixels is too small
    #if ( dqfracok < 0.25 ) {
    if ( dqfracok < 0.05 ) {
	sendmsg( "ERROR", "Mask fraction too low", str(dqfracok), "PROC")
        imdel( image )
    } else {
        # Image is ok, so continue processing

        # Retrieve the location of the dark from the header, if available
        dark = "" ; hselect( s1, "dark", yes ) | scan( dark )
        if (dark == "") {
    	    if (sgc_nodarkcont) { # value set in NEWFIRM.cl
    	        # No dark available, OK to continue, but send warning
    	        sendmsg( "WARNING", "No dark available, data not dark subtracted",
    	            "", "PROC" )
    	    } else {
    	        # No dark available, not OK to continue, so throw error
    	        sendmsg( "ERROR", "No dark available, data not dark subtracted",
    	            "", "PROC" )
    	        status = 0
    	    }
        } else {
            # Dark available, subtract it
            imarith( image, "-", dark, image )
        }

        linerr = 0
        iferr {
            # Note that linearize scales the image to ncoadds=1, Fowler 
            # sampling of 1, and to exptime=1s.
            linearize( image )
        } then {
            linerr = 1
        } else
            ;

        if (linerr==1) {
            if (sgc_nolincont) {
                # No nonlinearity correction available, but OK to continue
    	        sendmsg( "WARNING", 
                    "No nonlinearity correction available, data not linearized",
    	            "", "PROC" )
                # Scale the image to ncoadds=1, Fowler sampling of 1, and
                # exptime=1s
                x = exptime*fowler*coadds
                imarith( image, "/", x, image )
    	    } else {
                # No nonlinearity correction available, not OK to continue
    	        sendmsg( "ERROR", 
                    "No nonlinearity correction available, data not linearized",
    	            "", "PROC" )
                status = 0
            }
        }
    
        # Retrieve the location of the flat field from the header, if available
        flat = "" ; hselect( s1, "flat", yes ) | scan( flat )
        if (flat == "") {
    	    if (sgc_noflatcont) { # value set in NEWFIRM.cl
    	        # No dome flat available, OK to continue, so send warning
    	        sendmsg( "WARNING", "No dome flat available, data not flatfielded",
    	            "", "PROC" )
    	    } else {
    	        # No dome flat available, not OK to continue, so throw error
    	        sendmsg( "ERROR", "No dome flat available, data not flatfielded",
    	            "", "PROC" )
    	        status = 0
    	    }
        } else {
            # Dome flat available, so divide data by flat
            imarith( image, "/", flat, image )
        }
    
        # Store the original exposure time
        hedit( image, "oexptime", exptime, add+, ver- )
        # Set the exposure time to 1 s
        hedit( image, "exptime", 1, update+, ver- )
    
        # Store the original Fowler sampling
        hedit( image, "ofsample", '(fsample)', add+, ver- )
        # Scale the image by the Fowler sampling (i.e., normalize to fowler=1)
        imarith( image, "/", fowler, image )
        # Set the Fowler sampling to 1
        hedit( image, "fsample", 1, update+, ver- )

        # Retrieve saturate keyword from the header. Note that this 
        # value has been put in by a getcal, meaning that the original
        # value put in by the KTM has been overwritten. The value from
        # the KTM was scaled up by ncoadds and nfowler. Saturate
        # as retrieved by getcal is for ncoadds=1 and nfowler=1.
        # Saturate still needs to be scaled down by exptime. It
        # is important to realize that this value of saturate is not
        # a true threshold. To find out which pixels are saturated,
        # the saturation.db imexpression should be used.
        hselect( image, "saturate", yes ) | scan( saturate )
        # Store the modified saturation value
        saturate = saturate/(exptime)
        hedit( image, "saturate", saturate, update+, ver- )
    
    }

    # Clean up the temporary mask
    imdel( "cummask2.pl" )

}

logout( status )
