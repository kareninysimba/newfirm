#!/bin/env pipecl
#
# Group by filter

int     ljd
string  dataset, indir, datadir, listoflists
string  inlist, lfile, zlist, flist, olist, fname, sname, instrument
struct  *fd, filter

# Define packages and tasks.
util
proto
images
noao
servers
mscred
task    ccdproc         = mscsrc$ccdproc.cl
task    _ccdlist        = mscsrc$x_ccdred.e
cache   mscred observatory

# Set filenames.
names ("fil", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".fil"
listoflists = indir // dataset // ".lol"
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
mscred.ssfile = "pipedata$subsets.dat"
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nFILGROUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# This block should not be needed after development of the code
if ( access(listoflists) )
    delete( listoflists )
;

# Append "[0]" to all files in the list.
list = inlist
while (fscan( list, fname) != EOF)
    print (fname//"[0]", >> "filgroup1.tmp")

# Get the instrument and  filters for the dependent types.
hselect ( "@filgroup1.tmp", "$I,INSTRUME,FILTER", yes) | sort (col=3, >> "filgroup2.tmp")

# Separate into filter lists.
s2 = ""
list = "filgroup2.tmp"
while (fscan (list, fname, instrument, filter) != EOF) {
    # Setup for new filter.
    if (filter != s2) {
        s2 = filter; sname = ""

        # Get the filter abbreviation.
        match (filter, mscred.ssfile) | fields ("STDIN", "2") | scan (sname)
        if (sname == "") {
            sendmsg ("ERROR", "Filter not in subset file", filter, "VRFY")
            next
        }
        ;

        # Add to output filter list.
        print (datadir // dataset // "_" // sname // ".lst", >> listoflists)
    }
    ;
    if (sname == "")
        next
    ;

    # Add to filter file.
    fname = substr (fname, 1, stridx("[",fname)-1)
    printf ("%s -> %s\n", fname, dataset//"_"//sname//".lst", >> lfile)
    print (fname, >> dataset//"_"//sname//".lst")
}
list = ""

if (access(listoflists))
    ;
else {
    sendmsg( "ERROR", "No filter names recognized", dataset, "VRFY" )
    logout 0
}

# Loop over all the files in the listoflists and sort them
list = listoflists
while ( fscan( list, s1 ) != EOF ) {
    sort( s1, col=0, >> "filgroup3.tmp" )
    delete( s1 )
    rename( "filgroup3.tmp", s1 )
}
list = ""

if (access(listoflists)) {
    sort (listoflists) | uniq (> "filgroup3.tmp")
    rename ("filgroup3.tmp", listoflists)
}
;

delete( "filgroup?.tmp" )

logout 1
