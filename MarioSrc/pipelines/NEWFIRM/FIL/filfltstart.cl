#!/bin/env pipecl

int	status
string	datadir, dataset, extname
string	ilist, image, indir, lfile, listoflists, olist, temp1, temp2

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set filenames.
names ("fil", envget ("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
indir   = names.indir
lfile   = datadir // names.lfile
listoflists = indir // dataset // ".lol"

cd (datadir)

# Log start of processing.
printf ("\nFILGROUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

type (listoflists)

# Call the SWC pipelines 
temp1 = "filflt1.tmp"
temp2 = "filflt2.tmp"
callpipe ("fil", "flt", "split", listoflists, temp1, temp2)
concat (temp2) | scan (status)
concat (temp1)
printf ("Callpipe returned %d\n", status)

if (access (listoflists) == YES)
    delete (listoflists)
;

logout( status )
