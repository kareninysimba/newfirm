#!/bin/env pipecl
#
# STKSTACK -- Stack exposures with weighting and rejection.
#
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful: Stack was not created, even though accepted SIFs found
#   2 = Successful?: No acceptable SIFs found; stack not created.  This is 
#       not necessarily "unsuccessful" since it is possible that 
#       all SIFs are legitimately rejected - see stkselect.
#   3 = Successful?: One acceptable SIF found; stack created.

# Declare variables.
string  dataset, datadir, lfile, indir, ilist, rlist, slist, ofile
string  method, wkey, msg, bpmfile, expmap
string	shortname
int	ncombine,ict

# Load the packages.
images
lists
proto
noao
artdata
imred

# Set the dataset name and uparm.
names( "stk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
shortname = names.shortname
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".stk"
rlist = "stkrlist.tmp"
slist = "stkslist.tmp"
ofile = shortname //"_stk"
bpmfile = shortname//"_stk_bpm"
expmap = shortname//"_stk_expmap"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSTKSTACK (%s): ", dataset ) | tee (lfile)
time | tee (lfile)

# Go to the relevant directory
cd( datadir )

# Create slist, a sorted list of images to be stacked.
if ( access( slist ) ) delete( slist, verify- )
;
touch( slist )
hselect( "@"//rlist, "$I,REJECT,STKSCALE", yes, > "stkstack1.tmp" )
list = "stkstack1.tmp"
while ( fscan( list, s1, s2, x ) != EOF ) {
    s3 = substr( s2, 1, 1 )
    if ( s3 != "N" )
        next
    ;
    printf( "%f %s\n", x, s1, >> slist )
}
list = ""
sort( slist, numeric+ ) | fields ( "STDIN", "2", > slist )
count( slist ) | scan( ncombine )

# Check that the number of SIFs to combine is not 0. 
# If it is, then send message to monitor and logout with
# status of 2.
if ( ncombine == 0 ) {
    sendmsg( "WARNING", "No SIFs to stack, based on REJECT keyword.",
        dataset, "PROC" )
    logout 2
}
;

# Parse stk_weight to determine which weighting keyword to use.
# TODO: this should be based on a rule.
j = stridx( ",", stk_weight )
if (j != 0) {
    method = substr( stk_weight, 1, j-1 )
} else {
    method = stk_weight
}
wkey = ""
if ( (method == "total") || (method =="default") ) {
    wkey="WTOT"
} else if ( method == "see" ) {
    wkey = "WSEEING"
} else if ( method == "transns" ) {
    wkey = "WTRNSNS"
}
;
if ( wkey == "" ) {
    wkey = "OEXPTIME"
    method = "none"
    sendmsg( "WARNING", "stk_weight not recognized, setting it to none.",
        dataset, "PROC" )
}
;
    
if (ncombine==1) {

    # Only one exposure in the list, so copy the input data, and create
    # an exposure map.
    imcopy( "@"//slist, ofile )
    hsel( "@"//slist, "BPM", yes ) | scan( s1 )
    imcopy( s1, bpmfile//".pl" )
    hselect( ofile, "OEXPTIME", yes ) | scan(x)
    imcopy( ofile, ofile//"expmap.pl" )
    imreplace( ofile//"expmap.pl", x, lower=INDEF, upper=INDEF )
        
} else {

    # Combine the exposures. Although it is expected that the 
    # exposure times are the same for all exposures in a sequence,
    # the imcombine below is set up such that this is not a
    # requirement, i.e., the exposure times may be different.
    # 
    # If the images are not scaled to exptime=1, one would
    # expect that imcombine would give a straight sum (or average)
    # of all the contributing exposures (at least in the skynoise
    # dominated regime). In the pipeline, the images are scaled
    # back to exposure time of 1s. 
    #
    # To combine the images properly, one could first scale the
    # image back to the original exposure time (with the scale= 
    # keyword). However, when using the scale keyword, the output
    # in the combined image is also scaled (by the average over all
    # images of the scale factor with respect the first image).
    # It can be complicated to calculate this scale factor 
    # because the number of images contributing to each pixel
    # in the output of combine can vary, 
    # 
    # Fortunately, the images can also be scaled properly by using
    # weights, and that is done here.
    # 
    # The scale keyword is also used to photometrically scale
    # the exposures before combining them.
    
    # Run imcombine.  The imcombine will use the masks defined in the
    # BPM keyword to identify bad pixels. For the combine option,
    # lmedian is used, which, in the case of only two contributing
    # datasets provides the lowest value of the two, not the average.
    # This is needed in case one of those two contains a transient.
    # If the average were used, the transient would not be rejected
    # downstream in the pipeline.
    imcombine( "@"//slist, ofile, imcmb="",
        headers="", bpmasks=bpmfile//".pl", rejmasks="", nrejmasks="",
        expmasks=expmap//".pl", sigmas="", logfile=lfile, combine="lmedian",
        reject="none", project=no, outtype="real", outlimits="",
        offsets="wcs", masktype="novalue", maskvalue="2", blank=0.,
        scale="!stkscale", zero="none", weight="!"//wkey, statsec="",
        expname="oexptime",
        lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1, nkeep=1,
        mclip=yes, lsigma=3., hsigma=3., rdnoise="0.", gain="1.",
        snoise="0.", sigscale=0.1, pclip=-0.5, grow=0., mode="ql" )

    if ( debug_mode ) {
        # The following section transforms the BPMs for the images used
        # to make the stack.  These transformed BPMs are then combined,
        # by summing, to a stacked BPM with the pixel values giving the
        # total number of pixels along each column that are TR-flagged.	
        if (access("stkstack-TRmaskTEST.list"))
            delete("stkstack-TRmaskTEST.list",verify-)
        ;
        if (access("stkstack-TRmaskTEST2.list"))
            delete("stkstack-TRmaskTEST2.list",verify-)
        ;
        hselect("@"//slist,"BPM",yes,>"stkstack-TRmaskTEST.list")
        ict=1
        list="stkstack-TRmaskTEST.list"
        while(fscan(list,s2) != EOF) {
            imexpr("a>=8?1: 0", "TRmaskTEST"//ict//".pl", s2)
            printf("%s\n","TRmaskTEST"//ict//".pl",>>"stkstack-TRmaskTEST2.list")
            ict=ict+1
        }
        list=""
        imcombine("@stkstack-TRmaskTEST2.list",shortname//"_stkTRmask.pl",
            imcmb="",headers="",bpmasks="",rejmasks="",nrejmasks="",
            expmasks="",sigmas="",logfile=lfile,
            combine="sum",reject="none",project=no,outtype="real",outlimits="",
            offsets="wcs",blank=0.,
            scale="none",zero="none",weight="none",statsec="",expname="",
            lthreshold=INDEF,hthreshold=INDEF,nlow=INDEF,nhigh=INDEF,nkeep=INDEF,
            mclip=no,lsigma=INDEF,hsigma=INDEF,rdnoise="0.",gain="1.",
            snoise="0.",sigscale=INDEF,pclip=INDEF,grow=0.,mode="ql")
    }
    ;

} 

# Check if the stack was successful.
if ( imaccess( ofile ) == NO )
    logout 0
;

# Now set the IMCMB keywords from the parent exposures.
if ( access( "stkstack2.tmp" ) )
    delete( "stkstack2.tmp" )
;
if ( access( "stkstack3.tmp" ) )
    delete( "stkstack3.tmp" )
;
list = slist
while ( fscan( list, s1 ) != EOF ) {
    i = strldx( "/", s1 ) + 1
    if (i > 1)
	s1 = substr( s1, i, 999 )
    ;
    i = stridx( "_", s1 ) - 1
    if (i > 1)
	s1 = substr( s1, 1, i )
    ;
    printf( "%s\n", s1, >> "stkstack2.tmp" )
}
list = ""
sort( "stkstack2.tmp" ) | unique( "STDIN", > "stkstack3.tmp" )

list = "stkstack3.tmp"
for ( ncombine=0; fscan(list,s1)!=EOF; ) {
    ncombine += 1
    printf ("IMCMB%03d\n", ncombine) | scan (s3)
    nhedit (ofile, s3, s1, "Contributing exposure (archive name)", add+,ver-)
}
list = ""

# Update keywords.
nhedit (ofile, "NCOMBINE", ncombine, "Number stacked",ver-)
nhedit (ofile, "MAGZERO", "(STKMZ)", ".",ver-)
nhedit (ofile, "WMETHOD", wkey, "Weighting method", add+,ver-)
nhedit (ofile, "DATASEC", del+,ver-)

# If there is only one exposure set exit status to 3 to to skip stkqual.
# Ideally, we want stkqual to run but I have not yet gotten it to work.
if (ncombine == 1)
    logout 3
;

# Clean up.
if ( do_cleanup ) {
    delete( "stkstack?.tmp" )
}
;

logout 1
