#!/bin/env pipecl
#
# SPKPEDESTAL

# Declare variables.
string  dataset, datadir, lfile, indir, ilist, rlist, slist
string  spkstack, bpmfile, rule, method, wkey, msg
string	shortname, offmeth, mbpm, inlist, outlist, obm, wcs
string  instack, outstack
int	ncombine, ict, offorder, iter

# Load the packages.
redefine mscred=mscred$mscred.cl
images
lists
util
noao
proto
nproto
mscred
cache mscred
nfextern
ace
task parseresbck = "NHPPS_PIPESRC$/NEWFIRM/SKY/parseresbck.cl"
task $mkmask = "$!sed s/rsp.fits/rsp_obm.fits/ $1 > tmp && paste -d\  $1 tmp > $2"
task $mksub = "$!sed s/rsp.fits/rsp$2.fits/ $1 > tmp && paste -d\  $3 tmp > $4"
task $mksub2 = "$!sed s/rsp.fits/rsp$2.fits/ $1 | awk \'BEGIN{FS=\"/\"}{print \$NF}\' > $5 && paste -d\  $3 $4 $5 > $6"

# Set the dataset name and uparm.
names( "spk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
shortname = names.shortname
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".spk"
slist = "spkslist.tmp"
spkstack = shortname //"_spk"
bpmfile = shortname//"_spk_bpm"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSPKPEDESTAL (%s): ", dataset ) | tee (lfile)
time | tee (lfile)

# Go to the relevant directory
cd( datadir )

# Only run this module if specified in sps_resbck and the data have
# offset sky.
parseresbck( sps_resbck ) | scan( s1, i, s1, i, offmeth, offorder )
print( sps_resbck )
printf( "%s %d %s %d %s %d\n", s1, i, s1, i, offmeth, offorder )

# Test whether offset sky exposures were used to subtract the sky.
list = slist
i = fscan( list, s1 )
line = ""; hselect( s1, "SSUBINFO", yes ) | scan( line )
list = ""

if ( ( offmeth != "pedestal" ) || ( strstr("ffset",line) == 0 ) )
    logout( 1 )
;

# If this is the offset part of the sequence, don't do the pedestal
# removal.
hselect( s1, "OBSTYPE", yes ) | scan( s2 )
if ( ( s2 == 'sky' ) || ( s2 == 'SKY' ) ) {
    print "Not doing pedestal removal for offset sky exposures"
    logout( 1 )
}
;

# Make sure more than one image is available
count( slist ) | scan( ncombine )
if ( ncombine < 2 ) {
    sendmsg( "WARNING", "Insufficient SIFs to stack", dataset, "PROC" )
    logout( 2 )
}
;

# Create an initial object mask from the stack created
obm = "pass1.pl"
iferr {
    $aceall( spkstack, masks="!BPM", skyotype="subsky", skies="",
        sigmas="", objmasks=obm, catalogs="", skyimage="",
        exps="", gains="", omtype="all", extnames="",
        catdefs="pipedata/swcace.def", catfilter="", logfiles="STDOUT",
        verbose=2, order="", nmaxrec=INDEF, gwtsig=INDEF, gwtnsig=INDEF,
        fitstep=100, fitblk1d=10, fithclip=2.,
        fitlclip=3., fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
        blksize=-10, blknsubblks=2, updatesky=yes, bpdetect="100",
        bpflag="1-6", convolve="bilinear 3 3", hsigma=3., lsigma=10., 
        hdetect=yes, ldetect=no, neighbors="8", minpix=6, sigavg=4., sigmax=4.,
        bpval=INDEF, splitmax=INDEF, splitstep=0., splitthresh=5.,
        sminpix=8, ssigavg=10., ssigmax=5., ngrow=0, agrow=0.,
        magzero="!MAGZREF", cafwhm=3.)
} then {
    printf( "ACE failed on SECOND-PASS STACK dataset %s\n", spkstack )
    logout( 0 )
} else
    ;

# Match the masks to the input list
# First, create the list, which each line containing an input image and
# the output matched mask
mkmask( slist, "slistobm" )
list = "slistobm"
# Loop over all entries to match the object mask from the stack
# to each of the input images
while ( fscan( list, s1, s2 ) != EOF ) {
    s3 = substr( s2, strldx("/",s2)+1, 999 )
    mscimage( obm, s3, reference=s1, wcs="match" )
}
list = ""

# Subtract the estimated background
# Create a list in which each lines contains three entries: the input
# image, the matched mask created above, and the output image
mksub( slist, "2", "slistobm", "slistsub" )
list = "slistsub"
# Loop over all entries to subtract the background
while( fscan( list, s1, s2, s3 ) != EOF ) {
    # Match the model (the original stack) to the data
    mscimage( spkstack, s3, reference=s1, wcs="match" )
    # Subtract the matched model from the data
    imarith( s1, "-", s3, "diff" )
    # Mask was created here but s2 points to remote path
    mbpm = substr( s2, strldx("/",s2)+1, 999 )
    # Fit a polynomial to the difference
    hedit( "diff", "BPM", mbpm, add+, ver-, update+ )
    mscskysub( "diff", "fit", 3, 3, type_output="fit", regions="mask" )
    # Subtract the polynomial from the data
    imdel( s3 )
    imarith( s1, "-", "fit", s3 )
    # Clean up
    imdel( "diff,fit" )
}

# Create the new stack. First, parse spk_weight to determine which
# weighting keyword to use for weighting SIFs.
rule = spk_weight
j = stridx( ",", rule )
if ( j != 0 ) {
    method = substr( rule, 1, j-1 )
} else {
    method = rule
}
wkey = ""
if ( (method == "total") || (method =="default") ) {
    wkey = "WTOT"
} else if ( method == "see" ) {
    wkey = "WSEEING"
} else if ( method == "transns" ) {
    wkey = "WTRNSNS"
}
if ( wkey == "" ) {
    wkey = "OEXPTIME"
    method = "none"
    msg = "spk_weight not recognized by spkstack. setting it to 'none'."
    sendmsg( "WARNING", msg, dataset, "PROC" )
}
;
    
# Create the new stack in which much of the pedestal offsets are removed
fields( "slistsub", "3", > "spkslist2.tmp" )
imcombine( "@spkslist2.tmp", shortname//"_spk2", imcmb="",
    headers="", bpmasks=shortname//"_spkbpm2.pl", rejmasks="", nrejmasks="",
    expmasks="", sigmas="", logfile="STDOUT", combine="average",
    reject="none", project=no, outtype="real", outlimits="",
    offsets="wcs", masktype="novalue", maskvalue="2", blank=0.,
    scale="!spkscale", zero="none", weight="!"//wkey, statsec="",
    expname="oexptime",
    lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1, nkeep=1,
    mclip=yes, lsigma=3., hsigma=3., rdnoise="0.", gain="1.",
    snoise="0.", sigscale=0.1, pclip=-0.5, grow=0., mode="ql" )

# Create an improved mask
obm = "pass2.pl"
iferr {
    $aceall( shortname//"_spk2", masks="!BPM", skyotype="subsky", skies="",
        sigmas="", objmasks=obm, catalogs="", skyimage="",
        exps="", gains="", omtype="all", extnames="",
        catdefs="pipedata/swcace.def", catfilter="", logfiles="STDOUT",
        verbose=2, order="", nmaxrec=INDEF, gwtsig=INDEF, gwtnsig=INDEF,
        fitstep=100, fitblk1d=10, fithclip=2.,
        fitlclip=3., fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
        blksize=-10, blknsubblks=2, updatesky=yes, bpdetect="100",
        bpflag="1-6", convolve="bilinear 3 3", hsigma=3., lsigma=10., 
        hdetect=yes, ldetect=no, neighbors="8", minpix=6, sigavg=4., sigmax=4.,
        bpval=INDEF, splitmax=INDEF, splitstep=0., splitthresh=5.,
        sminpix=8, ssigavg=10., ssigmax=5., ngrow=0, agrow=0.,
        magzero="!MAGZREF", cafwhm=3.)
} then {
    printf( "ACE failed on SECOND-PASS STACK dataset %s\n",
	    shortname//"_spk2" )
} else
    ;

# Match the updated masks to the input list
list = "slistobm"
# Loop over all entries to match the object mask from the stack
# to each of the input images
while ( fscan( list, s1, s2 ) != EOF ) {
    s3 = substr( s2, strldx("/",s2)+1, 999 )
    if ( imaccess( s3 ) )
	imdel( s3 )
    ;
    s2 = substr( s3, 1, strlstr(".fits",s3)-1 ) // "_bpm"
    if ( imaccess( s2 ) )
	imdel( s2 )
    ;
    mscimage( obm, s3, reference=s1, wcs="match" )
}
list = ""

print "BEGIN ITER"
# Start of iterations. A minimum of 1 iteration is needed.
if ( offorder < 1 )
    offorder = 1
;
iter = 1
fields( "slistsub", "2", > "masklist" )
while ( iter <= offorder ) {
    
    # Set the name of the stacks
    instack = shortname//"_spk"//str(iter+1)
    outstack = shortname//"_spk"//str(iter+2)
    # Set the input list
    inlist = "spkslist"//str(iter+1)//".tmp"
    # Create the output list, and also merge the lists for the loop:
    outlist = "spkslist"//str(iter+2)//".tmp"
    mksub2( slist, str(iter+2), inlist, "masklist", outlist, "looplist" )
    list = "looplist"
    # Loop over all entries to subtract the background
    while( fscan( list, s1, s2, s3 ) != EOF ) {
        # Match the model
        mscimage( instack, s3, reference=s1, wcs="match" )
        # Subtract the matched model from the data
        imarith( s1, "-", s3, "diff" )
        # Mask was created here but s2 points to remote path
        mbpm = substr( s2, strldx("/",s2)+1, 999 )
        # Fit a polynomial to the difference
        hedit( "diff", "BPM", mbpm, add+, ver-, update+ )
        mscskysub( "diff", "fit", 3, 3, type_output="fit", regions="mask" )
        # Subtract the polynomial from the data
        imdel( s3 )
        imarith( s1, "-", "fit", s3 )
        # Clean up
        imdel( "diff,fit" )
    }

    # Create a new stack
    imcombine( "@"//outlist, outstack, imcmb="", headers="",
        bpmasks=shortname//"_spk_bpm"//str(iter+2)//".pl",
        rejmasks="", nrejmasks="",
        expmasks="", sigmas="", logfile="STDOUT", combine="average",
        reject="none", project=no, outtype="real", outlimits="",
        offsets="wcs", masktype="novalue", maskvalue="2", blank=0.,
        scale="!spkscale", zero="none", weight="!"//wkey, statsec="",
        expname="oexptime",
        lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1, nkeep=1,
        mclip=yes, lsigma=3., hsigma=3., rdnoise="0.", gain="1.",
        snoise="0.", sigscale=0.1, pclip=-0.5, grow=0., mode="ql" )

    iter = iter + 1
}

# Check if the stack was successful.
if ( imaccess( outstack ) == NO )
    logout( 0 )
;

# Now set the IMCMB keywords from the parent exposures.
if ( access( "spkstack2.tmp" ) )
    delete( "spkstack2.tmp" )
;
if ( access( "spkstack3.tmp" ) )
    delete( "spkstack3.tmp" )
;
list = slist
while ( fscan( list, s1 ) != EOF ) {
    i = strldx( "/", s1 ) + 1
    if (i > 1)
	s1 = substr( s1, i, 999 )
    ;
    i = stridx( "_", s1 ) - 1
    if (i > 1)
	s1 = substr( s1, 1, i )
    ;
    printf( "%s\n", s1, >> "spkstack2.tmp" )
}
list = ""
sort( "spkstack2.tmp" ) | unique( "STDIN", > "spkstack3.tmp" )

# Determine what the final data product name will be for the
# images that went into this stack.
s1 = substr( dataset, 1, strstr("-sec",dataset)-1 )
s2 = substr( s1, 1, strstr("-gos",s1)-1 ) // "-" //
     substr( s1, strlstr("-gos",s1)+4, 999 ) // "-"

list = "spkstack3.tmp"
for (ncombine=0; fscan(list,s1)!=EOF; ) {
    ncombine += 1
    printf ("IMCMB%03d\n", ncombine) | scan (s3)
    s1 = s2 // s1 // "_r.fits"
    nhedit (outstack, s3, s1, "Contributing exposure (archive name)", add+)
}
list = ""

# Update keywords.
nhedit (outstack, "NCOMBINE", ncombine, "Number stacked")
nhedit (outstack, "MAGZERO", "(SPKMZ)", ".")
nhedit (outstack, "WMETHOD", wkey, "Weighting method", add+)
nhedit (outstack, "DATASEC", del+)

# Replace the stack created by spkqual with the one created here:
imdel( spkstack )
imrename( outstack, spkstack )
imdel( shortname//"_spk2" )

# Replace the input images with the background-subtracted images
imdel( "@"//slist )
nhedit( "@"//outlist, "RESBCKMT", "pedestal",
    "Residual sky subtraction method", add+, ver- )
imcopy( "@"//outlist, "@"//slist )

# This module is configured to run always for debugging/testing
logout( 1 )
