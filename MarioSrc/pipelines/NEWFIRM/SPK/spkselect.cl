#!/bin/env pipecl
#
# SPKSELECT - Select the exposures to be stacked, and determine photometric
# scaling factors
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful (not implemented in module)
#   2 = Successful: No images to resample and stack 
#   3 = Successful: Only one image to resample and "stack"
#   4 = Successful: No images to stack, but there are images to resample.
#           This will be checked in spkstack.cl, and not taken care of in
#           spk.xml.  So, this could be changed to status 1 later.  
#   5 = Unsuccessful:  Rejection or weighting algorith failed, but
#           processing continues with all images accepted (except those
#           that have been rejected by user), if rejection failed, and
#           with uniform weights, if weighting failed.
#
# History
#
#   R. Swaters -------- Created.
#   T. Huard 20081219   Added code to provide weights for the SIFs depending on
#       the transparency and seeing, if the IRAF parameter
#       "spk_weighting" is set to 1.
#   T. Huard 20081224   Included some error catching for cases when MAGZERO,
#       SEEING1, and/or SKYNOIS1 are not found in the FITS headers of
#       the SIFs. If this happens, default values for MAGZERO,SEEING1,
#       SKYNOIS1 of 22., 1., 5., are assigned for now. The default for
#       SKYNOIS1 is somewhat arbitrary (I looked at some SIFs and 
#       found values of ~2.)
#   T. Huard 20090107   Removed "END END" from the bottom of the
#       spkselect-qual1.txt file, and changed .tbl to .txt.
#   T. Huard 20090129   Added SEEMID and SEESIG keywords so that weights can be 
#       recomputed without having to know the distribution of seeing
#       values.  (These are used in spkqual.cl.)
#   T. Huard 20090210   Removed SEEMID, SEESIG keywords. These are recomputed
#       in spkqual.cl.  Added more information to the output table of 
#       accepted images (spkselect-qual-accept.txt), information not
#       used in weighting but used in quality characterization.  An
#       output list of images rejected due to pipeline problems with
#       data, such as those with WCS solutions or missing keywords,
#       is now created (spkselect-pipe-reject.txt). An output table of
#       those images rejected because of data quality, such as given
#       by transparency or seeing thresholds, is now created
#       (spkselect-qual-reject.txt).  In the current version of the 
#       module, however, there is no implementation of rejection based
#       on data quality.  Reworked some of the image loops, added more
#       documentation, and reformatted the file.
#   T. Huard 20090508   After comparing the differences between spkselect.cl
#       and stkselect.cl (previous version before recent changes), I concluded
#       that the only real difference between the two was stkscale factor
#       is written to PMAS in stkselect.cl, but this is not done in
#       spkselect.cl.  So, I have taken the current (as of 20090508) version
#       of stkselect.cl, which has the rejection/weighting modules implemented,
#       and did a search and replace on stk->spk and STK->SPK.  I then placed
#       the new spkselect code below, but retained the original comments/
#       history at the beginning of this procedure.  For now, I have commented
#       out (with ##) that portion of code in this module that writes the
#       spkscale factor to PMAS.  We can uncommented it, if we decide we want
#       to save this in PMAS.
#   T. Huard 20090509   Added checks that there are still SIFs after the
#       KW-rejection stage and the QUAL-rejection stage.  If not, then there
#       are images to be resampled, but not to be stacked.  In this case,
#       exit with status 4. Changed REJECT1->REJECT2, WTRNSNS1->WTRNSNS2, etc.
#   T. Huard 20090510   Added code to fix bugs when case of all but one image
#       is rejected due to WCS.  Also, needed to add SPKMZ, SPKSCALE keywords
#       to the remaining image in that case (for spkstack).
#   T. Huard 200908--   Added check on istat after rejection modules to
#       keep spkselect from hanging.  Hanging occurred whenever the rejection
#       modules would fail, for some reason, thereby returning a message into
#       "line".  Then, the first characters in "line" would not be an integer;
#       thus, when it is scanned into "istat" (defined to be an integer), 
#       spkselect would hang with "istat: ", acting as if istat is not defined.
#       Now, if this happens, an ERROR is printed to the log (and message
#       monitor) and processing continues with no rejection of images based
#       on image quality. (status=5)
#   T. Huard 20090904   Fixed checks on istat after weighting modules.
#       Similar to checks after rejection modules.  Failure of weighting
#       also results in status=5.  In this case, uniform weights are 
#       applied but an ERROR is printed to the log (and message monitor).
#   T. Huard 20100225   Fixed bug with calculation of scaling factors SPKSCALE
#	based on relative transparencies.  Because of some code history, the
#	transparency and skynoise columns had been swapped, and the previous
#	version of this module was taking the skynoise for the transparency.
#	This bug had been fixed in Revision 1.17, but was inadvertently 
#	reintroduced in Revision 1.19.
#	Last Revised:  T. Huard  20100225  3:00pm

# Declare variables.
int     status = 1
int     first = 1
int     istat
real    magzero,seeing,skynoise,skymode,trans,transREF,spkscale,spkmz
string  datadir,dataset,ilist,lfile,ofile,indir,olist,shortname
string  ref,msg,reject,cast,rejectTMP
struct  *list2

# Load the packages.
images
lists
proto
servers
dataio
task $paste = "$!paste -d\  $1 $2 > $3"
task $rejection = "$rejection1.cl $1 $2"
task $weighting = "$weighting1.cl $1 $2"

# Set the dataset name and uparm.
names( "spk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".spk"
olist = "spkolist.tmp"
ofile = dataset//"_spk"
shortname = names.shortname
set (uparm = names.uparm)

# Log the operation.
printf( "\nSPKSELECT (%s):\n", dataset ) | tee(lfile)

# Go to the relevant directory
cd( datadir )

# *******************************************************************
# ***** PREFILTER FOR TESTING PURPOSES (only 1 SIF survive WCS) *****
# *******************************************************************
#list=ilist
#i=fscan(list,s1)
#hedit(s1,"REJECT","N!",add+,update+,verify-,show-)
#while(fscan(list,s1) != EOF) {
#    hedit(s1,"REJECT","Y!",add+,update+,verify-,show-)
#    hedit(s1,"WCSCAL","F",add+,update+,verify-,show-)
#}
#list=""
# *************************************************************************
# ***** PREFILTER FOR TESTING PURPOSES (only 1 SIF survive rejection) *****
# *************************************************************************
#list=ilist
#i=fscan(list,s1)
#hedit(s1,"REJECT","N!",add+,update+,verify-,show-)
#while(fscan(list,s1) != EOF) {
#    hedit(s1,"REJECT","Y!",add+,update+,verify-,show-)
#}
#list=""
# ********************************************************
# ***** MAKE SURE NO IMAGES ARE PRESET FOR REJECTION *****
# ********************************************************
list=ilist
while(fscan(list,s1) != EOF) {
    hedit(s1,"REJECT","",add+,update+,verify-,show-)
}
list=""

# Make a table here, "spkselect-table.txt", that is composed of six columns:
# filename, relative transparency, seeing, skynoise, skymode, and magzero.
# These quantities are useful either for computing the relative weights to
# construct the stack or for assessing the quality of the stack.  The
# transparency is normalized such that it is unity for magzero=22.  This
# is fine since these will be used to compute relative weights.  Images
# that do not have values for all four keywords are written to a file,
# "spkselect-rejKW.txt" for later use in this module.  (This is because I
# want the keyword check to come after the WCS check, and the REJECT 
# values to reflect that.)
#
# Also, there are two "rejection info tables" ("spkselect-table2.txt",
# "spkselect-reject.txt") created in this module.  These first is simply
# a copy of "spkselect-table.txt", but with the images ordered as the
# REJECT keywords are set.  The second is the value of the REJECT keywords
# for the images in the same order.  In this way, the two rejection info
# tables can be pasted to remake "spkselect-table.txt" with a column 
# containing the REJECT value.
if (access("spkselect-table.txt")) delete("spkselect-table.txt",verify-)
;
if (access("spkselect-rejKW.txt")) delete("spkselect-rejKW.txt",verify-)
;
if (access("spkselect-table2.txt")) delete("spkselect-table2.txt",verify-)
;
if (access("spkselect-reject.txt")) delete("spkselect-reject.txt",verify-)
;
touch("spkselect-rejKW.txt")
list=ilist
while(fscan(list,s1) != EOF) {
    hselect(s1,"MAGZERO,SEEING1,SKYNOIS1,SKYMODE",yes) | scan(magzero,seeing,
        skynoise,skymode)
    if (nscan() == 4) {
        # If all four keywords were available, great!  Compute transparency
        # and write the quantities to the table.
        trans=10**((magzero-22.)/2.5)
    } else {
        # If all four keywords were not available, then go through the process
        # of determining which were missing.  Compute transparency if MAGZERO
        # is available.  For those keywords not available write INDEF in the 
        # table.
        magzero=INDEF ; seeing=INDEF ; skynoise=INDEF ; skymode=INDEF
        hselect(s1,"MAGZERO",yes) | scan(magzero)
        hselect(s1,"SEEING1",yes) | scan(seeing)
        hselect(s1,"SKYNOIS1",yes) | scan(skynoise)
        hselect(s1,"SKYMODE",yes) | scan(skymode)
        if (magzero != INDEF) {
            trans=10**((magzero-22.)/2.5)
        } else {
            trans=INDEF
        }
        printf("%s\n",s1,>>"spkselect-rejKW.txt")
    }
    printf("%s %g %g %g %g %g\n",s1,trans,seeing,skynoise,skymode,
        magzero,>>"spkselect-table.txt")
}
list=""

# ******************************
# *** REJECTION BASED ON WCS ***
# ******************************
# Remove images without WCS.  The list of images with WCS saved as olist.
# For images rejected based on WCS, set REJECT keyword accordingly.
if (access(olist)) delete(olist,verify-)
;
touch(olist)
list = ilist
while ( fscan( list, s1 ) != EOF ) {
    s2 = "F"
    hselect( s1, "WCSCAL", yes ) | scan( s2 )
    if ( s2 == "T" ) {
        if ( first == 1 ) {
            first = 0
            ref = s1
        }
        ;
        print( s1, >> olist )
    } else {
        # For images rejected because of lack of WCS, set REJECT=Ywcs, unless
        # it was previously set by the user to be Y!.  In that case
        # REJECT=Y!wcs.  If REJECT was previously set by the user to be N!,
        # the pipeline needs to overrule that decision.  In that case
        # REJECT=YwcsN! in order to reject the image based on WCS, but to let
        # others know that the user wanted originally to keep it.  In that
        # case, a message is sent to the message monitor to alert the operator
        # of this change.
        reject="" ; hselect(s1,"REJECT",yes) | scan(reject)
        rejectTMP=substr(reject,1,2)
        if (rejectTMP == "Y!") {
            reject=reject//"wcs"
        } else if (rejectTMP == "N!") {
            reject="YwcsN!"
            msg="Overruling rejection overrule (N! -> YwcsN!) for "//s1
            sendmsg("WARNING",msg,shortname,"PROC")
        } else {
            reject="Ywcs"
        }
        hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
        # Make sure the REJECT is copied to REJECT2
        hedit(s1,"REJECT2",reject,add+,update+,verify-,show-)
        # Update rejection info tables    
        match(s1,"spkselect-table.txt",>>"spkselect-table2.txt")
        printf("%s\n",reject,>>"spkselect-reject.txt")
        # Set the weighting keywords to 0.
        hedit(s1,"WTRNSNS",0.,add+,update+,verify-,show-)
        hedit(s1,"WSEEING",0.,add+,update+,verify-,show-)
        hedit(s1,"WTOT",0.,add+,update+,verify-,show-)
        hedit(s1,"WTRNSNS2",0.,add+,update+,verify-,show-)
        hedit(s1,"WSEEING2",0.,add+,update+,verify-,show-)
        hedit(s1,"WTOT2",0.,add+,update+,verify-,show-)
        hedit(s1,"SPKSCALE",0.,add+,update+,verify-,show-)
    }
}
list = ""

# Following rejection of images based on WCS, determine number of exposures in
# the current group.  If there are no images left (exit status 2), or there is
# just one image left (exit status 3), then there is no need to proceed to the
# rejection and weighting code.
if ( access( olist ) ) {
    count( olist ) | scan( i )
} else
    i = 0
    
if (i==0)
    status = 2
else if (i==1) {
    # In the case where only one image remains, it essentially will be a stack
    # of itself.  The pipeline will set REJECT=N in this case, unless it was
    # previously set by a user to be N! or Y!.  In that case, it will retain its
    # value.
    status = 3
    fields(olist,"1",lines="1") | scan(s1)
    reject="" ; hselect(s1,"REJECT",yes) | scan(reject)    
    rejectTMP=substr(reject,1,2)
    if ((rejectTMP != "Y!") && (rejectTMP != "N!")) {
        reject="N"
        hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
        # Make sure the REJECT is copied to REJECT2
        hedit(s1,"REJECT2",reject,add+,update+,verify-,show-)
    }
    ;
    # Update rejection info tables   
    match(s1,"spkselect-table.txt",>>"spkselect-table2.txt")
    printf("%s\n",reject,>>"spkselect-reject.txt")

    # Set the weighting keywords to 1.  Set SPKSCALE, SPKMZ.
    hedit(s1,"WTRNSNS",1.,add+,update+,verify-,show-)
    hedit(s1,"WSEEING",1.,add+,update+,verify-,show-)
    hedit(s1,"WTOT",1.,add+,update+,verify-,show-)
    hedit(s1,"WTRNSNS2",1.,add+,update+,verify-,show-)
    hedit(s1,"WSEEING2",1.,add+,update+,verify-,show-)
    hedit(s1,"WTOT2",1.,add+,update+,verify-,show-)
    hedit(s1,"SPKSCALE",1.,add+,update+,verify-,show-)
    spkmz=0. ; hselect(s1,"MAGZERO",yes) | scan(spkmz)
    hedit(s1,"SPKMZ",spkmz,add+,update+,verify-,show-)
}
;
if (status != 1) {
    # Paste rejection info to table "spkselect-table.txt"
    paste("spkselect-table2.txt","spkselect-reject.txt","tmp.txt")
    rename("tmp.txt","spkselect-table.txt")
    # Clean up
    delete("tmp.txt",verify-)
    delete("spkselect-reject.txt",verify-)
    # Log out
    logout (status)
}
;

# ***************************************************
# *** REJECTION BASED ON AVAILABILITY OF KEYWORDS ***
# ***************************************************
# Reject images that do not have all four keywords - MAGZERO, SEEING1, SKYNOIS1,
# SKYMODE - written to the header.  It should be very rare that an image is
# rejected for this reason.  For images rejected for this reason, set their 
# REJECT keywords accordingly, and update rejection info tables.  Even if 
# spk_reject="none" and spk_weight=0 (i.e., no quality rejection is performed,
# and no weighting is done, except weighting by exposure time), we still reject
# images where one of these keywords has not been included because these are 
# important for characterizing the quality of the stack in spkqual.cl
#
# Make the list of images ("spkselect-inREJ.txt") that will be passed on to
# the quality rejection module.  These are all images in olist (which have
# already been cleaned from the WCS-problematic images), except for the images
# with keyword problems.
fields(olist,"1",>"spkselect-inREJ.txt")
list="spkselect-rejKW.txt"
while(fscan(list,s1) != EOF) {
    reject="" ; hselect(s1,"REJECT",yes) | scan(reject)    
    rejectTMP=substr(reject,1,2)
    if (rejectTMP == "Y!") {
        reject=reject//"kw"
    } else if (rejectTMP == "N!") {
        reject="YkwN!"
    } else {
        reject="Ykw"
    }
    hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
    # Make sure the REJECT is copied to REJECT2
    hedit(s1,"REJECT2",reject,add+,update+,verify-,show-)
    # Update rejection info tables    
    match(s1,"spkselect-table.txt",>>"spkselect-table2.txt")
    printf("%s\n",reject,>>"spkselect-reject.txt")
    # Filter out the keyword-rejected image from the image list that will
    # be given to the quality rejection module.
    match(s1,"spkselect-inREJ.txt",stop+,>"tmp.txt")
    rename("tmp.txt","spkselect-inREJ.txt")
    # Set the weighting keywords to 0.
    hedit(s1,"WTRNSNS",0.,add+,update+,verify-,show-)
    hedit(s1,"WSEEING",0.,add+,update+,verify-,show-)
    hedit(s1,"WTOT",0.,add+,update+,verify-,show-)
    hedit(s1,"WTRNSNS2",0.,add+,update+,verify-,show-)
    hedit(s1,"WSEEING2",0.,add+,update+,verify-,show-)
    hedit(s1,"WTOT2",0.,add+,update+,verify-,show-)
    hedit(s1,"SPKSCALE",0.,add+,update+,verify-,show-)
}
list=""

# Check that the number of SIFs to combine is not 0. 
# If it is, then send message to monitor and logout with
# status of 4 (images to resample, but no images to stack).
count("spkselect-inREJ.txt") | scan(i)
if (i==0) {
    msg="No more SIFs to reject/weight following WCS+KW rejection in SPKselect"
    print(msg)
    sendmsg("WARNING",msg,dataset,"PROC")
    status=4
    logout(status)
}
;

# ****************************************
# *** REJECTION BASED ON IMAGE QUALITY ***
# ****************************************
# Now, reject images based on their quality (seeing, transparency, skynoise),
# if spk_reject is not equal to "none".  Apply the rejection method specified
# by spk_reject.  The rejection module creates a list ("rejection.out"), which
# is a two column list, with the first column including all images input to the
# module and the second column including the REJECT values assigned.  The output
# of the rejection module is its exit status.  If call to rejection fails, then
# report this to the message monitor and proceed with no quality rejection done.
# Finally, create the list of images that will be given to the weighting code
# and set weight keywords (WTRNSNS, WSEEING, WTOT) for those images rejected
# based on quality that will not be forwarded to the weighting code.  An
# alternative approach could be to create the weighting list and set the
# weighting keywords in a separate loop.
# 
# *** SHOULD I MAKE A CHECK HERE THAT THERE ARE SUFFICIENT IMAGES IN 
# *** spkselect-inREJ.txt TO SEND TO REJECTION?
if (access("spkselect-inWEIGHT.txt")) delete("spkselect-inWEIGHT.txt",verify-)
;
touch("spkselect-inWEIGHT.txt")
if (spk_reject != "none") {
    line="" ; rejection("spkselect-inREJ.txt",spk_reject) | scan(line)
    istat=99 ; print(line) | scan(istat)   # line->character is necessary because of 
                                # format of the rejection output
    if ((isindef(istat)==YES) || (istat==99)) {
        status=5
        msg="Rej failed (indef). Setting all REJECT=N except where overruled."
        sendmsg("ERROR",msg,shortname,"PROC")
        printf("%s\n",line) | tee(lfile)        
        list="spkselect-inREJ.txt"
        while(fscan(list,s1) != EOF) {
            reject="" ; hselect(s1,"REJECT",yes) | scan(reject)
            rejectTMP=substr(reject,1,2)
            if ((rejectTMP != "N!") && (rejectTMP != "Y!")) {    
                reject="N"
                hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
            }
            ;
            # Make sure the REJECT is copied to REJECT2
            hedit(s1,"REJECT2",reject,add+,update+,verify-,show-) 
            # Update rejection info tables   
            match(s1,"spkselect-table.txt",>>"spkselect-table2.txt")
            printf("%s\n",reject,>>"spkselect-reject.txt")        
        }
        list=""
        copy("spkselect-inREJ.txt","spkselect-inWEIGHT.txt")    
    } else if (istat !=1) {
        status=5
        msg="Rej failed (mod). Setting all REJECT=N except where overruled."
        sendmsg("ERROR",msg,shortname,"PROC")
        printf("%s\n",line) | tee(lfile)        
        list="spkselect-inREJ.txt"
        while(fscan(list,s1) != EOF) {
            reject="" ; hselect(s1,"REJECT",yes) | scan(reject)
            rejectTMP=substr(reject,1,2)
            if ((rejectTMP != "N!") && (rejectTMP != "Y!")) {    
                reject="N"
                hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
            }
            ;
            # Make sure the REJECT is copied to REJECT2
            hedit(s1,"REJECT2",reject,add+,update+,verify-,show-) 
            # Update rejection info tables   
            match(s1,"spkselect-table.txt",>>"spkselect-table2.txt")
            printf("%s\n",reject,>>"spkselect-reject.txt")        
        }
        list=""
        copy("spkselect-inREJ.txt","spkselect-inWEIGHT.txt")
    } else {
        # Update rejection info tables, making use of the output file of 
        # rejection module, which is a two-column table.  The first column
        # includes the filenames, and the second column includes the 
        # REJECT values.
        rename("rejection.out","spkselect-outREJ.txt")
        fields("spkselect-outREJ.txt","1",>"tmpF.txt")
        fields("spkselect-outREJ.txt","2",>"tmpR.txt")
        list="tmpF.txt"
        list2="tmpR.txt"
        while((fscan(list,s1) != EOF) && (fscan(list2,reject) != EOF)) {
            # Update rejection info tables.            
            match(s1,"spkselect-table.txt",>>"spkselect-table2.txt")
            printf("%s\n",reject,>>"spkselect-reject.txt")
            # If the image is not rejected based on quality, include it in
            # the list for weighting.  If the image was rejected, set the
            # weighting keywords to 0, and do not include it in the list
            # for weighting.
            if (substr(reject,1,1) == "N") {
                printf("%s\n",s1,>>"spkselect-inWEIGHT.txt")
            } else {
                hedit(s1,"WTRNSNS",0.,add+,update+,verify-,show-)
                hedit(s1,"WSEEING",0.,add+,update+,verify-,show-)
                hedit(s1,"WTOT",0.,add+,update+,verify-,show-)
                hedit(s1,"WTRNSNS2",0.,add+,update+,verify-,show-)
                hedit(s1,"WSEEING2",0.,add+,update+,verify-,show-)
                hedit(s1,"WTOT2",0.,add+,update+,verify-,show-)
                hedit(s1,"SPKSCALE",0.,add+,update+,verify-,show-)
            }
            # Make sure the REJECT is copied to REJECT2
            hedit(s1,"REJECT2",reject,add+,update+,verify-,show-)
        }
        list=""
        list2=""
        delete("tmpF.txt",verify-)
        delete("tmpR.txt",verify-)
    }
}
;

# Since the REJECT keywords should all be set now, paste the rejection
# info tables to generate an updated table "spkselect-table.txt".
paste("spkselect-table2.txt","spkselect-reject.txt","tmp.txt")
rename("tmp.txt","spkselect-table.txt")
delete("spkselect-table2.txt",verify-)
delete("spkselect-reject.txt",verify-)

# Check that the number of SIFs to combine is not 0. 
# If it is, then send message to monitor and logout with
# status of 4 (images to resample, but no images to stack).
count("spkselect-inWEIGHT.txt") | scan(i)
if (i==0) {
    msg="No SIFs to weight following WCS+KW+QUAL rejection in SPKselect"
    print(msg)
    sendmsg("WARNING",msg,dataset,"PROC")
    status=4
    logout(status)
}
;

# **********************************
# *** WEIGHTING BASED ON QUALITY ***
# **********************************
# Prepare the input list for the weighting module, and sort the entries
# in order of decreasing transparency.  Compute the relative scaling,
# based on transparency, and write these to SPKSCALE.
# Note that a lesser zeropoint (MAGZERO), or lesser transparency, means
# that the image has lower throughput, and therefore it needs to be scaled
# up.  
#
# Even if spk_weight="none", this list preparation is done since it 
# enables a convenient way to set set SPKSCALE in the images.
if (access("tmp.txt")) delete("tmp.txt",verify-)
;
list="spkselect-inWEIGHT.txt"
while(fscan(list,s1) != EOF) {
    match(s1,"spkselect-table.txt",>>"tmp.txt")
}
list=""
fields("tmp.txt","1,3,2,4",lines="1-",>"tmp2.txt")
sort("tmp2.txt",column=3,numeric+,reverse_sort+,>"spkselect-inWEIGHT.txt")
delete("tmp.txt",verify-)
delete("tmp2.txt",verify-) 
# At this point, the list contains all images that will be stacked,
# even if each will receive different weights.  So, compute the relative
# transparency scaling factors (not the weight!) and include in the
# SPKSCALE keywords.  Also, record the MAGZERO of the reference image
# (which is also appropriate for scaled images) to SPKMZ keywords.  Finally,
# create a table "spkselect-SPKSCALE.txt" of filename and spkscale for info.
if (access("spkselect-SPKSCALE.txt"))
    delete("spkselect-SPKSCALE.txt",verify-)
;
fields("spkselect-inWEIGHT.txt","1,3",lines="1") | scan(s1,transREF)
hselect(s1,"MAGZERO",yes) | scan(spkmz) # could be recal from transREF
list="spkselect-inWEIGHT.txt"
while(fscan(list,s1,seeing,trans,skynoise) != EOF) {
    spkscale=transREF/trans
    hedit(s1,"SPKSCALE",spkscale,add+,update+,verify-,show-)
    hedit(s1,"SPKMZ",spkmz,add+,update+,verify-,show-)
    printf("%s %g\n",s1,spkscale,>>"spkselect-SPKSCALE.txt")
##    # Create a short version of the dataset name; store scale factor in PMAS
##    s2=substr(s1,strldx("/",s1)+1,strlstr(".fits",s1)-1)
##    printf("%12.5e\n",spkscale) | scan(cast)
##    setkeyval(class="objectimage",id=s2,dm=dm,keyword="dqphscal",value=cast)
}    
list=""

# If "spk_weighting" is set, then weight and/or reject data depending on seeing
# transparency, and sky noise level.  It should not be necessary to explicitly
# weight by the exposure time since this dependence is implicit in the sky
# noise level.  For example, if we were combining 1-sec and 2-sec exposures
# having the same seeing and transparency, the 1-sec exposures should have
# a sky noise (per second) that is greater than the sky noise of the 2-sec
# exposures by ~sqrt(2).
#
# If "spk_weight" is not set, then weight data depending on the exposure 
# time only.  Instead of exposure time, we could instead weight by the 
# 1/(sky noise)^2, similar to what is done when weighting by seeing,
# transparency, and sky noise.
if (spk_weight != "none") {
    # Call the weighting module
    line="" ; weighting("spkselect-inWEIGHT.txt",spk_weight) | scan(line)
    istat=99 ; print(line) | scan(istat)   # line->character is necessary because of 
                                # format of the rejection output
    if ((isindef(istat)==YES) || (istat==99)) {
        status=5
        msg="Weighting failed (indef). Setting WSEEING,WTRNNS,WTOT to 1."
        sendmsg("ERROR",msg,shortname,"PROC")
        printf("%s\n",line) | tee(lfile)        
        list="spkselect-inWEIGHT.txt"
        while(fscan(list,s1) != EOF) {
            hedit(s1,"WTRNSNS",1.0,add+,update+,verify-,show-)
            hedit(s1,"WSEEING",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTOT",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTRNSNS2",1.0,add+,update+,verify-,show-)
            hedit(s1,"WSEEING2",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTOT2",1.0,add+,update+,verify-,show-)
        }
        list=""
    } else if (istat != 1) {
        status=5
        msg="Weighting failed (mod). Setting WSEEING,WTRNNS,WTOT to 1."
        sendmsg("ERROR",msg,shortname,"PROC")
        printf("%s\n",line) | tee(lfile)        
        list="spkselect-inWEIGHT.txt"
        while(fscan(list,s1) != EOF) {
            hedit(s1,"WTRNSNS",1.0,add+,update+,verify-,show-)
            hedit(s1,"WSEEING",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTOT",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTRNSNS2",1.0,add+,update+,verify-,show-)
            hedit(s1,"WSEEING2",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTOT2",1.0,add+,update+,verify-,show-)
        }
        list=""
    } else {
        rename("weighting.out","spkselect-outWEIGHT.txt")
    }
} else {
    # In this case, the weights are set to unity.  Weighting by
    # the exposure times will be performed in the imcombine statement
    # in spkstack.
    list="spkselect-inWEIGHT.txt"
    while(fscan(list,s1) != EOF) {
        hedit(s1,"WTRNSNS",1.0,add+,update+,verify-,show-)
        hedit(s1,"WSEEING",1.0,add+,update+,verify-,show-)
        hedit(s1,"WTOT",1.0,add+,update+,verify-,show-)
        hedit(s1,"WTRNSNS2",1.0,add+,update+,verify-,show-)
        hedit(s1,"WSEEING2",1.0,add+,update+,verify-,show-)
        hedit(s1,"WTOT2",1.0,add+,update+,verify-,show-)
    }
	list=""
}

# Clean up.
# DO NOT REMOVE spkselect-table.txt SINCE IT IS NEEDED BY spkqual.cl!

printf("Exit code: %d\n",status)

logout(status)
