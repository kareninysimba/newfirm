#!/bin/env pipecl
#
# SPKSTACK -- Stack exposures with weighting and rejection.
#
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful: Stack was not created, even though accepted SIFs found
#   2 = Successful?: No acceptable SIFs found; stack not created.  This is 
#               not necessarily "unsuccessful" since it is possible that 
#               all SIFs are legitimately rejected - see spkselect.
#   3 = Successful?: One acceptable SIF found; stack created.

# Declare variables.
string  dataset, datadir, lfile, indir, ilist, rlist, slist, ofile
string  bpmfile, expmap, rule, method, wkey, msg
string	shortname
int	ncombine, ict

# Load the packages.
images
lists
proto

# Set the dataset name and uparm.
names( "spk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
shortname = names.shortname
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".spk"
rlist = "spkrlist.tmp"
slist = "spkslist.tmp"
ofile = shortname //"_spk"
bpmfile = shortname//"_spk_bpm"
expmap = shortname//"_spk_expmap"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSPKSTACK (%s): ", dataset ) | tee (lfile)
time | tee (lfile)

# Go to the relevant directory
cd( datadir )

# Create slist, a sorted list of images to be stacked.
if ( access( slist ) ) delete( slist, verify- )
;
touch( slist )
hselect( "@"//rlist, "$I,REJECT,SPKSCALE", yes, > "spkstack.tmp" )
list = "spkstack.tmp"
while ( fscan( list, s1, s2, x ) != EOF ) {
    s3 = substr( s2, 1, 1 )
    if ( s3 != "N" )
        next
    ;
    printf( "%f %s\n", x, s1, >> slist )
}
list = ""
delete( "spkstack.tmp", verify- )
sort( slist, numeric+ ) | fields( "STDIN", "2", > slist )

# Check that the number of SIFs to combine is not 0. 
# If it is, then send message to monitor and logout with
# status of 2.
count( slist ) | scan( ncombine )
if ( ncombine == 0 ) {
    msg = "No SIFs to stack, based on REJECT keyword."
    sendmsg( "WARNING", msg, dataset, "PROC" )
    logout( 2 )
}
;

# First, parse spk_weight to determine which weighting keyword to
# use for weighting SIFs.
rule=spk_weight
j=stridx(",",rule)
if (j != 0) {
    method=substr(rule,1,j-1)
} else {
    method=rule
}
wkey=""
if ((method == "total") || (method =="default")) {
    wkey="WTOT"
} else if (method == "see") {
    wkey="WSEEING"
} else if (method == "transns") {
    wkey="WTRNSNS"
}
if (wkey == "") {
    wkey="OEXPTIME"
    method="none"
    msg="spk_weight not recognized by spkstack. setting it to 'none'."
    print(msg)
    sendmsg("WARNING",msg,dataset,"PROC")
}
;
    
if (ncombine==1) {

    # Only one exposure in the list, so copy the input data
    imcopy( "@"//slist, ofile )
    hsel( "@"//slist, "BPM", yes ) | scan( s1 )
    imcopy( s1, bpmfile//".pl" )
    hselect(ofile,"OEXPTIME",yes) | scan(x)
    imcopy(ofile,ofile//"expmap.pl")
    imreplace(ofile//"expmap.pl",x,lower=INDEF,upper=INDEF)
        
} else {

    # Combine the exposures. Although it is expected that the 
    # exposure times are the same for all exposures in a sequence,
    # the imcombine below is set up such that this is not a
    # requirement, i.e., the exposure times may be different.
    # 
    # If the images are not scaled to exptime=1, one would
    # expect that imcombine would give a straight sum (or average)
    # of all the contributing exposures (at least in the skynoise
    # dominated regime). In the pipeline, the images are scaled
    # back to exposure time of 1s. 
    #
    # To combine the images properly, one could first scale the
    # image back to the original exposure time (with the scale= 
    # keyword). However, when using the scale keyword, the output
    # in the combined image is also scaled (by the average over all
    # images of the scale factor with respect the first image).
    # It can be complicating to calculate this scale factor 
    # because the number of images contributing to each pixel
    # in the output of combine can vary, 
    # 
    # Fortunately, the images can also be scaled properly by using
    # weights, and that is done here.
    # 
    # The scale keyword is also used to photometrically scale
    # the exposures before combining them.
    
    # Run imcombine.  The imcombine will use the masks defined in the
    # BPM keyword to identify bad pixels.    
    imcombine( "@"//slist, ofile, imcmb="",
        headers="", bpmasks=bpmfile//".pl", rejmasks="", nrejmasks="",
        expmasks=expmap//".pl", sigmas="", logfile=lfile, combine="average",
        reject="none", project=no, outtype="real", outlimits="",
        offsets="wcs", masktype="novalue", maskvalue="2", blank=0.,
        scale="!spkscale", zero="none", weight="!"//wkey, statsec="",
        expname="oexptime",
        lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1, nkeep=1,
        mclip=yes, lsigma=3., hsigma=3., rdnoise="0.", gain="1.",
        snoise="0.", sigscale=0.1, pclip=-0.5, grow=0., mode="ql" )


    # The following section transforms the BPMs for the images used
    # to make the stack.  These transformed BPMs are then combined,
    # by summing, to a stacked BPM with the pixel values giving the
    # total number of pixels along each column that are TR-flagged.	
    if (access("spkstack-TRmaskTEST.list"))
        delete("spkstack-TRmaskTEST.list",verify-)
    ;
    if (access("spkstack-TRmaskTEST2.list"))
        delete("spkstack-TRmaskTEST2.list",verify-)
    ;
    hselect("@"//slist,"BPM",yes,>"spkstack-TRmaskTEST.list")
    ict=1
    list="spkstack-TRmaskTEST.list"
    while(fscan(list,s2) != EOF) {
        imexpr("a>=8?1: 0", "TRmaskTEST"//ict//".pl", s2)
        printf("%s\n","TRmaskTEST"//ict//".pl",>>"spkstack-TRmaskTEST2.list")
        ict=ict+1
    }
    list=""
    imcombine("@spkstack-TRmaskTEST2.list",shortname//"_spkTRmask.pl",
    	imcmb="",headers="",bpmasks="",rejmasks="",nrejmasks="",
        expmasks="",sigmas="",logfile=lfile,
        combine="sum",reject="none",project=no,outtype="real",outlimits="",
        offsets="wcs",blank=0.,
        scale="none",zero="none",weight="none",statsec="",expname="",
        lthreshold=INDEF,hthreshold=INDEF,nlow=INDEF,nhigh=INDEF,nkeep=INDEF,
        mclip=no,lsigma=INDEF,hsigma=INDEF,rdnoise="0.",gain="1.",
        snoise="0.",sigscale=INDEF,pclip=INDEF,grow=0.,mode="ql")
    
	
} 

# Check if the stack was successful.
if (imaccess(ofile) == NO)
    logout 0
;

# Now set the IMCMB keywords from the parent exposures.
if ( access( "spkstack2.tmp" ) )
    delete( "spkstack2.tmp" )
;
if ( access( "spkstack3.tmp" ) )
    delete( "spkstack3.tmp" )
;
list = slist
while ( fscan( list, s1 ) != EOF ) {
    i = strldx( "/", s1 ) + 1
    if (i > 1)
	s1 = substr( s1, i, 999 )
    ;
    i = stridx( "_", s1 ) - 1
    if (i > 1)
	s1 = substr( s1, 1, i )
    ;
    printf( "%s\n", s1, >> "spkstack2.tmp" )
}
list = ""
sort( "spkstack2.tmp" ) | unique( "STDIN", > "spkstack3.tmp" )

# Determine what the final data product name will be for the
# images that went into this stack.
s1 = substr( dataset, 1, strstr("-sec",dataset)-1 )
s2 = substr( s1, 1, strstr("-gos",s1)-1 ) // "-" //
     substr( s1, strlstr("-gos",s1)+4, 999 ) // "-"

list = "spkstack3.tmp"
for (ncombine=0; fscan(list,s1)!=EOF; ) {
    ncombine += 1
    printf ("IMCMB%03d\n", ncombine) | scan (s3)
    s1 = s2 // s1 // "_r.fits"
    nhedit (ofile, s3, s1, "Contributing exposure (archive name)", add+)
}
list = ""

# Undate keywords.
nhedit (ofile, "NCOMBINE", ncombine, "Number stacked")
nhedit (ofile, "MAGZERO", "(SPKMZ)", ".")
nhedit (ofile, "WMETHOD", wkey, "Weighting method", add+)
nhedit (ofile, "DATASEC", del+)

# If there is only one exposure set exit status to 3 to to skip spkqual.
# Ideally, we want spkqual to run but I have not yet gotten it to work.
if (ncombine == 1)
    logout 3
;

logout 1
