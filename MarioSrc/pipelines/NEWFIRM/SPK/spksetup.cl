#!/bin/env pipecl
#
# MTDSTATS

struct  statusstr
string  dataset, indir, datadir, ifile, im, lfile, maskname, shortname, s4

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
names( "spk", envget("OSF_DATASET") )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ifile = indir // dataset // ".spk"
shortname = names.shortname

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
        caldir = caldir // subdir

set (uparm = names.uparm)
set (pipedata = names.pipedata)

cd (datadir)

# Log start of processing.
printf( "\nSPKSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

# The input list contains both the images to be stacked and the 
# matching first-pass tack. The stack is removed from
# the input list and copied to the local directory.
# Extract the first-pass stack
touch( "spksetup1.tmp" )
match( "_stk.fits", ifile, print-, >> "spksetup1.tmp" )
count( "spksetup1.tmp" ) | scan( i )
if ( i == 1 )
    ;
else {
    sendmsg( "ERROR", "Incorrect number of stacks received ("//i//")",
        shortname, "PROC" )
    logout 0
}

# Copy the first-pass stack and its mask, which is assumed to reside in
# the same directory as the stack.
concat( "spksetup1.tmp" ) | scan( s1 )
s2 = substr( s1, strlstr("/",s1)+1, strlstr(".fits",s1)-1 )
imcopy( s1, s2 )
hselect( s2, "BPM", yes ) | scan( s3 )
printf ("BPM = %s\n", s3)
i = stridx ("[", s3)
if (i > 0) {
    s3 = substr (s3, 1, i-1)
    if (substr (s3, i-5, i-1) != ".fits")
       s3 += ".fits"
    ;
}
;
s4 = substr( s1, 1, strldx("/",s1) ) // s3
printf ("BPM = %s\n", s4)
copy( s4, s3 )

# Write the full iraf pathname of the local stack file to a file. The
# latter will be used by spkspistart.cl. Note that the implicit
# assumption is that the SPI pipeline will run on the same node as the
# SPK. If that is not the case, there is no need to copy the stack to
# a local directory first (and the SPI should be restructured, because
# it assumes the data resides on the same node, i.e., it does not copy
# data to its local directory).
pathnames( s2, >> "fpstack" )
# Remove the saturated/bad pixel masks from the input list
match( "_stk.fits", ifile, print-, stop+, >> "spksetup2.tmp" )
# Update the original ifile
delete( ifile )
rename( "spksetup2.tmp", ifile )

delete( "spksetup?.tmp" )

logout 1
