#!/bin/env pipecl
#
# SPKSPISTART

int	status = 1
string	datadir, dataset, indir, ilist, lfile, olist
string  temp1, temp2, listoflists
struct  *list2

# Tasks and packages.
images
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set the dataset name and uparm.
names( "spk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
olist = "spkolist.tmp"
ilist = indir // dataset // ".spk"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSPKSPISTART (%s):\n", dataset )

# Go to the relevant directory
cd( datadir )

# Create the list of lists. This is done based on olist, not ilist,
# because olist contains only images with a WCS. Processing in the
# SPI pipeline requires a WCS solution for transient detection
# and resampling.
listoflists = indir//dataset//"spi.lol"
list = olist
while ( fscan( list, s1 ) != EOF ) {
    s2 = substr( s1, strldx("/",s1)+1, strlstr("_ss2",s1)-1 )
    s2 = substr( s2, 1, stridx("_",s2)-1 ) //
         substr( s2, stridx("_",s2)+1, 999 )
    print( s1, >> s2//".tmp" )
    # Add the name of the first pass stack, written to fpstack by
    # spksetup.cl.
    concat( "fpstack", >> s2//".tmp" )
    print( s2//".tmp", >> "spkspistart1.tmp" )
}
list = ""
pathnames( "@spkspistart1.tmp", > listoflists )

# Call the SPI pipelines 
temp1 = "spkspistart2.tmp"
temp2 = "spkspistart3.tmp"
callpipe( "spk", "spi", "split", listoflists, temp1, temp2 )
concat( temp2 ) | scan( status )
concat( temp1 )
printf( "Callpipe returned %d\n", status )

delete( "spkspistart?.tmp" )

logout( status )
