#!/bin/env pipecl
#
# MTDSTATS

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ifile, im, lfile, maskname, shortname

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
skynames( envget("OSF_DATASET") )

# Set paths and files.
dataset = skynames.dataset
datadir = skynames.datadir
lfile = datadir // skynames.lfile
indir = skynames.indir
ifile = indir // dataset // ".sky"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
        caldir = caldir // subdir

set (uparm = skynames.uparm)
set (pipedata = skynames.pipedata)

cd (datadir)

# Log start of processing.
printf( "\nSKYSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Check if input files are accessible.
if (defvar( "PIPEDEBUG" )) {
    type( ifile )
    list = ifile

    while( fscan( list, s1 ) != EOF ) {
        if ( !imaccess( s1 ) ) {
            sendmsg ("ERROR", "Can't access image", s1, "CAL")
            status = 0
        }
        ;
    }
    list = ""
    if (status == 0)
        logout (status)
    ;
}
;

# Setup uparm and pipedata
delete( substr( skynames.uparm, 1, strlen(skynames.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

# Get the calibrations.
list = ifile
while (fscan (list, s1) != EOF) {
    shortname = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    skynames( shortname )

    # Retrieve the bad pixel mask
    getcal (s1, "bpm", cm, caldir,
        obstype="!obstype", detector="!instrume", imageid="!detector",
        filter="", exptime="", mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
        sendmsg( "ERROR", "Getcal failed for bpm: "//getcal.statstr,
            shortname, "CAL" )
        status = 0
        break
    }
    ;

    # Retrieve the cumulative pixel mask
    # Read keywords needed needed for the getcal
    hsel( s1, "OBSID", yes ) | scan( s3 )
    # Retrieve the location of the cumulative mask
    getcal( s1, "masks", cm, caldir, imageid="!detector", mjd="!mjd-obs",
        match="%"//s3, obstype="", detector="", filter="" )
    i = 0
    while ( (getcal.statcode>0) && (i<=5) ) {
	# Try again a bit later
	print "Retrying..."
	sleep( 300 ) 
        getcal( s1, "masks", cm, caldir, imageid="!detector", mjd="!mjd-obs",
            match="%"//s3, obstype="", detector="", filter="" )
        i = i+1
    }
    if (getcal.statcode>0) {
        if (sciencepipeline) {
            sendmsg( "ERROR", "Getcal failed for mask: "//getcal.statstr,
	        shortname, "PROC" )
            status = 0
            break
        } else {
            sendmsg( "WARNING", "Getcal failed for mask: "//getcal.statstr,
	        shortname, "PROC" )
            # There is no saturation mask available for some reason,
            # but processing should continue in the QRP. Use the
            # standard BPM instead.
            hselect( s1, "BPM", yes ) | scan( s2 )
            maskname = substr( s2, strldx("/",s2)+1, 999 )
            imcopy( s2, maskname )
            hedit( s1, "CBPM", maskname, add+, update+, ver- )
        }
    } else {
        # Extract the file name from the full path
        maskname = substr( getcal.value, strldx("/",getcal.value)+1, 999 )
        # Copy the cumulative mask to here ...
        imcopy( getcal.value, maskname )
        # ... and add it to the header
        hedit( s1, "CBPM", maskname, add+, update+, ver- )
    }

    # Write the full path of the cumulative bad pixel mask to file
    # for later modules to use. This eliminates the need for downstream
    # getcals.
    print( getcal.value, >> skynames.cbpmloc )

}
list = ""




logout 1
