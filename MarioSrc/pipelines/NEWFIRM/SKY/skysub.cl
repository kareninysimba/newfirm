#!/bin/env pipecl
#
# SKYSUB - second pass sky subtraction

int	status = 1
int	offsetsky, num, runmedwindow, nimages, nimgs
int	objectpos, imageid, pointpos, ir, ix, iy, rad
real	stkscale, skymode, skymode2, mjd, exptime, seqno
string	beststk, obstype, ut, dataset, indir, lfile, datadir
string  match, ilist, sortedlist, cast, ssubinfo
string  prevout, previn, nocmpat, nocdpat, observat

# Tasks and packages
redefine mscred=mscred$mscred.cl
images
tv
lists
proto
imfilter
dataqual
servers
noao
nproto
mscred
cache mscred
task $findmatch = "$!findmatch $1 $2"
task $paste = "$!paste -d\  $1 $2 > $3"

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"

# Set file and path names.
skynames( envget("OSF_DATASET") )
dataset = skynames.dataset

# Set filenames.
indir = skynames.indir
datadir = skynames.datadir
ilist = indir // dataset // ".sky"
lfile = datadir // skynames.lfile
set (uparm = skynames.uparm)
set (pipedata = skynames.pipedata)

# Log start of processing.
printf( "\nSKYSUB2 (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Clean up during redo.
# Note that *.fits was removed from the delete below because the
# previous stage may now create .fits files that are needed here.
delete ("*holes.pl,*.tmp")

# Keep original ilist to make reprocessing possible
if ( access( "rerun" ) ) {
    delete( ilist )
    copy( "rerun", ilist )
} else
    copy( ilist, "rerun" )

# Make sure the list is properly sorted
sortedlist = "sortedlist"
# The following line is only for historical reasons (when the
# ilist contained two entries per line - this also affected 
# the number of columns in the sort below).
fields( ilist, "1", >> "skysub1.tmp" )
hsel( "@skysub1.tmp", "$I,NOCID", yes, >> "skysub2.tmp" )
# Paste the input list and the file just created into one
# file. osfn converts ilist into a format readable by the OS
paste( osfn(ilist), "skysub2.tmp", "skysub3.tmp" )
# Sort by the third column, containing NOCID
sort( "skysub3.tmp", column=3, numeric=yes, >> "skysub4.tmp" )
# Extract the first two columns
fields( "skysub4.tmp", "1-2", > sortedlist )
# Store the sorted list in the original input list
delete( ilist )
rename( sortedlist, ilist )
# Clean up
delete( "skysub*.tmp" )

# Go through the input list and calculate the mode for all images if
# not previously determined. Also merge bad pixel mask with persistence
# mask.
list = ilist
count( ilist ) | scan( num )
offsetsky = 0
i = 0
while ( fscan( list, s1, s2 ) != EOF ) {

    # Inform user about the progress (mostly useful when run
    # from the command line)
    i = i+1
    printf( "Processing %d out of %d.\n", i, num )
    printf( "Image name: %s\n", s1 )

    # Read the obstype from the header, increase offsetsky
    # counter if obstype==sky
    obstype = ""
    hsel( s1, "OBSTYPE", yes ) | scan( obstype )
    if ( obstype == "sky" ) {
        offsetsky = offsetsky+1
    }

    # Set the output name for this exposure
    s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    skynames( s3 )

    # Delete the mask if it already exists
    if ( imaccess( skynames.mask ) ) 
        imdel( skynames.mask )
    ;

    # Calculate the mode, masking out bad pixels
    mimstat( s1, fields="mode", nclip=3, lsigma=2, 
        usigma=2, format-, imasks="!CBPM" ) | scan( skymode )
    # Store the mode in the header
    hedit( s1, "SKYMODE", skymode, add+, ver- )

    # Create a short version of the dataset name
    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    # Get the MJD from the header
    hsel( s1, "MJD-OBS,EXPTIME", yes ) | scan( mjd, exptime )
    # Store the derived skymode in PMAS

    newDataProduct( s2, mjd, "objectimage", "" )
    storekeywords( class="objectimage", id=s1, sid=s2, dm=dm )
    printf("%12.5e\n", skymode/exptime ) | scan( cast )
    setkeyval( class="objectimage", id=s2, dm=dm, keyword="dqskval",
        value=cast )

}
list = ""

# Read NOCMPAT, NOCDPAT, and IMAGEID from the last exposure in the
# sequence. These keywords should be the same for the entire sequence.
nocmpat = "" ; hsel( s1, "NOCMPAT", yes ) | scan( nocmpat )
nocdpat = "" ; hsel( s1, "NOCDPAT", yes ) | scan( nocdpat )
imageid = 0 ; hsel( s1, "IMAGEID", yes ) | scan( imageid )
observat = ""; hsel( s1, "OBSERVAT", yes ) | scan( observat )

if ( nocdpat == "Q" ) 
    nocdpat = "4Q"
;
if ( nocmpat == "Q" ) 
    nocmpat = "4Q"
;

# If NOCMPAT or NOCDPAT=Q|4Q (four-shooter mode), ignore offset sky exposures.
if ( ( nocmpat == "4Q" ) || ( nocdpat == "4Q" ) ) {
    offsetsky = 0
}
;

ssubinfo = "Unknown"
if ( offsetsky == 0 ) {
    # Do sky subtraction by one-pass, in-field dithering. 
    print( "ALL OBJECTS: SKY SUBTRACTION WITH RUNMED (i.e., in-field dithering)" )
    match = "object"
    ssubinfo = "In-field dithering with running median filtering"
} else if ( offsetsky == num ) {
    # Treat skys as objects.
    print( "ALL SKIES: SKY SUBTRACTION WITH RUNMED (i.e., in-field dithering)" )
    match = "object"
    offsetsky = 0
    ssubinfo = "In-field dithering with running median filtering"
} else {
    # Use offset exposures for sky subtraction.
    print( "SKY SUBTRACTION WITH OFFSET SKY EXPOSURES" )
    match = "sky"
    ssubinfo = "Dithered offset sky with running median filtering"
}
;

if ( access( "skymode.tmp" ) )
    delete( "skymode.tmp" )
;
 
# First, separate object and sky exposures.
list = ilist
touch ("stksub1.tmp,skymode.tmp")
while( fscan( list, s1, s2 ) != EOF ) {

    # Set the output file names for this exposure
    s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    skynames( s3 )

    # Read the obstype from the header if there is a mixture.
    obstype = ""
    if (match == "object")
        obstype = "object"
    else
	hsel( s1, "OBSTYPE", yes ) | scan( obstype )

    if ( obstype == match ) {
        print( s1, >> "stksub1.tmp" )
        # Delete the output files if they already exists
        if ( imaccess( skynames.sky ) )
            imdel( skynames.sky )
        ;

	# Write the output file names to their lists	
	print( skynames.sky, >> "stksub3.tmp" ) # sky images
	# Write the output file names to their lists	
	print( skynames.holes//".pl", >> "stksub4.tmp" ) # sky images
        # Write the inverse of the mode to file for
        # use as scaling by runmed
        skymode = INDEF ; hsel( s1, "SKYMODE", yes ) | scan( skymode )
	if (isindef(skymode)) {
            printf( "ERROR: could not retrieve skymode for %s\n", s1 )
            status = 2
        }
	;
        x = 1/skymode
	print( x, >> "skymode.tmp" )
    }
    ;

    if ( obstype == "object" ) {
	print( s1, >> "stksub2.tmp" )
    } else {
	if ( obstype == "sky" )
            ;
        else {
	    print( "ERROR: Unknown value for obstype" )
            status = 3
	}
        ;
    }

}
list = ""

# If NOCMPAT=Q (four-shooter mode), then the exposures without the
# extended source in them become "offset" exposures, and the ones with
# the extended sky in them become the obejct exposures. Note that any
# true "offset" exposures have been removed from the list.
if ( ( nocmpat == "4Q" ) || ( nocdpat == "4Q" ) ) {
    ssubinfo = "4Q sky subtraction"

    if ( observat == 'CTIO' ) {

        # Extended object moves through the extensions as follows:
        # first im1, then im2, then im4, and finally im3.
        if ( imageid == 3 )
	    objectpos = 4
        else 
	    if ( imageid == 4 )
	        objectpos = 3
    	    else
	        objectpos = imageid

    } else {

        # Extended object moves through the extensions as follows:
        # first im2, then im1, then im3, and finally im4.
        if ( imageid == 1 )
            objectpos = 2
        else 
            if ( imageid == 2 )
                objectpos = 1
            else
                objectpos = imageid
    }
 
    # Ensure all the lists exist
    touch( "_stksub1.tmp" )
    touch( "_stksub2.tmp" )
    touch( "_stksub3.tmp" )
    touch( "_stksub4.tmp" )

    # Loop over the list of all object exposures
    list = "stksub2.tmp"
    while ( fscan( list, s1 ) != EOF ) {

        # Determine whether the extended source is in this exposure
        # First read nocmpos/nocdpos, which indicate the position of the
        # current exposure within the mapping pattern.
        if ( nocmpat == "4Q" ) {
            pointpos = 0 ; hsel( s1, "NOCMPOS", yes ) | scan( pointpos )
        } else {
            pointpos = 0 ; hsel( s1, "NOCDPOS", yes ) | scan( pointpos )
        }
        if ( pointpos == objectpos ) {

            # This exposure has an extended object, so add it to
            # the object list.
            print( s1, >> "_stksub2.tmp" )
            hedit( s1, "FSOBJECT", "yes", add+, ver- )

            # The skylevel is redetermined, because the value
            # derived above was based on the entire image. In 4Q mode,
            # there may be a large object present, which would bias
            # the sky level to higher values, leading to too low
            # sky levels in the sky-subtracted data.

            # Get the size of the sky image
            hsel( s1, "NAXIS1,NAXIS2", yes ) | scan( ix, iy )
            # Determine the radius of the circle in pixels
            ir = ix
            if ( iy < ir )
                ir = iy
            ;
            # Divide by 2 (diameter to radius) and 100 (percent to fraction)
            rad = ir*sky_4qradius/200
            ix = ix/2
            iy = iy/2
            if ( access( "skysub1.inp" ) )
                delete( "skysub1.inp" )
            ;
            printf( "%d %d 0 e\n", ix, iy, > "skysub1.inp" )
            # Create a mask with good values outside of this circle
            # centered on the middle of the image. First get the
            # existing mask
            hsel( s1, "CBPM", yes ) | scan( s2 )
            if ( imaccess( "tmpmask.pl" ) )
                imdel( "tmpmask.pl" )
            ;
            # Mark the circle as bad
            imedit( s2, "tmpmask.pl", cursor="skysub1.inp", radius=rad,
                value=1, display- )
            delete( "skysub1.inp" )
            # Calculate the mode, masking out bad pixels
            mimstat( s1, fields="mode", nclip=3, lsigma=2, 
                usigma=2, format-, imasks="tmpmask.pl" ) | scan( skymode )
            imdel( "tmpmask.pl" )
            # Update the mode in the header
            hedit( s1, "SKYMODE", skymode, update+, ver- )
 
            # Update the sky value in PMAS
            # Create a short version of the dataset name
            s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
            # Get the MJD from the header
            hsel( s1, "MJD-OBS,EXPTIME", yes ) | scan( mjd, exptime )
            # Store the derived skymode in PMAS
            # TODO: check whether value is updated or ignored
            printf("%12.5e\n", skymode/exptime ) | scan( cast )
            setkeyval( class="objectimage", id=s2, dm=dm, keyword="dqskval",
                value=cast )

        } else {
            # This exposure does not have the extended object, so add
            # it to the runmed list, and the output filename list.
            print( s1,  >> "_stksub1.tmp" )
            # Set the output file names for this exposure
            s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
            skynames( s3 )
            # Write the output file names to their lists	
            print( skynames.sky, >> "_stksub3.tmp" )
            # Write the hole mask file names to their lists	
            print( skynames.holes//".pl", >> "_stksub4.tmp" )
            # Write the inverse of the mode to file for
            # use as scaling by runmed
            skymode = INDEF ; hsel( s1, "SKYMODE", yes ) | scan( skymode )
            x = 1/skymode
	    print( x, >> "_skymode.tmp" )
            # The sky should also be subtracted from the extensions
            # without the extended object, so add it to the object
            # list as well.
            print( s1, >> "_stksub2.tmp" )
            hedit( s1, "FSOBJECT", "no", add+, ver- )
        }

    }
    list = ""

    # Reset the lists to newly created ones
    delete( "stksub1.tmp,stksub2.tmp,stksub3.tmp,stksub4.tmp,skymode.tmp" )
    rename( "_stksub1.tmp", "stksub1.tmp" )
    rename( "_stksub2.tmp", "stksub2.tmp" )
    rename( "_stksub3.tmp", "stksub3.tmp" )
    rename( "_stksub4.tmp", "stksub4.tmp" )
    rename( "_skymode.tmp", "skymode.tmp" )

}
;

# Get the number of images
nimages = 0; count( "stksub1.tmp" ) | scan( nimages )

print( nimages )
print( sky_minrunmedwin )
print( offsetsky )
# Check whether enough images are available, for the case of in-field
# dithering.
if ((nimages<sky_minrunmedwin)&&(offsetsky==0)) {

    if (nimages<2) {

	ssubinfo = "Too few images, no sky subtraction"
        # Sky subtraction cannot be done, but produce output anyway so that
        # the downstream pipelines can continue. A rough sky subtraction
        # will be done in the SWC pipeline where the ace sky image is 
        # subtracted.
        sendmsg( "WARNING",
	    "Too few exposures, cannot do sky subtraction",
    	    "int(nimages)", "VRFY" )
        print( "TOO FEW EXPOSURES (%d), NO SKY SUBTRACTION", nimages )
        list = "stksub1.tmp" #ilist
        if ( fscan( list, s1 ) != EOF ) {
            # Get the part of the filename after the last /, strip the .fits
            s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
            # Set the sky variables for this image
            skynames( s2 )
            # Copy the unskysubtracted image
	    imcopy( s1, skynames.skysub )
        }
        list = ""

    } else {

	ssubinfo = "Few images, using pairwise sky subtraction"
        sendmsg( "WARNING",
	    "Too few images for running median, using pairwise skysubtraction",
    	    "", "VRFY" )
        print( "PAIRWISE SKY SUBTRACTION" )
        list = "stksub1.tmp" #ilist
        for( nimgs=1; fscan(list,s1)!=EOF; nimgs+=1 ) {
            # Get part of the filename after the last /, stripping the .fits
            s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
            # Set the sky variables for this image
            skynames( s2 )
            # If nimgs>1, i.e., we have a pair, subtract them
            print( nimgs )
            if (nimgs>1) {
	        imarith( previn, "-", s1, prevout )
            }
	    ;
            # Store the output file name for the next item on the list. This
            # has to be done here so that prevout is set to the current item
            # for the "reverse" subtraction for the last item in the list.
            prevout = skynames.skysub
            # Do a reverse subtraction for the last image in the list
            if (nimgs==nimages) {
                imarith( s1, "-", previn, prevout )
            }
	    ;
            # Store the current input file name to create the second half
            # of the pair for the next item on the list.
            previn = s1
        }
        list = ""
        delete( "skysub1.tmp,skysub2.tmp" )

    }

    logout 1

}
;

# Do the sky subtraction for offset sky exposures and there are too
# few images for runmed
if ((nimages<sky_minrunmedwin)&&(offsetsky>0)) {

    if (nimages<2) {

	ssubinfo = "Too few images, no sky subtraction"
        # With only one image, the image itself will have to be the
        # sky image.
        imcopy( "@stksub1.tmp", "@stksub3.tmp" )

    } else {

	ssubinfo = "Offset sky from minmax rejection"
        # Only a few images are available, combine them to create
        # a somewhat source-free image.
	# Use the name of the first image in the list as the output
        # file name.
        head( "stksub3.tmp", nlines=1 ) | scan( s1 )
        # Set the output name for this exposure
        s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
        skynames( s2 )
        imcombine( "@stksub1.tmp", skynames.sky, imcmb="$I",
            logfile="STDOUT", combine="average", reject="minmax", 
            offsets="none", weight="!oexptime", expname="oexptime",
            nlow=0, nhigh=1, nkeep=1, grow=1. )
        delete( "stksub3.tmp" )
        printf( "%s\n", skynames.sky, >> "stksub3.tmp" )

    }

}
;

# Limit size of runmedwindow
runmedwindow = nimages
if (runmedwindow>sky_maxrunmedwin)
    runmedwindow = sky_maxrunmedwin
;

if (nimages>=sky_minrunmedwin) {
    # Create the sky images from the offset exposures. Note that the
    # mask is not applied here because the "fixpixed" bad pixels should
    # also be sky subtracted.
    runmed( "@stksub1.tmp", "@stksub3.tmp", window=runmedwindow,
        inmaskkey="CBPM", scale="@skymode.tmp", outtype="filter",
        nclip=2., exclude+, outscale-, normscale-,
        masks="@stksub4.tmp", outmaskkey="HOLES" )
}
;

# Calculate the mode in the newly created sky images
list = "stksub3.tmp"
while( fscan( list, s1 ) != EOF ) {
    # Calculate the mode, masking out bad pixels in WCS space
    mimstat( s1, fields="mode", nclip=3, lsigma=2, 
        usigma=2, format-, imasks="!CBPM" ) | scan( skymode )
    # Store the mode in the header
    hedit( s1, "SKYMODE", skymode, add+, ver- )
    }
list = ""

# Create a list with UTs of the offset sky exposures.
# This is used below to find the best matching sky exposure.
# Exclude sky images with undefined sky modes.
hselect( "@stksub3.tmp", "$I,NOCID,SKYMODE", yes ) | match( "INDEF", stop+ ) | 
    fields( "STDIN", "1-2", >> "stksub5.tmp" )
count( "stksub5.tmp" ) | scan( i )
if ( i == 0 ) {
    sendmsg( "ERROR", "No valid sky images left", "", "PROC" )
    logout( 0 )
}
;

# Now loop over all object and sky exposures and subtract the 
# relevant sky image.
# First, create the merged list
concat( "stksub1.tmp", >> "stksub6.tmp" )
concat( "stksub2.tmp", >> "stksub6.tmp" )
list = "stksub6.tmp"
while( fscan( list, s1 ) != EOF ) {

    # Set the output file names for this exposure
    s3 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    skynames( s3 )

    # Retrieve sequence number from the header ...
    hselect( s1, "NOCID", yes ) | scan( seqno )
    # ... and find the sky closest to the current sequence number
    findmatch( "stksub5.tmp", seqno ) | scan( beststk )

    # Retrieve the sky level in the object and sky images
    # from the headers
    hselect( s1, "SKYMODE", yes ) | scan( skymode )
    hselect( beststk, "SKYMODE", yes ) | scan( skymode2 )

    # Scale the sky image to match the mode of the object image
    stkscale = skymode/skymode2
    imarith( beststk, "*", stkscale, "tmpscaledsky" )
    # Delete the output sky subtracted file if it already exists
    if ( imaccess( skynames.skysub ) ) {
        imdel( skynames.skysub )
    }
    ;
    # Subtract the selected sky image from the current image
    imarith( s1, "-", "tmpscaledsky", skynames.skysub )
    printf( "Subtracted %f times %s from %s\n", stkscale, beststk, s1 )
    # Copy the "holes" mask from the sky image to the sky subtracted
    # image
    hselect( beststk, "HOLES", yes ) | scan( s3 )
    if (nscan()==1)
        hedit( skynames.skysub, "HOLES", s3, add+, ver- )
    ;
    hedit( skynames.skysub, "SSUBINFO", ssubinfo, add+, ver-, show+ )
#    hselect( skynames.skysub, "CBPM", yes ) | scan( s3 )
#    fixpix( skynames.skysub, s3 )
    # Clean up
    imdel( "tmpscaledsky" )

}

if (status>1) {
    printf( "An error occurred, the exit code is %i.\n", status )
}
;

# Clean up sky images
imdel( "@stksub3.tmp" )

logout( 1 )
