procedure parseresbck( pars )

string pars {prompt="Parameters for residual sky subtraction"}

begin

    string p1,p2,p3
    string o1,o2,o3
    string prs

    prs = pars

    if ( ( prs == "" ) || ( prs == "default" ) ) {
        prs = "poly4,poly1,none"
    }
    ;

    p2 = ""; p3 = ""
    print( prs ) | translit( "STDIN", ",", " " ) | scan( p1, p2, p3 )
    if ( ( p2 == "" ) || ( p2 == "default" ) )
        p2 = "poly1"
    ;
    if ( ( p3 == "" ) || ( p3 == "default" ) )
        p3 = "none"
    ;

    o1 = "4"; o2 = "1"; o3 = "1"
    if ( strstr("poly",p1)>0 ) {
        o1 = substr( p1, 5, 999 )
        if ( o1 == "" )
       	    o1 = 4
	;
        p1 = "poly"
    }
    ;
    if ( strstr("poly",p2)>0 ) {
        o2 = substr( p2, 5, 999 )
        if ( o2 == "" )
       	    o2 = 1
	;
        p2 = "poly"
    }
    ;
    if ( strstr("poly",p3)>0 ) {
        o3 = substr( p3, 5, 999 )
        if ( o3 == "" )
       	    o3 = 1
	;
        p3 = "poly"
    }
    ;

    printf( "%s %s %s %s %s %s\n", p1, o1, p2, o2, p3, o3 )

end
