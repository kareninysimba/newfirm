#!/bin/env pipecl

int     namps
string  datadir, dataset, gcldir, ilist, image, indir, lfile

# Tasks and packages
images

# Set file and path names.
skynames (envget("OSF_DATASET"))
dataset = skynames.dataset

# Set filenames.
indir = skynames.indir
datadir = skynames.datadir
ilist = indir // dataset // ".sky"
lfile = datadir // skynames.lfile
set (uparm = skynames.uparm)
set (pipedata = skynames.pipedata)

# Log start of processing.
printf ("\nSKYSSKSTART (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Create the list of files to work on from the output of the SGC 
# pipeline, so we look for .sgc files in the relevant $NEWFIRM_GCL/data
# directory.
# Construct the GCL path
gcldir = "NEWFIRM_GCL$/data/" // skynames.parent // "-gcl/"

match( ".fits", gcldir//"*.sgc", print- )

# first-gossimnirj1-skyim1
# first-gossimnirj1-gcl
print( skynames.parent )
print( gcldir )

logout 1

list = ilist
for (namps=1; fscan(list,s1)!=EOF; namps+=1) {
    print( s1 )
    s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    skynames (s2)
    image = skynames.image
    imcopy( s1, image )
}


logout 1
