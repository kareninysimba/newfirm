#!/bin/env pipecl
#
# SKYREMRINGS

int     status = 1
int     xcen, ycen
string  dataset, indir, datadir, ilist, lfile, image, olist, area
real	skymode

# Tasks and packages
images
servers
dataqual
proto

# get the dataset name
skynames( envget("OSF_DATASET") )

# Set paths and files.
indir = skynames.indir
dataset = skynames.dataset
datadir = skynames.datadir
lfile = datadir // skynames.lfile
ilist = indir // dataset // ".sky"
set (uparm = skynames.uparm)
set (pipedata = skynames.pipedata)

cd( datadir )

# Log start of processing.
printf( "\nSKYREMRINGS (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# In this module, the files are copied from their remote location (as
# given in the original input list) to here. To make sure skysub.cl
# uses the correct files, a new one with the local paths is created,
# and written over the original input list.
olist = "skyremrings1.tmp"

if ( ring_removal == yes ) {

    # Get first file from list to determine which array is being used
    head( ilist ) | scan( s1 )
    hselect( s1, "IMAGEID", yes ) | scan( i )
    if ( i == 1 ) {
        xcen = 2046; ycen = 2046;
	area = "[1024:2046,1024:2046]"
    } else if ( i == 2 ) {
        xcen = 0; ycen = 2046;
	area = "[1:1023,1024:2046]"
    } else if ( i == 3 ) {
        xcen = 2046; ycen = 0;
	area = "[1024:2046,1:1023]"
    } else {
        xcen = 0; ycen = 0;
	area = "[1:1023,1:1023]"
    }

    list = ilist
    while ( fscan( list, s1 ) != EOF ) {

        # Set the output name for this exposure
        s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
	skynames( s2 )
	# Copy file locally
	image = skynames.image
	imcopy( s1, image )
	# Print full IRAF path to olist
	pathnames( image//".fits", >> olist )

	# Get cumulative pixel mask from header
        hselect( image, "CBPM", yes ) | scan( s2 )

imcopy( image, image//"_ringsub" )

	# Calculate the mode, masking out bad pixels, in the
        # approximate are where the rings are not present (or at
	# least not strong)
        mimstat( s1//area, fields="mode", nclip=3, lsigma=2, 
            usigma=2, format-, imasks=s2//area ) | scan( skymode )

	# Perform ring removal
        removerings( skynames.image, mask=s2,
            xcenter=xcen, ycenter=ycen, nrings=500, inner=50,
	    skymode=skymode, niter=3, low=3, high=3, pclip=0.5 )

    }
    list = ""

    # Write the new list over the original input list
    delete( ilist )
    rename( olist, ilist )

}
;

dir

logout( status )
