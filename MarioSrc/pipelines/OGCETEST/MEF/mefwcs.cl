#!/bin/env pipecl
#
# MEFWCS -- Update the WCS.

int	status = 1
int	nsets
real    mjd, axs, ays, axr, ayr, x1, x2, x3, x4
string	dataset, lfile, tlfile, procid, cast, proj
string	cat, sifdataset, sifdir, sifcat, sifmcat, sifwcsdb, sifshort, shortdir

# Tasks and packages.
utilities
images
imcoords
mario
tables
ttools
servers
dataqual

# Set dataset directory and logfile.
mefnames (envget ("OSF_DATASET"))
dataset = mefnames.dataset
cat = mefnames.shortname // "_usno"
set (uparm = mefnames.uparm)
set (pipedata = mefnames.pipedata)
lfile = mefnames.lfile
tlfile = "mefwcs.tmp"
cd (mefnames.datadir)

task $mefwcs_usnomatch_proc = "$mefwcs_usnomatch_proc.py $1"
task $mefwcs_usnomap_proc = "$mefwcs_usnomap_proc.py $1"
task $mefwcs_ccmap_proc = "$mefwcs_ccmap_proc.py $1"

# Log start of processing.
printf ("\nMEFWCS (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Insert new data product.
# Put in datamanager PMASS.
#set (NHPPS_NODE_DM=envget("NHPPS_NODE_CM"))
#set (NHPPS_PORT_DM=envget("NHPPS_PORT_CM"))
hsel( mefnames.hdr, "mjd-obs", yes ) | scan( mjd )
newdataproduct( mefnames.shortname, observedMJD=mjd, class="mefobjectimage")
storekeywords( class="mefobjectimage", id=mefnames.hdr,
    sid=mefnames.shortname, dm=dm )

# Set up processing list files.
files ("*.ace", > "mefwcs.tmp")
list = "mefwcs.tmp"
while (fscan (list, s1) != EOF) {
    sifdataset = substr (s1, 1, strstr(".ace",s1)-1)
    sifnames (sifdataset)
    sifcat = ""; match (sifnames.cat, s1) | scan (sifcat)
    if (sifcat == "")
        next
    ;
    sifdir = substr (sifcat, 1, strldx("/",sifcat))
    print (sifcat, >> "mefwcscats.tmp")
    print (sifnames.mcat, >> "mefwcsmcats.tmp")
    printf ("%s %s %s %s\n", sifdir, sifnames.mcat, sifnames.wcsdb, sifnames.shortname,
        >> "mefwcsdb.tmp")
}
list = ""; delete ("mefwcs.tmp")

# Match the catalogs against the reference catalog.
if (access(cat)) {
    usnomatch ("@mefwcscats.tmp", cat, matchcat="@mefwcsmcats.tmp",
	imcatdef="@pipedata$catmatch.def", fracmatch=mef_fracmatch,
	logfile=tlfile) |& scan (line)
    concatenate (tlfile) | tee (lfile)
} else
    printf ("Warning: No reference catalog (%s)\n", cat) | scan (line)
if (substr (line, 1, 7) == "Warning") {
    sendmsg ("ERROR", substr(line,9,1000), "", "PROC")
    printf ("ERROR: %s\n", substr(line,9,1000), >> lfile)
    status = 2
} else {
    # Merge matched catalogs.
    mefmerge ("@mefwcsmcats.tmp", mefnames.mcat, catdef="",
        filter="G<22.", append+, verbose+)

    mefwcs_usnomatch_proc( tlfile )
    cl < mefwcs_usnomatch_proc.cl
    delete ("mefwcs_usnomatch_proc.cl")
}
delete (tlfile)

# Compute a global correction and add a constraining grid.
flpr
if (status == 1) {
    if (mef_wcsglobal)
	usnomap ("@mefwcsmcats.tmp", logfile=tlfile) |& scan (line)
    else
	usnomap ("@mefwcsmcats.tmp", ngrid=9, logfile=tlfile) |& scan (line)
    concatenate (tlfile) | tee (lfile)
    if (substr (line, 1, 7) == "Warning") {
        sendmsg ("ERROR", substr(line,9,1000), "", "PROC")
	printf ("ERROR: %s\n", substr(line,9,1000), >> lfile)
	status = 2
    } else {
        mefwcs_usnomap_proc( tlfile )
        cl < mefwcs_usnomap_proc.cl
	delete ("mefwcs_usnomap_proc.cl")
    }
    delete (tlfile)
}
;

# Do WCS solutions.  Get the shifted tangent point from the logfile.
if (status == 1) {
    axr = 0
    ayr = 0
    axs = 0
    ays = 0
    nsets = 0
    match ("new tangent point", lfile) | translit ("STDIN", "(,)", del+) |
	scan (s1, s1, s1, s1, s1, s2)
    x = real (s1); y = real (s2)
    hedit (mefnames.hdr, "OLDRA", "(RA)", add+, show-, >> lfile)
    hedit (mefnames.hdr, "OLDDEC", "(DEC)", add+, show-, >> lfile)
    hedit (mefnames.hdr, "RA", s1, show+, >> lfile)
    hedit (mefnames.hdr, "DEC", s2, show+, >> lfile)

    list = "mefwcsdb.tmp"
    nsets = 0
    for (i=1; fscan(list,sifdir,sifmcat,sifwcsdb,sifshort)!=EOF; i+=1) {
        if (i == 1) {
	    # Set projection info from WCSDB file.
	    proj = ""
	    s1 = ""; match ("WCSDB", sifcat) | scan (s2, s2, s2, s1)
	    if (s1 != "") {
		match ("projection", s1) | scan (s2, s3)
		if (s3 == "zpx") {
		    proj = "zpx"
		    print (proj, > "mefwcsproj.tmp")
		    match ("projp", s1) | sort | uniq (>> "mefwcsproj.tmp")
		}
		;
	    }
	    ;
	}
	;
        if (mef_wcsglobal) {
	    match ("INDEF", sifmcat, > "mefwcsgrid.tmp")
	    if (proj == "zpx")
		ccmap ("mefwcsgrid.tmp", sifwcsdb, solution="wcs",
		    lngref=x, latref=y, projection="mefwcsproj.tmp",
		    xxorder=3, xyorder=3, yxorder=3, yyorder=3,
		    xxterms="half", yxterms="half", > tlfile)
	    else
		ccmap ("mefwcsgrid.tmp", sifwcsdb, solution="wcs",
		    lngref=x, latref=y, > tlfile)
	    delete ("mefwcsgrid.tmp")
	} else {
	    if (proj == "zpx")
		ccmap (sifmcat, sifwcsdb, solution="wcs",
		    lngref=x, latref=y, projection="mefwcsproj.tmp",
		    xxorder=3, xyorder=3, yxorder=3, yyorder=3,
		    xxterms="half", yxterms="half", > tlfile)
	    else
		ccmap (sifmcat, sifwcsdb, solution="wcs",
		    lngref=x, latref=y, > tlfile)
	}

	concatenate (tlfile) | tee (lfile)
	# Redirect input parameters for mefwcs_ccmap_proc to file.
	# mefwcs_ccmap_proc will read the parameters from file.
        # This is a way to avoid escaping the "!" in sifdir.
        printf("%s %s %s\n", tlfile, sifdir, sifshort, >> "mefwcs.inp" )
        mefwcs_ccmap_proc( "mefwcs.inp" )
        delete( "mefwcs.inp" )
        cl < mefwcs_ccmap_proc.cl
	delete ("mefwcs_ccmap_proc.cl")
	# Get the scale and the rms from the header to calculate the 
	# average over all extensions
	s1 = sifdir // sifshort
	hselect( s1, "dqwcccxr,dqwcccyr,dqwcccxs,dqwcccys", yes ) |
	    scan( x1, x2, x3, x4 )
	if (nscan()==4) {
	    axr = axr+x1
	    ayr = ayr+x2
	    axs = axs+x3
	    ays = ays+x4
            nsets = nsets+1
        }
	;
    }
    list = ""
    delete (tlfile)
    if (nsets>0) {
        axr = axr/nsets
        ayr = ayr/nsets
        axs = axs/nsets
        ays = ays/nsets
	x1 = (axs + ays) / 2

	# Update global header.
	hedit( mefnames.hdr, "PIXSCALE", x1, add+, show+, >> lfile)
	hedit( mefnames.hdr, "PIXSCAL1", axs, add+, show+, >> lfile)
	hedit( mefnames.hdr, "PIXSCAL2", ays, add+, show+, >> lfile)
	hedit( mefnames.hdr, "WCSXRMS", axr, add+, show+, >> lfile)
	hedit( mefnames.hdr, "WCSYRMS", ayr, add+, show+, >> lfile)

	# Update SIF headers.
	print ("PIXSCALE ", x1, >> "mefgwcs.hdr")
	print ("PIXSCAL1 ", axs, >> "mefgwcs.hdr")
	print ("PIXSCAL2 ", axs, >> "mefgwcs.hdr")
	print ("WCSXRMS ", axr, >> "mefgwcs.hdr")
	print ("WCSYRMS ", ayr, >> "mefgwcs.hdr")

	# Update PMAS
	printf("%12.5e\n", axr ) | scan( cast )
        setkeyval( dm, "mefobjectimage", mefnames.shortname, "dqwccaxr", cast )
	printf("%12.5e\n", ayr ) | scan( cast )
        setkeyval( dm, "mefobjectimage", mefnames.shortname, "dqwccayr", cast )
	printf("%12.5e\n", axs ) | scan( cast )
        setkeyval( dm, "mefobjectimage", mefnames.shortname, "dqwccaxs", cast )
	printf("%12.5e\n", ays ) | scan( cast )
        setkeyval( dm, "mefobjectimage", mefnames.shortname, "dqwccays", cast )
    } else 
	sendmsg("WARNING", "Could not determine average WCS info", "", "DQ" )
}
;

# Update headers.
if (status == 1) {
    # Trigger results back to SIF pipeline and update global header.
    list = "mefwcsdb.tmp"
    for (i=1; fscan(list,sifdir,sifmcat,sifwcsdb,sifshort)!=EOF; i+=1) {

	# Update global parameters.
        if (i == 1) {
	    x1 = INDEF; thselect (sifmcat, "MAGZERO", yes) | scan (x1)
	    if (isindef(x1)==NO)
		printf ("MAGZERO %g\n", x1, >> "mefgwcs.hdr")
	    ;
	    x2 = INDEF; thselect (sifmcat, "MAGZSIG", yes) | scan (x2)
	    if (isindef(x2)==NO)
		printf ("MAGZSIG %g\n", x2, >> "mefgwcs.hdr")
	    ;
	    x3 = INDEF; thselect (sifmcat, "MAGZERR", yes) | scan (x3)
	    if (isindef(x3)==NO)
		printf ("MAGZERR %g\n", x3, >> "mefgwcs.hdr")
	    ;
	    x4 = INDEF; thselect (sifmcat, "MAGZNAV", yes) | scan (x4)
	    if (isindef(x4)==NO)
		printf ("MAGZNAV %d\n", x4, >> "mefgwcs.hdr")
	    ;

	    if (isindef(x1)==NO)
		hedit (mefnames.hdr, "MAGZERO", x1, add+, show+, >> lfile)
	    ;
	    if (isindef(x2)==NO)
		hedit (mefnames.hdr, "MAGZSIG", x2, add+, show+, >> lfile)
	    ;
	    if (isindef(x3)==NO)
		hedit (mefnames.hdr, "MAGZERR", x3, add+, show+, >> lfile)
	    ;
	    if (isindef(x4)==NO)
		hedit (mefnames.hdr, "MAGZNAV", x4, add+, show+, >> lfile)
	    ;
	    hedit (mefnames.hdr, "WCSCAL", "(1==1)", add+, show+, >> lfile)
	}
	;

	copy ("mefgwcs.hdr", "mefwcs.hdr")
	x1 = INDEF; thselect (sifmcat, "MAGZERO1", yes) | scan (x1)
	if (isindef(x1)==NO)
	    printf ("MAGZERO1 %g\n", x1, >> "mefwcs.hdr")
	;
	x2 = INDEF; thselect (sifmcat, "MAGZSIG1", yes) | scan (x2)
	if (isindef(x2)==NO)
	    printf ("MAGZSIG1 %g\n", x2, >> "mefwcs.hdr")
	;
	x3 = INDEF; thselect (sifmcat, "MAGZERR1", yes) | scan (x3)
	if (isindef(x3)==NO)
	    printf ("MAGZERR1 %g\n", x3, >> "mefwcs.hdr")
	;
	j = INDEF; thselect (sifmcat, "MAGZNAV1", yes) | scan (j)
	if (isindef(j)==NO)
	    printf ("MAGZNAV1 %d\n", j, >> "mefwcs.hdr")
	;

	move ("mefwcs.hdr,"//sifwcsdb, sifdir)
	copy (sifmcat, sifdir)
    }
    list = ""
} else
    hedit (mefnames.hdr, "WCSCAL", "(1==2)", add+, show+, >> lfile)

delete ("mefgwcs.hdr,mefwcs*.tmp")
logout (status)
