#!/bin/env pipecl
#
# MEFGCAT -- Get reference catalogs for WCS and photometric calibration.
#
# The way in which the catalogs are obtain may change with time but this
# routine encapsulates this.  It is run as a separate stage so that it
# can proceed in parallel with other operations.

int	rulepassed, nfound
real	ra, dec, equinox
string	cat, rtnfile, rfile, lfile, rulepath

# Set dataset directory.
mefnames (envget("OSF_DATASET"))
cat = mefnames.shortname // "_usno"
lfile = mefnames.lfile
set (uparm = mefnames.uparm)
cd (mefnames.datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), mefnames.dataset) | tee (lfile)
time | tee (lfile)

# Load packages.
proto
images
noao
servers
task	mefgetcat = NHPPS_PIPEAPPSRC$/MEF/mefgetcat.cl

# Define rules
findrule( cm, "enough_usno_entries", validate=1 ) | scan( rulepath )
task enough_usno_entries = (rulepath)

# Region is centered on telescope RA and DEC.  The catalog is part of the
# parameter set.

s1 = "@" // "OGCETEST_MEF$/input/" // mefnames.dataset // ".mef//\[0]"
ra = INDEF; dec = INDEF; equinox = 2000.
hselect (s1, "TELRA,TELDEC,TELEQUIN", yes) | scan (ra, dec, equinox)
if (isindef(ra) || isindef(dec) || isindef(equinox))
    hselect (s1, "RA,DEC,EQUINOX", yes) | scan (ra, dec, equinox)
;
if (isindef(ra) || isindef(dec) || isindef(equinox)) {
    hselect (s1, "CRVAL1,CRVAL2", yes) | scan (ra, dec)
    ra /= 15
}
;
if (isindef(ra) || isindef(dec) || isindef(equinox)) {
    sendmsg ("ERROR", "Pointing information not found", "", "VRFY")
    logout 0
}
;
mefgetcat (ra, dec, 0:30:00, cat, bmax=23., vmax=23.)

# Check if a reasonable catalog returned.
if (access (cat) == NO) {
    sendmsg ("ERROR", "No USNO match catalog returned", "", "PROC")
    logout 0
} else
    ;
count (cat) | scan (nfound)
enough_usno_entries( nfound ) | scan( rulepassed )
if (rulepassed==0) {
    sendmsg ("ERROR", "Too few entries in USNO match catalog", str(nfound),
        "PROC")
    delete (cat)
    logout 0
}
;

# Log something.
printf ("    Retrieved %d reference objects from USNO-B1.0\n     \
    centered at %.2h %.1h\n", nfound, ra, dec) | tee (lfile)

logout 1
