# PUTPHOT -- Set the photometry data in the data manager.

procedure putphot (input)

file	input = "putphot.dat"	{prompt="Photometry data"}
file	dm = "inet:6050"	{prompt="Data manager port"}

file	imageid = "ampnames.dat" {prompt="Extname to imageid translations"}

struct	*fd

begin
	real	mjd
	string	key, det, ext, filt
	struct	value

	fd = input
	while (fscan (fd, mjd, key, det, ext, filt, value) != EOF) {
	    if (nscan() < 5)
		next
	    if (det == "-")
	        det = ""
	    if (ext == "-")
	        ext = ""
	    else {
	        match (ext, imageid, stop-) | scan (ext, line)  
		ext = line
	    }
	    if (filt == "-")
	        filt = ""

	    putcal ("", "keyword", dm, value=filt, class=key,
		detector=det, imageid=ext, filter=value, exptime="", mjd=mjd)
	    #putcal ("", "keyword", dm, value=value, class=key,
	#	detector=det, imageid=ext, filter=filt, exptime="", mjd=mjd)
	}
	fd = ""
end
