#!/bin/env pipecl
#
# MEFWCS -- Update the WCS.

int	status = 1
int	nsets
real    mjd, axs, ays, axr, ayr, x1, x2, x3, x4
string	dataset, lfile, tlfile, procid, cast
string	sifdataset, sifdir, sifcat, sifmcat, sifwcsdb, sifshort, shortdir

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"

# Tasks and packages.
utilities
images
imcoords
mario
tables
ttools
servers
dataqual

# Set dataset directory and logfile.
mefnames (envget ("OSF_DATASET"))
dataset = mefnames.dataset
set (uparm = mefnames.uparm)
set (pipedata = mefnames.pipedata)
lfile = mefnames.lfile
tlfile = "mefwcs.tmp"
cd (mefnames.datadir)

task $mefwcs_usnomatch_proc = "$mefwcs_usnomatch_proc.py $1"
task $mefwcs_usnomap_proc = "$mefwcs_usnomap_proc.py $1"
task $mefwcs_ccmap_proc = "$mefwcs_ccmap_proc.py $1"

# Log start of processing.
printf ("\nMEFWCS (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Insert new data product.
hsel( mefnames.hdr, "mjd-obs", yes ) | scan( mjd )
newDataProduct( mefnames.shortname, mjd, "mefobjectimage", "")
storekeywords( class="mefobjectimage", id=mefnames.hdr,
    sid=mefnames.shortname, dm=dm )

# Set up processing list files.
delete (dataset//".ace")
files ("*.ace", > "mefwcs.tmp")
list = "mefwcs.tmp"
while (fscan (list, s1) != EOF) {
    sifdataset = substr (s1, 1, strstr(".ace",s1)-1)
    sifnames (sifdataset)
    sifcat = ""; match (sifnames.cat, s1) | scan (sifcat)
    if (sifcat == "")
        next
    ;
    sifdir = substr (sifcat, 1, strldx("/",sifcat))
    print (sifcat, >> "mefwcscats.tmp")
    print (sifnames.mcat, >> "mefwcsmcats.tmp")
    printf ("%s %s %s %s\n", sifdir, sifnames.mcat, sifnames.wcsdb, sifnames.shortname,
        >> "mefwcsdb.tmp")
}
list = ""; delete ("mefwcs.tmp")

# Match the catalogs against the reference catalog.
if (access(mefnames.mcat)) {
    usnomatch ("@mefwcscats.tmp", mefnames.mcat, matchcat="@mefwcsmcats.tmp",
	imcatdef="@pipedata$catmatch.def", fracmatch=mef_fracmatch,
	logfile=tlfile) |& scan (line)
    concatenate (tlfile) | tee (lfile)
} else
    printf ("Warning: No reference catalog (%s)\n", mefnames.mcat) | scan (line)
if (substr (line, 1, 7) == "Warning") {
    sendmsg ("ERROR", substr(line,9,1000), "", "PROC")
    printf ("ERROR: %s\n", substr(line,9,1000), >> lfile)
    status = 2
} else {
    # Merge matched catalogs.
    delete (mefnames.mcat)
    mefmerge ("@mefwcsmcats.tmp", mefnames.mcat, catdef="",
        filter="G<22.", append+, verbose+)

    mefwcs_usnomatch_proc( tlfile )
    cl < mefwcs_usnomatch_proc.cl
    delete ("mefwcs_usnomatch_proc.cl")
}
delete (tlfile)

# Compute a global correction and add a constraining grid.
flpr
if (status == 1) {
    usnomap ("@mefwcsmcats.tmp", logfile=tlfile) |& scan (line)
    concatenate (tlfile) | tee (lfile)
    if (substr (line, 1, 7) == "Warning") {
        sendmsg ("ERROR", substr(line,9,1000), "", "PROC")
	printf ("ERROR: %s\n", substr(line,9,1000), >> lfile)
	status = 2
    } else {
        mefwcs_usnomap_proc( tlfile )
        cl < mefwcs_usnomap_proc.cl
	delete ("mefwcs_usnomap_proc.cl")
    }
    delete (tlfile)
}
;

# Do WCS solutions.  Get the shifted tangent point from the logfile.
if (status == 1) {
    axr = 0
    ayr = 0
    axs = 0
    ays = 0
    nsets = 0
    match ("new tangent point", lfile) | translit ("STDIN", "(,)", del+) |
	scan (s1, s1, s1, s1, s1, s2)
    x = real (s1); y = real (s2)
    hedit (mefnames.hdr, "RA", s1, show+)
    hedit (mefnames.hdr, "DEC", s2, show+)
    list = "mefwcsdb.tmp"
    nsets = 0
    while (fscan (list, sifdir, sifmcat, sifwcsdb, sifshort) != EOF) {
        if (mef_wcsglobal) {
	    match ("INDEF", sifmcat, > "mefwcsgrid.tmp")
	    ccmap ("mefwcsgrid.tmp", sifwcsdb, solution="wcs",
	        lngref=x, latref=y, projection="projp.zpx",
		xxorder=3, xyorder=3, yxorder=3, yyorder=3,
		xxterms="half", yxterms="half", > tlfile)
	    delete ("mefwcsgrid.tmp")
	} else {
	    ccmap (sifmcat, sifwcsdb, solution="wcs",
	        lngref=x, latref=y, projection="projp.zpx",
		xxorder=3, xyorder=3, yxorder=3, yyorder=3,
		xxterms="half", yxterms="half", > tlfile)
	}
	concatenate (tlfile) | tee (lfile)
	# Redirect input parameters for mefwcs_ccmap_proc to file.
	# mefwcs_ccmap_proc will read the parameters from file.
        # This is a way to avoid escaping the "!" in sifdir.
        printf("%s %s %s\n", tlfile, sifdir, sifshort, >> "mefwcs.inp" )
        mefwcs_ccmap_proc( "mefwcs.inp" )
        delete( "mefwcs.inp" )
        cl < mefwcs_ccmap_proc.cl
	delete ("mefwcs_ccmap_proc.cl")
	# Get the scale and the rms from the header to calculate the 
	# average over all extensions
	s1 = sifdir // sifshort
	hselect( s1, "dqwcccxr,dqwcccyr,dqwcccxs,dqwcccys", yes ) |
	    scan( x1, x2, x3, x4 )
	if (nscan()==4) {
	    axr = axr+x1
	    ayr = ayr+x2
	    axs = axs+x3
	    ays = ays+x4
            nsets = nsets+1
        }
	;
    }
    list = ""
    delete (tlfile)
    if (nsets>0) {
        axr = axr/nsets
        ayr = ayr/nsets
        axs = axs/nsets
        ays = ays/nsets
	printf("%12.5e\n", axr ) | scan( cast )
        setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
	    keyword="dqwccaxr", value=cast )
	hedit( mefnames.hdr, "wcsxrms", axr, add+, update+, verify-, show+, >> lfile)
	printf("%12.5e\n", ayr ) | scan( cast )
        setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
	    keyword="dqwccayr", value=cast )
	hedit( mefnames.hdr, "wcsyrms", ayr, add+, update+, verify-, show+, >> lfile)
	printf("%12.5e\n", axs ) | scan( cast )
        setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
	    keyword="dqwccaxs", value=cast )
	printf("%12.5e\n", ays ) | scan( cast )
        setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
	    keyword="dqwccays", value=cast )
    } else 
	sendmsg("WARNING", "Could not determine average WCS info", "", "DQ" )
}
;

# Update headers.
if (status == 1) {
    # Trigger results back to SIF pipeline.
    list = "mefwcsdb.tmp"
    while (fscan (list, sifdir, sifmcat, sifwcsdb, sifshort) != EOF) {
	thselect (sifmcat, "dmagzero", yes) | scan (line); i = fscan (line, x)
	thselect (sifmcat, "dmag", yes) | scan (line)
	z = INDEF; thselect (sifmcat, "dqdmag", yes) | scan (z)
	printf ("DMAGZERO %.3f\nDMAG %s\nMAGZERR 0.5\n",
	    x, line, > sifdir//"mefwcs.hdr")
	if (isindef(z)==NO)
	    printf ("DQDMAG %.3f\n", x, >> sifdir//"mefwcs.hdr"
	;
	move (sifmcat//","//sifwcsdb, sifdir)
    }
    list = ""

    # Update global header.
    hselect (mefnames.hdr, "MAGZERO", yes) | scan (y)
    y += x
    printf ("%.3f\n", x) | scan (x)
    printf ("%.3f\n", y) | scan (y)
    hedit (mefnames.hdr, "MAGZERO", y, add+, update+, verify-, show+, >> lfile)
    if (isindef(z)==NO)
	hedit (mefnames.hdr, "DQDMAG", x, add+, update+, verify-, show+, >> lfile)
    ;
    hedit (mefnames.hdr, "DMAGZERO", x, add+, update+, verify-, show+, >> lfile)
    hedit (mefnames.hdr, "MAGZERR", 0.5, add+, update+, verify-, show+, >> lfile)
    hedit (mefnames.hdr, "WCSCAL", "(1==1)", add+, update+, verify-, show+, >> lfile)
} else
    hedit (mefnames.hdr, "WCSCAL", "(1==2)", add+, update+, verify-, show+, >> lfile)

delete ("mefwcs*.tmp")

logout (status)
