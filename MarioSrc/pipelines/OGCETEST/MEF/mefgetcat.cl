# MEFGETCAT -- Get a catalog of coordinates.
# This routine is an interface to whatever catalog access method is used.
# The returned catalog consists of RA, DEC, g, r, i.
# The gri magnitudes are obtain by a transform from J, F, and N.

procedure mefgetcat (ra, dec, radius, cat)

real	ra			{prompt="RA center (hr)"}
real	dec			{prompt="DEC center (deg)"}
real	radius			{prompt="Radius (deg)"}
file	cat			{prompt="Output catalog"}

real	bmax = 100.		{prompt="Maximum blue magnitude"}
real	vmax = 100.		{prompt="Maximum visual magnitude"}
real	rmax = 100.		{prompt="Maximum red magnitude"}
int	nmax = INDEF		{prompt="Maximum number of sources"}
real	equinox = 2000.		{prompt="Equinox (yr)"}
real	epoch = 2000.		{prompt="Epoch (yr)"}

struct	*list

begin
	file	out, temp1, temp2
	string	rstr, dstr
	real	r, d, rad, j1, f1, j2, f2, n1, g, i
	int	nj1, nj2, nf1, nf2

	# Set temporary file.
	temp1 = mktemp ("tmp$mefgetcat")
	temp2 = temp1 // "1"

	# Define packages and tasks.
	task $scat = "$!scat -c ub1 -n -1 -r $3 $1 $2"

	# Set input parameters.
	r = ra * 15
	d = dec
	rad = radius * 3600.
	out = cat

	# Get the catalog data.  In this case we use USNO-B1.0 with SCAT.
	scat (r, d, rad) | fields ("STDIN", "2-8", lines="13-", > temp1)

	# Filter the output.  We use the J/F magnitudes in preference to the
	# O/E unless the former are missing.  In that case we make a
	# transform from O/E to J/F.

	nj1 = 0; nj2 = 0; nf1 = 0; nf2 = 0
	list = temp1
	while (fscan (list, rstr, dstr, j1, f1, j2, f2, n1) != EOF) {
	    if (j2 < 99 && f2 < 99) {
	        j1 = j2
	        f1 = f2
		nj2 += 1
		nf2 += 1
	    } else if (j1 < 99 && f1 < 99) {
		d = j1 - f1
		j1 = j1 + 0.048 - 0.243 * d
		f1 = f1 + 0.114 - 0.015 * d
		nj1 += 1
		nf1 += 1
	    } else
	        next
	    if (j1 > bmax || f1 > vmax || n1 > rmax)
	        next
	    g = 0.906 * j1 + 0.094 * f1 - 0.050
	    r = 0.129 * j1 + 0.871 * f1 + 0.076
	    if (n1 < 99)
		i = 0.173 * f1 + 0.827 * n1 + 0.386
	    else
	        i = 99
	    printf ("%s %s %5.2f %5.2f %5.2f\n", rstr, dstr, g, r, i,
	       >> temp2)
	}
	list = ""; delete (temp1)

	if (isindef(nmax))
	    rename (temp2, out)
	else {
	    sort (temp2, col=4, num+, rev-) | head (nl=nmax, > out)
	    delete (temp2)
	}

	if (nj1 > 0)
	    printf (" Using %d O magnitudes\n", nj1)
	if (nf1 > 0)
	    printf (" Using %d E magnitudes\n", nf1)
	if (nj2 > 0)
	    printf (" Using %d J magnitudes\n", nj2)
	if (nf2 > 0)
	    printf (" Using %d F magnitudes\n", nf2)
end
