#!/bin/env pipecl
# 
# XTALK -- This routine splits the input MEFs into smaller MEFS, one per
# ARCON box and calls the crosstalk pipeline for each one.

string	cpipe				# Calling pipeline
string	tpipe = ""			# Target pipelines
string	tpipe2 = ""			# Pipeline called by target pipeline

string	dataset, indir, datadir, ifile, lfile, hdr, temp1, temp2
string	imname, im, ampnames, extname, arcon, oname, oname1
string	ilist, olist, tlist, elist
struct	*fd

# Tasks and packages.
images
mscred
task mscextensions = mscsrc$x_mscred.e
cache mscred mscextensions
task $pipeselect = "$!pipeselect $1 $2 0 0"
task $callpipe = "$!call.cl $1 $2 $3 '$4' $5 2>&1 > $6; echo $? > $7"

# Set dataset and pipeline information.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
cpipe = names.pipe
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ifile = indir // dataset // "." // cpipe
lfile = datadir // names.lfile
ampnames = "pipedata$ampnames.dat"
mscred.logfile = lfile		
set (uparm = names.uparm)
set (pipedata = names.pipedata)

if (cpipe == "cal") {
    calnames (dataset)
    hdr = datadir // calnames.hdr
    tpipe = "xtc"
    tpipe2 = "scl"
} else {
    mefnames (dataset)
    hdr = datadir // mefnames.hdr
    tpipe = "xtm"
    tpipe2 = "sif"
}

# Work in data directory.
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset | tee (lfile)
time | tee (lfile)

# Do quick check for target pipeline.
pipeselect (envget ("NHPPS_SYS_NAME"), tpipe) | count | scan (i)
if (i == 0) {
    sendmsg ("WARNING", "Pipeline is not running", tpipe, "PIPE")
    logout 3
}
;
pipeselect (envget ("NHPPS_SYS_NAME"), tpipe2) | count | scan (i)
if (i == 0) {
    sendmsg ("WARNING", "Pipeline is not running", tpipe2, "PIPE")
    logout 3
}
;

# Get the list of extensions to process from the header of the first MEF.
imname = ""; head (ifile, nlines=1) | scan (imname)
if (imname == "") {
    sendmsg ("ERROR", "No data found", "", "VRFY")
    logout 0
}
;
im = imname // "[0]"

# Set lists for the splitting and lists for the target pipeline.
ilist = "xtalk1.tmp"; olist = "xtalk2.tmp"
tlist = "xtalk3.tmp"; elist = "xtalk4.tmp"
temp1 = "xtalk5.tmp"; temp2 = "xtalk6.tmp"
touch (olist)
list = ifile
for (j=1; fscan(list,imname)!=EOF; j+=1) {
    # Map extensions.
    mscextensions (imname, output="file", index="1-", extname="", extver="",
        lindex=no, lname=yes, lver=no, ikparams="", > elist)

    fd = elist
    for (i=1; fscan(fd,im)!=EOF; i+=1) {
	# Set extension name and arcon index for extensions in ampnames.
	# We change the extension name so that file sorting will remain
	# order with respect to the extensions.

	extname = ""
	hselect (im, "extname,ampname", yes) | scan (extname,line)
	extname = strlwr (extname)
	if (extname == "")
	    break
	else if (fscanf (extname, "im%d", k) > 0)
	    printf ("im%02d\n", k) | scan (extname)
	else
	    printf ("im%02d\n", i) | scan (extname)
	arcon = ""; match (line, ampnames, meta-) | scan (arcon)
	if (arcon == "") {
	    i -= 1
	    next
	}
	;
	arcon = substr (arcon, 1, 1)

	# The target dataset is called <dataset>-<pipe><arcon>.
	printf ("%s%s_%s\n", datadir, dataset, arcon) | scan (oname)

	# Create new target dataset.
	if (access (oname//"."//tpipe) == NO)
	    print (oname//"."//tpipe, >> tlist)
	;
	    
	# Create new MEF.
	printf ("%s%02d\n", oname, j) | scan (oname1)
	match (oname1, olist) | count | scan (k)
	if (k == 0) {
	    pathname (oname1//".fits", >> oname//"."//tpipe)
	    printf ("%s[0]\n", imname, >> ilist)
	    print (oname1, >> olist)
	}
	;

	# Add extensions.
	print (im, >> ilist)
	printf ("%s[%s,append,inherit]\n", oname1, extname, >> olist)

	# Save global header.
	if (i == 1 && j == 1) {
	    printf ("%s[0]\n", imname, >> ilist)
	    print (hdr, >> olist)
	}
	;
    }
    fd = ""; delete (elist)
}
list = ""

if (access(ilist) == NO) {
    sendmsg ("ERROR", "Failed to find extensions to correct", "", "PROC")
    logout 0
}
;

# Split the files.
imcopy (input="@"//ilist, output="@"//olist, verbose+)
delete (ilist); delete (olist)

# Set list to use in call.
rename (tlist, "mefxtalk.tmp")
tlist = "mefxtalk.tmp"

## Clean up.
delete ("xtalk*.tmp")

logout 1
