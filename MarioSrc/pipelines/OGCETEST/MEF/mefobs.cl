#!/bin/env pipecl
#
# MEFOBS -- Send observation status to monitor.

string	node, opusmon

# Get monitor address.
if (access ("MarioProc$/servers")) {
    opusmon = ""
    match ("opusmon", "MarioProc$/servers", stop-) | scan (opusmon)
    if (opusmon == "")
	logout 1
    node = envget("NHPPS_NODE_NAME")
} else
    logout 1

task $find = $!find
task $touch = $!touch
servers

cd ("OGCETEST_MEF$/" // "obs")
touch ("mefobs")
sleep (1)
if (access (".mefobs")) {
    find . -name '\*___\*' -cnewer .mefobs -print > temp
    delete (".mefobs", verify-)
} else
    find . -name '\*___\*' -print > temp
rename ("mefobs", ".mefobs", field="all")

cpmsgs (input="temp", output=opusmon, switchbd="", prefix=node//" ",
    nlines=100, wait=no, reporterr=no)
delete ("temp", verify-)

logout 1
