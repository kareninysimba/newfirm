#!/bin/env pipecl
#
# MEFTEL -- Interface to telescope file triggers.
#
# This module is triggered by ".tel" triggers and converts them to
# triggers for science or calibration pipelines.

string	dataset

# Load tasks and packages.
utilities
images
task $pipeselect = $!pipeselect

# Set directories and files.
dataset = envget ("EVENT_NAME")
cd ("OGCETEST_MEF$/input")

# Ignore certain events.
s1 = ""; head (dataset//".tel") | scan (s1)
if (s1=="" || substr(dataset,1,4)=="test" || substr(dataset,1,5) == "focus") {
    delete (dataset//".tel")
    if (access ("return/"//dataset))
	delete ("return/"//dataset)
    ;
    logout 1
}
;

# Check data and determine type of exposure.
if (imaccess (s1//"[0]")) {
    line = ""; hselect (s1//"[0]", "obstype", yes) | scan (line)
} else {
    sendmsg ("ERROR", "Cannot access data", s1, "VRFY")
    logout 0
}

# Trigger pipeline.
printf ("(%s)\n", line)
if (line == "zero" || line == "dome flat") {
    s2 = ""; pipeselect (envget ("NHPPS_SYS_NAME"), "cal", 1, 0, 0) | scan (s2)
    if (s2 == "") {
        sendmsg ("ERROR", "No pipeline running", "cal", "PIPE")
	logout 0
    }
    ;

    # Now trigger the CAL pipeline.  Note that we don't reset the dataset
    # name here and leave that up to the CALTEL module.

    if (access ("return/"//dataset))
        delete ("return/"//dataset)
    ;
    move (dataset//".tel", s2)
    touch (s2//dataset//".teltrig")

} else {
    # Reset name of dataset.  This protects against repeated names.
    # However, if the actual observed names are desired then comment
    # out the following block.

    s2 = dataset; hselect (s1//"[0]", "obsid", yes) | scan (s2)
    print (s2) | translit ("STDIN", ".", delete+) | scan (s2)
    if (s2 != dataset) {
	if (access ("return/"//dataset))
	    rename ("return/"//dataset, "return/"//s2)
	;
        rename (dataset//".tel", s2//".tel")
	dataset = s2
    }
    ;

    # Now trigger the MEF pipeline.
    rename (dataset//".tel", dataset//".mef")
    touch (dataset//".meftrig")
}

logout 1
