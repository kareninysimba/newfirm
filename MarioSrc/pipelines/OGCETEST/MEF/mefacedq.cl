#!/bin/env pipecl
#
# MEFACEDQ -- Take ACE results and merge them.

int	status = 1
int	dqobmaxa
real	seeingp, skyadu, skynoise, dpthadu, exptime, dqskfrac, dmag, skymean
string	dataset, dqreject
file	lfile
struct	sky, sig, dqrej, filter
struct	sft, frg, pgr

# Tasks and  packages.
servers
images
utilities
proto
noao
artdata
tables
ttools
task mksky = "NHPPS_PIPEAPPSRC$/MEF/mksky.cl"

findrule (cm, "good_for_sft", validate=1) | scan (s1)
task good_for_sft = (s1)
findrule (cm, "good_for_frg", validate=1) | scan (s1)
task good_for_frg = (s1)
findrule (cm, "good_for_pgr", validate=1) | scan (s1)
task good_for_pgr = (s1)

# Set dataset directory and logfile.
mefnames (envget ("OSF_DATASET"))
dataset = mefnames.dataset
set (pipedata = mefnames.pipedata)
set (uparm = mefnames.uparm)
cd (mefnames.datadir)
lfile = mefnames.lfile

# Log start of processing.
printf ("\nMEFACEDQ (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Compute global (average) values.
thselect ("*_mcat", "SEEINGP1", yes) | average | scan (seeingp)
thselect ("*_mcat", "SKYADU1", yes) | average | scan (skyadu)
thselect ("*_mcat", "SKYNOIS1", yes) | average | scan (skynoise)
thselect ("*_mcat", "DPTHADU1", yes) | average | scan (dpthadu)
delete ("*_mcat")

# Initialize values.
dqobmaxa = INDEF
dqskfrac = INDEF
sky = ""
sig = ""
touch ("mefacedq.lis")

# Compute maximum object area and global sky coverage.
# Set global rejection flag if appropriate.
dqreject = ""
files ("*ccd*.ace", > "mefacedq.tmp")
list = "mefacedq.tmp"
while (fscan (list, s1) != EOF) {
    s2 = substr (s1, 1, strstr(".ace",s1)-1)
    sifnames (s2)
    getkeyval (dm=dm, id=sifnames.image, keyword="dqrej",
        class="objectimage") | scan (dqrej)
    if (dqrej != "None")
        dqreject = dqrej
    ;
    getkeyval (dm=dm, id=sifnames.image, keyword="dqobmxa",
        class="objectimage") | scan (s3)
    printf ("  %s: DQOBMXA = %s\n", sifnames.image, s3, >> lfile)
    if (s3 != "None") {
	if (isindef(dqobmaxa))
	    dqobmaxa = int (s3)
	else
	    dqobmaxa = max (dqobmaxa, int(s3))
    }
    ;
    getkeyval (dm=dm, id=sifnames.image, keyword="dqskfrc",
        class="objectimage") | scan (s3)
    printf ("  %s: DQSKFRC = %s\n", sifnames.image, s3, >> lfile)
    if (s3 != "None") {
	if (isindef(dqskfrac))
	    dqskfrac = real (s3)
	else
	    dqskfrac = min (dqskfrac, real(s3))
    }
    ;
}
list = ""

if (isindef(dqobmaxa)) {
    status = 2
    sendmsg ("ERROR", "No maximum object area information", "", "VRFY")
}
;
if (isindef(dqskfrac)) {
    status = 2
    sendmsg ("ERROR", "No sky coverage information", "", "VRFY")
}
;

# Analyze sky images.
match ("_sky", "*.ace", print-, > "mefacedq.tmp")
count ("mefacedq.tmp") | scan (i)
if (i < 1) {
    status = 2
    sendmsg( "ERROR", "No sky images to work on", "", "VRFY")
} else {
    rename ("mefacedq.tmp", "mefacedq.lis")
    mksky ("mefacedq.lis", mefnames.sky, mefnames.hdr, "pipedata$mefmksky.dat",
        blank=-1000, verbose-)
    imstat (mefnames.sky, fields="mean,stddev", lower=-999, format-) |
        scan (sky)
}

# Analyze sigma images.
match ("_sig", "*.ace", print-, > "mefacedq.tmp")
count ("mefacedq.tmp") | scan (i)
if (i < 1) {
    status = 2
    sendmsg ("ERROR", "No skysig images to work on", "", "VRFY")
} else {
    rename ("mefacedq.tmp", "mefacedq.lis")
    mksky ("mefacedq.lis", mefnames.sig, mefnames.hdr, "pipedata$mefmksig.dat",
        blank=-1000, verbose-)
    imstat (mefnames.sig, fields="mean,stddev", lower=-999, format-) |
        scan (sig)
}

# Check rule for using exposure in sky flat.
if (dqreject != "") {
    sft = "N " // dqreject
    frg = "N " // dqreject
    pgr = "N " // dqreject
} else {
    filter = "INDEF"; exptime = INDEF; skymean = INDEF; dmag = INDEF
    x = INDEF; y = INDEF; z = INDEF
    hselect (mefnames.hdr, "FILTER", yes) | scan (filter)
    hselect (mefnames.hdr, "EXPTIME", yes) | scan (exptime)
    if (sky != "")
	i = fscan (sky, skymean)
    ;
    hselect (mefnames.hdr, "MAGZERO,MAGZREF", "(MAGZFLT==FILTER)") |
	scan (dmag, z)
    if (isindef(z)==NO)
        dmag = dmag - z
    ;
    s1 = "F"; hselect (mefnames.hdr, "WCSCAL", yes) | scan (s1)
    if (s1 == "T")
	match ("Reference point?*hours", lfile) | scan (s1, s1, x, y)
    ;
    good_for_sft (filter, exptime, skymean, dmag, dqobmaxa, dqskfrac, x, y) |
        scan (sft)
    good_for_frg (filter, exptime, skymean, dmag, dqobmaxa, dqskfrac, x, y) |
        scan (frg)
    good_for_pgr (filter, exptime, skymean, dmag, dqobmaxa, dqskfrac, x, y) |
        scan (pgr)
}

# Write information to MEF header, to SIF header, and to PMASS.
list = "mefacedq.lis"
for (j=1; fscan(list,s1)!=EOF; j+=1) {
    i = strldx ("/", s1)
    s2 = substr (s1, 1, strldx("/",s1))
    if (j == 1) {
	if (dqreject != "") {
	    setkeyval (dm, "mefobjectimage", mefnames.shortname, "dqreject",
	        dqreject)
	    hedit (mefnames.hdr, "DQREJECT", dqreject, add+, show+, >> lfile)
	}
	;
	if (isindef(dqobmaxa)==NO) {
	    printf ("%d\n", dqobmaxa) | scan (s1)
	    dqobmaxa = int (s1)
	    setkeyval (dm, "mefobjectimage", mefnames.shortname, "dqobmaxa", s1)
	    hedit (mefnames.hdr, "DQOBMAXA", dqobmaxa, add+, show+, >> lfile)
	}
	;
	if (isindef(dqskfrac)==NO) {
	    printf ("%.3f\n", dqskfrac) | scan (s1)
	    dqskfrac = real (s1)
	    setkeyval (dm, "mefobjectimage", mefnames.shortname, "dqskfrac", s1)
	    hedit (mefnames.hdr, "DQSKFRAC", dqskfrac, add+, show+, >> lfile)
	}
	;
	if (sky != "" && fscan(sky,x)==1) {
	    hedit (mefnames.hdr, "SKY", sky, add+, show+, >> lfile)
	    hedit (mefnames.hdr, "SKYMEAN", x, add+, show+, >> lfile)
	} else
	    x = INDEF
	if (sig != "" && fscan(sig,y)==1) {
	    hedit (mefnames.hdr, "SIG", sig, add+, show+, >> lfile)
	    hedit (mefnames.hdr, "SIGMEAN", y, add+, show+, >> lfile)
	} else
	    y = INDEF
	hselect (mefnames.hdr, "SFTFLAG", yes) | scan (line)
	if (line == "")
	    hedit (mefnames.hdr, "SFTFLAG", sft, add+, show+, >> lfile)
	else
	    sft = line
	hselect (mefnames.hdr, "FRGFLAG", yes) | scan (line)
	if (line == "")
	    hedit (mefnames.hdr, "FRGFLAG", frg, add+, show+, >> lfile)
	else
	    frg = line
	hselect (mefnames.hdr, "PGRFLAG", yes) | scan (line)
	if (line == "")
	    hedit (mefnames.hdr, "PGRFLAG", pgr, add+, show+, >> lfile)
	else
	    pgr = line
    }

    # Set information for SIF headers.
    if (isindef(seeingp)==NO)
	printf ("SEEINGP %.2f\n", seeingp, >> "mefacedq.hdr")
    ;
    if (isindef(skyadu)==NO)
	printf ("SKYADU %.5g\n", skyadu, >> "mefacedq.hdr")
    ;
    if (isindef(skynoise)==NO)
	printf ("SKYNOISE %.5g\n", skyadu, >> "mefacedq.hdr")
    ;
    if (isindef(dpthadu)==NO)
	printf ("DPTHADU %.2f\n", dpthadu, >> "mefacedq.hdr")
    ;
    if (isindef(x)==NO)
	printf ("SKYMEAN %.5g\n", x, >> "mefacedq.hdr")
    ;
    if (isindef(y)==NO)
	printf ("SIGMEAN %.5g\n", y, >> "mefacedq.hdr")
    ;
    printf ("SFTFLAG %s\n", sft, >> "mefacedq.hdr")
    printf ("FRGFLAG %s\n", frg, >> "mefacedq.hdr")
    printf ("PGRFLAG %s\n", pgr, >> "mefacedq.hdr")

    move ("mefacedq.hdr", s2)
}
list = ""

# Update sky and sig headers.
if (imaccess(mefnames.sky))
    mkheader (mefnames.sky, mefnames.hdr, append-, verbose-)
;
if (imaccess(mefnames.sig))
    mkheader (mefnames.sig, mefnames.hdr, append-, verbose-)
;

delete ("mefacedq*")
logout (status)
