#{ MARIO.CL -- Pipeline configuration script.
# This may do any valid IRAF setup but it is mostly intended for defining
# global variables.  Note that these become package parameters.

#string	psq_order = "asc"	{prompt="PSQ selection order"}
string	psq_order = "desc"	{prompt="PSQ selection order"}
int	psq_limit = 999999	{prompt="PSQ data limit"}
string	ds_cache = ""		{prompt="Data service cache"}
string	psq_redo = "no"		{prompt="Redo previous processing?",
				 enum="yes|no|cal|obj"}
int     num_trigger = INDEF     {prompt="Number of pipelines to trigger"}
int	dir_mingroup = 15	{prompt="Minimum grouping"}
string	cal_skyflat = "flat"	{prompt="Sky flat type (ignore|flat|object)"}
int	cal_nseqmax = 20	{prompt="Maximum cal sequence"}
int	cal_nzero = 3		{prompt="Min zero combine for putcal"}
int	cal_nflat = 3		{prompt="Min flat combine for putcal"}
real	cal_mjdmin = -300	{prompt="Min MJD for putcal"}
real	cal_mjdmax = 300	{prompt="Max MJD for putcal"}
real	cal_dmjd = 10		{prompt="MJD window for getcal"}
bool	day_usecallib = yes	{prompt="Use calibration library?"}
string	day_mkcal = "yes"	{prompt="Make calibration even if in library (yes|no qual|eval"}
string	day_dopgr = "default"	{prompt="Pupil ghost removal? (yes|no|default)"}
string	day_dofringe = "default"{prompt="Fringe correction? (yes|no|default)"}
string	day_doskyflat = "default"{prompt="Sky flat cal? (yes|no|default)"}
bool	day_dorsp = yes		{prompt="Do resample images?"}
string	pgr_scale = "global"	{prompt="Scale calculation (global|local|none)"}
string	frg_scale = "global"	{prompt="Scale calculation (global|local|none)"}
real	sft_minexptime = 60.	{prompt="Min exposure time for stacking (sec)"}
int	sft_maxobjarea = 500000	{prompt="Max object area for stacking (pix)"}
real	sft_minskyfrac = 0.80	{prompt="Min sky fraction for stacking"}
string	srs_trrej = "craverage"	{prompt="Type of CR rejection performed within image (none|craverage)"}
string	str_trrej = "acediff"	{prompt="Type of CR rejection performed during 2-pass stacking (none|acediff)"}
int	mdc_n2pass = 3		{prompt="Minimum number of images for 2-pass stacking"}
int	sft_smooth = "9"	{prompt="Use smooth sky flat?  (yes|no|number)"}
int     sft_window = 255	{prompt="Size of median filter window"}
string	sft_overlap = "75 20 -0.02 70" {prompt="Overlap threshold parameters"}
real	rsp_grid = 1		{prompt="Tangent point grid (deg)"}
real	rsp_scale = 0.25	{prompt="Resampling scale"}
string	rsp_interp = "lsinc17"	{prompt="Resampling interpolant"}
int	png_blk = 2		{prompt="PNG version block size"}
int	png_pix = 120		{prompt="PNG preview pixel size"}
int	verbose = 0		{prompt="Verbosity level"}
bool	dps_review = no		{prompt="Review data products?"}
bool	mdp_doshort = yes	{prompt="Convert to short integer?"}
real	mdp_maxbscale = -30	{prompt="Maximum BSCALE parameter"}
real	mef_fracmatch = 0.25	{prompt="WCS matching fraction"}
bool	mef_wcsglobal = yes	{prompt="Do only global WCS update"}
string	email = "operator"	{prompt="Address to send email"}
string	dps_archive = "MasterCal"	{prompt="Archive data products?"}
string	dts_ftp = "no"		{prompt="FTP data products (yes|no|proposal)"}
string	dts_save = "MEF?*.fits"	{prompt="Pattern to save"}
string	all_erract = "return"	{prompt="Error action (none|return)"}

keep
