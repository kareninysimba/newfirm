#!/bin/env pipecl
#
# SENDTRIG -- Send data and trigger.
#
# The contents of the data file are copied to the target file and the
# trigger file is created.  The target file can be specified by an @file.
# The files are sought first in the data directory and then in the
# input directory.  The input files may be removed after the triggering.

string	datafile		{prompt="Data file"}
string	targfile		{prompt="Target file"}
bool	delfiles = YES		{prompt="Delete files?"}

string	indir, datadir, lfile, dfile, tfile, rfile

# Get arguments.
if (fscan (cl.args, datafile, targfile, delfiles) < 2)
    logout 0
;

# Set directories and files.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", strupr(envget("NHPPS_MODULE_NAME")), names.dataset) |
    tee (lfile)
time | tee (lfile)

# Set trigger file.
if (substr(targfile,1,1)=="@") {
    rfile = substr (targfile, 2, 999)
    if (access(rfile))
        files ("@"//rfile) | scan (tfile)
    else if (access(indir//rfile))
        files ("@"//indir//rfile) | scan (tfile)
    else
        tfile = ""
} else {
    rfile = ""
    tfile = targfile
}

# Set data file.
if (access(datafile))
    dfile = datafile
else if (access(indir//datafile))
    dfile = indir//datafile
else
    dfile = ""

# Check for missing files.
if (tfile == "" || dfile == "")
    logout 0
;

# Send data and trigger files.
copy (dfile, tfile)
touch (tfile//"trig")

# Delete input files if desired.
if (delfiles) {
    if (access(dfile))
        delete (dfile)
    ;
    if (access(rfile))
        delete (rfile)
    ;
}
;

logout 1
