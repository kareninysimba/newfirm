#!/bin/env pipecl
#
# SIFSETUP -- Setup SIF processing data including calibrations.

int	status = 1
int	founddir
struct	statusstr, mask_name
string	dataset, indir, datadir, ifile, im, lfile
file	caldir = "MC$"

# Packages and tasks.
task    $cp = "$!cp -r $(1) $(2)"
task    $ln = "$!ln -s $(1) $(2)"
servers
images
utilities
noao
astutil
task sifsetwcs = "NHPPS_PIPEAPPSRC$/SIF/sifsetwcs.cl"

# Set paths and files.
sifnames (envget("OSF_DATASET"))
dataset = sifnames.dataset
indir   = sifnames.indir
datadir = sifnames.datadir
lfile   = datadir // sifnames.lfile
ifile   = indir // dataset // ".sif"

set (uparm = sifnames.uparm)
set (pipedata = sifnames.pipedata)

# Log start of processing.
printf ("\nSIFSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Set secondary return files.
cd (indir//"return")
if (access (dataset//".sif")) {
    head (dataset//".sif") | scan (s1)
    s1 = substr (s1, 1, strstr("/input/",s1)) 
    s2 = dataset // ".ace"
    print (s1//"input/"//s2, > dataset//".ace")
}
;

# Need to be able to access the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Setup data subdirectories.
delete (substr(sifnames.uparm, 1, strlen(sifnames.uparm)-1))
s1 = ""; head (ifile) | scan (s1)
iferr {
    setdirs (s1//"[0]")
} then
    logout 0
else
    ;

# Get the calibrations.  There are two stages.  The first gets required
# calibrations and the script exits with an error if the calibrations
# are not found.  The second stage is for optional calibrations.  Only
# warnings are printed in that case.

list = ifile
while (fscan (list, s1) != EOF) {
    getcal (s1, "zero", cm, caldir,
	obstype="!obstype", detector="!detector", imageid="!ccdname",
	filter="!filter", exptime="", mjd="!mjd-obs", dmjd=cal_dmjd) |
	scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for zero", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;
	
    getcal (s1, "_flat", cm, caldir,
	obstype="!obstype", detector="!detector", imageid="!ccdname",
	filter="!filter", exptime="", mjd="!mjd-obs", dmjd=cal_dmjd) |
	scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for flat", str(status)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;

    getcal (s1, "bpm", cm, caldir,
	obstype="!obstype", detector="!detector", imageid="!ccdname",
	filter="!filter", exptime="", mjd="!mjd-obs", > "dev$null")
#    getcal (s1, "MAGZREF", cm, caldir,
#	obstype="", detector="!detector", imageid="!ccdname",
#	filter="!photindx", exptime="", mjd="!mjd-obs", > "dev$null")
}
list = ""

if (status == 0)
    logout 0
;

# Wait for the FITS kernel.
flpr
sleep 1

# Set the WCS.  and copy the crosstalk masks to the data directory.
# The crosstalk mask has the same name as the MEF dataset it refers to,
# with "_xtm" appended to it and ".pl" extension.  The amplifier number
# (e.g. 01) comes after the "_xtm" string and before the file extension.

list = ifile
while (fscan (list, s1) != EOF) {
    sifsetwcs (image=s1) | tee (lfile)
    i = strldx ("_", s1)
    printf ("%sxtm%s\n", substr(s1,1,i), substr(s1,i,100)) | scan (s2)
    if (imaccess (s2))
        imrename (s2, ".")
    ;
}
list = ""

logout 1
