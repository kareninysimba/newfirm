# SIFSETWCS -- Set the WCS from a database and RA/Dec header keywords.

procedure sifsetwcs ()

string	image			{prompt="Image"}

begin
	file	db
	string	imageid, str
	real	raval, decval, eqval
	struct	wcsastrm

	# Get image ID and database from WCSDB keyword.
	hselect (image, "imageid,wcsdb", yes) | scan (imageid,db)
	if (nscan() == 2) {
	    imageid = "im" // imageid

	    # Set the WCSASTRM keyword.
	    match ("WCSASTRM", db, stop-) | scan (str, str, str, wcsastrm)
	    if (nscan() > 3) {
		hedit (image, "WCSASTRM", wcsastrm, add+,
		    del-, verify-, show-, update+)
		printf ("WCSASTRM= '%s'\n", wcsastrm)
	    }

	    # Set the WCS and delete any WCSSOL keyword.
	    ccsetwcs (image, db, imageid, xref=INDEF, yref=INDEF,
		xmag=INDEF, ymag=INDEF, xrotation=INDEF, yrotation=INDEF,
		lngref=INDEF, latref=INDEF, lngunits="", latunits="",
		transpose=no, projection="tan", coosystem="j2000",
		update=yes, verbose+) |
		    match ("hours", "STDIN", stop+) |
		    match ("Updating", "STDIN", stop+)
	    hedit (image, "WCSSOL", add-, addonly-, del+, verify-, show-,
	        update+)

	    # Set the WCS reference point to the telescope coordinates.
	    raval = INDEF; decval = INDEF; eqval = 2000.
	    hselect (image, "telra,teldec,telequin", yes) |
		translit ("STDIN", '"', delete+, collapse-) |
		scan (raval, decval, eqval)
	    if (isindef(raval) || isindef(decval) || isindef(eqval)) 
		hselect (image, "ra,dec,equinox", yes) |
		    translit ("STDIN", '"', delete+, collapse-) |
		    scan (raval, decval, eqval)
	    if (isindef(raval) || isindef(decval) || isindef(eqval)) 
		printf ("    WARNING: Tangent point not updated - \
		    TEL coords not found\n")
	    else {
		if (eqval != 2000.) {
		    printf ("%g %g\n", raval, decval) |
		    precess ("STDIN", eqval, 2000.) |
		    scan (raval, decval)
		}
		raval = raval * 15.
		hedit (image, "crval1", raval, add+, del-, update+,
		    show-, verify-)
		hedit (image, "crval2", decval, add+, del-, update+,
		    show-, verify-)
		printf ("    Reference point: %.2H %.1h 2000.0 (hours degrees)\n",
		    raval, decval)
	    }
	} else
	    printf ("    WARNING: WCS not updated - WCSDB not found\n")
end
