#!/bin/env pipecl
#
# BP1LISTS -- Create lists of dome flats for each detector, filter and CCD.
#
# The input file is a list of dome flats.  The dome flats are assumed to have
# specific file naming convention from the MOSAIC pipeline:
# 
#     <basename>-<filter>-<tel><date>F-ccd<num>.fits
# 
# where <tel> is four digits.  Typically the files would be from
# the data manager directories.

string	dataset, datadir, ifile, ofile, lfile
string	filt, tel, ccd

# Load packages.
util
images
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set file and path names.
names ("bp1", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".bp1"
ofile = datadir // dataset // "-all.list"
lfile = datadir // names.lfile
set (uparm = names.uparm)
cd (datadir)

# Clean up for restart.
delete (dataset//"-*")

# Log start of processing.
printf ("\nBP1LISTS (%s): ", dataset) | tee (lfile)
time | tee (lfile)

list = ifile
while (fscan (list, s1) != EOF) {
    
    # Parse the name.
    s2 = substr (s1, strldx("/",s1)+1, 1000)
    print (s2) | translit ("STDIN", "-.", " ") | scan (s3, filt, tel, ccd, s3)
    if (nscan() < 5)
        next
    ;
    tel = substr (tel, 1, 4)

    # Check if the dome flat has saturated pixels.
    x = 0.; hselect (s1, "dqglfsat", yes) | scan (x)
    if (x > 0.01)
        next
    ;

    # Set the output list.
    s2 = dataset // "_" // tel // ccd
    s3 = s2 // filt

    # Add any new list to output list of lists of ...
    if (access(s2) == NO)
        path (s2, >> ofile)
    ;
    if (access(s3) == NO)
        path (s3, >> s2)
    ;

    # Add image to output list.
    path (s1, >> s3)
}
list = ""

# Call pipeline.
callpipe ("bp1", "bp2", "split", ofile, "bp1lists1.tmp", "bp1lists2.tmp")
i = 1; concat ("bp1lists2.tmp") | scan (i)
concat ("bp1lists1.tmp")
delete ("bp1lists1.tmp,bp1lists2.tmp")
delete (ofile)

logout (i)
