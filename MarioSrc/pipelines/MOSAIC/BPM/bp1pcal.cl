#!/bin/env pipecl
#
# BP1PCAL -- Put bad pixel masks into the data manager.

string	dataset, datadir, ifile, lfile

# Load packages.
servers
images

# Set file and path names.
names ("bp1", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = datadir // dataset // ".list"
lfile = datadir // names.lfile
set (uparm = names.uparm)
cd (datadir)

# Log start of processing.
printf ("\nBP1PCAL (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Select the bad pixel masks.
match (".pl", ifile, > "bp1pcal.tmp")
concat ("bp1pcal.tmp", >> lfile)

# Check the header for the range of validity.
s1 = ""; head ("bp1pcal.tmp") | scan (s1)
if (s1 == "") {
    sendmsg ("ERROR", "No bad pixels masks created", "", "CAL")
    logout (0)
}
;
x = 50000; y = 60000
hselect (s1, "mjdmin,mjdmax", yes) | scan (x, y)
x = int (x-1); y = int (y+1)
y = 60000
printf ("mjdstart=%d, mjdend=%d\n", x, y, >> lfile)

# Put the calibrations.
putcal ("@bp1pcal.tmp", "image", cm, class="bpm", detector=instrument,
    imageid="!ccdname", filter="", exptime="", quality=1., match="",
    mjdstart=x, mjdend=y) | scan (i, line)

if (i != 0) {
    sendmsg ("ERROR", "Failed to enter bad pixels masks", line, "CAL")
    i = 0
} else
    i = 1

delete ("bp1pcal.tmp")

logout (i)
