#!/bin/env pipecl
#
# BP2CMB -- Combine the flats by filter.
#
# The input file is a list of lists.  Each of the lists is all the
# dome flats for a single filter and CCD.  The final result is
# the average over all filters.

string	dataset, datadir, ifile, lfile, image, temp
real	mjdmin, mjdmax
struct	*list2

# Load packages.
images

# Set file and path names.
names ("bp2", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".bp2"
lfile = datadir // names.lfile
image = names.shortname
temp = "BP2CMB"
set (uparm = names.uparm)
cd (datadir)

# Log start of processing.
printf ("\nBP2CMB (%s): ", dataset) | tee (lfile)
time | tee (lfile)

mjdmin = 100000.; mjdmax = 0.
list = ifile
for (i=1; fscan(list,s1)!=EOF; i+=1) {

    # Copy data if not local.  This is an optimization.
    b1 = no
    list2 = s1
    while (fscan (list2, s2) != EOF) {
	path (substr(s2,stridx("!",s2)+1,1000)) | scan (s3)
	if (s3 != s2) {
	    b1 = yes
	    break
	}
	;
    }
    list2 = ""
    if (b1) {
	rename (s1, temp//"1.tmp")
	list2 = temp//"1.tmp"
	while (fscan (list2, s2) != EOF) {
	    s3 = substr (s2, strldx("/",s2)+1, 1000)
	    imcopy (s2, s3, verb+, >> lfile)
	    print (s3, >> s1)
	}
	list2 = ""; delete (temp//"1.tmp")
    }
    ;
        
    # Make average dome flat.
    printf ("\nMake average dome flat for filter...", >> lfile)
    imcombine ("@"//s1, temp, headers="", bpmasks="", rejmasks="",
	nrejmasks="", expmasks="", sigmas="", logfile=lfile,
	combine="average", reject="none", project=no, outtype="real",
	outlimits="", offsets="none", masktype="none", maskvalue="0",
	blank=0., scale="none", zero="none", weight="none", statsec="",
	expname="", lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1,
	nkeep=1, mclip=yes, lsigma=3., hsigma=3., rdnoise="0.", gain="1.",
	snoise="0.", sigscale=0.1, pclip=-0.5, grow=0.)

    # Divide each dome flat by the average.
    # Also compute the min/max MJD.
    printf ("\nRatio individual dome flats with the average...\n", >> lfile)
    list2 = s1
    for (j=1; fscan(list2,s2)!=EOF; j+=1) {
	s3 = temp // i // "_" // j
        imarith (s2, "/", temp, s3)
	print (s3, >> temp//"1.tmp")
	hselect (s2, "mjd-obs", yes) | scan (x)
	mjdmin = min (mjdmin, x)
	mjdmax = max (mjdmax, x)
    }
    list2 = ""
    imdel (temp)
    if (b1)
	imdel ("@"//s1)
    ;

    # Combine the ratios with scaling.
    printf ("\nCombine ratios for a filter ...", >> lfile)
    s2 = temp // i
    imcombine ("@"//temp//"1.tmp", s2, headers="", bpmasks="",
	rejmasks="", nrejmasks="", expmasks="", sigmas="",
	logfile=lfile, combine="average", reject="none", project=no,
	outtype="real", outlimits="", offsets="none", masktype="none",
	maskvalue="0", blank=0., scale="mean", zero="none", weight="none",
	statsec="", expname="", lthreshold=INDEF, hthreshold=INDEF, nlow=1,
	nhigh=1, nkeep=1, mclip=yes, lsigma=3., hsigma=3., rdnoise="0.",
	gain="1.", snoise="0.", sigscale=0.1, pclip=-0.5, grow=0.)
    imdel ("@"//temp//"1.tmp")
    delete (temp//"1.tmp")

    print (s2, >> temp//"2.tmp")
}
list = ""

# Combine all the filters to make the final data product.
printf ("\nCombine average ratios for all filters ...", >> lfile)
imcombine ("@"//temp//"2.tmp", image, headers="", bpmasks="", rejmasks="",
    nrejmasks="", expmasks="", sigmas="", logfile=lfile,
    combine="average", reject="none", project=no, outtype="real",
    outlimits="", offsets="none", masktype="none", maskvalue="0",
    blank=0., scale="mean", zero="none", weight="none", statsec="",
    expname="", lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1,
    nkeep=1, mclip=yes, lsigma=3., hsigma=3., rdnoise="0.", gain="1.",
    snoise="0.", sigscale=0.1, pclip=-0.5, grow=0.)
imdel ("@"//temp//"2.tmp")
del (temp//"2.tmp")

# Set the MJD range.
hedit (image, "mjdmin", mjdmin, add+, show+)
hedit (image, "mjdmax", mjdmax, add+, show+)

# Clean up lists.
delete ("@"//ifile)

logout (1)
