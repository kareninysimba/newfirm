# BPMEDIT -- Edit BPM masks.

procedure bpmedit (images)

string	images		{prompt="List of images"}

struct	*fd

begin
	file	im, bpm, temp
	struct	dispcmd

	temp = mktemp ("tmp$iraf")

	sections (images, option="fullname", > temp)

	imedit.search = 0
	imedit.radius = 0
	imedit.aperture = "square"
	imedit.value = 0

	fd = temp
	while (fscan (fd, im) != EOF) {
	    bpm = ""; hselect (im, "BPM", yes) | scan (bpm)
	    if (bpm == "") {
		printf ("WARNING: No BPM keyword (%s)\n", im)
		next
	    }
	    if (imaccess(bpm)==NO) {
		printf ("WARNING: Can't access BPM (%s)\n", bpm)
		next
	    }

	    dispcmd  = "display " // im // " 1"
	    dispcmd += " over=$image erase=$erase ocol='1=green,red' >& dev$null"
	    display (im, 2, over="")
	    imedit (bpm, "", command=dispcmd)
	}
	fd = ""; delete (temp, verify-)
end
