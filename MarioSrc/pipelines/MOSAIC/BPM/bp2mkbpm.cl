#!/bin/env pipecl
#
# BP2MKBPM -- Make BPM from dome flat ratio image.
#
# Note that there is an occasional bug with using the FITS version of
# a mask so we explicitly use pl masks as output from ACE.

string	dataset, datadir, ifile, lfile, image, mask
file	edgemask, fit, abs, abs1, resid, obm1, obm2, obm3, obm4, obm5
int	nc, nl

# Load packages.
images
mscred
cache mscred
task mscskysub = "mscsrc$x_mscred.e"
ace

# Set file and path names.
names ("bp2", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".bpm"
lfile = datadir // names.lfile
image = names.shortname
mask = image // "_bpm.pl"
set (uparm = names.uparm)
cd (datadir)

edgemask = "bp2mkbpm1.pl"
fit = "bp2mkbpm2.fits"
abs = "bp2mkbpm3.fits"
abs1 = "bp2mkbpm4.fits"
resid = "bp2mkbpm5.fits"
obm1 = "bp2mkbpm6.pl"
obm2 = "bp2mkbpm7.pl"
obm3 = "bp2mkbpm8.pl"
obm4 = "bp2mkbpm9.pl"
obm5 = "bp2mkbpm10.pl"
imdel ("bp2mkbpm*pl,bp2mkbpm*fits")

# Log start of processing.
printf ("\nBP2MKBPM (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Remove background.  This is done in two passes using masks
# to avoid ringing from bad pixels.

# First pass: Use an edge mask, detect sources, fit with medium order.

hselect (image, "naxis1,naxis2", yes) | scan (nc, nl)
printf ("%d,%d\n", nc, nl) | scan (s2)
i = nc - 11; j = nl - 11
printf ("(I<11||I>%d||J<11||J>%d)?1 : 0\n", i, j) | scan (line)
imexpr (line, edgemask, dims=s2, verb-)

detect (image, masks=edgemask, skys="", sigmas="", exps="", gains="",
    objmasks=obm1, omtype="boolean", catalogs="", extnames="",
    catdefs="ace$lib/catdef.dat", catfilter="", logfiles=lfile,
    dodetect=yes, dosplit=no, dogrow=no, doevaluate=no,
    skytype="block", fitstep=100, fitblk1d=10, fithclip=2.,
    fitlclip=3., fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
    blksize=-20, blknsubblks=2, updatesky=no, bpdetect="1-100",
    bpflag="1-100", convolve="", hsigma=30., lsigma=30., hdetect=yes,
    ldetect=yes, neighbors="8", minpix=1, sigavg=4., sigmax=4.,
    bpval=INDEF, splitmax=INDEF, splitstep=0.4, splitthresh=5.,
    sminpix=8, ssigavg=10., ssigmax=5., ngrow=2, agrow=2.,
    magzero="INDEF")
mscskysub (image, fit, 10, 20, type_output="fit",
    function="spline3", cross_terms=yes, xmedian=1, ymedian=1,
    median_perce=50., lower=0., upper=0., ngrow=0, niter=0,
    regions="mask", rows="*", columns="*", border="50",
    sections="", circle="", mask=obm1, div_min=INDEF)
imdelete (obm1)

# Second pass: Use absolute value of background subtracted image,
# with deeper detection and higher order.  The absolute image
# replicates to trace bad columns without problems with bad edges.

i = nl - 199; j = nl - 100
printf (abs1//"[*,%d:%d]\n", j, i) | scan (s2)
i = nl - 99; j = nl
printf (abs//"[*,%d:%d]\n", i, j) | scan (s3)

imexpr ("abs(a-b)", abs1, image, fit, verb-)
imcopy (abs1, abs, verb-)
imcopy (abs1//"[*,200:101]", abs//"[*,1:100]", verb-)
imcopy (s2, s3, verb-)
imdelete (fit)
imdelete (abs1)

detect (abs, masks=edgemask, skys="", sigmas="", exps="", gains="",
    objmasks=obm1, omtype="boolean", catalogs="",
    extnames="", catdefs="ace$lib/catdef.dat", catfilter="",
    logfiles=lfile, dodetect=yes, dosplit=no, dogrow=no, doevaluate=no,
    skytype="block", fitstep=100, fitblk1d=10, fithclip=2., fitlclip=3.,
    fitxorder=1, fityorder=1, fitxterms="half", blkstep=1, blksize=-20,
    blknsubblks=2, updatesky=no, bpdetect="1-100", bpflag="1-100",
    convolve="block 1 5", hsigma=12., lsigma=10., hdetect=yes,
    ldetect=no, neighbors="8", minpix=1, sigavg=4., sigmax=4., bpval=INDEF,
    splitmax=INDEF, splitstep=0.4, splitthresh=5., sminpix=8, ssigavg=10.,
    ssigmax=5., ngrow=2, agrow=2., magzero="INDEF")
mscskysub (image, fit, 20, 40, type_output="fit",
    function="spline3", cross_terms=yes, xmedian=1, ymedian=1,
    median_perce=50., lower=0., upper=0., ngrow=0, niter=0,
    regions="mask", rows="*", columns="*", border="50",
    sections="", circle="", mask=obm1,  div_min=INDEF)
imdelete (obm1)
imdelete (edgemask)
imdelete (abs)

# Make residual and absolute residual images.
imexpr ("a-b", resid, image, fit, verb-)
imexpr ("abs(a-b)", abs1, image, fit, verb-)
imcopy (abs1, abs, verb-)
imcopy (abs1//"[*,200:101]", abs//"[*,1:100]", verb-)
imcopy (s2, s3, verb-)
imdelete (fit)

# Detect bad columns and lines in absolute residual image.
detect (abs, masks="", skys="", sigmas="", exps="", gains="",
    objmasks=obm1, omtype="boolean", catalogs="",
    extnames="", catdefs="ace$lib/catdef.dat",
    catfilter="(xmax-xmin)<25&&(ymax-ymin)>85&&ellip>0.9&&(theta<-85||theta>85)",
    logfiles=lfile, dodetect=yes, dosplit=no, dogrow=no,
    doevaluate=yes, skytype="block", fitstep=100, fitblk1d=10,
    fithclip=2., fitlclip=3., fitxorder=1, fityorder=1,
    fitxterms="half", blkstep=1, blksize=-20, blknsubblks=2,
    updatesky=no, bpdetect="1-100", bpflag="1-100",
    convolve="block 1 21", hsigma=5., lsigma=10., hdetect=yes, ldetect=no,
    neighbors="8", minpix=1, sigavg=4., sigmax=4., bpval=INDEF,
    splitmax=INDEF, splitstep=0.4, splitthresh=5., sminpix=8,
    ssigavg=10., ssigmax=5., ngrow=0, agrow=0., magzero="INDEF")
detect (abs1, masks="", skys="", sigmas="", exps="", gains="",
    objmasks=obm2, omtype="boolean", catalogs="",
    extnames="", catdefs="ace$lib/catdef.dat",
    catfilter="(xmax-xmin)<10&&(ymax-ymin)>25&&ellip>0.9&&(theta<-85||theta>85)",
    logfiles=lfile, dodetect=yes, dosplit=no, dogrow=no,
    doevaluate=yes, skytype="block", fitstep=100, fitblk1d=10,
    fithclip=2., fitlclip=3., fitxorder=1, fityorder=1,
    fitxterms="half", blkstep=1, blksize=-20, blknsubblks=2,
    updatesky=no, bpdetect="1-100", bpflag="1-100",
    convolve="block 1 11", hsigma=8., lsigma=10., hdetect=yes, ldetect=no,
    neighbors="8", minpix=1, sigavg=4., sigmax=4., bpval=INDEF,
    splitmax=INDEF, splitstep=0.4, splitthresh=5., sminpix=8,
    ssigavg=10., ssigmax=5., ngrow=0, agrow=0., magzero="INDEF")
detect (abs1, masks="", skys="", sigmas="", exps="", gains="",
    objmasks=obm3, omtype="boolean", catalogs="",
    extnames="", catdefs="ace$lib/catdef.dat",
    catfilter="(xmax-xmin)>14&&(ymax-ymin)<3", logfiles=lfile,
    dodetect=yes, dosplit=no, dogrow=no, doevaluate=yes,
    skytype="block", fitstep=100, fitblk1d=10, fithclip=2.,
    fitlclip=3., fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
    blksize=-20, blknsubblks=2, updatesky=no, bpdetect="1-100",
    bpflag="1-100", convolve="block 7 1", hsigma=5., lsigma=10.,
    hdetect=yes, ldetect=no, neighbors="8", minpix=1, sigavg=4.,
    sigmax=4., bpval=INDEF, splitmax=INDEF, splitstep=0.4,
    splitthresh=5., sminpix=8, ssigavg=10., ssigmax=5., ngrow=0,
    agrow=0., magzero="INDEF")
    imdelete (abs)
    imdelete (abs1)

# Detect other features in residual image.
detect (resid, masks="", skys="", sigmas="", exps="", gains="",
    objmasks=obm4, omtype="boolean", catalogs="",
    extnames="", catdefs="ace$lib/catdef.dat", catfilter="",
    logfiles=lfile, dodetect=yes, dosplit=no, dogrow=no, doevaluate=no,
    skytype="block", fitstep=100, fitblk1d=10, fithclip=2., fitlclip=3.,
    fitxorder=1, fityorder=1, fitxterms="half", blkstep=1, blksize=-20,
    blknsubblks=2, updatesky=no, bpdetect="1-100", bpflag="1-100",
    convolve="block 3 3", hsigma=10., lsigma=10., hdetect=yes,
    ldetect=yes, neighbors="8", minpix=1, sigavg=4., sigmax=4., bpval=INDEF,
    splitmax=INDEF, splitstep=0.4, splitthresh=5., sminpix=8, ssigavg=10.,
    ssigmax=5., ngrow=3, agrow=2., magzero="INDEF")
imdelete (resid)

# Detect gross features in original image.
detect (image, masks="", skys="", sigmas="", exps="", gains="",
    objmasks=obm5, omtype="boolean", catalogs="",
    extnames="", catdefs="ace$lib/catdef.dat", catfilter="",
    logfiles=lfile, dodetect=yes, dosplit=no, dogrow=no, doevaluate=no,
    skytype="block", fitstep=100, fitblk1d=10, fithclip=2., fitlclip=3.,
    fitxorder=1, fityorder=1, fitxterms="half", blkstep=1, blksize=-10,
    blknsubblks=2, updatesky=no, bpdetect="1-100", bpflag="1-100",
    convolve="", hsigma=10., lsigma=10., hdetect=yes,
    ldetect=yes, neighbors="8", minpix=1, sigavg=4., sigmax=4.,
    bpval=INDEF, splitmax=INDEF, splitstep=0.4, splitthresh=5.,
    sminpix=8, ssigavg=10., ssigmax=5., ngrow=1, agrow=0., magzero="INDEF")

# Merge masks.
if (imaccess(mask))
    imdel (mask)
;
imexpr ("max(a,b,c)>0?1 : (max(d,e)>0?2 : 0)", mask,
    obm1, obm2, obm3, obm4, obm5, verb-)
imdel ("bp2mkbpm*pl,bp2mkbpm*fits")
hedit (image, "BPM", mask, add+)

logout (1)
