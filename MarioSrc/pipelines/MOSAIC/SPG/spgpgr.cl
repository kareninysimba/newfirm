#!/bin/env pipecl
#
# SPGPGR -- Remove pupil ghost.

real	quality = -1
string	dataset, datadir, ifile, lfile, cast, p, spg
real	scale, mjd
file	caldir = "MC$"

# Packages and task.
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"
images
servers
dataqual

# Set paths and files.
names ("spg", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".spg"
lfile = datadir // names.lfile
set (uparm = names.uparm)
cd (datadir)

# Log start of processing.
printf ("\nSPGPGR (%s): ", dataset) | tee (lfile)
time | tee (lfile)

list = ifile
while (fscan (list, s1) != EOF) {
    # Set the calibrated name from the uncalibrated name.
    i = strldx ("/", s1)
    j = strlstr (".fits", s1)
    s2 = substr (s1, i+1, j-1)
    p = s2 // "_p"
    s3 = substr (s2, 1, strldx("-",s2)-1)
    s3 = substr (s3, strldx("-",s3)+1,1000)

    # Set the pupil ghost template and scaling.
    spg = ""; hselect (s1, "PUPIL", yes) | scan (spg)
    scale = INDEF; hselect (s1, "PGRSCALE", yes) | scan (scale)
    if (scale == INDEF)
        spg = ""
    if (spg != "") {
	if (imaccess(spg) == NO) {
	    spg = ""
	    getcal (s1, "pupil", cm, caldir, obstype="",
	        detector=instrument, imageid="!ccdname", filter="!filter",
		exptime="", mjd="!mjd-obs", quality=0.,
		match="!ccdsum") | scan (i, line)
	    if (i == 0)
		hselect (s1, "PUPIL", yes) | scan (spg)
	    else
	        sendmsg ("WARNING", "No library pupil ghost template to apply",
		    substr(s1,strldx("/",s1)+1,1000), "CAL")
	}
	;
    }
    ;

    # Subtract scaled pupil ghost template from data.
    if (spg != "") {
	if (scale == 1.) {
	    printf ("%s = %s - %s\n", p, s1, spg)
	    imexpr ("a - b", p, s1, spg, outtype="real",
		verbose-)
	} else {
	    printf ("%s = %s - %g * %s\n", p, s1, scale, spg)
	    imexpr ("a - b * c", p, s1, spg, scale, outtype="real",
	    verbose-)
	}
	hselect (p, "mjd-obs", yes) | scan (mjd)
	newDataProduct (p, mjd, "objectimage", "")
	storekeywords (class="objectimage", id=p, sid=p, dm=dm)
	printf ("%12.5e\n", scale) | scan ( cast )
	setkeyval (class="objectimage", id=p, dm=dm, keyword="dqpgscal",
	    value=cast)
	setkeyval (class="objectimage", id=p, dm=dm, keyword="dqpgcal",
	    value=spg)
    } else {
	printf ("%s = %s\n", p, s1)
	imcopy (s1, p, verbose-)
    }
}
list = ""

logout 1
