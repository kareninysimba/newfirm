# NGTTRIG -- Trigger pipelines with list files.
#
# This procedure will look at the MarioProc directory to determine which
# nodes are running the input process for a pipeline and distributes the
# list files to the nodes.

procedure ngttrig (input, process)

string	input			{prompt="List of trigger files"}
string	process			{prompt="Trigger process name"}
bool	verbose = no		{prompt="Verbose?"}

struct	*fd1, *fd2

begin
	int	nfnames
	file	fnames, trigdirs, fname, trigdir

	# Set temporary files.
	fnames = mktemp ("tmp$ngt")
	trigdirs = mktemp ("tmp$ngt")

	# Count the number of files and return if none are specified.
	files (input, sort+, > fnames)
	nfnames = 0
	count (fnames) | scan (nfnames)
	if (nfnames == 0) {
	    delete (fnames, verify-)
	    return
	}

	# Select trigger directories.
	cache pipeselect
	pipeselect (envget ("NHPPS_SYS_NAME"), process, nfnames, 0, > trigdirs)

	# Assign files to trigger directories.
	fd1 = fnames; fd2 = trigdirs; nfnames = 0
	while (fscan (fd1, fname) != EOF) {
	    if (fscan (fd2, trigdir) == EOF)
	        delete (fname, verify-)
	    else {
		movefiles (fname, trigdir, verbose=verbose)
		nfnames = nfnames + 1
	    }
	}
	fd1 = ""; delete (fnames, verify-)
	fd2 = ""; delete (trigdirs, verify-)

	# Return an error if no trigger directories are found.
	if (nfnames == 0)
	    printf ("ERROR: No trigger directories found\n")
end
