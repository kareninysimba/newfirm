#!/bin/env pipecl
#
# SFTAPPLY -- Apply sky flat.
#
# Flags:
# _	Sky created.  Possibly ask operator to review.
# 	If no review enter in calibration library.
# Y	Sky flat is ok.  Enter in calibration library.
# N	Sky flat is not ok.  Use calibration library.
# A	Sky flat not created.  Use calibration library.
# S	Skip sky flat fielding.

int	status = 1
int	rulepassed
real	quality = -1
real	dqsfsnr, mjd, mjdstart, mjdend
string	dataset, datadir, ifile, lfile, sft
string	flag, rulepath
file	caldir = "MC$"

# Packages and task.
servers
images
mscred
cache mscred

# Define rules
findrule( dm, "use_fit_skyflat", validate=1 ) | scan( rulepath )
task use_fit_skyflat = (rulepath)

# Set paths and files.
flag = envget ("OSF_FLAG")
sftnames (envget ("OSF_DATASET"))
dataset = sftnames.dataset
datadir = sftnames.datadir
ifile = sftnames.indir // dataset // ".sft"
lfile = datadir // sftnames.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = sftnames.uparm)
set (pipedata = sftnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSFTAPPLY (%s, '%s'): ", dataset, flag) | tee (lfile)
time | tee (lfile)

# Set sky flat to use.  Note that if sft_smooth=="no" then no ssft
# image will have been produced earlier.

if (flag == "_" && imaccess(sftnames.ssft)) {
    i = 1; hselect (sftnames.ssft, "ncombine", yes) | scan (i)
    if (sft_smooth == "yes")
	rulepassed = 1
    else if (i <= int (sft_smooth))
        rulepassed = 1
    else {
	# Get bandtype from the header and SNR of sky flat.  Set to -1
	# if dqsfsnr is not defined (this means that too few frames were
	# available and dqsfsnr could not be determined; if that happens the
	# sky flat is presumably of too low quality to be used unsmoothed.

	s1 = ""; hselect (sftnames.sft, "bandtype", yes ) | scan( s1 )
	dqsfsnr = -1; hselect( sftnames.sft, "dqsfsnr", yes ) | scan( dqsfsnr )
	rulepassed = 1; use_fit_skyflat( s1, dqsfsnr ) | scan( rulepassed )
    }

    if (rulepassed > 0) {
	printf("Using SMOOTH sky flat\n")
	imdelete (sftnames.sft)
	imrename (sftnames.ssft, sftnames.sft)
    }
    ;
}
;
if (flag == "_" && imaccess(sftnames.sft)==NO)
    logout 0

# Trigger DRV pipeline if it is running.
if (flag == "_") {
    task $pipeselect = "$!pipeselect"
    s1 = ""; pipeselect (envget ("NHPPS_SYS_NAME"), "drv", 0, 0) | scan (s1)
    if (s1 != "") {
	daynames (sftnames.parent)
	s2 = daynames.parent // "-drv-" // daynames.child
	s3 = s1 // s2 // ".sft"
	pathname (sftnames.sft, > s3)
	touch (s3//"trig")
	if (!dps_review)
	    flag = "Y"
	;
    } else
	flag = "Y"
}
;

# Wait for review.
if (flag == "_")
    logout 2
;

# Add sky flat to calibrations manager if one has been defined.
# If the _ssft remains part of the pipeline, it should also be
# sent to PMAS
if (flag == "Y") {
    hselect (sftnames.sft, "mjd-obs,quality", yes) | scan (mjd, quality)
    mjdstart = mjd + cal_mjdmin; mjdend = mjd + cal_mjdmax
    putcal (sftnames.sft//".fits", "image", cm, class="sft",
        detector=cl.instrument, imageid="!ccdname", filter="!filter",
	exptime="", quality=quality, match="!ccdsum",
	mjdstart=mjdstart, mjdend=mjdend)
    if (putcal.statcode != 0) {
	sendmsg ("ERROR", "Putcal failed", putcal.statstr, "CAL")
	status = 0
    }
    ;
} else if (imaccess(sftnames.sft))
    imdelete (sftnames.sft)
;
if (status == 0)
    logout 4
;

# Don't apply sky flat.  This is here rather than earlier so the code
# above could chose to reset the flag.
if (flag == "A" && sft_cal == "no")
    flag = "S"
;
if (flag == "S")
    logout 3
;

# Determine if a calibration was computed and its quality.
# To avoid a call to the DM we put the sky flat in the local MC.
# The SSF pipeline is likely to be running on the same machine.

if (imaccess(sftnames.sft) && quality >= 0.) {
    if (imaccess("MC$"//sftnames.sft)==YES)
	sendmsg ("WARNING", "Calibration already in MarioCal",
	    sftnames.sft, "CAL")
    else
	imcopy (sftnames.sft, "MC$", verb+)
    sftnames.sft = "MC$" // sftnames.sft
}
;

# Loop over the input files and set things for the SSF pipeline.
list = ifile
while (fscan (list, s1) != EOF) {
    # Extract the image dataset name.
    s2 = substr (s1, strldx("/",s1)+1, strldx("-",s1)-1)
    s2 = substr (s2, strldx("-",s2)+1,1000)

    # Set the sky flat.
    if (quality >= 0.)
	hedit (s1, "SFT", sftnames.sft, add+)
    else
        getcal (s1, "sft", cm, caldir, obstype="", detector=cl.instrument,
	    imageid="!ccdname", filter="!filter", exptime="", mjd="!mjd-obs",
	    dmjd=cal_dmjd, quality=0., match="!ccdsum") #| scan (i, line)
    hselect (s1, "$I,sft", yes)

    # Set output files.
    s2 = "sft2ssf_" // s2 // ".tmp"
    path (s2, >> "sft2ssf.tmp")
    print (s1, >> s2)
}
list = ""

logout 1
