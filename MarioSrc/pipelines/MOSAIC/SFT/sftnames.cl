# SFTNAMES -- Directory and filenames for the SFT pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure sftnames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline name"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file	parent			{prompt = "Parent part of dataset"}
file	child			{prompt = "Child part of dataset"}
file	sft			{prompt = "Sky flat image"}
file	ssft			{prompt = "Smoothed sky flat image"}
file	lfile			{prompt = "Log file"}
file	bpm			{prompt = "Bad pixel mask"}
file	obm			{prompt = "Object mask"}
file	s			{prompt = "Sky flat corrected image"}
bool	base = no		{prompt = "Child part of dataset"}
string	pattern = ""		{prompt = "Pattern"}

begin
	# Set generic names.
	names ("sft", name, pattern=pattern, base=base)
	dataset = names.dataset
	pipe = names.pipe
	shortname = names.shortname
	rootdir = names.rootdir
	indir = names.indir
	datadir = names.datadir
	uparm = names.uparm
	pipedata = names.pipedata
	parent = names.parent
	child = names.child
	lfile = names.lfile

	# Set pipeline specific names.
	sft = shortname // "_sft"
	ssft = shortname // "_ssft"
	s = shortname // "_s"
	bpm = sft // "bpm"
	obm = sft // "_obm"


	if (pattern != "") {
	    sft += ".fits"
	    ssft += ".fits"
	    bpm += ".pl"
	    obm += ".fits"
	    s += ".fits"
	}
end
