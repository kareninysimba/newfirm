#!/bin/env pipecl
#
# SFTSETUP -- Setup SFT processing data. 
# 
# The input list of images is assumed to contain the uparm keyword.

string	dataset, datadir, ifile, lfile, statusstr, mkcal
int	founddir
real	qual = 0.
file	caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Set paths and files.
sftnames (envget("OSF_DATASET"))
dataset = sftnames.dataset
ifile = sftnames.indir // dataset // ".sft"
datadir = sftnames.datadir

lfile = datadir // sftnames.lfile
set (uparm = sftnames.uparm)
set (pipedata = sftnames.pipedata)

# Work in the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\nSFTSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean up from previous processing.
delete ("sftsetup[^_]*,*.fits")
if (access(ifile//"obm")) {
    concat (ifile//"obm", ifile, append+)
    delete (ifile//"obm")
}
;

# Separate obm files and sort.
match ("_obm.fits", ifile) | sort ( > ifile//"obm")
match ("_obm.fits", ifile, stop+) | sort ( > "sftsetup.tmp")
rename ("sftsetup.tmp", ifile)

# Setup UPARM.
delete (substr(sftnames.uparm, 1, strlen(sftnames.uparm)-1))
s1 = ""; head (ifile, nlines=1) | scan (s1)
iferr {
    setdirs (s1)
} then {
    logout 0
} else
    ;

# Check for library sky flats.  The purpose of this loop here is to
# allow skipping generating an sky flat if there is one in the
# library with higher precedence.  Note that the sft_cal flag can force
# creation of an sky flat.

i = fscan (sft_cal, mkcal, qual)
if (mkcal == "libfirst" || mkcal == "libonly") {
    b1 = yes
    list = ifile
    while (fscan (list, s1) != EOF) {
        getcal (s1, "sft", cm, caldir, obstype="", detector=instrument,
	    imageid="!ccdname", filter="!filter", exptime="", mjd="!mjd-obs",
	    dmjd=cal_dmjd, quality=qual, match="!ccdsum", > "dev$null")
	if (getcal.statcode != 0) {
	    b1 = no
	    break
	}
	;
    }
    list = ""
} else
    b1 = no

if (mkcal == "libonly" || (b1 && mkcal == "libfirst")) {
    sendmsg ("WARNING", "Creation of sky flat skipped", "", "CAL")
    logout 2
}
;

# Check dataset comments for the A flag.
s1 = ""
if (access(ifile//"com"))
    match ("\#A", ifile//"com") | scan (s1)
;
if (s1 == "#A" && mkcal == "no")
    logout 2
;

# We're going to need the object masks from the input data so copy them.
# While we are at it we change the values to compress the object mask.
# Note that we can choose the values that are good and bad here.

list = ifile
while (fscan (list, s1) != EOF) {
    s2 = ""; hselect (s1, "objmask", yes) | scan (s2)
    if (s2 == "")
        next
    ;
    i = strldx ("[", s2)
    if (i > 0)
        s2 = substr (s2, 1, i-1) // ".fits"
    ;
    match (s2, ifile//"obm") | scan (s3)
    #copy (s3, ".", verbose+)
    iferr {
	imexpr ("min(a,11)", s2//"[pl,type=mask]", s3//"[pl]", verb-)
    } then {
	copy (s3, ".", verbose+)
    } else
        ;
        
}
list = ""

# Allow time for object masks to actually appear.  This is some kind of
# bug/problem with our pipeline OS such that the next stage might not
# actually find the files without the following delay.
sleep (1)

logout 1
