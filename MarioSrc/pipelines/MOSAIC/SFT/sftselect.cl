# SFTSELECT -- Select sky stack images and set scale factors.
# The scale factors are based on the SKYMEAN values.
# Note this has output parameters and so the task should be cached.

procedure sftselect (key, input, output, scales)

string	key			{prompt="Selection keyword"}
file	input			{prompt="File of input images"}
file	output			{prompt="File of selected images"}
file	scales			{prompt="File of scale factors"}
bool	cksftflag = yes		{prompt="Check SFTFLAG?"}
int	nselect			{prompt="Number selected"}
int	nforced			{prompt="Number of forced selections"}
string	filter			{prompt="Filter"}

begin
	file	im, im1, temp
	string	sftflag
	real	skymean, scale, norm
	struct	flt

	temp = mktemp ("sftselect")

	# Initialize.
	if (access(output))
	    delete (output, verify-)
	if (access(scales))
	    delete (scales, verify-)
	nselect = 0
	nforced = 0
	filter = ""

	# Check images.
	hselect ("@"//input, "$I,SKYMEAN,"//key, yes) |
	    translit ("STDIN", '"', delete+, > temp)
	list = temp; norm = INDEF
	while (fscan (list, im, skymean, sftflag) != EOF) {
	    im1 = substr (im, strldx("/",im)+1, strlen(im))
	    if (nscan() < 3) {
		sendmsg ("WARNING", "Missing keywords", im1, "VRFY")
		next
	    }
	    if (skymean <= 0.0001) {
		sendmsg ("WARNING", "SKYMEAN must be positive",
		    im1//" "//str(skymean), "VRFY")
		next
	    }
	    if (isindef(norm))
	        norm = skymean
	    scale = norm / skymean
	    if (sftflag == "Y!" || sftflag == "N!")
		nforced += 1
	    if (cksftflag && substr(sftflag,1,1) == "N")
	        next
	    print (im, >> output)
	    print (scale, >> scales)
	    nselect += 1
	    if (filter == "") {
		hselect (im, "FILTER", yes) | scan (flt)
		filter = flt
	    }
	}
	list = ""; delete (temp, verify-)

	if (filter == "")
	    filter = "default"
end
