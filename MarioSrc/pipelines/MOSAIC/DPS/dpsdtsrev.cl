#!/bin/env pipecl
# 
# DPSDTSREV -- Review data products in a DTS directory.
#
# This version of DPSREVIEW is run manually from the DTS directory.

string  dataset, datadir, htmldir, html, s4, osf_cmd, host, sftflag
struct	obstype, proctype

# Load tasks and packages
task $hostname = "$!hostname -f"
task $pwd = "$!pwd"
task $resize = "$!convert -resize"
task dpssummary = "NHPPS_PIPESRC$/MOSAIC/DPS/dpssummary.cl"
task dpssummary1 = "NHPPS_PIPESRC$/MOSAIC/DPS/dpssummary1.cl"
images
utilities
fitsutil

# Set names and directories.
path | scan (datadir)
s1 = substr (datadir, 1, strldx("/",datadir)-1)
s1 = substr (s1, strldx("/",s1)+1,1000)
s1 = substr (s1, 1, strldx("-",s1)) // "dps"
names ("dps", s1)
dataset = names.dataset

# Log start of module.
printf ("\n%s (%s): ", "dpsdtsrev", dataset)
time

# Create data subdirectories.  Note that this may be called more than once
# so we delete any old data.
cd (names.rootdir//"output")
if (access("html")==NO)
    mkdir ("html")
;
htmldir = names.shortname//"/"
if (access(htmldir))
    delete (htmldir//"*")
else
    mkdir (htmldir)
cd (htmldir)
htmldir = "../"//names.shortname//"/"
html = "../html/"//names.shortname // ".html"
if (access(html))
    delete (html)
;

# Create list of datasets.
files (datadir//"*_png.fits") | match ("_r_png", "STDIN", stop+, > "dpsrev1.tmp")
list = "dpsrev1.tmp"
while (fscan (list, s2) != EOF) {
    s1 = substr (s2, 1, strstr("_png",s2)-1)

    # Create the HTML file and initialize the table.
    if (access(html)==NO) {
	printf ('<HTML><TITLE>%s</TITLE><BODY><CENTER><H1>%s</H1></CENTER>\n',
	    names.shortname, names.shortname, > html)
	printf ('\n<TABLE BORDER="1">\n', >> html)
    } else
	printf ('</TR>\n', >> html)

    # Add the MEF version.
    printf ("<TR>\n", >> html)
    copy (s2, ".", verbose-)
    s2 = substr (s2, strldx("/",s2)+1, 1000)
    s3 = substr (s2, 1, strstr("_png.fits",s2)-1)
    i = strldx ("-", s3)
    if (i == 0)
        s4 = s3
    else
	s4 = substr (s3, i+1, 1000)
    imhead (s2//"[0]", l+, > s3//".hdr")

    printf ('<TD ALIGN="LEFT" VALIGN="CENTER">', >> html)
    dpssummary (s2//"[0]", >> html)
    printf ('</A></TD>\n', >> html)
    printf ('<TD ALIGN="LEFT" VALIGN="CENTER">', >> html)
    dpssummary1 (s2//"[0]", >> html)
    printf ('</A></TD>\n', >> html)

    # Observation type and processing type and various keywords.
    obstype = ""; hselect (s2//"[0]", "obstype", yes) | scan (obstype)
    proctype = ""; hselect (s2//"[0]", "proctype", yes) | scan (proctype)
    sftflag = ""; hselect (s2//"[0]", "SFTFLAG", yes) |
	translit ("STDIN", '"', delete+) | scan (sftflag)

    fgread (s2, "1,2", "", verbose-)
    delete (s2)
    resize (800, s3//"_x"//png_blk//".png", s3//"_800.png")
    printf ('<TD ALIGN="CENTER" VALIGN="BOTTOM">', >> html)
    if (proctype != "MasterCal") {
	if (substr(sftflag,1,1) != "Y")
	    printf ('<INPUT TYPE="CHECKBOX">Sky Stack<BR>\n', >> html)
	else
	    printf ('<INPUT TYPE="CHECKBOX" CHECKED>Sky Stack<BR>\n', >> html)
    }
    ;
    printf ('<IMG SRC="%s%s_%d.png">\n', htmldir, s3, png_pix, >> html)
    printf ('<BR><A HREF="%s%s.hdr">%s</A>\n', htmldir, s3, s4, >> html)
    printf ('<BR>[<A HREF="%s%s_%d.png">%d</A>',
        htmldir, s3, 800, 800, >> html)
    printf (', <A HREF="%s%s_x%d.png">x%d</A>]\n',
        htmldir, s3, png_blk, png_blk, >> html)
    printf ('</TD>\n', >> html)

    # Add the resampled version.
    s2 = ""; files (s1//"_r_png.fits*") | scan(s2)
    if (s2 == "")
        next
    ;

    copy (s2, ".", verbose-)
    s2 = substr (s2, strldx("/",s2)+1, 1000)
    s3 = substr (s2, 1, strstr("_png.fits",s2)-1)
    i = strldx ("-", s3)
    if (i == 0)
	s4 = s3
    else
	s4 = substr (s3, i+1, 1000)
    imhead (s2//"[0]", l+, > s3//".hdr")
    fgread (s2, "1,2", "", verbose-)
    delete (s2)
    resize (800, s3//"_x"//png_blk//".png", s3//"_800.png")
    printf ('<TD ALIGN="CENTER" VALIGN="BOTTOM">', >> html)
    printf ('<IMG SRC="%s%s_%d.png">\n', htmldir, s3, png_pix, >> html)
    printf ('<BR><A HREF="%s%s.hdr">%s</A>\n', htmldir, s3, s4, >> html)
    printf ('<BR>[<A HREF="%s%s_%d.png">%d</A>',
	htmldir, s3, 800, 800, >> html)
    printf (', <A HREF="%s%s_x%d.png">x%d</A>]\n',
	htmldir, s3, png_blk, png_blk, >> html)
    printf ('</TD>\n', >> html)

}
list = ""; delete ("dpsrev1.tmp")

# Finish table.
if (access(html))
    printf ('</TR>\n', >> html)
;

# Add link to sky analysis.
s1 = ""; files (datadir//"*fig.html") | scan (s1)
if (s1 != "") {
    s2 = substr (s1, strldx("/",s1)+1, 1000)
    files (datadir//"*fig*", > "dtsrev1.tmp")
    copy ("@dtsrev1.tmp", ".", verbose-)
    printf ('\n<H4><A HREF="%s%s">Sky Analysis</A></H4>\n',
        htmldir, s2, >> html)
}
;

if (access(html)) {
    # Finish HTML.
    printf ('\n</TABLE></BODY></HTML>\n', >> html)
}

logout 1
