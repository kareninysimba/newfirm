#!/bin/env pipecl
#
# CALPCAL -- Put calibration into calibration archive.

int	status = 1
real	mjd, mjdstart, mjdend
string	dataset, msg
struct	obstype

# Load the packages.
images
lists
servers

# Set the dataset name and uparm.
dataset = envget ("OSF_DATASET")
set (uparm = "MOSAIC_CAL$/data/" // dataset // "/uparm/")
cd ("MOSAIC_CAL$/data/" // dataset)

# Log the operation.
printf ("\nCALPCAL (%s):\n", dataset)

# Get list of SIF images.  Require that all were successful.
# Note that later we also check for a flag to skip putting the calibration
# but allowing the flat field to be a data product.

list = dataset // ".scl"
while (fscan(list,s1)!=EOF) {
    s2 = ""; match ("-ccd[0-9]*.fits", s1) | scan (s2)
    if (s2 == "") {
	if (access("calpcal.lis")==YES)
	    delete ("calpcal.lis")
	;
	break
    }
    print (s2, >> "calpcal.lis")
}
list = ""

if (access("calpcal.lis")==NO) {
    sendmsg ("ERROR", "No calibration to put", "", "CAL")
    logout (0)
}
;

# Get obstype from last image.
hselect (s2, "mjd-obs,obstype", yes) | scan (mjd, obstype)
if (obstype == "dome flat")
    obstype = "dflat"
else if (obstype == "sky flat")
    obstype = "sflat"
mjdstart = mjd + cal_mjdmin
mjdend = mjd + cal_mjdmax
i = 1; hselect (s2, "ncombine", yes) | scan (i)

# For flat fields we need to average the mean values and set the gains.
if (obstype == "dflat" || obstype == "sflat") {
    average.data_value = INDEF
    hselect ("@calpcal.lis", "ccdmean", yes) | average | scan (x)
    if (isindef(x)==NO) {
	printf (" CCDMEAN: %g\n", x)
	hedit ("@calpcal.lis", "DCCDMEAN", "(ccdmean/"//x//")",
	    add+, show-, update+, verify-)
	hedit ("@calpcal.lis", "CCDMEAN", value=x,
	    add+, show-, update+, verify-)
	hedit ("@calpcal.lis", "CCDMEANT", del+, show-, update+, verify-)
    } else {
        sendmsg ("WARNING", "Failed to compute flat mean level",
	    "not put in DM", "CAL")
	status = 0
    }
    average.data_value = INDEF
    hselect ("@calpcal.lis", "ccdmin", yes) | average | scan (x)
    if (isindef(x)==NO) {
	x = max (1., x)
	printf (" CCDMIN: %g\n", x)
	imreplace ("@calpcal.lis", x, lower=INDEF, upper=2., radius=0)
    } else {
        sendmsg ("WARNING", "Failed to compute CCD min good level",
	    "not put in DM", "CAL")
	status = 0
    }
    average.data_value = INDEF
    hselect ("@calpcal.lis", "gain", yes) | average | scan (x)
    if (isindef(x)==NO) {
	printf (" GAINMEAN: %g\n", x)
	hedit ("@calpcal.lis", "GAINMEAN", value=x,
	    add+, show-, update+, verify-)
    } else {
        sendmsg ("WARNING", "Failed to compute flat mean gain",
	    "not put in DM", "CAL")
	status = 0
    }
}
;

if (status == 0)
    logout (status)
;

# Check for badcal flag.
x = 0.; hselect ("@calpcal.lis", "quality", yes) | sort (num+) | scan (x)

# Put the calibration files in the Data Manager.
if (x > -1.) {
    if (obstype == "zero") {
	if (i < cal_nzero)
	    sendmsg ("WARNING", "Too few zero/dark exposures",
		str(i) // " - not put in DM", "CAL")
	else {
	    # Activate for special processing.
	    if (NO) {
		
		# This is to make binned calibrations when the observer
		# did not take them.
		list = "calpcal.lis"
		while (fscan(list,s1)!=EOF) {
		    j = strldx ("/", s1) + 1
		    i = strstr ("-ccd", s1)
		    s2 = substr (s1, j, i-1) // "2" // substr (s1, i, 999)
		    blkavg (s1, s2, 2, 2, option="average")
		    hedit (s2, "CCDSUM", "2 2")
		    print (s2, >> "calpcal2.lis")
		}
		list = ""
		putcal ("@calpcal2.lis", "image", dm=cm, class=obstype,
		    detector=instrument, imageid="!ccdname", filter="!filter",
		    mjdstart=mjdstart, exptime="", mjdend=mjdend,
		    quality=x, match="!nextend,ccdsum") | scan (i, line)
		delete ("calpcal2.lis")
	    }
	    ;

	    putcal ("@calpcal.lis", "image", dm=cm, class=obstype,
		detector=instrument, imageid="!ccdname", filter="!filter",
		mjdstart=mjdstart, exptime="", mjdend=mjdend,
		quality=x, match="!nextend,ccdsum") | scan (i, line)
	    if (i != 0)
		status = 0
	    ;
	}
    } else if (obstype == "dark") {
	    putcal ("@calpcal.lis", "image", dm=cm, class="zero",
		detector=instrument, imageid="!ccdname", filter="!filter",
		mjdstart=mjdstart, exptime="!exptime", mjdend=mjdend,
		quality=x, match="!nextend,ccdsum") | scan (i, line)
	    if (i != 0)
		status = 0
	    ;
    } else if (obstype == "dflat" || obstype == "sflat") {
	if (i < cal_nflat)
	    sendmsg ("WARNING", "Too few dome/sky flat exposures",
		str(i) // " - not put in DM", "CAL")
	else {
	    # Activate for special processing.
	    if (NO) {

		# This is to make binned calibrations when the observer
		# did not take them.
		list = "calpcal.lis"
		while (fscan(list,s1)!=EOF) {
		    j = strldx ("/", s1) + 1
		    i = strstr ("-ccd", s1)
		    s2 = substr (s1, j, i-1) // "2" // substr (s1, i, 999)
		    blkavg (s1, s2, 2, 2, option="average")
		    hedit (s2, "CCDSUM", "2 2")
		    print (s2, >> "calpcal2.lis")
		}
		list = ""
		putcal ("@calpcal2.lis", "image", dm=cm, class=obstype,
		    detector=instrument, imageid="!ccdname", filter="!filter",
		    mjdstart=mjdstart, exptime="", mjdend=mjdend,
		    quality=x, match="!nextend,ccdsum") | scan (i, line)
		delete ("calpcal2.lis")
	    }
	    ;

	    putcal ("@calpcal.lis", "image", dm=cm, class=obstype,
		detector=instrument, imageid="!ccdname", filter="!filter",
		exptime="", mjdstart=mjdstart, mjdend=mjdend,
		quality=x, match="!nextend,ccdsum") | scan (i, line)
	    if (i != 0)
		status = 0
	    ;
	    match ("map.fits", "*.scl", print-, > "calpcal.lis")
	    count ("calpcal.lis") | scan (i)
	    if (i > 0)
	        putcal ("@calpcal.lis", "image", dm=cm, class="refflat",
	            detector=instrument, imageid="!ccdname", filter="!filter",
		    exptime="", mjdstart=mjdstart, mjdend=mjdend,
		    quality=x., match="!nextend,ccdsum") | scan (i, line)
	    ;
	}
	;
    } else {
	printf ("unknown type %s\n", obstype) | scan (line)
	status = 0
    }
} else {
    hselect ("@calpcal.lis", "$I,quality", yes)
    printf ("quality=%g\n", x) | scan (line)
    status = 2
}

delete ("calpcal.lis")
if (status != 1)
    sendmsg ("ERROR", "Putcal failed", line, "CAL")
;

logout (status)
