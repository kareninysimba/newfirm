#!/bin/env pipecl
# 
# CAL2DSP -- Call the DSP pipeline.

string  dsp, dataset, datadir, lfile

# Tasks and packages
proto
task pipeselect = "$!pipeselect"

# Set names and directories.
calnames (envget ("OSF_DATASET"))
dataset = calnames.dataset
datadir = calnames.datadir
lfile = calnames.lfile
set (uparm = calnames.uparm)
cd (datadir)

# Check if there is a preview file.
if (access(calnames.gif) == NO)
    logout 1
;

# Log start of module.
printf ("\nCAL2DSP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Check if DSP pipeline is running and exit if not.
dsp = ""; pipeselect (envget ("NHPPS_SYS_NAME"), "dsp", 1, 0) | scan (dsp)
if (dsp == "") {
    sendmsg ("WARNING", "No pipeline running", "dsp", "PIPE")
    logout 3
}
;

# Create the file to send to the DSP pipeline.
pathnames (calnames.lfile, >> dsp//dataset//".dsp")
pathnames (calnames.hdr, >> dsp//dataset//".dsp")
pathnames (calnames.gif, >> dsp//dataset//".dsp")
pathnames (calnames.gif2, >> dsp//dataset//".dsp")

# Trigger the DSP pipeline.
touch (dsp//dataset//".dsptrig")

logout 1
