#!/bin/env pipecl
#
# CALTEL -- Interface to telescope cal file triggers.
#
# This module is triggered by ".tel" triggers.  It groups calibrations
# by exposure type and filter into @files.  The @files are consumed by
# a different module based on how long since the files has been modified.
# The name of the dataset is set based on the OBSID rather than the name
# provided by the telescope.

string	dataset
struct	filter

# Load tasks and packages.
images
utilities

# Set directories and files.
dataset = envget ("EVENT_NAME")
cd ("MOSAIC_CAL$/input")

# Check data and determine type of exposure and filter.
s3 = ""; head (dataset//".tel") | scan (s3)
if (imaccess (s3//"[0]")) {
    line = ""; hselect (s3//"[0]", "obstype", yes) | scan (line)
    if (line == "dark")
        line = "zero"
    if (line != "zero") {
	s2 = ""; hselect (s3//"[0]", "filter", yes) |
	    translit ("STDIN", '"', delete+) | scan (s2)
    }
    ;

    if (line == "dome flat" || line == "sky flat") {
	filter = ""; hselect (s3//"[0]", "filter", yes) | scan (filter)
	i = fscan (filter, s1)
	filter = s1
    }
    ;

    # Reset name of dataset.
    s2 = dataset; hselect (s3//"[0]", "obsid", yes) | scan (s2)
    print (s2) | translit ("STDIN", ".", delete+) | scan (s2)
    if (s2 != dataset) {
        rename (dataset//".tel", "data/"//s2//".cal")
	dataset = s2
    }
    ;
} else {
    sendmsg ("ERROR", "Cannot access data", s3, "VRFY")
    logout 0
}

# Switch on type.
if (line == "zero")
    print ("data/"//dataset//".cal", >> "z.teltmp")
else if (line == "dome flat" || line == "sky flat")
    print ("data/"//dataset//".cal", >> "f_"//filter//".teltmp")
else
     logout 0

# Finish up.
logout 1
