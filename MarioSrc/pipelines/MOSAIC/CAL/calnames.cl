# CALNAMES -- Directory and filenames for the CAL pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure calnames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline name"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file	parent			{prompt = "Parent part of dataset"}
file	child			{prompt = "Child part of dataset"}
file	lfile			{prompt = "Log file"}
file	hdr			{prompt = "CAL header"}
file	gif			{prompt = "GIF"}
file	gif2			{prompt = "GIF"}
string	pattern = ""		{prompt = "Pattern"}
bool	base = no		{prompt = "Child part of dataset"}

begin
	# Set generic names.
	names ("cal", name, pattern=pattern, base=base)
	dataset = names.dataset
	pipe = names.pipe
	shortname = names.shortname
	rootdir = names.rootdir
	indir = names.indir
	datadir = names.datadir
	uparm = names.uparm
	pipedata = names.pipedata
	parent = names.parent
	child = names.child
	lfile = names.lfile

	# Set pipeline specific names.
	hdr = shortname // "_00.fits"
	gif = shortname // ".gif"
	gif2 = gif // "2"

	if (pattern != "") {
	    ;
	}
end
