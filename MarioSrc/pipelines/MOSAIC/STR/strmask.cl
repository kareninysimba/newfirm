#!/bin/env pipecl
#
# strmask
#
# Description:
#
#	This module makes use of the IRAF task acediff to compare a
#	non-resampled SIF to a reference stack -- presumably clean of
#	cosmic rays, artifacts, and transients -- of the same region
#	to identify pixels affected by these.  Affected pixels in the
#	non-resampled SIF are "fixed" by acediff, and these pixels are
#	identified in the object mask output from acediff.  This object
#	mask is merged with the bad pixel mask of the non-resampled SIF.
#	Pixels flagged by acediff are identified in the bad pixel mask by
#	value 8, and previously identified bad pixels retain their values
#	in the merged mask.  In cases when a previously bad pixel is also
#	flagged by acediff, that pixel retains its original value in the
#	merged bad pixel mask.

# Exit Status Values:
#
#	1 = Successful
#	2 = Unsuccessful (Currently not set in module)
#
# History:
#
#	T. Huard  200808--	Created, documented.
#	T. Huard  20080818	Cleaned up code and documentation.  Changed
#				value of bad pixel code to 8 (from 6) for
#				pixels flagged by acediff.
#	T. Huard  20080903	Replaced section of code that merged previous
#				bpm with that produced from acediff with a
#				simple mskexpr statement.
#	T. Huard  20080911	Using strnames.cl now to help define names
#				in this module.  Also, pixels identified in
#				the acediff object pixel mask are "fixed"
#				(using fixpix) in the non-resampled SIF
#				present in the current working directory. 
#	Last Revised:  T. Huard  20080915  11:50am
#


# Declare variables
string  dataset,indir,datadir,ilist,lfile
string	sifFILE,sifFILEBASE,imcropFILE,newbpmFILE,bpmORIG

# Load packages
redefine mscred = mscred$mscred.cl
noao
nproto
nfextern
ace
mscred

# Set file and path names
strnames(envget("OSF_DATASET"))
lpar names
dataset=strnames.dataset
indir=strnames.indir
datadir=strnames.datadir
ilist=strnames.ilist
lfile=strnames.lfile
sifFILE=strnames.sifFILE
sifFILEBASE=strnames.sifFILEBASE
imcropFILE=strnames.imcropFILE
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=strnames.uparm)
set (pipedata=strnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSTRmask (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Copy the non-resampled SIF to the current directory.  Then, use acediff,
# with the cropped harsh MDC stack as a reference, to identify cosmic rays,
# artifacts, and transients that may be present in the non-resampled SIF.
# acediff will then create a mask identifying the affected pixels.

newbpmFILE="" ; hselect(sifFILE,"BPM",yes) | scan(newbpmFILE)
bpmORIG=substr(newbpmFILE,1,strlstr(".pl",newbpmFILE)-1)//"ORIG.pl"
if (access(sifFILEBASE)) {
    delete(sifFILEBASE,verify-)
    delete(strnames.catdiff,verify-)
    delete(strnames.obmdiff,verify-)
}
;

# Check whether -ORIG.fits exists (from previous run of	module on
# dataset). If so, this is the original and should be renamed (only to be
# renamed again later) before continuing!

if (access(bpmORIG))
    rename(bpmORIG,newbpmFILE)
;

# Copy non-resampled SIF to current directory
imcopy(sifFILE,sifFILEBASE)

# Add a keyword for the time difference from the reference.
# This gets added to the difference catalog and is then used in
# computing moving object rates.

hselect (sifFILEBASE, "MJD-OBS,EXPTIME", yes) | scan (x, z)
x += z / 172800 
hselect (imcropFILE, "MJD-OBS", yes) | scan (y)
z = (x - y) * 86400 
hedit (sifFILEBASE, "TSEP", z, add+, verify-, show-, update+)

# Find the difference sources and create a mask and catalog.

acediff (sifFILEBASE, imcropFILE, rfrac=0.7,  extnames="", logfiles="",
    verbose=2, masks="!BPM", exps="", gains="", scales="",
    rmasks="!BPM", rexps="", rgains="", rscales="", roffset="wcs",
    catalogs=strnames.catdiff, catdefs=names.src//"pipedata/strmask.def",
    catfilter="", order="", nmaxrec=INDEF, magzero="!MAGZERO",
    cafwhm=2, gwtsig=INDEF, gwtnsig=INDEF, objmasks=strnames.obmdiff,
    omtype="all", skies="", sigmas="", fitstep=100, fitblk1d=10,
    fithclip=2., fitlclip=3., fitxorder=1, fityorder=1, fitxterms="half",
    blkstep=1, blksize=-10, blknsubblks=2, hdetect=yes, ldetect=no,
    updatesky=yes, bpdetect="1-10", bpflag="", convolve="bilinear 3 3",
    hsigma=3., lsigma=10., neighbors="8", minpix=6, sigavg=2.,
    sigmax=2., bpval=INDEF, splitstep=0., splitmax=INDEF, splitthresh=5.,
    sminpix=8, ssigavg=10., ssigmax=5., ngrow=4, agrow=3.)

# Convert the acediff objmask to a BPM, and "fix" the flagged pixels in
# the image (in current directory).

rename(newbpmFILE,bpmORIG)
mskexpr("(i==7||i>10)? 1: 0",newbpmFILE,strnames.obmdiff)
fixpix(sifFILEBASE,newbpmFILE,verbose-,pixels-)
delete(newbpmFILE,verify-)

# Merge the object pixel mask output by acediff with the previous bad pixel
# mask for the image.  A pixel identified by ace is flagged with a value of 8
# in the merged bad pixel mask, unless it was previously identified as a bad
# pixel, in which case it retains its original value in the mask.  Finally,
# fix the BPM value in the header of the acediff-corrected non-resampled
# SIF to reflect the name of the merged bad pixel mask.

mskexpr("m>0?m:(i>0? 8: 0)",newbpmFILE,strnames.obmdiff,refmask=bpmORIG)
hedit(sifFILEBASE,"BPM",newbpmFILE,verify-)

# Clean up.
delete(strnames.obmdiff,verify-)

logout(1)
