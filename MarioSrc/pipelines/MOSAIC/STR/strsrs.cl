#!/bin/env pipecl
#
# strsrs
#
# Description:
#
#	This module resamples a SIF, after it has been run through strmask, in
#	preparation for the second-pass stacking of MDC.  Most of this module has
#	been cut and paste, with minor revisions, from the srsrsp module.
#
# Exit Status Values:
#
#	1 = Successful
#	2 = Unsuccessful (Currently not set in module)
#
# History:
#
#	T. Huard  200808--	Created, documented.
#	T. Huard  20090908	Removing the added header keywords (raRSP,decRSP).
#	T. Huard  20090911	Using strnames.cl now to help define names in this module.
#	Last Revised:  T. Huard  20080911  5:30pm
#

# Declare variables
int	icheck,exitval
real	raRSP,decRSP, cd1, cd2, cd3, cd4, scale, ra, dec
string  sifFILEBASE,ref,bpm,bpm1,bpm2,bpm3
string  dataset,indir,datadir,ilist,lfile

# Load packages
redefine mscred = mscred$mscred.cl
noao
nproto
nfextern
ace
mscred

# Set file and path names
strnames(envget("OSF_DATASET"))
dataset=strnames.dataset
indir=strnames.indir
datadir=strnames.datadir
ilist=strnames.ilist
lfile=strnames.lfile
sifFILEBASE=strnames.sifFILEBASE
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=strnames.uparm)
set (pipedata=strnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSTRmask (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful)
exitval=1


##################################################################
# BEGIN:
# The following section was cut and pasted from srsrsp.cl in SRS,
# with minor changes (e.g., s1 replaced with sifFILEBASE, and 
# ra and dec replaced with raRSP and decRSP).
##################################################################
    # Check for a reference image.
    ref = ""; hselect (sifFILEBASE, "WCSREFIM", yes) | scan (ref)
    if (ref != "") {
	if (imaccess(ref) == NO)
	    ref = ""
	;
    }
    ;

    # Set output name.
    s2 = substr (sifFILEBASE, 1, strstr (".fits",sifFILEBASE)-1)
    i = strlen (s2)
    if (substr(s2,i-1,i-1) == "_")
	s2 = substr (s2, 1, i-2)
    ;
    s2 += "_r"

    # Set the input and temporary masks.
    hselect (sifFILEBASE, "bpm", yes) | scan (bpm1)	
    bpm = s2 // "_bpm.pl"
    bpm2 = "strrsp_tmp2.pl"
    bpm3 = "strrsp_tmp3.pl"
    if (access(bpm2))
	rename (bpm2, bpm1)
    ;

raRSP=0. ; hselect(sifFILEBASE,"RSPRA",yes) | scan(raRSP)
decRSP=0. ; hselect(sifFILEBASE,"RSPDEC",yes) | scan(decRSP)

    # Set scale to account for binning. Round to nearest quarter arcsec.
    #scale = rsp_scale
    hselect (sifFILEBASE, "CD*", yes) | scan (cd1, cd2, cd3, cd4)
    scale = sqrt ((cd1**2 + cd2**2 + cd3**2 + cd4**2) / 2.) * 3600.
    scale = nint (scale / 0.25) * 0.25

    # Resample the image.
    printf ("Interpolation: %s\n", rsp_interp, >> lfile)
    #rename (bpm1, bpm2)
    #imexpr ("a==3?0:a", bpm1, bpm2, verb-)
    if (ref != "") 
	mscimage (sifFILEBASE, s2, verb+, wcssource="image", reference=ref,
	    interpolant=rsp_interp, boundary="constant", constant=0.) |
	    tee (lfile)
    else {
	mscimage (sifFILEBASE, s2, verb+, wcssource="parameters",
	    ra=raRSP, dec=decRSP, scale=scale, rotation=0,
	    interpolant=rsp_interp, boundary="constant", constant=0) |
	    tee (lfile)
	hedit( s2, "rspscale", scale, add+, ver- )
    }
    #rename (bpm2, bpm1)
    s3 = substr (s2, 1, strlstr("-ccd",s2)-1) // "_r"
    hedit (s2, "IMCMBR", s3, add+, show-, ver-)
    hedit (s2, "CCDSIZE,???SEC,[ADL]T[MV]*", del+,ver-)

    # Make masks with mask values.
    imrename (bpm, bpm3)
    set pmmatch = "world 9"
    mskexpr ("m>0?m:(i==2?6:i)", bpm, bpm3, refmask=bpm1, verb-)
    hedit (bpm, "IMCMBR", bpm, add+, show-, ver-)
##################################################################
# END of cut-and-paste section
##################################################################

# Write some general information to the log file
printf("%s\n","New Resampled Image: "//s2) | tee(lfile)
printf("%s\n","Mask of New Resampled Image: "//bpm) | tee(lfile)

# Clean up.
imdelete(bpm3)
hedit(sifFILEBASE,"RSPRA",del+,ver-)
hedit(sifFILEBASE,"RSPDEC",del+,ver-)

logout(exitval)
