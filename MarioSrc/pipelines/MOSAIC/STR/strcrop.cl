#!/bin/env pipecl
#
# strcrop
#
# Description:
#
#   Extract subimage from a "cropped" (in CTR) first-pass (harsh, median) MDC
#	stack to match the field of a non-resampled SIF.  After extracting the
#	subimage, the cropped MDC stack is scaled such that its magzero will equal
#	that of the non-resampled SIF.  The sky level of the cropped MDC stack is
#	also shifted such that it will match that of the non-resampled SIF.  In
#	this way, the cropped MDC stack may be directly compared with the
#	non-resampled SIF in order to identify cosmic rays, artifacts, and
#	transients.
#
# Exit Status Values:
#
#   1 = Successful
#   2 = Unsuccessful (Currently not set in module)
#
# History:
#
#   T. Huard  200808--  Created, documented.
#   T. Huard  20080817  Cleaned up code and documentation.
#   T. Huard  20080825  Instead of prepending "strcrop_" to the basename of
#						the subimage and bpm, now "_strcrop" is appended.	
#   F. Valdes 20080910	Instead of appending "_strcrop" use "_crp" 
#   T. Huard  20080911	Using strnames.cl now to help define names in this
#						module.
#   T. Huard  20090720	Fixed the scaling factor used to match the transparency
#                       of the cropped stack to the non-resampled SIF.
#						Previously, this was the reciprocal of the scaling
#						factor needed.  Also, file was revised to conform to
#						conventional format.
#   Last Revised:   T. Huard  20090720  7:30AM
#

# Declare variables
int     exitval
string  stackFILE,sifFILE,imcropFILE,bpmcropFILE
string  dataset,indir,datadir,ilist,lfile
real    newmag0,magzero,skymean0,skymean,I0,scale,skydiff

# Load packages
redefine mscred = mscred$mscred.cl
noao
nproto
nfextern
ace
mscred

# Set file and path names
strnames(envget("OSF_DATASET"))
dataset=strnames.dataset
indir=strnames.indir
datadir=strnames.datadir
ilist=strnames.ilist
lfile=strnames.lfile
stackFILE=strnames.stackFILE
sifFILE=strnames.sifFILE
imcropFILE=strnames.imcropFILE
bpmcropFILE=strnames.bpmcropFILE
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=strnames.uparm)
set (pipedata=strnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSTRmask (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful)
exitval=1

# Determine the relative scalings.  Unlike ctrcrop.cl, the relative scaling
# here should not be unity, in general.  The cropped MDC stack will not have
# the same magzero as a non-resampled SIF.
#hselect(stackFILE,"NEWMAG0",yes) | scan(newmag0)
newmag0=INDEF ; hselect(stackFILE,"MAGZERO",yes) | scan(newmag0)
magzero=INDEF ; hselect(sifFILE,"MAGZERO",yes) | scan(magzero) 
skymean0=INDEF ; hselect(stackFILE,"SKYMEAN",yes) | scan(skymean0)
skymean=INDEF ; hselect(sifFILE,"SKYMEAN",yes) | scan(skymean)
printf("%s\n","DIAGNOSTIC:") | tee(lfile)
printf("%s\n","       MDCstack: "//stackFILE) | tee(lfile)
printf("%s\n","       non-resamp SIF: "//sifFILE) | tee(lfile)
printf("%s\n","       MAGZERO in MDCstack: "//str(newmag0)) | tee(lfile)
printf("%s\n","       MAGZERO in non-resamp SIF: "//str(magzero)) | tee(lfile)
printf("%s\n","       SKYMEAN in MDCstack: "//str(skymean0)) | tee(lfile)
printf("%s\n","       SKYMEAN in non-resamp SIF: "//str(skymean)) | tee(lfile)
###I0=10.**(-real(newmag0)/2.5)
###scale=(10.**(-real(magzero)/2.5))/I0 # scaling to make cropped reference
										# image have same magzero as
										# non-resampled SIF
I0=10.**(real(newmag0)/2.5) # Note that the variable name "I0" is somewhat
					        # misleading here since this does NOT refer to
					        # the reference transparency.  I0 here refers
					        # to the transparency of the cropped stack,
					        # which will be scaled to match that of the 
					        # individual SIF.					
scale=(10.**(real(magzero)/2.5))/I0 # scaling to make cropped reference image
									# have same magzero as non-resampled SIF

# Use mscimage to extract appropriate subimage from harsh (median) MDC stack
# for the non-resampled SIF 
if (access("tmp1.fits")) delete("tmp1.fits",verify-)
;
if (access("tmp2.fits")) delete("tmp2.fits",verify-)
;
if (access("tmp3.fits")) delete("tmp3.fits",verify-)
;
mscimage(stackFILE,"tmp1.fits",format="image",pixmask+,wcssource="match",
	reference=sifFILE,ra=INDEF,dec=INDEF,scale=INDEF,rotation=INDEF,blank=0.,
	interpol="linear",minterpol="linear",boundary="constant",constant=0.,
	fluxcon-,ntrim=8,nxblock=INDEF,nyblock=INDEF,interact-,nx=10,ny=20,
	fitgeom="general",xxorder=4,xyorder=4,xxterms="half",yxorder=4,yyorder=4,
    yxterms="half")

# Apply scaling to the cropped MDC stack, match skies, and rename the cropped
# MDC stack and BPM.  The BPM, MAGZERO, and SKYMEAN values in header of the
# cropped MDC stack are fixed to reflect these changes.
if (access(imcropFILE)) delete(imcropFILE,verify-)
;
if (access(bpmcropFILE)) delete(bpmcropFILE,verify-)
;
imarith("tmp1.fits","*",scale,"tmp2.fits")
skydiff=skymean-scale*skymean0
imarith("tmp2.fits","+",skydiff,imcropFILE)
imrename("tmp1_bpm.pl",bpmcropFILE)
hedit(imcropFILE,"BPM",bpmcropFILE,show-,ver-)
hedit(imcropFILE,"MAGZERO",magzero,show-,ver-)

# Clean up.
delete("tmp*.fits",verify-)

logout(exitval)
