# MOVREVIEW -- Review cutouts for moving objects.
#
# There is a bug in partab that truncates strings.  So for long enough
# filenames this can be a problem.

procedure movreview ()

file	movfile  = "*_mov.fits"	{prompt="Primary cutout MEF"}
int	cluster = 1		{prompt="Cluster"}
int	frame   = 1		{prompt="Primary frame"}
string	select = "XA"		{prompt="Selection pattern"}
string	accept = "A"		{prompt="Acceptance character"}
string	delete = "D"		{prompt="Deletion character"}
int	maxcluster = 3		{prompt="Maximum cluster size"}
file	movdata = "NHPPS_PIPEAPPSRC$/MOV/pipedata/"	{prompt="Data file directory\n"}

file	zmovfile = ")movreview.movfile"	{prompt="Alternate cutout MEF"}
string	zselect = "XB"		{prompt="Alternate selection pattern"}
int	zframe  = 2		{prompt="Alternate frame"}

string	space = "+"		{prompt="Map space to desired key"}
string	radii = "10,11,12"	{prompt="Radii"}
real	match = 2		{prompt="Matching distance (arcsec)"}

struct	*fd, *fd1

begin
	int	i, j, k, l, nexp, nim, ncluster, nmin, wcs, f, dir, prev, s, row
	int	c1, c2, l1, l2, nl, ltv1, dltv1, ltv2, dltv2, c1last, l1last
	real	x, y, px, py, ra, dec, ltm2, mtch
	string	key, extn, rasex, decsex, arg1, arg2
	file	movroot, movinfo, movmef, movlist, movcat, movtmp, im1, im2
	struct	cmd, line

	# Set query parameter.
	im2 = movfile
	files (im2) | count | scan (i)
	if (i != 1)
	    error (1, "Non-existent or ambiguous cutout file")
	files (im2) | scan (im2)

	# Set clobber.
	set clobber = yes

	# Set working files.
	movroot = "movrev"
	movinfo = movroot // ".info"
	delete (movroot//'*[pst]', verify-)

	# Create working files.
	if (access(movinfo)) {
	    im1 = im2
	    files (im1) | scan (im1)
	    if (strlen(im1) > 50 && strstr(".fits",im1) > 0)
	        im1 = substr (im1, 1, strstr(".fits",im1)-1)
	    movtmp = ""
	    thselect (movinfo, "COFILE", yes) | scan (movtmp)
	    if (movtmp != im1)
	        delete (movinfo, verify-)
	}
	if (!access("BAKDIR/"))
	    mkdir ("BAKDIR/")
	if (!imaccess("BAKDIR/"//im2//"[0]")) {
	    if (access(im2))
		copy (im2, "BAKDIR/")
	    else if (access(im2//".fits"))
		copy (im2//".fits", "BAKDIR/")
	}
	if (!access(movinfo)) {
	    im1 = im2
	    if (strlen(im1) > 50 && strstr(".fits",im1) > 0)
	        im1 = substr (im1, 1, strstr(".fits",im1)-1)
	    movtmp = movroot // ".tmp"
	    hselect (im1//"[0]", "CONEXP", yes) | scan (nexp)
	    mscselect (im1, "$I,COEXPID,COGRPID,CORA,CODEC",
	        extname="", > movtmp)
	    im2 = zmovfile
	    files (im2) | scan (im2)
	    if (strlen(im2) > 50 && strstr(".fits",im2) > 0)
	        im2 = substr (im2, 1, strstr(".fits",im2)-1)
	    if (im2 != im1)
		mscselect (im2, "$I,COEXPID,COGRPID,CORA,CODEC",
		    extname="", >> movtmp)
	    tinfo (movtmp, ttout-)
	    if (tinfo.nrows == 0) {
		printf ("WARNING: No Data Found\n")
		delete (movroot//"*", verify-)
		return
	    }
	    tcalc (movtmp, "ROW", "rownum", datatype="int")
	    tchcol (movtmp, "c1", "IMAGE", "", "", verbose-)
	    tchcol (movtmp, "c4", "RA", "", "", verbose-)
	    tchcol (movtmp, "c5", "DEC", "", "", verbose-)
	    tchcol (movtmp, "c3", "CLUSTER", "", "", verbose-)
	    tchcol (movtmp, "c2", "EXPID", "", "", verbose-)
	    tproject (movtmp, movinfo, "IMAGE,ROW,RA,DEC,CLUSTER,EXPID", uniq-)
	    delete (movtmp, verify-)
	    thedit (movinfo, "COFILE", im1, del-, show+)
	    thedit (movinfo, "CONEXP", nexp, del-, show-)

	    cluster = 1
	}

	# Initialize
	thselect (movinfo, "CONEXP", yes) | scan (nexp)
	tstat (movinfo, "CLUSTER", outtable="")
	nmin = 1
	mtch = match / 3600.
	ncluster = tstat.vmax
	if (cluster > ncluster)
	    cluster = 1
	dir = 1; wcs = 100 * frame; prev = 0

	# Loop through each cluster.
	key = 'r'
	while (key != 'q' && key != 'Q') {

	    # Default mapping.
	    if (key == '\\040')
	        key = space

	    # Switch on cursor command.
	    if (key == ':') {
		if (stridx ("#", cmd) == 0)
		    print (cmd) | translit ("STDIN", "=", " ") | scan (cmd)
	        i = fscan (cmd, arg1, arg2, line)
		if (i > 0) {
		    if (arg1 == "cluster") {
		        if (i > 1)
			    cluster = int (arg2)
			printf ("cluster %d\n", cluster)
		    } else if (arg1 == "frame") {
		        if (i > 1)
			    frame = int (arg2)
			printf ("frame %d\n", frame)
		    } else if (arg1 == "zframe") {
		        if (i > 1)
			    zframe = int (arg2)
			printf ("zframe %d\n", zframe)
		    } else if (arg1 == "#" || arg1 == "##") {
			# Add comment
			j = wcs / 100; i = mod (wcs, 100)
			if (i > nim)
			   goto rdcursor
			movlist = movroot // j // ".list"
			fd = movlist
			for (j=1; fscan(fd,im1)!=EOF; j+=1) {
			    if (substr(im1,1,1) == '#') {
			        j -= 1
				next
			    }
			    if (substr(im1,1,1) == "-") {
			        j = 0
				next
			    }
			    if (arg1 == "#" && j != i)
			        next
			    hedit (im1, arg2, line, add+,show+,verify-,update+)
			}
			fd = ""
		    } else if (arg1 == "maxcluster") {
			if (i > 1)
			    maxcluster = int (arg2)
			printf ("maxcluster = %d\n", maxcluster)
		    } else if (arg1 == "nmin") {
			if (i > 1)
			    nmin = int (arg2)
			printf ("nmin = %d\n", nmin)
		    } else if (arg1 == "space") {
		        if (i > 1)
			    space = arg2
			printf ("space = %s\n", space)
		    } else if (arg1 == "radii") {
		        if (i > 1)
			    radii = arg2
			printf ("radii = %s\n", radii)
		    } else if (arg1 == "match") {
		        if (i > 1)
			    mtch = real(arg2) / 3600.
			printf ("match = %.0f\n", mtch*3600.)
		    } else {
		        if (fscan (arg1, cluster) == 1) {
			    prev = cluster
			    cluster = int (arg1)
			    f = wcs / 100; key = 'r'; next
			}
		    }
		}
	    } else if (key == 'r') {
	    	# Select cutouts.
		f = wcs / 100
		movlist = movroot // f // ".list"
		movtmp = movroot // ".tmp"
		for (i=0; cluster!=i;
		    cluster=mod(ncluster+cluster-1+dir,ncluster)+1) {
		    if (f == frame)
			printf ("%s???%03d_?*\n", select, cluster) | scan(extn)
		    else
			printf ("%s???%03d_?*\n", zselect, cluster) | scan(extn)
		    match (extn, movinfo, stop-, meta+) | sort (> movtmp)
		    if (f == frame) {
			count (movtmp) | scan (nim)
			if (nim > maxcluster * nexp) {
			    concat (movtmp) |
				match ("INDEF", stop+, > movtmp)
			    count (movtmp) | scan (nim)
			}
			match ("INDEF", movtmp, stop+) | count | scan (k)
			match ("INDEF", movtmp, stop+) | fields ("STDIN", "6") |
			    sort | uniq | count | scan (l)
		    }
		    rename (movtmp, movlist)
		    print ("-----", >> movlist)
		    if (f == frame)
			printf ("%s???%03d_?*\n", zselect, cluster) | scan(extn)
		    else
			printf ("%s???%03d_?*\n", select, cluster) | scan(extn)
		    match (extn, movinfo, stop-, meta+) | sort (> movtmp)
		    if (f == zframe) {
			count (movtmp) | scan (nim)
			if (nim > maxcluster * nexp) {
			    concat (movtmp) |
				match ("INDEF", stop+, > movtmp)
			    count (movtmp) | scan (nim)
			}
			match ("INDEF", movtmp, stop+) | count | scan (k)
			match ("INDEF", movtmp, stop+) | fields ("STDIN", "6") |
			    sort | uniq | count | scan (l)
		    }
		    concat (movtmp, >> movlist); flpr concat
		    delete (movtmp, verify-)
		    if (k > 0 && nim <= maxcluster * nexp && l >= nmin)
			break
		    if (nim == 0)
		        ;
		    else if (k == 0)
			printf ("Cluster %d: No Data\n", cluster)
		    else if (l < nmin)
			printf ("Cluster %d: Not in enough images (%d)\n",
			    cluster, l)
		    else if (nim > maxcluster * nexp)
		        printf ("Cluster %d: Too big (%d)\n", cluster, nim)
		    if (i == 0)
		        i = cluster
		}
		if (k == 0 || nim > maxcluster * nexp || l < nmin) {
		    printf ("WARNING: No Data Found\n")
		    key = 'q'
		    next
		}
		if (l < 2) {
		    key = 'D'; next
		}
	    } else if (key == 'd') {
		# Mark cutout as deleted.
	        j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		movlist = movroot // j // ".list"
		fd = movlist
		for (j=1; fscan(fd,im1,row)!=EOF; j+=1) {
		    if (substr(im1,1,1) == '#') {
		        j -= 1
			next
		    }
		    if (substr(im1,1,1) == '-') {
			j = 0
			next
		    }
		    if (j != i)
			next
		    k = stridx ("[", im1) + 1; l = stridx ("]", im1) - 1
		    arg1 = 'D' // substr (im1,k+1,l)
		    hedit (im1, "EXTNAME", arg1, show-, verify-, update+)
		    im2 = substr(im1,1,k-1) // arg1 // substr(im1,l+1,999)
		    partab (im2, movinfo, "IMAGE", row)
		}
		fd = ""
		#key = 'r'; next
	    } else if (key == 'i') {
		j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		movlist = movroot // j // ".list"
		movmef = movroot // j // ".fits"
		movcat = movroot // j // ".cat"
		tdump (movlist, cd="", pf="", data="STDOUT",
		    col="", row=i) | scan (im1, row, px, py, k, l)

		# Get information.
		im2 = movmef // substr (im1, stridx("[",im1), 999)
		hselect (im2, "COIMAGE,COSEC,EXPTIME,CORATE", yes) |
		    scan (arg1, arg2, x, y)
		z = y * x / 3600.
		printf ("%s%s\n", arg1, arg2)
		printf ("EXPTIME = %d sec, RATE = %.1f arcsec/hr, \
		    TRAIL = %.1f arcsec\n", x, y, z)
	    } else if (key == 'c') {
		j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		movlist = movroot // j // ".list"
		movmef = movroot // j // ".fits"
		tdump (movlist, cd="", pf="", data="STDOUT",
		    col="", row=i) | scan (im1, row, ra, dec, k, l)
		printf ("Again:\n")
		if (fscan (imcur, x, y, wcs, key, cmd) == EOF)
		    next
		j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		movlist = movroot // j // ".list"
		movmef = movroot // j // ".fits"
		tdump (movlist, cd="", pf="", data="STDOUT",
		    col="", row=i) | scan (im2, row, px, py, k, l)
		print (ra, dec) |
		wcsctran ("STDIN", "STDOUT", im1, "world", "physical",
		    columns="1 2", units="h n", formats="",
		    min_sigdigit=9, verbose-) | scan (x, y)
		print (ra, dec) |
		wcsctran ("STDIN", "STDOUT", im2, "world", "physical",
		    columns="1 2", units="h n", formats="",
		    min_sigdigit=9, verbose-) | scan (ra, dec)
		print (px, py) |
		wcsctran ("STDIN", "STDOUT", im2, "world", "physical",
		    columns="1 2", units="h n", formats="",
		    min_sigdigit=9, verbose-) | scan (px, py)
		hselect (im1, "COIMID", yes) | scan (arg1)
		printf ("%s: (%.1f,%.1f) -> (%.1f,%.1f), (%.1f,%.1f)\n",
		    arg1, x, y, ra, dec,  px, py)
	    } else if (key == 's') {
		# Move cutout to new cluster
		s = ncluster + 1
	        j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		movlist = movroot // j // ".list"
		fd = movlist
		for (j=1; fscan(fd,im1,row)!=EOF; j+=1) {
		    if (substr(im1,1,1) == '#') {
		        j -= 1
			next
		    }
		    if (substr(im1,1,1) == '-') {
			j = 0
			next
		    }
		    if (j != i)
			next
		    k = stridx ("[", im1) + 1; l = stridx ("]", im1) - 1
		    arg1 = substr (im1, k, k+4); arg2 = substr (im1, k+8, l) 
		    printf ("%s%03d%s\n", arg1, s, arg2) | scan (extn)
		    hedit (im1, "COTILE2", s, show-, verify-, update+)
		    hedit (im1, "COGRPID", s, show-, verify-, update+)
		    hedit (im1, "EXTNAME", extn, show-, verify-, update+)
		    im2 = substr(im1,1,k-1) // extn // substr(im1,l+1,999)
		    partab (im2, movinfo, "IMAGE", row)
		    partab (s, movinfo, "CLUSTER", row)
		}
		fd = ""
		#key = 'r'; next
	    } else if (key == 'S') {
	        # Close out split.
		ncluster = s
	    } else if (key == 'A' || key == 'D') {
		# Mark cluster as accepted or deleted.
	        j = wcs / 100
		movlist = movroot // j // ".list"
		fd = movlist
		while (fscan (fd, im1, row) != EOF) {
		    if (substr(im1,1,1) == '#' || substr(im1,1,1) == '-')
			next
		    k = stridx ("[", im1) + 1; l = stridx ("]", im1) - 1
		    if (key == 'A')
		        key = accept
		    else if (key == 'D')
		        key = delete
		    arg1 = key // substr (im1,k+1,l)
		    hedit (im1, "EXTNAME", arg1, show-, verify-, update+)
		    im2 = substr(im1,1,k-1) // arg1 // substr(im1,l+1,999)
		    partab (im2, movinfo, "IMAGE", row)
		}
		fd = ""
	        cluster = mod (ncluster+cluster-1+dir, ncluster) + 1
		f = j; key = 'r'; next
	    } else if (key == 'm' || key == 'x' || key == 'X') {
		j = wcs / 100; i = mod (wcs, 100)
printf ("wcs=%d, j=%d, i=%d, nim=%d, x=%.1f, y=%.1f\n", wcs, i, j, nim, x, y)
		if (i > nim)
		   goto rdcursor
		movlist = movroot // j // ".list"
		movmef = movroot // j // ".fits"
		movcat = movroot // j // ".cat"
		tdump (movlist, cd="", pf="", data="STDOUT",
		    col="", row=i) | scan (im1, row, px, py, k, l)
		if (key == 'm') {
		    # Get coordinates.
		    im2 = movmef // substr (im1, stridx("[",im1), 999)
		    print (x, y) |
		    wcsctran ("STDIN", "STDOUT", im2, "logical", "physical",
			columns="1 2", units="", formats="",
			min_sigdigit=9, verbose-) | scan (px, py)
		    hselect (im2, "DETSEC,LTM2_2", yes) |
			translit ("STDIN", "[:,]", " ", del-) |
			scan (x, ra, y, dec, ltm2)
printf ("px=%.1f, py=%.1f", px, py)
		    py -= (y - 1) / ltm2
printf (" -> px=%.1f, py=%.1f\n", px, py)
		    print (px, py) |
		    wcsctran ("STDIN", "STDOUT", im2, "physical", "world",
			columns="1 2", units="", formats="%.2H %.1h",
			min_sigdigit=9, verbose-) | scan (rasex, decsex)
printf ("ra=%s, dec=%s\n", rasex, decsex)
		} else {
		    x = px; y = py
		    rasex = "INDEF"; decsex = "INDEF"
		}
goto rdcursor

		# Update source.
	        j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		movlist = movroot // j // ".list"
		fd = movlist
		for (j=1; fscan(fd,im1,row)!=EOF; j+=1) {
		    if (substr(im1,1,1) == '#') {
		        j -= 1
			next
		    }
		    if (substr(im1,1,1) == '-') {
			j = 0
			next
		    }
		    if (j != i)
			next
		    hedit (im1, "CORA", rasex, show-, verify-, update+)
		    hedit (im1, "CODEC", decsex, show-, verify-, update+)
		    print (im1) | translit ("STDIN", "[]", " ", del-) |
		       scan (im2, extn)
		    partab (rasex, movinfo, "RA", row)
		    partab (decsex, movinfo, "DEC", row)
		}
		fd = ""

		# If 'X' remove all references to the source within 2 arcsec.
		if (key == 'X' && !isindef(x) && !isindef(y)) {
		    movtmp = movroot // ".tmp"
		    match ("INDEF", movinfo, stop+, > movroot//"1.tmp")
		    print (x, y, l, > movroot//"2.tmp")
		    tmatch (movroot//"1.tmp", movroot//"2.tmp", movtmp,
		        "RA,DEC,EXPID", "c1,c2,c3", mtch, incol1="",
			incol2="", factor="15,1,1", sphere+,
			diagfile="", >& "dev$null")
		    fd = movtmp
		    while (fscan(fd,im1,row)!=EOF) {
		        if (substr(im1,1,1) == '#')
			    next
			hedit (im1, "CORA", rasex, show-, verify-, update+)
			hedit (im1, "CODEC", decsex, show-, verify-, update+)
			print (im1) | translit ("STDIN", "[]", " ", del-) |
			   scan (im2, extn)
			partab ("INDEF", movinfo, "RA", row)
			partab ("INDEF", movinfo, "DEC", row)
		    }
		    fd = ""
		    delete (movroot//"*.tmp", verify-)
		}
	    } else if (key == 'Z') {
		# Delete all sources at the position.
	        j = wcs / 100; i = mod (wcs, 100)
		movlist = movroot // j // ".list"
		fd = movlist
		while (fscan(fd,im1,row,x,y,k,l)!=EOF) {
		    if (substr(im1,1,1) == '#' || substr(im1,1,1) == '-')
			next
		    if (isindef(x) || isindef(y))
			next
		    movtmp = movroot // ".tmp"
		    match ("INDEF", movinfo, stop+, > movroot//"1.tmp")
		    print (x, y, l, > movroot//"2.tmp")
		    tmatch (movroot//"1.tmp", movroot//"2.tmp", movtmp,
		        "RA,DEC,EXPID", "c1,c2,c3", mtch, incol1="", incol2="",
			factor="15,1,1", sphere+, diagfile="", >& "dev$null")
		    fd1 = movtmp
		    while (fscan(fd1,im1,row)!=EOF) {
			if (substr(im1,1,1) == '#')
			    next
			hedit (im1, "CORA", "INDEF", show-, verify-, update+)
			hedit (im1, "CODEC", "INDEF", show-, verify-, update+)
			partab ("INDEF", movinfo, "RA", row)
			partab ("INDEF", movinfo, "DEC", row)
		    }
		    fd1 = ""
		    delete (movroot//"*.tmp", verify-)
		}
		fd = ""
		key = 'D'; next
	    } else if (key == 'E') {
		# Delete exposure
	        j = wcs / 100; i = mod (wcs, 100)
		movlist = movroot // j // ".list"
		tabpar (movlist, "C6", i)
		i = int (tabpar.value)
		movtmp = movroot // ".tmp"
		match ("INDEF", movinfo, stop+, > movtmp)
		fd = movtmp
		while (fscan(fd,im1,row,x,y,k,l)!=EOF) {
		    if (substr(im1,1,1) == '#')
			next
		    if (l != i)
		        next
		    hedit (im1, "CORA", "INDEF", show-, verify-, update+)
		    hedit (im1, "CODEC", "INDEF", show-, verify-, update+)
		    partab ("INDEF", movinfo, "RA", row)
		    partab ("INDEF", movinfo, "DEC", row)
		}
		fd = ""; delete (movtmp)
		nmin -= 1
	    } else if (key == 'b') {
		j = wcs / 100; i = mod (wcs, 100)
		movmef = movroot // j // ".fits"
		movtmp = movroot // ".tmp"
		mscextensions (movmef, output="file", index="",
		    extname="", extver="", lindex=no, lname=yes,
		    lver=no, dataless=no, ikparams="", > movtmp)
		fd = movtmp
		for (j=1; fscan(fd,im2,row)!=EOF; j+=1)
		    display (im2, j, bpm="", >& "dev$null")
		fd = ""; delete (movtmp, verify-)
	    } else if (key == 'p') {
	        if (prev != 0) {
		    i = cluster
		    cluster = prev
		    prev = i
		    key = 'r'; next
		}
	    } else if (key == 'z') {
	        wcs = zframe * 100
		key = 'r'; next
	    } else if (key == '?') {
	        page (movdata//"movrev.key")
	    } else if (key == '-') {
	        dir = -1
		prev = cluster
	        cluster = mod (ncluster+cluster-1+dir, ncluster) + 1
		key = 'r'; next
	    } else if (key == '+') {
	        dir = 1
		prev = cluster
	        cluster = mod (ncluster+cluster-1+dir, ncluster) + 1
		key = 'r'; next
	    } else {
	        dir = 1
		prev = cluster
	        cluster = mod (ncluster+cluster-1+dir, ncluster) + 1
		key = 'r'; next
	    }

	    # Display cutouts.
	    if (f > 0) {

		# Extract cutouts and set overlay.
		# Note we have to change the physical coordinate system
		# because currently multiple tiles with the same physical
		# coordinates don't work correctly.

		movlist = movroot // f // ".list"
		movmef = movroot // f // ".fits"
		movcat = movroot // f // ".cat"
		movtmp = movroot // ".tmp"

		fd = movlist; flpr
		for (i=0; fscan (fd, im1, row) != EOF; i += 1) {
		    if (substr(im1,1,1) == '#') {
		        i -= 1
		        next
		    }
		    if (substr(im1,1,1) == '-')
		        break
		    if (i == 0) {
		        imdelete (movmef, verify-, >& "dev$null")
			delete (movcat, verify-, >& "dev$null")
		    }
		    extn = substr (im1, stridx("[",im1)+1, stridx("]",im1)-1)
		    im2 = movmef//"[" // extn // ",append]"
		    imcopy (im1, im2, verb-)
		    im2 = movmef//"[" // extn // "]"
		    printf ("Cluster %d / %d\n", cluster, ncluster) |
		        scan (line)
		    hedit (im2, "TITLE", line, show-, verify-, update+)
		}
		fd = ""

		# Separate overlapping tiles.
		mscselect (movmef, "DETSEC,EXTNAME", expr="yes", extnames="") |
		    translit ("STDIN", "[:,]", " ", del-, > movtmp)
		fd = movtmp; c1last=0; dltv1 = 0; dltv2 = 0
		while (fscan (fd, c1, c2, l1, l2, extn) != EOF) {
		    im2 = movmef // "[" // extn // "]"
		    if (c1 == c1last) {
			i = l1
		        l1 = l1last + nl + 2
			nl = l2 - i + 1
		        l2 = l1 + nl - 1
			dltv2 += 5000
			printf ("[%d:%d,%d:%d]\n", c1, c2, l1, l2) |
			    scan (key)
			hedit (im2, "DETSEC", key,
			    update+, show+, verify-)
			hselect (im2, "$LTV1,$LTV2", yes, missing="0.") |
			    scan (ltv1, ltv2)
			ltv1 -= dltv1; ltv2 -= dltv2
			hedit (im2, "LTV1", ltv1, add+, update+, show+, verify-)
			hedit (im2, "LTV2", ltv2, add+, update+, show+, verify-)
		    } else {
			hselect (im2, "$LTV1", yes, missing="0.") | scan (ltv1)
			ltv1 -= dltv1
			hedit (im2, "LTV1", ltv1, add+, update+, show+, verify-)
			nl = l2 - l1 + 1
			dltv1 += 5000 
		        dltv2 = 0
		    }
		    c1last = c1
		    l1last = l1

		    flpr
		    ra = INDEF; dec = INDEF
		    hselect (im2, "CORA,CODEC", yes) | scan (ra, dec)
		    if (isindef(ra) || isindef(dec))
			print (ra, dec, >> movcat)
		    else {
			print (ra, dec) |
			wcsctran ("STDIN", "STDOUT", im2, "world", "physical",
			    columns="1 2", units="h n", formats="",
			    min_sigdigit=9, verbose-, >> movcat)
		    }
		}
		fd = ""; delete (movtmp, verify-)

		# Display cutouts.
		if (access(movmef)) {
		    printf ("Cluster %d / %d\n", cluster, ncluster)
		    acetvmark (movmef, frame=f, erase=yes, catalog=movcat,
			fields="C1,C2", catfilter="", wcs="physical",
			mark="circle", radii=radii,
			zcombine="none", xgap=0, ygap=0, > "dev$null")
		} else
		    printf ("Cluster %d: No Data\n", cluster)

		f = 0
	    }

	    # Read cursor.
rdcursor:
	    if (fscan (imcur, x, y, wcs, key, cmd) == EOF)
	        key = "q"
	}

	if (key == 'q')
	    delete (movroot//'*[pst]', verify-)
end
