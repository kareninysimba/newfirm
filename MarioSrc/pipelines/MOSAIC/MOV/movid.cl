procedure movid ()

string	stbid			{prompt="STB ID"}
int	grpid			{prompt="Group ID"}
string	movid			{prompt="MOV ID"}
string	dir = "to"		{prompt="Direction",
				 enum="to|from"}
bool	verbose	= yes		{prompt="Print IDs?"}

begin
	int	base = 62
	int	i, j, k, r, d, n
	string	s

	if (dir == "to") {
	    movid = ""
	    for (j = 0; j < 2; j+=1) {
		if (j == 0)
		    r = int (substr (stbid, 3, 999)) + 2383280
		else {
		    s = substr (stb, 1, 2)
		    if (s == "kp")
			r = 1000 + grpid
		    else if (s == "ct")
			r = 2000 + grpid
		    else
			r = grpid
		}
		for (i=0; base**(i+1) <= r; i=i+1)
		    ;
		for (; i >= 0; i -= 1) {
		    d = base**i
		    n = r / d
		    r = r % d
		    if (n < 10)
			n += 48
		    else if (n < 36)
			n += 55
		    else
			n += 61
		    printf ("%s%c\n", movid, n) | scan (movid)
		}
	    }
	} else {
	    for (j=0; j<2; j+=1) {
		k = strlen (movid) 
		if (j == 0)
		    s = substr (movid,1,k-2)
		else
		    s = substr (movid,k-1,k)
		k = strlen (s); r = 0
		for (i=1; i<=k; i+=1) {
		    n = substr (s, i, i)
		    if (n <= 57)
			n -= 48
		    else if (n <= 90)
			n -= 55
		    else
			n -= 61
		    r = (base * r) + n
		}
		if (j == 0) {
		    r -= 2383280
		    printf ("%d\n", r) | scan (stbid)
		} else {
		    grpid = r % 1000
		    k = grpid / 1000 
		    if (k == 1)
		        stbid = "kp" // stbid
		    else if (k == 2)
		        stbid = "ct" // stbid
		    else
		        stbid = "xx" // stbid
		}
	    }
	}

	if (verbose)
	    printf ("%s %3d %s\n", stbid, grpid, movid)
end
