# MOVDP -- Create moving data products.

procedure movdp ()

file	input = "*_mov.fits"	{prompt="Input cutout file"}
string	output = ""		{prompt="Output root name"}
string	select = "AA?*"		{prompt="Extensions to select"}
bool	update = yes		{prompt="Update cutout file?"}
file	movdata = "NHPPS_PIPEAPPSRC$/MOV/pipedata/" {prompt="Data files"}

struct	*fd

begin
	int	i, j, clid, nexp, ngrp, nobs, nmag, cllast, row
	real	xi, eta, xi1, xi2, eta1, eta2, t, t1, t2, mag, magav
	real	rate, pa
	file	in, out, out1, out2, im, tmp, tmp1, tmp2, tmp3
	string	dataset, procid, stbid, tel, filt, mpc

	int	ival, ival1
	real	rval
	string	sval
	struct  lval

	# Get query parameters.
	in = input
	out = output

	# Set temporary files.
	tmp = mktemp ("mov")
	tmp1 = tmp // "1.tmp"
	tmp2 = tmp // "2.tmp"
	tmp3 = tmp // "3.tmp"

	# Set input cutout file.
	files (in) | count | scan (i)
	if (i != 1)
	    error (1, "Unknown or ambiguous input file\n")
	files (in) | scan (in)
	i = strstr (".fits", in) - 1
	if (i > 0)
	    in = substr (in, 1, i)
	if (update) {
	    imrename (in//".fits", tmp//".fits")
	    imcopy (tmp//"[0]", in, verbose-)
	    mscextract (tmp, in, extname=select, verbose+)
	    imdelete (tmp)
	}

	# Set output.
	if (out == "") {
	    out = in
	    i = strstr ("_mov", out) - 1
	    if (i > 0)
		out = substr (out, 1, i)
	}

	if (update) {
	    # Create catalog.
	    hselect (in//"[0]", "COGAMMA,COBETA", yes) | scan (xi, eta)
	    print ("#c COGRPID i %3d", > tmp1)
	    print ("#c COEXPID i %2d", >> tmp1)
	    print ("#c CORA d %12.2h hr", >> tmp1)
	    print ("#c CODEC d %12.1h deg", >> tmp1)
	    print ("#c COTSEP r %8.1f sec", >> tmp1)
	    print ("#c COMAG r %6.2f", >> tmp1)
	    print ("#c EXTNAME ch*16 %-16s", >> tmp1)
	    printf ("#k COGAMMA = %12.2h\n", xi, >> tmp1)
	    printf ("#k COBETA = %12.2h\n", eta, >> tmp1)
	    mscselect (in, "COGRPID,COEXPID,CORA,CODEC,COTSEP,COMAG,EXTNAME",
	        expr="(CORA!='INDEF')", >> tmp1)
	    tinfo (tmp1, ttout-)
	    ngrp = tinfo.nrows
	    if (ngrp > 0) {
		tcalc   (tmp1, "N", "ROWNUM", datatype="int") 
		acecopy (tmp1, tmp2, catdef=movdata//"movdp.dat",
		    filter="", verb-)
		delete (tmp1, verify-)
		rename (tmp2, tmp1)
		tsort (tmp1, "COGRPID,COTSEP", ascend+)

		# Make cluster catalog.
		tdump (tmp1, cdfile="", pfile="", datafile=tmp3,
		    columns="COGRPID,XI,ETA,COTSEP,COMAG", rows="-", pwidth=-1)
		fd = tmp3; cllast = 0; nobs = 0; ngrp = 0
		while (fscan (fd, clid, xi, eta, t, mag) != EOF) {
		    if (clid != cllast) {
			# Output cluster information.
			if (nobs > 0) {
			    ngrp += 1
			    t1 = (t2 - t1) / 3600.
			    xi1 = (xi2 - xi1) / t1
			    eta1 = (eta2 - eta1) / t1
			    pa = datan2 (-xi1, eta1)
			    rate = sqrt (xi1**2 + eta1**2)
			    if (nmag > 0)
				magav /= nmag
			    printf ("%d %d %6.1f %6.1f %5.1f\n",
				cllast, nobs, rate, pa, magav, >> tmp2)
			}

			# Initialize new cluster.
			cllast = clid
			nobs = 0
			nmag = 0
			xi1 = xi
			eta1 = eta
			t1 = t
			magav = 0.
		    }

		    nobs += 1
		    xi2 = xi
		    eta2 = eta
		    t2 = t
		    if (!isindef(mag)) {
			nmag += 1
			magav += mag
		    }
		}
		fd = ""; delete (tmp3, verify-)

		# Output cluster information.
		if (nobs > 0) {
		    ngrp += 1
		    t1 = (t2 - t1) / 3600.
		    xi1 = (xi2 - xi1) / t1
		    eta1 = (eta2 - eta1) / t1
		    pa = datan2 (-xi1, eta1)
		    rate = sqrt (xi1**2 + eta1**2)
		    if (nmag > 0)
			magav /= nmag
		    printf ("%d %d %6.1f %6.1f %5.1f\n",
			cllast, nobs, rate, pa, magav, >> tmp2)
		}

		# Join cluster information.
		tjoin (tmp1, tmp2, tmp3, "COGRPID", "c1", extrarows="neither",
		    tolerance="0.0", casesens=yes)
		delete (tmp1, verify-)
		delete (tmp2, verify-)

		# Update cutout file.
		tdump (tmp3, cdfile="", pfile="", datafile=tmp1,
		    columns="EXTNAME,C2,C3,C4,C5",
		    rows="-", pwidth=-1)
		delete (tmp3, verify-)
		fd = tmp1
		while (fscan(fd,im,nobs,rate,pa,mag)!=EOF) {
		    printf ("%s[%s]\n", in, im) | scan (im)
		    hedit (im, "CONOBS", nobs, verify-, show-, update+)
		    hedit (im, "CORATE", rate, verify-, show-, update+)
		    hedit (im, "COPA", pa, verify-, show-, update+)
		    hedit (im, "COMAG", mag, verify-, show-, update+)
		}
		fd = ""
	    }
	    delete (tmp1, verify-)
	    hedit (in//"[0]", "CONGRP", ngrp, verify-, show-, update+)
	}

	hselect (in//"[0]", "CODATA,CONGRP", yes) |
	    scan (sval, ngrp)
	i = stridx ("-", sval)
	dataset = substr (sval, 1, i-1)
	j = strldx ("_", dataset)
	dataset = substr (sval, 1, j-1) // substr (sval, i, 999)
	procid = substr (sval, j+1, i-1)
	tel = substr (sval, i+1, 999)
	i = stridx ("-", tel)
	tel = substr (tel, i+1, i+4)

	# Make output tables.

	# Initialize SQL file.
	out2 = out // ".sql"
	if (access(out2))
	    delete (out2, verify-)
	concat (movdata//"movdp.sql", out2)

	# Dataset table.
	out1 = out // "_ds.cat"
	print ("#c dataset ch*48", > out1)
	print ("#c cutouts ch*52", >> out1)
	print ("#c nexposures i %2d", >> out1)
	print ("#c date ch*24", >> out1)
	print ("#c mjd d %15.8f", >> out1)
	print ("#c lst d %13.1h hr", >> out1)
	print ("#c ra_field d %13.2h hr", >> out1)
	print ("#c dec_field d %13.1h deg", >> out1)
	print ("#c gamma_field d %13.1h deg", >> out1)
	print ("#c beta_field d %13.1h deg", >> out1)
	print ("#c opposition d %13.1h deg", >> out1)
	print ("#c filter ch*64", >> out1)
	print ("#c ngrp i %3d", >> out1)
	hselect (in//"[0]",
	    "CODATA,COFILE,CONEXP,CODATE,COMJD,COLST,CORACEN,CODECCEN,COGAMMA,COBETA,CORACEN,COFILTER,CONGRP",
	    yes, >> out1)
	hselect (in//"[0]", "COMJD,CORACEN", yes) | scan (t, xi)
	suncoord (t, verbose-)
	pa = (mod (suncoord.ra + 12, 24) - xi) * 15
	if (pa < -180)
	    pa += 360
	if (pa > 180)
	    pa -= 360
	partab (pa, out1, "OPPOSITION", 1)

	tdump (out1, cd="", pf="", data="STDOUT", col="", rows="-", pwidth=-1) |
	    words (> tmp1)
	fd = tmp1
	while (fscan (fd, sval) != EOF) {
	    printf ("REPLACE INTO MOVDS VALUES ('%s',\n'%s',\n",
		dataset, procid, >> out2)
	    i = fscan (fd, sval); printf ("'%s',\n",  sval, >> out2)
	    i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
	    i = fscan (fd, sval); printf ("'%s',\n",  sval, >> out2)
	    i = fscan (fd, rval); printf ("%g,\n",    rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.2h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, rval); printf ("'%13.1h',\n",  rval, >> out2)
	    i = fscan (fd, lval); printf ("'%s',\n", lval, >> out2)
	    i = fscan (fd, ival); printf ("%d);\n",    ival, >> out2)
	}
	fd = ""; delete (tmp1)

	# Exposure table.
	hselect (in//"[0]", "CONEXP", yes) | scan (nexp)
	out1 = out // "_exp.cat"
	print ("#c dataset ch*48", > out1)
	print ("#c expname ch*48", >> out1)
	print ("#c expid i %2d", >> out1)
	print ("#c tsep r %d sec", >> out1)
	print ("#c exptime r %d sec", >> out1)
	for (i = 1; i <= nexp; i += 1) {
	    hselect (in//"[0]",
	        "CODATA,COIM"//i//",COEXPI"//i//",COTSEP"//i//",COEXPT"//i,
		yes, >> out1)
	}
	tsort (out1, "tsep", ascend+)

	tdump (out1, cd="", pf="", data="STDOUT", col="", rows="-", pwidth=-1) |
	    words (> tmp1)
	fd = tmp1
	while (fscan (fd, sval) != EOF) {
	    printf ("REPLACE INTO MOVEXP VALUES ('%s',\n'%s',\n",
		dataset, procid, >> out2)
	    i = fscan (fd, sval); printf ("'%s',\n",  sval, >> out2)
	    i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
	    i = fscan (fd, rval); printf ("%g,\n",    rval, >> out2)
	    i = fscan (fd, rval); printf ("%g);\n",   rval, >> out2)
	}
	fd = ""; delete (tmp1)

	if (ngrp > 0) {

	    # Group catalog.
	    out1 = out // "_grp.cat"
	    print ("#c dataset ch*48", > out1)
	    print ("#c groupid i %3d", >> out1)
	    print ("#c nobs i %2d", >> out1)
	    print ("#c rate r %5.1f arcsec/hr", >> out1)
	    print ("#c pa r %4d deg", >> out1)
	    print ("#c mag r %5.1f", >> out1)
	    mscselect (in, "CODATA,COGRPID,CONOBS,CORATE,COPA,COMAG",
		expr="(CORA!='INDEF')", >> out1)
	    tproject (out1, tmp1, "", uniq+)
	    rename (tmp1, out1)
	    tsort (out1, "rate", ascend+)

	    tdump (out1, cd="", pf="", data="STDOUT",
	        col="", rows="-", pwidth=-1) | words (> tmp1)
	    fd = tmp1
	    while (fscan (fd, sval) != EOF) {
		printf ("REPLACE INTO MOVGRP VALUES ('%s',\n'%s',\n",
		    dataset, procid, >> out2)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
		i = fscan (fd, rval); printf ("%g,\n",    rval, >> out2)
		i = fscan (fd, rval); printf ("%g,\n",    rval, >> out2)
		i = fscan (fd, rval); printf ("%g);\n",   rval, >> out2)
	    }
	    fd = ""; delete (tmp1)

	    # Observation catalog.
	    out1 = out // "_obs.cat"
	    print ("#c dataset ch*48", > out1)
	    print ("#c groupid i %3d", >> out1)
	    print ("#c expid i %2d", >> out1)
	    print ("#c imageid ch*8", >> out1)
	    print ("#c ra_obs d %13.2h hr", >> out1)
	    print ("#c dec_obs d %13.1h deg", >> out1)
	    print ("#c extname ch*16", >> out1)
	    mscselect (in, "CODATA,COGRPID,COEXPID,COIMID,CORA,CODEC,EXTNAME",
		expr="(CORA!='INDEF')", >> out1)
	    tsort (out1, "groupid,expid", ascend+)

	    # MPC catalog.
	    out1 = out // "_mpc.cat"
	    print ("#c dataset ch*48", > out1)
	    print ("#c movid ch*6", >> out1)
	    print ("#c groupid i %3d", >> out1)
	    print ("#c expid i %2d", >> out1)
	    print ("#c mpcrec ch*80", >> out1)

	    mscselect (in, "SB_ID,PHOTFILT") | sort | scan (stbid, filt)
	    tjoin (out//"_ds.cat", out//"_exp.cat", tmp1,
	        "DATASET", "DATASET")
	    tjoin (tmp1, out//"_obs.cat", tmp2,
	        "DATASET,EXPID", "DATASET,EXPID")
	    tjoin (tmp2, out//"_grp.cat", tmp3,
	        "DATASET,GROUPID", "DATASET,GROUPID")
	    delete (tmp//"[12].tmp", verify-)
	    tdump (tmp3, cd="", pf="", data=tmp1,
	        col="DATE,GROUPID,EXPID,TSEP,MAG,RA_OBS,DEC_OBS", rows="-", pwidth=-1)
	    delete (tmp3, verify-)

	    fd = tmp1
	    for (row=1; fscan(fd,sval,ival,ival1,rval,mag,xi,eta)!=EOF; row+=1) {
		mpc = ""
		movid (stbid=stbid, grpid=ival, dir="to", verbose-)
		printf ("%11.11s\n", movid.movid) | scan (lval)
		mpc += lval
	        print (sval) | translit ("STDIN", "T-", " ", del-) |
		    scan (i, j, t1, t2)
		t1 += (t2 + rval / 3600) / 24
		printf (" * C%04d %02d %08.5f\n", i, j, t1) | scan (lval)
		mpc += lval
	        printf ("%011.2h\n", xi) |
		    translit ("STDIN", ":", " ", del-) | scan (lval)
		printf (" %11s\n", lval) | scan (lval)
		mpc += lval
	        printf ("%010.1h\n", abs(eta)) |
		    translit ("STDIN", ":", " ", del-) | scan (lval)
		if (eta < 0)
		    printf (" -%10s\n", lval) | scan (lval)
		else
		    printf (" +%10s\n", lval) | scan (lval)
		mpc += lval
		printf ("%9w%5.1f %c\n", mag, " ") | scan (lval)
		mpc += lval
		if (tel == "kp4m")
		    printf ("%6w695\n") | scan (lval)
		else
		    printf ("%6w807\n") | scan (lval)
		mpc += lval
		printf ("%s %s %d %d '%s'\n",
		    dataset, movid.movid, ival, ival1, mpc, >> out1)
	    }
	    fd = ""; delete (tmp1)

	    tdump (out1, cd="", pf="", data="STDOUT",
	        col="", rows="-", pwidth=-1) | words (> tmp1)
	    fd = tmp1
	    while (fscan (fd, sval) != EOF) {
		printf ("REPLACE INTO MOVMPC VALUES ('%s',\n'%s',\n",
		    dataset, procid, >> out2)
		i = fscan (fd, sval); printf ("'%s',\n",  sval, >> out2)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
		i = fscan (fd, ival); printf ("%d,\n",    ival, >> out2)
		i = fscan (fd, lval); printf ("'%s');\n", lval, >> out2)
	    }
	    fd = ""; delete (tmp1)
	}

end
