#!/bin/env pipecl
#
# SSCCAT -- Create a science catalog.

string	dataset, indir, datadir, ifile, lfile

# Tasks and packages.
images
ace

# Directories and files.
names ("ssc", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
ifile = indir // dataset // ".ssc"
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Work in data directory.
cd (datadir)

# Log start of processing.
printf ("\nSSCCAT (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Select image to use.
s1 = ""; match ("_s.fits", ifile) | scan (s1)
if (s1 == "") {
    s3 = substr (dataset, 1, strlen(dataset)-1)
    match (s3//".fits", ifile) | scan (s1)
}
;

if (s1 == "") {
    sendmsg ("ERROR", "No data to catalog", "", "VRFY")
    logout 0
}
;

# Extract extension name to use from dataset.
s3 = substr (dataset, strldx("_",dataset)+1,strlen(dataset)-1)

# Make sky and sigma maps using the earlier object mask.
s2 = ""
match ("obm", ifile) | scan (s2)
detect (s1, masks=s2, skys=dataset//"_sky",
    sigmas=dataset//"_sig", exps="", gains="", objmasks="",
    omtype="all", catalogs="", extnames="", catdefs="pipedata$ssccat.dat",
    catfilter="", logfiles=lfile, dodetect=no, dosplit=no, dogrow=no,
    doevaluate=no, skytype="block", fitstep=100, fitblk1d=10, fithclip=2.,
    fitlclip=3., fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
    blksize=-10, blknsubblks=2, updatesky=yes, convolve="block 3 3",
    hsigma=3., lsigma=10., hdetect=yes, ldetect=no, neighbors="8",
    minpix=6, sigavg=4., sigmax=4., bpval=INDEF, splitmax=INDEF,
    splitstep=0.4, splitthresh=5., sminpix=8, ssigavg=10., ssigmax=5.,
    ngrow=2, agrow=2., magzero="!MAGZERO")

# Make new object mask and catalog using previous skys.
s2 = ""
#match ("bpm.pl", ifile) | scan (s2)
z = 0; hselect (s1, "MAGZERO", yes) | scan (z)
detect (s1, masks=s2, skys=dataset//"_sky", sigmas=dataset//"_sig",
    exps="", gains="", objmasks=dataset//"_obm", omtype="all",
    catalogs=dataset//"_cat.fits\["//s3//"]", extnames="",
    catdefs="pipedata$ssccat.dat", catfilter="", logfiles=lfile, dodetect=yes,
    dosplit=yes, dogrow=yes, doevaluate=yes, skytype="block", fitstep=100,
    fitblk1d=10, fithclip=2., fitlclip=3., fitxorder=1, fityorder=1,
    fitxterms="half", blkstep=1, blksize=-10, blknsubblks=2, updatesky=yes,
    convolve="block 3 3", hsigma=3., lsigma=10., hdetect=yes, ldetect=no,
    neighbors="8", minpix=6, sigavg=4., sigmax=4., bpval=INDEF,
    splitmax=INDEF, splitstep=0.4, splitthresh=5., sminpix=6, ssigavg=8.,
    ssigmax=5., ngrow=2, agrow=2., magzero=z)

logout 1
