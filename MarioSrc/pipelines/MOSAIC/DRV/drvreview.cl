#!/bin/env pipecl
# 
# DRVREVIEW -- Make day calibration review.

string	cal			# Calibration type (pgr|frg|sft)
string osf_mod			# module name to use in osf_update call
string osf_call			# <full path to osf_update> -a <pipeApp> -p <pipe> -f <dataset> -m <osf_mod> -s

string  module, dataset, datadir, lfile, name, png, html, host

# Load tasks and packages
task $hostname = "$!hostname -f"
task $pwd = "$!pwd"
task $mkgraphic = "$!mkgraphic"

# Get cal type.
if (fscan (cl.args, cal, osf_mod) != 2)
    logout 0
;

hostname | scan (host)

# Set names and directories.
module = envget ("NHPPS_MODULE_NAME")
names ("drv", envget ("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
name = names.shortname // "_" // cal
png = "." // name // ".png"
html = name // ".html"
cd (datadir)

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Build the graphic file.
match ("return", dataset//"-*."//cal, stop+, print-, >> module//".tmp")

# Ensure that the appropriate output directory exists
# then cd into it
if ( dps_review ) {
	if ( access ( names.rootdir // 'output/pending' ) == no )
		mkdir ( names.rootdir // 'output/pending' )
	;
	cd ( names.rootdir // 'output/pending' )
} else {
	if ( access ( names.rootdir // 'output/reviewed' ) == no )
		mkdir ( names.rootdir // 'output/reviewed' )
	;
	if ( access ( names.rootdir // 'output/reviewed/approved' ) == no )
		mkdir (  names.rootdir // 'output/reviewed/approved' )
	;
	cd (names.rootdir//'output/reviewed/approved')
}

mkgraphic ("@"//datadir//module//".tmp", png, "world", "png", 16) 
delete (datadir//module//".tmp")

# Build the web page.
printf ("<HTML><TITLE>%s</TITLE><BODY><CENTER><H1>%s</H1></CENTER>\n",
    name, name, > html)

printf ("\n", >> html)

if ( ! dps_review ) {
	# Treat the page as though it were approved (comment out the form)
	printf ("<!--", >> html)
}
;

printf ('<FORM ACTION="http://%s/cgi-bin/sendCmd.cgi" METHOD="post">', host, >> html)

# Set the USER environment variable of the CGI environment
printf ('\n<INPUT NAME="env" TYPE="hidden" VALUE="USER=%s">', envget ("USER"), >> html)

# Set the PYTHONPATH environment variable of the CGI environment
printf ('\n<INPUT NAME="env" TYPE="hidden" VALUE="PYTHONPATH=%s">', envget ("PYTHONPATH"), >> html)

# Insert a radio selection device to allow the user to choose between the 
# Accepted (Y), Rejected (N), and the Skip (S) status flags

# setup osf_call
osf_call = envget ("NHPPS_DIR_BASE") // "/bin/osf_update "
osf_call = osf_call // "-a " // envget ("NHPPS_SYS_NAME") // " -p drv -f " // dataset // " -m " // osf_mod // " -s"

printf ('\nAccept: <INPUT NAME="cmd" TYPE="radio" VALUE="%s Y" tabindex="1" checked>', osf_call, >> html)
printf ('\nReject: <INPUT NAME="cmd" TYPE="radio" VALUE="%s N" tabindex="2">', osf_call, >> html)
printf ('\nSkip: <INPUT NAME="cmd" TYPE="radio" VALUE="%s S" tabindex="3">', osf_call, >> html)

# Insert submit button
printf ('\n<INPUT TYPE="submit">', >> html)

# End the form
printf ("\n</FORM>", >> html)

if ( ! dps_review ) {
	# Treat the page as though it were approved (comment out the form)
	printf ("-->", >> html)
}
;

printf ('\n<CENTER><IMG SRC="%s"></CENTER>', png, >> html)
printf ("\n</BODY></HTML>\n", >> html)

if (dps_review) {
    pwd | scan (s2)
    printf ("http://%s%s/%s\n", host, s2, html) | scan (s3)
    sendmsg ("REVIEW", s3, "", "MAIL")
    logout 2
} else
    logout 1
