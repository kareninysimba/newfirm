#!/bin/env pipecl
# 
# DRVMVREVIEW -- Relocate the review files after the they have been reviewed.

string  lfile, name, html, resp, cal, dir

task $removeform = "$!drvdisableform.py $1"

# Get approval response
if (fscan (cl.args, cal) != 1)
    logout 0
;

# Set names and directories.
names ("drv", envget ("OSF_DATASET"))
lfile = names.datadir // names.lfile
name = names.shortname // "_" // cal
html = name // ".*"

resp = envget ("OSF_FLAG")
if( resp == "Y" )
	resp = "approved"
;
if( resp == "N" )
	resp = "rejected"
;
if( resp == "S" )
	resp = "skipped"
;

dir = names.rootdir // 'output/reviewed/' // resp // '/'

if ( access ( names.rootdir // 'output/pending' ) == no )
	logout 0
;

if ( access ( names.rootdir // 'output/reviewed' ) == no )
	mkdir ( names.rootdir // 'output/reviewed' )
;

if ( access ( dir ) == no )
	mkdir ( dir )
;

# Log start of module.
printf ("\nDRVMVREVIEW (%s): ", names.dataset) | tee (lfile)
time | tee (lfile)

cd (names.rootdir//"output")

# Move the file from the pending to the reviewed directory
movefiles ( 'pending/' // html, dir )
movefiles ( 'pending/.' // html, dir )

# Comment out the form element from the reviewed page
removeform( dir // name // ".html" )

logout 1
