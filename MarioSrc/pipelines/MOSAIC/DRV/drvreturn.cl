#!/bin/env pipecl
# 
# DRVRETURN -- Return flag which triggered this module.

string	p			# Return pipeline
int	c			# Return stage index

string  module, dataset, datadir, lfile, s, h, f

# Load tasks and packages
task $osf_update = "$!osf_update -a $6 -h $1 -p $2 -f $3 -c $4 -s $5"

# Get pipeline and index
if (fscan (cl.args, p, c) != 2)
    logout 0
;

# Set flag value.  This is the flag used to run this module.
s = envget ("OSF_FLAG")

# Set names and directories.
module = envget ("NHPPS_MODULE_NAME")
names (envget("NHPPS_SUBPIPE_NAME"), envget ("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
cd (datadir)

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Delete the files and trigger the pipeline.
concat (dataset//"-*."//p, >> module//".tmp")
list = module // ".tmp"
while (fscan (list, s1) != EOF) {
    h = substr (s1, 1, stridx("!",s1)-1)
    f = substr (s1, 1, strldx("/",s1)-1)
    f = substr (f, strldx("/",f)+1, 1000)
    osf_update (h, p, f, c, s, envget ("NHPPS_SYS_NAME"))
}
list = ""; delete (module//".tmp")

logout 1
