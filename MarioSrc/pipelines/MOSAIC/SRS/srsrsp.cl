#!/bin/env pipecl
#
# SRSRSP -- Resample images in input list.
#

int	nrspgrp
real	ra, dec, cd1, cd2, cd3, cd4, scale
string	dataset, datadir, ifile, lfile, ref, bpm, bpm1, bpm2, bpm3, sif, interp
struct	*fd

# Tasks and Packages.
redefine mscred=mscred$mscred.cl
noao
nproto
mscred
cache mscred

# Set paths and files.
names ("srs", envget("OSF_DATASET"))
dataset = names.dataset
ifile = names.indir // dataset // ".srs"
datadir = names.datadir
lfile = datadir // names.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Resample the images.
list = ifile 
while (fscan (list, s1, ra, dec) != EOF) {
    # Check for a reference image.
    ref = ""; hselect (s1, "WCSREFIM", yes) | scan (ref)
    if (ref != "") {
	if (imaccess(ref) == NO)
	    ref = ""
	;
    }
    ;

    # Set output names.
    s2 = substr (s1, strldx("/",s1)+1, strstr (".fits",s1)-1)
    i = strlen (s2)
    if (substr(s2,i-1,i-1) == "_")
	s2 = substr (s2, 1, i-2)
    ;
    sif = s2 // "_sif.list"
    s2 += "_r"

    if (imaccess(s2))
	next
    ;

    # Set the input and temporary masks.
    hselect (s1, "bpm", yes) | scan (bpm1)	
    bpm = s2 // "_bpm.pl"	
    bpm2 = "srsrsp_tmp2.pl"
    bpm3 = "srsrsp_tmp3.pl"
    if (access(bpm2))
        rename (bpm2, bpm1)
    ;

    # Set scale to account for binning. Round to nearest quarter arcsec.
    #scale = rsp_scale
    hselect (s1, "CD*", yes) | scan (cd1, cd2, cd3, cd4)
    scale = sqrt ((cd1**2 + cd2**2 + cd3**2 + cd4**2) / 2.) * 3600.
    scale = nint (scale / 0.25) * 0.25

    # Set interpolation type based on whether a second pass will be done.
    nrspgrp = 0; hselect (s1, "NRSPGRP", yes) | scan (nrspgrp)
    if (nrspgrp<mdc_n2pass || str_trrej=="none")
	interp = rsp_interp
    else
	interp = "linear"

    # Resample the image.
    printf ("Interpolation: %s\n", interp, >> lfile)
    #rename (bpm1, bpm2)
    #imexpr ("a==3?0:a", bpm1, bpm2, verb-)
    if (ref != "") 
	mscimage (s1, s2, verb+, wcssource="image", reference=ref,
	    interpolant=interp, boundary="constant", constant=0) | tee (lfile)
    else {
	mscimage (s1, s2, verb+, wcssource="parameters", ra=ra, dec=dec,
	    scale=scale, rotation=0, interpolant=interp,
	    boundary="constant", constant=0) | tee (lfile)
	hedit( s2, "rspscale", scale, add+, ver- )
    }
    #rename (bpm2, bpm1)
    s3 = substr (s2, 1, strlstr("-ccd",s2)-1) // "_r"
    hedit (s2, "IMCMBR", s3, add+, show-, ver-)
    hedit (s2, "CCDSIZE,???SEC,[ADL]T[MV]*", del+)

    # Make masks with mask values.
    imrename (bpm, bpm3)
    set pmmatch = "world 9"
    mskexpr ("m>0?m:(i==2?6:i)", bpm, bpm3, refmask=bpm1, verb-)
    hedit (bpm, "IMCMBR", bpm, add+, show-, ver-)

#    imdelete (bpm1)	# DO NOT DELETE bpm1!!  NEED THIS MASK TO REMAIN FOR ACCESS BY TWO-PASS STACKING (MDC)
    delete (bpm3)

    # Leave file pointing to SIF and BPM used.
    print (s1, > sif)
    pathname (bpm1, >> sif)
}
list = ""

logout 1
