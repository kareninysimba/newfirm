#!/bin/env pipecl
#
# SRSSETUP -- Setup SRS processing data. 
# 
# The input list of images is assumed to contain the uparm keyword.

string	dataset, datadir, ifile, lfile
int	founddir

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Set paths and files.
names ("srs", envget("OSF_DATASET"))
dataset = names.dataset
ifile = names.indir // dataset // ".srs"
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Work in the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Setup UPARM.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
s1 = ""; head (ifile, nlines=1) | scan (s1)
iferr {
    setdirs (s1)
} then {
    logout 0
} else
    ;

# Remove images which don't have WCSCAL.  This may have been done by the
# calling pipeline but we do it here for robustness.
# Get the bad pixel masks and remove from input list.  Note that this
# may leave a bad pixel file around but we will ignore it for now.

list = ifile
while (fscan (list, s1, line) != EOF) {
    if (strstr("bpm",s1) != 0) {
	imcopy (s1, ".", verbose-)
	next
    }
    ;

    s2="F"; hselect (s1, "WCSCAL", yes) | scan (s2)
    if (s2 != "F")
	printf ("%s %s\n", s1, line, >> "srssetup.tmp")
    ;
}
list = ""

if (access("srssetup.tmp")) {
    rename ("srssetup.tmp", ifile)
    logout 1
} else {
    printf ("WARNING: No data found\n")
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 0
}
