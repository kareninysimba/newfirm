#!/bin/env pipecl
#
# RSPSETUP -- Setup RSP processing data. 
# 
# The input list of images is assumed to contain the uparm keyword.

string	dataset, datadir, ifile, lfile
int	founddir

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Set paths and files.
rspnames (envget("OSF_DATASET"))
dataset = rspnames.dataset
ifile = rspnames.indir // dataset // ".rsp"
datadir = rspnames.datadir
lfile = datadir // rspnames.lfile
set (uparm = rspnames.uparm)
set (pipedata = rspnames.pipedata)

# Work in the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Setup UPARM.
delete (substr(rspnames.uparm, 1, strlen(rspnames.uparm)-1))
s1 = ""; head (ifile, nlines=1) | scan (s1)
iferr {
    setdirs (s1)
} then {
    logout 0
} else
    ;

# Remove images which don't have WCSCAL.  This may have been done by the
# calling pipeline but we do it here for robustness.  Note that removing
# an image doesn't remove the bad pixel.

list = ifile
while (fscan (list, s1) != EOF) {
    if (strstr("bpm",s1) != 0) {
	print (s1, >> "rspsetup1.tmp")
	next
    }
    ;

    s2="F"; hselect (s1, "WCSCAL", yes) | scan (s2)
    if (s2 != "F") {
	print (s1, >> "rspsetup.tmp")
	print (s1, >> "rspsetup1.tmp")
    }
    ;
}
list = ""

if (access("rspsetup.tmp")) {
    rename ("rspsetup.tmp", rspnames.indir//dataset//".dat")
    rename ("rspsetup1.tmp", ifile)
    logout 1
} else {
    delete ("rspsetup*.tmp")
    printf ("WARNING: No data found\n")
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 0
}
