#!/bin/env pipecl
#
# RSPTAN -- Set tangent points and group exposures for resampling and stacking.
#
# The input is a list of images.

int	nrspgrp
real	ra, dec
string	dataset, datadir, ifile, ifile1, lfile, rspgrp, rspord, bpm
struct	*fd

# Tasks and Packages.
images
task skysep	= "NHPPS_PIPESRC$/MOSAIC/RSP/skysep.cl"; cache skysep
task skygroup	= "NHPPS_PIPESRC$/MOSAIC/RSP/skygroup.cl"
task rspgrid	= "NHPPS_PIPESRC$/MOSAIC/RSP/rspgrid.cl"

# Set paths and files.
rspnames (envget("OSF_DATASET"))
dataset = rspnames.dataset
ifile = rspnames.indir // dataset // ".rsp"
ifile1 = rspnames.indir // dataset // ".dat"
datadir = rspnames.datadir
lfile = rspnames.lfile
cd (datadir)

# Clean up from previous error.
delete ("rsptan*tmp,rsp2srs*tmp")

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Get the pointing information.  Use the MEF headers.
match ("_00.fits", ifile1, > "rsptan2.tmp")
#hselect ("@rsptan2.tmp", "crval1,crval2,$I", yes, > "rsptan1.tmp")
hselect ("@rsptan2.tmp", "ra,dec,$I", yes, > "rsptan1.tmp")
delete ("rsptan2.tmp")

# Group the exposures.
skygroup ("rsptan1.tmp", "rsptan", extn=".tmp", sep=600, raunit="hr",
    raformat="%.2h", decformat="%.1h", keepcoords+, >> lfile)
delete ("rsptan1.tmp")

# Go through each group and assign the group name and tangent point.
# Each group forms a list in the lists of lists for CALL.
# The group name is the first name in the sorted MEF group list.  Note
# that by using the MEFs it is possible to have an exposure without
# an SIF because the SIF failed to be processed.  The goal is to have
# the same grouping and group name across all CCDs which are being
# done in parallel.

list = "rsptan.tmp"
while (fscan(list,s3) != EOF) {
    fd = s3
    while (fscan (fd, s1, x, y) != EOF) {
	s1 = substr (s1, strldx("-",s1)+1, strldx("_",s1)-1)
	printf ("%s %g %g\n", s1, x, y, >> "rsptan1.tmp")
    }
    fd = ""; delete (s3)
    sort ("rsptan1.tmp", > s3)
    delete ("rsptan1.tmp")

    fd = s3; rspgrp = ""; count (s3) | scan (nrspgrp)
    for (i=1; fscan (fd, s1, x, y) != EOF; i+=1) {
	# Set the tangent point and group name from the first entry.
        if (rspgrp == "") {
	    rspgrp = s1
	    rspgrid (x, y, grid=rsp_grid) | scan (ra, dec)
            printf ("--- %.0h %0.h %s ---\n", ra, dec, s1, >> lfile)
	}
	;

	# Find the CCD image from the MEF.
	s2=""; match (s1//"-ccd", ifile1) | scan (s2) 
	if (s2 == "")
	    next
	;
	s1 = s2
	print (s1, >> lfile)

	# Set the order string.
	j=1; hselect (s1, "IMAGEID", yes) | scan (j)
	printf ("%03d%02d\n", i, j) | scan (rspord)

	# Set the header keywords and output file.  The grouping keyword
	# is used to for grouping and to define the name for the output
	# group image.  The order parameter defines the order for the
	# imcombine list.  The number in the group is used to decide
	# whether a stack should be created.  The RA and DEC values store
	# the tangent point for resampling.

	hedit (s1, "RSPGRP", rspgrp, add+, show-, ver-)
	hedit (s1, "RSPORD", "dummy", add+, show-, ver-)
	hedit (s1, "RSPORD", rspord, add+, show-, ver-)
	hedit (s1, "NRSPGRP", nrspgrp, add+, show-, ver-)
	hedit (s1, "RSPRA", ra, add+, show-, ver-)
	hedit (s1, "RSPDEC", dec, add+, show-, ver-)
	printf ("%.0h %.0h %s\n", ra, dec, s1, >> "rsptan1.tmp")

	# Extract image dataset name.
	s2 = substr (s1, strldx("/",s1)+1, strldx("-",s1)-1)
	s2 = substr (s2, strldx("-",s2)+1, 1000)

	# Set output files.
	s2 = "rsp2srs_" // s2 // ".tmp"
	path (s2, >> "rsp2srs.tmp")
	printf ("%s %.0h %.0h\n", s1, ra, dec, >> s2)

	bpm = ""; hselect (s1, "BPM", yes) | scan (bpm)
	if (bpm != "")
	    match (bpm, ifile, >> s2)
	;
    }
    fd = ""
    rename ("rsptan1.tmp", s3)
}
list = ""

logout 1
