#!/bin/env pipecl
#
# mdcgrp
# 
# Prepare input lists and list of these lists for call to the CTR pipeline.
# The first row of each input list contains the full pathname to the
# first-pass (median or "harsh") stack created by mdcstack1.  The remaining
# rows list the full pathnames for the resampled SIFs of a given extension
# used to construct the harsh stack.  There is one list created for each
# extension (ccd1.list, ccd2.list, ..., ccd8.list) and a list of these lists
# (mdcgrpALL.list) used in the call statement.
# 
# Exit Status Values:
# 
#   1 = Successful
#   2 = Unsuccessful  (Currently not set in module)

# Declare variables
int	iccd
string	dataset, indir, datadir, ilist, lfile, shortname, datadirFULL

# Load packages
proto

# Set file and path names
mdcnames (envget("OSF_DATASET"), pass=1)
print ("DEBUG: mdcnames.datadir in MODULE mdcgrp: "//mdcnames.datadir)
print ("DEBUG: mdcnames.datadirFULL in MODULE mdcgrp: "//mdcnames.datadirFULL)
dataset = mdcnames.dataset
indir = mdcnames.indir
datadir = mdcnames.datadir
ilist = mdcnames.ilist
lfile = mdcnames.lfile
shortname = mdcnames.shortname
datadirFULL = mdcnames.datadirFULL
print ("datadirFULL-begin"//datadirFULL)
set (uparm=mdcnames.uparm)
set (pipedata=mdcnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Create lists (ccd1.list, ccd2.list, ...) and list of lists (mdcgrpALL.list)
# for call to the CTR pipeline

if (access("mdcgrpALL.list")) {
	delete ("mdcgrpALL.list", verify-)
	delete ("ccd*.list", verify-)
}
;

for (iccd=1; iccd<=8; iccd+=1) {
	match ("ccd"//iccd//"_r.fits", ilist) | count | scan (i)
	if (i == 0)
	    next
	;
	printf ("%s\n", datadirFULL//"ccd"//iccd//".list",
	    >>"mdcgrpALL.list")
	printf ("%s\n", datadirFULL//shortname//"_sk1.fits",
	    >"ccd"//iccd//".list")
	match ("ccd"//iccd//"_r.fits", ilist, >>"ccd"//iccd//".list")
}
copy ("mdcgrpALL.list", "mdcgrpALL-copy.list", verbose-)

logout(1)
