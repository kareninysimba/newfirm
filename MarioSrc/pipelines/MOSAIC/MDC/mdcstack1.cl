#!/bin/env pipecl
#
# MDCSTACK1 -- First-pass stacking of SIFs from SRS.
# This is a harsh (median) stack to eliminate transients.

# Declare variables
string  dataset, indir, datadir, ilist, lfile
string	rspord, filename, skymean, magzero, magzero0
real	I0, scale

# Load packages
images
servers
utilities
proto

# Set file and path names
mdcnames (envget("OSF_DATASET"),  pass=1)
dataset = mdcnames.dataset
indir = mdcnames.indir
datadir = mdcnames.datadir
ilist = mdcnames.ilist
lfile = mdcnames.lfile
set (uparm=mdcnames.uparm)
set (pipedata=mdcnames.pipedata)

cd (datadir)
delete ("mdcstack*.tmp")

# Log start of processing.
printf ("\nMDCSTACK1 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Read the headers of the resampled SIFs in order to run some basic checks
# and help with stacking properly.

hselect("@"//ilist,"RSPORD,$I,SKYMEAN,MAGZERO",yes) |
    sort ( > "mdcstack1.tmp")

# Compute the relative scaling factors (derived from MAGZERO values) and
# zeros (additive shifts derived from the SKYMEAN values) for the resampled
# SIFs and write these to their FITS headers (create new keywords MDCSKY1
# and MDCSCALE1).  In this way, imcombine will be able to use these values to
# properly scale and shift the individual resampled SIFs.  IMPORTANT NOTE:
# The SKYMEAN values in the SIF headers are negative those necessary to be
# useful for imcombine to subtract the sky before stacking the SIFs.  Also,
# in imcombine, the scaling is applied before the zero (see documentation
# for imcombine).  However, the task accounts for the change in zero
# (new zero is -SKYMEAN*SCALE) resulting from rescaling.  So, the effect
# of scale on the zero should not be done explicitly before inputting the
# scales into imcombine.

list = "mdcstack1.tmp"; delete (ilist); I0 = INDEF
while (fscan(list,rspord,filename,skymean,magzero) != EOF) {
    if (nscan() < 4)
        next
    ;
    print (filename, >> ilist)
    if (isindef(I0)) {
	magzero0 = magzero
###	I0 = 10.**(-real(magzero)/2.5)
	I0 = 10.**(real(magzero)/2.5)
	hedit (filename, "MDCSKY1", str(-real(skymean)), add+)
	hedit (filename, "MDCSCALE1", str(1.0), add+)
    } else {
###	scale = I0/(10.**(-real(magzero)/2.5))
	scale = I0/(10.**(real(magzero)/2.5))
	hedit (filename, "MDCSKY1", str(-real(skymean)), add+)
	hedit (filename, "MDCSCALE1", str(scale), add+)
    }
}
list = ""; delete ("mdcstack1.tmp")

# Combine the resampled SIFs in the sequence, using the WCS information
# included in their headers to align the images.  In combining, the MEDIAN
# of pixel values in a stacked pixel column is used, after rejecting the
# pixels identified in the bad pixel masks.

imdelete (mdcnames.seqlabel, >& "dev$null")
imdelete (mdcnames.bpm, >& "dev$null")
imdelete (mdcnames.expmask, >& "dev$null")
count (ilist) | scan (i)
if (i > 480) {
    list = ilist
    while (fscan(list,s1)!=EOF) {
        s2 = substr (s1, strldx("/",s1)+1, 999)
	s2 = substr (s2, 1, strstr("-ccd",s2)-1)
	print (s2, >> "mdcstack1.tmp")
    }
    list = ""
    sort ("mdcstack1.tmp") | unique (> "mdcstack1.tmp")
    list = "mdcstack1.tmp"
    while (fscan(list,s1)!=EOF) {
	if (imaccess(s1)==YES) {
	    imdelete (s1)
	    imdelete (s1//"_bpm.pl")
	}
	;
        match (s1, ilist, > "mdcstack2.tmp")
	imcombine ("@mdcstack2.tmp", s1,
	    bpmasks=s1//"_bpm.pl", headers="", rejmask="",
	    nrejmask="", expmask="", imcmb="",
	    logfile=lfile, combine="average", reject="none",
	    project-, outtype="real", outlimits="", offsets="wcs",
	    masktype="novalue", maskvalue=1, blank=0, scale="none",
	    zero="none", weight="none", statsec="", expname="",
	    lthresh=INDEF, hthresh=INDEF)
	flpr imcombine
	print (s1, >> "mdcstack3.tmp")
	print (s1, >> "mdcstack4.tmp")
	print (s1//"_bpm.pl", >> "mdcstack4.tmp")
    }
    list = ""; delete ("mdcstack1.tmp,mdcstack2.tmp")

} else
    copy (ilist, "mdcstack3.tmp")

imcombine ("@mdcstack3.tmp", mdcnames.seqlabel, bpmasks=mdcnames.bpm//".pl",
    headers="", rejmask="", nrejmask="", expmask=mdcnames.expmask//".pl",
    imcmb="IMCMBR",  logfile=lfile, combine="median", reject="none",
    project-, outtype="real", outlimits="", offsets="wcs",
    masktype="novalue", maskvalue=1, blank=0, scale="!MDCSCALE1",
    zero="!MDCSKY1", weight="none", statsec="", expname="", lthresh=INDEF,
    hthresh=INDEF)

if (imaccess(mdcnames.seqlabel)==NO)
    logout 0
;

# Set headers.
hedit(mdcnames.seqlabel,"SKYMEAN",str(0.00),add+)
hedit(mdcnames.seqlabel,"MAGZERO",str(magzero0),add+)

# Clean up.
if (access("mdcstack4.tmp"))
    imdelete ("@mdcstack4.tmp")
;
#imdelete ("*ccd*_sk1*")
delete ("mdcstack*.tmp")

logout 1
