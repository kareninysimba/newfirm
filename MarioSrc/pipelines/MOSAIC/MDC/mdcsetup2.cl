#!/bin/env pipecl
#
# MDCsetup2
#
# Description:
#
# 	This module copies the new badpixel masks created by the STR pipeline
#	to the current directory.
#
# Exit Status Values:
#
# 	1 = Successful
#	
# History:
# 
# 	T. Huard  20080815  Created.	
#	T. Huard  20080828  Added information to log about number of bpms expected but not copied.
#	T. Huard  20080911  Using mdcnames.cl now to help define names in this module.
# 	Last Revised: T. Huard  20080911  1:20pm

# Declare variables
int	exitval,icount,icountBAD
string  dataset,indir,datadir,ilist,lfile
string  listfile

# Load packages
proto

# Set file and path names
mdcnames(envget("OSF_DATASET"), pass=2)
dataset=mdcnames.dataset
indir=mdcnames.indir
datadir=mdcnames.datadir
ilist=mdcnames.ilist
lfile=mdcnames.lfile
set (uparm=names.uparm)
set (pipedata=names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nMDCsetup2 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful)
exitval=1

# Search list file in datadir to obtain the full pathnames of the
# bad pixel masks for the new resampled SIFs created by STR.
if (access("tmp.list")) delete("tmp.list",verify-)
;
listfile=datadir//dataset//".list"
match("/MOSAIC_STR/data/",listfile) | match("_r_bpm.pl",>"tmp.list")

# Copy the bad pixel masks for the resampled SIFs to the current directory
if (access("tmp2.list")) delete("tmp2.list",verify-)
;
icount=0
icountBAD=0
list="tmp.list"
while (fscan(list,s1) != EOF) {
	s2=substr(s1,strldx("/",s1)+1,strlen(s1))
	if (access(s2)) delete(s2,verify-)
	;
	imcopy(s1,s2,verbose-)
	if (access(s2)) icount=icount+1
	else icountBAD=icountBAD+1
}
list=""
delete("tmp.list",verify-)

# Write some general information to the log file
printf("%s\n","MDCsetup2: Number of badpixel masks copied: "//str(icount)) | tee(lfile)
if (icountBAD >= 1) {
	printf("%s\n","MDCsetup2: Number of badpixel masks EXPECTED BUT NOT FOUND: "//str(icountBAD)) | tee(lfile)
	printf("%s\n","***************** SOME BPMs NOT FOUND!!! *****************") | tee(lfile)
}
;

logout(exitval)

