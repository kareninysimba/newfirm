#!/bin/env pipecl
#
# DAY2RSP -- Send list of images to the RSP pipeline.
#
# This module sets the list of images and then uses call.cl to call the
# pipeline.

if (day_dorsp == NO)
    logout 1
;

int	status = 2
string  dataset, indir, datadir, ilist, olist, temp1, temp2

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"
images

# Setup directories and files.
daynames (envget ("OSF_DATASET"))
dataset = daynames.dataset
indir = daynames.indir
datadir = daynames.datadir
ilist = indir // dataset // ".day"
olist = "day2rsp1.tmp"
temp1 = "day2rsp2.tmp"
temp2 = "day2rsp3.tmp"
set (uparm = daynames.uparm)
cd (datadir)

# Create list to process.
i = 0
if (access (dataset//".sft")) {
    match ("_s.fits", "@"//dataset//".sft", print-, > olist)
    count (olist) | scan (i)
}
;
if (i == 0 && access (dataset//".frg")) {
    match ("_f.fits", "@"//dataset//".frg", print-, > olist)
    count (olist) | scan (i)
}
;
if (i == 0 && access (dataset//".pgr")) {
    match ("_p.fits", "@"//dataset//".pgr", print-, > olist)
    count (olist) | scan (i)
}
;
if (i == 0) {
    match ("ccd[0-9]*.fits", ilist, > olist)
    count (olist) | scan (i)
}
;

# Check for data with WCS calibration.
if (i != 0) {
    rename (olist, temp1)
    hselect ("@"//temp1, "$I", "(WCSCAL)", > olist)
    count (olist) | scan (i)
}
;

if (i == 0) {
    delete (olist)
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 1
}
;

# Add bad pixel masks.
match ("ccd[0-9]*_bpm.pl", ilist, >> olist)

# Add MEF headers.
match ("_00.fits", ilist, >> olist)

# Call pipeline.
callpipe ("day", "rsp", "copy", datadir//olist, temp1, temp2)
concat (temp2) | scan (status)
concat (temp1)
delete ("day2rsp*.tmp")

logout (status)
