#!/bin/env pipecl
#
# DAYEVAL -- Evaluate object masks to determine if stacks can be made.
#
# This routine has two entries.  The first is to calculate the histogram
# and call the FTR pipeline and the second is to respond to the result
# from the FTR pipeline.
#
# status:
#    1=Wait for FTR pipeline to evaluate all CCDs
#    2=Continue with sky flat steps
#    3=Continue with sky flat steps using calibration library
#    4=Skip sky flat steps

int	status = 1
string	dataset, datadir, ifile, lfile

# Tasks and packages
utilities
task $osf_test = "$!osf_test -a $2 -p ftr -f $1"
task $pipeselect = "$!pipeselect $1 $2 0 0"
images

# Set paths and files.
daynames (envget("OSF_DATASET"))
dataset = daynames.dataset
datadir = daynames.datadir
ifile = daynames.indir // dataset // ".day"
lfile = datadir // daynames.lfile
set (uparm = daynames.uparm)
cd (datadir)

delete ("dayeval*")

# Respond to flag value by setting logout status to do or skip calibrations.
s1 = envget ("OSF_FLAG")
printf( "Flag read from blackboard: %s\n", s1 )
if (s1 == "Y")
    status = 2
else if (s1 == "N") {
    sendmsg ("WARNING", "Bad sky coverage for day calibrations", "", "CAL")
    printf ("WARNING: Bad sky coverage for day calibrations\n", >> lfile)
    if (strstr("lib",pgr_cal)>0 || strstr("lib",frg_cal)>0 ||
	strstr("lib",sft_cal)>0)
	status = 3
    else
        status = 4
}
;

if (status != 1)
    logout (status)
;

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Compute sum of object masks.
match ("obm", ifile, > "dayeval2.tmp")
list = "dayeval2.tmp"
for (i=1; fscan(list,s1)!=EOF; i+=1) {
    s2 = s1 // "[pl]"
    s3 = "N"; hselect (s2, "SFTFLAG", yes) |
	translit ("STDIN", '"', delete+) | scan (s3)
    if (s3 != "Y") {
        i -= 1
        next
    }
    ;
    if (i == 1)
	imexpr ("min(a,1)", "dayeval1.pl", s2, verb-)
    else {
	# Workaround for undiagnosed error "can't update header in dayeval2.pl".
	if (mod(i,20) == 0)
	    flpr
	;
	imexpr ("min(a,1)+b", "dayeval2.pl", s2, "dayeval1.pl", verb-)
	rename ("dayeval2.pl", "dayeval1.pl")
    }
}
list = ""; delete ("dayeval2.tmp")

# Compute histogram.  If no data respond with continue calibrations.
if (imaccess("dayeval1.pl")) {
    imhist ("dayeval1.pl", z1=0, z2=i, list+, > "dayeval1.tmp")
    concat ("dayeval1.tmp", >> lfile)
} else
    logout 2

# Trigger ftr pipeline to combine histograms.

# Find pipeline.
pipeselect (envget ("NHPPS_SYS_NAME"), "ftr", > "dayeval2.tmp")
count ("dayeval2.tmp") | scan (i)
s1 = ""
if (i == 1)
    head ("dayeval2.tmp") | scan (s1)
else if (i > 1) {
    osf_test (daynames.parent, envget ("NHPPS_SYS_NAME")) | scan (s2)
    s2 = substr (s2, strldx(":",s2)+1, 1000)
    if (stridx("_",s2) > 0)
	s2 = substr (s2, 1, stridx("_",s2)-1)
    ;
    match (s2, "dayeval2.tmp") | scan (s1)
}
;
delete ("dayeval2.tmp")

# Trigger pipeline.
if (s1 != "") {
    s2 = s1 // dataset // ".evl"
    if (access("dayeval1.tmp")) {
	copy ("dayeval1.tmp", s2)
	delete ("dayeval1.tmp")
    } else
        touch (s2)
    touch (s2//"trig")
} else
    status = 0
delete ("dayeval*")
logout (status)
