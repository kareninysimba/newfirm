#!/bin/env pipecl
#
# DAY2FRG -- Send list of images to the FRG pipeline.
#
# This module sets the list of images and then uses call.cl to call the
# pipeline.

int	status = 2
string  dataset, indir, datadir, ilist, lfile, olist, temp1, temp2

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Setup directories and files.
daynames (envget ("OSF_DATASET"))
dataset = daynames.dataset
indir = daynames.indir
datadir = daynames.datadir
ilist = indir // dataset // ".day"
lfile = datadir // daynames.lfile
olist = "day2frg1.tmp"
temp1 = "day2frg2.tmp"
temp2 = "day2frg3.tmp"
set (uparm = daynames.uparm)
cd (datadir)

# Create list to process. We need to look for the products of the PGR
# pipeline, and if these are not available (e.g., because pupil ghost removal
# was not necessary or done), we need to look for the results of the SIF
# pipeline.
i = 0
if (access (dataset//".pgr")) {
    match ("_p.fits", "@"//dataset//".pgr", print-, > olist)
    count (olist) | scan (i)
}
;
if (i == 0) {
    match ("ccd[0-9]*.fits", ilist, > olist)
    count (olist) | scan (i)
}
;
if (i == 0) {
    delete (olist)
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 1
}
;

# Find obm data.
i = 0
if (access (dataset//".pgr")) {
    match ("_obm.fits", "@"//dataset//".pgr", print-, > temp1)
    count (temp1) | scan (i)
}
;
if (i == 0) {
    match ("_obm.fits", ilist, > temp1)
    count (temp1) | scan (i)
}
;
if (i == 0) {
    delete (olist)
    delete (temp1)
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 1
}
;

# Find sky data.
match ("ccd[0-9]*_sky.fits", ilist, > temp2)

# Add to list.
concat (temp1//","//temp2, >> olist) 
delete (temp1//","//temp2) 

# Set comments.
line=""; match ("Bad sky coverage for fringe", lfile) | scan (line)
if (line != "")
    printf ("#A %s\n", line, >> olist)
;

# Call pipeline.
callpipe ("day", "frg", "copy", datadir//olist, temp1, temp2)
concat (temp2) | scan (status)
concat (temp1)
delete ("day2frg*.tmp")

logout (status)
