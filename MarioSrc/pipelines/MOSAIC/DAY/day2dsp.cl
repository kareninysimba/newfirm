#!/bin/env pipecl
# 
# DAY2DSP -- Call the DSP pipeline.

logout 1

string  dsp, dataset, datadir, lfile

# Tasks and packages
proto
task pipeselect = "$!pipeselect"

# Set names and directories.
daynames (envget ("OSF_DATASET"))
dataset = daynames.dataset
datadir = daynames.datadir
lfile = daynames.lfile
set (uparm = daynames.uparm)
cd (datadir)

# Log start of module.
printf ("\nDAY2DSP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Check if DSP pipeline is running and exit if not.
dsp = ""; pipeselect (envget ("NHPPS_SYS_NAME"), "dsp", 1, 0) | scan (dsp)
if (dsp == "") {
    sendmsg ("WARNING", "No pipeline running", "dsp", "PIPE")
    logout 3
}
;

# Create the file to send to the DSP pipeline.
pathnames (daynames.lfile, >> dsp//dataset//".dsp")
if (access (daynames.frggif)) {
    pathnames (daynames.frggif, >> dsp//dataset//".dsp")
    pathnames (daynames.frggif2, >> dsp//dataset//".dsp")
    pathnames (daynames.frghdr, >> dsp//dataset//".dsp")
}
;
if (access (daynames.sftgif)) {
    pathnames (daynames.sftgif, >> dsp//dataset//".dsp")
    pathnames (daynames.sftgif2, >> dsp//dataset//".dsp")
    pathnames (daynames.sfthdr, >> dsp//dataset//".dsp")
}
;

daynames (daynames.parent, pattern="*")
pathnames (daynames.gif, >> dsp//dataset//".dsp")
pathnames (daynames.gif2, >> dsp//dataset//".dsp")
pathnames (daynames.ogif, >> dsp//dataset//".dsp")
pathnames (daynames.hdr, >> dsp//dataset//".dsp")

# Trigger the DSP pipeline.
touch (dsp//dataset//".dsptrig")

logout 1
