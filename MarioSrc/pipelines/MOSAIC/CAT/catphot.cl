#!/bin/env pipecl
#
# CATPHOT
#
# Description:
#
#    
#    
# 
# Exit Status Values:
#
#    1 = Successful
#    2 = 

# Declare variables
string	dataset, indir, datadir, ilist, lfile, rspgrp, infile

# Load packages
images
proto

# Set file and path names. Work in data directory.
names ("cat",envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir//dataset//".cat"
lfile = datadir//names.lfile
set (uparm=names.uparm)
cd (datadir)

# Log start of processing.
printf ("\nCATPHOT (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Input file is the return file from SRS to RSP.  If this file does not exist,
# then exit since there are no rebinned SIFs to group. 

print("I AM IN CAT!")

logout(1)
