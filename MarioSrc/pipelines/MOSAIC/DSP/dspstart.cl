#!/bin/env pipecl
#
# DSPSTART -- File trigger start of DSP pipeline.

int	status = 1
string  dataset, datadir, htmlfile, ifile, temp1, temp2
struct	*fd

# Tasks and packages.
images
proto
task $dsphtml = "$!dsphtml.cl"
task $dspngt = "$!dspngt.cl"
task $dspall = "$!dspall.cl"

# Directories and files.
dataset = envget ("EVENT_NAME")
ifile = "MOSAIC_DSP$/input/" // dataset // ".dsp"
temp1 = "MOSAIC_DSP$/input/" // dataset // "1.tmp"
temp2 = "MOSAIC_DSP$/input/" // dataset // "2.tmp"

# Log execution.
printf ("\nDSPSTART (%s): ", dataset); time

# The rootname of each dataset is given by the FITS global header.
match ("_00.fits", ifile, > temp1)

datadir = ""
list = temp1
while (fscan (list, s1) != EOF) {

#    # Set directory based on date.  This is specific to OBSID.
#    s2 = ""; hselect (s1, "obsid", yes) | scan (s2)
#    s3 = substr (s2, 1, 4)
#    if (s3 == "ct4m") {
#	i = stridx ("T", s2)
#	s3 = substr (s2, i+1, 1000)
#	s2 = substr (s2, 6, i-1)
#	if (int(s3) >= 160000)
#	    s2 = int(s2) + 1
#	s2 = "ct4m" // s2
#    } else if (s3 == "kp4m") {
#	i = stridx ("T", s2)
#	s3 = substr (s2, i+1, 1000)
#	s2 = substr (s2, 6, i-1)
#	if (int(s3) >= 190000)
#	    s2 = int(s2) + 1
#	s2 = "kp4m" // s2
#    } else {
#        s3 = dataset//".dsp"
#        sendmsg ("WARNING", "OBSID missing or unrecognized", s2, "VRFY")
#	next
#    }

    # Set directory based on root of dataset name.
    s2 = substr (s1, strldx("/",s1)+1, 1000)
    s2 = substr (s2, 1, stridx("-",s2)-1)

    # Check if this is a new directory and if so set the summary
    # page for the previous directory.
    if (datadir != "" && datadir != s2) {
        cd ("MOSAIC_DSP$/output/" // datadir)
	dspngt
    }
    ;

    # Setup directory.
    datadir = s2
    if (access("MOSAIC_DSP$/output/" // datadir) == NO)
	mkdir ("MOSAIC_DSP$/output/" // datadir)
    ;
    cd ("MOSAIC_DSP$/output/" // datadir)

    # Set subdirectory.
    s2 = substr (s1, 1, strldx("/",s1)-1)
    s2 = substr (s2, strldx("/",s2)+1, 1000)
    if (access(s2)==NO)
	mkdir (s2)
    ;
    cd (s2)

    # Copy files.
    s2 = substr (s1, strldx("/",s1)+1, strlstr("_00.fits",s1)-1)
    match (s2, ifile, > "files")
    copy ("@files", ".")
    delete ("files")

    # Make header.
    hfix (s2//"_00.fits", command="type $fname", update-, > s2//".hdr")
}
list = ""; delete (temp1)

if (status == 0)
    logout (status)
;

# Make summary page.
if (access ("MOSAIC_DSP$/output/" // datadir)) {
    cd ("MOSAIC_DSP$/output/" // datadir)
    dspngt
}
;

# Make all sumary page.
#cd ("MOSAIC_DSP$/output/")
#printf ("dspall\n")
#dspall

delete ("MOSAIC_DSP$/input/"//dataset//".dsp")

logout 1
