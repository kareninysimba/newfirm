#!/bin/env pipecl
#
# SCI2MSC -- Setup SCI processing data for an MEF. 
# 
# The input list of all pipeline products.
# There must by 00.fits files to define the MEF datasets.

string	cpipe = "sci"			# Calling pipeline
string	tpipe = "msc"			# Target pipeline
string	textn = ".msc"			# Target pipeline Trigger extension
string	rextn = ".msc"			# Return trigger file extension
int	ntrig = 2			# Number to trigger here
int	founddir

string	dataset, indir, datadir, ilist, lfile, spool, pipes, odir, tname, cname
struct	*fd1

# Tasks and packages.
task $pipeselect = "$!pipeselect"
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Files and directories.
names (cpipe, envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
spool = indir // "spool/"
ilist = indir // dataset // ".sci"
lfile = datadir // names.lfile
set (uparm = names.uparm)

# Work in data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\nSCI2MSC (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Check for pipelines.
pipes = spool // dataset // rextn
touch (pipes)
if (defvar ("NHPPS_MIN_DISK"))
    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, envget("NHPPS_MIN_DISK"), > pipes)
else
    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, > pipes)
count (pipes) | scan (i)
if (i == 0) {
    sendmsg ("ERROR", "Pipeline not running", tpipe, "PIPE")
    delete (pipes)
    logout 2
}
;

# Get list of MEF global files.
match ("MOSAIC_MEF?*_00.fits", ilist, > "sci2msc.tmp")
count ("sci2msc.tmp") | scan (i)
if (i == 0) {
    sendmsg ("ERROR", "No data found", "", "VRFY")
    delete (pipes)
    delete ("sci2msc.tmp")
    logout 3
}
;

# Setup uparm directory from header of first file.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
s1 = ""; head ("sci2msc.tmp") | scan (s1)
iferr {
    setdirs (s1)
} then {
    logout 0
} else
    ;

# Loop over each MEF.
i = 1; j = 0
list = "sci2msc.tmp"; fd1 = pipes
while (fscan (list, s1) != EOF) {
    j += 1

    # Set names.
    s2 = substr (s1, strldx("/",s1)+1, strlstr("_00.fits",s1)-1)
    printf ("%s_%03d\n", dataset, j) | scan (cname)
    tname = s2 // "s"

    # Create @file.
    match (s2, ilist, >> indir//"data/"//tname//textn)

    # Setup files for triggering and return.
    print (tname, >> spool//cname//rextn)
    print (cname//rextn, >> dataset//textn)

    # Trigger subpipeline.
    if (i > ntrig)
        next
    ;

    if (fscan (fd1, odir) == EOF) {
        i += 1
	fd1 = pipes
	if (fscan (fd1, odir) == EOF)
	    next
	;
    }
    ;
    if (i > ntrig)
        next
    ;

    copy (indir//"data/"//tname//textn, odir//tname//textn)
    pathname (indir//cname//rextn, >> odir//"return/"//tname//textn)
    print (substr(odir,1,stridx("!",odir)-1), > spool//cname//rextn//".node")
    touch (odir//tname//textn//"trig")
}
list =""; fd1 = ""
delete ("sci2msc.tmp")

logout 1
