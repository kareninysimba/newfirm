#!/bin/env pipecl
#
# SCI2MOV -- Call moving object pipeline.
#
# The input list is all pipeline products from FTR.  A list of lists for
# each parallel group (a stack) is created and then the pipelines are
# triggered by the standard call routine with the split option.
#
# The input to the MOV pipeline is a list calibrated SIFs which contributed
# to a stack and difference catalogs.
#
# STATUS:
#    0 = error
#    1 = no data
#    2 = pipeline called
#    3 = pipeline not running

file	ilist, cat, im, mov

# Load packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"
utilities

# Set file and path names.
names ("sci", envget("OSF_DATASET"))

# Set working directory.
cd (names.datadir)
ilist = names.indir // names.dataset // ".sci"
delete ("sci2mov*.tmp")

# Log start of processing.  The callpipe will log to the logfile.
printf ("\nSCI2MOV-SETUP (%s): ", names.dataset)
time

# Find catalogs.
match ("MOSAIC_STR/?*_diff.cat", ilist, > "sci2mov1.tmp")
count ("sci2mov1.tmp") | scan (i)
if (i == 0) {
    delete ("sci2mov*tmp")
    logout (1)
}
;

# Find calibrated SIFs.
match ("_s.fits", ilist, > "sci2mov2.tmp")
count ("sci2mov2.tmp") | scan (i)
if (i == 0) {
    match ("_f.fits", ilist, > "sci2mov2.tmp")
    count ("sci2mov2.tmp") | scan (i)
}
;
if (i == 0) {
    match ("_p.fits", ilist, > "sci2mov2.tmp")
    count ("sci2mov2.tmp") | scan (i)
}
;
if (i == 0)
    match ("MOSAIC_SIF/?*-ccd[0-9]*.fits", ilist, > "sci2mov2.tmp")
;

# Match catalogs and images into MOV input lists.
list = "sci2mov1.tmp"
while (fscan (list, cat) != EOF) {
    mov = substr (cat, strldx("/",cat)+1, 999)
    mov = substr (mov, 1, strstr("_diff",mov)-1)
    im = ""; match (mov, "sci2mov2.tmp") | scan (im)
    mov = substr (cat, 1, strstr("-ctr",cat)-1)
    mov = substr (mov, strstr("-mdc",mov)+4, 999)
    if (im != "")
	printf ("%s\n%s\n", cat, im, >> "sci2mov_"//mov//".tmp")
    ;
}
list = ""; delete ("sci2mov[12].tmp")

# Make list of lists.
pathnames ("sci2mov_*.tmp", > "sci2mov1.tmp")

print sci2mov1.tmp
concat sci2mov1.tmp

# Submit to the MOV pipelines.
callpipe ("sci", "mov", "split", "sci2mov1.tmp", "sci2mov2.tmp", "sci2mov3.tmp")
concat ("sci2mov2.tmp")
i = 1; concat ("sci2mov3.tmp") | scan (i)
delete ("sci2mov*.tmp")
logout (i)
