# SCLNAMES -- Directory and filenames for the SCL pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure sclnames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline name"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file	parent			{prompt = "Parent part of dataset"}
file	child			{prompt = "Child part of dataset"}
file	lfile			{prompt = "Log file"}
file	image			{prompt = "Primary image name"}
file	bpm			{prompt = "Bad pixel mask"}
file	cal			{prompt = "Combined calibration name"}
file	blk1			{prompt = "Block averaged image"}
file	blk2			{prompt = "Block averaged image"}
string	pattern = ""		{prompt = "Pattern"}
bool	base = no		{prompt = "Child part of dataset"}

begin
	# Set generic names.
	names ("scl", name, pattern=pattern, base=base)
	dataset = names.dataset
	pipe = names.pipe
	shortname = names.shortname
	rootdir = names.rootdir
	indir = names.indir
	datadir = names.datadir
	uparm = names.uparm
	pipedata = names.pipedata
	parent = names.parent
	child = names.child
	lfile = names.lfile

	# Set pipeline specific names.
	image = dataset
	cal = shortname
	bpm = shortname // "_bpm"
	i = strstr ("Zero", cal)
	if (i > 0)
	    cal = substr (cal, 1, i-2) // substr (cal, i+4, 1000)
	i = strstr ("F-", cal)
	if (i > 0)
	    cal = substr (cal, 1, i-1) // substr (cal, i+1, 1000)
	blk1 = cal // "_blk8"
	blk2 = cal // "_blk64"

	if (pattern != "") {
	    image += ".fits"
	    cal += ".fits"
	    bpm += ".pl"
	    blk1 += ".fits"
	    blk2 += ".fits"
	}
end
