# MKMAP -- Make a low resolution spatial map.

procedure mkmap (input, output)

file	input				{prompt="Input image"}
file	output				{prompt="Output map"}
real	mean = INDEF			{prompt="Mean of good data"}
real	lthresh = INDEF			{prompt="Low threshold"}
int	mwindow = 8			{prompt="Median window"}
int	bwindow = 16			{prompt="Block average window"}

begin
	int	nc, nl
	file	tmp, tmp1, tmp2, tmp3

	tmp = mktemp ("mkmap")
	tmp1 = tmp // "1"
	tmp2 = tmp // "2"
	tmp3 = tmp // "3"

	# First median filter to eliminate cosmic rays.
	fmedian (input, tmp1, xwindow=mwindow, ywindow=mwindow, verb-)

	# Replace low values.
	if (!isindef(mean) && !isindef(lthresh))
	    imreplace (tmp1, mean, lower=INDEF, upper=lthresh, radius=0.)

	# Block average.
	blkavg (tmp1, output, bwindow, bwindow, op="average")
	imdel (tmp//"*")

	# Normalize by projections.
	hselect (output, "naxis1,naxis2", yes) | scan (nc, nl)

	imrename (output, tmp1)
	blkavg (tmp1, tmp2, 1, nl, op="average")
	blkrep (tmp2, tmp3, 1, nl)
	imexpr ("a/b", output, tmp1, tmp3, verb-)
	imdel (tmp//"*")

	imrename (output, tmp1)
	blkavg (tmp1, tmp2, nc, 1, op="average")
	blkrep (tmp2, tmp3, nc, 1)
	imexpr ("a/b", output, tmp1, tmp3, verb-)
	imdel (tmp//"*")
end
