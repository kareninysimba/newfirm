# SCLDQZERO -- Combine bias images with data quality checking.
#
# When the input list has more than one bias it checks for good ones.
# This rejection of bad biases is different from the rejection that is
# done by zcombine. Here rejection is done based on global statistics,
# whereas in zcombine rejection is done based on a per-pixel basis. In
# principle, a bias frame could be globally bad (e.g., elevated noise
# level) but some pixels of such a frame would not be rejected by zcombine.
# Because possible structure in the bias may affect the global statistics,
# the quality control has to be done in two steps: first, the bias frames
# should be combined and subtracted from the individual ones. Next, the
# statistics of the individual frames should be determined and compared to
# the expected values. If some frames need to be rejected, zcombine needs
# to be run again.

procedure scldqzero (dataset, ilist, imname)

string	dataset			{prompt="Dataset name"}
file	ilist			{prompt="List of input zero images"}
file	imname			{prompt="Output zero image"}
real	quality			{prompt="Quality"}
int	status			{prompt="Status return"}

struct	*fd

begin
	int	i, nin, nout, rulepassed, ampn, nkey
	int	result = 0
	real	rdnoise, gain, dqglmean, dqglsig, exprdnoise, mjd
	real	x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, dqovjprb
	string	rulepath, im, sim, parentImages, cast
	file	tmpfile, olist

	quality = 0.
	status = 0.

	tmpfile = mktemp ("tmpdq")
	olist = mktemp ("tmpdq")

        printf( "Starting SCLDQZERO\n", >> lfile)

	# Define rules
	findrule (dm, "enough_biases", validate=1) | scan (rulepath)
	task enough_biases = (rulepath)
	findrule (dm, "bias_ok", validate=1) | scan (rulepath)
	task bias_ok = (rulepath)

	# Check input number of images to combine.
	i = 0; count (ilist) | scan (i)

	# If no image are in the list return an error status.
	if (i == 0) {
	    sendmsg ("WARNING", "No data to combine for this dataset", "", "DQ")
	    quality = -1.
	    status = 0
	    return
	}

	# If there is only one image set some header/PMAS info and return it.
	if (i == 1) {
	    # Check if there are enough biases.
	    enough_biases (i) | scan (rulepassed)
	    if (rulepassed==0)
		sendmsg ("WARNING", "Small number of bias frames", str(i),
		    "DQ")

	    imcopy ("@"//ilist, imname)

	    # Enter data product.
	    hselect (imname, "mjd-obs", yes) | scan (mjd)
	    newDataProduct(dataset, mjd, "biasimage", "") | scan( result, line)
	    if (result>0)
        	sendmsg("WARNING", "newDataProduct failed", line, "DM")
    	    ;
            storekeywords( class="biasimage", id=imname, sid=dataset, dm=dm )

	    # Set header and PMASS information.
	    hedit (imname, "dqzenusd", 1, add+, verify-, show+, update+, >> lfile)
	    hedit (imname, "dqzenrej", 0, add+, verify-, show+, update+, >> lfile)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzenusd", value="1")
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzenrej", value="0")

	    status = 1
	    return
	}

	# If we get here then we have multiple biases to check and combine.

	zcombine ("@"//ilist, imname, amps=no, offset="physical",
	    imcmb="DTNSANAM", >> lfile)

	nin = 0; nout = 0; parentImages = ''
	fd = ilist
	while (fscan (fd, im) != EOF) {
	    sim = substr (im, strldx("/",im)+1, 1000)

	    # Get required header info and return with an error if missing.
	    rdnoise = INDEF; hselect (im, "rdnoise", yes) | scan (rdnoise)
	        nkey = nscan()
	    gain = INDEF; hselect (im, "gain", yes) | scan (gain)
	        nkey += nscan()
	    dqglsig = INDEF; hselect (im, "dqglsig", yes) | scan (dqglsig)
	        nkey += nscan()
	    dqglmean = INDEF; hselect (im, "dqglmean", yes) | scan (dqglmean)
	        nkey += nscan()
	    ampn = INDEF; hselect (im, "imageid", yes) | scan (ampn)
	        nkey += nscan()
	    dqovjprb = INDEF; hselect (im, "dqovjprb", yes) | scan (dqovjprb)
	        nkey += nscan()
	    #hselect (im, "rdnoise, gain, dqglsig, dqglmean, imageid", yes) |
	    #    scan (rdnoise, gain, dqglsig, dqglmean, ampn)
	    #nkey = nscan()
	    if (nkey < 6) {
		fd = ""
		sendmsg ("WARNING", "Missing header information", sim, "DQ")
		print (im)
	        print (rdnoise, gain, dqglsig, dqglmean, ampn, dqovjprb)
		quality = -1.
		status = 0
		return
	    }

	    rdnoise = rdnoise/gain
	    if (dqglsig==0) {
	        dqglmean = INDEF
	    } else {
	        dqglmean = dqglmean/dqglsig
	    }

	    # Get the statistics of the residual image.
	    imarith (im, "-", imname, tmpfile)
	    imstat (tmpfile, fields="mean,stddev", nclip=1, lsigma=2, usigma=2,
	        format-) | scan (x1, x2)
	    imdel (tmpfile)

	    # Convert mean (x1) into units of sigma (x2). In some cases
	    # x2 can be 0, indicating im and imname are the same. In that
	    # case, x1 is left unchanged.
	    if (x2 != 0)
	        x1 = x1/x2
	    ;

	    # Calculate the expected noise in the average bias frame
	    # The pixel values in a subtracted bias (Bsub) are:
	    # Bsub = B1-sum(Bn)[i=1..n]/n
	    #      = B1((n-1)/n)-sum(Bn)[i=2..n]/n
	    # The corresponding noise is:
	    # sig  = sqrt{sig1**2*((n-1)/n)**2+sum((sign/n)**2)}
	    #      = sqrt(sig1**2*((n-1)/n)**2+(n-1)*(sig/n)**2)
	    #      = sig/n*sqrt(i*i-i)
	    exprdnoise = rdnoise * sqrt (i*i-i) / i;

	    if (verbose>=2) {
	        # Write data quality data to log file ...
	        printf ("DQ | image: %s\n", im)
	        printf ("DQ | rdnoise[ADU]: %f\n", rdnoise)
	        printf ("DQ | expected noise[ADU]: %f\n", exprdnoise)
	        printf ("DQ | sigma[ADU]: %f (before zcombine)\n", dqglsig)
	        printf ("DQ | mean/sigma: %f (before zcombine)\n", dqglmean)
	        printf ("DQ | sigma[ADU]: %f (after zcombine)\n", x2)
	    }
	    ;

	    # Express the noise in units of the expected noise
	    x2 = x2/exprdnoise
	    if (verbose>=2) {
	        printf ("DQ | sigma[exp. sigma]: %f (after zcombine)\n", x2)
	        printf ("DQ | mean/sigma: %f (after zcombine)\n", x1)
	    }
	    ;

	    # ... to the header ...
	    hedit (im, "dqbiesig", exprdnoise, add+, verify-, show+, update+, >> lfile)
	    hedit (im, "dqbizmns", x1, add+, verify-, show+, update+, >> lfile)
	    hedit (im, "dqbizsge", x2, add+, verify-, show+, update+, >> lfile)

	    # ... and to PMAS
	    printf ("%12.5e\n", exprdnoise) | scan (cast)
	    setkeyval (class="zeroimage", id=sim, dm=dm,
		keyword="dqbiesig", value=cast)
	    printf ("%12.5e\n", x1) | scan (cast)
	    setkeyval (class="zeroimage", id=sim, dm=dm,
		keyword="dqbizmns", value=cast)
	    printf ("%12.5e\n", x2) | scan (cast)
	    setkeyval (class="zeroimage", id=sim, dm=dm,
		keyword="dqbizsge", value=cast)

	    # Check if the bias is OK.
	    bias_ok (ampn, x2, x1, dqovjprb ) | scan (rulepassed)
	    if (rulepassed==1) {
		printf  ("%s\n", im, >> olist)
		if (strlen(parentImages)==0) {
		    parentImages = sim
		} else {
		   parentImages = parentImages // ',' // sim
		}
		nout = nout+1
	    } else
		sendmsg ("WARNING", "Rejected this bias frame", im, "DQ")

	    # Increase the counter
	    nin = nin+1
	}
	fd = ""
	
	# Check if there are enough biases.
	enough_biases (nout) | scan (rulepassed)
	if (rulepassed==0)
	    sendmsg ("WARNING", "Small number of bias frames", str(nout), "DQ")

	# No images left...
	if (nout==0) {
	    if (imaccess (imname))
		imdel (imname)
	    sendmsg ("WARNING", "No bias frames to combine after rejections",
	        "", "DQ")
	    quality = -1.
	    status = 2
	    return
	}

	# Make a new zero image without the rejected ones.
	if (nout < nin) {
	    if (imaccess (imname))
		imdel (imname)
	    if (nout>1)
		zcombine ("@"//olist, imname, amps=no, offset="physical",
		    imcmb="DTNSANAM", >> lfile)
	    else
		imcopy ("@"//olist, imname)
	}

	# Enter new data product.
	hselect (imname, "mjd-obs", yes) | scan (mjd)
	newDataProduct(dataset, mjd, "biasimage", "") | scan( result, line )
	if (result>0)
            sendmsg("WARNING", "newDataProduct failed", line, "DM")
    	;
        storekeywords( class="biasimage", id=imname, sid=dataset, dm=dm )

	# Write DQ to header and PMAS.
	nin = nin-nout
	hedit (imname, "dqzenusd", nout, add+, verify-, show+, update+, >> lfile)
	hedit (imname, "dqzenrej", nin, add+, verify-, show+, update+, >> lfile)
	printf ("%d\n", nout) | scan (cast)
	setkeyval (class="biasimage", id=dataset, dm=dm,
	    keyword="dqzenusd", value=cast)
	printf ("%d\n", nin) | scan (cast)
	setkeyval (class="biasimage", id=dataset, dm=dm,
	    keyword="dqzenrej", value=cast)
	parentImages = "\"" // parentImages // "\""
	setkeyval (class="biasimage", id=dataset, dm=dm,
	    keyword="parentBiases", value=parentImages)

	# Now check the bias structure.
	biasstruct (imname, >> lfile)
	flpr # Force IRAF to update the header of imname
	hselect (imname, "dqzexslp,dqzeyslp,dqzexsig,dqzeysig,dqzeglme,\
	    dqzeglsi,dqzexmea,dqzexcef,dqzeymea,dqzeycef", yes) |
	    scan (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
	if (nscan()==10) {
	    printf ("%12.5e\n", x1) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzexslp", value=cast)
	    printf ("%12.5e\n", x2) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzeyslp", value=cast)
	    printf ("%12.5e\n", x3) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzexsig", value=cast)
	    printf ("%12.5e\n", x4) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzeysig", value=cast)
	    printf ("%12.5e\n", x5) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzeglme", value=cast)
	    printf ("%12.5e\n", x6) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzeglsi", value=cast)
	    printf ("%12.5e\n", x7) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzexmea", value=cast)
	    printf ("%12.5e\n", x8) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzexcef", value=cast)
	    printf ("%12.5e\n", x9) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzeymea", value=cast)
	    printf ("%12.5e\n", x10) | scan (cast)
	    setkeyval (class="biasimage", id=dataset, dm=dm,
		keyword="dqzeycef", value=cast)
	} else
	    sendmsg ("WARNING", "Could not read all data from biasstruct",
	        "", "DQ")

	# Finish up.
	delete (olist)
	status = 1
end
