#!/bin/env pipecl
#
# SCLSETUP -- Setup SCL processing data including calibrations.

int     founddir
int	status = 1
struct	obstype, statusstr, mask_name
string	dataset, indir, datadir, ifile, im, lfile
file	caldir = "MC$"

# Tasks and packages
task $awk = "$!awk.sh"
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
servers
images

# get the dataset name
sclnames (envget("OSF_DATASET"))
dataset = sclnames.dataset

# Set paths and files.
indir = sclnames.indir
ifile = indir // dataset // ".scl"
datadir = sclnames.datadir
lfile = datadir // sclnames.lfile
set (uparm = sclnames.uparm)
set (pipedata = sclnames.pipedata)
cd (datadir)
	
# Log start of processing.
printf ("\nSCLSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

## Work in data directory.
#founddir = 0
#while (founddir==0) {
#    if (access (datadir)) {
#        founddir = 1
#    } else {
#        sleep 1
#        printf("Retrying to access %s\n", datadir)
#    }
#}

# Check if input files are accessible.
if (defvar ("PIPEDEBUG")) {
    type (ifile)
    list = ifile
    while (fscan (list, s1) != EOF) {
        if (!imaccess (s1)) {
	    sendmsg ("ERROR", "Can't access image", s1, "CAL")
	    status = 0
	}
	;
    }
    list = ""
    if (status == 0)
        logout (status)
    ;
}
;

# Setup UPARM.
delete (substr(sclnames.uparm, 1, strlen(sclnames.uparm)-1))
s1 = ""; head (ifile, nlines=1) | scan (s1)
iferr {
    setdirs (s1, umatch="!ccdsum")
} then {
    logout 0
} else
    ;

# Get the calibrations.
list = ifile
    while (fscan (list, s1) != EOF) {
    getcal (s1, "bpm", cm, caldir, obstype="", detector=instrument,
	imageid="!ccdname", filter="!filter", exptime="",
	mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;
    getcal (s1, "zero", cm, caldir,
	obstype="!obstype", detector=instrument, imageid="!ccdname",
	filter="!filter", exptime="", mjd="!mjd-obs", dmjd=cal_dmjd,
	match="!nextend,ccdsum") | scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for zero", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;
    getcal (s1, "calops", cm, caldir,
	obstype="", detector=instrument, imageid="",
	filter="!filter", exptime="", match="", mjd="!mjd-obs") |
	scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for calops", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;
    getcal (s1, "photindx", cm, caldir,
	obstype="", detector="", imageid="", filter="!filter",
	exptime="", mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for photindx", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;
    getcal (s1, "bandtype", cm, caldir,
	obstype="", detector="", imageid="", filter="!filter",
	exptime="", mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for bandtype", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;
}
list = ""

if (status == 0)
    logout 0
;

# and copy the crosstalk masks over from indir
# by convention, the crosstalk mask has the same 
# name as the dataset it refers to, with "_xtm" 
# appended to it and ".pl" extension.
# the amplifier number (e.g. 01) comes after the
# "_xtm" string and before the file extension.
awk (dataset,
    '\'{b = match ($0, /_([0-9]+)$/); print (substr ($0, 1, b-1) "_xtm" substr ($0, b+1))}\'') |
    scan (mask_name)

mask_name = indir // "data/" // mask_name

if (imaccess (mask_name)) {
    imrename (oldnames=mask_name, newnames="./", verbose=no)
}
;

logout 1
