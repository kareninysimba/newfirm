#!/bin/env pipecl
#
# SCLPROC -- CCD process a list file of single CCD images.

int	status = 1
int	namps, nccds, nextend
int	nlist, saturate, min, max, npix, nsat, dx, dy, order, ovscnmethod
int     result = 0
struct	obstype
string	dataset, indir, datadir, ilist, ilist1, olist, blist, im, bpm, lfile
real	dqglmean,dqglsig,dqglfsat,x1,x2,x3,x4,x5,mjd,dqovjprb, dqovmin
string	saturation, bleed, observedMJD, ampn, dqmon, dqmonport, function
string	cast, sim, rulepath, im2
bool	oflag, tflag, zflag, dflag, fflag, sflag, bflag, pflag
struct	dqrej, ccdsum

# Load packages.
servers
images
mscred
task mergeamps = "msccmb$x_combine.e"
dataqual
cache mscred

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"

# Set file and path names.
sclnames (envget("OSF_DATASET"))
dataset = sclnames.dataset

# Define rules
findrule (dm, "ovscn_subtract_method", validate=1) | scan (rulepath)
task ovscn_subtract_method = (rulepath)

# Setup configuration file
#task getconfig = "$getconfig.py $1 $2"

# Set filenames.
indir = sclnames.indir
datadir = sclnames.datadir
ilist = indir // dataset // ".scl"
lfile = datadir // sclnames.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = sclnames.uparm)
set (pipedata = sclnames.pipedata)

# Go to working directory and clean up if necessary.
cd (datadir)
delete ("*fits,*pl,sclproc*tmp")

# Log start of processing.
printf ("\nSCLPROC (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Do processing.
ilist1 = indir // dataset // "1.tmp"
olist = indir // dataset // "2.tmp"
blist = dataset // "_bpm.tmp"
if (access(ilist1))
    delete (ilist1)
;
if (access(olist))
    delete (olist)
;
if (access(blist))
    delete (blist)
;

# Set processing flags.
head (ilist) | scan (s1)
obstype = ""; hselect (s1, "obstype", yes) | scan (obstype)
if (obstype == "dome flat")
    obstype = "flat"
else if (obstype == "sky flat")
    obstype = "sflat"
if (obstype != "zero" && obstype != "dark" &&
    obstype != "flat" && obstype != "sflat") {
    sendmsg ("ERROR", "Unknown calibration type", obstype, "CAL")
    logout (0)
}
;
if (obstype == "zero" || obstype == "dark")
    obstype = "zeroimage"
else
    obstype = "domeflatimage"

s2 = "OTZFSBP"
hselect (s1, "CALOPS", yes) | scan (s2)
oflag = (stridx("O",s2) > 0)
tflag = (stridx("T",s2) > 0)
zflag = (stridx("Z",s2) > 0)
dflag = (stridx("D",s2) > 0)
fflag = NO
sflag = (stridx("S",s2) > 0)
bflag = YES
pflag = (stridx("P",s2) > 0)

# Turning on saturation checking and output masks can be used to measure
# the number of saturated pixels.  For now we don't do this.
#
# We will also have to rase the saturation value for binning.

ccdsum = ""; hselect (s1, "CCDSUM", yes) | scan (ccdsum)

if (sflag) {
    saturation = _ccdtool.saturation
} else
    saturation = "INDEF"
bleed = "INDEF"

# Determine which method to use to subtract bias
ovscnmethod = 3; ovscn_subtract_method (INDEF) | scan( ovscnmethod )

list = ilist; nccds = 0
for (namps=0; fscan(list,im)!=EOF; namps+=1) {
    print (im)
    im2 = substr (im, strldx("/",im)+1, 1000)

    # Determine number of ccds to later compare against the number of amps.
    s2 = substr (im2, 1, strldx("_",im2)-1)
    s2 = substr (s2, strldx("_",s2)+1,999)
    nccds = max (nccds, int(s2))

    # Use bad pixel mask if checking for saturation.
    if (saturation == "INDEF")
        bpm = ""
    else {
	i = strstr (".fits", im2)
	if (i > 0)
	    bpm = substr (im2, 1, i-1) // "_bpm.pl"
	else
	    bpm = im2 // "_bpm.pl"
    }

    # separate path and filename
    sim = substr (im, strldx("/",im)+1, 1000)
    # Determine overscan properties, including possible jumps
    overscan( im, >> lfile )
    # Read the keywords from the header
    hsel( im, "mjd-obs,nextend", yes ) | scan( mjd, nextend )
    newDataProduct(sim, mjd, obstype, "") | scan(result, line)
    if (result>0)
        sendmsg("WARNING", "newDataProduct failed", line, "DM")
    ;
    storekeywords( class="singleimage", id=im, sid=sim, dm=dm )
    hsel( im, "dqovmll,dqovmlj,dqovmjl,dqovmjj", yes ) |
        scan( x1, x2, x3, x4 )
    if (nscan()==4) {
        printf("%12.5e\n", x1 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmll", value=cast )
            printf("%12.5e\n", x2 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmlj", value=cast )
        printf("%12.5e\n", x3 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmjl", value=cast )
        printf("%12.5e\n", x4 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmjj", value=cast )
    } else {
	print( "WARNING: could not read one of dqov mll/mlj/mjl/mjj" )
    }
    hsel( im, "dqovmllr,dqovmljr,dqovmjlr,dqovmjjr", yes ) |
        scan( x1, x2, x3, x4 )
    if (nscan()==4) {
        printf("%12.5e\n", x1 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmllr", value=cast )
        printf("%12.5e\n", x2 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmljr", value=cast )
        printf("%12.5e\n", x3 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmjlr", value=cast )
        printf("%12.5e\n", x4 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmjjr", value=cast )
    } else {
	print( "WARNING: could not read one of dqov mllr/mljr/mjlr/mjjr" )
    }
    # Provide a failsafe value for dqovjprb. With dqovjprb set to
    # 1, the overscan will be subtracted line by line. This will
    # eliminate the possibility that a frame with a bias jump
    # will not be processed properly if dqovjprb is not read.
    dqovjprb = 1
    hsel( im, "dqovjprb,dqovmin,dqovmax,dqovmean,dqovsig,dqovsmea,dqovssig",
        yes ) | scan( dqovjprb, dqovmin, x1, x2, x3, x4, x5 )
    if (nscan()==7) {
        printf("%12.5e\n", dqovjprb ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovjprb", value=cast )
        printf("%12.5e\n", dqovmin ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmin", value=cast )
        printf("%12.5e\n", x1 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmax", value=cast )
        printf("%12.5e\n", x2 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovmean", value=cast )
        printf("%12.5e\n", x3 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovsig", value=cast )
        printf("%12.5e\n", x4 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovsmea", value=cast )
        printf("%12.5e\n", x5 ) | scan( cast )
        setkeyval( class="singleimage", id=sim, dm=dm,
            keyword="dqovssig", value=cast )

        # Do check for totally bad amplifier.
        if (x1<=1. || x2<=1. || x3<=1. || dqovmin<=0.) {
	    sendmsg("WARNING", "bad amp", sim, "CAL")
	    dqrej = "bad amp"
            setkeyval( class="singleimage", id=sim, dm=dm,
                keyword="dqrej", value=dqrej )
	    next
        }
        ;
    }
    ;

    order = 1
    function = "minmax"
    if ((ovscnmethod==1)||(ovscnmethod==4)) {
        order = 1
        function = "legendre"
    }
    ;
    if ((ovscnmethod==2)||(ovscnmethod==5)) {
        order = 100
        function = "legendre"
    }
    ;
    if (dqovjprb>0.5) {
	printf( "Jump in overscan detected\n", >> lfile )
        if (verbose>=1)
	    printf( "Jump in overscan detected\n" )
	;
        if (ovscnmethod>=4) {
            order = 1
            function = "minmax"
        }
        ;
    }
    ;

    if (ovscnmethod>=4) {
        # Run ccdproc - this needs to be done within the loop for
        # ovscnmode>=4 because the overscan subtraction method may be
        # different from file to file, depending on whether bias jumps
        # are present.
        iferr {
	    if (imaccess(im2))
	        imdelete (im2)
	    ;
	    if (ccdsum != "1 1" && ccdsum != "") {
		saturation = 65000
		hedit (im, "SATURATE", saturation, show-)
		imreplace (im, 65535, lower=INDEF, upper=0., radius=0)
	    }
	    ;
            _ccdtool (im, im2, "", bpm, fixpix=pflag, overscan=oflag,
	        zerocor=zflag, darkcor=dflag, flatcor=fflag, sflatcor=no,
	        saturation=saturation, sgrow=0, bleed=bleed, order=order,
		function=function, >> lfile)
        } then {
            sendmsg ("ERROR", $errmsg, "", "CAL")
            status = 0
        }
        ;
    }
    ;

    if (status == 0)
        break
    ;

    print (im, >> ilist1)
    print (im2, >> olist)
    if (bpm != "")
	print (bpm, >> blist)
    ;
}
list = ""

if (access(ilist1)==NO)
    logout (0)
;

if (status!=0 && ovscnmethod<=3) {
    if (access(blist))
        bpm = "@" // blist
    else
        bpm = ""

    # For ovscnmethod<=3 the method is the same for all images regardless
    # of the presence of jumps, so ccdproc (=_ccdtool) is run outside the loop
    iferr {
	if (ccdsum != "1 1" && ccdsum != "") {
	    saturation = 65000
	    hedit ("@"//ilist, "SATURATE", saturation, show-)
	    imreplace ("@"//ilist1, 65535, lower=INDEF, upper=0., radius=0)
	}
	;
        _ccdtool ("@"//ilist1, "@"//olist, "", bpm, fixpix=pflag,
	    overscan=oflag, zerocor=zflag, darkcor=dflag, flatcor=fflag,
	    sflatcor=no, saturation=saturation, sgrow=0, bleed=bleed,
	    order=order, function=function, >> lfile)
    } then {
        sendmsg ("ERROR", $errmsg, "", "CAL")
        status = 0
    }
    ;
}
;
delete (ilist1)
if (access(blist))
    delete (blist)
;

if (status == 0)
    logout (status)
;

# Write overscan subtraction method to header
hedit( "@"//olist, "ovscnmtd", ovscnmethod, add+, ver-, >> lfile )

# Merge amplifiers.
# For dual-amp this will reject those pairs with only one good amp.
# There is still a potential problem when all of one amp are bad.

if (namps != nccds) {
    for (i=1; i<=nccds; i+=1) {
	s3 = sclnames.shortname
	s3 = substr (s3, 1, strldx("-",s3)-1)
	printf ("%s_%02d\n", s3, i) | scan (s1)
	printf ("%s_%s\n", s1, sclnames.child) | scan (s2)
	files (s1//"_im??.fits", > "sclproc1.tmp")
	files (s1//"_im??_bpm*", > "sclproc1_bpm.tmp")
	count ("sclproc1.tmp") | scan (j)
	if (j <= 1) {
	    delete ("sclproc1.tmp")
	    delete ("sclproc1_bpm.tmp")
	    next
	}
	;
	if (saturation == "INDEF")
	    bpm = ""
	else
	    bpm = s2 // "_bpm"
	mergeamps ("@sclproc1.tmp", s2, bpm, amps=no,
	    imcmb="none", verbose+, >> lfile)
	if (imaccess(bpm)==NO) {
	    mergeamps ("@sclproc1_bpm.tmp", bpm, "", amps=no,
		imcmb="none", verbose+, >> lfile)
	    imcopy (bpm//".fits", bpm//".pl", v-)
	    imdelete (bpm//".fits")
	    hedit (s2, "bpm", bpm//".pl", add+)
	}
	;
	imdelete ("@sclproc1.tmp,@sclproc1_bpm.tmp")
	print (s2, >> "sclproc2.tmp")

	hedit (s2, "nextend", nextend, add+, verify-, update+, show-)
	hsel(s2, "mjd-obs", yes ) | scan( mjd )
	newDataProduct(s2, mjd, obstype, "") | scan(result, line)
	if (result>0)
	    sendmsg("ERROR", "newDataProduct failed", sim, "DM")
	;
	storekeywords( class="singleimage", id=s2, sid=s2, dm=dm )
	delete ("sclproc1.tmp,sclproc1_bpm.tmp")
    }
    if (access ("sclproc2.tmp"))
	rename ("sclproc2.tmp", olist)
    else {
	sendmsg ("ERROR", "Missing amps", sim, "CAL")
        delete (olist)
    }
}
;
if (access(olist)==NO) {
    logout 0
}
;

# Check for successful processing.
rename (olist, ilist1)
list = ilist1
while (fscan (list, im) != EOF) {
    if (imaccess (im) == NO)
        break
    ;
    # Determine mean and sigma and write to header
    mimstat( im, imask="!BPM", fields="mean,stddev,min,max", nclip=1, lsigma=2,
        usigma=2, format- ) | scan( dqglmean, dqglsig, min, max )

#    # Rather than depend on minreplace in ccdtool use the masked minimum.
#    # This is needed for vignetted data.  This "flag" value will be
#    # replaced by the average minimum in CALPCAL.
#    hedit (im, "CCDMIN", min, add+, show-)
#    imreplace (im, 0., lower=INDEF, upper=min, radius=0)

    # Get saturation threshold from header, then determine fraction
    # of saturated pixels.  Rather than working with the processed image
    # a more accurate way would be to count the number of pixels with a
    # value of 4 in the bad pixel mask.  For now we keep the old method.

    saturate = INDEF
    hselect( im, "i_naxis[1],i_naxis[2],saturate", yes ) |
        scan (dx,dy,saturate)
    npix = dx*dy; nsat = 0
    if (isindef(saturate))
        nsat = 0.
    else if (saturation == "INDEF") {
        imstat( im, fields="npix", lower=saturate, nclip=0, format- ) |
            scan(nsat)
    } else {
        bpm=""; hselect (im, "bpm", yes) | scan (bpm)
	if (bpm != "")
	    imstat( bpm, fields="npix", lower=4, upper=4, nclip=0, format- ) |
		scan(nsat)
	else
	    nsat = 0
    }
    dqglfsat = (1.*nsat)/(1.*npix)
    # Write data quality data to log file ...
    if (verbose>=2) {
        printf( "DQ | image: %s\n", im )
        printf( "DQ | global mean: %f\n", dqglmean )
        printf( "DQ | global sigma: %f\n", dqglsig )
        printf( "DQ | fraction of saturated pixels: %f\n", dqglfsat )
    }
    ;
    # ... to header ...
    hedit( im, "dqglfsat", dqglfsat, add+, ver-, >> lfile )
    hedit( im, "dqglmean", dqglmean, add+, ver-, >> lfile )
    hedit( im, "dqglsig", dqglsig, add+, ver-, >> lfile )
    # ... and to PMAS
    printf("%12.5e\n", dqglmean ) | scan( cast )
    setkeyval( class="singleimage", id=im, dm=dm,
        keyword="dqglmean", value=cast )
    printf("%12.5e\n", dqglsig ) | scan( cast )
    setkeyval( class="singleimage", id=im, dm=dm,
        keyword="dqglsig", value=cast )
    printf("%12.5e\n", dqglfsat ) | scan( cast )
    setkeyval( class="singleimage", id=im, dm=dm,
        keyword="dqglfsat", value=cast )

    # Reject bad exposures.
    if (dqglfsat > 0.01) {
	sendmsg ("ERROR", "DQGLFSAT", str(dqglfsat), "CAL")
        imdel (im)
	next
    }
    ;
    
    print (im, >> olist)
}
list = ""

if (access(olist)==NO)
    logout (0)
;

## Send mean overscan value to keyword monitor.
#s1 = envget ("KWMON") // ":" // envget ("NHPPS_NODE_SB")
#impkwm ("@"//olist, "OVSNMEAN", kwm=s1, element="!imageid", indexkw="mjd-obs")

# Delete data in the input data directory.
imdelete ("MOSAIC_SCL$/input/" // "data/@" // olist, >& "dev$null")

# Delete data in the MOSAIC_XTC directory.
match ("MOSAIC_XTC", ilist, print-, > "sclproc2.tmp")
#imdelete ("@sclproc2.tmp", >& "dev$null")
delete ("sclproc2.tmp")

# Make list of processed data.
pathnames ("@"//olist, > ilist)
delete (ilist1)
delete (olist)

logout (status)
