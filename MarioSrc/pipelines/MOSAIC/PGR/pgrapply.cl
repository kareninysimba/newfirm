#!/bin/env pipecl
#
# PGRAPPLY -- Apply pupil ghost information and set files to call SPG pipeline.

real	quality = -1
string	dataset, datadir, ifile, lfile, pgr
real	scale, mjd

# Packages and task.
images

# Set paths and files.
pgrnames (envget("OSF_DATASET"))
dataset = pgrnames.dataset
datadir = pgrnames.datadir
ifile = pgrnames.indir // dataset // ".pgr"
lfile = datadir // pgrnames.lfile
set (uparm = pgrnames.uparm)
cd (datadir)

# Log start of processing.
printf ("\nPGRAPPLY (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Determine if calibration was computed and its quality.
# To avoid a call to the DM we put the pupil ghost template in the local MC.
# The SPG pipeline is likely to be running on the same machine.

if (imaccess(pgrnames.pgr)) {
    hselect (pgrnames.pgr, "quality", yes) | scan (quality)
    # To avoid a call to the DM put the pupil ghost template in MC.
    if (quality >= 0.) {
	if (imaccess("MC$"//pgrnames.pgr)==YES)
	    sendmsg ("WARNING", "Calibration already in MarioCal",
	        pgrnames.pgr, "CAL")
	else
	    imcopy (pgrnames.pgr, "MC$", verb+)
	pgrnames.pgr = "MC$" // pgrnames.pgr
    }
}
;

# Loop over the input files and set things for the SPG pipeline.
list = ifile
while (fscan (list, s1) != EOF) {
    # Extract the image dataset name.
    s2 = substr (s1, strldx("/",s1)+1, strldx("-",s1)-1)
    s2 = substr (s2, strldx("-",s2)+1,1000)

    # Set the pupil ghost template if computed.
    if (quality >= 0.) {
	hedit (s1, "PUPIL", pgrnames.pgr, add+)
	hedit (s1, "PUPGHOST", pgrnames.pgr, add+)
    }
    ;

    # Set the scale if computed.
    if (access(dataset//".scl")) {
        scale = 1.; match (s2, dataset//".scl") | scan (s3, scale)
	hedit (s1, "PGRSCALE", scale, add+)
    }
    ;

    # Set output files.
    s2 = "pgr2spg_" // s2 // ".tmp"
    path (s2, >> "pgr2spg.tmp")
    print (s1, >> s2)
}
list = ""

logout 1
