#!/bin/env pipecl
#
# PGRSETUP -- Setup PGR processing data. 
# 
# The input list of images is assumed to contain the uparm keyword.

string	dataset, datadir, ifile, ifile1, lfile, mkcal
int	founddir
real	qual = 0.
file	caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers
proto
mscred
cache mscred
task mscextensions = "mscsrc$x_mscred.e"
task pupilfit = "mscsrc$x_mscred.e"
cache mscextensions

# Define rules
findrule (dm, "enough_for_pupilghost", validate=1) | scan (s1)
task enough_for_pupilghost = (s1)

# Set paths and files.
pgrnames (envget("OSF_DATASET"))
dataset = pgrnames.dataset
ifile = pgrnames.indir // dataset // ".pgr"
datadir = pgrnames.datadir
lfile = datadir // pgrnames.lfile
set (uparm = pgrnames.uparm)
set (pipedata = pgrnames.pipedata)

# Work in the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\nPGRSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Separate obm files.
match ("_obm.fits", ifile) | sort ( > ifile//"obm")
match ("_obm.fits", ifile, stop+) | sort ( > "pgrsetup.tmp")
rename ("pgrsetup.tmp", ifile)

# Setup UPARM.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
s1 = ""; head (ifile, nlines=1) | scan (s1)
iferr {
    setdirs (s1, umatch="!ccdsum")
} then {
    logout 0
} else
    ;

# Make pupil pattern mask and check if any part of the pattern is in the image.
# The presence or absence of the mask is used to define which extensions
# need the correction.

pupilfit (s1, pgrnames.pgm//".pl", mask="", type="mask", verbose-)
imstat (pgrnames.pgm, fields="npix", upper=0.1, format-) | scan (i)
if (i == 0)
    imdel (pgrnames.pgm)
;

# Check for library calibration.  The purpose of this loop here is to
# allow skipping generating an pupil ghost template if there is one in
# the library with higher precedence.  Note that the pgr_cal flag can force
# creation of an template.

i = fscan (pgr_cal, mkcal, qual)
if (mkcal == "libfirst" || mkcal == "libonly") {
    b1 = yes
    list = ifile
    while (fscan (list, s1) != EOF) {
        getcal (s1, "pupil", cm, caldir, obstype="", detector=instrument,
            imageid="!ccdname", filter="!filter", exptime="", mjd="!mjd-obs",
            dmjd=10*cal_dmjd, quality=qual, match="!ccdsum", > "dev$null")
        if (getcal.statcode != 0) {
	    b1 = no
	    break
	}
        ;
    }
    list = ""
} else
    b1 = no

if (mkcal == "libonly" || (b1 && mkcal == "libfirst")) {
    sendmsg ("WARNING", "Creation of pupil pattern skipped", "", "CAL")
    logout 2
}
;

# Check dataset comments for the A flag.
s1 = ""
if (access(ifile//"com"))
    match ("\#A", ifile//"com") | scan (s1)
;
if (s1 == "#A" && mkcal == "no")
    logout 2
;

# We're going to need the object masks from the input data.

list = ifile
while (fscan (list, s1) != EOF) {
    s2 = ""; hselect (s1, "objmask", yes) | scan (s2)
    if (s2 == "")
        next
    ;
    i = strldx ("[", s2)
    if (i > 0)
        s2 = substr (s2, 1, i-1) // ".fits"
    ;
    match (s2, ifile//"obm") | scan (s3)
    #copy (s3, ".", verbose+)
    iferr {
	imexpr ("min(a,11)", s2//"[pl,type=mask]", s3//"[pl]", verb-)
    } then {
	copy (s3, ".", verbose+)
    } else
        ;
}
list = ""

# Allow time for object masks to actually appear.  This is some kind of
# bug/problem with our pipeline OS such that the next stage might not
# actually find the files without the following delay.
sleep (1)

if (imaccess (pgrnames.pgm))
    logout 1
else
    logout 3
