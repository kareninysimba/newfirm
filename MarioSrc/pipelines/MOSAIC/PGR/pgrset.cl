#!/bin/env pipecl
#
# PGRSET -- Verify template and set pupil template.
#
# Flags:
# _     Pupil template created.  Possibly ask operator to review.
#       If no review enter in calibration library.
# Y     Pupil template is ok.  Enter in calibration library.
# N     Pupil template is not ok.  Use calibration library.
# A     Pupil template not created.  Use calibration library.
# S     Skip pupil ghost correction.

int	status = 1
real	quality = -1
real	mjd, mjdstart, mjdend
string	dataset, datadir, lfile
string	flag

# Packages and tasks.
images
servers

# Set paths and files.
flag = envget ("OSF_FLAG")
pgrnames (envget("OSF_DATASET"))
dataset = pgrnames.dataset
datadir = pgrnames.datadir
lfile = datadir // pgrnames.lfile
set (uparm = pgrnames.uparm)
cd (datadir)

# Log start of processing.
printf ("\nPGRSET (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Evaluate the pupil ghost template.
# We don't have anything here yet.

# Trigger DRV pipeline if it is running and the OSF flag is "_".
if (flag == "_") {
    task $pipeselect = "$!pipeselect"
    s1 = ""; pipeselect (envget ("NHPPS_SYS_NAME"), "drv", 0, 0) | scan (s1)
    if (s1 != "") {
	daynames (pgrnames.parent)
	s2 = daynames.parent // "-drv-" // daynames.child
	s3 = s1 // s2 // ".pgr"
	if (imaccess(pgrnames.pgr)) {
	    pathname (pgrnames.pgr, > s3)
	    touch (s3//"trig")
	    if (!dps_review)
		flag = "Y"
	    ;
	} else {
	    touch (s3//"trig")
	    flag = "Y"
	}
    } else
        flag = "Y"
}
;

# Wait for review.
if (flag == "_")
    logout 2
;

# Put the calibration files in the Data Manager.
if (flag == "Y") {
    if (imaccess(pgrnames.pgr)) {
	hselect (pgrnames.pgr, "mjd-obs,quality", yes) | scan (mjd, quality)
	mjdstart = mjd + cal_mjdmin; mjdend = mjd + cal_mjdmax
	putcal (pgrnames.pgr//".fits", "image", dm=cm, class="pupil",
	    detector=instrument, imageid="!ccdname", filter="!filter",
	    exptime="", quality=quality, match="!ccdsum",
	    mjdstart=mjdstart, mjdend=mjdend)
	if (putcal.statcode != 0) {
	    sendmsg ("ERROR", "Putcal failed", putcal.statstr, "CAL")
	    status = 0
	}
	;
    }
    ;
} else if (imaccess(pgrnames.pgr))
    imdelete (pgrnames.pgr)
;
if (status == 0)
    logout 4
;

# Don't apply pupil ghost correction.  This is here rather than earlier so the
# the code above can reset the flag.
if (flag == "A" && pgr_cal == "no")
    flag = "S"
;
if (flag == "S")
    logout 3
;

logout 1
