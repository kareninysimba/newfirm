#!/bin/env pipecl

# FTRSKY -- Analyze sky images to reject exposures from the sky stack.

int	status = 1
bool	update = yes
bool	show = no
int	figno = 1
struct	*fd

bool	sameref
int	n, ndet, ngroup
real	quality = -1.
string	s4, dataset, datadir, lfile, ilist, refskymap, stk, obm, fig, fign
real	hsigma, lsigma
real	mean = 1.
real	stddev = INDEF
real	stddev2 = INDEF

# Tasks and packages.
task $mkgraphic = "$!mkgraphic $1 $2 world png 1 'i1>0'"
images
proto
utilities
servers
noao
nproto
nfextern
ace
mscred
cache mscred, scombine
task mscskysub = "mscsrc$x_mscred.e"
task scmb = "mariosrc$scmb.cl"
task sftselect = "NHPPS_PIPESRC$/MOSAIC/SFT/sftselect.cl"
cache sftselect
task ftrmontage = "NHPPS_PIPESRC$/MOSAIC/FTR/ftrmontage.cl"
task ftrsigclip = "NHPPS_PIPESRC$/MOSAIC/FTR/ftrsigclip.cl"
cache ftrsigclip
findrule (dm, "enough_for_sflat", validate=1) | scan (line)
task enough_for_sflat = (line)

# Files and directories.
names ("ftr", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
ilist = datadir // dataset // ".list"
fig = names.shortname// "_fig"
stk = names.shortname // "_skymap"
obm = names.shortname // "_skymap_obm"
mscred.logfile = ""
mscred.verbose = yes
mscred.instrument = "pipedata$mosaic.dat"
reset (pipedata=names.pipedata)
reset (uparm=names.uparm)
cd (datadir)

# Clean up for restart.
imdelete ("*_sky*,ftrsky*.fits")
delete ("ftrsky*.tmp,ftrupd*.tmp,*.png,*.html")

# Check sky data.
match ("MOSAIC_MEF?*_sky", ilist, > "ftrsky1.tmp")
count ("ftrsky1.tmp") | scan (i)
if (i == 0) {
    delete ("ftrsky*.tmp")
    logout 1
}
;

# Log start of processing.
printf ("\n%s (%s): ", "FTRSKY", dataset) | tee (lfile)
time | tee (lfile)

# Get sky images.
imcopy ("@ftrsky1.tmp", ".", verbose-)
files ("*_sky.fits", > "ftrsky1a.tmp")

# Check whether to reset flags.
hedit ("@ftrsky1a.tmp", "???FLAG", ".") |
    translit ("STDIN", ',="', " ", > "ftrsky1b.tmp")
match ("standard deviation", "ftrsky1b.tmp", > "ftrsky1c.tmp")
match ("sky mean", "ftrsky1b.tmp", >> "ftrsky1c.tmp")
match ("magnitude zeropoint", "ftrsky1b.tmp", >> "ftrsky1c.tmp")
match ("RMS", "ftrsky1b.tmp", >> "ftrsky1c.tmp")
match ("extended source light", "ftrsky1b.tmp", >> "ftrsky1c.tmp")
fields ("ftrsky1c.tmp", "1,2") | sort | uniq (> "ftrsky1b.tmp")
delete ("ftrsky1c.tmp")

# Reset flags.
match ("PRGFLAG", "ftrsky1b.tmp", > "ftrsky1c.tmp")
count ("ftrsky1c.tmp") | scan (i)
if (i > 0) {
    touch ("ftrsky.tmp,ftrskyobm.tmp")
    list = "ftrsky1c.tmp"
    while (fscan (list, s1, s2) != EOF) {
	s2 = substr (s1, 1, strstr("_sky",s1)-1)
	print (s1, >> "ftrsky.tmp")
	match (s2//"_00", ilist, >> "ftrsky.tmp")
	match (s2//"_sky", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*.fits", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*_obm.fits", ilist, >> "ftrskyobm.tmp")
    }
    list = "ftrskyobm.tmp"
    while (fscan (list, s1) != EOF)
	print (s1//"[pl]", >> "ftrsky.tmp")
    list = ""

    list = "ftrsky.tmp"
    while (fscan (list, s1) != EOF)
	hedit (s1, "PGRFLAG", "Y", show=no, verify-, update+)
    list = ""
    delete ("ftrsky.tmp,ftrskyobm.tmp")
}
;

match ("FRGFLAG", "ftrsky1b.tmp", > "ftrsky1c.tmp")
count ("ftrsky1c.tmp") | scan (i)
if (i > 0) {
    touch ("ftrsky.tmp,ftrskyobm.tmp")
    list = "ftrsky1c.tmp"
    while (fscan (list, s1, s2) != EOF) {
	s2 = substr (s1, 1, strstr("_sky",s1)-1)
	print (s1, >> "ftrsky.tmp")
	match (s2//"_00", ilist, >> "ftrsky.tmp")
	match (s2//"_sky", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*.fits", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*_obm.fits", ilist, >> "ftrskyobm.tmp")
    }
    list = "ftrskyobm.tmp"
    while (fscan (list, s1) != EOF)
	print (s1//"[pl]", >> "ftrsky.tmp")
    list = ""

    list = "ftrsky.tmp"
    while (fscan (list, s1) != EOF)
	hedit (s1, "FRGFLAG", "Y", show=no, verify-, update+)
    list = ""
    delete ("ftrsky.tmp,ftrskyobm.tmp")
}
;

match ("SFTFLAG", "ftrsky1b.tmp", > "ftrsky1c.tmp")
count ("ftrsky1c.tmp") | scan (i)
if (i > 0) {
    touch ("ftrsky.tmp,ftrskyobm.tmp")
    list = "ftrsky1c.tmp"
    while (fscan (list, s1, s2) != EOF) {
	s2 = substr (s1, 1, strstr("_sky",s1)-1)
	print (s1, >> "ftrsky.tmp")
	match (s2//"_00", ilist, >> "ftrsky.tmp")
	match (s2//"_sky", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*.fits", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*_obm.fits", ilist, >> "ftrskyobm.tmp")
    }
    list = "ftrskyobm.tmp"
    while (fscan (list, s1) != EOF)
	print (s1//"[pl]", >> "ftrsky.tmp")
    list = ""

    list = "ftrsky.tmp"
    while (fscan (list, s1) != EOF)
	hedit (s1, "SFTFLAG", "Y", show=no, verify-, update+)
    list = ""
    delete ("ftrsky.tmp,ftrskyobm.tmp")
}
;

delete ("ftrsky1b.tmp,ftrsky1c.tmp")

# Manual rejections.
s1 = datadir // dataset // "_rej.list"
if (access(s1)) {
    list = s1
    while (fscan (list, s1, line) != EOF) {
	s2 = substr (s1, 1, strstr("_sky",s1)-1)
	print (s1, >> "ftrsky.tmp")
	match (s2//"_00", ilist, >> "ftrsky.tmp")
	match (s2//"_sky", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*.fits", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*_obm.fits", ilist, >> "ftrskyobm.tmp")
	fd = "ftrskyobm.tmp"
	while (fscan (fd, s3) != EOF)
	    print (s3//"[pl]", >> "ftrsky.tmp")
	fd = ""

	fd = "ftrsky.tmp"
	while (fscan (fd, s1) != EOF) {
	    hedit (s1, "PGRFLAG", line, add+, show=no, verify-, update+)
	    hedit (s1, "FGRFLAG", line, add+, show=no, verify-, update+)
	    hedit (s1, "SFTFLAG", line, add+, show=no, verify-, update+)
	}
	fd = ""
	delete ("ftrsky.tmp,ftrskyobm.tmp")
    }
    list = ""
    rename (s1, s1//"BAK")
}
;

# Reset pipedata.
head ("ftrsky1a.tmp") | scan (s1)
hselect (s1, "pipedata", yes) | scan (names.pipedata)
reset (pipedata=names.pipedata//"/")

# Start HTML file.
pathname (fig//".html", >> ilist)
printf ('<HTML><HEAD><TITLE>Skies for %s</TITLE></HEAD></BODY>\n',
    names.shortname, > fig//".html")
printf ('<CENTER><H2>Sky Analysis for %s</H2></CENTER>\n',
    names.shortname, >> fig//".html")
printf ('\n<H4>Sky Map Analysis</H4>\n\n', >> fig//".html")

# Record list of sky files.
printf ('<P>Figure %d: List of all sky files.\n', figno, >> fig//".html")
printf ('<PRE>\n', >> fig//".html")
hselect ("@ftrsky1a.tmp", "$I,SFTFLAG", yes) |
    translit ("STDIN", '"', delete+, >> fig//".html")
printf ('</PRE>\n', >> fig//".html")

# Make montage of all sky files.
ftrmontage ("ftrsky1a.tmp", "All sky files.", output=fig, olist=ilist,
    temp="ftrsky", ncol=10, nstep=45)

# Extract exposures which have not been previously rejected and make montage.
sftselect ("SFTFLAG", "ftrsky1a.tmp", "ftrskygood.tmp", "dev$null", cksftflag+)
if (sftselect.nselect > 0) {
    ftrmontage ("ftrskygood.tmp", "Sky files after rejection by prior criteria.",
        output=fig, olist=ilist, temp="ftrsky", ltrim=0)
} else
    printf ('<P>All skies rejected by prior criteria: exposure time, \
	source density, and maximum source size.\n', >> fig//".html")

# Finish if the exposures have been specifically selected.
if (sftselect.nforced > 0) {
    printf ("Sky flat exposures explicitly selected.\n", >> lfile)
    printf ('<P>Sky flat exposures explicitly selected.\n', >> fig//".html")
    imdelete ("@ftrsky1a.tmp,ftrsky*.fits")
    delete ("ftrsky*.tmp")
    logout (status)
}
;

# Check if there are enough exposures for the rule.
n = sftselect.nselect
enough_for_sflat (sftselect.filter, n) | scan (i)
if (i == 0) {
    printf ("Not enough for making sky flat.\n", >> lfile)
    printf ('<P>Not enough for making sky flat.\n', >> fig//".html")
    imdelete ("@ftrsky1a.tmp,ftrsky*.fits")
    delete ("ftrsky*.tmp")
    logout (status)
}
;

# Apply a reference sky map if one is available.
head ("ftrsky1a.tmp", nl=1) | scan (refskymap)
getcal (refskymap, "refskymap", cm, "MC$", obstype="", detector=cl.instrument,
    imageid="", filter="!filter", exptime="", mjd="!mjd-obs", dmjd=INDEF,
    quality=0, match="!nextend,ccdsum", > "dev$null")
if (getcal.statcode == 0) {
    refskymap = getcal.value
    printf ("Using reference sky map (%s)\n", refskymap, >> lfile)

    fd = "ftrsky1a.tmp"
    while (fscan (fd, s1) != EOF) {
	s2 = "orig_" // s1
	print (s2, >> "ftrsky1b.tmp")
        imrename (s1, s2)
	imexpr ("a==-1000?a:(a/b)", s1, s2, refskymap, verbose-)
    }
    fd = ""

    refskymap = substr (refskymap, stridx("$",refskymap)+1,
        strstr(".fits",refskymap)-1)
    ftrmontage ("ftrskygood.tmp", "Flat field by reference "//refskymap//".",
        output=fig, olist=ilist, temp="ftrsky", ltrim=0)

    # Check if it is from the same dataset. This assumes a specific
    # naming convention.
    i = stridx ("-", refskymap)
    s1 = substr(refskymap, 1, strldx("_", substr(refskymap,1,i)))
    s2 = substr(stk, 1, strldx("_", substr(stk,1,i)))
    sameref = (s1 == s2)
    if (sameref) {
        s1 = substr (refskymap, i, 999)
        s2 = substr (stk, i, 999)
	sameref = (s1 == s2)
    }
    ;
} else {
    refskymap = ""
    sameref = NO
}

# Do sigma clipping.
count ("ftrsky1a.tmp") | scan (n)
if (n > 5) {

    # Image standard deviation.
    imstat ("@ftrskygood.tmp", fields="image,stddev", format-,
	lower=-999, > "ftrsky2.tmp")
    ftrsigclip ("ftrsky2.tmp", "ftrskygood.tmp", "ftrskyrej.tmp",
        temp="ftrsky3", html=fig//".html", logfile=lfile,
	label="Standard deviation", lclip=6, hclip=3, lsigma=6, hsigma=3)
    stddev2 = ftrsigclip.mean
    delete ("ftrsky2.tmp")

    # Sky mean.
    hselect ("@ftrsky1a.tmp", "$I,$SKYMEAN", yes, > "ftrsky2.tmp")
    ftrsigclip ("ftrsky2.tmp", "ftrskygood.tmp", "ftrskyrej.tmp",
        temp="ftrsky3", html=fig//".html", logfile=lfile,
	label="Sky mean",
	lclip=3, hclip=3, lsigma=4, hsigma=10)
    delete ("ftrsky2.tmp")

    # Magnitude zeropoint.
    hselect ("@ftrsky1a.tmp", "$I,$MAGZERO", yes, > "ftrsky2.tmp")
    ftrsigclip ("ftrsky2.tmp", "ftrskygood.tmp", "ftrskyrej.tmp",
        temp="ftrsky3", html=fig//".html", logfile=lfile,
	label="Magnitude zero point",
	lclip=3, hclip=3, lsigma=4, hsigma=8)
    delete ("ftrsky2.tmp")

    # Make montage of remaining skies.
    if (access("ftrskyrej.tmp") && access("ftrskygood.tmp")) {
	ftrmontage ("ftrskygood.tmp", "Sky files after sigma clipping.",
	    output=fig, olist=ilist, temp="ftrsky", ltrim=0)
    }
    ;
}
;

# Check number rule.
if (access("ftrskygood.tmp"))
    count ("ftrskygood.tmp") | scan (n)
else
    n = 0
enough_for_sflat (sftselect.filter, n) | scan (i)
if (i == 0) {
    printf ("Not enough remaining for making sky flat\n", >> lfile)
    printf ('<P>Not enough remaining for making sky flat\n', >> fig//".html")

    # Flag rejected skys.
    if (access("ftrskyrej.tmp")) {
	touch ("ftrsky.tmp,ftrskyobm.tmp")
	list = "ftrskyrej.tmp"
	while (fscan (list, s1, line) != EOF) {
	    s2 = substr (s1, 1, strstr("_sky",s1)-1)
	    print (line, >> "ftrsky.tmp")
	    match (s2//"_00", ilist, >> "ftrsky.tmp")
	    match (s2//"_sky", ilist, >> "ftrsky.tmp")
	    match (s2//"-ccd[0-9]*.fits", ilist, >> "ftrsky.tmp")
	    print (line, >> "ftrskyobm.tmp")
	    match (s2//"-ccd[0-9]*_obm.fits", ilist, >> "ftrskyobm.tmp")
	}
	list = ""

	list = "ftrskyobm.tmp"
	while (fscan (list, s1, line) != EOF) {
	    if (s1 == "#")
		printf ("%s %s\n", s1, line, >> "ftrsky.tmp")
	    else
		print (s1//"[pl]", >> "ftrsky.tmp")
	}
	list = ""

	list = "ftrsky.tmp"; s2 = ""
	while (fscan (list, s1, s3) != EOF) {
	    if (s1 == "#") {
	        s2 = s3
		next
	    }
	    ;
	    if (s2 == "Standard") {
		hedit (s1, "SFTFLAG",
		    "((SFTFLAG=='Y')?'N standard deviation':SFTFLAG)",
		    show=show, verify-, update=update)
		hedit (s1, "FRGFLAG",
		    "((FRGFLAG=='Y')?'N standard deviation':FRGFLAG)",
		    show=show, verify-, update=update)
		hedit (s1, "PGRFLAG",
		    "((PGRFLAG=='Y')?'N standard deviation':PGRFLAG)",
		    show=show, verify-, update=update)
	    } else if (s2 == "Sky") {
		hedit (s1, "SFTFLAG",
		    "((SFTFLAG=='Y')?'N sky mean':SFTFLAG)",
		    show=show, verify-, update=update)
		hedit (s1, "FRGFLAG",
		    "((FRGFLAG=='Y')?'N sky mean':FRGFLAG)",
		    show=show, verify-, update=update)
	    } else if (s2 == "Magnitude") {
		hedit (s1, "SFTFLAG",
		    "((SFTFLAG=='Y')?'N magnitude zeropoint':SFTFLAG)",
		    show=show, verify-, update=update)
		hedit (s1, "FRGFLAG",
		    "((FRGFLAG=='Y')?'N magnitude zeropoint':FRGFLAG)",
		    show=show, verify-, update=update)
	    }
	    ;
	}
	list = ""

	delete ("ftrskyrej.tmp,ftrsky.tmp,ftrskyobm.tmp")
    }
    ;

    imdelete ("@ftrsky1a.tmp,ftrsky*.fits")
    delete ("ftrsky*.tmp")
    logout (status)
}
;

# Remove sky gradients.  We need to make masks to exclude missing data.
list = "ftrskygood.tmp"
while (fscan(list,s1) != EOF) {
    s2 = substr (s1, strldx("/",s1)+1, strstr(".fits",s1)-1)
    imexpr ("a==-1000?1 : 0", s2//"bpm.pl", s1, verbose-)
    mscskysub (s1, s2, 2, 2, xmed=1, ymed=1, mask=s2//"bpm.pl")
    hedit (s2, "BPM", s2//"bpm.pl", add+, show=no, verify-)
    print (s2//"bpm.pl", >> "ftrskybpm.tmp")
}
list = ""

# Make montage.
ftrmontage ("ftrskygood.tmp", "Sky files after removing planar gradient.",
    output=fig, olist=ilist, temp="ftrsky", ltrim=0)

# Make sky stack and subtract from each sky map.
unlearn scombine
scombine.masktype = "!BPM"
scombine.blank = -1000
scombine.lsigma=3
scombine.hsigma=3
sftselect ("SFTFLAG", "ftrskygood.tmp", "dev$null", "ftrskyscales.tmp",
    cksftflag+)
scmb ("ftrskygood.tmp", stk, stk//"_bpm", "ftrskyscales.tmp", "ftrskystk",
    ngroup=INDEF, imcmb="IMCMBMEF", masktype="goodvalue", > "dev$null")
mimstat (stk, imask="!BPM", fields="mean,stddev", format-) | scan (mean,stddev)
joinlines ("ftrskygood.tmp", "ftrskyscales.tmp", > "ftrsky2.tmp")
rename ("ftrsky2.tmp", "ftrskyscales.tmp")

list = "ftrskyscales.tmp"
while (fscan(list,s1,x)!=EOF) {
    s2 = "ss_" // s1
    imexpr ("c==0?(a-b/d)+e:a", s2, s1, stk, stk//"_bpm", x, mean, verbose-)
    print (s2, >> "ftrsky1c.tmp")
    imstat (s2, fields="image,stddev", format-, lower=-999, >> "ftrsky2.tmp")
}
list = ""

imrename (stk, stk//"tmp")
imexpr ("c==0?a/b:a", stk, stk//"tmp", mean, stk//"_bpm", verbose-)
imdelete (stk//"tmp")
stddev /= mean; mean = 1

figno += 1; fign = fig + figno
mkgraphic (stk, fign)
printf ('<P>Figure %d: Sky stack.\n', figno, >> fig//".html")
printf ('<P><IMG SRC="%s", width=300>\n', fign//".png", >> fig//".html")
pathname (fign//".png", >> ilist)

# Reject deviant exposures.
if (n > 5) {

    # Image RMS.
    imstat ("@ftrsky1c.tmp", fields="image,stddev", format-,
	lower=-999, >> "ftrsky2.tmp")
    ftrsigclip ("ftrsky2.tmp", "ftrskygood.tmp", "ftrskyrej1.tmp",
        temp="ftrsky3", html=fig//".html", logfile=lfile,
	label="RMS", lclip=2, hclip=2, lsigma=3, hsigma=3)
    stddev2 = ftrsigclip.mean
    delete ("ftrsky2.tmp")

    # If exposures are rejected restack and make figures.
    if (access("ftrskyrej1.tmp")) {
	concat ("ftrskyrej1.tmp", >> "ftrskyrej.tmp")
	imdelete ("@ftrsky1c.tmp,"//stk//","//stk//"_bpm")
	delete ("ftrsky1c.tmp,ftrskyrej1.tmp,ftrskyscales.tmp")
	sftselect ("SFTFLAG", "ftrskygood.tmp", "dev$null", "ftrskyscales.tmp",
	    cksftflag+)
	scmb ("ftrskygood.tmp", stk, stk//"_bpm", "ftrskyscales.tmp",
	    "ftrskystk", ngroup=INDEF, imcmb="IMCMBMEF",
	    masktype="goodvalue", > "dev$null")
	mimstat (stk, imask="!BPM", fields="mean,stddev", format-) |
	    scan (mean, stddev)
	joinlines ("ftrskygood.tmp", "ftrskyscales.tmp", > "ftrsky2.tmp")
	rename ("ftrsky2.tmp", "ftrskyscales.tmp")

	list = "ftrskyscales.tmp"
	while (fscan(list,s1,x)!=EOF) {
	    s2 = "ss_" // s1
	    imexpr ("c==0?(a-b/d)+e:a", s2, s1, stk, stk//"_bpm",
	        x, mean, verbose-)
	    print (s2, >> "ftrsky1c.tmp")
	}
	list = ""

	ftrmontage ("ftrskygood.tmp", "Sky files after rms rejection.",
	    output=fig, olist=ilist, temp="ftrsky", ltrim=0)

	imrename (stk, stk//"tmp")
	imexpr ("c==0?a/b:a", stk, stk//"tmp", mean, stk//"_bpm", verbose-)
	imdelete (stk//"tmp")
	stddev /= mean; mean = 1

	figno += 1; fign = fig + figno
	mkgraphic (stk, fign)
	printf ('<P>Figure %d: Sky stack after rms rejection.\n',
	    figno, >> fig//".html")
	printf ('<P><IMG SRC="%s", width=300>\n', fign//".png", >> fig//".html")
	pathname (fign//".png", >> ilist)
    }
    ;
}
;

# Make montage.
ftrmontage ("ftrsky1c.tmp", 'Sky files after "sky subtraction".',
    output=fig, olist=ilist, temp="ftrsky", ltrim=3)

# Detect sources.
if (n >= 5) { 
    if (isindef(stddev2))
        s3 = ""
    else if (refskymap == "")
	s3 = str (max (stddev2/2, 0.01))
    else
	s3 = str (max (stddev2/2, 0.01))
    touch ("ftrskyrej1.tmp,ftrskyrej2.tmp,ftrskygood1.tmp,ftrskygood2.tmp")
    for (hsigma=8; hsigma<=15; hsigma+=1) {
	if (refskymap == "")
	    lsigma = 10.
	else
	    lsigma = hsigma
	delete ("ftrskyrej[12]*.tmp,ftrskygood[12]*.tmp")
	count ("ftrsky1c.tmp") | scan (n)
	list = "ftrsky1c.tmp"; i = 0; j = 0
	while (fscan (list,s2) != EOF) {
	    s1 = substr (s2, 4, 999)
	    aceall (s2//"[3:38,3:38]", masks="!BPM", skyotype="subsky",
		skies="", sigmas=s3, objmasks="ftrskyobm.pl", catalogs="",
		skyimage="", exps="", gains="", omtype="all", extnames="",
		catdefs="", catfilter="", logfiles=lfile, verbose=2,
		order="", nmaxrec=INDEF, gwtsig=INDEF, gwtnsig=INDEF,
		fitstep=1, fitblk1d=1, fithclip=2., fitlclip=3.,
		fitxorder=1, fityorder=1, fitxterms="half", blkstep=0,
		blksize=0, blknsubblks=2, updatesky=no, bpdetect="100",
		bpflag="1-5", convolve="bilinear 3 3", hsigma=hsigma,
		lsigma=lsigma, hdetect=yes, ldetect=yes, neighbors="8",
		minpix=9, sigavg=4., sigmax=4., bpval=INDEF,
		splitmax=INDEF, splitstep=0., splitthresh=5.,
		sminpix=8, ssigavg=10., ssigmax=5., ngrow=0, agrow=0.,
		magzero="INDEF") |
		match ("objects detected") | scan (ndet)
	    if (ndet > 0) {
		printf ("%s %s\n", s1, "# Source detection",
		    >> "ftrskyrej1.tmp")
		printf ("%s %s\n", s2, "# Source detection",
		    >> "ftrskyrej2.tmp")
		j += 1
	    } else {
		print (s1, >> "ftrskygood1.tmp")
		print (s2, >> "ftrskygood2.tmp")
	    }
	    imdelete ("ftrskyobm.pl")
	}
	list = ""

	if (n-j >= max(5,n/2))
	    break
	;
    }

    # Output results.
    if (access("ftrskyrej2.tmp") && access("ftrskygood2.tmp")) {
	ftrmontage ("ftrskygood2.tmp", "Sky files after source rejection.",
	    output=fig, olist=ilist, temp="ftrsky", ltrim=3)
	delete ("ftrskyrej2.tmp,ftrskygood2.tmp")
    } else if (access("ftrskygood2.tmp")) {
	printf ('<P> No residual "sources" detected.\n', >> fig//".html")
	delete ("ftrskygood2.tmp")
    } else {
	printf ('<P> Residual "sources" detected in all exposures.\n',
	    >> fig//".html")
	delete ("ftrskyrej2.tmp")
    }

    # Check how many were rejected.
    #if (n < 8 && n - j < 2) {
    if (n < 0 && n - j < 2) {
	printf ('<P> <B>Not enough remaining exposures.</B>\n', >> fig//".html")
	concat ("ftrskyrej1.tmp", >> "ftrskyrej.tmp")
	delete ("ftrskyrej1.tmp,ftrskygood*.tmp")
    #} else if (n - j < max (4, n/3)) {
    } else if (n - j < max (4, n/4)) {
	printf ('<P> <B>Not enough remaining exposures.  \
	    Source rejection ignored.</B>\n', >> fig//".html")
    } else {
	if (access("ftrskygood1.tmp")) {
	    rename ("ftrskygood1.tmp", "ftrskygood.tmp")
	    if (access ("ftrskyrej1.tmp"))
		concat ("ftrskyrej1.tmp", >> "ftrskyrej.tmp")
	    ;
	} else {
	    if (access ("ftrskyrej1.tmp"))
		concat ("ftrskyrej1.tmp", >> "ftrskyrej.tmp")
	    ;
	    delete ("ftrskygood.tmp,ftrskyrej1.tmp")
	}
	;
    }

    # If exposures are rejected restack.
    imdelete ("@ftrsky1c.tmp")
    if (access("ftrskyrej1.tmp")) {
	imdelete (stk//","//stk//"_bpm")
	sftselect ("SFTFLAG", "ftrskygood.tmp", "dev$null", "ftrskyscales.tmp",
	    cksftflag+)
	scmb ("ftrskygood.tmp", stk, stk//"_bpm", "ftrskyscales.tmp",
	    "ftrskystk", ngroup=INDEF, imcmb="IMCMBMEF",
	    masktype="goodvalue", > "dev$null")
	mimstat (stk, imask="!BPM", fields="mean,stddev", format-) |
	    scan (mean, stddev)
	imrename (stk, stk//"tmp")
	imexpr ("c==0?a/b:a", stk, stk//"tmp", mean, stk//"_bpm", verbose-)
	imdelete (stk//"tmp")
	stddev /= mean; mean = 1

	figno += 1; fign = fig + figno
	mkgraphic (stk, fign)
	printf ('<P>Figure %d: Sky stack.\n', figno, >> fig//".html")
	printf ('<P><IMG SRC="%s", width=300>\n', fign//".png", >> fig//".html")
	pathname (fign//".png", >> ilist)
    }
    ;
}
;

# If there is a good list left evaluate it further.
if (access("ftrskygood.tmp")) {
    # Look for residual source light in stack.
    if (isindef(stddev))
        s3 = ""
    if (refskymap == "")
	s3 = str (max (stddev/3, 0.002))
    else
	s3 = str (max (stddev/2, 0.002))
    if (refskymap == "") {
        hsigma = 8.
        lsigma = 12.
    } else {
        hsigma = 8.
        lsigma = 12.
    }
    aceall (stk//"[3:38,3:38]", masks="!BPM", skyotype="subsky", skies=mean,
	sigmas=s3, objmasks=obm, catalogs="", skyimage="", exps="",
	gains="", omtype="all", extnames="", catdefs="", catfilter="",
	logfiles=lfile, verbose=2, order="", nmaxrec=INDEF, gwtsig=INDEF,
	gwtnsig=INDEF, fitstep=1, fitblk1d=1, fithclip=2., fitlclip=3.,
	fitxorder=1, fityorder=1, fitxterms="half", blkstep=0, blksize=0,
	blknsubblks=2, updatesky=no, bpdetect="100", bpflag="1-5",
	convolve="bilinear 3 3", hsigma=hsigma, lsigma=lsigma, hdetect=yes,
	ldetect=yes, neighbors="8", minpix=9, sigavg=4., sigmax=4.,
	bpval=INDEF, splitmax=INDEF, splitstep=0., splitthresh=5.,
	sminpix=8, ssigavg=10., ssigmax=5., ngrow=0, agrow=0.,
	magzero="INDEF") |
	match ("objects detected") | scan (ndet)
    if (ndet > 0 && !sameref) {
	printf ('<P><B>Residual source light (%d) detected in sky stack. \
	    Sky flat creation will be skipped.</B>\n', ndet, >> fig//".html")
    }
    ;

    # If a reference sky map was used make a stack from the original data.
    if (refskymap != "") {
	imdelete ("@ftrsky1a.tmp,"//stk//"_bpm")
	imrename ("@ftrsky1b.tmp", "@ftrsky1a.tmp")
	imrename (stk, stk//"1")
	delete ("ftrskyscales.tmp")
	sftselect ("SFTFLAG", "ftrskygood.tmp", "dev$null", "ftrskyscales.tmp",
	    cksftflag+)
	scmb ("ftrskygood.tmp", stk, stk//"_bpm", "ftrskyscales.tmp",
	    "ftrskystk", ngroup=INDEF, imcmb="IMCMBMEF",
	    masktype="goodvalue", > "dev$null")

	figno += 1; fign = fig + figno
	mkgraphic (stk, fign)
	printf ('<P>Figure %d: Final sky stack.\n', figno, >> fig//".html")
	printf ('<P><IMG SRC="%s", width=300>\n', fign//".png", >> fig//".html")
	pathname (fign//".png", >> ilist)
    }
    ;

    # Reject all remaining exposures if extended source light is detected.
    if (ndet > 0 && !sameref) {
	list = "ftrskygood.tmp"
	while (fscan(list,s1) != EOF)
	    printf ("%s %s\n", s1, "# Source detection", >> "ftrskyrej.tmp")
	list = ""; delete ("ftrskygood.tmp")
	quality = -1
    } else
	quality = 0
} else
    quality = -1

# Put into the calibration library.
if (imaccess(stk)) {
    hselect (stk, "mjd-obs", yes) | scan (x)
    y = x + cal_mjdmin
    z = x + cal_mjdmax
    putcal (stk//".fits", "image", cm, class="refskymap",
	detector=cl.instrument, imageid="", filter="!filter", exptime="",
	quality=quality, match="!nextend,ccdsum", mjdstart=y, mjdend=z,
	>& "dev$null")
}
;

# Set flags.

# This rename pushes the updating to a separate stage.
if (access("ftrskyrej.tmp"))
    rename ("ftrskyrej.tmp", "ftrupdrej.tmp")
;

if (access("ftrskyrej.tmp")) {
    touch ("ftrsky.tmp,ftrskyobm.tmp")
    list = "ftrskyrej.tmp"
    while (fscan (list, s1, line) != EOF) {
	s2 = substr (s1, 1, strstr("_sky",s1)-1)
        print (line, >> "ftrsky.tmp")
	match (s2//"_00", ilist, >> "ftrsky.tmp")
	match (s2//"_sky", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*.fits", ilist, >> "ftrsky.tmp")
        print (line, >> "ftrskyobm.tmp")
	match (s2//"-ccd[0-9]*_obm.fits", ilist, >> "ftrskyobm.tmp")
    }
    list = ""

    list = "ftrskyobm.tmp"
    while (fscan (list, s1, line) != EOF) {
	if (s1 == "#")
	    printf ("%s %s\n", s1, line, >> "ftrsky.tmp")
	else
	    print (s1//"[pl]", >> "ftrsky.tmp")
    }
    list = ""

    list = "ftrsky.tmp"; s2 = ""
    while (fscan (list, s1, s3) != EOF) {
	if (s1 == "#") {
	    s2 = s3
	    next
	}
	;
	printf ("Update %s = %s\n", s1, s2)
	if (s2 == "Standard") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N standard deviation':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N standard deviation':FRGFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "PGRFLAG",
		"((PGRFLAG=='Y')?'N standard deviation':PGRFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Sky") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N sky mean':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N sky mean':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Magnitude") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N magnitude zeropoint':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N magnitude zeropoint':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "RMS") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N RMS':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N RMS':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Source") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N extended source light':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N extended source light':FRGFLAG)",
		show=show, verify-, update=update)
	}
	;
	flpr
    }
    list = ""
    delete ("ftrskyrej.tmp,ftrsky.tmp,ftrskyobm.tmp")
}
;

# Cleanup.
imdelete ("@ftrsky1a.tmp,@ftrskybpm.tmp,ftrsky*.fits")
delete ("ftrsky*.tmp")

# Set for interactive review.
if (quality == -1 && status == 1 && plinteract == "yes")
    status = 2
;

logout (status)
