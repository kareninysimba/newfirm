#!/bin/env pipecl
#
# FTRNGTCK -- Check results of NGT pipeline to determine whether to call
# the DAY and SCI pipelines.
#
# STATUS
# 0		Trapped or untrapped fatal error
# 1		Data for DAY and SCI pipelines
# 2		No data for DAY and SCI pipelines

int	status = 1
int	nfiles
string	dataset, datadir, lfile

# Define packages and tasks.
utilities

# Set files and pathnames.
names ("ftr", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = names.lfile

# Log processing.
printf ("\nFTRNGTCK (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Check for processed science exposures.
match ("MOSAIC_MEF?*_00.fits", datadir//dataset//".list") | count | scan (nfiles)
if (nfiles == 0)
    status = 2
;

logout (status)
