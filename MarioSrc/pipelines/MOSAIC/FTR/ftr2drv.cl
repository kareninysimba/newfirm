#!/bin/env pipecl
#
# FTR2DRV -- Send list of DAY datasets to the DRV pipeline.
#
# Note this is a simple modification of the module called to send data
# to the DAY pipeline (i.e.  lis2pipe).

string	option				# Option
string	patcmd				# Pattern command
string	cpipe				# Calling pipeline
string	tpipe 				# Target pipeline
int	ntrig = 2			# Number to trigger here

int	status = 2
string	dataset, module, indir, datadir, ilist, subfile, lfile
string	olist, patlist, temp

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# The calling pipeline is set from the environment and the target pipeline
# is set by an argument.

names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))

cpipe = names.pipe
if (fscan (cl.args, option, patcmd, tpipe, ntrig) < 3)
    logout 0
;

# Files and directories.
dataset = names.dataset
module = envget ("NHPPS_MODULE_NAME")
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
olist = module//"1.tmp"
patlist = module//"2.tmp"
temp = module//"3.tmp"
cd (datadir)

# Select input list.
if (option == "trigger")
    ilist = indir // dataset // "." // cpipe
else if (option == "datalist")
    ilist = datadir // dataset // ".list"

# Log start of processing.
printf ("\n%s (%s): ", module, dataset) | tee (lfile)
time | tee (lfile)

# Check input list.
if (access (ilist) == NO) {
    sendmsg ("ERROR", "Input list not found", ilist, "VRFY")
    logout 0
}
;

# Generate list of patterns and IDs.
task $patcmd = ("$!" // patcmd // " $(1)")
patcmd (ilist, > patlist)
count (patlist) | scan (i)
if (i == 0) {
    sendmsg ("ERROR", "No data found", "", "VRFY")
    delete (pipes)
    delete (patlist)
    logout 3
}
;

# Loop over each pattern and create list of the datasets.
list = patlist
while (fscan (list, s1, s2) != EOF) {
    match (s1, ilist) | count | scan (i)
    if (i > 0)
	print (dataset//"-drv-"//s2, >> olist)
    ;
}
list =""; delete (patlist)

# Check if any data was found.
if (access (olist) == NO) {
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 1
}
;

# Submit to the target pipeline.
callpipe (cpipe, tpipe, "copy", datadir//olist, patlist, temp)
concat (temp) | scan (status)
concat (patlist)
delete (module//"*.tmp")

logout (status)
