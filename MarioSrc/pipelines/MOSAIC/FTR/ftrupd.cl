#!/bin/env pipecl

# FTRUPD -- Update sky flags.

bool	update = yes
bool	show = no

string	dataset, datadir, ilist, lfile

# Tasks and packages.

# Files and directories.
names ("ftr", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ilist = datadir // dataset // ".list"
lfile = datadir // names.lfile
reset (pipedata=names.pipedata)
reset (uparm=names.uparm)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", "FTRUPD", dataset) | tee (lfile)
time | tee (lfile)

if (access("ftrupdrej.tmp")) {
    touch ("ftrupd.tmp,ftrupdobm.tmp")
    list = "ftrupdrej.tmp"
    while (fscan (list, s1, line) != EOF) {
	s2 = substr (s1, 1, strstr("_sky",s1)-1)
        print (line, >> "ftrupd.tmp")
	match (s2//"_00", ilist, >> "ftrupd.tmp")
	match (s2//"_sky", ilist, >> "ftrupd.tmp")
	match (s2//"-ccd[0-9]*.fits", ilist, >> "ftrupd.tmp")
        print (line, >> "ftrupdobm.tmp")
	match (s2//"-ccd[0-9]*_obm.fits", ilist, >> "ftrupdobm.tmp")
    }
    list = ""

    list = "ftrupdobm.tmp"
    while (fscan (list, s1, line) != EOF) {
	if (s1 == "#")
	    printf ("%s %s\n", s1, line, >> "ftrupd.tmp")
	else
	    print (s1//"[pl]", >> "ftrupd.tmp")
    }
    list = ""

    list = "ftrupd.tmp"; s2 = ""
    while (fscan (list, s1, s3) != EOF) {
	if (s1 == "#") {
	    s2 = s3
	    next
	}
	;
	printf ("Update %s = %s\n", s1, s2)
	if (s2 == "Standard") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N standard deviation':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N standard deviation':FRGFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "PGRFLAG",
		"((PGRFLAG=='Y')?'N standard deviation':PGRFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Sky") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N sky mean':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N sky mean':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Magnitude") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N magnitude zeropoint':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N magnitude zeropoint':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "RMS") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N RMS':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N RMS':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Source") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N extended source light':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N extended source light':FRGFLAG)",
		show=show, verify-, update=update)
	}
	;
	flpr
    }
    list = ""
    delete ("ftrupdrej.tmp,ftrupd.tmp,ftrupdobm.tmp")
}
;

logout 1
