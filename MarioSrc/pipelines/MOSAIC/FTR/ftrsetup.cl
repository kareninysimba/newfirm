#!/bin/env pipecl
#
# FTRSETUP -- Do initial setup of dataset.
#
# Currently this just prepares a list of ENIDS.
# This list is used to check previous processing when it ends up in the
# output directory of the DTS pipeline.

string	dataset, indir, datadir, lfile, ilist

# Tasks and packages.
task $psqlog = "$!psqlog"
images

# Files and directories.
names ("ftr", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
ilist = indir // dataset // ".ftr"
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", "ftrsetup", dataset) | tee (lfile)
time | tee (lfile)

# Record in PSQ.
psqlog (names.shortname, "submitted")

# Write the list of ENIDs.
list = ilist
while (fscan (list, s1) != EOF)
    hselect (s1//"[0]", "ENID*", yes) | words (>> dataset//".enids")
list = ""

concat (dataset//".enids")

logout (1)
