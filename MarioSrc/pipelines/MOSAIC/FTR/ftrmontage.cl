# FTRMONTAGE -- Make a montage figure for FTRSKY.

#global	figno

procedure ftrmontage (input, title)

string	input			{prompt="Input images"}
string	title = ""		{prompt="Title"}

file	output			{prompt="Output rootname"}
file	olist			{prompt="Output list to append png names"}
string	temp			{prompt="Tempfile rootname"}

int	ncol = 10		{prompt="Number of columns in montage"}
int	nstep = 45		{prompt="Cell size (pix)"}
int	ltrim = 0		{prompt="Left trim for grid match"}

struct	*fd

begin
	int	n
	string	fign
	file	png, html, grid, offsets, stats, scales, template
	real	val, minval
	struct	str

	figno += 1
	fign = output // figno
	png = output // figno // ".png"
	html = output // ".html"
	grid = temp // "grid.tmp"
	offsets = temp // "offsets.tmp"
	stats = temp // "stats.tmp"
	scales = temp // "scls.tmp"
	template = temp // "template"

	# Set the stats.
	imstatistics ("@"//input, fields="mode", format-,
	    lower=-999, > stats)
	fd = stats; minval = INDEF
	while (fscan (fd, val) != EOF) {
	    val = 1 / max (val, 1e-8)
	    if (isindef(minval))
	        minval = val
	    else
	        minval = min (minval, val)
	    print (val, >> scales)
	}
	fd = ""; delete (stats)

	# Make the gridded image.  Create the grid and template if needed.
	flpr
	if (access(grid)==NO) {
	    printf ("grid %d %d 1000 %d\n", ncol, nstep, nstep) | scan (str)
	    imcombine ("@"//input, fign, headers="", bpmasks="",
		rejmasks="", nrejmasks="", expmasks="", sigmas="",
		logfile=grid, combine="sum", reject="none", project=no,
		outtype="real", outlimits="", offsets=str,
		masktype="none", maskvalue=0, blank=0., scale="@"//scales,
		zero="none", weight="none", statsec="", expname="",
		lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1,
		nkeep=1, mclip=yes, lsigma=3., hsigma=3., rdnoise=0.,
		gain=1., snoise=0., sigscale=0.1, pclip=-0.5, grow=0.)
	    imexpr ("a!=0?0 : a", template, fign, verbose-)
	} else {
	    fd = input
	    while (fscan (fd, str) != EOF) {
		str = substr (str, ltrim+1, 999)
		match (str, grid) | fields ("STDIN", "3,4", >> offsets)
	    }
	    fd = ""
	    count (offsets) | scan (n, n)
	    if (n > 0)
		print ("0 0", >> offsets)
	    else
	        offsets = ""
	    print (minval, >> scales)
	    imcombine ("@"//input//","//template, fign, headers="", bpmasks="",
		rejmasks="", nrejmasks="", expmasks="", sigmas="",
		logfile="", combine="sum", reject="none", project=no,
		outtype="real", outlimits="", offsets=offsets,
		masktype="none", maskvalue="0", blank=0., scale="@"//scales,
		zero="none", weight="none", statsec="", expname="",
		lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1,
		nkeep=1, mclip=yes, lsigma=3., hsigma=3., rdnoise="0.",
		gain="1.", snoise="0.", sigscale=0.1, pclip=-0.5, grow=0.)
	    delete (offsets)
	}
	delete (scales)
	flpr

	# Make the graphic.
	mkgraphic (fign, fign)
	imdelete (fign)

	# Add figure to HTML output.
	printf ('<P>Figure %d: %s\n', figno, title, >> html)
	printf ('<P><IMG SRC="%s">\n', fign//".png", >> html)

	# Add PNG to output list.
	pathname (fign//".png", >> olist)
end
