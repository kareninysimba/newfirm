#!/bin/env pipecl
#
# FTRsciPREP
#
# Description:
#
# 	This module creates lists for the SCI pipeline.
# 
# Exit Status Values:
#
#	1 = Successful
#	2 = No combined stacked SIFs
#
# History:
#
# 	T. Huard  20080424  Created.
# 	Last Revised:  T. Huard  20080428  5:00pm

# Declare variables
int	exitval,icheck
string	dataset,indir,datadir,ilist,lfile
string	infile,filename,bpm

# Load packages
images
proto
utilities

# Set file and path names
names("ftr",envget("OSF_DATASET"))
dataset=names.dataset
indir=names.indir
datadir=names.datadir
ilist=indir//dataset//".ftr"
lfile=datadir//names.lfile
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=names.uparm)
set (pipedata=names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nFTRgrp (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful).
exitval=1

# Input file is the return file from MDC to FTR.  If this file does not exist,
# then exit since there are no combined stacked SIFs.

if (access("ftrsciPREP_tmp.list")) delete("ftrsciPREP_tmp.list",ver-)
;
touch("ftrsciPREP_tmp.list")
infile=datadir//dataset//".mdc"
if (access(infile)==NO) logout(2)
;
list=infile
while(fscan(list,filename) != EOF) {
	concat(filename,>>"ftrsciPREP_tmp.list")
}
list=""

# Sort through infile to obtain a list of the combined stacked-SIF filenames.  
# If there are no stacked SIFs, then the procedure exits.
match("_stk.fits","ftrsciPREP_tmp.list",metacharacters=no,>"ftrsciPREP_stacks.list")
count("ftrsciPREP_stacks.list") | scan(icheck)
if (icheck == 0) logout(2)
;

# Sort through infile to obtain a list of badpixel masks for the combined stacked SIFs.
match("_stk_bpm.fits","ftrsciPREP_tmp.list",metacharacters=no,>"ftrsciPREP_bpms.list")
count("ftrsciPREP_bpms.list") | scan(icheck)
if (icheck == 0) logout(2)
;

# Replace the bpm with full pathname, and generate 2-column list (filename, bpm) for
# input to the SCI pipeline.
if (access("ftrsciPREP.list")) delete("ftrsciPREP.list",ver-)
;
touch("ftrsciPREP.list")
list="ftrsciPREP_stacks.list"
while(fscan(list,filename) != EOF) {
	bpm=""; hselect(filename,"BPM",yes) | scan(bpm)
	if (bpm != "") {
	    match(bpm,"ftrsciPREP_bpms.list") | scan(bpm)
	    printf("%s %s\n",filename,bpm,>>"ftrsciPREP.list")
	}
	;
}
list=""

#delete("ftrsciPREP_tmp.list",ver-)
#delete("ftrsciPREP_stacks.list",ver-)
#delete("ftrsciPREP_bpms.list",ver-)

logout(exitval)
