This document documents both the initial static masks in these
directories as well as the mask values added during processing.

Static Masks (bpm, initial)

    0 - good
    1 - defect or poor CCD pixels (interpolated)
    2 - defect or poor CCD pixels (not interpolated)
    3 - vignetted (special flat field handling, exclude in stacking)

SIFPROC (bpm, added)

+   4 - saturated (interpolated)
+   5 - bleed trail (interpolated)

SIFACE (obm, created)

    0 - good and not in source
    3 - vignetted and excluded from source detection
    4 - saturated but not in a source
    5 - bleed trail but not in a source
    >10 - object mask values which include bad pixel bit for all but vignetting

SRSTR (bpm, added)

+   7 - cosmic rays detected by craverage and not identified by other flag

SRSRSP (bpm created)

Use pmmatch to create version of input mask.  Preserve values in this
mask in the resampled version.  Note there is a bug (still to be diagnosed)
in the pmmatch that sometimes misses things near the edge hence the
merging of the mscimage mask.

    1 - defect or poor CCD pixels (interpolated)
+   1 - added if bad in mscimage version and not in pmmatch version (not interpolated)
    2 - defect or poor CCD pixels
    3 - vignetted
    4 - saturated (interpolated)
    5 - bleed trail (interpolated)
+   6 - out of bounds in resampled
    7 - cosmic ray (craverage)

STRMASK (bpm added)

    7 - cosmic ray (craverage, interpolated for resampled version)
+   8 - difference detections (interpolated for resampled version)

STRRSP (resample bpm updated)

Same as SRSRSP.

==================================================================

ACE:

bpdetect - If input bad pixels are in this list they are excluded
during detection and included in the output object mask.  The output
value is given by bpval if not INDEF otherwise the input value is
used.  However, the output value is limited (by a min function) to
be in the bad pixel range (1-(NUMSTART-1)).

bpflag - If input bad pixels are not in the bpdetect list but are in
this list the are ignored during detection but any source which
includes the pixel has the bad pixel bit set in the object mask.

Any input bad pixels not in either of the above lists is totally
ignored and does not enter the output object mask.  Note that if
a value in the bpflag list is desired in the output object mask then
it must be added after creation of the object mask by merging with
the input bad pixel mask.
