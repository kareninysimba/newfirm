  ¦V  ū  ž$TITLE = "Domeflat"
$CTIME = 830428773
$MTIME = 830428773
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
PCOUNT  =                 7756 / Heap size in bytes
GCOUNT  =                    1 / Only one group
EXTNAME = 'pl      '           / Extension name
ORIGIN  = 'NOAO-IRAF FITS Image Kernel December 2001' / FITS file originator
INHERIT =                    F / Inherits global header
DATE    = '2003-06-11T18:32:20'
IRAF-TLM= '14:40:01 (20/06/2003)'
OBJECT  = 'Domeflat'           / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat3018'          / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '18:01:17.00'        / RA of observation (hr)
DEC     = '20:41:4.10'         / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2000-09-30T22:04:39.3' / Date of observation start (UTC approximate)
TIME-OBS= '22:04:39.3'         / Time of observation start
MJDHDR  =       51817.91991435 / MJD of header creation
LSTHDR  = '18:01:16.9        ' / LST of header creation

OBSERVAT= 'CTIO    '           / Observatory
TELESCOP= 'CTIO 4.0 meter telescope'
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2000.0 / Equinox of tel coords
TELRA   = '18:01:17.00       ' / RA of telescope (hr)
TELDEC  = '20:41:04.1        ' / DEC of telescope (deg)
HA      = '-00:00:01.9       ' / hour angle (H:M:S)
ZD      = '50.9              ' / Zenith distance
AIRMASS =                1.582 / Airmass
TELFOCUS= '14665             ' / Telescope focus
CORRCTOR= 'Blanco Corrector'   / Corrector Identification
ADC     = 'Blanco ADC'         / ADC Identification

DETECTOR= 'Mosaic2 '           / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.270 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.270 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    3 / Filter position
FILTER  = 'V CTIO            ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -3.2000000000000E+00 / North TV Camera focus position
TV2FOC  = -2.0790000000000E+00 / South Camera focus position
ENVTEM  =  1.5000000000000E+01 / Ambient temperature (C)
DEWAR   = 'Mosaic2 Dewar'      / Dewar identification
DEWTEM  = -1.6410000600000E+02 / Dewar temperature (C)
DEWTEM2 = -5.5200001000000E+01 / Fill Neck temperature (C)
DEWTEM3 = 'N2  89.1'
CCDTEM  = -1.0080000300000E+02 / CCD temperature  (C)
CCDTEM2 = 'CCD 161.3'

WEATDATE= 'Sep 30 22:01:02 2000' / Date and time of last update
WINDSPD = '2.4     '           / Wind speed (mph)
WINDDIR = '44      '           / Wind direction (degrees)
AMBTEMP = '19.5    '           / Ambient temperature (degrees C)
HUMIDITY= '16      '           / Ambient relative humidity (percent)
PRESSURE= '784     '           / Barometric pressure (millibars)

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '13July99ver7_30'    / Controller software version
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Sat Sep 30 17:16:42 2000' / Date waveforms last compiled
ARCONWM = 'OverlapXmit EarlyReset SplitShift ' / Waveform mode switches on
ARCONGI =                    2 / Gain selection (index into Gain Table)

OBSERVER= 'A. Clocchiatti, C. Smith' / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'ct4m.20000930T220439' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaic.tcl (Aug00)' / Image creation software version




KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/ctio/4meter/caldir/Mosaic2A/' / OTF calibration directory
XTALKFIL= 'mscdb$noao/Mosaic2/CAL0009/xtalkA0009' / Crosstalk file

CHECKSUM= '<unknown>'          / Header checksum
DATASUM = '<unknown>'          / Data checksum
CHECKVER= '<unknown>'          / Checksum version
RECNO   =                    0 / NOAO archive sequence number

EXPTIME =             18.12155 / Exposure time (sec)
DARKTIME=             26.94336 / Dark time (sec)
IMAGEID =                    1 / Image identification

CCDNAME = 'SITe #98173FABR14-02 (NOAO 26)' / CCD name
AMPNAME = 'SITe #98173FABR14-02 (NOAO 26), lower left (Amp11)' / Amplifier name
GAIN    =                  2.6 / gain expected for amp 111 (e-/ADU)
RDNOISE =                  6.1 / read noise expected for amp 111 (e-)
SATURATE=                48000 / Maximum good data value (ADU)
CONHWV  = 'ACEB001_AMP11'      / Controller hardware version
ARCONG  =                  2.6 / gain expected for amp 111 (e-/ADU)
ARCONRN =                  6.1 / read noise expected for amp 111 (e-)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,1:4096]'    / Amplifier section
DETSEC  = '[1:2048,1:4096]'    / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                   1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =                   0. / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                   0. / CCD to detector transformation
DTV2    =                   0. / CCD to detector transformation

WCSASTRM= 'ct4m.19990714T012701 (USNO-K V) by F. Valdes 1999-08-02' / WCS Source




EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =            270.32083 / Coordinate reference value
CRVAL2  =            20.684472 / Coordinate reference value
CRPIX1  =            4244.3258 / Coordinate reference pixel
CRPIX2  =            4304.2481 / Coordinate reference pixel
CD1_1   =       -6.8295807e-08 / Coordinate matrix
CD2_1   =        7.3313414e-05 / Coordinate matrix
CD1_2   =         7.374228e-05 / Coordinate matrix
CD2_2   =       -1.1927219e-06 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "3. 4. 4. 2. -0.3171856965643079 -0.015'




WAT1_002= '0652479325533 -0.3126038394350166 -0.1511955040928311 0.002318100364'




WAT1_003= '838772 0.01749134520424022 -0.01082784423020123 -0.1387962673564234 '




WAT1_004= '-4.307309762939804E-4 0.009069288008295441 0.002875265278754504 -0.0'




WAT1_005= '4487658756007625 -0.1058043162287004 -0.0686214765375767 "'
WAT2_001= 'wtype=tnx axtype=dec latcor = "3. 4. 4. 2. -0.3171856965643079 -0.01'




WAT2_002= '50652479325533 -0.3126038394350166 -0.1511955040928311 0.00553481957'




WAT2_003= '8784082 0.01258790793029932 0.01016780085575339 0.01541083298696018 '




WAT2_004= '0.03531979587941362 0.0150096457430599 -0.1086479352595234 0.0399806'




WAT2_005= '086902122 0.02341002785565408 -0.07773808393244387 "'
XTALKCOR= '03-Oct-2000 20:12:50 Crosstalk is im2 * 0.00119'
TRIM    = 'Oct  3 20:13 Trim is [25:2072,1:4096]'
OVERSCAN= 'Oct  3 20:13 Overscan is [2087:2136,1:4096], mean 1167.167'
CCDPROC = 'Oct  4  2:07 CCD processing done'
ZEROCOR = 'Oct  4  2:07 Zero is Zero[im1]'
NCOMBINE=                   21
ORIGBPM1= '0x010100.bpm.sm021202_1.pl'
ORIGBPM2= '0x020100.bpm.sm000000_2.pl'
    Ž                          ¾  ¾       ž      ’’’               ’      ca 	@`	 
@
 	@ 	@ 	@`	 
@ 	@ 	@ 	@ 	@ 	@ 	@ 	@ 	@ 	@ 	@ 	@ 	@ 	@ 	@ 	@`	 	@ 	@`	 	@`	 	@`	 	@`	 	@`	`	`	 	@`	`	`	`	`	`	 	@`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	`	 	@  	@`
`	`
` 
Bø 	@ @v&Ø@F 	@ 
@ 	@ @	vÄ 
@&Ć@6ŗ@`	&»@ 	@ 
@]vÄ``&·@v­ @`	`
`	`
&@vz`
``&c@(vX&a@ 	@ @vk&t@0vk&t@vj``	`
`	``&6@v+ @,&)@v &)@v&(@ 	@ @v2 
@`	 @`	&@v
 @&@ @" 	@ 
@v&&@
v&$@ 	@ 
@-v-&6@3`	`
```` @ @ @` @ @ @`````` 
@
w
'@w	'@w'@w'@w``&ł@vķ&ų@$vķ&ö@vģ @`&ß@vÕ`
`	&Ė@vĮ`&æ@v³&¼@v²&½@v²&»@ 	@ @vÅ`
`````&@v``&v@vk``&^@vS 
@ 	@&I@+v>``&1@)`	`
` 
@ @ 	@ 
@ 	@`	 
@ @`	`` @`	` 
@o`	 
@`	` @	` @ @T @ @`` @	 @ @ @ @```` @,` @@`` @`` @h @ @` @``` @` @ @``` @Q` @“``` @` @`` @` @`` @``` @}``` @O @ @` @"``` @L @zß*ī@` @ī``` @ @``` @0` @ @ @?` @(` @` @U @ @Š   ’         ’      @P[P?L  ’ 
   
  @Q6°  ’ 	   	  @ę  ’ 	   	  @ę  ’ 
   
  @Qd  ’      @Ģ@   ’      @Ė@   ’ 
   
  @WĶ   ’      @·@ -  ’ 	   	  @ę  ’ 
   
  @R9­  ’ 	   	  @ę  ’ 
   
  @RĆ#  ’      @Į@#  ’ 
   
  @W T  ’      @ #@Ą  ’      @ #@æ  ’      @ #@Ą  ’ 
   
  @Rõń  ’ 	   	  @å  ’ 
   
  @Se  ’      @@^  ’ 	   	  @ę  ’ 
   
  @Swo  ’ 	   	  @ę  ’      @@F  ’      @@E  ’      @@F  ’      @Ŗ@: , ’ 	   	  @ę  ’ 
   
  @SÄT"  ’ 
   
  @UćR  ’ 
   
  @SšSö  ’ 	   	  @Wę  ’      @ż@Sē  ’ 	   	  @Wę  ’ 
   
  @URH  ’      @>@S¦  ’ 	   	  @Wę  ’      @@SE  ’ 
   
  @RUR  ’ 
   
  @TäS  ’ 
   
  @UDR¢  ’      @&@Q½  ’      @&@Q¾  ’ 
   
  @UjR|  ’      @k@Qy  ’      @j@Qx  ’      @QÖ@Qy  ’      @Ō@V  ’ 
   
  @VQg  ’      @@PL  ’      @@PK  ’ 
   
  @WPM  ’ 
   
  @W¤PB  ’ 	   	  @Wę  ’ 
   
  @V“Q2  ’      @ę@Pž  ’      @Vē X@P„  ’ 
   
  @PSW  ’      @ Q@W  ’ 
   
  @PWU  ’ 
   
  @WGP  ’      @U]QźP  ’      @[@R  ’      @[@R  ’      @[@R  ’      @\@R  ’ 
   
  @U^R  ’      @+@U¹  ’      @*@Uø  ’      @+@U¹  ’      @v@Pn  ’      @v@Pm  ’      @v@Pn  ’ 
   
  @W¦P@  ’ 	   	  @Wę  ’      @Ļ@P  ’      @ę@Tż  ’      @å@Tż  ’      @ę@Tż  ’      @ Ü@~@ @!@Sa  ’ 	   	  @  ’ 	   	  @~  ’ 
   
  @~V ū 
 ’ 	   	  @~  ’ 	   	  @}  ’ 	   	  @|  ’ 	   	  @{  ’ 
   
  @{PAD  ’ 	   	  @{  ’ 	   	  @z  ’ 	   	  @y  ’ 	   	  @x  ’ 	   	  @w  ’ 	   	  @v  ’ 	   	  @u  ’ 	   	  @t  ’ 	   	  @s  ’ 	   	  @r  ’ 	   	  @q  ’ 	   	  @p  ’ 	   	  @o  ’ 	   	  @n  ’ 	   	  @m  ’ 	   	  @l  ’ 	   	  @k  ’ 	   	  @j  ’ 	   	  @i  ’ 	   	  @h  ’ 	   	  @g  ’ 	   	  @f  ’ 	   	  @e  ’ 	   	  @d  ’ 	   	  @c  ’ 	   	  @b  ’ 	   	  @a  ’ 	   	  @`   ’ 	   	  @_”  ’ 	   	  @^¢  ’ 	   	  @]£  ’ 	   	  @\¤  ’ 	   	  @[„  ’ 	   	  @Z¦  ’ 	   	  @Y§  ’ 	   	  @XØ  ’ 	   	  @W©  ’ 	   	  @VŖ  ’ 	   	  @U«  ’ 	   	  @T¬  ’ 	   	  @S­  ’ 	   	  @R®  ’ 	   	  @QÆ  ’ 	   	  @P°  ’ 	   	  @N²  ’ 	   	  @M³  ’ 	   	  @L“  ’ 	   	  @Kµ  ’ 	   	  @J¶  ’ 	   	  @I·  ’ 	   	  @Hø  ’ 	   	  @Fŗ  ’ 	   	  @E»  ’ 	   	  @D¼  ’ 	   	  @C½  ’ 	   	  @Aæ  ’ 	   	  @@Ą  ’ 	   	  @>Ā  ’ 	   	  @=Ć  ’ 	   	  @;Å  ’ 	   	  @:Ę  ’ 	   	  @8Č  ’ 	   	  @7É  ’ 	   	  @5Ė  ’ 	   	  @3Ķ  ’ 	   	  @2Ī  ’ 	   	  @0Š  ’ 	   	  @.Ņ  ’ 	   	  @,Ō  ’ 	   	  @*Ö  ’ 	   	  @(Ų  ’ 	   	  @&Ś  ’ 	   	  @#Ż  ’ 	   	  @!ß  ’ 	   	  @ā  ’ 	   	  @ä   ’ 	   	  @ę  ’ 
   
  @Sµ1  ’ 	   	  @ę  ’ 
   
  @Sµ1  ’      @PĮRō1  ’ 
   
  @Sµ1ø ’ 	   	  @ę  ’      @Ā@"  ’ 	   	  @ę F ’ 	   	  @ę  ’ 
   
  @Pīų  ’ 	   	  @ę  ’      @@ ā 	 ’ 	   	  @ę  ’ 
   
  @QR  ’ 	   	  @ę  ’ 
   
  @QŌ ] ’ 	   	  @ę  ’ 	   	  @ę  ’ 	   	  @ę ( ’ 	   	  @ę  ’ 	   	  @ę  ’      @5@Æ  ’ 	   	  @ę 0 ’ 	   	  @ę  ’ 	   	  @ę  ’ 	   	  @ę  ’ 	   	  @Wę  ’ 	   	  @Wę  ’ 	   	  @Wę  ’      @@SČ  ’ 	   	  @Wę  ’ 	   	  @Wę  ’      @U@S " ’ 	   	  @Wę  ’ 
   
  @TuSq  ’ 	   	  @Wę 
 ’ 	   	  @Wę  ’ 	   	  @Wę  ’ 
   
  @TĀS$ - ’ 	   	  @Wę 3 ’ 	   	  @Wę  ’ 
   
  @STŪ  ’      @@TÖ  ’      @@TŌ  ’      @@TŅ  ’      @ @TŃ  ’      @’@TŠ  ’      @ž@TĻ  ’      @ż@TĪ  ’      @ü@TĶ  ’      @ū@TĢ  ’      @ü@TĶ  ’      @ż@TĪ  ’      @ž@TĻ  ’      @;@ Ā@TŠ  ’      @;@ Ā@TŃ  ’      @;@ Ä@TŅ  ’      @@TŌ  ’      @@TÖ  ’ 
   
  @STŪ 
 ’ 	   	  @Wę  ’ 	   	  @Wę  ’      @%@Q½  ’ 	   	  @Wę  ’ 	   	  @Wę  ’      @Ō@V $ ’ 	   	  @Wę  ’ 	   	  @Wę  ’ 	   	  @Wę  ’ 	   	  @Wę  ’ 	   	  @Wę  ’      @ Q@W  ’ 	   	  @Wę  ’ 	   	  @Wę  ’      @@PE  ’ 	   	  @Wę  ’ 	   	  @Wę  ’ 	   	  @Wę  ’ 	   	  @Wę + ’ 	   	  @Wę ) ’ 	   	  @Wę  ’ 
   
  @P,Wŗ  ’      @ *@Wŗ  ’ 
   
  @P,Wŗ  ’      @ś@Qź  ’ 	   	  @Wę  ’ 
   
  @PęW   ’ 	   	  @Wę  ’ 	   	  @Wš  ’ 
   
  @R®UB  ’      @¬@UA  ’ 	   	  @Wš  ’      @ a@W  ’      @ a@W  ’      @ a@W  ’ 	   	  @Wš  ’      @ B@W¬  ’ 
   
  @PCW­ o ’ 	   	  @Wš  ’ 
   
  @TXS  ’ 	   	  @Wš  ’      @Qb ž@ @U  ’      @Qb ž@U 	 ’      @`@U  ’      @QÜ @U  ’      @Ū@ @U T ’      @`@U  ’      @QĖ @U  ’      @`@U  ’      @@Y@U  ’      @QZ@U 	 ’      @`@U  ’      @Q Ś@U  ’      @`@U  ’      @9@%@U  ’      @`@U  ’      @ @Ź@U  ’      @ @É@U  ’      @ @Č@U  ’      @PŹ@U , ’      @`@U  ’      @QŹ @U @ ’      @`@U  ’      @@]@U  ’      @Q]@U  ’      @`@U  ’      @`@R$Se  ’      @`@!@ @Sa h ’      @`@!@Sa  ’      @ A@@!@Sa  ’      @`@!@Sa  ’      @`@!@QQŚ  ’      @`@!@Sa  ’      @RU @!@Sa  ’      @S@ @!@Sa  ’      @RU @!@Sa  ’      @`@!@Sa  ’      @@ Ī@!@Sa  ’      @@ Ī@!@Sa  ’      @`@!@Sa  ’      @`@Q@Sa  ’      @`@@@Sa  ’      @`@@@Sa Q ’      @`@!@Sa  ’      @`@PŲI@Sa “ ’      @`@!@Sa  ’      @`@!@ ź@Rs  ’      @`@!@ ź@Rr  ’      @`@!@ ė@Rs  ’      @`@!@Sa  ’      @`@!@@PD  ’      @`@!@Sa  ’      @`@ »@d@Sa  ’      @`@P½d@Sa  ’      @`@!@Sa  ’      @`@ @!@Sa  ’      @`@!@Sa  ’      @ č@v@!@Sa  ’      @Péw@!@Sa  ’      @`@!@Sa  ’      @`@!@PųRi  ’      @`@!@ ö@Rh  ’      @`@!@PųRi } ’      @`@!@Sa  ’      @`@ @@Sa  ’      @`@ @@Sa  ’      @`@ @@Sa O ’      @`@!@Sa  ’      @Prī@!@Sa  ’      @`@!@Sa  ’      @Pi÷@!@Sa " ’      @`@!@Sa  ’      @`@Qe ¼@Sa  ’      @`@c@ ¼@Sa  ’      @`@Qe ¼@Sa L ’      @`@!@Sa  ’      @ Ü@~@!@Sa  ’      @ Ü@~@!@Sa  ’      @ Ü@~@!@R“P­ ī ’      @ Ü@~@!@Sa  ’      @ Ü@~@!@B@P  ’      @ Ü@~@!@A@	P  ’      @ Ü@~@!@@@
P  ’      @ Ü@~@!@?@P  ’      @ Ü@~@!@@@P  ’      @ Ü@~@!@@@
P  ’      @ Ü@~@!@A@	P  ’      @ Ü@~@!@B@P 0 ’      @ Ü@~@!@Sa  ’      @P0 ¬@~@!@Sa  ’      @ Ü@~@!@Sa  ’      @ Ü@~@!@PIS ? ’      @ Ü@~@!@Sa  ’      @PI @~@!@Sa ( ’      @ Ü@~@!@Sa  ’      @ Ü@~@!@QŗQ§  ’      @ Ü@~@!@Sa  ’      @ Ü@~@Pš1@Sa U ’      @ Ü@~@!@Sa  ’      @ Ü@~@PEÜ@Sa Š ’      @ Ü@~@!@Sa