  ¦V  ,  2$TITLE = "DFLATS Observation(s)"
$CTIME = 973252159
$MTIME = 973256851
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
RAWFILE = 'n501453.fits'       / Original raw file
FILENAME= 'K4M10B_20101027-IFlat-kp4m20101027T235153F' / Current filename
NEXTEND =                    8 / Number of extensions
OBSTYPE = 'dome flat'          / Observation type
PROCTYPE= 'MasterCal'          / Processing type
PRODTYPE= 'image   '           / Product type
MIMETYPE= 'application/fits'   / Mimetype of this data file
EXPTIME =                   55 / Exposure time (sec)
OBJRA   = '18:50:24.22'        / Right Ascension
OBJDEC  = '-17:00:00.0'        / Declination
OBJEPOCH=               2010.8 / [yr] epoch

          # Single exposure quantities are representative from 1st exposure
TIMESYS = 'UTC     '           / Time system
DATE-OBS= '2010-10-27T23:51:53.0' / Date and time of exposure start
TIME-OBS= '23:51:53'           / Universal time
MJD-OBS =       55496.99436343 / MJD of observation start
ST      = '18:50:24'           / Sidereal time

OBSERVAT= 'KPNO    '           / Observatory
OBS-ELEV=                2120. / [km] Observatory elevation
OBS-LAT = '31:57.8 '           / [deg] Observatory latitude
OBS-LONG= '111:36.0'           / [deg] Observatory longitude
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2010.8 / Equinox of tel coords
TELRA   = '18:50:24.22'        / RA of telescope (hr)
TELDEC  = '-17:00:00.0'        / DEC of telescope (deg)
TELFOCUS=                -8873 / Telescope focus
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off     '           / ADC Mode
ADCPAN1 =                 0.01 / [deg] MSE ADC 1 prism angle
ADCPAN2 =                 0.05 / [deg] MSE ADC 2 prism angle
CORRCTOR= 'Mayall Corrector'   / Corrector Identification

INSTRUME= 'mosaic_1_1'         / Mosaic detector
NDETS   =                    8 / Number of detectors in mosaic
FILTER  = 'I Nearly-Mould k1005' / Filter name
FILTID  = 'k1005   '           / Unique filter identification
PHOTBW  =              191.459 / [nm] Filter wavelength FWHM
PHOTFWHM=              191.459 / [nm] Filter wavelength FWHM
PHOTCLAM=              820.453 / [nm] Filter wavelength width
FILTPOS =                    5 / Instrument filter position
FILTERSN= 'k1005   '           / Filter serial number
NOCGAIN = 'normal  '           / NOCS gain setting
SHUTSTAT= 'dark    '           / Shutter status
ENVTEM  =                   13 / [celsius] MSE temp7 - Ambient
DEWAR   = 'Mosaic1.1 Dewar'    / Dewar identification
DEWTEM  =               -178.5 / [celsius] MSE temp1 - Dewar tank
DEWTEM2 =                 -9.7 / [celsius] MSE temp3 - Fill Neck
CCDTEM  =          -106.199997 / [celsius] MSE temp2 - CCD Focal Plate

OBSERVER= 'Howell, Schweiker, Mathis, Reedy' / Observer(s)
PROPOSER= 'David Sawyer'       / Proposer(s)
PROPID  = '2010B-2005'         / Proposal identification
OBSID   = 'kp4m.20101027T235153' / Observation ID
EXPID   =                    0 / Monsoon exposure ID
NOCID   =        2455497.70176 / NOCS exposure ID

CONTROLR= 'Mosaic System Electronics  Sep 2' / MSE name and revision date
DHEFILE = 'mosaic1_Seq2amp250Kpx.ucd' / Sequencer file

NOHS    = '2.0.0   '           / NOHS ID
NOCMDOF =                    0 / [arcsec] Map Dec offset
NOCMITER=                    0 / Map iteration count
NOCNO   =                   31 / observation number in this sequence
NOCPOST = 'dfs     '           / ntcs_moveto ra dec epoch
NOCDROF =                    0 / [arcsec] Dither RA offset
NOCDHS  = 'DFLATS  '           / DHS script name
NOCORA  =                    0 / [arcsec] RA offset
NOCODEC =                    0 / [arcsec] Dec offset
NOCMPOS =                    0 / Map position
NOCDITER=                    0 / Dither iteration count
NOCMPAT = '4Q      '           / Map pattern
NOCMREP =                    0 / Map repetition count
NOCDDOF =                    0 / [arcsec] Dither Dec offset
NOCTOT  =                   50 / Total number of observations in set
NOCSCR  = 'DFLATS  '           / NOHS script run
NOCFOCUS=                    0 / [um] nics_focus value
NOHS    = 'Mosaic.1.1'         / NOHS ID
NOCTIM  =                   55 / [s] Requested integration time
NOCOFFT = '0 0     '           / ntcs_offset RA Dec offset (arcsec)
NOCSYS  = 'kpno 4m '           / system ID
NOCNUM  =                   10 / observation number request
NOCLAMP = 'On      '           / Dome flat lamp status (on|off|unknown)
NOCRBIN =                    1 / CCD row binning
NOCMROF =                    0 / [arcsec] Map RA offset
NOCSKY  =                    0 / sky offset modulus
NOCNPOS =                    1 / observation number in requested number
NOCCBIN =                    1 / CCD column binning
NOCTYP  = 'DFLATS  '           / Observation type
NOCDPOS =                    0 / Dither position
NOCDPAT = '5PX     '           / Dither pattern
NOCDREP =                    0 / Dither repetition count

RAINDEX =                    0 / [arcsec] RA index
RAZERO  =                570.5 / [arcsec] RA zero
ALT     = '41:02:15.0'         / Telescope altitude
DECINST =                    0 / [arcsec] Dec instrument center
DECDIFF =                    0 / [arcsec] Dec diff
FOCUS   =                -8873 / [mm] Telescope focus
PARALL  =                  360 / [deg] parallactic angle
DECZERO =              -375.12 / [arcsec] Dec zero
AZ      = '180:00:00.0'        / Telescope azimuth
RADIFF  =                    0 / [arcsec] RA diff
RAINST  =                    0 / [arcsec] RA instrument center
DECOFF  =                    0 / [arcsec] Dec offset
DECINDEX=                    0 / [arcsec] Dec index
RAOFF   =                    0 / [arcsec] RA offset

DOMEERR =               135.85 / [deg] Dome error as distance from target
DOMEAZ  =                    0 / [deg] Dome position

MSEREADY= 'dark    '           / MSE shutter ready (none|guide|dark|restore)

TCPGDR  = 'off     '           / Guider status (on|off|lock)

DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1_1        '  /  instrument identifier
DTCALDAT= '2010-10-27        '  /  calendar date from observing schedule
ODATEOBS= '                  '  /  previous DATE-OBS
DTUTC   = '2010-10-27T23:57:30'  /  post exposure UTC epoch from DTS
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2010B-2005        '  /  observing proposal ID
DTPI    = 'David Sawyer      '  /  Principal Investigator
DTPIAFFL= 'National Optical Astronomy Observatory'  /  PI affiliation
DTTITLE = 'On-sky Commissioning of MOSA 1.1'  /  title of observing proposal
DTCOPYRI= 'AURA              '  /  copyright holder of data
DTACQUIS= 'mosaic1dhs-01-4m.kpno.noao.edu'  /  host name of data acquisition com
DTACCOUN= 'cache             '  /  observing account name
DTACQNAM= '/home/data/n501453.fits'  /  file name supplied at telescope
DTNSANAM= 'kp1247271.fits    '  /  file name in NOAO Science Archive
DTSTATUS= 'done              '  /  data transport status
SB_HOST = 'mosaic1dhs-01-4m.kpno.noao.edu'  /  iSTB client host
SB_ACCOU= 'cache             '  /  iSTB client user account
SB_SITE = 'kp                '  /  iSTB host site
SB_LOCAL= 'kp                '  /  locale of iSTB daemon
SB_DIR1 = '20101027          '  /  level 1 directory in NSA DS
SB_DIR2 = 'kp4m              '  /  level 2 directory in NSA DS
SB_DIR3 = '2010B-2005        '  /  level 3 directory in NSA DS
SB_RECNO=              1247271  /  iSTB sequence number
SB_ID   = 'kp1247271         '  /  unique iSTB identifier
SB_NAME = 'kp1247271.fits    '  /  name assigned by iSTB
RMCOUNT =                    0  /  remediation counter
RECNO   =              1247271  /  NOAO Science Archive sequence number
XTALKFIL= 'Mosaic11_xtalkdummy.txt' /

GAIN    =                  1.1 / Approx inv gain (ADU/e)
RDNOISE =                  5.0 / Approx readout noise (e)
SATURATE=               220000 / Approx saturation (ADU)

DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.001509 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         3.103876E-42 / depth of longest jump down
CCDMEAN =            149304.45
OVSCNMTD=                    3
DQGLFSAT=                   0.
IMCMB001= 'kp1247271.fits'
IMCMB002= 'kp1247272.fits'
IMCMB003= 'kp1247274.fits'
IMCMB004= 'kp1247275.fits'
IMCMB005= 'kp1247277.fits'
IMCMB006= 'kp1247278.fits'
IMCMB007= 'kp1247281.fits'
IMCMB008= 'kp1247285.fits'
IMCMB009= 'kp1247286.fits'
IMCMB010= 'kp1247287.fits'
NCOMBINE=                   10
QUALITY =                   0.
GAINMEAN=                  1.1

PIPELINE= 'NOAO Mosaic Pipeline' / Name of calibration pipeline
PLVER   = 'MOSAIC V1.1'        / Pipeline version
EFFTIME =                   55 / [s] Effective exposure time
PLPROPID= 'CAL     '

PLQUEUE = 'K4M10B  '           / PL Queue
PLQNAME = '20101027'           / PL Dataset
PLPROCID= '7ae6e75 '           / PL Processing ID
PLFNAME = 'IFlat-kp4m20101027T235153F' / PL Filename
PLOFNAME= 'n501453_pl'         / Original file name
PCOUNT  =                    0 / No 'random' parameters
GCOUNT  =                    1 / Only one group
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
EXTNAME = 'ccd7    '           / Extension name
INHERIT =                    T / Inherits global header
DATE    = '2010-11-02T18:19:26' / Date FITS file was generated
IRAF-TLM= '2010-11-03T17:47:13' / Time of last modification
IMAGEID =                    7 / Image identification

CCDNAME = 'SN13-01 '           / CCD name
AMPNAME = 'SN13-01:C'          / Amplifier name
BUNIT   = 'adu     '           / [adu] ADU counts
CCDSUM  = '1 1     '           / CCD pixel summing
DETSEC  = '[4097:6144,4097:8192]' / Detector section


XTALKCOR= 'Nov  2 11:03 No crosstalk correction required'
BIASFIL = 'K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z[ccd7]' / Bias reference
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =                   0. / min in overscan region
DQOVMAX =             313.1429 / max in overscan region
DQOVMEAN=             308.1466 / mean in overscan region
DQOVSIG =             19.51982 / sigma in overscan region
DQOVSMEA=             308.2081 / mean in collapsed overscan strip
DQOVSSIG=             1.317215 / sigma in collapsed overscan strip
SATPROC = 'Nov  2 11:15 Sat: 220000. ADU (242000. e-), grw=0'
TRIM    = 'Nov  2 11:15 Trim is [1:1024,1:4096]'
FIXPIX  = 'Nov  2 11:15 Fix Mosaic11_dummy_7_bpm.pl + sat' /
OVERSCAN= 'Nov  2 11:15 Overscan is [1025:1074,1:4096], function=minmax'
ZEROCOR = 'Nov  2 11:15 Zero is K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z' /
CCDPROC = 'Nov  2 11:15 CCD processing done'
AMPMERGE= 'Nov  2 11:16 Merged 2 amps'
DQGLMEAN=             148024.5
DQGLSIG =             3273.519
DQDFCRAT=             2691.354
PUPILCOR= 'Nov  2 11:17 maximum amplitude = 1.284E-6'
DCCDMEAN=            0.9815252
CCDMNTMP=             149304.5
   Ž                          	J  	J     Ą  2      ’’’               ’Ą      gé` @```` 
@`	``` 	@`	`	 	@`	`	`	 	@`	 	@ 	@`	`	 	@`	`	 	@ 	@`	 	@`	`	`	 	@ 	@3ōP1¾P ÕP-47PR!¶P µP`	#É @vb ·P`
%Ŗ v@3P#NP1ZP0cPP`4BP0%P$qPW`
`N`
`
 «P`
4;P`
 	P% %@ 	 @u²`
bq’`	`	`	`	`
!ÜP* ĆP0PA"ö @4¢P ^P"®P`
3P`
 UP$C@t9$C@6bP!ÉP*`
`#’ +@1PC1OPd"÷ 
@5/P#ŅP48P% %@1P! -@ 	 @3P
# @5IP"AP`
3P$NP1nP1P#}P4P&%.P!* 7@4xP$ C@ 	 @2RP' æP0¶P&`	"I@r?1PY#Ō @ 	@ 	@5wP$P	`	`
!O @5śPd& @5śP	"²P1,P2eP5`
&Ų I@r"@qŌ`pL 	P!1EP3ZP+#P2hP pP#1P"!P$P5ĻP.&į #@3QP`	0ĀP:$@3IPF 
PI0ĢP"P`
3ÉP!1P 	P`
"P4¬P	!}P$@3®P#· @qi!s /@4P$
 "@17P`	0ÉP5`` BP08P$3ņP%X @6ćP$ģP2P0ŖP$½@sm#y@sn`	`
1>P2UP%"^P#`	`
$@t~2±P"P0DP"P`	 }P ÉP4DPJ#P;#T@sK2 P6`
`
`
1¶P8e` ÜP	3„P`
`
%! @4ŻP$ę @wC 7P0.P'"žP1EPQ`	0ķP&o@ve&o@4GP?2ŠP%£Pa} 
@q~¹ ’         ’ 	   	  P9Ē  ’ 	   	  Pć  ’ 	   	  P4Ģ  ’ 
   
   @ó  ’ 	   	  Pq  ’ 	   	  PĮ?  ’ 	   	  PøH  ’ 	   	  Pd  ’ 
   
   @h  ’ 	   	  Pi  ’ 	   	  PŗF  ’ 	   	  Pé  ’ 	   	  Pń  ’ 	   	  Pc  ’ 	   	  QKµ  ’ 	   	  QVŖ  ’ 
   
  @ä  ’ 	   	  Pś  ’ 	   	  Q]£  ’ 	   	  Qe  ’ 
   
  Ž@   ’ 
   
  Ž@  ’ 
   
  @h  ’ 	   	  Qi  ’ 	   	  Ql  ’ 	   	  Qm  ’ 	   	  Qn  ’ 	   	  Qo  ’ 
   
  Q)Pgp  ’ 
   
  @q  ’ 	   	  QÅ;  ’ 	   	  QŹ6  ’ 	   	  Qs  ’ 	   	  Q¦Z  ’ 	   	  QĻ1  ’ 	   	  QøH  ’ 
   
  @|  ’ 
   
  @}  ’ 
   
  RNP°  ’ 	   	  RO±  ’ 	   	  R*Ö  ’ 
   
  /@Ļ  ’ 	   	  R[„  ’ 	   	  Rź  ’ 	   	  Rļ  ’ 	   	  R-Ó  ’ 	   	  R>Ā  ’ 
   
  ;@Ą  ’ 
   
  9@Ą  ’ 
   
  :@Ć  ’ 
   
  :@Ä  ’ 	   	  R”_  ’ 	   	  Rd  ’ 
   
  j@  ’ 	   	  Rk  ’ 
   
  @w  ’ 
   
  @w  ’ 	   	  Rm  ’ 	   	  R{  ’ 
   
  ²@L  ’ 	   	  RæA  ’ 	   	  RĄ@  ’ 
   
  Ū@#  ’ 
   
  ÷@  ’ 
   
  ÷@  ’ 
   
  4@Ź  ’ 	   	  S-Ó  ’ 
   
  Ū@#  ’ 
   
  Ū@"  ’ 	   	  RŽ"  ’ 	   	  S{  ’ 
   
  @{  ’      @Pā  ’ 	   	  Sd  ’ 
   
  x@  ’ 
   
  Y@„  ’ 	   	  Sy  ’ 	   	  Sź  ’      @R73  ’ 	   	  Tó  ’ 	   	  T$Ü  ’ 	   	  SŪ%  ’ 
   
  RŠQ%  ’ 	   	  SŪ%  ’ 
   
  Ś@$  ’ 	   	  Th  ’ 
   
  ShQJN  ’ 	   	  To  ’ 	   	  TC½  ’ 	   	  Th  ’ 
   
  H@¶  ’ 
   
  G@·  ’ 	   	  TT¬  ’ 
   
  “@J  ’ 	   	  Tk  ’ 	   	  Te  ’ 	   	  U’  ’ 	   	  Tń  ’ 	   	  Té  ’ 	   	  U-Ó  ’ 	   	  Ut  ’ 
   
  `@  ’ 	   	  Ub  ’ 
   
  @m  ’ 	   	  Un  ’ 	   	  Ue  ’ 	   	  Uv  ’ 	   	  U5Ė  ’ 	   	  U6Ź  ’ 
   
  ¤@Z  ’ 	   	  Uq  ’      Y@Qé¼  ’ 	   	  UD¼  ’ 
   
  C@»  ’ 	   	  UE»  ’ 	   	  U>Ā  ’ 	   	  Ur  ’ 	   	  Ug  ’ 	   	  Ug  ’ 	   	  UÄ<  ’      ō@P  ’ 
   
  õ@  ’ 
   
  É@4  ’ 
   
  É@3  ’ 
   
  ½@A  ’ 	   	  Võ  ’ 	   	  Uģ  ’ 
   
  ź@  ’ 
   
  ź@  ’ 
   
  ģ@  ’ 
   
  ]@   ’ 
   
  F@ø  ’ 
   
  E@ø  ’ 
   
  E@¹  ’ 
   
  $@Ł  ’ 	   	  V'Ł  ’ 	   	  VXØ  ’ 	   	  VXØ  ’ 
   
  C@»  ’ 	   	  VE»  ’ 	   	  V1Ļ  ’       @VS   ’ 	   	  V`   ’ 	   	  W ż  ’ 	   	  Vź  ’ 	   	  Vü  ’      VE {@>  ’ 
   
  VEP|?  ’ 	   	  Vć  ’ 	   	  V¬T  ’ 
   
  ¬@R  ’ 
   
  @@ ½  ’ 
   
  @@ ¾  ’ 	   	  W ~  ’ 	   	  W ē  ’ 	   	  W u  ’ 
   
  @ t  ’ 	   	  W u  ’ 
   
  }@   ’ 	   	  W   ’ 
   
  X@ ¦  ’ 
   
  G@ ·  ’      Vć m@ ®  ’ 
   
  P@ ®  ’ 	   	  W2 Ī  ’ 
   
  a@   ’ 	   	  W® R  ’ 
   
  QxV? I  ’ 	   	  WĻ 1  ’ 	   	  WŠ 0  ’ 
   
  ū@   ’ 	   	  ž@  ’      H   ’ 	   	  U0Š  ’ 	   	  P¹G  ’ 	   	  Qh  ’ 	   	  Qh  ’ 	   	  Vč  ’ 
   
  ÷@  ’ 	   	  Rų  ’ 	   	  Rų  ’ 	   	  PZ¦  ’ 	   	  Qõ  ’ 	   	  Tl  ’ 	   	  S±O  ’ 	   	  WÓ -  ’ 	   	  V>Ā  ’ 	   	  P]£  ’ 	   	  S²N  ’ 
   
  é@  ’ 	   	  W· I  ’ 	   	  RÅ;  ’ 
   
  Ć@;  ’ 	   	  RÕ+  ’ 	   	  P»E  ’ 	   	  VD¼  ’ 	   	  VE»  ’ 
   
  (@Ö  ’ 	   	  Tp  ’ 	   	  Sz  ’ 
   
  G@ ¶  ’ 	   	  QC½  ’ 	   	  P@Ą  ’ 	   	  UŅ.  ’      X@č@»  ’ 
   
  C@»  ’ 
   
  Ś@#  ’ 
   
  @f  ’ 	   	  W ’  ’ 
   
   ī@  ’ 
   
  @|  ’ 	   	  Q~  ’ 
   
  @ü@  ’ 
   
  @ū@  ’      @ &@Ģ  ’      @ %@Ķ  ’      @ &@Ķ  ’      @ (@	Ī  ’      @ )@Ī  ’      @ )@Š  ’ 
   
  @P,Ó  ’ 	   	  @’  ’      @ @ā  ’      @ @ą  ’      @ @į  ’ 	   	  @ ą  ’ 	   	  @į  ’ 	   	  @ć  ’ 	   	  @ä  ’ 	   	  @å  ’ 	   	  @ē  ’ 	   	  @é  ’ 	   	  @č  ’ 	   	  @é  ’ 	   	  @ė  ’ 	   	  @ķ  ’ 	   	  @ī  ’ 	   	  @š  ’ 	   	  @ń  ’ 	   	  @ņ  ’ 	   	  @ō  ’ 	   	  @õ  ’ 	   	  @
ö  ’ 	   	  @	÷  ’ 	   	  @ł  ’ 	   	  @ś  ’ 	   	  @ū  ’ 	   	  @ü  ’ 	   	  @ż  ’ 	   	  @ž  ’ 	   	  @’