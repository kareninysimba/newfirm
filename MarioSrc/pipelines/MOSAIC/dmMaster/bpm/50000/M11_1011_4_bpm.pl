  ¦V  +Ó  ¬$TITLE = "DFLATS Observation(s)"
$CTIME = 973251655
$MTIME = 973255305
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
RAWFILE = 'n501453.fits'       / Original raw file
FILENAME= 'K4M10B_20101027-IFlat-kp4m20101027T235153F' / Current filename
NEXTEND =                    8 / Number of extensions
OBSTYPE = 'dome flat'          / Observation type
PROCTYPE= 'MasterCal'          / Processing type
PRODTYPE= 'image   '           / Product type
MIMETYPE= 'application/fits'   / Mimetype of this data file
EXPTIME =                   55 / Exposure time (sec)
OBJRA   = '18:50:24.22'        / Right Ascension
OBJDEC  = '-17:00:00.0'        / Declination
OBJEPOCH=               2010.8 / [yr] epoch

          # Single exposure quantities are representative from 1st exposure
TIMESYS = 'UTC     '           / Time system
DATE-OBS= '2010-10-27T23:51:53.0' / Date and time of exposure start
TIME-OBS= '23:51:53'           / Universal time
MJD-OBS =       55496.99436343 / MJD of observation start
ST      = '18:50:24'           / Sidereal time

OBSERVAT= 'KPNO    '           / Observatory
OBS-ELEV=                2120. / [km] Observatory elevation
OBS-LAT = '31:57.8 '           / [deg] Observatory latitude
OBS-LONG= '111:36.0'           / [deg] Observatory longitude
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2010.8 / Equinox of tel coords
TELRA   = '18:50:24.22'        / RA of telescope (hr)
TELDEC  = '-17:00:00.0'        / DEC of telescope (deg)
TELFOCUS=                -8873 / Telescope focus
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off     '           / ADC Mode
ADCPAN1 =                 0.01 / [deg] MSE ADC 1 prism angle
ADCPAN2 =                 0.05 / [deg] MSE ADC 2 prism angle
CORRCTOR= 'Mayall Corrector'   / Corrector Identification

INSTRUME= 'mosaic_1_1'         / Mosaic detector
NDETS   =                    8 / Number of detectors in mosaic
FILTER  = 'I Nearly-Mould k1005' / Filter name
FILTID  = 'k1005   '           / Unique filter identification
PHOTBW  =              191.459 / [nm] Filter wavelength FWHM
PHOTFWHM=              191.459 / [nm] Filter wavelength FWHM
PHOTCLAM=              820.453 / [nm] Filter wavelength width
FILTPOS =                    5 / Instrument filter position
FILTERSN= 'k1005   '           / Filter serial number
NOCGAIN = 'normal  '           / NOCS gain setting
SHUTSTAT= 'dark    '           / Shutter status
ENVTEM  =                   13 / [celsius] MSE temp7 - Ambient
DEWAR   = 'Mosaic1.1 Dewar'    / Dewar identification
DEWTEM  =               -178.5 / [celsius] MSE temp1 - Dewar tank
DEWTEM2 =                 -9.7 / [celsius] MSE temp3 - Fill Neck
CCDTEM  =          -106.199997 / [celsius] MSE temp2 - CCD Focal Plate

OBSERVER= 'Howell, Schweiker, Mathis, Reedy' / Observer(s)
PROPOSER= 'David Sawyer'       / Proposer(s)
PROPID  = '2010B-2005'         / Proposal identification
OBSID   = 'kp4m.20101027T235153' / Observation ID
EXPID   =                    0 / Monsoon exposure ID
NOCID   =        2455497.70176 / NOCS exposure ID

CONTROLR= 'Mosaic System Electronics  Sep 2' / MSE name and revision date
DHEFILE = 'mosaic1_Seq2amp250Kpx.ucd' / Sequencer file

NOHS    = '2.0.0   '           / NOHS ID
NOCMDOF =                    0 / [arcsec] Map Dec offset
NOCMITER=                    0 / Map iteration count
NOCNO   =                   31 / observation number in this sequence
NOCPOST = 'dfs     '           / ntcs_moveto ra dec epoch
NOCDROF =                    0 / [arcsec] Dither RA offset
NOCDHS  = 'DFLATS  '           / DHS script name
NOCORA  =                    0 / [arcsec] RA offset
NOCODEC =                    0 / [arcsec] Dec offset
NOCMPOS =                    0 / Map position
NOCDITER=                    0 / Dither iteration count
NOCMPAT = '4Q      '           / Map pattern
NOCMREP =                    0 / Map repetition count
NOCDDOF =                    0 / [arcsec] Dither Dec offset
NOCTOT  =                   50 / Total number of observations in set
NOCSCR  = 'DFLATS  '           / NOHS script run
NOCFOCUS=                    0 / [um] nics_focus value
NOHS    = 'Mosaic.1.1'         / NOHS ID
NOCTIM  =                   55 / [s] Requested integration time
NOCOFFT = '0 0     '           / ntcs_offset RA Dec offset (arcsec)
NOCSYS  = 'kpno 4m '           / system ID
NOCNUM  =                   10 / observation number request
NOCLAMP = 'On      '           / Dome flat lamp status (on|off|unknown)
NOCRBIN =                    1 / CCD row binning
NOCMROF =                    0 / [arcsec] Map RA offset
NOCSKY  =                    0 / sky offset modulus
NOCNPOS =                    1 / observation number in requested number
NOCCBIN =                    1 / CCD column binning
NOCTYP  = 'DFLATS  '           / Observation type
NOCDPOS =                    0 / Dither position
NOCDPAT = '5PX     '           / Dither pattern
NOCDREP =                    0 / Dither repetition count

RAINDEX =                    0 / [arcsec] RA index
RAZERO  =                570.5 / [arcsec] RA zero
ALT     = '41:02:15.0'         / Telescope altitude
DECINST =                    0 / [arcsec] Dec instrument center
DECDIFF =                    0 / [arcsec] Dec diff
FOCUS   =                -8873 / [mm] Telescope focus
PARALL  =                  360 / [deg] parallactic angle
DECZERO =              -375.12 / [arcsec] Dec zero
AZ      = '180:00:00.0'        / Telescope azimuth
RADIFF  =                    0 / [arcsec] RA diff
RAINST  =                    0 / [arcsec] RA instrument center
DECOFF  =                    0 / [arcsec] Dec offset
DECINDEX=                    0 / [arcsec] Dec index
RAOFF   =                    0 / [arcsec] RA offset

DOMEERR =               135.85 / [deg] Dome error as distance from target
DOMEAZ  =                    0 / [deg] Dome position

MSEREADY= 'dark    '           / MSE shutter ready (none|guide|dark|restore)

TCPGDR  = 'off     '           / Guider status (on|off|lock)

DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1_1        '  /  instrument identifier
DTCALDAT= '2010-10-27        '  /  calendar date from observing schedule
ODATEOBS= '                  '  /  previous DATE-OBS
DTUTC   = '2010-10-27T23:57:30'  /  post exposure UTC epoch from DTS
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2010B-2005        '  /  observing proposal ID
DTPI    = 'David Sawyer      '  /  Principal Investigator
DTPIAFFL= 'National Optical Astronomy Observatory'  /  PI affiliation
DTTITLE = 'On-sky Commissioning of MOSA 1.1'  /  title of observing proposal
DTCOPYRI= 'AURA              '  /  copyright holder of data
DTACQUIS= 'mosaic1dhs-01-4m.kpno.noao.edu'  /  host name of data acquisition com
DTACCOUN= 'cache             '  /  observing account name
DTACQNAM= '/home/data/n501453.fits'  /  file name supplied at telescope
DTNSANAM= 'kp1247271.fits    '  /  file name in NOAO Science Archive
DTSTATUS= 'done              '  /  data transport status
SB_HOST = 'mosaic1dhs-01-4m.kpno.noao.edu'  /  iSTB client host
SB_ACCOU= 'cache             '  /  iSTB client user account
SB_SITE = 'kp                '  /  iSTB host site
SB_LOCAL= 'kp                '  /  locale of iSTB daemon
SB_DIR1 = '20101027          '  /  level 1 directory in NSA DS
SB_DIR2 = 'kp4m              '  /  level 2 directory in NSA DS
SB_DIR3 = '2010B-2005        '  /  level 3 directory in NSA DS
SB_RECNO=              1247271  /  iSTB sequence number
SB_ID   = 'kp1247271         '  /  unique iSTB identifier
SB_NAME = 'kp1247271.fits    '  /  name assigned by iSTB
RMCOUNT =                    0  /  remediation counter
RECNO   =              1247271  /  NOAO Science Archive sequence number
XTALKFIL= 'Mosaic11_xtalkdummy.txt' /

GAIN    =                  1.1 / Approx inv gain (ADU/e)
RDNOISE =                  5.0 / Approx readout noise (e)
SATURATE=               220000 / Approx saturation (ADU)

DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.001509 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         3.103876E-42 / depth of longest jump down
CCDMEAN =            149304.45
OVSCNMTD=                    3
DQGLFSAT=                   0.
IMCMB001= 'kp1247271.fits'
IMCMB002= 'kp1247272.fits'
IMCMB003= 'kp1247274.fits'
IMCMB004= 'kp1247275.fits'
IMCMB005= 'kp1247277.fits'
IMCMB006= 'kp1247278.fits'
IMCMB007= 'kp1247281.fits'
IMCMB008= 'kp1247285.fits'
IMCMB009= 'kp1247286.fits'
IMCMB010= 'kp1247287.fits'
NCOMBINE=                   10
QUALITY =                   0.
GAINMEAN=                  1.1

PIPELINE= 'NOAO Mosaic Pipeline' / Name of calibration pipeline
PLVER   = 'MOSAIC V1.1'        / Pipeline version
EFFTIME =                   55 / [s] Effective exposure time
PLPROPID= 'CAL     '

PLQUEUE = 'K4M10B  '           / PL Queue
PLQNAME = '20101027'           / PL Dataset
PLPROCID= '7ae6e75 '           / PL Processing ID
PLFNAME = 'IFlat-kp4m20101027T235153F' / PL Filename
PLOFNAME= 'n501453_pl'         / Original file name
PCOUNT  =                    0 / No 'random' parameters
GCOUNT  =                    1 / Only one group
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
EXTNAME = 'ccd4    '           / Extension name
INHERIT =                    T / Inherits global header
DATE    = '2010-11-02T18:19:21' / Date FITS file was generated
IRAF-TLM= '2010-11-03T17:47:13' / Time of last modification
IMAGEID =                    4 / Image identification

CCDNAME = 'SN10-02 '           / CCD name
AMPNAME = 'SN10-02:A'          / Amplifier name
BUNIT   = 'adu     '           / [adu] ADU counts
CCDSUM  = '1 1     '           / CCD pixel summing
DETSEC  = '[6145:8192,1:4096]' / Detector section


XTALKCOR= 'Nov  2 11:02 No crosstalk correction required'
BIASFIL = 'K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z[ccd4]' / Bias reference
DQOVMJLR=                   5. / length of deepest jump down
DQOVMJJR=             57.45077 / amplitude of deepest jump
DQOVJPRB=                   1. / probability of a jump
DQOVMIN =               332.44 / min in overscan region
DQOVMAX =             615.7551 / max in overscan region
DQOVMEAN=             605.6566 / mean in overscan region
DQOVSIG =             22.07127 / sigma in overscan region
DQOVSMEA=             605.6375 / mean in collapsed overscan strip
DQOVSSIG=             2.663475 / sigma in collapsed overscan strip
SATPROC = 'Nov  2 11:08 Sat: 220000. ADU (242000. e-), grw=0'
TRIM    = 'Nov  2 11:08 Trim is [1:1024,1:4096]'
FIXPIX  = 'Nov  2 11:08 Fix Mosaic11_dummy_4_bpm.pl + sat' /
OVERSCAN= 'Nov  2 11:08 Overscan is [1025:1074,1:4096], function=minmax'
ZEROCOR = 'Nov  2 11:08 Zero is K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z' /
CCDPROC = 'Nov  2 11:09 CCD processing done'
AMPMERGE= 'Nov  2 11:09 Merged 2 amps'
DQGLMEAN=             145943.5
DQGLSIG =              4811.06
DQDFCRAT=             2653.518
DCCDMEAN=            0.9675512
CCDMNTMP=             149304.5
    Ž                          1  1     S  ¬      ’’’               ’S      eß U@v-"P$, @s  	Pr(`
 ØP$<P5]Pč"Pd#dP5eP#
P 	P!`
# o@1P4P%P3 P P.2„P%LP i@p`5ĢP!P1P0`	#CPo`	2P 
PQ UP."GP" P `
`
 » @2gPaC`	`
3%P$@@t7!P## @rq3¼P#ÅP,"r  @ 	 0@5ÜP$ŅP
1āP`
 P"Ķ@qØ``
4®P/#_P#3VP!#P1!’P/ 	P2P 2*P`	#XP"@ 
 =@4gP P$#ė @3JP#S @sJ`
0P`
"ŃP4`	0ÄP+`	 ÄP1¶P`
1įPc¹`0óP4P#1P1`
3ŁP&] @3ōP2P$[P4P(`	`
`	`
0ÄP!åP`	`
`	$\ &@5tP2%}@ut`
bQrG`
`
`
`
`
`
`
`
`
`
`
`
`
`
$Ž@ 
@6P#sP`	 
P`	`
`	`
` 
P0 P4QP!P'`
!NP0ÓP`
`
`
2¤P$ 	P$P"S 0@3P 	P;2üP,%3P<`	27Po1_P# P<3P#Õ @1HP1¾Pv"AP2 Ī g@1#Pl19P`
`
`
20P`
$m l@t5pĶ"ÅP
`	`
"3 /@1§P"!° @5P% @ 
@%ų ’         ’ 	   	  @’  ’ 
   
   @ł  ’ 
   
   W@§  ’ 	   	  Pė  ’ 	   	  Pv  ’ 
   
   u@  ’ 	   	  På  ’ 	   	  P0Š  ’ 	   	  Pģ  ’ 	   	  P`   ’ 	   	  @’  ’ 	   	  PAæ  ’ 	   	  Pa  ’ 
   
   Ņ@,  ’ 	   	  PŌ,  ’ 	   	  Pń  ’ 	   	  P¼D  ’ 	   	  Px  ’ 	   	  PŻ#  ’ 
   
   Ü@"  ’ 
   
   Õ@)  ’ 	   	  PČ8  ’ 	   	  P¬T  ’ 	   	  PŖV  ’ 
   
  T@Ŗ  ’ 
   
  M@±  ’ 	   	  Qķ  ’ 	   	  P’  ’ 
   
   ż@  ’ 	   	  Pž  ’ 
   
   ü@  ’ 	   	  Pż  ’ 
   
  @v  ’ 	   	  Qx  ’ 	   	  Q{  ’ 	   	  Q|  ’ 
   
  @{  ’ 
   
  @|  ’ 
   
  @  ’ 
   
  ~@  ’ 
   
  }@  ’ 
   
  |@  ’ 
   
  |@  ’ 
   
  {@  ’ 
   
  z@  ’ 
   
  y@  ’ 
   
  x@  ’ 
   
  w@  ’ 
   
  u@  ’ 
   
  t@  ’ 
   
  s@  ’ 
   
  s@  ’ 
   
  q@  ’ 	   	  Qa  ’ 	   	  Q¾B  ’ 	   	  Rļ  ’ 	   	  Rž  ’ 	   	  Qņ  ’ 	   	  Qš  ’ 
   
  &@Ų  ’ 	   	  Rź  ’ 	   	  RXØ  ’ 
   
  W@§  ’ 	   	  RY§  ’ 	   	  RZ¦  ’ 
   
  @į  ’ 
   
  @į  ’ 	   	  R#Ż  ’ 
   
  @ņ  ’ 	   	  Rņ  ’ 	   	  R~  ’ 
   
  PÓQļ>  ’ 
   
  Ć@;  ’ 	   	  Rl  ’ 	   	  Rw  ’ 
   
  QQ7*  ’ 	   	  S	÷  ’ 
   
  č@  ’ 	   	  Rź  ’ 
   
  !@Ż  ’ 
   
   @Ü  ’ 
   
  @Ż  ’ 
   
   @Ż  ’ 	   	  S=Ć  ’ 	   	  Su  ’ 
   
  k@  ’ 	   	  Sm  ’ 
   
  a@  ’ 	   	  Sr  ’ 	   	  SĮ?  ’ 	   	  SĘ:  ’ 
   
  f@  ’ 
   
  g@  ’ 	   	  Tė  ’ 	   	  Tģ  ’ 	   	  Só  ’ 
   
  !@Ż  ’ 
   
  !@Ü  ’ 	   	  T    ’      @RZ#  ’ 
   
  _@  ’ 	   	  T­S  ’ 	   	  TY§  ’ 	   	  T]£  ’ 	   	  Tw  ’ 	   	  Ta  ’ 	   	  Uõ  ’ 	   	  Tż  ’ 	   	  TÉ7  ’ 	   	  Uķ  ’ 
   
  (@Ö  ’ 	   	  U)×  ’ 
   
  ņ@  ’ 	   	  Tó  ’ 	   	  TŃ/  ’ 
   
  Ļ@/  ’ 	   	  T×)  ’ 
   
  Ö@(  ’ 	   	  TŲ(  ’ 
   
  Ö@'  ’      QŠ@'  ’ 
   
  ×@'  ’ 	   	  Tź  ’ 	   	  Tå  ’ 
   
  ī@  ’ 
   
  ķ@  ’ 
   
  ģ@  ’ 
   
  ģ@  ’ 	   	  Tś  ’ 
   
  ł@  ’ 	   	  Tū  ’      ^@ Ō@Č  ’ 
   
  4@É  ’ 	   	  U6Ź  ’ 	   	  UY§  ’ 	   	  UZ¦  ’ 	   	  Uj  ’ 	   	  Uu  ’ 	   	  UD¼  ’ 	   	  V"Ž  ’ 	   	  UŅ.  ’ 
   
  Ń@-  ’ 	   	  UŅ.  ’ 	   	  Vį  ’ 	   	  UæA  ’ 	   	  UĒ9  ’ 	   	  VAæ  ’ 	   	  V~  ’      ų@ļ@  ’ 
   
   @ ž  ’ 
   
  ’@ ž  ’ 	   	  W ’  ’ 	   	  Vų  ’ 
   
  {@   ’ 	   	  W į  ’ 	   	  W  ą  ’ 	   	  W å  ’ 	   	  W1 Ļ  ’      H   ’ 	   	  W b  ’ 	   	  W a  ’      @@ O  ’ 
   
  RU P  ’ 	   	  WÕ +  ’ 
   
  Ó@ +  ’ 	   	  W¦ Z  ’ 	   	  Wó   ’ 
   
  @ū@  ’ 	   	  Vł  ’ 	   	  RŲ(  ’ 
   
  @ a  ’ 
   
  7@Ē  ’ 	   	  Qń  ’ 
   
  @ą  ’ 	   	  Vų  ’ 	   	  VøH  ’ 
   
  ^@  ’ 
   
  @ģ  ’ 	   	  W š  ’ 	   	  V1Ļ  ’ 	   	  Q+Õ  ’ 	   	  PøH  ’ 	   	  T]£  ’ 
   
  @|  ’ 
   
  p@  ’ 	   	  Qq  ’ 	   	  W ń  ’ 	   	  S(Ų  ’ 	   	  W m  ’ 	   	  Qa  ’ 	   	  WŠ 0  ’ 	   	  T³M  ’ 
   
  X@ „ % ’ 	   	  WZ ¦