  �V  /  $TITLE = "Dflats  filter1 = B"
$CTIME = 847645488
$MTIME = 847645589
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-10T23:35:53' / Date FITS file was generated
IRAF-TLM= '16:36:00 (10/11/2006)' / Time of last modification
OBJECT  = 'Dflats  filter1 = B' / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat007'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '16:25:58.92'        / RA of observation (hr)
DEC     = '29:20:15.82'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-10-08T22:39:54.2' / Date of observation start (UTC approximate)
TIME-OBS= '22:39:54.2'         / Time of observation start
MJD-OBS =       53286.94437731 / MJD of observation start
MJDHDR  =       53286.94436343 / MJD of header creation
LSTHDR  = '16:25:05          ' / LST of header creation

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2004.8 / Equinox of tel coords
TELRA   = '16:26:10.38       ' / RA of telescope (hr)
TELDEC  = '29:19:37.5        ' / DEC of telescope (deg)
ZD      = '2.6458            ' / Zenith distance
AIRMASS =                1.001 / Airmass
TELFOCUS=                -9020 / Telescope focus
CORRCTOR= 'Mayall Corrector'   / Corrector Identification
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off               ' / ADC tracking state null-track-preset
ADCPAN1 =  0.0000000000000E+00 / ADC 1 prism angle
ADCPAN2 =  0.0000000000000E+00 / ADC 2 prism angle

DETECTOR= 'CCDMosaThin1'       / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.258 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.258 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    2 / Filter position
FILTER  = 'B Harris k1002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -4.0300000000000E-01 / North TV Camera focus position
TV2FOC  = -2.1000000000000E+00 / South Camera focus position
ENVTEM  =  1.9900000000000E+01 / Ambient temperature (C)
DEWAR   = 'CCDMosaThin1 Dewar' / Dewar identification
DEWTEM  = -1.6839999400000E+02 / Dewar temperature (C)
DEWTEM2 =  1.2000000000000E+00 / Fill Neck temperature (C)
DEWTEM3 = 'N2   0.0'
CCDTEM  = -9.9400002000000E+01 / CCD temperature  (C)
CCDTEM2 = 'CCD   0.0'

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '2.000  13Feb96     (add mode and group to hdrs)' / Controller softwar
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Fri Oct  8 14:16:41 2004' / Date waveforms last compiled

OBSERVER= 'Schweiker'          / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'kp4m.20041008T223954' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/kpno/4meter/caldir/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic1_xtalk030301.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =                63009  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



































































































































DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1          '  /  instrument identifier
DTCALDAT= '2004-10-08        '  /  calendar date from observing schedule
DTPUBDAT= '2006-04-09        '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2004B-0321        '  /  observing proposal ID
DTPI    = 'Arlin Crotts      '  /  Principal Investigator
DTPIAFFL= 'Columbia University'  /  PI affiliation
DTTITLE = 'Microlensing in M31 at Large Distances and for Large Masses'  /  titl
DTACQUIS= 'tan.kpno.noao.edu '  /  host name of data acquisition computer
DTACCOUN= 'lp                '  /  observing account name
DTACQNAM= '/md1/4meter/dflat007.fits'  /  file name supplied at telescope
DTNSANAM= 'kp063009.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041008/kp4m/2004B-0321/kp063009.fits.gz'
UPARM   = 'MarioCal$/CCDMosaThin1_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M1_50000'
IMAGEID =                    8 / Image identification
EXPTIME =               18.000 / Exposure time in secs
DARKTIME=               19.478 / Total elapsed time in secs

CCDNAME = 'SITe #7223FBR10-02 (NOAO 10)' / CCD name
AMPNAME = 'SITe #7223FBR10-02 (NOAO 10), upper left (Amp23)' / Amplifier name
GAIN    =                  2.7 / gain expected for amp 423 (e-/ADU)
RDNOISE =                  5.6 / read noise expected for amp 423 (e-)
SATURATE=                28000 / Maximum good data value (ADU)
CONHWV  = 'ACEB004_AMP23'      / Controller hardware version
ARCONG  =                  2.7 / gain expected for amp 423 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,4096:1]'    / Amplifier section
DETSEC  = '[6145:8192,4097:8192]' / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                  -1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =               4097.0 / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                6144. / CCD to detector transformation
DTV2    =                4096. / CCD to detector transformation

WCSASTRM= 'kp4m.19981012T035510 (Tr 37 B) by L. Davis 19981013' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =             246.4955 / Coordinate reference value
CRVAL2  =            29.337728 / Coordinate reference value
CRPIX1  =           -2189.5024 / Coordinate reference pixel
CRPIX2  =           -62.603679 / Coordinate reference pixel
CD1_1   =        6.6447846e-07 / Coordinate matrix
CD2_1   =       -7.1174129e-05 / Coordinate matrix
CD1_2   =       -7.1658886e-05 / Coordinate matrix
CD2_2   =        6.4903791e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. -0.2965453646647815 -0.001'
WAT1_002= '742039682310374 -0.3015592630690014 -0.1532080105024498 1.1491904504'
WAT1_003= '87457E-4 -5.269180478923738E-5 5.415270156632761E-4 -9.3281434172018'
WAT1_004= '82E-5 1.310433054403524E-4 5.765595642491415E-4 -1.456494843133616E-'
WAT1_005= '5 4.592676508605742E-5 -6.374359492698783E-5 -1.244851527444973E-5"'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. -0.2965453646647815 -0.00'
WAT2_002= '1742039682310374 -0.3015592630690014 -0.1532080105024498 1.235509668'
WAT2_003= '635310E-4 5.486738451147500E-5 2.837481929060693E-4 6.35710063668713'
WAT2_004= '0E-6 2.359280292286416E-5 4.108253842533054E-4 -9.708749013328279E-5'
WAT2_005= ' 2.052140089586407E-4 -5.864980494508054E-6 -1.025366656761586E-5"'

CHECKSUM= 'HDofKCldHCldHCld'    /  ASCII 1's complement checksum
DATASUM = '450889595 '          /  checksum of data records
XTALKCOR= 'Oct 23 16:49 Crosstalk is 9.04E-4*im07'
ZERO    = 'MarioCal$/UBVRI_K20041008_1161645623-kp4m20041008T235941Z-ccd8.fits'
CALOPS  = 'XOTZFWSBPG'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             2458.665 / min in overscan region
DQOVMAX =             2460.905 / max in overscan region
DQOVMEAN=              2459.66 / mean in overscan region
DQOVSIG =             2.035421 / sigma in overscan region
DQOVSMEA=             2459.659 / mean in collapsed overscan strip
DQOVSSIG=            0.2888523 / sigma in collapsed overscan strip
TRIM    = 'Oct 23 16:59 Trim is [25:2072,1:4096]'
OVERSCAN= 'Oct 23 16:59 Overscan is [2087:2136,1:4096], function=minmax'
ZEROCOR = 'Oct 23 16:59 Zero is MarioCal$/UBVRI_K20041008_1161645623-kp4m200410'
CCDPROC = 'Oct 23 16:59 CCD processing done'
PROCID  = 'kp4m.20041008T223954V4'
OVSCNMTD=                    3
DQGLFSAT=                   0.
DQGLMEAN=             7738.963
DQGLSIG =             239.1103
DQDFCRAT=             429.9424
DQDFGLME=             7715.291 / DQ global mean of zero frame
DQDFGLSI=             301.6295 / DQ global sigma of zero frame
DQDFXMEA=             7715.289 / DQ mean of collapsed x bias
DQDFXSIG=             233.2564 / DQ sigma of collapsed x bias
DQDFXSLP=           -0.3411791 / DQ slope of collapsed x bias
DQDFXCEF=            0.8649563 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             7715.291 / DQ mean of collapsed y bias
DQDFYSIG=              122.375 / DQ sigma of collapsed y bias
DQDFYSLP=           0.04761514 / DQ slope of collapsed y bias
DQDFYCEF=            0.4601238 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  356
DQADNUSD=                    7
DQADNREJ=                    0
DQADGLME=              7599.32 / DQ global mean of zero frame
DQADGLSI=             293.2099 / DQ global sigma of zero frame
DQADXMEA=              7599.32 / DQ mean of collapsed x bias
DQADXSIG=             229.8103 / DQ sigma of collapsed x bias
DQADXSLP=           -0.3362325 / DQ slope of collapsed x bias
DQADXCEF=            0.8651983 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=              7599.32 / DQ mean of collapsed y bias
DQADYSIG=             120.4882 / DQ sigma of collapsed y bias
DQADYSLP=           0.04663381 / DQ slope of collapsed y bias
DQADYCEF=            0.4576977 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
DCCDMEAN=            0.8965848
GAINMEAN=                 2.75
PROCID01= 'kp4m.20041008T223954V3'
PROCID02= 'kp4m.20041008T233852V3'
PROCID03= 'kp4m.20041008T231927V3'
PROCID04= 'kp4m.20041008T230002V3'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53286.95
MJDMAX  =             53853.01
OBJMASK = 'bp2mkbpm6.pl'
   �                          �  �      D        ���               �� D      ````` 
 @ �@`Lpp7```p0��@```
` 
R5 
 �@ P_ @ R� 
 @`	 @ @ 	8@ 
P� 
@  �@y 	@ s@`0t@� ��         ��       �@4@ S@ @ 2@  �@0Z@ @    ��      W@P7  �@0Qd P0@PP 	@ !  ��       G@P�  ��       G@�  �� 
   
   VH�  ��       �@0  ��       �@/  ��       �@0  �� 
   
   R�0  ��       �@/  �� 
   
   R�0  �� 
   
   W� K  ��       @�  ��       �@ &  ��       �@ &  �� 
   
   R�  �� 	   	  W� M  ��       W�p M  ��       W�p` L  �� 	   	  W� M  �� 
   
   V�?  �� 
   
   W^ �  ��       \@ � y �� 	   	  W� M  ��       W�p` L  ��       @ �@P�0@ L  �� 
   
   VI�  ��       H@�  ��       H@�  ��       VGP�  ��       G@�  ��       E@�  ��       H@�  ��       H@�  ��       V
Q�p` L  ��       V
P@Qh0@ L