  ¦V  /  	$TITLE = "Dflats  filter1 = B"
$CTIME = 847644383
$MTIME = 847644516
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-10T23:35:55' / Date FITS file was generated
IRAF-TLM= '16:36:02 (10/11/2006)' / Time of last modification
OBJECT  = 'Dflats  filter1 = B' / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat007'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '16:25:58.92'        / RA of observation (hr)
DEC     = '29:20:15.82'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-10-08T22:39:54.2' / Date of observation start (UTC approximate)
TIME-OBS= '22:39:54.2'         / Time of observation start
MJD-OBS =       53286.94437731 / MJD of observation start
MJDHDR  =       53286.94436343 / MJD of header creation
LSTHDR  = '16:25:05          ' / LST of header creation

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2004.8 / Equinox of tel coords
TELRA   = '16:26:10.38       ' / RA of telescope (hr)
TELDEC  = '29:19:37.5        ' / DEC of telescope (deg)
ZD      = '2.6458            ' / Zenith distance
AIRMASS =                1.001 / Airmass
TELFOCUS=                -9020 / Telescope focus
CORRCTOR= 'Mayall Corrector'   / Corrector Identification
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off               ' / ADC tracking state null-track-preset
ADCPAN1 =  0.0000000000000E+00 / ADC 1 prism angle
ADCPAN2 =  0.0000000000000E+00 / ADC 2 prism angle

DETECTOR= 'CCDMosaThin1'       / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.258 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.258 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    2 / Filter position
FILTER  = 'B Harris k1002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -4.0300000000000E-01 / North TV Camera focus position
TV2FOC  = -2.1000000000000E+00 / South Camera focus position
ENVTEM  =  1.9900000000000E+01 / Ambient temperature (C)
DEWAR   = 'CCDMosaThin1 Dewar' / Dewar identification
DEWTEM  = -1.6839999400000E+02 / Dewar temperature (C)
DEWTEM2 =  1.2000000000000E+00 / Fill Neck temperature (C)
DEWTEM3 = 'N2   0.0'
CCDTEM  = -9.9400002000000E+01 / CCD temperature  (C)
CCDTEM2 = 'CCD   0.0'

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '2.000  13Feb96     (add mode and group to hdrs)' / Controller softwar
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Fri Oct  8 14:16:41 2004' / Date waveforms last compiled

OBSERVER= 'Schweiker'          / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'kp4m.20041008T223954' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/kpno/4meter/caldir/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic1_xtalk030301.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =                63009  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



































































































































DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1          '  /  instrument identifier
DTCALDAT= '2004-10-08        '  /  calendar date from observing schedule
DTPUBDAT= '2006-04-09        '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2004B-0321        '  /  observing proposal ID
DTPI    = 'Arlin Crotts      '  /  Principal Investigator
DTPIAFFL= 'Columbia University'  /  PI affiliation
DTTITLE = 'Microlensing in M31 at Large Distances and for Large Masses'  /  titl
DTACQUIS= 'tan.kpno.noao.edu '  /  host name of data acquisition computer
DTACCOUN= 'lp                '  /  observing account name
DTACQNAM= '/md1/4meter/dflat007.fits'  /  file name supplied at telescope
DTNSANAM= 'kp063009.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041008/kp4m/2004B-0321/kp063009.fits.gz'
UPARM   = 'MarioCal$/CCDMosaThin1_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M1_50000'
IMAGEID =                    4 / Image identification
EXPTIME =               18.000 / Exposure time in secs
DARKTIME=               19.486 / Total elapsed time in secs

CCDNAME = 'SITe #7465FBR01-02 (NOAO 17)' / CCD name
AMPNAME = 'SITe #7465FBR01-02 (NOAO 17), lower right (Amp14)' / Amplifier name
GAIN    =                  2.5 / gain expected for amp 214 (e-/ADU)
RDNOISE =                  5.4 / read noise expected for amp 214 (e-)
SATURATE=                30000 / Maximum good data value (ADU)
CONHWV  = 'ACEB002_AMP14'      / Controller hardware version
ARCONG  =                  2.5 / gain expected for amp 214 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[2072:25,1:4096]'   / Amplifier section
DETSEC  = '[6145:8192,1:4096]' / Detector section

ATM1_1  =                  -1. / CCD to amplifier transformation
ATM2_2  =                   1. / CCD to amplifier transformation
ATV1    =               2073.0 / CCD to amplifier transformation
ATV2    =                   0. / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                6144. / CCD to detector transformation
DTV2    =                   0. / CCD to detector transformation

WCSASTRM= 'kp4m.19981012T035510 (Tr 37 B) by L. Davis 19981013' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =             246.4955 / Coordinate reference value
CRVAL2  =            29.337728 / Coordinate reference value
CRPIX1  =           -2179.0473 / Coordinate reference pixel
CRPIX2  =            4125.8791 / Coordinate reference pixel
CD1_1   =       -4.1925745e-07 / Coordinate matrix
CD2_1   =       -7.1283942e-05 / Coordinate matrix
CD1_2   =       -7.1815745e-05 / Coordinate matrix
CD2_2   =       -4.2121101e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. 3.735682462492365E-4 0.295'
WAT1_002= '3172635031091 -0.3013080051887377 -0.1536649177186417 -2.42675168291'
WAT1_003= '1114E-4 -2.984776249554674E-4 -5.295450658518399E-4 -9.8382230245456'
WAT1_004= '19E-5 1.511932023262053E-4 5.709821760663337E-4 7.712881056764677E-6'
WAT1_005= ' -6.243885819238467E-5 -5.353492014446661E-5 1.323439365835454E-5"'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. 3.735682462492365E-4 0.29'
WAT2_002= '53172635031091 -0.3013080051887377 -0.1536649177186417 1.91670961576'
WAT2_003= '0169E-4 9.306036303449705E-5 2.906599814786414E-4 -6.446778841907368'
WAT2_004= 'E-6 -9.944574467967150E-5 -3.874669531050944E-4 -1.019129537953247E-'
WAT2_005= '4 2.128791671096252E-4 1.748908419193041E-6 -1.559169075353465E-5"'

CHECKSUM= 'FEfmFDdlFDdlFDdl'    /  ASCII 1's complement checksum
DATASUM = '3355571175'          /  checksum of data records
XTALKCOR= 'Oct 23 16:48 No crosstalk correction required'
ZERO    = 'MarioCal$/UBVRI_K20041008_1161645623-kp4m20041008T235941Z-ccd4.fits'
CALOPS  = 'XOTZFWSBPG'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =              2982.92 / min in overscan region
DQOVMAX =              2985.34 / max in overscan region
DQOVMEAN=             2984.263 / mean in overscan region
DQOVSIG =             1.927577 / sigma in overscan region
DQOVSMEA=             2984.265 / mean in collapsed overscan strip
DQOVSSIG=            0.3273424 / sigma in collapsed overscan strip
TRIM    = 'Oct 23 17:04 Trim is [65:2112,1:4096]'
OVERSCAN= 'Oct 23 17:04 Overscan is [1:50,1:4096], function=minmax'
ZEROCOR = 'Oct 23 17:04 Zero is MarioCal$/UBVRI_K20041008_1161645623-kp4m200410'
CCDPROC = 'Oct 23 17:04 CCD processing done'
PROCID  = 'kp4m.20041008T223954V4'
OVSCNMTD=                    3
DQGLFSAT=                   0.
DQGLMEAN=              9633.97
DQGLSIG =             354.1123
DQDFCRAT=             535.2206
DQDFGLME=             9584.406 / DQ global mean of zero frame
DQDFGLSI=             425.6426 / DQ global sigma of zero frame
DQDFXMEA=             9584.405 / DQ mean of collapsed x bias
DQDFXSIG=             244.4176 / DQ sigma of collapsed x bias
DQDFXSLP=           -0.3161205 / DQ slope of collapsed x bias
DQDFXCEF=            0.7648309 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             9584.406 / DQ mean of collapsed y bias
DQDFYSIG=             334.6357 / DQ sigma of collapsed y bias
DQDFYSLP=            0.2567375 / DQ slope of collapsed y bias
DQDFYCEF=            0.9072759 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  356
DQADNUSD=                    7
DQADNREJ=                    0
DQADGLME=             9441.127 / DQ global mean of zero frame
DQADGLSI=             415.7433 / DQ global sigma of zero frame
DQADXMEA=             9441.127 / DQ mean of collapsed x bias
DQADXSIG=             240.8131 / DQ sigma of collapsed x bias
DQADXSLP=           -0.3115337 / DQ slope of collapsed x bias
DQADXCEF=            0.7650157 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=             9441.127 / DQ mean of collapsed y bias
DQADYSIG=             329.9463 / DQ sigma of collapsed y bias
DQADYSLP=            0.2531658 / DQ slope of collapsed y bias
DQADYCEF=            0.9073693 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
DCCDMEAN=             1.114061
GAINMEAN=                 2.75
PROCID01= 'kp4m.20041008T223954V3'
PROCID02= 'kp4m.20041008T233852V3'
PROCID03= 'kp4m.20041008T231927V3'
PROCID04= 'kp4m.20041008T230002V3'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53286.95
MJDMAX  =             53853.01
OBJMASK = 'bp2mkbpm6.pl'
 Ž  Ž                                 N  	      ’’’               ’N      ``*`, @ @" @ 	@ 
@`	 
@ 
@qmÄ`` @Ļ`	` 
@` @4 @`` @` @`` @
 @ @ @` @``>Ä@ @` @ @	 @`
 
@ 
@ 
@`
`
`
 
@ 
@
 
@ 
@`
 
@ 
@`
 
@` 
@ @ 
@ @`
`
 
@ 
@ 
@ 
@` @
 
@ 
@ 
@ 
@`
 
@ 
@`
 
@ 
@ 
@
`
 @ 
@ 
@	`
 
@`
`
 
@ 
@ 
@
`
`
 
@  
@`
`
 
@ 
@`
 
@`
 
@`
 
@
`
```` @````` @```` @ 
@ 
@``
`
`
`
`
`
 
@) 
@`
`
 @ 
@ 
@`
 @ 
@ 
@ 
@ 
@`
 
@ 
@`
` @ @`
`
 
@ 
@ 
@`
 @ @` @ 
@ 
@< 
@ 
@ 
@`
`
 
@ 
@`
 
@ 
@ 
@`
 
@ 
@	`
 
@ 
@ 
@`
 
@`
 @ 
@ 
@g_7_@
 
@ 
@ 
@ 
@ 
@ 
@ 
@`
 
@`
`
`
 
@ 
@ 
@`
 @ 
@ 
@ 
@`
 
@`
 
@`
 
@[ 
@ 
@	 
@ 
@ 
@	 
@`
 
@ 
@`
 
@`
 
@ 
@ 
@ 
@ 
@!`
`
`
`
 
@`
 
@ 
@`
 
@F`
 
@8`
 
@#`
 @``
`
 
@ 
@`
`` @`
 @ 
@`
 @`
 @' 
@ 
@`
`
 
@`
 
@ 
@-`
 
@ 
@ 
@% 
@`
 @`
` 
@ `
`
 @` @Z 
@`
 
@`
`
`
` 
@`
`
`
`
`
 
@ 
@`
 @`
` @ 
@ 
@`
 @` A+ 
@ 
@	 
@ 
@`
 @ 
@`
` 
@ 
@ 
@ 
@ 
@ Ar 
@`
 
@ 
@g`
`
`
 
@0`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@>`
 @"`
 @` @` @" 
@ 
@ @×`
 @F`
 @`
``
 @2`
``   ’         ’ *   *   PPp`0Å@  )@0  @ Y@  Ž@ 
@PP @ @P @ @ @ j  ’ ,   ,   PPp`0Å@  9@0  @PPP
 [@ @ @P  ź@ @PP @ &@P k  ’       PPp`0§@ Qf @ æ  ’       @Pp`č " ’       Pp`č  ’ 	   	  Pé  ’ 
   
   @é  ’ 	   	  Pé  ’ 
   
   @é  ’ 
   
  PPé q ’ 	   	  Pé  ’      P @Z  ’       Pp`0 @Z  ’       Pp @Z  ’      P @Z 	 ’      P @Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z 
 ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   £@Y  ’ 
   
   £@Z  ’ 
   
   £@Y  ’ 
   
   ¤@Z  ’ 
   
   ¤@Y  ’      P£ @Z  ’ 
   
   ¢@Z  ’      P£ @Z  ’ 
   
   ¢@Z  ’      P£ @Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’       £@ å@ r  ’       £@ ä@ r 
 ’ 
   
   £@Z  ’ 
   
   £@Y  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z 
 ’ 
   
   ¤@Z  ’       ¤@ Q1)  ’ 
   
   ¤@Z  ’ 
   
   £@Z 	 ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z 
 ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z   ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z 
 ’ 
   
   £@Z  ’       £@  æ@  ’       £@  ½@	  ’       £@  ½@
  ’       £@  ½@  ’       £@  ¼@  ’       £@  ¼@  ’       £@  ¼@  ’       ¤@  ½@  ’       £@  ½@«@ć  ’       ¤@  ½@«@ā  ’       ¤@  ½@  ’       ¤@  ¾@  ’       ¤@  Ą@
  ’       £@  Ą@	  ’       £@  Ą@  ’       ¤@  Į@  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’      P£ @Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z ) ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   ¢@Z  ’      P£ @Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’      P£ @Z  ’ 
   
   ¢@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’       ¤@ @J  ’       £@ @I  ’       £@ @J  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’       £@ (@0  ’       £@ '@/  ’       £@ '@0  ’       £@ T)1  ’ 
   
   £@Z  ’ 
   
   ¢@Z < ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   £@Y  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z 	 ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’       £@ Q×  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’       ¤@@8  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z [ ’ 
   
   £@Z  ’ 
   
   ¤@Z 	 ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z 	 ’ 
   
   ¢@Z  ’ 
   
   ¢@Y  ’ 
   
   £@Y  ’ 
   
   ¢@Y  ’ 
   
   ¢@Z  ’ 
   
   ”@Z  ’ 
   
   ¢@Y  ’ 
   
   £@Y  ’ 
   
   ¢@Y  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   £@Y ! ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z F ’ 
   
   £@Z  ’ 
   
   ¤@Z 8 ’ 
   
   £@Z  ’ 
   
   ¤@Z # ’ 
   
   £@Z  ’       £@/@$ ` ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’       ¢@ W:P 	  ’       ¢@1@)  ’       ¢@ Q@0 @P  ’ 
   
   ¢@Z  ’       ¢@ ¶@*z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’       £@@( '  ’ 
   
   £@Z  ’       £@ @* ' ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z - ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z % ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’       ¤@ c@+Ģ  ’ 
   
   ¤@Z  ’       ¤@«@)  ’ 
   
   ¤@Z   ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’       ¤@ J@  ’       £@ I@  ’       £@ QK Z ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ”@Z  ’      P¢ @Z  ’ 
   
   ”@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’       £@Ń@*_  ’ 
   
   £@Z  ’       £@V--  ’       £@@*  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’       £@ R¢ø  ’       £@  @·  ’       £@ R¢ø+ ’ 
   
   £@Z  ’ 
   
   ¤@Z 	 ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’       £@m@$É  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’       £@³@*}  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’       £@ p@čr ’ 
   
   £@Z  ’ 
   
   £@Y  ’ 
   
   £@Z  ’ 
   
   £@Y g ’ 
   
   £@Z  ’ 
   
   ¤@Z  ’ 
   
   £@Z  ’ 
   
   ¤@Z 0 ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’ 
   
   £@Z  ’ 
   
   £@Y  ’ 
   
   £@Z  ’ 
   
   £@Y > ’ 
   
   £@Z  ’       £@U@ļ " ’ 
   
   £@Z  ’       £@ Siń  ’       £@ g@š  ’       £@ g@ļ  ’       £@ g@š  ’       £@ Siń " ’ 
   
   £@Z  ’ 
   
   ¢@Z  ’        J@0 U@Z × ’ 
   
   £@Z  ’       Pn0 5@Z F ’ 
   
   £@Z  ’       £@M@* ć  ’ 
   
   £@Z  ’       £@`Y  ’ 
   
   @&R  ’       £@`Y 2 ’ 
   
   £@Z  ’       £@ Vę @ q  ’        @0 @ å@ q  ’        @0 @ Two@ q  ’ 	   	   G’  ’      P ö@,Ē  ’      Pō@1Ä  ’      P ö@,Ē Ļ ’ 	   	  Pé  ’      P @Z  ’ 
   
  PPZ  ’      P @Z  ’      P @Y 4 ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z 
 ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z  ’      P @Z  ’      P @ @¾  ’      P @Z