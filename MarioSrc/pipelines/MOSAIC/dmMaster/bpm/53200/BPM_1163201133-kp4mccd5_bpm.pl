  ŚV  /  Â$TITLE = "Dflats  filter1 = B"
$CTIME = 847644541
$MTIME = 847644719
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-10T23:35:51' / Date FITS file was generated
IRAF-TLM= '16:35:59 (10/11/2006)' / Time of last modification
OBJECT  = 'Dflats  filter1 = B' / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat007'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '16:25:58.92'        / RA of observation (hr)
DEC     = '29:20:15.82'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-10-08T22:39:54.2' / Date of observation start (UTC approximate)
TIME-OBS= '22:39:54.2'         / Time of observation start
MJD-OBS =       53286.94437731 / MJD of observation start
MJDHDR  =       53286.94436343 / MJD of header creation
LSTHDR  = '16:25:05          ' / LST of header creation

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2004.8 / Equinox of tel coords
TELRA   = '16:26:10.38       ' / RA of telescope (hr)
TELDEC  = '29:19:37.5        ' / DEC of telescope (deg)
ZD      = '2.6458            ' / Zenith distance
AIRMASS =                1.001 / Airmass
TELFOCUS=                -9020 / Telescope focus
CORRCTOR= 'Mayall Corrector'   / Corrector Identification
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off               ' / ADC tracking state null-track-preset
ADCPAN1 =  0.0000000000000E+00 / ADC 1 prism angle
ADCPAN2 =  0.0000000000000E+00 / ADC 2 prism angle

DETECTOR= 'CCDMosaThin1'       / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.258 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.258 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    2 / Filter position
FILTER  = 'B Harris k1002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -4.0300000000000E-01 / North TV Camera focus position
TV2FOC  = -2.1000000000000E+00 / South Camera focus position
ENVTEM  =  1.9900000000000E+01 / Ambient temperature (C)
DEWAR   = 'CCDMosaThin1 Dewar' / Dewar identification
DEWTEM  = -1.6839999400000E+02 / Dewar temperature (C)
DEWTEM2 =  1.2000000000000E+00 / Fill Neck temperature (C)
DEWTEM3 = 'N2   0.0'
CCDTEM  = -9.9400002000000E+01 / CCD temperature  (C)
CCDTEM2 = 'CCD   0.0'

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '2.000  13Feb96     (add mode and group to hdrs)' / Controller softwar
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Fri Oct  8 14:16:41 2004' / Date waveforms last compiled

OBSERVER= 'Schweiker'          / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'kp4m.20041008T223954' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/kpno/4meter/caldir/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic1_xtalk030301.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =                63009  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



































































































































DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1          '  /  instrument identifier
DTCALDAT= '2004-10-08        '  /  calendar date from observing schedule
DTPUBDAT= '2006-04-09        '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2004B-0321        '  /  observing proposal ID
DTPI    = 'Arlin Crotts      '  /  Principal Investigator
DTPIAFFL= 'Columbia University'  /  PI affiliation
DTTITLE = 'Microlensing in M31 at Large Distances and for Large Masses'  /  titl
DTACQUIS= 'tan.kpno.noao.edu '  /  host name of data acquisition computer
DTACCOUN= 'lp                '  /  observing account name
DTACQNAM= '/md1/4meter/dflat007.fits'  /  file name supplied at telescope
DTNSANAM= 'kp063009.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041008/kp4m/2004B-0321/kp063009.fits.gz'
UPARM   = 'MarioCal$/CCDMosaThin1_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M1_50000'
IMAGEID =                    5 / Image identification
EXPTIME =               18.000 / Exposure time in secs
DARKTIME=               19.490 / Total elapsed time in secs

CCDNAME = 'SITe #7061FBR03-02 (NOAO 02)' / CCD name
AMPNAME = 'SITe #7061FBR03-02 (NOAO 02), upper left (Amp21)' / Amplifier name
GAIN    =                  2.3 / gain expected for amp 321 (e-/ADU)
RDNOISE =                  9.0 / read noise expected for amp 321 (e-)
SATURATE=                32000 / Maximum good data value (ADU)
CONHWV  = 'ACEB003_AMP21'      / Controller hardware version
ARCONG  =                  2.3 / gain expected for amp 321 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,4096:1]'    / Amplifier section
DETSEC  = '[1:2048,4097:8192]' / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                  -1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =               4097.0 / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                   0. / CCD to detector transformation
DTV2    =                4096. / CCD to detector transformation

WCSASTRM= 'kp4m.19981012T035510 (Tr 37 B) by L. Davis 19981013' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =             246.4955 / Coordinate reference value
CRVAL2  =            29.337728 / Coordinate reference value
CRPIX1  =            4206.9202 / Coordinate reference pixel
CRPIX2  =           -55.851876 / Coordinate reference pixel
CD1_1   =        -5.563622e-07 / Coordinate matrix
CD2_1   =        -7.128914e-05 / Coordinate matrix
CD1_2   =       -7.1766416e-05 / Coordinate matrix
CD2_2   =       -1.9302281e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. -0.2967623866738803 -0.001'
WAT1_002= '740040337968481 0.1531061648402187 0.2998254625876741 2.386563556714'
WAT1_003= '842E-4 -3.313486454802691E-4 5.576783113562560E-4 -9.454756845870097'
WAT1_004= 'E-5 2.138908190042069E-4 -5.593359813624481E-4 3.445369296658811E-6 '
WAT1_005= '5.218496233612544E-5 -5.200065277482977E-5 5.503245981760675E-7"'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. -0.2967623866738803 -0.00'
WAT2_002= '1740040337968481 0.1531061648402187 0.2998254625876741 -1.9704682404'
WAT2_003= '27079E-4 1.519283750837069E-4 -2.683349458389367E-4 -1.0755879778498'
WAT2_004= '45E-5 -1.066260348674774E-4 3.970504227249098E-4 -8.184411217280594E'
WAT2_005= '-5 -2.137585931955552E-4 2.910535894431762E-5 1.983978788415179E-7"'

CHECKSUM= 'fD9PgB8MfB8MfB8M'    /  ASCII 1's complement checksum
DATASUM = '2222875649'          /  checksum of data records
XTALKCOR= 'Oct 23 16:49 No crosstalk correction required'
ZERO    = 'MarioCal$/UBVRI_K20041008_1161645623-kp4m20041008T235941Z-ccd5.fits'
CALOPS  = 'XOTZFWSBPG'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             2288.408 / min in overscan region
DQOVMAX =             4173.755 / max in overscan region
DQOVMEAN=             2294.177 / mean in overscan region
DQOVSIG =             21.94843 / sigma in overscan region
DQOVSMEA=             2291.324 / mean in collapsed overscan strip
DQOVSSIG=            0.9048016 / sigma in collapsed overscan strip
TRIM    = 'Oct 23 17:04 Trim is [25:2072,1:4096]'
OVERSCAN= 'Oct 23 17:04 Overscan is [2087:2136,1:4096], function=minmax'
ZEROCOR = 'Oct 23 17:04 Zero is MarioCal$/UBVRI_K20041008_1161645623-kp4m200410'
CCDPROC = 'Oct 23 17:04 CCD processing done'
PROCID  = 'kp4m.20041008T223954V4'
OVSCNMTD=                    3
DQGLFSAT=                   0.
DQGLMEAN=             10660.34
DQGLSIG =             362.3882
DQDFCRAT=             592.2411
DQDFGLME=             10593.74 / DQ global mean of zero frame
DQDFGLSI=             564.4677 / DQ global sigma of zero frame
DQDFXMEA=             10593.74 / DQ mean of collapsed x bias
DQDFXSIG=             489.9838 / DQ sigma of collapsed x bias
DQDFXSLP=             0.486771 / DQ slope of collapsed x bias
DQDFXCEF=            0.5874735 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             10593.74 / DQ mean of collapsed y bias
DQDFYSIG=             235.7399 / DQ sigma of collapsed y bias
DQDFYSLP=          -0.08514699 / DQ slope of collapsed y bias
DQDFYCEF=            0.4271285 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  356
DQADNUSD=                    7
DQADNREJ=                    0
DQADGLME=             10435.74 / DQ global mean of zero frame
DQADGLSI=             552.4194 / DQ global sigma of zero frame
DQADXMEA=             10435.74 / DQ mean of collapsed x bias
DQADXSIG=              482.464 / DQ sigma of collapsed x bias
DQADXSLP=            0.4796005 / DQ slope of collapsed x bias
DQADXCEF=            0.5878413 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=             10435.74 / DQ mean of collapsed y bias
DQADYSIG=             232.1342 / DQ sigma of collapsed y bias
DQADYSLP=          -0.08398128 / DQ slope of collapsed y bias
DQADYCEF=            0.4278246 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
DCCDMEAN=             1.232923
GAINMEAN=                 2.75
PROCID01= 'kp4m.20041008T223954V3'
PROCID02= 'kp4m.20041008T233852V3'
PROCID03= 'kp4m.20041008T231927V3'
PROCID04= 'kp4m.20041008T230002V3'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53286.95
MJDMAX  =             53853.01
OBJMASK = 'bp2mkbpm6.pl'
   Ţ                          Ă  Ă     ×  Â      ˙˙˙               ˙×      ``` @ @ @` @` @` @ @ @` @	 @ @ @ @ @	 @ @ @ @ @	``` @` @" @```` @ @ @
 @8 @ 
@ @`
 @ 
@` @ 
@` 
@ 
@ @$ 
@!+ô@{č+ő@ @J @|,@ @>|,@<@`,@ @ @ @<(@
 @ @` 
@ @ @`` @ @` 
@
 @``` @ 
@` @` @` 
@/` @ @ @' @ @` @J @ 
@ @U`` @)` @` 
@ @ @ @ 
@2` @6 @ 
@ @ @``` @` @ @` @ @ @` @p` @`` @9 @ @ @ @- @ 
@` @ @ @Ż @` @c` @ @ @D` @D` @@`` @ @! @ @ @ @! @ @ @`` @` @@ @` @` @ @ 
@``
 @Ś @ @` @Z` @`` @ @	 @N` @ @	 @1 @` @ @ @ @Y @ @.` 
@` 
@ @ 
@` @`` @ @` @ @ @K @ @ @`` @ @` @ @W @
 @$ @ 
@^ @` @	 @ @ @ @ @ď```` @` @ @`` @ @,``` @s @!N@ @`` @
 @ @ @```qý   ˙         ˙        !@QÂ0@`0 :@ @`0@§@ Pp0@c  ˙        !@QÂ0@`0 k@`TA0@c  ˙       Qć0@`0 k@`TA0@c  ˙      ć@ l@`TA0@c  ˙      Qç m@`TA0@c  ˙      Qç Pnp`TA0@c  ˙      Qç`Pmp`TA0@c  ˙       Qćp`Pmp`TA0@c  ˙       Qć0@ Pmp`TA0@c  ˙       Qć0@ Pkp`TA0@c  ˙       Qć0@`Pkp`TA0@c  ˙      ć@ Pkp`TA0@c  ˙       RUp`TA0@c  ˙      @ RTp`TA0@c  ˙      @ RTp`TA0@`c 	 ˙      @ RTp`TA0@c  ˙      @ RTp`TA0@`c  ˙      @ RTp`TA0@c  ˙      @ RSp`TA0@c  ˙      @ RSp`o@QĎ0@c 	 ˙      @ RSp`TA0@c  ˙      @ RSp`0@@c  ˙      @ RSp`TA0@c  ˙      @ RTp`TA0@c  ˙      @ RTp`TA0@`c 	 ˙      @ RTp`TA0@c  ˙      @ RTp`TA0@`c  ˙      @ RTp`TA0@`PA"  ˙      @ RTp`TA0@  @@!  ˙      @ RTp`TA0@  ?@!  ˙       RUp`TA0@  @@! " ˙       RUp`TA0@c  ˙      T@`TA0@c  ˙      RT`p`TA0@c  ˙      T@`TA0@c  ˙      RT`p`TA0@c  ˙      S@`TA0@c  ˙      RT`p`TA0@c  ˙       RUp`TA0@c 
 ˙      RV TB0@c 8 ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙       V0@`c  ˙ 
   
  @c  ˙      @`c  ˙ 
   
  @c  ˙ 
   
  @b  ˙      @`c $ ˙ 
   
  @c ! ˙       V0@c  ˙       V0@c  ˙       V0@c  ˙       V0@`c  ˙       V0@`c  ˙       V0@c 
 ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙ 
   
  @c 
 ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c  ˙      @`c  ˙ 
   
  @c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙ 
   
  @c / ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b ' ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b J ˙       V0@c  ˙ 
   
  @c  ˙       V0@c U ˙      @ V0@c  ˙      @ V0@c  ˙      @ V0@c ) ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙ 
   
  @c 2 ˙       V0@c  ˙       PˇUá0@c 6 ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@  Â@   ˙       V0@  Á@   ˙       V0@ PĂ   ˙       V0@b  ˙       V0@c  ˙       V0@`pb  ˙       V0@b  ˙       V0@`c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c p ˙       V0@c  ˙       V0@ Q @ W  ˙       V0@ 
@ V  ˙       V0@ @ T  ˙       V0@ Q T 9 ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       @U0@c - ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b Ż ˙       V0@c  ˙       V0@  @O  ˙       V0@  @O c ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c D ˙       V0@c  ˙       SôR¤0@c D ˙       V0@c  ˙       SŢRş0@c @ ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b ! ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       Ĺ@SŃ0@c ! ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@`c  ˙       V0@`pb  ˙       V0@b  ˙       V0@c  ˙       V0@`c @ ˙       V0@c  ˙      V`0@c  ˙       #@0Tr`0@c  ˙       #@0Tq`0@c  ˙       #@0Tr`0@c  ˙      V`0@c  ˙ 
   
  @c  ˙      V`0@c  ˙ 
   
  @c  ˙      V`0@c Ś ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c Z ˙       V0@c  ˙       TŰQ˝0@c  ˙       V0@c  ˙       V0@`c  ˙       V0@b  ˙       V0@`pb 	 ˙       V0@`c N ˙       V0@c  ˙       V0@`c  ˙       V0@c 	 ˙       V0@`c 1 ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c Y ˙       V0@c  ˙       V0@b . ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙ 
   
  @c  ˙       V0@c  ˙       V0@  @F  ˙       V0@  @F  ˙       V0@ PG  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b  ˙       V0@a  ˙       V0@b K ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       @S0@c  ˙       @S0@`c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c W ˙       V0@c 
 ˙       V0@`c $ ˙       V0@c  ˙ 
   
  @c ^ ˙       V0@c  ˙       V0@b  ˙       V0@c 	 ˙       V0@b  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@`c ď ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b  ˙       V0@c  ˙       V0@b , ˙       V0@c  ˙       6@)P90@c  ˙      3@/ P60@c  ˙       6@*P80@c s ˙       V0@c  ˙ 
   
  @c  ˙D  D   A @ @P @ @ 
@ @P @0 @  +@ k@QdP-PPPPPP3P< @P
 @PP @PP @PPP @PPP @PP	P @PPPPPP @ @ @P @P @P @P @PPP @PPPP @PPP @ @ @ @PPPP @P @P @P @ @P @P @P @PP @P @ @ @ @ @	 @ @	 @ @ @ @PPPPP @P @PP @PPPP @P @PP0@` @P @ @ @ @PP @PP @ @PP @ @PPPPPP @ @PPP @ @ @PPP
PPPPP @ @ @ @P @PPPPPPP @P @ @P @ @PP @ @ @PPPPP @ @ @ @P @PPP @ @ @ @P @P @ @  ˙      Q@c  ˙      Q U0@c  ˙      Q Q@TA0@c  ˙      Q QR0@ TA0@c 
 ˙      Q QR0@`T@0@c  ˙      Q ă@ Pl0@`T@0@c  ˙      Q ă@ Pl0@`T@0@`b  ˙       Qp`0 â@ Pl0@`T@0@`b  ˙       Qp`0 â@ Pl0@`T@0@c  ˙       Qp`Pâ0@ Pl0@`@Qź0@`b  ˙ 4   4   @Ý @ @ @	 @p` @ @ @PP
P @0P^ @ P;0@ Pl0@`n@ @Qź0@`b  ˙       V0@`c  ˙       V0@`c J ˙       V0@c  ˙       V0@`c  ˙       V0@`c > ˙       V0@c  ˙       V0@c  ˙       V0@`c  ˙       V0@c  ˙       V0@`c  ˙       V0@c