  �V  0�  �$TITLE = "Domeflats (afternoon) filter1 = B"
$CTIME = 847989339
$MTIME = 847990228
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-14T20:02:51' / Date FITS file was generated
IRAF-TLM= '13:02:59 (14/11/2006)' / Time of last modification
OBJECT  = 'Domeflats (afternoon) filter1 = B' / Name of the object observed
FILENAME= 'dflat011'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '20:01:49.85'        / RA of observation (hr)
DEC     = '20:39:35.79'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-12-08T19:33:49.3' / Date of observation start (UTC approximate)
TIME-OBS= '19:33:49.302'       / Time of observation start
MJD-OBS =       53347.81515396 / MJD of observation start
MJDHDR  =       53347.81507176 / MJD of header creation
LSTHDR  = '20:02:03.4        ' / LST of header creation

OBSERVAT= 'CTIO    '           / Observatory
TELESCOP= 'CTIO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2000.0 / Equinox of tel coords
TELRA   = '20:01:49.85       ' / RA of telescope (hr)
TELDEC  = '20:39:35.8        ' / DEC of telescope (deg)
HA      = '00:00:01.1        ' / hour angle (H:M:S)
ZD      = '50.8              ' / Zenith distance
AIRMASS =                1.581 / Airmass
TELFOCUS= '16071             ' / Telescope focus
CORRCTOR= 'Blanco Corrector'   / Corrector Identification
ADC     = 'Blanco ADC'         / ADC Identification

DETECTOR= 'Mosaic2 '           / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                   16 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.270 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.270 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    3 / Filter position
FILTER  = 'B Harris c6002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -1.6010000000000E+00 / North TV Camera focus position
TV2FOC  = -1.9970000000000E+00 / South Camera focus position
ENVTEM  =  2.0900000000000E+01 / Ambient temperature (C)
DEWAR   = 'Mosaic2 Dewar'      / Dewar identification
DEWTEM  =  0.0000000000000E+00 / Dewar temperature (C)
DEWTEM2 = -4.9799999000000E+01 / Fill Neck temperature (C)
DEWTEM3 = 'N2  90.6'
CCDTEM  = -9.4000000000000E+01 / CCD temperature (C)
CCDTEM2 = 'CCD 169.8'

WEATDATE= 'Dec 08 19:31:02 2004' / Date and time of last update
WINDSPD = '4.7     '           / Wind speed (mph)
WINDDIR = '248     '           / Wind direction (degrees)
AMBTEMP = '23.2    '           / Ambient temperature (degrees C)
HUMIDITY= '24      '           / Ambient relative humidity (percent)
PRESSURE= '782     '           / Barometric pressure (millibars)
DIMMSEE = 'mysql_query():Unknown Error seeing=column' / Tololo DIMM seeing

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '13July99ver7_30'    / Controller software version
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Wed Dec  8 15:51:13 2004' / Date waveforms last compiled

OBSERVER= 'ESSENCE/SuperMACHO' / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'ct4m.20041208T193349' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/ctio/4meter/caldir/Mosaic2/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic2_xtalk040501.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =               135390  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



























































































































DTSITE  = 'ct                '  /  observatory location
DTTELESC= 'ct4m              '  /  telescope identifier
DTINSTRU= 'optic             '  /  instrument identifier
DTCALDAT= '2004-12-08        '  /  calendar date from observing schedule
DTPUBDAT= 'none              '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= 'noao              '  /  observing proposal ID
DTPI    = 'Christopher Stubbs'  /  Principal Investigator
DTPIAFFL= 'University of Washington'  /  PI affiliation
DTTITLE = 'A Next Generation Microlensing Survey of the LMC'  /  title of observ
DTACQUIS= 'ctioa8.ctio.noao.edu'  /  host name of data acquisition computer
DTACCOUN= 'mosaic            '  /  observing account name
DTACQNAM= '/ua80/mosaic/tonight/dflat011.fits'  /  file name supplied at telesco
DTNSANAM= 'ct135390.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041208/ct4m/noao/ct135390.fits.gz'
UPARM   = 'MarioCal$/Mosaic2_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M2_53166'
IMAGEID =                   13 / Image identification
EXPTIME =               35.000 / Exposure time in secs
DARKTIME=               43.820 / Total elapsed time in secs

CCDNAME = 'SITe #98421FABR07-02 (NOAO 31)' / CCD name
AMPNAME = 'SITe #98421FABR07-02 (NOAO 31), upper left (Amp21)' / Amplifier name
GAIN    =                  2.4 / gain expected for amp 421 (e-/ADU)
RDNOISE =                  5.3 / read noise expected for amp 421 (e-)
SATURATE=                42000 / Maximum good data value (ADU)
CONHWV  = 'ACEB004_AMP21'      / Controller hardware version
ARCONG  =                  2.4 / gain expected for amp 421 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,4096:1]'    / Amplifier section
DETSEC  = '[4097:6144,4097:8192]' / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                  -1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =               4097.0 / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                4096. / CCD to detector transformation
DTV2    =                4096. / CCD to detector transformation

WCSASTRM= 'ct4m.20020927T000014 (USNO N B Harris c6002) by F. Valdes 2002-10-24'
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =            300.45771 / Coordinate reference value
CRVAL2  =            20.659942 / Coordinate reference value
CRPIX1  =           -91.217079 / Coordinate reference pixel
CRPIX2  =              -23.221 / Coordinate reference pixel
CD1_1   =       -2.5939867e-07 / Coordinate matrix
CD2_1   =         7.447199e-05 / Coordinate matrix
CD1_2   =         7.412796e-05 / Coordinate matrix
CD2_2   =       -1.5464468e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. 0.001240543243571545 0.305'
WAT1_002= '32552795606 0.006230573831111668 0.1593080082351559 -2.8632748045930'
WAT1_003= '67E-4 -6.862667742811570E-5 -6.463288649230908E-4 -1.079645858275469'
WAT1_004= 'E-4 1.092766070802854E-5 -2.252037309694490E-4 3.580175912155704E-6 '
WAT1_005= '-4.815066627492912E-5 -5.152519584463043E-5 8.627535980683276E-6 "'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. 0.001240543243571545 0.30'
WAT2_002= '532552795606 0.006230573831111668 0.1593080082351559 -5.739566530878'
WAT2_003= '452E-5 1.936750166860838E-5 -8.869602715288811E-5 -1.062977553389737'
WAT2_004= 'E-5 -5.748265540240881E-5 -4.161766317930904E-4 -1.026564891359817E-'
WAT2_005= '4 -8.743224220471274E-5 6.620581601025897E-6 -2.287772548745048E-5 "'

CHECKSUM= 'aACZa8AZaAAZa5AZ'    /  ASCII 1's complement checksum
DATASUM = '135814224 '          /  checksum of data records
XTALKCOR= 'Oct 23 22:52 Crosstalk is 7.05E-4*im14+9.20E-4*im15+6.71E-4*im16'
ZERO    = 'MarioCal$/UBVRI_C20041208_1161662005-ct4m20041208T190047Z-ccd7.fits'
CALOPS  = 'XOTZFWSBP'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             1374.908 / min in overscan region
DQOVMAX =             1377.485 / max in overscan region
DQOVMEAN=             1376.136 / mean in overscan region
DQOVSIG =              2.32207 / sigma in overscan region
DQOVSMEA=             1376.136 / mean in collapsed overscan strip
DQOVSSIG=             0.398158 / sigma in collapsed overscan strip
TRIM    = 'Oct 24  0:05 Trim is [25:1048,1:4096]'
OVERSCAN= 'Oct 24  0:05 Overscan is [1063:1112,1:4096], function=minmax'
ZEROCOR = 'Oct 24  0:05 Zero is MarioCal$/UBVRI_C20041208_1161662005-ct4m200412'
CCDPROC = 'Oct 24  0:05 CCD processing done'
PROCID  = 'ct4m.20041208T193349V5'
OVSCNMTD=                    3
AMPMERGE= 'Oct 24  0:05 Merged 2 amps'
DQGLFSAT=                   0.
DQGLMEAN=             12048.43
DQGLSIG =             779.6708
DQDFCRAT=             344.2408
DQDFGLME=             12045.33 / DQ global mean of zero frame
DQDFGLSI=             784.8552 / DQ global sigma of zero frame
DQDFXMEA=             12045.33 / DQ mean of collapsed x bias
DQDFXSIG=             752.3671 / DQ sigma of collapsed x bias
DQDFXSLP=            -1.162863 / DQ slope of collapsed x bias
DQDFXCEF=            0.9139954 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             12045.33 / DQ mean of collapsed y bias
DQDFYSIG=             158.8582 / DQ sigma of collapsed y bias
DQDFYSLP=           -0.0145364 / DQ slope of collapsed y bias
DQDFYCEF=            0.1082106 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  708
FLAT    = 'MarioCal$/UBVRI_C20041208_1161662005-B-ct4m20041210T195018F-ccd7'
DQADNUSD=                    9
DQADNREJ=                    0
DQADGLME=              8714.57 / DQ global mean of zero frame
DQADGLSI=             566.9507 / DQ global sigma of zero frame
DQADXMEA=              8714.57 / DQ mean of collapsed x bias
DQADXSIG=             543.0048 / DQ sigma of collapsed x bias
DQADXSLP=            -0.838334 / DQ slope of collapsed x bias
DQADXCEF=            0.9129748 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=              8714.57 / DQ mean of collapsed y bias
DQADYSIG=              127.085 / DQ sigma of collapsed y bias
DQADYSLP=          -0.03150687 / DQ slope of collapsed y bias
DQADYCEF=            0.2931793 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
DCCDMEAN=             1.056009
GAINMEAN=                 2.55
PROCID01= 'ct4m.20041208T193349V4'
PROCID02= 'ct4m.20041208T202848V4'
PROCID03= 'ct4m.20041208T201149V4'
PROCID04= 'ct4m.20041208T195415V4'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53347.82
MJDMAX  =             53740.88
OBJMASK = 'bp2mkbpm6.pl'
    �                          �  �     �  �      ���               ���      a`` @`` @	 @o @�  @ @ @^ @�  @ @`` @ @ @" @` @K @1  @ @ @o�?�@ @ @ @
 @X @ @ @ @ @ @ @ @` @J @ @n` @:` @`` @V @ @ @ @ @` @*` @` @ @ @ @ @ @ @ @ @``` @$` @` @ @ @ @ @` @ ` @& @ @	 @`` @ @	` @-�@`zpp\```.p0@=�@,`` @D``` @`` @` @* @ @` @ @&` @ @ @ @ @ @ @` @ @` @ @ @ @ @ @
` @ @	 @ @`` @
 @ @R` @` @+@p- @pp:�@`` @` @ @7`jS``:c@@ @ @ @+`
 @.j�``:�@ @`
 @ 
@` 
@`` @``
` 
@ @`
i:9-@iy @ @ @ @h�x�h�x�`` @ 
@
 @'`
 @h�8�@`
 @`
 @	 
@ @`
 @```` @ 
@ 	@`
 @V`
 @e`
``
``
 @ 
@` @ @`
`` @`
 @`` @ @T 
@ @& 
@ @`
 @W`
`` @`
`````
 @ 
@ @`
`` @`
 @ 
@ 
@ @`
 @3`
 @ @ 
@! 
@` 
@ @`
 	@ 
@ @"`
 @�`
 @ @ 
@ 	@`
 @S`
`` @?`
 	@$`
 @ 
@ @	`
` @`
 @`
 @``` @`
 @ @%`
 @ 
@ @`
``` @< 
@ @ 
@ @`
 @%!�@q�`!�@ @ 
@ @q�``
 
@&`	 @`	 @+`	 @`	 @ @5 	@` @ 	@ @`	 @%`	 @`	 @`	 @4 	@` @`	` @!`	 @`	 @ @5`	 @ @Z 	@ @  @ 	@ 	@ @`	` @`	`5````````   ��         �� 	   	  W �  �� 	   	  W �  ��       P@0T� �  �� 	   	  W �  ��       �@0S_ �  ��       �@0S_ �  �� 	   	  W �  �� 5   5   P� >@ �@ @P @ @ @ @P	 @P @ @ @ @P @PPP�Q��@ @0P PP �  ��         @ 	@ F�p @� +  ��        @ Gp @�   ��        Gp @�   ��        	G0@ @�   ��        G0@ @�   ��        G0@ @�   ��        G0@ @�   ��        G0@ @� 
  ��        G0@ @� 
  ��       RKp`R@p`0R� P�p   ��       RKp`R@p`0R� P�p   ��       RKp`0RA`0R� P�p   ��      RLRBR�P�   ��      RL @0Q.R�P�   ��      RL Q0Q/R�P�  	 ��      RLRBR�P�  
 ��      J@RBR�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      RLRBR�P�   ��      RL@@R�P�   ��      RL Q�0 L@R�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      RL@@R�P�  " ��      RLRBR�P�   ��      RL RAp`0R�P�   ��      RLRBR�P�   ��      RL@@`0R�P�   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      RLRBR�P�  
 ��      J@RBR�P�  X ��      RLRBR�P�   ��      K@RAR�P�   ��      RLRBR�P�   ��      K@RAR�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      RLRBR�P�   ��      K@RAR�P�   ��      RLRBR�P�   ��      K@RAR�P�  J ��      RLRBR�P�   ��      RLRB  %@0RiP�  n ��      RLRBR�P�   ��      RLRB Q�P0P�P�  : ��      RLRBR�P�   ��      RLRB  �@0Q�P�   ��      RLRB  �@0Q�P�   ��      RLRB  �@P0Q�P�   ��      RLRB  �@0Q�P�  V ��      RLRBR�P�   ��      K@RAR�P�   ��      RLRBR�P�   ��      RLRB �@0P�P�   ��      RLRB  �@0Q�P�   ��      RLRBR�P�   ��      RLRB Qg0Q*P�  * ��      RLRBR�P�   ��      RLA@R�P�   ��      RLRBR�P�   ��      RL@@ l@0Q#P�   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      RLRBR�P�   ��      K@RAR�P�   ��      RLRBR�P�   ��      RLA@R�P�   ��      RLRBR�P�   ��      RLA@R�P�  $ ��      RLRBR�P�   ��      RL R0P6R�P�   ��      RLRBR�P�   ��      RLRBR� P�0PR   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      RL@@R�P�    ��      RLRBR�P�   ��      RL@@R�P�  & ��      RLRBR�P�   ��      RL @0Q%R�P�  	 ��      RLRBR�P�   ��      RLRBR� �  ��      RLRB e@0P* �  ��      RLRBR� �  ��      RLA@R� � 	 ��      RLRBR� �  ��      RLRB Pr0R �  ��      RLRBR� � , ��      RLRBR� �  ��      RL  R@0Q�R� �  ��      RL  S@0Q�R� � D ��      RLRBR� �  ��      J@RBR� �  ��      RL P�0QwR� �  ��      RLRBR� �  ��      J@RBR� �  ��      RLRBR� �  ��      J@RBR� �  ��      RLRBR� �  ��      RL@@R� � * ��      RLRBR� �  ��      RLRB �@0Q �  ��      RLRBR� �  ��      RL Q�0P�R� �  ��      RLRB  �@0Q� � & ��      RLRBR� �  ��      RL P0R@R� �  ��      RLRBR� �  ��      RL@@R� �  ��      RLRBR� �  ��      K@RAR� �  ��      RLRBR� �  ��      J@RBR� �  ��      RLRBR� �  ��      RLA@R� �  ��      RLRBR� �  ��      RLA@R� �  ��      RLRBR� �  ��      RLA@R� �  ��      RLRBR� �  ��      J@RBR� �  ��      RLRBR� �  ��      K@RAR� � 
 ��      RLRBR� �  ��      RLRB  �@0R �  ��      RL@@  �@0R � 	 ��      RL@@R� �  ��      RL?@R� �  ��      RL@@R� �  ��      RLRBR� �  ��      RL@@R� � 
 ��      RLRBR� �  ��      RL P�0Q�R� � R ��      RLRBR� �  ��      RL P}0Q�R� �  ��      RLRBR� �  ��      RL P�0Q�R� �  ��      RLRBR� �  ��      RLRBR� �  ��      K@RAR� �  ��      RLRBR� �  ��      K@RAR� �  ��      RLRBR� �  ��      K@RAR� � 7 ��      RLRBR� �  ��      RLRB Qo0Q" � @ ��      RLRBR� �  ��       RKp`0RAR� �  ��      RLRBR� � + �� 
   
  T�R� �  ��       S0QwR� � . �� 
   
  T�R� �  ��       �@0Q�R� �  �� 
   
  T�R� �  ��      �@-P�R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  ��       R�0@R� �  ��       �@0@R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��       T�0PR� �  ��       �@0PR� �  ��       �@0PR� �  ��       �@0P
R� �  ��       �@0P	R� �  ��       �@0PR� �  ��       �@0PR� �  ��       �@0PR� �  ��       �@0PR� �  ��       �@0PR� �  ��       �@0PR� �  �� 
   
  T�R� � 
 ��      �@R� � ' �� 
   
  T�R� �  ��      T�R� P� "  �� 
   
  T�R� �  ��       R�0Q�R� �  �� 
   
  T�R� �  ��      T�R� P�  	 �� 
   
  T�R� �  ��      T�R�  P@ �  �� 
   
  T�R� �  ��      T� �@0P� �  ��      T� �@0P� �  ��      T� �@0P� �  ��      T� �@0P� �  ��      T� �@0P� �  ��      T� Q�0P� �  �� 
   
  T�R� �  �� 	   	  W �  �� 
   
  T�R� �  ��      �@R� � V �� 
   
  T�R� �  ��       R�0Q�R� � e �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  ��      �@PR� �  ��      T�PR� �  �� 
   
  T�R� �  ��      T�R� P� &  ��      T�R�  �@ %  ��      T�R�  �@ &  �� 
   
  T�R� �  ��      T�R�  �@  ��      T�R�  �@  ��      T�R�  �@   ��      T� P�0R �  ��      T�  �@0R � T �� 
   
  T�R� �  ��      T�  d@0R+ � & �� 
   
  T�R� �  ��      T�R�  A@ �  �� 
   
  T�R� �  ��      T� Q0Q � W �� 
   
  T�R� �  ��       T0P�R� �  ��      �@/  	@0P�R� �  ��      S�P�R� �  �� 
   
  T�R� �  ��      �@R �@R� �  ��      �@ �@R� �  ��      �@R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      T�R�  �@   �� 
   
  T�R� �  ��       �@0Q�R� �  ��       �@0Q�R� �  ��       �@0Q�R� �  �� 
   
  T�R� �  ��      T� Pb0R/ �  �� 
   
  T�R� �  �� 
   
  T�R�P�  ��      �@R�P�  �� 
   
  T�R�P�  ��       R�0Q�R�P� 3 �� 
   
  T�R�P�  ��      T� Q�0QP�  ��      T� �@0QP�  �� 
   
  T�R�P� ! �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  �� 	   	  W �  �� 
   
  T�R� �  ��       �@0Q�R� � " �� 
   
  T�R� �  ��       R�0Q�R� � � �� 
   
  T�R� �  ��      T� �@0P� �  ��      T� �@0P� �  �� 
   
  T�R� �  �� 	   	  W �  �� 
   
  T�R� �  ��       T�0PR� � S �� 
   
  T�R� �  ��       S0Q�R� �  ��       @0Q�R� �  ��       S0Q�R� � ? �� 
   
  T�R� �  �� 	   	  W � $ �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      T�R� P� \ 	 �� 
   
  T�R� �  ��      T� P�0Q� �  ��      T� P�Qd0P] �  �� 
   
  T�R� �  ��      T� Q;0QV �  �� 
   
  T�R� �  ��       �@0P�R� �  ��       �@0P�R� �  ��       �@0P�R� �  ��       �@0P�R� �  ��       �@0P�R� �  �� 
   
  T�R� �  ��       �@0Q�R� �  ��       �@0Q�R� � % �� 
   
  T�R� �  ��      T� RF0PK �  �� 
   
  T�R� �  ��       l@0QR� �  �� 
   
  T�R� �  ��      T�R� P �  ��      T�R� P �  ��      T�R�  @ �  ��      T�R� P  � < �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��       R�0Q�R� � % �� 
   
  T�R� �  ��       T�p` y@0R �  ��       T�p`P{0R �  ��      �@R� �  �� 
   
  T�R� �  �� 
   
  T�R� � & �� 	   	  W �  ��       �@,VP �  �� 	   	  W �  ��       l@0V� � + �� 	   	  W �  ��       R�0T; �  �� 	   	  W �  ��       �@0Pr �  ��       �@0Pr � 5 �� 	   	  W �  ��       �@0Q� �  ��       U�0Q� �  �� 	   	  W �  ��       �@0UP �  �� 	   	  W �  ��       P�0V4 � % �� 	   	  W �  ��       R40T� �  �� 	   	  W �  ��      @,T� �  �� 	   	  W �  ��      �@)U � 4 �� 	   	  W �  ��        �@0VG �  ��       P�0VH �  �� 	   	  W �  ��       @0Q �  ��       @0Q � ! �� 	   	  W �  ��       S�0SQ �  �� 	   	  W �  ��       QP0U� �  ��       N@0U� � 5 �� 	   	  W �  ��        �@0V1 �  ��        �@0V1 � Z �� 	   	  W �  ��       @0Q �  �� 	   	  W �  ��       T�p`0R� �  ��       T�p`0R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��       �@-0PhRBR�P�   ��       �@(0P4@@`0R�P�   ��       �@/0 _@RBR�P�   ��       �@)0 /@RBR�P�   ��       �@0PR� �  ��       �@0PR� �  ��       �@0PR� �  ��       �@0PR� �  ��       TO0P?R� �  ��      RLRB m@0Q  �  ��      RLRB n@0Q  �  ��      RLRB Qp0Q! �  ��      RLRB  v@0R �  ��      RLRB  r@0R �  ��      RLRB  u@0R �  ��      RLRB  t@0R �  ��      RLRB  r@0R �  ��       SJ0QD �@0P
 �  ��      T� �@0P
 �  ��       �@0Q� �@0P
 �  ��       [@0P�RBR� �  ��       X@0P�@@R� �  ��       W@0P�RBR� �  ��       W@0P�@@R� �  ��       Y@0P�RBR� �  ��       X@0P�RBR� �  ��       W@0P�RBR� �  ��       Y@0P�RBR� �  ��       Z@0P�RBR� �