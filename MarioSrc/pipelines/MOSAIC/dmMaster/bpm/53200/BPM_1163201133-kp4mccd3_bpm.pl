  ĻV  /?  .$TITLE = "Dflats  filter1 = B"
$CTIME = 847644310
$MTIME = 847644351
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-10T23:34:57' / Date FITS file was generated
IRAF-TLM= '16:35:05 (10/11/2006)' / Time of last modification
OBJECT  = 'Dflats  filter1 = B' / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat007'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '16:25:58.92'        / RA of observation (hr)
DEC     = '29:20:15.82'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-10-08T22:39:54.2' / Date of observation start (UTC approximate)
TIME-OBS= '22:39:54.2'         / Time of observation start
MJD-OBS =       53286.94437731 / MJD of observation start
MJDHDR  =       53286.94436343 / MJD of header creation
LSTHDR  = '16:25:05          ' / LST of header creation

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2004.8 / Equinox of tel coords
TELRA   = '16:26:10.38       ' / RA of telescope (hr)
TELDEC  = '29:19:37.5        ' / DEC of telescope (deg)
ZD      = '2.6458            ' / Zenith distance
AIRMASS =                1.001 / Airmass
TELFOCUS=                -9020 / Telescope focus
CORRCTOR= 'Mayall Corrector'   / Corrector Identification
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off               ' / ADC tracking state null-track-preset
ADCPAN1 =  0.0000000000000E+00 / ADC 1 prism angle
ADCPAN2 =  0.0000000000000E+00 / ADC 2 prism angle

DETECTOR= 'CCDMosaThin1'       / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.258 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.258 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    2 / Filter position
FILTER  = 'B Harris k1002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -4.0300000000000E-01 / North TV Camera focus position
TV2FOC  = -2.1000000000000E+00 / South Camera focus position
ENVTEM  =  1.9900000000000E+01 / Ambient temperature (C)
DEWAR   = 'CCDMosaThin1 Dewar' / Dewar identification
DEWTEM  = -1.6839999400000E+02 / Dewar temperature (C)
DEWTEM2 =  1.2000000000000E+00 / Fill Neck temperature (C)
DEWTEM3 = 'N2   0.0'
CCDTEM  = -9.9400002000000E+01 / CCD temperature  (C)
CCDTEM2 = 'CCD   0.0'

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '2.000  13Feb96     (add mode and group to hdrs)' / Controller softwar
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Fri Oct  8 14:16:41 2004' / Date waveforms last compiled

OBSERVER= 'Schweiker'          / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'kp4m.20041008T223954' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/kpno/4meter/caldir/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic1_xtalk030301.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =                63009  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



































































































































DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1          '  /  instrument identifier
DTCALDAT= '2004-10-08        '  /  calendar date from observing schedule
DTPUBDAT= '2006-04-09        '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2004B-0321        '  /  observing proposal ID
DTPI    = 'Arlin Crotts      '  /  Principal Investigator
DTPIAFFL= 'Columbia University'  /  PI affiliation
DTTITLE = 'Microlensing in M31 at Large Distances and for Large Masses'  /  titl
DTACQUIS= 'tan.kpno.noao.edu '  /  host name of data acquisition computer
DTACCOUN= 'lp                '  /  observing account name
DTACQNAM= '/md1/4meter/dflat007.fits'  /  file name supplied at telescope
DTNSANAM= 'kp063009.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041008/kp4m/2004B-0321/kp063009.fits.gz'
UPARM   = 'MarioCal$/CCDMosaThin1_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M1_50000'
IMAGEID =                    3 / Image identification
EXPTIME =               18.000 / Exposure time in secs
DARKTIME=               19.486 / Total elapsed time in secs

CCDNAME = 'SITe #7298FBR03-02 (NOAO 13)' / CCD name
AMPNAME = 'SITe #7298FBR03-02 (NOAO 13), lower right (Amp12)' / Amplifier name
GAIN    =                  2.9 / gain expected for amp 212 (e-/ADU)
RDNOISE =                  5.6 / read noise expected for amp 212 (e-)
SATURATE=                40000 / Maximum good data value (ADU)
CONHWV  = 'ACEB002_AMP12'      / Controller hardware version
ARCONG  =                  2.9 / gain expected for amp 212 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[2072:25,1:4096]'   / Amplifier section
DETSEC  = '[4097:6144,1:4096]' / Detector section

ATM1_1  =                  -1. / CCD to amplifier transformation
ATM2_2  =                   1. / CCD to amplifier transformation
ATV1    =               2073.0 / CCD to amplifier transformation
ATV2    =                   0. / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                4096. / CCD to detector transformation
DTV2    =                   0. / CCD to detector transformation

WCSASTRM= 'kp4m.19981012T035510 (Tr 37 B) by L. Davis 19981013' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =             246.4955 / Coordinate reference value
CRVAL2  =            29.337728 / Coordinate reference value
CRPIX1  =           -44.348574 / Coordinate reference pixel
CRPIX2  =            4111.9649 / Coordinate reference pixel
CD1_1   =       -1.0794432e-07 / Coordinate matrix
CD2_1   =       -7.2367184e-05 / Coordinate matrix
CD1_2   =       -7.2021474e-05 / Coordinate matrix
CD2_2   =       -1.4094224e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. 9.239570296984172E-4 0.296'
WAT1_002= '0728531387589 -0.151415123220828 -0.002702340005683134 -2.6472572396'
WAT1_003= '03200E-4 3.704077644580447E-5 -5.223368457894908E-4 -1.1117618247283'
WAT1_004= '16E-4 5.454579714195485E-5 1.994298713413152E-4 -1.198909704460132E-'
WAT1_005= '6 -4.270185443038038E-5 -3.056872085815367E-5 1.130735373413304E-6"'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. 9.239570296984172E-4 0.29'
WAT2_002= '60728531387589 -0.151415123220828 -0.002702340005683134 8.9786180849'
WAT2_003= '63590E-5 7.235168104230941E-6 1.003730828822857E-4 1.937270919139057'
WAT2_004= 'E-8 -5.885261776387826E-5 -3.414468160698770E-4 -8.349172606715145E-'
WAT2_005= '5 6.759709804204376E-5 1.097365300315989E-6 -3.406115548277192E-6"'

CHECKSUM= '39NZA9LY49LYA9LY'    /  ASCII 1's complement checksum
DATASUM = '3895808875'          /  checksum of data records
XTALKCOR= 'Oct 23 16:48 Crosstalk is 0.00298*im04'
ZERO    = 'MarioCal$/UBVRI_K20041008_1161645623-kp4m20041008T235941Z-ccd3.fits'
CALOPS  = 'XOTZFWSBPG'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             2948.424 / min in overscan region
DQOVMAX =             2951.222 / max in overscan region
DQOVMEAN=             2949.858 / mean in overscan region
DQOVSIG =             1.807428 / sigma in overscan region
DQOVSMEA=             2949.856 / mean in collapsed overscan strip
DQOVSSIG=            0.3332982 / sigma in collapsed overscan strip
TRIM    = 'Oct 23 16:54 Trim is [65:2112,1:4096]'
OVERSCAN= 'Oct 23 16:54 Overscan is [1:50,1:4096], function=minmax'
ZEROCOR = 'Oct 23 16:54 Zero is MarioCal$/UBVRI_K20041008_1161645623-kp4m200410'
CCDPROC = 'Oct 23 16:54 CCD processing done'
PROCID  = 'kp4m.20041008T223954V4'
OVSCNMTD=                    3
DQGLFSAT=          9.536743E-7
DQGLMEAN=             8405.091
DQGLSIG =             170.4824
DQDFCRAT=             466.9495
DQDFGLME=             8387.273 / DQ global mean of zero frame
DQDFGLSI=             207.9769 / DQ global sigma of zero frame
DQDFXMEA=             8387.273 / DQ mean of collapsed x bias
DQDFXSIG=              60.4813 / DQ sigma of collapsed x bias
DQDFXSLP=           0.00978759 / DQ slope of collapsed x bias
DQDFXCEF=           0.09569736 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             8387.273 / DQ mean of collapsed y bias
DQDFYSIG=             172.1396 / DQ sigma of collapsed y bias
DQDFYSLP=            0.1111195 / DQ slope of collapsed y bias
DQDFYCEF=            0.7633641 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  356
DQADNUSD=                    7
DQADNREJ=                    0
DQADGLME=             8261.969 / DQ global mean of zero frame
DQADGLSI=             195.7235 / DQ global sigma of zero frame
DQADXMEA=             8261.969 / DQ mean of collapsed x bias
DQADXSIG=             59.51873 / DQ sigma of collapsed x bias
DQADXSLP=          0.009374689 / DQ slope of collapsed x bias
DQADXCEF=           0.09314264 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=             8261.969 / DQ mean of collapsed y bias
DQADYSIG=             170.0099 / DQ sigma of collapsed y bias
DQADYSLP=            0.1098826 / DQ slope of collapsed y bias
DQADYCEF=            0.7643228 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
PUPILCOR= 'Oct 23 16:57 maximum amplitude = 2.013E-6'
DCCDMEAN=            0.9706318
GAINMEAN=                 2.75
PROCID01= 'kp4m.20041008T223954V3'
PROCID02= 'kp4m.20041008T233852V3'
PROCID03= 'kp4m.20041008T231927V3'
PROCID04= 'kp4m.20041008T230002V3'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53286.95
MJDMAX  =             53853.01
OBJMASK = 'bp2mkbpm6.pl'
    Ū                          ´  ´      R  .      ˙˙˙               ˙ R       @
 z@` 
 @ @ @ PD! P`
 @` @``1W Z@` Ž@	`	 @ @`` @ 
@B 	[@ 
 @ Q` @ @ R` R 
Pŋ @` P 
P/ 
P  
 C@X ˙        
 ˙       @ø  ˙       Ę@3  ˙ 
   
   QÍ3  ˙       ×@&  ˙       Ö@&  ˙       ×@&  ˙ 
   
   Sč  ˙       R@Ģ  ˙       R@Ŧ 	 ˙ 	   	  Qų  ˙      Q S^  ˙       Qp`ø  ˙      @`ø  ˙       Qp`ø  ˙      @`ø  ˙ 
   
  @ų B ˙ 	   	  Qų  ˙ 
   
   UJļ  ˙       @^  ˙       Ā@>  ˙       Ā@=  ˙       ŋ@=  ˙       Ā@=  ˙       1@Í  ˙       1@Ė  ˙ 
   
   P5Ë  ˙       ų@  ˙       ų@  ˙       ų@  ˙ 
   
   Rc  ˙ 
   
   U  ˙ 
   
   P?Á  ˙       @ø  ˙ 
   
   PÅ;  ˙        Á@8  ˙        Ā@	7  ˙        ŋ@6  ˙        Ā@	7  ˙        Á@8  ˙ 
   
   PÅ;