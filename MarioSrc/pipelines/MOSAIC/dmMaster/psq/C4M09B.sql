REPLACE INTO PSQ VALUES('C4M09B', 'C4M09B', 'C4M09BD', 'MOSAIC',
'dir', 'disabled', 'proctype=''Raw'' and dtinstru=''mosaic_2'' and obstype not in (''focus'',''test'')');

REPLACE INTO C4M09B (dataset) VALUES ('20090911');
REPLACE INTO C4M09B (dataset) VALUES ('20090915');
REPLACE INTO C4M09B (dataset) VALUES ('20090919');
REPLACE INTO C4M09B (dataset) VALUES ('20090923');
REPLACE INTO C4M09B (dataset) VALUES ('20091016');
REPLACE INTO C4M09B (dataset) VALUES ('20091019');
REPLACE INTO C4M09B (dataset) VALUES ('20091021');
REPLACE INTO C4M09B (dataset) VALUES ('20091105');
REPLACE INTO C4M09B (dataset) VALUES ('20091109');
REPLACE INTO C4M09B (dataset) VALUES ('20091113');
REPLACE INTO C4M09B (dataset) VALUES ('20091116');
REPLACE INTO C4M09B (dataset) VALUES ('20091119');
REPLACE INTO C4M09B (dataset) VALUES ('20091123');
REPLACE INTO C4M09B (dataset) VALUES ('20100108');
REPLACE INTO C4M09B (dataset) VALUES ('20100109');
REPLACE INTO C4M09B (dataset) VALUES ('20100113');
REPLACE INTO C4M09B (dataset) VALUES ('20100117');
REPLACE INTO C4M09B (dataset) VALUES ('20100120');

REPLACE INTO C4M09BD VALUES ('20090911', '20090911', '20090914',
  'dtcaldat between ''2009-09-11%'' and ''2009-09-14%''');
REPLACE INTO C4M09BD VALUES ('20090915', '20090915', '20090918',
  'dtcaldat between ''2009-09-15%'' and ''2009-09-18%''');
REPLACE INTO C4M09BD VALUES ('20090919', '20090919', '20090922',
  'dtcaldat between ''2009-09-19%'' and ''2009-09-22%''');
REPLACE INTO C4M09BD VALUES ('20090923', '20090923', '20090925',
  'dtcaldat between ''2009-09-23%'' and ''2009-09-25%''');
REPLACE INTO C4M09BD VALUES ('20091016', '20091016', '20091018',
  'dtcaldat between ''2009-10-16%'' and ''2009-10-18%''');
REPLACE INTO C4M09BD VALUES ('20091019', '20091019', '20091020',
  'dtcaldat between ''2009-10-19%'' and ''2009-10-20%''');
REPLACE INTO C4M09BD VALUES ('20091021', '20091021', '20091024',
  'dtcaldat between ''2009-10-21%'' and ''2009-10-24%''');
REPLACE INTO C4M09BD VALUES ('20091105', '20091105', '20091108',
  'dtcaldat between ''2009-11-05%'' and ''2009-11-08%''');
REPLACE INTO C4M09BD VALUES ('20091109', '20091109', '20091112',
  'dtcaldat between ''2009-11-09%'' and ''2009-11-12%''');
REPLACE INTO C4M09BD VALUES ('20091113', '20091113', '20091115',
  'dtcaldat between ''2009-11-13%'' and ''2009-11-15%''');
REPLACE INTO C4M09BD VALUES ('20091116', '20091116', '20091118',
  'dtcaldat between ''2009-11-16%'' and ''2009-11-18%''');
REPLACE INTO C4M09BD VALUES ('20091119', '20091119', '20091122',
  'dtcaldat between ''2009-11-19%'' and ''2009-11-22%''');
REPLACE INTO C4M09BD VALUES ('20091123', '20091123', '20091125',
  'dtcaldat between ''2009-11-23%'' and ''2009-11-25%''');
REPLACE INTO C4M09BD VALUES ('20100108', '20100108', '20100108',
  'dtcaldat between ''2010-01-08%'' and ''2010-01-08%''');
REPLACE INTO C4M09BD VALUES ('20100109', '20100109', '20100112',
  'dtcaldat between ''2010-01-09%'' and ''2010-01-12%''');
REPLACE INTO C4M09BD VALUES ('20100113', '20100113', '20100116',
  'dtcaldat between ''2010-01-13%'' and ''2010-01-16%''');
REPLACE INTO C4M09BD VALUES ('20100117', '20100117', '20100119',
  'dtcaldat between ''2010-01-17%'' and ''2010-01-19%''');
REPLACE INTO C4M09BD VALUES ('20100120', '20100120', '20100122',
  'dtcaldat between ''2010-01-20%'' and ''2010-01-22%''');
