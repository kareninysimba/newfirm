REPLACE INTO PSQ VALUES('MTEST', 'MTEST', 'MTESTD', 'MOSAIC', 'dir',
    'disabled', 'proctype=''Raw'' and obstype!=''focus''');

/* Dataset definitions for Stacking Test: Objects + Cal */
REPLACE INTO MTEST (dataset) VALUES ('S20070904');
REPLACE INTO MTESTD VALUES ('S20070904', 20070904, 20070905,
  'dtinstru=''mosaic_1'' and (obstype=''zero'' or filter like ''%k1004%'') and (substr(reference,3,6) between ''592288'' and ''592297'' or substr(reference,3,6) between ''592464'' and ''592686'' or substr(reference,3,6) between ''592706'' and ''592736'' or substr(reference,3,6) between ''592817'' and ''592826'')');

/* Dataset definitions for Stacking Test: Objects only */
REPLACE INTO MTEST (dataset) VALUES ('S20070904o');
REPLACE INTO MTESTD VALUES ('S20070904o', 20070904, 20070905,
  'dtinstru=''mosaic_1'' and (obstype=''object'' and filter like ''%k1004%'') and (substr(reference,3,6) between ''592288'' and ''592297'' or substr(reference,3,6) between ''592464'' and ''592686'' or substr(reference,3,6) between ''592706'' and ''592736'' or substr(reference,3,6) between ''592817'' and ''592826'')');

/* Dataset definitions for small test */
REPLACE INTO MTEST (dataset) VALUES ('3Z3F3O');
REPLACE INTO MTESTD VALUES ('3Z3F3O', 20070904, 20070905,
  'dtinstru=''mosaic_1'' and (obstype=''zero'' or filter like ''%k1004%'') and (substr(reference,3,6) between ''592288'' and ''592290'' or substr(reference,3,6) between ''592464'' and ''592502'' or substr(reference,3,6) between ''592817'' and ''592819'')');

/* Dataset definitions for small test */
REPLACE INTO MTEST (dataset) VALUES ('3O');
REPLACE INTO MTESTD VALUES ('3O', 20070904, 20070905,
  'dtinstru=''mosaic_1'' and filter like ''%k1004%'' and substr(reference,3,6) between ''592464'' and ''592502''');
