REPLACE INTO PSQ VALUES ('SAMPLE', 'SAMPLE', 'SAMPLED', 'MOSAIC', 'dir',
    'disabled', 'proctype=''Raw'' and obstype not in (''focus'',''test'')');

REPLACE INTO SAMPLE (dataset) VALUES ('20051001');
REPLACE INTO SAMPLE (dataset) VALUES ('20051003');
REPLACE INTO SAMPLE (dataset) VALUES ('20051005');
REPLACE INTO SAMPLE (dataset) VALUES ('20051024');
REPLACE INTO SAMPLE (dataset) VALUES ('20051026');
REPLACE INTO SAMPLE (dataset) VALUES ('20051028');
REPLACE INTO SAMPLE (dataset) VALUES ('20051030');
REPLACE INTO SAMPLE (dataset) VALUES ('20051107');
REPLACE INTO SAMPLE (dataset) VALUES ('20051108');

REPLACE INTO SAMPLED VALUES ('20051001', 20051001, 20051002,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-10-01%'' and ''2005-10-02%''');
REPLACE INTO SAMPLED VALUES ('20051003', 20051003, 20051004,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-10-03%'' and ''2005-10-04%''');
REPLACE INTO SAMPLED VALUES ('20051005', 20051005, 20051006,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-10-05%'' and ''2005-10-06%''');
REPLACE INTO SAMPLED VALUES ('20051024', 20051024, 20051024,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-10-24%'' and ''2005-10-24%''');
REPLACE INTO SAMPLED VALUES ('20051026', 20051026, 20051026,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-10-26%'' and ''2005-10-26%''');
REPLACE INTO SAMPLED VALUES ('20051028', 20051028, 20051028,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-10-28%'' and ''2005-10-28%''');
REPLACE INTO SAMPLED VALUES ('20051030', 20051030, 20051030,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-10-30%'' and ''2005-10-30%''');
/*REPLACE INTO SAMPLED VALUES ('20051107', 20051107, 20051108,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-11-07%'' and ''2005-11-08%''');*/
REPLACE INTO SAMPLED VALUES ('20051107', 20051107, 20051107,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-11-07%'' and ''2005-11-07%''');
REPLACE INTO SAMPLED VALUES ('20051108', 20051108, 20051108,
  'dtinstru=''mosaic_2'' and dtcaldat between ''2005-11-08%'' and ''2005-11-08%''');
