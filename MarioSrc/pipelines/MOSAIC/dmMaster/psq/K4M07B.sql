REPLACE INTO PSQ VALUES('K4M07B', 'K4M07B', 'K4M07BD', 'MOSAIC',
'dir', 'disabled', 'proctype=''Raw'' and dtinstru=''newfirm'' and obstype not in (''focus'',''test'')');

REPLACE INTO K4M07B (dataset) VALUES ('20070831');
REPLACE INTO K4M07B (dataset) VALUES ('20070904');
REPLACE INTO K4M07B (dataset) VALUES ('20070906');
REPLACE INTO K4M07B (dataset) VALUES ('20070911');
REPLACE INTO K4M07B (dataset) VALUES ('20070914');
REPLACE INTO K4M07B (dataset) VALUES ('20071001');
REPLACE INTO K4M07B (dataset) VALUES ('20071005');
REPLACE INTO K4M07B (dataset) VALUES ('20071012');
REPLACE INTO K4M07B (dataset) VALUES ('20071206');
REPLACE INTO K4M07B (dataset) VALUES ('20071207');
REPLACE INTO K4M07B (dataset) VALUES ('20071210');
REPLACE INTO K4M07B (dataset) VALUES ('20071213');
REPLACE INTO K4M07B (dataset) VALUES ('20071216');
REPLACE INTO K4M07B (dataset) VALUES ('20071218');

REPLACE INTO K4M07BD VALUES ('20070831', '20070831', '20070903',
  'dtcaldat between ''2007-08-31%'' and ''2007-09-03%''');
REPLACE INTO K4M07BD VALUES ('20070904', '20070904', '20070905',
  'dtcaldat between ''2007-09-04%'' and ''2007-09-05%''');
REPLACE INTO K4M07BD VALUES ('20070906', '20070906', '20070910',
  'dtcaldat between ''2007-09-06%'' and ''2007-09-10%''');
REPLACE INTO K4M07BD VALUES ('20070911', '20070911', '20070913',
  'dtcaldat between ''2007-09-11%'' and ''2007-09-13%''');
REPLACE INTO K4M07BD VALUES ('20070914', '20070914', '20070916',
  'dtcaldat between ''2007-09-14%'' and ''2007-09-16%''');
REPLACE INTO K4M07BD VALUES ('20071001', '20071001', '20071004',
  'dtcaldat between ''2007-10-01%'' and ''2007-10-04%''');
REPLACE INTO K4M07BD VALUES ('20071005', '20071005', '20071008',
  'dtcaldat between ''2007-10-05%'' and ''2007-10-08%''');
REPLACE INTO K4M07BD VALUES ('20071012', '20071012', '20071015',
  'dtcaldat between ''2007-10-12%'' and ''2007-10-15%''');
REPLACE INTO K4M07BD VALUES ('20071206', '20071206', '20071206',
  'dtcaldat between ''2007-12-06%'' and ''2007-12-06%''');
REPLACE INTO K4M07BD VALUES ('20071207', '20071207', '20071209',
  'dtcaldat between ''2007-12-07%'' and ''2007-12-09%''');
REPLACE INTO K4M07BD VALUES ('20071210', '20071210', '20071212',
  'dtcaldat between ''2007-12-10%'' and ''2007-12-12%''');
REPLACE INTO K4M07BD VALUES ('20071213', '20071213', '20071215',
  'dtcaldat between ''2007-12-13%'' and ''2007-12-15%''');
REPLACE INTO K4M07BD VALUES ('20071216', '20071216', '20071217',
  'dtcaldat between ''2007-12-16%'' and ''2007-12-17%''');
REPLACE INTO K4M07BD VALUES ('20071218', '20071218', '20071219',
  'dtcaldat between ''2007-12-18%'' and ''2007-12-19%''');
