-- addcol.sql
-- 
-- SQL script to add a column to the catalog table
-- This versions is needed for our old sqlite version because
-- it doesn't support ALTER TABLE.

BEGIN TRANSACTION;

CREATE TEMPORARY TABLE oldcatalog (
    CLASS       VARCHAR(9) NOT NULL, 
    MJDSTART    FLOAT NULL, 
    MJDEND      FLOAT NULL, 
    DETECTOR    VARCHAR(71) NULL, 
    IMAGEID     VARCHAR(71) NULL, 
    FILTER      VARCHAR(71) NULL, 
    EXPTIME     FLOAT NULL, 
    NCOMBINE	INT NULL,
    QUALITY	FLOAT NULL,
    DATATYPE    VARCHAR(9) NULL, 
    VALUE       VARCHAR(127) NULL
);

INSERT INTO oldcatalog SELECT * FROM catalog;

DROP TABLE catalog;
CREATE TABLE catalog (
    CLASS       VARCHAR(9) NOT NULL, 
    MJDSTART    FLOAT NULL, 
    MJDEND      FLOAT NULL, 
    DETECTOR    VARCHAR(71) NULL, 
    IMAGEID     VARCHAR(71) NULL, 
    FILTER      VARCHAR(71) NULL, 
    EXPTIME     FLOAT NULL, 
    NCOMBINE	INT NULL,
    QUALITY	FLOAT NULL,
    MATCH	VARCHAR(512) NULL,
    DATATYPE    VARCHAR(9) NULL, 
    VALUE       VARCHAR(127) NULL
);
CREATE INDEX MJDIndx ON catalog (MJDSTART);
CREATE INDEX ImageIdIndx ON catalog (IMAGEID);

INSERT INTO catalog (
    CLASS, 
    MJDSTART, 
    MJDEND, 
    DETECTOR, 
    IMAGEID, 
    FILTER, 
    EXPTIME, 
    NCOMBINE,
    QUALITY,
    DATATYPE, 
    VALUE
) SELECT * FROM oldcatalog;

DROP TABLE oldcatalog;
COMMIT;
