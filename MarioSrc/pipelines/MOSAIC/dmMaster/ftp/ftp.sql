/* SQL commands to (re-)init the FTP portion of the database */
/* The provides the mapping between DTPROPID and FTP account */

DROP TABLE IF EXISTS FTP;

CREATE TABLE FTP (
    first_name		char(16),
    last_name		char(16),
    proposal		char(16),
    ftp_name		char(16),
    ftp_password	char(16),
    ftp_dir		varchar(64),
    nvo_dir		varchar(64)
);

