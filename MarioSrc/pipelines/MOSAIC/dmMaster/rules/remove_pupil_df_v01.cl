procedure remove_pupil_df( tel, filter, amp )

struct tel {prompt='Give telescope'}
string filter {prompt='Give filter'}
int amp {prompt='Give amplifier'}

string version = "v01"

begin
    
    struct t
    string f
    int a, remove_pupil_df

    t = tel
    f = filter
    a = amp

    # Make removal the default
    remove_pupil_df = 1
    
    if (t=="KPNO 4.0 meter telescope") {
        if ( a==1 || a==4 || a==5 || a==8 ) {
            remove_pupil_df = 0
        } else {
            if (f=="U") { remove_pupil_df = 1 };
            if (f=="B") { remove_pupil_df = 1 };
            if (f=="V") { remove_pupil_df = 1 };
            if (f=="R") { remove_pupil_df = 1 };
            if (f=="I") { remove_pupil_df = 2 };
        }
    }
    ;

    if (t=="CTIO 4.0 meter telescope") {
        remove_pupil_df = 0
    }
    ;

    print( remove_pupil_df )
    
end
