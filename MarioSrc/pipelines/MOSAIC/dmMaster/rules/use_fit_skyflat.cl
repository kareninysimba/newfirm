procedure use_fit_skyflat_v01( btype, snr )

string btype {prompt='Give band type'}
real snr {prompt='Give SNR in sky flat'}

string version = "v01"

begin
   
    string b
    real s

    b = btype
    s = snr

    # SNR cutoff should be determined from real data, set to 
    # 50 for now. This should result in much smaller uncertainties
    # in the photometry because many pixels will be averaged then.
    if ( b=="narrow" || s<50 ) {
       print "1"
    } else {
       print "0"
    }

end
