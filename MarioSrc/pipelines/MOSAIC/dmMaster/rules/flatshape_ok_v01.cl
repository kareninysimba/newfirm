procedure flatshape_ok_v01 (immap, refmap)

file	refmap			{prompt="Reference map"}
file	immap			{prompt="Image map"}
string	logstr			{prompt="Log string"}

begin
	int	result
	file	tmp, ref
	real	stddev
	struct	str

	tmp = mktemp ("fshape")

	result = 0
	if (refmap != "") {
	    imexpr ("a/b", tmp, immap, refmap, verb-)
	    mimstat (tmp, imask="!BPM", fields="stddev", format-) |
	        scan (stddev)
	    imdel (tmp)
	    if (stddev < 0.01)
	        result = 1
	    if (strldx ("/", refmap) > 0)
		ref = substr (refmap, strldx("/",refmap)+1, 1000)
	    else
	        ref = refmap
	    printf ("refmap=%s immap=%s stddev=%.3f\n",
	        ref, immap, stddev) | scan (str)
	} else {
	    mimstat (immap, imask="!BPM", fields="stddev", format-) |
	        scan (stddev)
	    if (stddev < 0.1)
	        result = 1
	    printf ("immap=%s stddev=%.3f\n", immap, stddev) | scan (str)
	}

	logstr = str
	print (result)
end
