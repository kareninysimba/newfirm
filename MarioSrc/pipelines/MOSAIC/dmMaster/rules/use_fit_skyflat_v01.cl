procedure use_fit_skyflat_v01( btype, snr )

string btype {prompt='Give band type'}
real snr {prompt='Give SNR in sky flat'}

string version = "v01"

begin
   
    string b
    real s

    b = btype
    s = snr

    # snr cutoff needs to be determined first, set to 0 for
    # now to disable
    if ( b=="narrow" || s<0 ) {
       print "1"
    } else {
       print "0"
    }

end
