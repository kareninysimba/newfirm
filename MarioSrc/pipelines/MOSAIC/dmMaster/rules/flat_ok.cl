procedure flat_ok_v01( amp, filter, mean, saturate, fracsat, bandtype, flattype )

int amp {prompt='Amplifier number'}
string filter {prompt='Filter name'}
real mean {prompt='Average value'}
real saturate {prompt='Saturation level'}
real fracsat {prompt='Fraction of saturated pixels'}
string bandtype {prompt='Band type'}
string flattype {prompt='Flat type'}

begin

    int a
    string b,f,t
    real m,r,s
    real mthresh,rthresh

    a = amp
    b = bandtype
    f = filter
    m = mean
    r = fracsat
    s = saturate
    t = flattype

    # Current rule is for all amps and all filters
    mthresh = 2000
    rthresh = 0.0001
    if (b=='narrow') {
        mthresh = 1000
    }
    ;
    if (t=='twilight') {
	mthresh = 1000
	rthresh = 0.005
    }
    ;

    if ((m>mthresh)&&(m<0.9*s)&&(r<rthresh)) {
        print "1"
    } else {
        print "0"
    }
    ;

end

