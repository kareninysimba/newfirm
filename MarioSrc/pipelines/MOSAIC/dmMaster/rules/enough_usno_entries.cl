procedure enough_usno_entries_v01 (numstars)

int	numstars	{prompt="Number of USNO stars in match catalog"}

begin
    int	n

    n = numstars

    if (n>=1) 
        print ("1")
    else
        print ("0")
end
