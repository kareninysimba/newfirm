procedure bias_ok_v01( amp, noise, mean, jumpprob )

int amp {prompt='Give amplifier'}
real noise {prompt='Noise (in units of expected sigma)'}
real mean {prompt='Average divided by noise'}
real jumpprob {prompt='Probability image contains a bias jump'}

begin

    int a
    real m,n,j

    a = amp
    m = mean
    n = noise
    j = jumpprob

    # Current rule is for all amps 

    if ((n>0.25)&&(n<4)&&(abs(m)<3)) {
        print "1"
    } else {
        print "0"
    }
    ;

end
