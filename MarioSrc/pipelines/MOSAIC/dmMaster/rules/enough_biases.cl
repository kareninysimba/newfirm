procedure enough_biases_v01 (numbias)

int	numbias		{prompt="Number of bias images"}

begin
	int	n

	n = numbias

	if (n>=10)
	    print ("1")
	 else
	    print ("0")
end
