procedure ovscn_subtract_method_v01(exptime)

real	exptime
string version = "v01"

begin
    # Set the overscan subtraction method
    # 1 - subtract the average of the overscan from the image
    # 2 - subtract a higher order polynomial
    # 3 - subtract the average of each line in the overscan from each line
    #     in the image
    # 4 - do 3 if a bias jump is detected, 1 for the rest
    # 5 - do 3 if a bias jump is detected, 2 for the rest

    if (isindef(exptime))
        print 3
    else if (exptime > sft_minexptime)
        print 3
    else
	print 5
end
