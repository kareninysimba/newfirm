procedure enough_for_putcal_v01 (obstype, numimg)

string	obstype	{prompt="obstype"}
int	numimg	{prompt="Number of bias images"}

begin
	int	n
	string	o

	n = numimg
	o = obstype

	if (n>=3)
	    print ("1")
	else
	    print ("0")
end
