<?xml version="1.0" encoding="utf-8" standalone="no" ?>
<!DOCTYPE Pipeline SYSTEM "NHPPS.dtd">
<Pipeline system="MOSAIC" name="mdc" poll="0.1">
  <Description>
	The MDC pipeline receives stacked-SIFs that make up a single
	dither sequence of MOSAIC images that have been dark-corrected and
	flat-fielded but not sky subtracted.  This pipeline combines these
	stacked-SIFs to form a composite image, masking the bad pixels
	identified in the bpm files associated with the input stacked-SIFs.
  </Description>

  <Module name="mdcstart">
    <Description>
	Start up the MDC pipeline
    </Description>
    <Trigger>
      <FileRequirement fnPattern="*.mdctrig"/>
    </Trigger>
    <PreProcAction>
      <RenameTrigger argv="$EVENT_NAME.mdctrig $EVENT_NAME.mdcproc"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="start.cl"/>
    </ProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <RemoveTrigger argv="$EVENT_NAME.mdcproc"/>
      <OSFUpdate argv="mdcstart p"/>
    </PostProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <RenameTrigger argv="$EVENT_NAME.mdcproc $EVENT_NAME.mdcerr"/>
      <OSFUpdate argv="${self.name} e"/>
    </PostProcAction>
  </Module>

  <Module name="mdcsetup1">
    <Description>
	Prepare for processing.  Copy badpixel masks to datadir.
    </Description>
    <Trigger>
      <OSFRequirement argv="mdcstart = p"/>
      <OSFRequirement argv="${self.name} = _"/>
    </Trigger>
    <PreProcAction>
      <OSFUpdate argv="${self.name} p"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="mdcsetup1.cl"/>
    </ProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <OSFUpdate argv="${self.name} e"/>
    </PostProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <OSFUpdate argv="${self.name} c"/>
    </PostProcAction>
    <PostProcAction>
      <ExitCode val="2"/>
      <OSFUpdate argv="${self.name} c"/>
      <OSFUpdate argv="mdcstack1 c"/>
      <OSFUpdate argv="mdcgrp c"/>
      <OSFUpdate argv="mdc2ctrC c"/>
      <OSFUpdate argv="mdc2ctrR c"/>
      <OSFUpdate argv="mdcsetup2 c"/>     
    </PostProcAction>
  </Module>

  <Module name="mdcstack1">
    <Description>
	The MDCstack1 module receives the resampled SIFs stacked-SIFs that
	make up a sequence of MOSAIC images that have been dark-corrected
	and flat-fielded but not sky subtracted.  This pipeline combines
	these resampled SIFs to form a composite image.  This composite
	image represents a "harsh stack" since the input resampled
	SIFs are combined using "median", and the resulting composite
	image should be free of cosmic rays, artifacts, and transits.
	This composite image is used only for removing cosmic rays,
	artifacts, and transits in the non-resampled SIFs.
    </Description>
    <Trigger>
      <OSFRequirement argv="mdcsetup1 = c"/>
      <OSFRequirement argv="${self.name} = _"/>
    </Trigger>
    <MaxExec time="0:12:0:0" />
    <PreProcAction>
      <OSFUpdate argv="${self.name} p"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="mdcstack1.cl"/>
    </ProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <OSFUpdate argv="${self.name} c"/>
    </PostProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <ExitCode val="2"/>
      <OSFUpdate argv="${self.name} e"/>
    </PostProcAction>
  </Module>

  <Module name="mdcgrp">
    <Description>
	This module prepares the lists (one for each extension) and list
	of lists for call to the CTR pipeline.
    </Description>
    <Trigger>
      <OSFRequirement argv="mdcstack1 = c"/>
      <OSFRequirement argv="${self.name} = _"/>
    </Trigger>
    <PreProcAction>
      <OSFUpdate argv="${self.name} p"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="mdcgrp.cl"/>
    </ProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <OSFUpdate argv="${self.name} c"/>
    </PostProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="2"/>
      <ExitCode val="11"/>
      <OSFUpdate argv="${self.name} e"/>
    </PostProcAction>
  </Module>
  
  <Module name="mdc2ctrC">
    <Description>
    	Call the CTR pipeline.
    </Description>
    <Trigger>
      <OSFRequirement argv="mdcgrp = c"/>
      <OSFRequirement argv="${self.name} = _"/>
    </Trigger>
    <PreProcAction>
      <OSFUpdate argv="${self.name} p"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="call.cl mdc ctr split mdcgrpALL.list 2"/>
    </ProcAction>
    <PostProcAction>
      <ExitCode val="2"/>
      <OSFUpdate argv="${self.name} c"/>
      <OSFUpdate argv="mdc2ctrR w"/>
    </PostProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <OSFUpdate argv="${self.name} c"/>
      <OSFUpdate argv="mdc2ctrR c"/>
    </PostProcAction>
    <PostProcAction>
      <ExitCode val="3"/>
     <OSFUpdate argv="${self.name} c"/>
      <OSFUpdate argv="mdcstart c"/>
    </PostProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <OSFUpdate argv="${self.name} e"/>
      <OSFClose/>
    </PostProcAction>
  </Module>

  <Module name="mdc2ctrR" maxActive="1">
    <Description>
        Return to MDC from the CTR pipeline.
    </Description>
    <Trigger>
      <FileRequirement fnPattern="*.ctrtrig"/>
    </Trigger>
    <PreProcAction>
      <RenameTrigger argv="$EVENT_NAME.ctrtrig $EVENT_NAME.ctrproc"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="return.cl mdc ctr INDEX(mdcstart) INDEX(mdc2ctrR) warn INDEF yes"/>
    </ProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <RemoveTrigger argv="$EVENT_NAME.ctrproc"/>
    </PostProcAction>
    <PostProcAction>
      <ExitCode val="2"/>
      <RemoveTrigger argv="$EVENT_NAME.ctrproc"/>
      <OSFUpdateParent argv="${self.name} c"/>
    </PostProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <RenameTrigger argv="$EVENT_NAME.ctrproc $EVENT_NAME.ctrerr"/>
    </PostProcAction>
  </Module>
  
  <Module name="mdcsetup2">
    <Description>
        Prepare for processing.  Copy badpixel masks to datadir.
    </Description>
    <Trigger>
      <OSFRequirement argv="mdc2ctrR = c"/>
      <OSFRequirement argv="${self.name} = _"/>
    </Trigger>
    <PreProcAction>
      <OSFUpdate argv="${self.name} p"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="mdcsetup2.cl"/>
    </ProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <OSFUpdate argv="${self.name} e"/>
    </PostProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <OSFUpdate argv="${self.name} c"/>
    </PostProcAction>
  </Module>

  <Module name="mdcstack2">
    <Description>
	The MDCstack module receives stacked-SIFs that make up a single
	dither sequence of MOSAIC images that have been dark-corrected and
	flat-fielded but not sky subtracted.  This pipeline combines these
	stacked-SIFs to form a composite image, masking the bad pixels
	identified in the bpm files associated with the input stacked-SIFs.
    </Description>
    <Trigger>
      <OSFRequirement argv="mdcsetup2 = c"/>
      <OSFRequirement argv="${self.name} = _"/>
    </Trigger>
    <MaxExec time="0:12:0:0" />
    <PreProcAction>
      <OSFUpdate argv="${self.name} p"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="mdcstack2.cl"/>
    </ProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <OSFUpdate argv="${self.name} c"/>
    </PostProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <ExitCode val="2"/>
      <OSFUpdate argv="${self.name} e"/>
    </PostProcAction>
  </Module>

  <Module name="mdcmdp">
    <Description>
    	Create the final data products.
    </Description>
    <Trigger>
      <OSFRequirement argv="mdcstack2 = c"/>
      <OSFRequirement argv="${self.name} = _"/>
    </Trigger>
    <PreProcAction>
      <OSFUpdate argv="${self.name} p"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="mdcmdp.cl"/>
    </ProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <OSFUpdate argv="${self.name} c"/>
    </PostProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <ExitCode val="2"/>
      <OSFUpdate argv="${self.name} e"/>
    </PostProcAction>
  </Module>

  <Module name="mdcdone">
    <Description>
        Finish MDC Pipeline
    </Description>
    <Trigger>
      <OSFRequirement argv="mdcmdp = c"/>
      <OSFRequirement argv="${self.name} = _"/>
    </Trigger>
    <PreProcAction>
      <OSFUpdate argv="${self.name} p"/>
    </PreProcAction>
    <ProcAction>
      <Foreign argv="done.cl .mdc MDCDONE no"/>
    </ProcAction>
    <PostProcAction>
      <ExitCode val="1"/>
      <OSFUpdate argv="${self.name} d"/>
      <OSFUpdate argv="mdcstart c"/>
      <OSFConditRemove/>
    </PostProcAction>
    <PostProcAction isFailure="true">
      <ExitCode val="0"/>
      <ExitCode val="11"/>
      <OSFUpdate argv="${self.name} e"/>
      <OSFClose/>
    </PostProcAction>
  </Module>
</Pipeline>
