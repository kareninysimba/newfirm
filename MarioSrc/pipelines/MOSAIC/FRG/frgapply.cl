#!/bin/env pipecl
#
# FRGAPPLY -- Apply fringe information and set files to call SFG pipeline.

real	quality = -1
string	dataset, datadir, ifile, lfile, frg
real	scale, mjd

# Packages and task.
images

# Set paths and files.
frgnames (envget("OSF_DATASET"))
dataset = frgnames.dataset
datadir = frgnames.datadir
ifile = frgnames.indir // dataset // ".frg"
lfile = datadir // frgnames.lfile
set (uparm = frgnames.uparm)
cd (datadir)

# Log start of processing.
printf ("\nFRGAPPLY (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Determine if a calibration was computed and its quality.
# To avoid a call to the DM we put the fringe template in the local MC.
# The SFG pipeline is likely to be running on the same machine.

if (imaccess(frgnames.frg)) {
    hselect (frgnames.frg, "quality", yes) | scan (quality)
    # To avoid a call to the DM put the fringe template in MC.
    if (quality >= 0.) {
	if (imaccess("MC$"//frgnames.frg)==YES)
	    sendmsg ("WARNING", "Calibration already in MarioCal",
	        frgnames.frg, "CAL")
	else
	    imcopy (frgnames.frg, "MC$", verb+)
	frgnames.frg = "MC$" // frgnames.frg
    }
}
;

# Loop over the input files and set things for the SFG pipeline.
list = ifile
while (fscan (list, s1) != EOF) {
    # Extract the image dataset name.
    s2 = substr (s1, strldx("/",s1)+1, strldx("-",s1)-1)
    s2 = substr (s2, strldx("-",s2)+1,1000)

    # Set the fringe template if computed.
    if (quality >= 0.)
	hedit (s1, "FRINGE", frgnames.frg, add+)
    ;

    # Set the scale if computed.
    if (access(dataset//".scl")) {
        scale = 1.; match (s2, dataset//".scl") | scan (s3, scale)
	hedit (s1, "FRGSCALE", scale, add+)
    }
    ;

    # Set output files.
    s2 = "frg2sfg_" // s2 // ".tmp"
    path (s2, >> "frg2sfg.tmp")
    print (s1, >> s2)
}
list = ""

logout 1
