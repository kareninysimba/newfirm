#!/bin/env pipecl
#
# FRGSCALE -- Compute scale factors.

int	status = 1
real	quality = -1
string	dataset, datadir, ifile, lfile, sfile, ftrname, frg
real	scale, statwt
file	caldir = "MC$"

# Packages and task.
task $pipeselect = "$!pipeselect $1 $2 0 0"
task $osf_test = "$!osf_test -a $2 -p ftr -f $1"
images
servers
mscred
task patfit = "mscsrc$x_mscred.e"
cache mscred

# Set paths and files.
frgnames (envget("OSF_DATASET"))
daynames (frgnames.parent)
ftrname = daynames.parent
dataset = frgnames.dataset
datadir = frgnames.datadir
ifile = frgnames.indir // dataset // ".frg"
sfile = dataset // ".scl"
lfile = datadir // frgnames.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = frgnames.uparm)
cd (datadir)

# Log start of processing.
printf ("\nFRGSCALE (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Determine if calibration exists and its quality.
if (imaccess(frgnames.frg))
    hselect (frgnames.frg, "quality", yes) | scan (quality)
;

# Compute scale factors.
scale = 1.; statwt = 1.
list = ifile
while (fscan (list, s1) != EOF) {
    # Set fringe image.
    if (quality >= 0.)
	frg = frgnames.frg
    else
        frg = ""
    hselect (s1, "FRINGE", yes) | scan (frg)
    if (imaccess(frgnames.frg) == NO) {
	getcal (s1, "fringe", cm, caldir, obstype="",
	    detector=cl.instrument, imageid="!ccdname", filter="!filter",
	    exptime="", mjd="!mjd-obs", dmjd=cal_dmjd, quality=0.,
	    match="!ccdsum") | scan (i, line)
	if (i == 0)
	    hselect (s1, "FRINGE", yes) | scan (frg)
	else
	    sendmsg ("WARNING", "No library fringe template to apply",
		substr(s1,strldx("/",s1)+1,1000), "CAL")
    }
    if (imaccess(frg) == NO)
        next
    ;

    if (frg_scale != "none") {
	s2 = "!SKYMEAN"; hselect (s1, "SKYIM", yes) | scan (s2)
	if (s2 != "!SKYMEAN") {
	    s3 = ""; match (s2, ifile//"sky") | scan (s3)
	    if (s3 == "") {
		sendmsg ("ERROR", "Could not find sky file", s3, "VRFY")
		status = 0
		break
	    }
	    iferr {
		imcopy (s3, s2)
	    } then {
	        sleep 1
		imcopy (s3, s2)
	    } else
	        ;
	}
	;

	patfit (s1, "", frg, masks="!objmask", patmasks="", background=s2,
	    bkgpattern="0.", bkgweight="", outtype="none", ncblk=5, nlblk=5,
	    logname="FRGSCALE", logfile=lfile, verbose+, > "frgscale.tmp")

	if (imaccess(s2))
	    imdelete (s2)
	;

	match ("scale", "frgscale.tmp") | scanf ("  scale = %g", scale)
	match ("statwt", "frgscale.tmp") | scanf ("  statwt = %g", statwt)
	delete ("frgscale.tmp")

	# Place reality checks on the scale.
	if (scale < 0. || scale > 3.) {
	    scale = max (0., min (3., scale))
	    statwt = statwt / 4.
	}
	;
    }
    ;

    s2 = substr (s1, strldx("/",s1)+1, 1000)
    s2 = substr (s2, 1, strldx("-",s2)-1)
    s2 = substr (s2, strldx("-",s2)+1, 1000)
    printf ("%s %.2f %.4g\n", s2, scale, statwt, >> sfile)
}
list = ""

if (frg_scale != "global")
    logout 1
;

# Trigger ftr pipeline to compute global scales.

# Find pipeline.
pipeselect (envget ("NHPPS_SYS_NAME"), "ftr", > "frgscale.tmp")
count ("frgscale.tmp") | scan (i)
s1 = ""
if (i == 1)
    head ("frgscale.tmp") | scan (s1)
else if (i > 1) {
    osf_test (ftrname, envget ("NHPPS_SYS_NAME")) | scan (s2)
    s2 = substr (s2, strldx(":",s2)+1, 1000)
    if (stridx("_",s2) > 0)
	s2 = substr (s2, 1, strldx("_",s2)-1)
    match (s2, "frgscale.tmp") | scan (s1)
}
;
delete ("frgscale.tmp")

# Trigger pipeline.
if (s1 != "") {
    s2 = s1 // ftrname // "-" // daynames.child // ".frg"
    if (access(sfile)) {
	pathname (sfile, >> sfile)
	copy (sfile, s2)
	delete (sfile)
	status = 2
    } else {
        touch (s2)
	status = 1
    }
    touch (s2//"trig")
} else
    status = 0

logout (status)
