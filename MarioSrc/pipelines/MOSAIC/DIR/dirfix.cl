# DIRFIX -- Template for header fixing.

procedure dirfix (dataset, ifile, lfile)

string	dataset			{prompt="Dataset Name"}
file	ifile			{prompt="Input file"}
file	lfile			{prompt="Log file"}
int	status = 1		{prompt="Return status"}

struct	*fd

begin
	file	im

	status = 0

	# Log start of processing.
	printf ("\n%s (%s): ", "dirfix", dataset)
	time

	# Add your own code here.  This shows a simple example.
#	printf ("Changing DTPROPID to PLTEST...\n")
#
#	fd = ifile
#	while (fscan (fd, im) != EOF) {
#	    hedit (im//"[0]", "DTPROPID", "PLTEST", verify-, show+)
#	}
#	fd = ""

	status = 1
end
