#!/bin/env pipecl
#
# DIRCLEANUP -- Clean up staged files.

logout 1

string  dataset
file	indir, datadir, ilist, lfile

# Set tasks and packages.
task	$psqstatus = "$!psqstatus"

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir // dataset // ".ds"
lfile = datadir // names.lfile
if (access (ilist) == NO)
    logout 1
;
cd (datadir)

# Log start of processing.
printf ("\nDIRCLEANUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean up staged files.
s1 = ""; psqstatus (dataset) | scan (s1)
if (s1=="resubmit" || s1=="pending") {
    if (access ("Saved") == NO) {
        mkdir ("Saved")
	sleep (1)
    }
    ;
    move ("@"//ilist, "Saved")
} else
    ;
    #delete ("@"//ilist)

# Remove tmp files from data directory.
delete ("*.tmp")

# Remove any DUPOBSID files.
delete ("DUPOBSID*")

# Remove files from indir.  This is also done in the done stage but
# we can call this module for error recover.
cd (indir)
delete (dataset//".*")

logout 1
