#!/bin/env pipecl
#
# DIRGROUP -- Group by filter dependent and independent observation types.
# Within the filter dependent types break groups up by night but require
# a minimum number per night otherwise combine nights
#
# This routine depends on the variable cl.instrument, keywords filter,
# mjd-obs, and observat as well as the observation type and filter
# translations given by the files in "pipedata".

int	ljd, l
real	exptime
string	dataset, dataset1, id, indir, datadir
string	inlist, lfile, zlist, flist, olist, slist
string	fname, sname, sname1, sname2
struct	*fd, filter, ccdsum

# Define packages and tasks.
util
proto
images
noao
servers
mscred
task	ccdproc		= "mscsrc$ccdproc.cl"
task    _ccdlist        = "mscsrc$x_ccdred.e"
cache	mscred, observatory

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
dataset1 = dataset
if (strldx("_",dataset1)>0)
    dataset1 = substr (dataset1, 1, strldx("_",dataset1)-1)
;
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".dir"
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
mscred.ssfile = "pipedata$subsets.dat"
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nDIRGROUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean up.
delete ("*[.zfos]tmp")

# Append "[1]" to all files in the list.
list = inlist
while (fscan( list, fname) != EOF)
    print (fname//"[1]", >> "dirgroup.tmp")

# Go through the input list and identify the types.
_ccdlist ("@dirgroup.tmp")
_ccdlist( "@dirgroup.tmp", names=yes, ccdtype="zero", >> "z.tmp" )
_ccdlist( "@dirgroup.tmp", names=yes, ccdtype="dark", >> "z.tmp" )
_ccdlist( "@dirgroup.tmp", names=yes, ccdtype="flat", >> "f.tmp" )
_ccdlist( "@dirgroup.tmp", names=yes, ccdtype="object", >> "o.tmp" )
delete ("dirgroup.tmp")

# Create zero list.
list = "z.tmp"
while (fscan( list, fname) != EOF) {
    fname = substr (fname, 1, strldx("[",fname)-1)
    printf ("%s -> %s\n", fname, dataset//"_"//"Zero.ztmp", >> lfile)
    print (fname, >> dataset//"_"//"Zero.ztmp")
}
list = ""; delete ("z.tmp")

# Get the filters for the dependent types.
hselect ("@f.tmp", "$I,$filter", yes) | sort (col=2, >> "dirgroup.tmp")
delete ("f.tmp")

# Separate into filter lists.
s2 = ""
list = "dirgroup.tmp"
while (fscan (list, fname, filter) != EOF) {

    # Setup for new filter.
    if (filter != s2) {
	# First make sure this filter has an associated CALOPS.
	s2 = filter; sname = ""
	getcal ("", "calops", cm, "", obstype="", detector=cl.instrument,
	    imageid="", filter=filter, exptime="", mjd="50000") | scan (s1)
	if (s1 != "CALOPS") {
	    sendmsg ("ERROR", "CALOPS for filter not found", filter, "VRFY")
	    next
	}
	;

	# Get the filter abbreviation.
	match (filter, mscred.ssfile) | fields ("STDIN", "2") |
	    translit ("STDIN", "_", del+, col+) | scan (sname)
	if (sname == "") {
	    sendmsg ("ERROR", "Filter not in subset file", filter, "VRFY")
	    next
	}
	;
    }
    ;
    if (sname == "")
        next
    ;

    # Add to filter file.
    fname = substr (fname, 1, stridx("[",fname)-1)
    printf ("%s -> %s\n", fname, dataset//"_"//sname//"F.ftmp", >> lfile)
    print (fname, >> dataset//"_"//sname//"F.ftmp")
}
list = ""; delete ("dirgroup.tmp")

# Get the filters for the dependent types.
hselect ("@o.tmp", "$I,$mjd-obs,$exptime,$filter,$ccdsum", yes) |
    sort (col=4) | words (>> "dirgroup.tmp")
delete ("o.tmp")

# Set default observatory.
observatory.observatory = ""
observatory.timezone = 0

# Separate into filter lists.
s2 = ""; touch ("dirgroup1.tmp")
list = "dirgroup.tmp"
while (fscan (list, fname, x, exptime, filter) != EOF) {
    if (fscan (list, x) == EOF)
        break
    ;
    if (fscan (list, exptime) == EOF)
        break
    ;
    if (fscan (list, filter) == EOF)
        break
    ;
    if (fscan (list, ccdsum) == EOF)
        break
    ;
    if (isindef(exptime))
        next
    ;

    if (ccdsum == "INDEF" || ccdsum == "1 1")
        ccdsum = ""
    else
        print ("b", ccdsum) | translit ("STDIN", " ", del+, col+) |
	    scan (ccdsum)

    # Setup for new filter.
    if (filter//ccdsum != s2) {
	# First make sure this filter has an associated CALOPS.
	s2 = filter//ccdsum; sname = ""
	getcal ("", "calops", cm, "", obstype="", detector=cl.instrument,
	    imageid="", filter=filter, exptime="", mjd="50000") | scan (s1)
	if (s1 != "CALOPS") {
	    sendmsg ("ERROR", "CALOPS for filter not found", filter, "VRFY")
	    next
	}
	;

	# Get the filter abbreviation.
	match (filter, mscred.ssfile) | fields ("STDIN", "2") |
	    translit ("STDIN", "_", del+, col+) | scan (sname)
	if (sname == "") {
	    sendmsg ("ERROR", "Filter not in subset file", filter, "VRFY")
	    next
	}
	;

	# Add to output filter list.
	sname += ccdsum
	print ("dirgroup_" // sname // ".tmp", >> "dirgroup1.tmp")
    }
    ;
    if (sname == "")
        next
    ;

    # Set time zone from observatory and then compute LJD.
    if (observatory.observatory == "") {
        s1 = ""; hselect (fname, "observat", yes) | scan (s1)
	if (s1 != "")
	    observatory ("set", s1)
	;
    }
    ;
    ljd = int (x - observatory.timezone/24. - 0.5)

    # Add to filter file.
    fname = substr (fname, 1, stridx("[",fname)-1)
    printf ("%d %f %s\n", ljd, exptime, fname, >> "dirgroup_"//sname//".tmp")
}
list = ""; delete ("dirgroup.tmp")

# Separate into night lists and short exposure list if possible.
# This will check for the night list from a previous processing run.
# The logic is fragile if there is a mixture of previously
# grouped and not grouped.

list = "dirgroup1.tmp"
while (fscan (list, s1) != EOF) {
    sname = substr (s1, 10, strlen(s1)-4)
    sort (s1, > "dirgroup.tmp"); rename ("dirgroup.tmp", s1)
    s3 = ""; j = 0; k = 0
    fd = s1
    for (i=0; fscan(fd,ljd,exptime,fname)!=EOF; i+=1) {

	# Check for previous grouping.
	id = dataset1 // "_" // substr (fname, strldx("/",fname)+1,999)
	if (strstr(".fits",id)>0)
	    id = substr (id, 1, strstr(".fits",id)-1)
	getkeyval (dm, "obsdataproduct", id, "URL") | scan (sname2)
	#printf ("getkeyval (%s) = '%s'\n", id, sname2)
	if (sname2 != "None") {
	    if (sname2 == sname // "S") {
		if (dir_mingroup > 999)
		    s2 = dataset // "_" // sname // "AS.stmp"
		else
		    s2 = dataset // "_" // sname2 // ".stmp"
	    } else {
		if (dir_mingroup > 999)
		    s2 = dataset // "_" // sname // "A.otmp"
		else
		    s2 = dataset // "_" // sname2 // ".otmp"
	    }
	    printf ("Previous: %s -> %s\n", fname, s2, >> lfile)
	    print (fname, >> s2)
	    i -= 1
	    next
	}
	;

        if (exptime < sft_minexptime) {
	    sname2 = sname // "S"
	    setkeyval (dm, "dataproduct", id, "URL", sname2)
	    s2 = dataset // "_" // sname2 // ".stmp"
	    printf ("%s -> %s\n", fname, s2, >> lfile)
	    print (fname, >> s2)
	    i -= 1
	    next
	}
	;

	# Check for new night.
        if (ljd != k) {
	    if (i > dir_mingroup) {
		i = 0
		j += 1
	    }
	    ;
	    if (j < 1)
		sname1 = sname
	    else
	        sname1 = sname // j
	    k = ljd
	}
	;

	setkeyval (dm, "dataproduct", id, "URL", sname1)
	s3 = dataset // "_" // sname1 // ".otmp"
	printf ("%s -> %s\n", fname, s3, >> lfile)
	print (fname, >> s3)
    }
    fd = ""

    # Merge lists if needed.
    if (j > 0) {
        count (s3) | scan (l)
        if (i < dir_mingroup || l < dir_mingroup / 2) {
	    j -= 1
	    if (j < 1)
		sname1 = sname
	    else
		sname1 = sname // j
	    s2 = dataset // "_" // sname1 // ".otmp"
	    printf ("Merge %s -> %s\n", s3, s2, >> lfile)
	    concat (s3, s2, append+)
	    delete (s3)
	}
    }
    ;

    delete (s1)
}
list = ""; delete ("dirgroup1.tmp")

# Create lists of lists for zeros, flats, long exposures, and short exposures.

zlist = "zlist.tmp"
flist = "flist.tmp"
olist = "olist.tmp"
slist = "slist.tmp"

pathnames ("*.ztmp", sort+, > zlist)
count (zlist) | scan (i)
if (i == 0)
    delete (zlist)
;

pathnames ("*.ftmp", sort+, > flist)
count (flist) | scan (i)
if (i == 0)
    delete (flist)
;

pathnames ("*.otmp", sort+, > olist)
count (olist) | scan (i)
if (i == 0)
    delete (olist)
;

pathnames ("*.stmp", sort+, > slist)
count (slist) | scan (i)
if (i == 0)
    delete (slist)
;

logout 1
