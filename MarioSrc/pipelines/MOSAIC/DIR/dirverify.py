#!/bin/env python
#
# Verify the header and correct where possible

# Import python modules
import datetime
import os
import sys
import time
import pyfits as p
import re
import sqlite
from sqlutil import *
from sendmsg import *
from remediate import *

# Uncomment to exit with Z flag for debugging.
#sys.exit(99)

pipeApp = os.environ ['NHPPS_SYS_NAME']

# Set filenames and directories
dataset = os.environ['OSF_DATASET']
indir   = os.path.join (
    os.environ ['NHPPS_DATA'],
    pipeApp,
    '%s_DIR' % (pipeApp),
    'input') + os.path.sep
datadir = os.path.join (
    os.environ ['NHPPS_DATA'],
    pipeApp,
    '%s_DIR' % (pipeApp),
    'data',
    dataset) + os.path.sep

# Set up message monitor
msghost = os.environ['NHPPS_NODE_MM']
msgport = os.environ['NHPPS_PORT_MM']

# Get the node name
thishost = os.environ ['NHPPS_NODE_NAME']

# Set up connection to the data manager
dmhost = os.environ['NHPPS_NODE_DM']
dmport = os.environ['NHPPS_PORT_DM']
m = re.search( '(\d+)', dmport )
try:
    dmport = int(m.group(1))
except:
    sendmsg( 'ERROR', 'NHPPS_PORT_DM could not be set', '', 'VRFY', dataset )
    sys.exit(1)

# Precompile search on whitespace
onwhitespace = re.compile('\s+')

# Load global variables for the pipeline
# TODO: also read dataset specific variables, and make this a loadable module
# A first attempt at this has been made, but the use of 'exec' makes things a
# a little more difficult, because apparently the exec is executed in a
# different name space. Something like
# exec 'verbose = 1' in modutil.__dict__
# in a file named test.py that imports modutil
# where modutil.py contains the initial definition of verbose and the code
# below as a function does seem to do the trick if used in the calling
# script (i.e., test.py), but I have not figured out which namespace(s) to
# supply to the exec in modutil.py.
# Note that the code below is now also used in remediate.py. This was the
# main reason for trying to get this sorted out now, so we can avoid the
# duplicate code.

pipeGlobals = os.path.join (
    os.environ ['NHPPS_PIPESRC'],
    pipeApp, 'config', '%s.cl' % (pipeApp))

globals = file (pipeGlobals, 'r')

for line in globals.readlines():
    thisline = line.strip()
    fields = onwhitespace.split(thisline)
    if (len(fields)>=5):
        if (fields[2]=='='):
            # Retrieve the argument (between the '=' and the '{')
            m = re.search( '=\s+(.*)\s*\{', thisline )
            thisvalue = m.group(1)
            if (fields[3]=='yes'): thisvalue='1'
            if (fields[3]=='no'): thisvalue='0'
            if (fields[3]=='INDEF'): thisvalue='"INDEF"'
            global verbose
            exec fields[1]+'='+thisvalue

# Wrapper function around send_message
def sendmsg ( status, message, submsg, id, dset ):
    omsg = message
    if (len(submsg)>0):
        message += ' ('+submsg+')'
    send_message( msghost, int(msgport), message, id, dset,
                  thishost, status, 'DIRVERIFY', False )
    return omsg

# Function for logging rejected files
def logrejects( infile, msg ):
    global datasetok
    print >> rejects, '%s:\t%s' % ( infile, msg )
    datasetok = 0

# Function to check the header
def checkheader( infile, thishdu, keys, fixexpr, fixmeth, thislevel, subset, shortname ):
    thisheader = thishdu[subset].header
    if subset==0:
        if (verbose): print "Checking top level",
    else:
        if (verbose): print "Checking subset %d" % ( subset ),
    # Initialize lists
    keysnotfound = []
    thesekeys = []
    theseexpr = []
    thesemeth = []
    subdel = []
    # Copy keys (which come from input dictionary) to thesekeys
    for key in keys:
        thesekeys.append( key )
    for this in fixexpr:
        theseexpr.append( this )
    for this in fixmeth:
        thesemeth.append( this )
    # Initialize list to contain keywords that could not properly be remediated
    problemkeys = []
    # Loop over all keywords in this header
    for card in thisheader.ascardlist():
        # Append a string to keyword to distinguish top and subset
        # level keywords
        keyname = card.key+'-'+thislevel
        # For each keyword that is longer than 2 characters (keywords
        # that are 2 charachters long only contain the appended
        # string, so they were empty to begin with and therefore
        # should be skipped), check whether it is in thesekeys (which
        # is a copy of keys, which in turn is derived from the input
        # dictionary). If the current keyword is in thesekeys, perform
        # remediation if an expression is defined in the dictionary to
        # validate the keyword and if a remediation scheme is given,
        # and then remove it from thesekeys. Otherwise (i.e., if it is
        # not in thesekeys), append it to keysnotfound. At the end of
        # this loop, thesekeys will contain only keywords that in the
        # dictionary but not in the fits file, and keysnotfound will
        # contain keywords that are in the fits file but not in the
        # dictionary
        foundkey = -1
        if (len(keyname)>2):
            for i in range(len(thesekeys)):
                if (keyname == thesekeys[i]) and (level[keyname]==thislevel):
                    foundkey = i
            if foundkey>=0:
                if not (re.search( '^COMMENT', thesekeys[foundkey])):
                    mykey = thesekeys[foundkey][:-2]
                    myexpr = theseexpr[foundkey]
                    mymeth = thesemeth[foundkey]
                    # Remediate keywords that are present in the header, if needed
                    fixthis,value,type,instr = remediate( hdu, subset, mykey, myexpr, mymeth )
                    if len(instr)>0:
                        # Evaluate instruction that was sent back
                        if instr=='subdel':
                            # Add keyname to list of keywords to be deleted,
                            # and set fixthis to -1 to avoid further actions
                            subdel.append( thesekeys[foundkey][:-2] )
                            if subset>0:
                                fixthis = -1
                    if fixthis>0:
                        thisheader[mykey] = value
                        txt = 'Remediating keyword '+mykey+' to'
                        sendmsg( 'WARNING', txt, str(value), 'VRFY', shortname )
                    elif fixthis==0:
                        txt = 'Remediation of keyword '+mykey+' failed'
                        sendmsg( 'WARNING', txt, '', 'VRFY', shortname )
                        problemkeys.append( thesekeys[foundkey] )
                        print "KLOEP"
                    del thesekeys[foundkey],theseexpr[foundkey],thesemeth[foundkey]
            else:
                keysnotfound.append( keyname )
    # Try to remediate keywords that are missing from the header
    i = len(thesekeys)-1
    while i>=0:
        if level[thesekeys[i]]==thislevel:
            mykey = thesekeys[i][:-2]
            myexpr = theseexpr[i]
            mymeth = thesemeth[i]
            mystatus = status[thesekeys[i]]
            if mystatus=='FATAL' or mystatus=='ERROR' or mystatus=='RMDIATE':
                fixthis,value,type,instr = remediate( hdu, subset, mykey, myexpr, mymeth )
                if len(instr)>0:
                    # Evaluate instruction that was sent back
                    if instr=='subdel':
                        # Add keyname to list of keywords to be deleted,
                        # and set fixthis to -1 to avoid further actions
                        subdel.append( thesekeys[i][:-2] )
                        if subset>0:
                            fixthis = -1
                if (fixthis>0):
                    if type=='str':
                        thisheader.update( mykey, value )
                    if type=='float':
                        thisheader.update( mykey, float(value) )
                    if type=='int':
                        thisheader.update( mykey, int(value) )
                    txt = 'Remediating keyword '+mykey+' to'
                    sendmsg( 'WARNING', txt, str(value), 'VRFY', shortname )
                    del thesekeys[i],theseexpr[i],thesemeth[i]
                elif fixthis==0:
                    txt = 'Remediation of keyword '+mykey+' failed'
                    sendmsg( 'WARNING', txt, '', 'VRFY', shortname )
        i -= 1
    # Remove keywords from subsets if needed
    for key in subdel:
        try:
            nextend = hdu[0].header['nextend']
        except:
            nextend = 0
        # Loop over all subsets
        i = 1
        printmsg = True
        while i<=nextend:
            try:
                tmp = hdu[i].header[key]
            except:
                tmp = ''
            else:
                del hdu[i].header[key]
                if printmsg:
                    txt = 'Deleting keyword '+key+' from subsets'
                    sendmsg( 'WARNING', txt, '', 'VRFY', shortname )
                    printmsg = False
            i += 1
    # Assume the header is ok until found otherwise
    headerok = 1
    # Report the keywords that were found in the header but not
    # in the dictionary
    if len(keysnotfound)>0:
        if (verbose): print "\nKeywords in the fits file but not in the dictionary:"
        for remaining in keysnotfound:
            if (verbose): print remaining[:-2]
            headerok = 0
    # Report keywords in the dictionary that were not found in the
    # fits file, or keywords that were present in the header but for
    # which the remediation failed, and report error and warning
    # messages as necessary.  Skip keywords that the keyword
    # dictionary tells us to ignore.  To avoid numerous duplicate
    # messages at the subset level, duplicate warnings are counted and
    # consolidated into one message.
    printmsg = 1
    # Append the problem keywords, i.e., those present in the header
    # but for which the remediation failed to thesekeys, the list of
    # keywords that were not found in the header
    thesekeys = thesekeys + problemkeys
    for remaining in thesekeys:
        if level[remaining]==thislevel:
            if not (status[remaining]=='IGNORE'):
                if (printmsg==1):
                    if (headerok==1):
                        if (verbose): print "\n",
                    if (verbose):
                        print "Keywords in the dictionary but not in the fits file:"
                    printmsg = 0
                if (verbose): print "%s (%s)" % (remaining[:-2], status[remaining])
                if subset==0:
                    if (status[remaining]=='FATAL'):
                        msg = sendmsg( 'WARNING','Critical keyword incorrect or missing' ,
                                       remaining[:-2], 'VRFY', shortname )
                        logrejects( infile, msg )
                    elif (status[remaining]=='ERROR'):
                        dowarning = 1
                        obstype = thisheader['obstype'].lower()
                        if (obstype=='dome flat') or (obstype=='zero'):
                            dowarning = 0
                            if (remaining[:-2]=='GAIN') or (remaining[:-2]=='RDNOISE') or (remaining[:-2]=='SATURATE'):
                                dowarning = 1
                        if dowarning:
                            sendmsg( 'WARNING', 'Important keyword incorrect or missing',
                                     remaining[:-2], 'VRFY', shortname )
                else:
                    if keyhist.has_key( remaining ):
                        keyhist[remaining] += 1
                    else:
                        keyhist[remaining] = 1
                    if subset==16:
                        if keyhist[remaining]==16:
                            freq = 'all'
                        elif keyhist[remaining]>=8:
                            freq = 'most'
                        else:
                            freq = 'some'
                        if (status[remaining]=='FATAL'):
                            txt = 'Critical keyword incorrect or missing for '+freq+' subsets'
                            msg = sendmsg( 'WARNING', txt, remaining[:-2],
                                           'VRFY', shortname )
                            logrejects( infile, msg )
                        elif (status[remaining]=='ERROR'):
                            txt = 'Important keyword incorrect or missing for '+freq+' subsets'
                            sendmsg( 'WARNING', txt, remaining[:-2], 'VRFY', shortname )
                headerok =0
    if (headerok) and (verbose):
        print "- OK"

# Log start of processing
lfile = datadir+dataset+'.log'
logfile = open( lfile, 'a' )

# define a class for simultaneous output to file and stdout
# TODO: make this importable, so that one can do "import pipeline" above
class DualOut:
    def __init__(self):
        self.content = []
    def write(self, string):
        ofile1 = open( lfile, 'a' )
        ofile1.write(string)
        sys.stdout.write(string)
        ofile1.close()
tee = DualOut()

# Print standard start of the processing
print >> tee, '\nDIRVERIFY %s: ' % ( dataset )
print >> tee, datetime.datetime.today () # TODO: change format to match cl

# Go to the relevant directory
os.chdir( indir+'data' )

# Set up input and output file lists
infile = file( indir+dataset+'.dir', 'r' )
keepers = file( indir+dataset+'.keeplog', 'w' )
rejects = file( datadir+dataset+'.rejectlog', 'w' )
# Initialize counters
totfiles = 0
rejected = 0
# Loop over all files in the list
# Note that it is assumed that all data sets are local
for line in infile.readlines():
    totfiles += 1
    datasetok = 1 # Indicates no problems for this dataset
    thisline = line.strip()

    # Make sure file is on local host
    fields = thisline.split("!")
    if len(fields)==2:
        # The part before the ! is the hostname, the rest is the filename
        tmp = fields[0].split('.')
        filehost = tmp[0]
        thisfile = fields[1]
    elif len(fields)==1:
        thisfile = thisline
    else:
        # More than one ! in the file name
        sendmsg( 'ERROR', 'Input file name garbled', thisline, 'VRFY', dataset )
        sys.exit(1)
    if not (filehost == thishost):
        sendmsg( 'ERROR', 'Host name in input list is not this host',
                 filehost, 'VRFY', dataset )
        sys.exit(1)

    # If the current file is a link, then work on the file the link
    # is pointing to.
    if os.path.islink( thisfile ):
        thisfile = os.path.realpath( thisfile );

    # Make sure the file ends in .fits
    if not (re.search('\.fits$',thisfile)):
        thisfile += '.fits'

    # Get the short file name
    fields = thisfile.split("/")
    shortfile = fields[-1]

    if (verbose): print "==== Processing %s ====" % thisfile
    print "==== Processing %s ====" % thisfile
        
    # Open the fits file
    hdu = p.open( thisfile, 'update', uint16=1 )
    try:
        hdu = p.open( thisfile, 'update', uint16=1 )
    except:
        msg = sendmsg( 'WARNING', 'Could not open fits file',
                       thisfile, 'VRFY', shortfile )
        logrejects( thisfile, msg )
    else:
        try:
            # Read the header
            prihdr = hdu[0].header
        except:
            msg = sendmsg( 'WARNING', 'Could not open fits header',
                           thisfile, 'VRFY', shortfile )
            logrejects( thisfile, msg )
            hdu.close(output_verify='ignore')

    if (datasetok>0):

        ###############################################################
        #                                                             #
        #                CHECK ALL HEADER KEYWORDS                    #
        #                                                             #
        ###############################################################

        # Determine where these data were taken
        # First try the obsid keyword
        thistel = 'NONE'
        try:
            obsid = prihdr['obsid']
        except:
            # obsid not found, try the telescope keyword
            try:
                telescop = prihdr['telescop']
            except:
                sendmsg( 'ERROR', 'Cannot determine originating telescope',
                         '', 'VRFY', shortfile )
                datasetok = 0
            else:
                if (re.search( 'CTIO 4.0', telescop )):
                    thistel = 'C'
                if (re.search( 'KPNO 4.0', telescop )):
                    thistel = 'K'
                if (re.search( 'WIYN 0.9', telescop )):
                    thistel = 'W'
        else:
            if (re.search( '^ct4m', obsid )):
                thistel = 'C'
            if (re.search( '^kp4m', obsid )):
                thistel = 'K'
            if (re.search( '^kp09m', obsid )):
                thistel = 'W'

        # Initialize lists and dictionaries
        keys = []
        level = {}
        status = {}
        fixexpr = []
        fixmeth = []

        if (thistel =='NONE'):
            sendmsg( 'ERROR', 'Cannot determine originating telescope', '',
                     'VRFY', shortfile )
            datasetok = 0
        else:
            # Load the dictionary from file
            # TODO: this should be moved outside the loop.
            dictfile = file( datadir+'/pipedata/keywords.dct', 'r' )
            for line in dictfile.readlines():
                thisline = line.strip()
                if not (re.search( '^#', thisline)):
                    fields = onwhitespace.split(thisline)
                    if (len(fields)<=5):
                        print "ERROR reading keyword information for %s" % (fields[0])
                    else:
                        if re.search( thistel, fields[2]):
                            name = fields[0]+'-'+fields[1]
                            keys.append( name )
                            level[name] = fields[1]
                            status[name] = fields[3]
                            fixexpr.append( fields[4] )
                            fixmeth.append( fields[5] )
            dictfile.close()

        # Check the top and subset level headers
        keyhist = {}
        checkheader( thisfile, hdu, keys, fixexpr, fixmeth, 'T', 0, shortfile )
        # Determine number of subsets
        try:
            nextend = prihdr['nextend']
        except:
            nextend = 0
        # Set flags that determine whether a message should be printed
        printmsg = 1
        printmsg2 = 1
        printmsg3 = 1
        # Loop over all subsets
        for i in range(nextend):
            subset = i+1
            try:
                thisheader = hdu[subset].header
            except:
                if printmsg==1:
                    msg = sendmsg( 'ERROR',
                                   'Could not open subset header. Is the fits file corrupt?',
                                   '', 'VRFY', shortfile )
                    logrejects( thisfile, msg )
                    printmsg = 0
            else:
                checkheader( thisfile, hdu, keys, fixexpr, fixmeth, 'S', subset, shortfile )

        # Close the fits file
        hdu.close(output_verify='ignore')
        # Available options: exception, ignore, fix, silentfix, warn

    if (datasetok==1):
        print >> keepers, thishost+'!'+thisfile
    else:
        rejected += 1
        
# Close the output files
keepers.close()
rejects.close()

# Send a warning summarizing how many files were rejected
if (rejected>0):
    msg = str(rejected)+' out of '+str(totfiles)+' rejected'
    sendmsg( 'WARNING', msg, '', 'VRFY', dataset )

# Exit with error if all files have been rejected
if (rejected==totfiles):
    sendmsg( 'ERROR', 'All input files were rejected', '', 'VRFY', dataset )
    sys.exit(1)

# Store the original input file
filea = indir+dataset+'.dir'
fileb = indir+dataset+'.orig'
os.rename( filea, fileb )

# Rename the file with the keepers to the original input file;
# this means rejected files will not be processed
fileb = indir+dataset+'.keeplog'
os.rename( fileb, filea )

sys.exit(0)
