# MEFNAMES -- Directory and filenames for the MEF pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure mefnames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline name"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file	parent			{prompt = "Parent part of dataset"}
file	child			{prompt = "Child part of dataset"}
file	lfile			{prompt = "Log file"}
file	hdr			{prompt = "MEF header"}
file	sky			{prompt = "MEF sky"}
file	sig			{prompt = "MEF sky sigma"}
file	mcat			{prompt = "Merged USNO catalog"}
file	gif			{prompt = "GIF"}
file	ogif			{prompt = "GIF"}
file	gif2			{prompt = "GIF"}
file	mgif			{prompt = "GIF"}
file	mgif2			{prompt = "GIF"}
string	pattern = ""		{prompt = "Pattern"}
bool	base = no		{prompt = "Child part of dataset"}

begin
	# Set generic names.
	names ("mef", name, pattern=pattern, base=base)
	shortname = names.shortname
	pipedata  = names.pipedata
	dataset	= names.dataset
	pipe	= names.pipe
	rootdir	= names.rootdir
	indir	= names.indir
	datadir	= names.datadir
	uparm	= names.uparm
	parent	= names.parent
	child	= names.child
	lfile	= names.lfile

	# Set pipeline specific names.
	hdr	= shortname // "_00.fits"
	mcat	= shortname // "_usno.mcat"
	sky	= shortname // "_sky.fits"
	sig	= shortname // "_sig.fits"
	gif	= shortname // ".gif"
	gif2	= gif // "2"
	ogif	= shortname // "_o.gif"
	mgif	= shortname // "_m.gif"
	mgif2	= mgif // "2"

	if (pattern != "") {
	    ;
	}
end
