#!/bin/env python

"""
Capture data quality information from the output of usnomatch and create
a .cl script to update the dataset and PMAS with.
"""

import re
import sys

outfile = open( 'mefwcs_usnomap_proc.cl', 'w' )

infile = file( sys.argv[1], 'r')
for line in infile.readlines():
    thisline = line.strip()
    m = re.match( 'tangent point shift = \((.*), (.*)\) arcsec', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcuptx\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupty\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUPTX\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUPTY\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))
        print >> outfile, "print( \"DQWCUPTX \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
        print >> outfile, "print( \"DQWCUPTY \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(2))
    m = re.match( 'fractional scale change = \((.*), (.*)\)', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupsx\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupsy\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUPSX\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUPSY\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))
        print >> outfile, "print( \"DQWCUPSX \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
        print >> outfile, "print( \"DQWCUPSY \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(2))
    m = re.match( 'axis rotation = \((.*), (.*)\) degrees', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupax\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupay\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUPAX\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUPAY\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))
        print >> outfile, "print( \"DQWCUPAX \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
        print >> outfile, "print( \"DQWCUPAY \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(2))
    m = re.match( 'rms = \((.*), (.*)\) arcsec', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcuprx\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcupry\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUPRX\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUPRY\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))
        print >> outfile, "print( \"DQWCUPRX \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
        print >> outfile, "print( \"DQWCUPRY \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(2))


infile.close()
outfile.close()


