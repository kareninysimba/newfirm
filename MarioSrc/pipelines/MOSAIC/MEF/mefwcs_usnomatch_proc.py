#!/bin/env python

"""
Capture data quality information from the output of usnomatch and create
a .cl script to update the dataset and PMAS with.
"""

import re
import sys

outfile = open( 'mefwcs_usnomatch_proc.cl', 'w' )

infile = file( sys.argv[1], 'r')
for line in infile.readlines():
    thisline = line.strip()
    m = re.match( 'x offset is (.*) pixels', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumxo\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUMXO\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "print( \"DQWCUMXO \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
    m = re.match( 'y offset is (.*) pixels', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumyo\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUMYO\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "print( \"DQWCUMYO \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
    m = re.match( 'rotation is (.*) degrees', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumro\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUMRO\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "print( \"DQWCUMRO \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
    m = re.match( 'total matched (.*) out of (.*) detections and (.*) references$', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumts\", value=\"%s\" )" % (m.group(2))
        frac = float(m.group(1))/float(m.group(3))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcummf\", value=\"%f\" )" % (frac)
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUMTS\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(3))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUMMF\", \"%f\", add+, update+, verify-, show+, >> lfile)" % (frac)
        print >> outfile, "print( \"DQWCUMTS \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
        print >> outfile, "print( \"DQWCUMMF \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
    print thisline
    m = re.match( '\w+ dmagzero = (.*) \+- (.*)$', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcummz\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=mefnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumme\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUMMZ\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( mefnames.hdr, \"DQWCUMME\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))
        print >> outfile, "print( \"DQWCUMMZ \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))
        print >> outfile, "print( \"DQWCUMME \", \"%s\", >> \"mefgwcs.hdr\")" % (m.group(1))

infile.close()
outfile.close()

