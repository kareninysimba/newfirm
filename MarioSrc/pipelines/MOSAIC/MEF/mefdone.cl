#!/bin/env pipecl
#
# MEFDONE -- Finish up pipeline.
# The end result of this pipeline is to return a list file.  It is
# up to later software to decide when to fetch and clean up the results.
#
# The last step is to remove the processing file.

string	dataset, indir, datadir, outdir, lfile

# Set environment.
mefnames (envget("OSF_DATASET"))
dataset = mefnames.dataset
set (uparm = mefnames.uparm)

indir = mefnames.indir
datadir = mefnames.datadir
outdir = mefnames.rootdir // "output/"
lfile = mefnames.lfile

# Log start of processing.
printf ("\nMEFDONE (%s): ", dataset) | tee (lfile, out="text", append+)
time | tee (lfile, out="text", append+)

# Set the result list in the output directory and log the contents.
pathdatasets (datadir//"*", sort-, > outdir//dataset//".list")
concatenate (datadir//"*.list", outdir//dataset//".list", append+)
type (outdir//dataset//".list") | tee (lfile, out="text", append+)

# Clean up processing files.
cd ("MOSAIC_MEF$/" // "sif")
delete ("@"//dataset//".list//.pmef", verify-)
delete (dataset//".list", verify-)
delete (indir//dataset//".pmef", verify-)

logout 1
