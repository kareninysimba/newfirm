#!/bin/env pipecl
# 
# Create PNGs from the raw images.

string  png, dataset, indir, datadir, ifile, lfile
struct	*fd

# Load tasks and packages
task $mkgraphic = "$!mkgraphic"
task $convert = "$!convert"
utilities
proto
images

# Set names and directories.
mefnames (envget ("OSF_DATASET"))
png = mefnames.shortname
dataset = mefnames.dataset
indir = mefnames.indir
datadir = mefnames.datadir
ifile = indir // dataset // ".mef"
lfile = mefnames.lfile
set (uparm = mefnames.uparm)
set (pipedata = mefnames.pipedata)
cd (datadir)

# Log start of module.
printf ("\nMEFRAWPNG (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Make PNGs.
# For now assume only one input.
list = ifile
while (fscan (list, s1) != EOF) {
    # Find offset file.
    line = ""; i = 1; j = 1
    hselect (s1//"[1]", "CCDSUM", yes) | scan (line)
    k = fscan (line, i, j)
    s2 = "pipedata$rawoff" // i // j // ".dat"
    if (access(s2) == NO) {
        printf ("ERROR: Could not find %s from %s\n", s2, s1)
        break
    }
    ;

    # Set extensions.
    imextension (s1, > "mefrawpng.tmp")
    hselect ("@mefrawpng.tmp", "$I,datasec", yes, > "mefrawpng1.tmp")
    translit ("mefrawpng1.tmp", "	", del+, > "mefrawpng.tmp")
    if (access("mefrawpng.tmp") == NO)
        printf ("ERROR: file %s not created??\n", "mefrawpng.tmp")
    ;

    # Set offsets.
    hselect ("@mefrawpng.tmp", "extname", yes, > "mefrawpng1.tmp")
    fd = "mefrawpng1.tmp"
    while (fscan(fd, s3) != EOF) {
        match ("{"//s3//"}", s2) | scan (s3, i, j)
	if (nscan() == 3)
	    print (i, j, >> "mefrawpng2.tmp")
	else {
	    if (access ("mefrawpng2.tmp") == YES)
	        delete ("mefrawpng2.tmp")
	    ;
	    break
	}
    }
    fd = ""; delete ("mefrawpng1.tmp")

    # Create PNG.
    if (access("mefrawpng2.tmp")==YES) {
	if (access("mefrawpng.tmp") == NO)
	    printf ("ERROR: where is file %s??\n", "mefrawpng.tmp")
	;
	mkgraphic ("@mefrawpng.tmp", "mefrawpng", "mefrawpng2.tmp",
	    "gif", 16, "none", "mode")
	convert ("-resize "//png_pix//"x"//png_pix,
	    "mefrawpng.gif", png//"_raw.png")
    }
    ;
    delete ("mefrawpng[^_]*")
    break
}
list = ""

if (access(png//"_raw.png")==NO)
    logout 0
;

logout 1
