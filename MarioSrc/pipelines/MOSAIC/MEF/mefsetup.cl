#!/bin/env pipecl
#
# MEFSETUP -- Set MEF processing data including calibrations.
#
# The module is responsible for checking the input and doing initialization
# for a particular MEF dataset.  This includes setting parameters and getting
# calibrations.  Note that only some calibrations need to set here because
# calibrations specific to the SIF pipeline will be set later.
#
# A revision is no to continue if zero and/or flats are not available.

int	founddir
string	dataset, indir, datadir, ifile, lfile, calops
int	status = 1
file	caldir = "MC$"

# Packages and tasks.
task    $cp = "$!cp -r $(1) $(2)"
task    $ln = "$!ln -s $(1) $(2)"
images
servers

# Files and directories.
mefnames (envget("OSF_DATASET"))
dataset = mefnames.dataset
indir   = mefnames.indir
datadir = mefnames.datadir
ifile = indir // dataset // ".mef"
lfile = datadir // mefnames.lfile
set (uparm = mefnames.uparm)
set (pipedata = mefnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nMEFSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Setup directories first.
# This is done for all files so that the SIF pipelines don't have to
# do a getcal.

delete (substr(mefnames.uparm, 1, strlen(mefnames.uparm)-1))
list = ifile
while (fscan (list, s1) != EOF) {
    iferr {
	setdirs (s1//"[0]")
    } then {
	status = 0
	break
    } else
	;
}
list = ""

if (status == 0)
    logout (status)
;

# Check input data.
cd (indir)
s3 = ""
list = ifile
while (fscan (list, s1) != EOF) {

    # Check for important keywords.  Much of the data verification should
    # probably be done before the data reaches the archive.  Also these
    # steps could be done more efficiently in a compiled script.

    s2 = "mjd-obs"; x = INDEF; hselect (s1//"[0]", s2, yes) | scan (x)
    if (isindef(x))
        s3 += s2
    else if (x < 40000 || x > 60000)
        s3 += s2
    ;
    s2 = "gain"; x = INDEF; hselect (s1//"[1]", s2, yes) | scan (x)
    if (isindef(x))
        s3 += " " // s2
    else if (x <= 0)
        s3 += " " // s2
    ;
    s2 = "saturate"; x = INDEF; hselect (s1//"[1]", s2, yes) | scan (x)
    if (isindef(x))
        s3 += " " // s2
    else if (x <= 0)
        s3 += " " // s2
    ;

    if (s3 != "") {
	imhead (s1//"[1]", l+)
        sendmsg ("ERROR", "Keyword error", s3, "VRFY")
	printf ("ERROR: Keyword error (%s)\n", s3) | tee (lfile)
	break
    }
    ;
}
list = ""

if (s3 != "")
    logout 3
;

# Set MEF calibrations.
s3 = mktemp (dataset)
list = dataset//".mef"
while (fscan (list, s1) != EOF) {
    s2 = substr( s1, strldx("/",s1)+1, strlen(s1) )

    # First set the operations.
    getcal (s1//"[0]", "calops", cm, caldir, obstype="", detector=instrument,
        imageid="", filter="!filter", exptime="", match="", mjd="!mjd-obs")
    if (getcal.statcode != 0) {
        sendmsg ("ERROR", "Getcal failed for CALOPS",
	    s2//" - "//getcal.statstr, "CAL")
	status = 0
	break
    }
    ;
    calops = getcal.value

    # Check for zero.
    getcal (s1//"[1]", "zero", cm, caldir,
        obstype="", detector=instrument, imageid="!ccdname",
	filter="", exptime="", quality=0., mjd="!mjd-obs",
	match="!nextend,ccdsum") | scan (i, line)

    # Check for basic flat: give preference to dome or twilight flats.
    if (stridx ("F", calops) > 0)
	getcal (s1//"[1]", "_flat asc", cm, caldir,
	    obstype="", detector=instrument, imageid="!ccdname",
	    filter="!filter", exptime="", quality=0.,  mjd="!mjd-obs",
	    match="!nextend,ccdsum")
    else
	getcal (s1//"[1]", "_flat desc", cm, caldir,
	    obstype="", detector=instrument, imageid="!ccdname",
	    filter="!filter", exptime="", quality=0.,  mjd="!mjd-obs",
	    match="!nextend,ccdsum")

    # Translate filter to best Landolt passband and then get the
    # B/V to passband coefficients.  It is not a fatal error if
    # not found.

    getcal (s1//"[0]", "photindx", cm, "",
        obstype="", detector="", imageid="", filter="!filter", exptime="",
	mjd="!mjd-obs") | scan (i, line)
    if (i != 0)
        sendmsg ("WARNING", "Getcal failed for PHOTIDX", s2//" - "//line,
	    "CAL")
    ;

    getcal (s1//"[0]", "photfilt", cm, "",
        obstype="", detector="", imageid="", filter="!photindx", exptime="",
	mjd="!mjd-obs") | scan (i, line)
    if (i != 0)
        sendmsg ("WARNING", "Getcal failed for PHOTFILT",
	    s2//" - "//line, "CAL")
    ;
    getcal (s1//"[0]", "photcoef", cm, "",
        obstype="", detector="", imageid="", filter="!photfilt", exptime="",
	mjd="!mjd-obs") | scan (i, line)
    if (i != 0)
        sendmsg ("WARNING", "Getcal failed for PHOTCOEF",
	    s2//" - "//line, "CAL")
    ;

    getcal (s1//"[0]", "MAGZREF", cm, "",
        obstype="", detector=instrument, imageid="", filter="!filter",
	exptime="", mjd="!mjd-obs")
    if (getcal.statcode != 0) {
	getcal (s1//"[0]", "MAGZREF", cm, "",
	    obstype="", detector=instrument, imageid="", filter="!photindx",
	    exptime="", mjd="!mjd-obs")
	if (getcal.statcode != 0)
	    getcal (s1//"[0]", "MAGZREF", cm, "",
		obstype="", detector="", imageid="", filter="!photindx",
		exptime="", mjd="!mjd-obs")
	;
	if (getcal.statcode == 0)
	    hedit (s1//"[0]", "MAGZFLT", "(PHOTINDX)", add+, show-)
    } else
	hedit (s1//"[0]", "MAGZFLT", "(FILTER)", add+, show-)
    if (getcal.statcode != 0)
        sendmsg ("WARNING", "Getcal failed for MAGZREF",
	    s2//" - "//getcal.statstr, "CAL")
    ;

    # Get WCSDB for photindx and if not found look for any solution.
    getcal (s1//"[0]", "wcsdb", cm, caldir,
        obstype="", detector=instrument, imageid="", filter="!photindx",
	exptime="", match="!nextend", mjd="!mjd-obs") | scan (i, line)
    if (i != 0) {
	getcal (s1//"[0]", "wcsdb", cm, "",
	    obstype="", detector=instrument, imageid="", filter="default",
	    exptime="", match="!nextend", mjd="!mjd-obs") | scan (i, line)
	if (i != 0)
	    sendmsg ("WARNING", "Getcal failed for WCSDB",
		s2//" - "//line, "CAL")
	;
    }
    ;

}
list = ""

if (status != 1)
    logout (status)
;

logout (status)
