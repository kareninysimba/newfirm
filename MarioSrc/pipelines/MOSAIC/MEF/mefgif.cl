#!/bin/env pipecl
# 
# Create GIFs from the SIF results.

string  dataset, datadir, lfile

# Load tasks and packages
task $mkgraphic = "$!mkgraphic"
task $mkgraphico = "$!mkgraphico"

# Set names and directories.
mefnames (envget ("OSF_DATASET"))
sifnames (mefnames.dataset, pattern="ccd[0-9]*")
dataset = mefnames.dataset
datadir = mefnames.datadir
lfile = mefnames.lfile
set (uparm = mefnames.uparm)
cd (datadir)

# Check for files and exit if not available.
match (sifnames.blk1, "*.sif", print-) | sort ("STDIN", > "mefgif.tmp")
count ("mefgif.tmp") | scan (i)
if (i == 0) {
    delete ("mefgif.tmp")
    logout 1
}
;

# Log start of module.
printf ("\nMEFGIF (%s): ", dataset) | tee (lfile)
time | tee (lfile)

#match (sifnames.blk1, "*.sif", print-) | sort ("STDIN", > "mefgif.tmp")
match (sifnames.bpmblk1, "*.sif", print-) | sort ("STDIN", > "mefgif1.tmp")
mkgraphic ("@mefgif.tmp", mefnames.gif, "gifoff", "gif")
mkgraphico ("@mefgif.tmp", mefnames.ogif, "@mefgif1.tmp",
    mefnames.mcat, "gifoff", "gif")

match (sifnames.blk2, "*.sif", print-) | sort ("STDIN", > "mefgif.tmp")
mkgraphic ("@mefgif.tmp", mefnames.gif2, "gifoff", "gif")

delete ("mefgif*.tmp")

logout 1
