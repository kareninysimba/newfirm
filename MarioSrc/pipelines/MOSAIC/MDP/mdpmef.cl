#!/bin/env pipecl
# 
# MDPMEF -- Make MEF files.
#
# This may be triggered more than once with different flag values.

int	ncombine
real	exptime, quality
string  dataset, datadir, ifile, rfile, lfile, nhscript, flag, s4, bpm
string	proctype, propid
struct	obstype

# Load tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
task $fpack = "$!fpack -D -Y -q 4 -v $(1)"
task $mkgraphic = "$!mkgraphic"
task $convert = "$!convert"
task $plver = "$!plver"
task mdphdr = "NHPPS_PIPESRC$/MOSAIC/MDP/mdphdr.cl"
task mdpwcs = "NHPPS_PIPESRC$/MOSAIC/MDP/mdpwcs.cl"
task mdpgwcs = "NHPPS_PIPESRC$/MOSAIC/MDP/mdpgwcs.cl"
fitsutil
images
servers
proto
utilities
noao
artdata
imcoords
mscred
cache mscred, average
task toshort = "mscred$src/x_mscred.e"

# Set names and directories.
mdpnames (envget ("OSF_DATASET"))
dataset = mdpnames.dataset
datadir = mdpnames.datadir
ifile = mdpnames.indir // dataset // ".mdp"
rfile = mdpnames.indir // "return/" // dataset // ".mdp"
lfile = mdpnames.lfile
set (uparm = mdpnames.uparm)
set (pipedata = mdpnames.pipedata)
cd (datadir)

# Delete any prexisting files.
delete ("mdpmef*tmp,mdpwcs*tmp", >& "dev$null")
delete (mdpnames.mef//"_*", >& "dev$null")
delete (mdpnames.mef//".fits", >& "dev$null")
delete (mdpnames.bpm//".fits", >& "dev$null")
delete (mdpnames.mefpng, >& "dev$null")

# Make copy of input list and return file for possible retriggering.
if (access(dataset//".mdp"))
    copy (dataset//".mdp", ifile)
else
    copy (ifile, dataset//".mdp")
if (access("return.tmp"))
    copy ("return.tmp", rfile)
else
    copy (rfile, "return.tmp")

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Find data.
j = 3
match ("_s.fits", ifile, > "mdpmef1.tmp")
count ("mdpmef1.tmp") | scan (i)
if (i == 0) {
    match ("_f.fits", ifile, > "mdpmef1.tmp")
    count ("mdpmef1.tmp") | scan (i)
}
;
if (i == 0) {
    match ("_p.fits", ifile, > "mdpmef1.tmp")
    count ("mdpmef1.tmp") | scan (i)
}
;

# If this is a second pass check if any different data products will generated.
flag = envget ("OSF_FLAG")
if (flag != "_" && i == 0) {
    delete ("mdpmef1.tmp")
    logout 4
}
;
if (flag != "_") {
    imdelete (mdpnames.mef, >& "dev$null")
    imdelete (mdpnames.bpm, >& "dev$null")
    imdelete (mdpnames.mefpng, >& "dev$null")
    i = 0
}
;

if (i == 0) {
    j = 1
    match ("SCL?*ccd[0-9]*.fits", ifile, > "mdpmef1.tmp")
    count ("mdpmef1.tmp") | scan (i)
}
;
if (i == 0) {
    j = 1
    match ("SIF?*ccd[0-9]*.fits", ifile, > "mdpmef1.tmp")
    count ("mdpmef1.tmp") | scan (i)
}
;
if (i == 0) {
    j = 5
    match ("_pgr.fits", ifile, > "mdpmef1.tmp")
    count ("mdpmef1.tmp") | scan (i)
}
;
if (i == 0) {
    j = 5
    match ("_frg.fits", ifile, > "mdpmef1.tmp")
    count ("mdpmef1.tmp") | scan (i)
}
;
if (i == 0) {
    j = 5
    match ("_sft.fits", ifile, > "mdpmef1.tmp")
    count ("mdpmef1.tmp") | scan (i)
}
;
if (i == 0) {
    delete ("mdpmef1.tmp")
    sendmsg ("ERROR", "No data found", "", "VRFY")
    logout 3
}
;

# Setup UPARM and PIPEDATA.
if (flag == "_") {
    delete (substr(mdpnames.uparm, 1, strlen(mdpnames.uparm)-1))
    s1 = ""; head ("mdpmef1.tmp") | scan (s1)
    iferr {
	setdirs (s1)
    } then {
	logout 0
    } else
	;
}
;

# For standardization we sort by CCD.
list = "mdpmef1.tmp"
while (fscan (list, s1) != EOF) {
    s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-j)
    s2 = substr (s2, strlstr("ccd",s2), 1000)
    printf ("%s %s\n", s2, s1, >> "mdpmef3.tmp")
}
list = ""
sort ("mdpmef3.tmp") | uniq (> "mdpmef2.tmp")
delete ("mdpmef3.tmp")

# Remove any individual exposures (_NN_, e.g., *_01_*) from the list,
# rename output to mdpmef2.tmp so that the rest of the code can remain
# the same
match( "_[0-9][0-9]_", "mdpmef2.tmp", stop=yes, > "mdpmef3.tmp" )
delete ("mdpmef2.tmp")
rename("mdpmef3.tmp", "mdpmef2.tmp")

# Set the image list and offset list.
s4 = ""
delete ("mdpmef1.tmp")
list = "mdpmef2.tmp"
while (fscan (list, s2, s1) != EOF) {
    if (s4 == "") {
hselect (s1, "CCDSUM", yes)
	line = ""; i = 1; j = 1
        hselect (s1, "CCDSUM", yes) | scan (line)
	k = fscan (line, i, j)
	s4 = "pipedata$offsets" // i // j // ".dat"
	if (access(s4) == NO)
	    break
	;
    }
    ;
    s3 = ""; match (s2, s4) | scan (s3, line)
    if (s3 != "") {
	print (s1, >> "mdpmef1.tmp")
	print (line, >> "mdpmef4.tmp")
    }
    ;
}
list = ""

# Make the global header.
mkglbhdr ("@mdpmef1.tmp", mdpnames.mef, exclude="@pipedata$mkglbhdr.exclude")

# Determine the various types.
hselect (mdpnames.mef, "$PROCTYPE,$OBSTYPE", yes) | scan (proctype, obstype)
if (proctype == "INDEF")
    proctype = "InstCal"
;
if (obstype == "dome flat")
    nhscript = "pipedata$mdpmef" // "flat" // ".nhedit"
else
    nhscript = "pipedata$mdpmef" // obstype // ".nhedit"
if (access(nhscript)==NO)
    nhscript = "pipedata$mdpmef" // "flat" // ".nhedit"
    
# Fix the global header.
mdphdr (mdpnames.mef, proctype, obstype, "")
nhedit (mdpnames.mef, comfile=nhscript)
nhedit (mdpnames.mef, "BUNIT", del+)

# Append the extensions.  Compress non-calibrations by conversion to
# 16 bits if selected.
quality = INDEF
list = "mdpmef2.tmp"
j = 0
while (fscan (list, s2, s1) != EOF) {
    if (imaccess(s1) != YES)
       next
    ;
    printf ("%s_%s\n", mdpnames.mef, s2) | scan (s3)
    if (mdp_doshort) {
	bpm = "";
	s4 = ""; hselect (s1, "bpm", yes) | scan (s4)
	if (s4 != "")
	    match (s4, ifile) | scan (bpm)
	;
	if (proctype == "MasterCal") {
	    imcopy (s1, s3, verbose-)
	    y = 0.; hselect (s1, "quality", yes) | scan (y)
	    if (isindef(quality))
	        quality = y
	    else
		quality = min (quality, y)
	} else
	    toshort (s1, s3, inmasks=bpm, sigma="!SIG", maxbscale=mdp_maxbscale,
		docopy+, logfile=lfile)
    } else
	imcopy (s1, s3, verbose-)
    i = 1; hselect (s1, "NCOMBINE", yes) | scan (i)
    mdphdr (s3, proctype, obstype, s2)
    nhedit (s3, comfile=nhscript)
    mdpwcs (s3, obstype, mosaic+)
    printf ("%s[%s,append,inherit]\n", mdpnames.mef, s2) | scan (s4)
    imcopy (s3, s4, verbose-)
    imdel (s3)
    j += 1
}
list = ""; delete ("mdpmef2.tmp")

nhedit (mdpnames.mef//"[0]", "NEXTEND", j, "Number of extensions",
    before="OBSTYPE", add+)

hselect (mdpnames.mef//"[0]", "$DTPROPID", yes) | scan (propid)
hselect( mdpnames.mef//"[1]", "$EXPTIME", yes ) | scan(exptime)
if (proctype=="MasterCal") {
    if (propid != "PLTEST")
        propid = "CAL"
    ;
    if (isindef(quality)==NO)
	nhedit (mdpnames.mef//"[0]", "QUALITY", quality, "PL quality indicator",
	     add+)
    ;
} else
    mdpgwcs( mdpnames.mef )
hedit (mdpnames.mef//"[0]", "PLPROPID", propid, add+, ver-)

# Create the MEF bad pixel masks.
match ("MOSAIC_SIF?*_bpm.pl", ifile, > "mdpmef2.tmp")
count ("mdpmef2.tmp") | scan (i)

if (i > 0) {
    # For standardization we sort by CCD.
    list = "mdpmef2.tmp"
    while (fscan (list, s1) != EOF) {
	s2 = substr (s1, strldx("/",s1)+1, strlstr("_bpm.pl",s1)-1)
	j = strstr ("ccd", s2)
	if (j == 0)
	    next
	;
	s2 = substr (s2, j, 1000)
	printf ("%s %s\n", s2, s1, >> "mdpmef3.tmp")
    }
    list = ""; delete ("mdpmef2.tmp")
    sort ("mdpmef3.tmp") | uniq (> "mdpmef2.tmp")
    delete ("mdpmef3.tmp")

    mkglbhdr (mdpnames.mef//"[0]", mdpnames.bpm)

    list = "mdpmef2.tmp"
    for (i=1; fscan (list, s2, s1) != EOF; i+=1) {
	if (i == 1) {
	    nhedit (mdpnames.bpm, "OBJECT", "Mask for "//mdpnames.mef, ".",
	        add+)
	    nhedit (mdpnames.bpm, "PRODTYPE", "dqmask", ".", add+)
	    nhedit (mdpnames.bpm, "DQMFOR", mdpnames.mef,
	        "Data for which this mask applies", add+)
	}
	;
	# Hacks to use regular FITS file so that mkheader doesn't segfault.
	# This seems to be a very rare thing but...
	s3 = "mdpmefbpm"
	#printf ("%s[%s,append,type=mask]\n", mdpnames.bpm, s2) | scan (s3)
	imcopy (s1, s3, verbose-)
	#printf ("%s[%s]\n", mdpnames.bpm, s2) | scan (s3)
	printf ("%s[%s]\n", mdpnames.mef, s2) | scan (s1)
	print (s1, > "mdpmef5.tmp")
	mkheader (s3, "@mdpmef5.tmp", append-, verbose-)
	nhedit (s3, "OBJECT", "Mask for "//s1, ".", add+)
	nhedit (s3, "PRODTYPE", "dqmask", ".", add+)
	nhedit (s3, "DQMFOR", s1, "Data for which this mask applies", add+)
	# There is a weird bug that the following eliminates the OBJECT
	# keyword from the extensions which causes problems in following
	# mkglbhdr.  DQMASK is now set in mdphdr but this comment is a warning.
	#nhedit (s1, "DQMASK", s3, "Data quality mask")

	printf ("%s[%s,append,type=mask]\n", mdpnames.bpm, s2) | scan (s1)
	imcopy (s3, s1, verbose-)
	imdelete (s3)
    }
    list = ""; delete ("mdpmef2.tmp")
} else
    delete ("mdpmef2.tmp")

# Create the graphic files.
if (access("mdpmef1.tmp")) {
    if (obstype=="pupil")
	mkgraphic ("@mdpmef1.tmp", mdpnames.mefpng1, "mdpmef4.tmp", "png",
	    png_blk, "'i1>0'") 
    else if (proctype=="MasterCal")
	mkgraphic ("@mdpmef1.tmp", mdpnames.mefpng1, "mdpmef4.tmp", "png",
	    png_blk, "'i1!=0'") 
    else {
    	hselect (mdpnames.mef//"[0]", "$SKYMEAN", yes) | scan (x)
	if (isindef(x) || x < 0)
	    mkgraphic ("@mdpmef1.tmp", mdpnames.mefpng1, "mdpmef4.tmp", "png",
		png_blk, "'i1!=0'") 
	else
	    mkgraphic ("@mdpmef1.tmp", mdpnames.mefpng1, "mdpmef4.tmp", "png",
		png_blk, "'i1>0'") 
    }
    convert ("-resize "//png_pix//"x"//png_pix, mdpnames.mefpng1,
        mdpnames.mefpng2)
    t_fgwrite ("-f mdpmef_png.fits", "-s", mdpnames.mefpng2, mdpnames.mefpng1)
    mkglbhdr (mdpnames.mef//"[1]", mdpnames.mefpng)
    nhedit (mdpnames.mefpng//"[0]", "PRODTYPE", "png", ".", add+)
    nhedit (mdpnames.mefpng//"[0]", "MIMETYPE", "image/png", ".", add+)
    fxcopy ("mdpmef_png.fits", mdpnames.mefpng, groups="1,2", new_file-)
    imdelete ("mdpmef_png.fits")
    delete ("mdpmef1.tmp")
    delete ("mdpmef4.tmp")
    delete (mdpnames.mefpng1)
    delete (mdpnames.mefpng2)
}
;

# Clean up
delete("mdpmef*.tmp,mdpwcs_all*.tmp")

# Set whether to review the results.
if (dps_review)
    logout 2
;

# Set the data products files.
if (access(mdpnames.mef//".fits")) {
    fpack (mdpnames.mef//".fits")
    print (mdpnames.mef//".fits.fz", > "mdpmef.dps")
}
;
if (access(mdpnames.bpm//".fits"))
    print (mdpnames.bpm//".fits", >> "mdpmef.dps")
;
if (access(mdpnames.mefpng))
    print (mdpnames.mefpng, >> "mdpmef.dps")
;

logout 1
