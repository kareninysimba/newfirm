#!/bin/env pipecl
# 
# MDPSTK -- Make stacked files.
#
# Currently, everything is done in MDC and the only thing here is to
# make the file for mdp2stb.

string  dataset, datadir, ifile, lfile

# Load tasks and packages

# Set names and directories.
mdpnames (envget ("OSF_DATASET"))
dataset = mdpnames.dataset
datadir = mdpnames.datadir
ifile = mdpnames.datadir // dataset // ".mdp"
lfile = datadir // mdpnames.lfile
set (uparm = mdpnames.uparm)
cd (datadir)

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Collect the pieces.
match ("MDC?*_stk.fits", ifile, > "mdpstk.dps")
match ("MDC?*_stk_bpm.fits", ifile, >> "mdpstk.dps")
match ("MDC?*_stk_expmask.fits", ifile, >> "mdpstk.dps")
match ("MDC?*_stk_png.fits", ifile, >> "mdpstk.dps")

logout 1
