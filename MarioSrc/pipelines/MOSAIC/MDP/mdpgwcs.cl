# MDPGWCS -- Set global header for WCS related keywords.

procedure mdpgwcs (image)

string	image			{prompt="Image header to edit"}

begin
	real	x1, x2, x3, x4, y1, y2, y3, y4
	real	 racen, deccen
	string  img, s1

	# Operate on global header.
	img = image//"[0]"

        # Set the global RA and DEC of the center and corners.
	if (access("mdpwcs_allwcs.tmp")) {
	    sort("mdpwcs_allwcs.tmp", col=1, ignore+, num+, rev-) |
	        scan (x1, y1)
	    racen = x1

	    sort("mdpwcs_allwcs.tmp", col=1, ignore+, num+, rev+) |
	        scan (x3, y3)
	    racen = (racen+x3)/2

	    sort("mdpwcs_allwcs.tmp", col=2, ignore+, num+, rev-) |
		scan (x2, y2)
	    deccen = y2

	    sort("mdpwcs_allwcs.tmp", col=2, ignore+, num+, rev+) |
	        scan (x4, y4)
	    deccen = (deccen+y4)/2

	    nhedit (img, "CORN4DEC", y4, "[deg] DEC corner of exposure",
		add+, after="DEC")
	    nhedit (img, "CORN3DEC", y3, "[deg] DEC corner of exposure",
		add+, after="DEC")
	    nhedit (img, "CORN2DEC", y2, "[deg] DEC corner of exposure",
		add+, after="DEC")
	    nhedit (img, "CORN1DEC", y1, "[deg] DEC corner of exposure",
		add+, after="DEC")
	    nhedit (img, "CENTRA", racen, "[deg] RA center of exposure",
		add+, after="DEC")
	    nhedit (img, "CORN4RA", x4, "[deg] RA corner of exposure",
		add+, after="DEC")
	    nhedit (img, "CORN3RA", x3, "[deg] RA corner of exposure",
		add+, after="DEC")
	    nhedit (img, "CORN2RA", x2, "[deg] RA corner of exposure",
		add+, after="DEC")
	    nhedit (img, "CORN1RA", x1, "[deg] RA corner of exposure",
		add+, after="DEC")
	    nhedit (img, "CENTDEC", deccen, "[deg] DEC center of exposure",
		add+, after="DEC")

	    printf("%.2H\n", racen) | scan (s1)
	    nhedit (img, "RA",  s1, "[h] RA center of exposure")
	    printf("%.1h\n", deccen) | scan (s1)
	    nhedit (img, "DEC", s1, "[deg] DEC center of exposure")
	}

end


