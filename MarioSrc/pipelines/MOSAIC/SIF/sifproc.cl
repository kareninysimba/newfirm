#!/bin/env pipecl
#
# SIFPROC -- Process CCD amplifier data.
#
# The input is a list of all amplifiers from a single CCD and single exposure.
# For mosaic this list will either be one or two images.

int	status = 1
int	dx, dy, nsat, npix, saturate, ovscnmethod, order, namps, nextend
string	dataset, ilist, lfile, olist, image, nointerp, bpm, function
real	gain, gainnorm, dccdmean, x1, x2, x3, x4, x5
real	dqglmean, dqglsig, dqglfsat, min, max, mjd, dqovjprb, dqovmin
string  bpmlist, saturation, bleed, cast, rulepath
bool	oflag, tflag, zflag, dflag, fflag, sflag, bflag, pflag
struct	dqrej, ccdsum

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"

# Tasks and packages.
servers
images
mscred
task mergeamps = "msccmb$x_combine.e"
dataqual
cache mscred
cache average

# Define rules
findrule (dm, "ovscn_subtract_method", validate=1) | scan (rulepath)
task ovscn_subtract_method = (rulepath)

# Set file and path names.
sifnames (envget ("OSF_DATASET"))
dataset = sifnames.dataset
ilist   = sifnames.indir//dataset//".sif"
lfile   = sifnames.datadir//sifnames.lfile

mscred.logfile    = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = sifnames.uparm)
set (pipedata = sifnames.pipedata)
cd (sifnames.datadir)

# Log start of processing.
printf ("\nSIFPROC (%s): ", dataset) | tee (lfile)
time | tee (lfile)

dqrej = ""
list = ilist
for (namps=0; fscan(list,s1)!=EOF; namps+=1) {
    # Set output names.
    s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
    sifnames (s2)
    image = sifnames.image
    nointerp = sifnames.nointerp
    bpm = sifnames.bpm

    # There is a bug where the same name can appear more than once.  As
    # a workaround check here.
    if (imaccess(image))
        next
    ;

    # Convert to electrons/s.
    gainnorm = INDEF
    s2 = ""; hselect (s1, "flat", yes) | scan (s2)

    if (imaccess(s2) == YES ) {
        hselect (s2, "gainmean", yes) | scan (gain)
        if (nscan()==1) {
            hedit (s1, "GAINNORM", "("//gain//"/exptime)",
                add+, verify-, show-, update+)
	    hselect (s1, "GAINNORM", yes) | scan (gainnorm)
	    # if gainnorm exists in the header, ccdproc will convert the
	    # the data to electrons/s when flat fielding
        } else {
            printf("ERROR: could not read GAINMEAN from header (%s)\n", s1)
	    status = 0
	    break
        }
    }
    ;
#    } else {
#        printf("ERROR: could not open %s\n", s2)
#	status = 0
#	break
#    }
#    ;
    
    # Determine overscan properties, including possible jumps
    overscan( s1, exclude=5 )
    # Read the keywords from the header
    hsel( s1, "dqovmll,dqovmlj,dqovmjl,dqovmjj", yes ) |
        scan( x1, x2, x3, x4 )
    print (s1)
    hsel( s1, "mjd-obs,nextend", yes ) | scan( mjd, nextend )
    newDataProduct(image, mjd, "objectimage", "")
    storekeywords( class="objectimage", id=s1, sid=image, dm=dm )
    if (nscan()==4) {
        printf("%12.5e\n", x1 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmll", value=cast )
        printf("%12.5e\n", x2 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmlj", value=cast )
        printf("%12.5e\n", x3 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmjl", value=cast )
        printf("%12.5e\n", x4 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmjj", value=cast )
    }
    hsel( s1, "dqovmllr,dqovmljr,dqovmjlr,dqovmjjr", yes ) |
        scan( x1, x2, x3, x4 )
    if (nscan()==4) {
        printf("%12.5e\n", x1 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmllr", value=cast )
        printf("%12.5e\n", x2 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmljr", value=cast )
        printf("%12.5e\n", x3 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmjlr", value=cast )
        printf("%12.5e\n", x4 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmjjr", value=cast )
    }
    # Provide a failsafe value for dqovjprb. With dqovjprb set to
    # 1, the overscan will be subtracted line by line. This will
    # eliminate the possibility that a frame with a bias jump
    # will not be processed properly if dqovjprb is not read.
    dqovjprb = 1
    hsel( s1, "dqovjprb,dqovmin,dqovmax,dqovmean,dqovsig,dqovsmea,dqovssig",
        yes ) | scan( dqovjprb, dqovmin, x1, x2, x3, x4, x5 )
    if (nscan()==7) {
        printf("%12.5e\n", dqovjprb ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovjprb", value=cast )
        printf("%12.5e\n", dqovmin ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmin", value=cast )
        printf("%12.5e\n", x1 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmax", value=cast )
        printf("%12.5e\n", x2 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovmean", value=cast )
        printf("%12.5e\n", x3 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovsig", value=cast )
        printf("%12.5e\n", x4 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovsmea", value=cast )
        printf("%12.5e\n", x5 ) | scan( cast )
        setkeyval( class="objectimage", id=image, dm=dm,
            keyword="dqovssig", value=cast )


        # Do check for totally bad amplifier.
        if (x1<=1. || x2<=1. || x3<=1. || dqovmin<=0.) {
	    sendmsg("WARNING", "bad amp", image, "CAL")
	    dqrej = "bad amp"
            setkeyval( class="singleimage", id=image, dm=dm,
                keyword="dqrej", value=dqrej )
	    #next
        }
        ;
    }
    ;


    # Set overscan method.
    x = INDEF; hselect (s1, "EXPTIME", yes) | scan (x)
    ovscnmethod = 3; ovscn_subtract_method (x) | scan( ovscnmethod )
    order = 1
    function = "minmax"
    if ((ovscnmethod==1)||(ovscnmethod==4)) {
        order = 1
        function = "legendre"
    }
    ;
    if ((ovscnmethod==2)||(ovscnmethod==5)) {
        order = 100
        function = "legendre"
    }
    ;
    if (dqovjprb>0.5) {
        printf( "Jump in overscan detected\n", >> lfile )
        if (cl.verbose>=1)
            printf( "Jump in overscan detected\n" )
        ;
        if (ovscnmethod>=4) {
            order = 1
            function = "minmax"
        }
        ;
    }
    ;
    printf( "Using overscan subtraction method: %d\n", ovscnmethod )

    # Set processing flags.
    s2 = "OTZFSBP"
    hselect (s1, "CALOPS", yes) | scan (s2)
    oflag = (stridx("O",s2) > 0)
    tflag = (stridx("T",s2) > 0)
    zflag = (stridx("Z",s2) > 0)
    dflag = (stridx("D",s2) > 0)
    fflag = (stridx("EF",s2) > 0)
    sflag = (stridx("S",s2) > 0)
    bflag = (stridx("B",s2) > 0)
    pflag = (stridx("P",s2) > 0)
    if (sflag)
	saturation = _ccdtool.saturation
    else
	saturation = "INDEF"
    if (bflag)
	bleed = _ccdtool.bleed
    else
	bleed = "INDEF"

    # For ARCON binned data saturation results in zero values.  So in
    # order to handle saturation as usual we need to replace zero or
    # negative values.  We do this here rather than earlier so that
    # crosstalk works correctly (the zero values appear to not produce
    # crosstalk) and to parallelize this.
    #
    # We also have to rase the saturation value.

    ccdsum = ""; hselect (s1, "CCDSUM", yes) | scan (ccdsum)
    if (ccdsum != "1 1" && ccdsum != "") {
        imreplace (s1, 65535, lower=INDEF, upper=0., radius=0)
	saturation = 65000
    }
    ;

    # CCDPROC: The data will be converted to e/s if gainnorm is present.

    _ccdtool (s1, image, nointerp, bpm, proctype="object",
	fixpix=pflag, overscan=oflag, zerocor=zflag, darkcor=dflag,
	flatcor=fflag, sflatcor=no, saturation=saturation, bleed=bleed,
	function=function, order=order)

    # Was image processed?
    if (imaccess (image) == NO) {
        status = 0
	break
    }
    ;

    # Write overscan subtraction method to header
    hedit( image, "ovscnmtd", ovscnmethod, add+, ver- )
}
list = ""

if (status == 0)
    logout (status)
;

# Merge amplifiers if there are more than one amplifier per CCD.
# Otherwise, rename the file to follow the -ccd convention.
if (namps > 1) {
    sifnames (dataset)
    mergeamps ("*_im??.fits", sifnames.image, sifnames.bpm//".pl",
        amps-, verb+) | tee (lfile)
    if (imaccess(sifnames.bpm)==NO) {
	mergeamps ("*_im??_bpm*", sifnames.bpm, "", amps-, verb+) | tee (lfile)
	imcopy (sifnames.bpm//".fits", sifnames.bpm//".pl", v-)
	imdelete (sifnames.bpm//".fits")
    }
    ;
    hedit (sifnames.image, "nextend", nextend, add+, verify-, show-)
    hedit (sifnames.image, "ncombine", del+, verify-, show-)

    # Remove extensions.
    hselect ("*_im??.fits", "bpm", yes, > "sifproc1.tmp")
    imdelete ("*_im??.fits,@sifproc1.tmp")
    delete ("sifproc1.tmp")
} else {
    sifnames (dataset)
    imrename( "*_im??.fits", sifnames.image )
    hselect( sifnames.image, "bpm", yes, > s1 )
    s2 = substr (s1, strldx("/",s1)+1, 999 )
    printf("XXXX %s %s\n", s2, sifnames.bpm )
    printf("XXXX %s\n", s1 )
    imrename( bpm,  sifnames.bpm//".pl" )
    hedit( sifnames.image, "bpm", sifnames.bpm//".pl", add+, ver- )
}

# Enter new data product.
hsel( sifnames.image, "mjd-obs", yes ) | scan( mjd )
newDataProduct(sifnames.image, mjd, "objectimage", "")
storekeywords( class="objectimage", id=sifnames.image, sid=sifnames.image,
    dm=dm )

# TODO: note that dqglmean, dqglsig and dqglfsat are determined
# from the overscan corrected images in sclproc, whereas here
# they are determined from the overscan corrected, bias substracted,
# and flat fielded images. Either the names need to be different
# here, or the ccdtool above needs to be split in separate stages,
# so that dgl... etc can be determined the same way. However, for
# now these parameters are not used further in the pipeline, so
# leave as is.
# Determine mean and sigma and write to header

sifnames (dataset)
mefnames (sifnames.parent)
image = sifnames.image
bpm = sifnames.bpm

imstat( image, fields="mean,stddev,min,max", nclip=1, lsigma=2,
    usigma=2, format- ) | scan( dqglmean, dqglsig, min, max )
# Get saturation threshold from header, then determine fraction
# of saturated pixels
saturate = INDEF; hselect( image, "i_naxis[1],i_naxis[2],saturate", yes ) |
    scan (dx,dy,saturate)
if (isindef(saturate))
    sendmsg ("WARNING", "Keyword missing", "saturate", "KEY")
;
npix = dx*dy; nsat = 0
imstat( image, fields="npix", lower=saturate, nclip=0, format- ) | scan(nsat)
dqglfsat = (1.*nsat)/(1.*npix)
# Write data quality data to log file ...
printf( "DQ | image: %s\n", image )
if (dqrej != "")
    printf( "DQ | reject: %s\n", dqrej )
;
printf( "DQ | global mean: %f\n", dqglmean )
printf( "DQ | global sigma: %f\n", dqglsig )
printf( "DQ | fraction of saturated pixels: %f\n", dqglfsat )
# ... to header ...
if (dqrej != "") {
    hedit( image, "dqrej", dqrej, add+, ver- )
    line = ""; hselect (image, "SIFERR", yes) | scan (line)
    line = line // "B"
    hedit (image, "SIFERR", line, add+, show+)
    status = 2
}
;
hedit( image, "dqglfsat", dqglfsat, add+, ver- )
hedit( image, "dqglmean", dqglmean, add+, ver- )
hedit( image, "dqglsig", dqglsig, add+, ver- )
# ... and to PMAS
if (dqrej != "")
    setkeyval( class="objectimage", id=image, dm=dm,
	keyword="dqrej", value=dqrej )
;
printf("%12.5e\n", dqglmean ) | scan( cast )
setkeyval( class="objectimage", id=image, dm=dm,
    keyword="dqglmean", value=cast )
printf("%12.5e\n", dqglsig ) | scan( cast )
setkeyval( class="objectimage", id=image, dm=dm,
    keyword="dqglsig", value=cast )
printf("%12.5e\n", dqglfsat ) | scan( cast )
setkeyval( class="objectimage", id=image, dm=dm,
    keyword="dqglfsat", value=cast )

# Since it is possible to have saturated pixel detected in the
# vignetted area we reset those pixels back to vignetted.
# Look at mask histogram (ie for sky level above saturation).
if (imaccess(bpm//".pl")) {
    hselect (image, "BPMCCD", yes) | scan (s1)
    imexpr ("a==3?a:b", "sifproc.pl", s1, bpm//".pl", verb-)
    rename ("sifproc.pl", bpm//".pl")
    imhistogram (bpm//".pl", z1=0, z2=10, bin=1, list+, > "sifproc.tmp")
    fields ("sifproc.tmp", "2", lines="1") | average (> "dev$null")
    i = average.sum
    fields ("sifproc.tmp", "2", lines="2,3,5,6") | average (> "dev$null")
    j = average.sum
    fields ("sifproc.tmp", "2", lines="4") | average (> "dev$null")
    k = average.sum
    delete ("sifproc.tmp")
    printf ("%s: Good = %d, Bad = %d, Vignetted = %d\n", bpm, i, j, k, >> lfile)
    x1 = 1.0*j/(1.0*(i+j))
    printf("%d\n", x1 ) | scan( cast )
    setkeyval( class="objectimage", id=image, dm=dm,
	keyword="dqobfrbp", value=cast )
    if (i < j) {
	if (dqrej == "")
	    dqrej = "saturated"
	else
	    dqrej = dqrej + " saturated"
	setkeyval( class="objectimage", id=image, dm=dm,
	    keyword="dqrej", value=dqrej )
	printf ("WARNING: More than half the pixels are flagged as bad.\n") |
	    tee (lfile)
	line = ""; hselect (image, "SIFERR", yes) | scan (line)
	line = line // "S"
	hedit (image, "SIFERR", line, add+, show+)
	hedit( image, "dqrej", dqrej, add+, ver- )
	status = 2
    }
    ;
}
;

# Reset for gain change.
if (isindef(gainnorm) == NO) {
    hedit (image, "gain1", "1.", show+, verify-, update+, >> lfile)
    hedit (image, "rdnoise1", "(rdnoise*gainnorm)", show+, verify-, update+,
        >> lfile)
    hedit (image, "exptime1", "1.", show+, verify-, update+, >> lfile)
}
;

# Set names for IMCMB keywords.
hedit (image, "IMCMBSIF", sifnames.shortname, add+, verify-, show-)
hedit (image, "IMCMBMEF", mefnames.shortname, add+, verify-, show-)

if (status != 0) {
    # Delete input data.
    imdelete ("@"//ilist)

    # Make new list of processed data.
    pathnames (sifnames.image, > ilist)
}
;

logout (status)
