#!/bin/env pipecl
#
# SIFHDR -- Set header with information from the MEF WCS solutions.

string	cast, dataset, datadir, lfile, image, procid
real	mz, mze, pixscale, seeing
real	skyadu, skymag, skynoise, dpthadu, photdpth, x1

# Set dataset directory.
sifnames (envget("OSF_DATASET"))
dataset = sifnames.dataset
datadir = sifnames.datadir
lfile = datadir // sifnames.lfile
image = sifnames.image
set (uparm = sifnames.uparm)
cd (sifnames.datadir)

# Log start of processing.
printf ("\nSIFHDR (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Load packages and tasks.
images
imcoords
ace
mario
servers

procid = envget ("NHPPS_MODULE_PID") // "_" // dataset

# Set WCS.
if (access (sifnames.wcsdb)) {
    ccsetwcs (image, sifnames.wcsdb, "wcs", >> lfile)
    ccsetwcs (sifnames.bpm, sifnames.wcsdb, "wcs", >> lfile)
    ccsetwcs (sifnames.obm//"[pl]", sifnames.wcsdb, "wcs", >> lfile)
    acesetwcs (sifnames.cat, sifnames.wcsdb, "wcs", >> lfile) 
} else
    printf ("  WARNING: No WCS database available\n") | tee (lfile)

# Update header.
if (access ("mefwcs.hdr")) {
    hedit (image, "WCSCAL", "(1=1)", add+, show-)

    list = "mefwcs.hdr"
    while (fscan (list, s1, line) != EOF) {
        if (line == "")
	    next
	;
	
	hedit (image, s1, line, add+, show+, >> lfile)
	thedit (sifnames.cat, s1, line, >> lfile)
    }
    list = ""

    # Update catalog magnitudes.
    thselect (sifnames.cat, "MAGZERO,MAGZREF", yes) | scan (mz, mze)
    mze = mz - mze
    dmag (sifnames.cat, mze, fields="MAG", >> lfile)
} else {
    # We should clean up the header but for now just a quick hack to add
    # a keyword indicating a WCS failure.

    hedit (image, "WCSCAL", "(1=2)", add+)
}
	    
if (access ("mefacedq.hdr")) {
    list = "mefacedq.hdr"
    while (fscan (list, s1, line) != EOF) {
	if (line == "")
	    next
	;

	hedit (image, s1, line, add+, show+, >> lfile)
	if (s1 == "SFTFLAG" || s1 == "FRGFLAG" || s1 == "PGRFLAG")
	    hedit (sifnames.obm//"[pl]", s1, line, add+, show+, >> lfile)
	;
    }
    list = ""
}
;

# Get data for conversions to magnitude and arcseconds.
hselect (image, "$MAGZERO,$PIXSCALE", yes ) | scan(mz, pixscale)

# Set seeing, skymag, and photometric depth.
hselect (image, "$SEEINGP1", yes) | scan (seeing)
if (isindef(pixscale)==NO && isindef(seeing)==NO) {
    seeing = pixscale * seeing
    printf ("%.2f\n", seeing) | scan (seeing)
    nhedit (image, "SEEING1", seeing, "[arcsec] seeing", add+)
}
;
hselect (image, "$SKYADU1", yes) | scan (skyadu)
if (isindef(mz)==NO && isindef(pixscale)==NO && isindef(skyadu)==NO) {
    if (skyadu > 0) {
	skymag =  mz - 2.5 * log10 (skyadu/pixscale**2)
	printf ("%.2f\n", skymag) | scan (skymag)
	nhedit (image, "SKYMAG1", skymag, "[mag/arcsec^2] sky brightness", add+)
    }
    ;
}
;
hselect (image, "$DPTHADU1", yes) | scan (dpthadu)
if (isindef(mz)==NO && isindef(dpthadu)==NO) {
    if (dpthadu > 0) {
	photdpth = mz - 2.5 * log10 (dpthadu)
	printf ("%.2f\n", photdpth) | scan (photdpth)
	nhedit (image, "PHTDPTH1", photdpth, "[mag] photometric depth", add+)
    }
    ;
}
;
hselect (image, "$SEEINGP", yes) | scan (seeing)
if (isindef(pixscale)==NO && isindef(seeing)==NO) {
    seeing = pixscale * seeing
    printf ("%.2f\n", seeing) | scan (seeing)
    nhedit (image, "SEEING", seeing, "[arcsec] seeing", add+)
}
;
hselect (image, "$SKYADU", yes) | scan (skyadu)
if (isindef(mz)==NO && isindef(pixscale)==NO && isindef(skyadu)==NO) {
    if (skyadu > 0) {
	skymag =  mz - 2.5 * log10 (skyadu/pixscale**2)
	printf ("%.2f\n", skymag) | scan (skymag)
	nhedit (image, "SKYMAG", skymag, "[mag/arcsec^2] sky brightness", add+)
    }
    ;
}
;
hselect (image, "$DPTHADU", yes) | scan (dpthadu)
if (isindef(mz)==NO && isindef(dpthadu)==NO) {
    if (dpthadu > 0) {
	photdpth = mz - 2.5 * log10 (dpthadu)
	printf ("%.2f\n", photdpth) | scan (photdpth)
	nhedit (image, "PHOTDPTH", photdpth, "[mag] photometric depth", add+)
    }
    ;
}
;

# Compute additional depth quantities for the SIF.
hselect (image, "$SKYNOISE1,$MAGZERO1,$MAGZERR1", yes) |
    scan (skynoise, x1, mze)
if (isindef(pixscale)==NO && isindef(mz)==NO &&
    isindef(seeing)==NO && isindef(skynoise)==NO) {

    # Set PHOTDPTH keyword requested by T. Lauer.
    # 5 sigma threshold and an optimal aperture of 1.35 FWHM
    # (which is formally optimal for pure Gaussian FWHM).
    # Later in MEFHDR the average of these will be computed for
    # the global photometric depth.

    photdpth (skynoise, seeing, mz, pixscale, verbose=verbose)

    # Send to PMASS
    printf("%12.5e\n", x1 ) | scan( cast )
    setkeyval( dm, "objectimage", image, "dqphzp", cast )
    printf("%12.5e\n", mze ) | scan( cast )
    setkeyval( dm, "objectimage", image, "dqphezp", cast )
    printf("%12.5e\n", photdpth.dqphdpps ) | scan( cast )
    setkeyval( dm, "objectimage", image, "dqphdpps", cast )
    printf("%12.5e\n", photdpth.dqphdpap ) | scan( cast )
    setkeyval( dm, "objectimage", image, "dqphdpap", cast )
    printf("%12.5e\n", photdpth.dqphapsz ) | scan( cast )
    setkeyval( dm, "objectimage", image, "dqphapsz", cast )
    printf("%12.5e\n", photdpth.dqphdppx ) | scan( cast )
    setkeyval( dm, "objectimage", image, "dqphdppx", cast )
}
;

logout 1
