# SIFNAMES -- Directory and filenames for the SIF pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure sifnames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline name"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file	parent			{prompt = "Parent part of dataset"}
file	child			{prompt = "Child part of dataset"}
file	lfile			{prompt = "Log file"}
file	image			{prompt = "Primary image name"}
file	nointerp		{prompt = "No interpolation image names"}
file	bpm			{prompt = "Bad pixel mask"}
file	sky			{prompt = "Sky map"}
file	sig			{prompt = "Sigma map"}
file	obm			{prompt = "Object mask"}
file	cat			{prompt = "Catalog"}
file	mcat			{prompt = "USNO matched catalog"}
file	wcsdb			{prompt = "WCS database"}
file	blk1			{prompt = "Block averaged image"}
file	blk2			{prompt = "Block averaged image"}
file	bpmblk1			{prompt = "Block averaged mask"}
file	bpmblk2			{prompt = "Block averaged mask"}
string	pattern = ""		{prompt = "Pattern"}
bool	base = no		{prompt = "Child part of dataset"}

begin
	# Set generic names.
	names ("sif", name, pattern=pattern, base=base)
	dataset = names.dataset
	pipe = names.pipe
	shortname = names.shortname
	rootdir = names.rootdir
	indir = names.indir
	datadir = names.datadir
	uparm = names.uparm
	pipedata = names.pipedata
	parent = names.parent
	child = names.child
	lfile = names.lfile

	# Set pipeline specific names.
	image = shortname
	nointerp = shortname // "_noi"
	wcsdb = shortname // ".wcsdb"
	blk1 = image // "_blk8"
	blk2 = image // "_blk16"

	bpm = shortname // "_bpm"
	sky = shortname // "_sky"
	sig = shortname // "_sig"
	obm = shortname // "_obm"
	cat = shortname // "_cat"
	mcat = shortname // "_mcat"
	bpmblk1 = bpm // "_blk8"
	bpmblk2 = bpm // "_blk16"

	if (pattern != "") {
	    image += ".fits"
	    blk1 += ".fits"
	    blk2 += ".fits"
	    bpm += ".pl"
	    sky += ".fits"
	    sig += ".fits"
	    obm += ".fits"
	    bpmblk1 += ".pl"
	    bpmblk2 += ".pl"
	}
end
