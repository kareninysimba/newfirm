/*

  overscan.c

  How to compile with graphical output (set DOGRAPH):
  g77 overscan.c -o overscan -Wall -lcfitsio -lcpgplot -lpgplot -L/usr/X11R6/lib -lX11 -lm -m32

  Without graphical output (do not set DOGRAPH - default):
  g77 overscan.c -o overscan -Wall -lcfitsio -lm

  Description:
  This program detects jumps in the overscan

  HISTORY:
  5/21/04    Document created
  8/19/04    Test version 0.1 ready
  4/13/05    Version 1.0 ready

*/

#ifdef DOGRAPH
#include <cpgplot.h>
#endif

#include <fitsio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* declare global variables */
char programname[120];           /* name under which this program is run */
int verbose = 0;                 /* default level for verbose */

/* declare functions */
void flag(int num);
void error(char *message, int giveusage);
void usage( void );
void pop(char **elements, int num, int *max);
float sqr(float x);
void getstats( float *array, long numitems, float *mean, float *sigma, float *min, float *max );
void getstatsfilter(float *array, long numitems, float low, float high, int niter, float *mean, float *sigma, float *min, float *max );
float getchance( int length, float ampl );

/* main body of the program */
int main(int argc, char **argv) {

   /* general variable */
   char buffer[80];              /* character buffer */
   char message[256];            /* message string */
   char *stop;                   /* string needed for string-to-float conversion */
   int i,j;                      /* counters */
   float low,middle,high;        /* low and high */
   float min, max, mean, sigma;  /* statistics */
   float dummy;                  /* dummy variable */
   
   /* variables related to command line options */
   int edef = 0;                 /* variables related to edge exclusion */
   int exclude = 0;
   int gdef=0;                   /* variables related to gain header keyword */
   char keygain[120];
   int odef=0;                   /* variables related to overscan header keyword */
   char keyoverscan[120];
   int rdef=0;                   /* variables related to rdnoise header keyword */
   char keyrdnoise[120];
   int tdef=0;                   /* variables related to threshold */
   float threshold;              /* sigma clipping level */
   int wdef=0;                   /* variables related to template width */
   int ntemplate;                /* half the number of points in template */

   /* variables related to reading of fits files */
   int status = 0;               /* status must always be initialized = 0  */
   int bitpix;                   /* bytes per pixel */
   int naxis;                    /* number of axes in FITS file */
   long naxes[2] = {1,1};        /* lower limit of pixels to be read */
   long laxes[2];                /* upper limit of pixels to be read */
   long incr[2] = {1,1};         /* increment between pixels to be read */

   /* variables related to input fits file */
   fitsfile *INFITS;             /* fits file pointers defined in fitsio.h */
   long nx,ny;                   /* variables related to image size */
   char fitsfilename[512];       /* fits file name */
   float *data;                  /* array for overscan data */
   long ndat;                    /* number of data elements in array */
   float gain;                   /* gain as read from fits header */
   float rdnoise;                /* rdnoise as read from fits header */

   /* variables related to structure detection in overscan region */
   int ns;                       /* number of points in strip */
   float *strip;                 /* array for collapsed overscan region */
   float *strap;                 /* array to be collapsed */
   float thesigma,themean;       /* mean and noise in strip after filtering */
   int startpos,endpos;          /* start and end of current jump */
   int length;                   /* length of current jump */
   int maxlength,rmaxlength;     /* maximum jump length */
   float numjumps;               /* number of jumps encountered */
   int injump;                   /* true if in a jump */
   float jump;                   /* amplitude of current jump */
   float jumpatmax,rjumpatmax;   /* amplitude of jump with maximum length */
   float maxjump,rmaxjump;       /* maximum amplitude of jump */
   int maxjumplength;            /* length of maximum jump */
   int rmaxjumplength;           /* length of maximum jump */
   float chance,mchance;         /* probability this is a jump */
   float glmean,glsigma;         /* mean and sigma in overscan region */

   /* variables related to structure detection through cross correlation */
   float *cc,*cc2;               /* array for cross correlation */
   float *template,*template2;   /* array for template */
   int peaktype;                 /* indicates peak or valley in cc */
   int peakpos,oldpeakpos;       /* position of peak */
   double sumx,sumy,sumxx,sumxy,sumyy; /* least-squares fitting */
   float avx,avy,lsqa,lsqb,lsqr,chi2;
   float thex[100],they[100];

#ifdef DOGRAPH
   /* variables related to plotting */
   float *xstrip;                /* array for x-axis */
#endif

   strcpy( programname, argv[0] ); /* store program name in global variable */

   /* Process command line options */
   i = argc-1;
   while (i>0) { /* Make a first loop to find -v arguments to determine verbose level */
      if ( strcmp(argv[i],"-v") == 0 ) {
	 verbose++; /* increase verbosity */
	 pop(argv,i,&argc); /* remove from argument list */
      }
      i--;
   }
   if (verbose>=1) printf("Verbose level: %i (out of 4)\n", verbose);
   if (verbose>=2) printf("Processing command line options:\n");
   if (verbose>=2) printf("   Number of command line arguments: %i\n", argc);
   i = argc-1;
   while (i>=1) { /* begin loop over command line arguments */
      if ( strcmp(argv[i], "-e")==0 ) {
	 if (i+1<argc) { /* Need one more argument for edge exclusion */
 	    exclude = (int) strtod(argv[i+1], &stop);
	    if ( stop==argv[i+1] ) { /* check for conversion error */
	       sprintf(message, "%s is not a valid argument for -e\n", stop); 
	       error(message,0);
	    }
	    if (verbose>=2) printf("   Edge exclusion: %i\n", exclude);
	    edef = 1;
	    pop(argv,i,&argc);
	    pop(argv,i,&argc);
	 } else error("-e is missing its argument",0);
      }
      if ( strcmp(argv[i], "-h")==0) /* help is asked, so give it and exit */
	 usage();
      if ( strcmp(argv[i], "-g")==0 ) {
	 if (i+1<argc) { /* Need one more argument for gain header keyword */
	    if (strcspn(argv[i+1],"-")==0) error("-g is missing its argument",0);
	    else strcpy(keygain,argv[i+1]);
	    if (verbose>=2) printf("   Reading gain from keyword: %s\n", keygain);
	    gdef = 1;
	    pop(argv,i,&argc);
	    pop(argv,i,&argc);
	 } else error("-g is missing its argument",0);
      }
      if ( strcmp(argv[i], "-o")==0 ) {
	 if (i+1<argc) { /* Need one more argument for overscan header keyword */
	    if (strcspn(argv[i+1],"-")==0) error("-o is missing its argument",0);
	    else strcpy(keyoverscan,argv[i+1]);
	    if (verbose>=2) printf("   Reading overscan region from keyword: %s\n", keyoverscan);
	    odef = 1;
	    pop(argv,i,&argc);
	    pop(argv,i,&argc);
	 } else error("-o is missing its argument",0);
      }
      if ( strcmp(argv[i], "-r")==0 ) {
	 if (i+1<argc) { /* Need one more argument for rdnoise header keyword */
	    if (strcspn(argv[i+1],"-")==0) error("-r is missing its argument",0);
	    else strcpy(keyoverscan,argv[i+1]);
	    if (verbose>=2) printf("   Reading read noise from keyword: %s\n", keyoverscan);
	    rdef = 1;
	    pop(argv,i,&argc);
	    pop(argv,i,&argc);
	 } else error("-r is missing its argument",0);
      }
      if ( strcmp(argv[i], "-t")==0 ) {
	 if (i+1<argc) { /* Need one more argument for threshold */
 	    threshold = (float) strtod(argv[i+1], &stop);
	    if ( stop==argv[i+1] ) { /* check for conversion error */
	       sprintf(message, "%s is not a valid argument for -t\n", stop); 
	       error(message,0);
	    }
	    if (verbose>=2) printf("   Threshold: %f (sigma)\n", threshold);
	    tdef = 1;
	    pop(argv,i,&argc);
	    pop(argv,i,&argc);
	 } else error("-t is missing its argument",0);
      }
      if ( strcmp(argv[i], "-w")==0 ) {
	 if (i+1<argc) { /* Need one more argument for width */
 	    ntemplate = (int) (strtod(argv[i+1], &stop)+0.5);
	    if ( stop==argv[i+1] ) { /* check for conversion error */
	       sprintf(message, "%s is not a valid argument for -w\n", stop); 
	       error(message,0);
	    }
	    if (verbose>=2) printf("   Half the template width: %i (pixels)\n", ntemplate);
	    wdef = 1;
	    pop(argv,i,&argc);
	    pop(argv,i,&argc);
	 } else error("-w is missing its argument",0);
      }
      i--;
   } /* end loop over command line arguments */

   /* After the known options have been processed, one argument, the  
      input fits file, should remain. */
   /* List the remaining arguments */
   if (verbose>=2) printf("   Remaining command line arguments: %i ", argc-1);
   if ((verbose>=2)&&(argc>1)) {
      for(i=1;i<argc;i++) { printf("%s ",argv[i]); }
      printf("\n");
   }
   if (argc>2) { /* Check whether one argument remains */
      error("too many or unknown command line arguments given",1);
   } else if (argc<2) error("no input file name given",1);
   /* One command line argument left, copy to string */
   strcpy(fitsfilename, argv[1]);
   if (verbose>=2) {
      printf("   Input fits file: %s\n", fitsfilename );
   }

   /* Check whether all the necessary information is available. 
      Provide defaults where not, or give an error message. */
   if (gdef==0) strcpy(keygain,"gain");
   if (odef==0) strcpy(keyoverscan,"biassec");
   if (rdef==0) strcpy(keyrdnoise,"rdnoise");
   if (tdef==0) threshold = 5;
   if (wdef==0) ntemplate = 10;
   if (edef==0) { exclude = 0; edef = 1; }
   if (exclude<0) exclude = 0;

   /* Open data fits file and obtain basic information */
   fits_open_file(&INFITS, fitsfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(INFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(INFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(INFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",fitsfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }
   /* Get the gain and readnoise from header */
   fits_read_keyword(INFITS, keyrdnoise, buffer, NULL, &status); 
   if (status!=0) {
     sprintf( message, "Could not read header keyword %s", keyrdnoise );
     error( message, 0 );
   }
   rdnoise = (float) strtod(buffer, &stop); /* convert string to float */
   /* check for conversion error */
   if ( strlen(stop)!=0 ) error("cannot convert keyword rdnoise to float", 0);
   fits_read_keyword(INFITS, keygain, buffer, NULL, &status); 
   if (status!=0) {
     sprintf( message, "Could not read header keyword %s", keygain );
     error( message, 0 );
   }
   gain = (float) strtod(buffer, &stop); /* convert string to float */
   /* check for conversion error */
   if ( strlen(stop)!=0 ) error("cannot convert keyword gain to float", 0);
   if (verbose>=2) printf( "Gain: %f  Read noise: %f\n", gain, rdnoise );
   /* Get the overscan region from header */
   fits_read_keyword(INFITS, keyoverscan, buffer, NULL, &status); 
   if (status!=0) {
     sprintf( message, "Could not read header keyword %s", keyoverscan );
     error( message, 0 );
   }
   if (verbose>=1)
     printf("Overscan region (from header keyword %s): %s\n", keyoverscan, buffer );
   /* convert the overscan string to numbers */
   sscanf( buffer, "\'[%li:%li,%li:%li]\'", &naxes[0], &laxes[0], &naxes[1], &laxes[1] );
   if (verbose>=3) {
      printf( "Values as scanned from %s: %li %li %li %li\n", keyoverscan, naxes[0], naxes[1], laxes[0], laxes[1] );
   }
   /* set array dimensions and number of elements */
   nx = laxes[0]-naxes[0]+1;
   ny = laxes[1]-naxes[1]+1;
   /* Adjust corners for edge exclusion, along the longest axis only */
   if (nx<ny) {
     laxes[1] -= 2*exclude;
     naxes[1] += exclude;
     ny = laxes[1]-naxes[1]+1;
   } else {
     laxes[0] -= 2*exclude;
     naxes[0] += exclude;
     nx = laxes[0]-naxes[0]+1;
   }
   ndat = nx*ny;
   if (verbose>=3) printf("nx: %li  ny: %li\n", nx, ny );
   /* Allocate memory for data array */
   data = (float *)calloc((unsigned) (ndat), sizeof(float));
   if (data == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",0);
   }
   /* Read the fits file into an array */
   fits_read_subset(INFITS, TFLOAT, naxes, laxes, incr, NULL, data, NULL, &status);
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=1)
     printf("Number of pixels read: %li\n", ndat );

   /* Determine basic statistics */
   if (verbose>=1) printf("Expected noise (readnoise/gain): %f\n", rdnoise/gain );
   getstats( data, ndat, &mean, &sigma, &min, &max );
   if (verbose>=1) printf("Before clipping\t%f %f %f %f\n", mean, sigma, min, max );
   /* Determine basic statistics after filtering out outlyers */
   getstatsfilter( data, ndat, 5, 5, 1, &mean, &sigma, &min, &max );
   if (verbose>=1) printf("After clipping\t%f %f %f %f\n", mean, sigma, min, max );
   glmean = mean;
   glsigma = sigma;

   /* Now try and detect banding/bias jumps. To that end, collapse the
      bias strip into a one-dimensional array (collapsing along the
      shortest axis). */
   if (nx<ny) {
     strip = (float *)calloc((unsigned) (ny), sizeof(float));
     strap = (float *)calloc((unsigned) (nx), sizeof(float));
     for(i=0;i<ny;i++) {
       for(j=0;j<nx;j++) {
	 strap[j] = data[(i*nx)+j];
       }
       getstats( strap, nx, &mean, &sigma, &min, &max );
       getstatsfilter( strap, nx, 5, 5, 1, &mean, &sigma, &min, &max );
       strip[i] = mean;
     }
     ns = ny;
   } else {
     strip = (float *)calloc((unsigned) (nx), sizeof(float));
     strap = (float *)calloc((unsigned) (ny), sizeof(float));
     for(i=0;i<nx;i++) {
       for(j=0;j<ny;j++) {
	 strap[j] = data[(j*nx)+i];
       }
       getstats( strap, nx, &mean, &sigma, &min, &max );
       getstatsfilter( strap, ny, 5, 5, 1, &mean, &sigma, &min, &max );
       strip[i] = mean;
     }
     ns = nx;
   }

   /* Calculate statistics in the strip */
   getstats( strip, ns, &themean, &thesigma, &min, &max );
   if (thesigma==0) {
     /* Values in strip are constant, so we cannot do much. Write keywords to header
	amd exit cleanly */
     dummy = 0;
     fits_update_key(INFITS, TFLOAT, "dqovmll", &dummy, "length of longest jump", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmlj", &dummy, "amplitude of longest jump", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmjl", &dummy, "length of highest jump", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmjj", &dummy, "amplitude of highest jump", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmllr", &dummy, "length of longest jump down", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmljr", &dummy, "depth of longest jump down", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmjlr", &dummy, "length of deepest jump down", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmjjr", &dummy, "amplitude of deepest jump", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovjprb", &dummy, "probability of a jump", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmin", &min, "min in overscan region", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmax", &max, "max in overscan region", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmean", &glmean, "mean in overscan region", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovmean", &glmean, "mean in overscan region", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovsig", &glsigma, "sigma in overscan region", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovsmea", &themean, "mean in collapsed overscan strip", &status); 
     fits_update_key(INFITS, TFLOAT, "dqovssig", &thesigma, "sigma in collapsed overscan strip", &status); 
     /* Close the fits file */
     fits_close_file( INFITS, &status );
     if (status) { /* if error occured, print error message and exit */
       fits_report_error(stderr, status);
       return(status);
     }
     /* free memory */
     free( (float *)data );
     free( (float *)strip );
     free( (float *)strap );
     exit(1);
   }

   if (verbose>=1) printf("temp\t%f %f %f %f\n", themean, thesigma, min, max );
   getstatsfilter( strip, ns, 3, 3, 5, &themean, &thesigma, &min, &max );
   if (verbose>=1) printf("After averaging\t%f %f %f %f\n", themean, thesigma, min, max );

   /* Define viewing area for graphical output, and open graphical window */
   low = min-5*thesigma;
   high = max+5*thesigma;
   middle = (high+low)/2;

#ifdef DOGRAPH
   cpgopen("/xserve");
   cpgask(0);
   cpgenv( 1900, 2100, low, high, 0, 0 );
   cpgenv( -5, ns+5, low, high, 0, 0);
   cpglab("Spectral axis (pixels)", "Counts", "");
   xstrip = (float *)calloc((unsigned) (ny), sizeof(float));
   for(i=0;i<ns;i++) xstrip[i] = (float)i;
   cpgline( ns, xstrip, strip );
#endif

   /* Try by template matching, let the template be of the form
      -1 -1 -1 1 1 1 */
   /* Define the template */
   template = (float *)calloc((unsigned) (ntemplate*2), sizeof(float));
   for(i=0;i<ntemplate;i++) {
     template[i] = -1;
     template[i+ntemplate] = 1;
   }
   /* Second template is a straight line */
   template2 = (float *)calloc((unsigned) (ntemplate*2), sizeof(float));
   for(i=0;i<2*ntemplate;i++) {
     template2[i] = -1+2*((float)i)/((float)(2*ntemplate-1));
   }
   /* Allocate space for cross correlation results */
   cc = (float *)calloc((unsigned) (ns), sizeof(float));
   cc2 = (float *)calloc((unsigned) (ns), sizeof(float));
   /* Do the cross correlation with both templates */
   for(i=ntemplate;i<ns-ntemplate;i++) {
     /* First determine local mean */
     mean = 0;
     for(j=-ntemplate;j<ntemplate;j++)
       mean += strip[i+j];
     mean /= (float)(ntemplate*2);
     /* Now cross correlate */
     for(j=-ntemplate;j<ntemplate;j++) {
       cc[i] += (strip[i+j]-mean)*template[j+ntemplate];
       cc2[i] += (strip[i+j]-mean)*template2[j+ntemplate];
     }
   }
   /* Now find the edges, which are associated with peaks in cc */
   injump = numjumps = maxjump = rmaxjump = 0;
   length = maxlength = rmaxlength = 0;
   maxjumplength = rmaxjumplength = 0;
   peaktype = 0;
   /* Correct threshold for number of points because noise increases */
   threshold *= sqrt((float)(ntemplate*2)); 
#ifdef DOGRAPH
   cpgsci(3);
#endif
   for(i=ntemplate;i<ns-ntemplate;i++) { /* loop over CC results */
     if ((cc[i]>threshold*thesigma)&&(peaktype!=1)) { /* start of a peak */
       injump = 1;
       peaktype = 1;
       startpos = i;
     }
     if ((cc[i]<threshold*thesigma)&&(peaktype==1)) { /* end of a peak */
       endpos = i;
       oldpeakpos = peakpos;
       peakpos = (endpos+startpos)/2;
       length = peakpos - oldpeakpos;
       jump = cc[peakpos]/ntemplate;
       if (jump>rmaxjump) { /* record maximum jump */
	 rmaxjump = jump;
	 rmaxjumplength = length;
       }
       /* A peak was found, fit a line around the peak */
       sumx = sumy = sumxy = sumxx = sumyy = 0;
       for(j=peakpos-ntemplate;j<peakpos+ntemplate;j++) {
	 sumx += (float)j;
	 sumy += strip[j];
       }
       avx = sumx/((float)(2*ntemplate));
       avy = sumy/((float)(2*ntemplate));
       for(j=peakpos-ntemplate;j<peakpos+ntemplate;j++) {
	 sumxy += ((float)j-avx)*(strip[j]-avy);
	 sumxx += ((float)j-avx)*((float)j-avx);
	 sumyy += (strip[j]-avy)*(strip[j]-avy);
       }
       lsqa = sumxy/sumxx;
       lsqb = avy-lsqa*avx;
       lsqr = fabs(sumxy/sqrt(sumxx*sumyy));
       /* Determine the chi2 for the straight line fit */
       chi2 = 0;
       for(j=peakpos-ntemplate;j<peakpos+ntemplate;j++) {
	 chi2 += pow((strip[j]-(lsqa*((float)j)+lsqb))/thesigma,2);
	 thex[j-peakpos+ntemplate] = (float)j;
	 they[j-peakpos+ntemplate] = lsqa*((float)j)+lsqb;
       }
       chi2 /= ntemplate*2-1;
       if ((endpos-startpos>1)&&(chi2>0.5)) { /* this is deemed to be a jump */
	 numjumps += 0.5; /* increase jump counter */
	 if (length>rmaxlength) { 
	   rmaxlength = length; /* record maximum length */
	   rjumpatmax = jump;
	 }
#ifdef DOGRAPH
	 /* Mark jump in graphical output */
	 cpgmove(peakpos,(high-low)*0.333+low);
	 cpgdraw(peakpos,(high-low)*0.667+low);
#endif
       } else { /* this is not a jump */
	 peakpos = oldpeakpos; /* restore to original value */
       }
       peaktype = 0;
     }
     if ((cc[i]<-threshold*thesigma)&&(peaktype!=-1)) { /* start of a valley */
       injump = 1;
       peaktype = -1;
       startpos = i;
     }
     if ((cc[i]>-threshold*thesigma)&&(peaktype==-1)) { /* end of a valley */
       endpos = i;
       oldpeakpos = peakpos;
       peakpos = (endpos+startpos)/2;
       length = peakpos - oldpeakpos;
       jump = -cc[peakpos]/ntemplate;
       if (jump>maxjump) { /* record maximum jump */
	 maxjump = jump;
	 maxjumplength = length;
       }
       /* A peak was found, fit a line around the peak */
       sumx = sumy = sumxy = sumxx = sumyy = 0;
       for(j=peakpos-ntemplate;j<peakpos+ntemplate;j++) {
	 sumx += (float)j;
	 sumy += strip[j];
       }
       avx = sumx/((float)(2*ntemplate));
       avy = sumy/((float)(2*ntemplate));
       for(j=peakpos-ntemplate;j<peakpos+ntemplate;j++) {
	 sumxy += ((float)j-avx)*(strip[j]-avy);
	 sumxx += ((float)j-avx)*((float)j-avx);
	 sumyy += (strip[j]-avy)*(strip[j]-avy);
       }
       lsqa = sumxy/sumxx;
       lsqb = avy-lsqa*avx;
       lsqr = fabs(sumxy/sqrt(sumxx*sumyy));
       /* Determine the chi2 for the straight line fit */
       chi2 = 0;
       for(j=peakpos-ntemplate;j<peakpos+ntemplate;j++) {
	 chi2 += pow((strip[j]-(lsqa*j+lsqb))/thesigma,2);
	 thex[j-peakpos+ntemplate] = (float)j;
	 they[j-peakpos+ntemplate] = lsqa*((float)j)+lsqb;
       }
       chi2 /= ntemplate*2-1;
       if ((endpos-startpos>1)&&(chi2>0.5)) { /* this is deemed to be a jump */
	 numjumps += 0.5;
	 if (length>maxlength) { /* record maximum length */
	   maxlength = length;
	   jumpatmax = jump;
	 }
#ifdef DOGRAPH
	 /* Mark jump in graphical output */
	 cpgmove(peakpos,(high-low)*0.333+low);
	 cpgdraw(peakpos,(high-low)*0.667+low);
#endif
       } else { /* this is not a jump */
	 peakpos = oldpeakpos; /* restore to original value */
       }
       peaktype = 0;
     }
   }
   if (verbose>=1) { /* print out some statistics */
     printf( "Number of jumps: %f\n", numjumps );
     printf( "max length: %i   ampl: %f\n", maxlength, jumpatmax );
     printf( "max ampl %f length %i \n", maxjump, maxjumplength );
     printf( "RRmax length: %i   ampl: %f\n", rmaxlength, rjumpatmax );
     printf( "RRmax ampl %f length %i \n", rmaxjump, rmaxjumplength );
   }

   /* Determine the probability this is a jump */
   chance = getchance( maxlength, jumpatmax/thesigma );
   mchance = getchance( maxjumplength, maxjump/thesigma );
   if (mchance>chance) { chance = mchance; }
   mchance = getchance( rmaxlength, rjumpatmax/thesigma );
   if (mchance>chance) { chance = mchance; }
   mchance = getchance( rmaxjumplength, rmaxjump/thesigma );
   if (mchance>chance) { chance = mchance; }
   if (verbose>=1) {
     printf( "Probability this overscan contains a jump: %f\n", chance );
   }

#ifdef DOGRAPH
   /* plot statistics in graphical output */
   for(i=ntemplate;i<ns-ntemplate;i++) {
     cc[i] += mean;
     cc2[i] += mean;
   }
   cpgsci(6);
   cpgmove(-5,themean);
   cpgdraw(ns+5,themean);
   cpgmove(-5,themean-thesigma);
   cpgdraw(ns+5,themean-thesigma);
   cpgmove(-5,themean+thesigma);
   cpgdraw(ns+5,themean+thesigma);
   cpgsci(7);
   cpgmove(-5,themean-sigma);
   cpgdraw(ns+5,themean-sigma);
   cpgmove(-5,themean+sigma);
   cpgdraw(ns+5,themean+sigma);
#endif

   /* Write keywords to header */
   dummy = (float) maxlength;
   fits_update_key(INFITS, TFLOAT, "dqovmll", &dummy, "length of longest jump", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovmlj", &jumpatmax, "amplitude of longest jump", &status); 
   dummy = (float) maxjumplength;
   fits_update_key(INFITS, TFLOAT, "dqovmjl", &dummy, "length of highest jump", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovmjj", &maxjump, "amplitude of highest jump", &status); 
   dummy = (float) rmaxlength;
   fits_update_key(INFITS, TFLOAT, "dqovmllr", &dummy, "length of longest jump down", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovmljr", &rjumpatmax, "depth of longest jump down", &status); 
   dummy = (float) rmaxjumplength;
   fits_update_key(INFITS, TFLOAT, "dqovmjlr", &dummy, "length of deepest jump down", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovmjjr", &rmaxjump, "amplitude of deepest jump", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovjprb", &chance, "probability of a jump", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovmin", &min, "min in overscan region", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovmax", &max, "max in overscan region", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovmean", &glmean, "mean in overscan region", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovsig", &glsigma, "sigma in overscan region", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovsmea", &themean, "mean in collapsed overscan strip", &status); 
   fits_update_key(INFITS, TFLOAT, "dqovssig", &thesigma, "sigma in collapsed overscan strip", &status); 

   /* Close the fits file */
   fits_close_file( INFITS, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }

   /* free memory */
   free( (float *)data );
   free( (float *)strip );
#ifdef DOGRAPH
   free( (float *)xstrip );
#endif
   free( (float *)strap );

   exit(1);

} /* end main */

/* Start of function definitions */

void flag(int num) {
  printf ( "Flag %i\n", num );
}

void error(char *message, int giveusage) {
   printf("ERROR: %s\n", message);
   if (giveusage) usage();
   else exit(0);
}

void usage( void ) {
   printf("Usage: %s [options] <data file> <template file> <continuum file>\n", programname);
   printf("    [-h] (provide this help)\n");
   printf("    [-v (increase verbosity by 1)]\n");
   exit(0);
}

void pop(char **elements, int num, int *max) {
   int i;
   
   if (num<*max) {
      for(i=num+1;i<*max;i++) {
         elements[i-1] = elements[i];
      }
   }
   *max = *max-1;
}

float sqr(float x){
   return x*x;
} 

void getstats( float *array, long numitems, float *mean, float *sigma, float *min, float *max ) {
   int i;
   float temp;
   double _mean,_sigma;

   _mean = _sigma = 0;
   *min = *max = array[0];
   for(i=0;i<numitems;i++) {
     temp = array[i];
     _mean += temp;
     _sigma += temp*temp;
     if (temp<*min) {
       *min = temp;
     } else { 
       if (temp>*max) {
	 *max = temp;
       }
     }
   }
   _sigma = sqrt( (_sigma-_mean*_mean/numitems)/(numitems-1) );
   _mean /= numitems;
   *mean = (float)_mean;
   *sigma = (float)_sigma;
}

void getstatsfilter(float *array, long numitems, float low, float high, int niter, float *mean, float *sigma, float *min, float *max ) {
  int i,j,count,prevcount;
  double _mean,_sigma;
  float *data,_min,_max,temp;

  data = (float *)calloc((unsigned) numitems, sizeof(float));
  for(i=0;i<numitems;i++) data[i] = array[i];
  
  count = numitems;
  _mean = *mean;
  _sigma = *sigma;
  for(i=1;i<=niter;i++) {
    prevcount = count;
    count = 0;
    _min = *max;
    _max = *min;
    for(j=0;j<prevcount;j++) {
      if ((data[j]>=_mean-low*_sigma)&&(data[j]<=_mean+high*_sigma)) {
        temp = data[j];
	data[count] = data[j];
	if (temp<_min) {
	    _min = temp;
	} else {
	  if (temp>_max) {
	    _max = temp;
	  }
	}
	count++;
      }
    }
    _mean = _sigma = 0;
    for(j=0;j<count;j++) {
      _mean += data[j];
      _sigma += data[j]*data[j];
    }
    _sigma = sqrt( (_sigma-_mean*_mean/count)/(count-1) );
    _mean /= count;
  }
  *mean = (float)_mean;
  *sigma = (float)_sigma;
  *min = (float)_min;
  *max = (float)_max;
  free( (float *)data );
}

float getchance( int length, float ampl ) {
  /* One-sided confidence levels for 0, 1, 2, ... sigma */
  float conflevs[] = { 0.50000, 0.84135, 0.97725, 0.99865, 0.9999685, 0.99999971, 0.999999999 };
  int confi;
  
  /* Estimate the chance that this is a true jump.
     P(Jump) = 1-P(byChance)
     where P(byChance) indicates this particular arrangement happened by chance 
     P(byChance) = Pi_n (1-c)
     where Pi_n indicates a product over n points, and 1-c the probability that point n
     happened by chance, where c is the one-sided confidence level given above. Assuming
     that the jump is of constant level:
     P(byChance) = (1-c)^n
     and
     P(Jump) = 1-(1-c)^n
  */
  confi = (int)(ampl);
  if (confi<0) { confi=0; }
  if (confi>6) { confi=6; }
  return( 1-pow(1-conflevs[confi],(float)length) );
}
