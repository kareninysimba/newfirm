/*

  biasstruct.c

  How to compile with graphical output:
  g77 findnoisychannel.c -o findnoisychannel -Wall -lcfitsio -lm

  Description:

  HISTORY:
  12/18/08   Document created
  12/24/08   Version 1.0 ready
  01/08/09   Removed detection of "1024 pixels" and simplified
             statistics for noisy channel detection by focusing only
	     on the noise of the first NARROWWIDTH columns in each
	     channel. Modified code for input of 2046x2046 images.
  01/21/09   Statistics in narrow strip are determined through
             three iterations of +-3 sigma clipping.

*/

#include <fitsio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Definitions */
#define  CHANNELWIDTH 32
#define  DETWIDTH 2048
#define  NARROWWIDTH 4

/* declare global variables */
char programname[120];           /* name under which this program is run */
int verbose = 0;                 /* default level for verbose */

/* declare functions */
void flag(int num);
void error(char *message, int giveusage);
void usage( void );
void pop(char **elements, int num, int *max);
float sqr(float x);
void keyname( char *pre, char *name, char keyword[80] );
int savefits( float *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits );
int savefits_i( int *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits );
void getstats( float *array, int numitems, float *mean, float *sigma, int offset );
void getstatsfilter(float *array, int numitems, float low, float high, int niter, float *mean, float *sigma, int offset );

/* main body of the program */
int main(int argc, char **argv) {

   /* general variable */
   char message[256];            /* message string */
   char *stop;                   /* string needed for string-to-float conversion */
   int i,j,k;                    /* counters */
   float mean,sigma;
   
   /* variables related to command line options */
   int mdef = 0, sdef = 0;       /* variables related to global image statistics */
   float globalmean,globalsigma; /* mean and sigma over entire frame */

   /* variables related to reading of fits files */
   int status = 0;               /* status must always be initialized = 0  */
   int bitpix;                   /* bytes per pixel */
   int naxis;                    /* number of axes in FITS file */
   long naxes[2] = {1,1};        /* lower limit of pixels to be read */

   /* variables related to input fits file */
   fitsfile *INFITS;             /* fits file pointers defined in fitsio.h */
   fitsfile *BPMFITS;            /* fits file pointers defined in fitsio.h */
   long nx,ny;                   /* variables related to image size */
   char fitsfilename[120];       /* fits file name */
   char outfilename[120];        /* fits file name */
   char bpmfilename[120];        /* bpm file name */
   char bpmoutfilename[120];     /* bpm out file name */
   float *data;                  /* array for fits data */
   int *bpmdata;                 /* array for bpm data */
   long ndat;                    /* number of data elements in array */

   /* variables related to channels, collapsed channels, and channel statistics */
   int nchan;                    /* number of channels */
   int channel;                  /* channel number */
   long nxc,nxs;                 /* channel and narrow channel width */
   long ndatok,ndatsmok;         /* number of pixels not in bpm*/
   long nstrip;                  /* number of pixels in strip */
   double sum,sumsq;             /* sum of data and data squared */
   double sumsm,sumsmsq;         /* sum of data and data squared */
   float dat;                    /* variable to store array element in */
   float *strip;                 /* array for narrow start of channel */
   float chanmean,chansigma;     /* mean and sigma over one channel */
   float chansmmean,chansmsigma; /* mean and sigma over one channel */
   float *chsigma,*chsmsigma;    /* arrays to store results from channel diagnostics */
   float *chslope,*chsmslope;    /* arrays to store results from channel diagnostics */

   strcpy( programname, argv[0] ); /* store program name in global variable */

   /* Process command line options */
   i = argc-1;
   while (i>0) { /* Make a first loop to find -v arguments to determine verbose level */
      if ( strcmp(argv[i],"-v") == 0 ) {
	 verbose++; /* increase verbosity */
	 pop(argv,i,&argc); /* remove from argument list */
      }
      i--;
   }
   if (verbose>=1) printf("Verbose level: %i (out of 4)\n", verbose);
   if (verbose>=2) printf("Processing command line options:\n");
   if (verbose>=2) printf("   Number of command line arguments: %i\n", argc);
   i = argc-1;
   while (i>=1) { /* begin loop over command line arguments */
      if ( strcmp(argv[i], "-h")==0) /* help is asked, so give it and exit */
	 usage();
      if ( strcmp(argv[i], "-m")==0 ) {
         if (i+1<argc) { /* Need one more argument for global mean */
            globalmean = (double) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -m\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Value for global mean supplied by user: %f\n", globalmean);
            mdef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-m is missing its argument",0);
      }
      if ( strcmp(argv[i], "-s")==0 ) {
         if (i+1<argc) { /* Need one more argument for global sigma */
            globalsigma = (double) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -s\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Value for global sigma supplied by user: %f\n", globalsigma);
            sdef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-s is missing its argument",0);
      }
      i--;
   } /* end loop over command line arguments */

   /* After the known options have been processed, one argument, the  
      input fits file, should remain. */
   /* List the remaining arguments */
   if (verbose>=2) printf("   Remaining command line arguments: %i ", argc-1);
   if ((verbose>=2)&&(argc>1)) {
      for(i=1;i<argc;i++) { printf("%s ",argv[i]); }
      printf("\n");
   }
   if (argc>3) { /* Check whether two arguments remain */
      error("too many or unknown command line arguments given",1);
   } else if (argc<3) error("too few file names given",1);
   /* Two command line arguments left, copy to strings */
   strcpy(fitsfilename, argv[1]);
   strcpy(bpmfilename, argv[2]);
   if (verbose>=2) {
      printf("   Input fits file: %s\n", fitsfilename );
      printf("   Input bpm file: %s\n", bpmfilename );
   }

   /* Check whether all the necessary information is available.                                   
      Provide defaults where not, or give an error message. */
   if (mdef==0) error("no global mean value has been supplied",1);
   if (sdef==0) error("no global sigma value has been supplied",1);

   /* Open data fits file and obtain basic information */
   fits_open_file(&INFITS, fitsfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(INFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(INFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(INFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",fitsfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }

   /* set array dimensions and number of elements */
   nx = naxes[0];
   ny = naxes[1];
   ndat = nx*ny;
   if (verbose>=3) printf("nx: %li  ny: %li\n", nx, ny );
   /* Allocate memory for data array */
   data = (float *)calloc((unsigned) (ndat), sizeof(float));
   if (data == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",0);
   }
   /* Read the fits file into an array */
   naxes[0] = naxes[1] = 1;
   fits_read_pix(INFITS, TFLOAT, naxes, ndat, NULL, data, NULL, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=2)
     printf("Number of pixels read: %li\n", ndat );

   /* Open BPM fits file and obtain basic information */
   fits_open_file(&BPMFITS, bpmfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(BPMFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(BPMFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(BPMFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",bpmfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }

   /* set array dimensions and number of elements */
   if ((naxes[0]!=nx)||(naxes[1]!=ny)) {
     error("Input fits and bpm images do not have the same size\n",0);
   }
   /* Allocate memory for data array */
   bpmdata = (int *)calloc((unsigned) (ndat), sizeof(int));
   if (bpmdata == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",0);
   }
   /* Read the fits file into an array */
   naxes[0] = naxes[1] = 1;
   fits_read_pix(BPMFITS, TINT, naxes, ndat, NULL, bpmdata, NULL, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=2)
     printf("Number of pixels read: %li\n", ndat );

   nxc = CHANNELWIDTH;
   nxs = NARROWWIDTH;
   nstrip = NARROWWIDTH*DETWIDTH;
   nchan = DETWIDTH/CHANNELWIDTH;
   strip = (float *)calloc((unsigned) nstrip, sizeof(float));

   chsigma = (float *)calloc((unsigned) nchan, sizeof(float));
   chsmsigma = (float *)calloc((unsigned) nchan, sizeof(float));
   chslope = (float *)calloc((unsigned) nchan, sizeof(float));
   chsmslope = (float *)calloc((unsigned) nchan, sizeof(float));

   /* Loop over all channels */
   for(channel=0;channel<nchan;channel++) {

     if (verbose>=2) {
       printf( "Processing channel %i out of %li\n", channel+1, nxc );
       printf( "Range along x-axis: %li to %li\n", channel*nxc, (channel+1)*nxc-1 );
     }
     
     ndatok = 0;
     ndatsmok = 0;

     /* Collapse channel in both directions, calculate global statistics */
     sum = sumsq = 0;
     sumsm = sumsmsq = 0;
     k = 0;
     for(i=0;i<nxc;i++) {
       for(j=0;j<ny;j++) {
	 dat = data[j*nx+channel*nxc+i-1];
	 if (bpmdata[j*nx+channel*nxc+i-1]==0) {
	   sum += dat;
	   sumsq += dat*dat;
	   ndatok++;
	   if (i<3) {
	     strip[k] = dat;
	     k++;
	   }
	 }
       }
     }
     chanmean = (float)(sum/ndatok);
     chansigma = (float)sqrt( (sumsq-sum*sum/ndatok)/(ndatok-1) );
     getstats( strip, k, &chansmmean, &chansmsigma, 0 );
     getstatsfilter( strip, k, 3, 3, 3, &chansmmean, &chansmsigma, 0 );

     /* Write out data quality information */
     if (verbose>=1) {
       printf("Stats for ch. %3i: %10.3f %10.3f\n",
	      channel+1, chansigma, chansmsigma );
     }

     chsigma[channel] = chansigma;
     chsmsigma[channel] = chansmsigma;

   } /* End loop over all channels */

   getstats( chsmsigma, nchan, &mean, &sigma, 0 );
   if (verbose>=2) {
     printf( "Mean and sigma before clipping: %f %f\n", mean, sigma );
   }
   getstatsfilter( chsmsigma, nchan, 2, 2, 3, &mean, &sigma, 0 );
   if (verbose>=2) {
     printf( "Mean and sigma after clipping: %f %f\n", mean, sigma );
   }
   for(channel=0;channel<nchan;channel++) {
     if (chsmsigma[channel]>=mean+20*sigma) {
       i = channel*CHANNELWIDTH+1;
       j = i+31;
       if (verbose>=1) {
	 printf("Found noisy channel: %i (from pixel %i to %i)\n",
		channel, i, j );
       }
       for(i=0;i<nxc;i++) {
	 for(j=0;j<ny;j++) {
	   bpmdata[j*nx+channel*nxc+i-1] = 1;
	 }
       }
     }
   }

   i = strlen(fitsfilename)-5;
   strncpy( outfilename, fitsfilename, i );
   outfilename[i] = '\0';
   strcat( outfilename, "_out.fits" );
   if (verbose>=1) printf( "Creating outfile: %s\n", outfilename );
   savefits( data, nx, ny, INFITS, outfilename );

   i = strlen(fitsfilename)-5;
   strncpy( bpmoutfilename, fitsfilename, i );
   bpmoutfilename[i] = '\0';
   strcat( bpmoutfilename, "_bpm.fits" );
   if (verbose>=1) printf( "Creating outfile: %s\n", bpmoutfilename );
   savefits_i( bpmdata, nx, ny, BPMFITS, bpmoutfilename );

   /* Close the fits file */
   fits_close_file( INFITS, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }

   /* Close the fits file */
   fits_close_file( BPMFITS, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }

   free( (float *)strip);
   free( (float *)data );

   exit(1);

} /* end main */

/* Start of function definitions */

void flag(int num) {
  printf ( "Flag %i\n", num );
}

void error(char *message, int giveusage) {
   printf("ERROR: %s\n", message);
   if (giveusage) usage();
   else exit(0);
}

void usage( void ) {
   printf("Usage: %s [options] <data file> <template file> <continuum file>\n", programname);
   printf("    [-h] (provide this help)\n");
   printf("    [-v (increase verbosity by 1)]\n");
   exit(0);
}

void pop(char **elements, int num, int *max) {
   int i;
   
   if (num<*max) {
      for(i=num+1;i<*max;i++) {
         elements[i-1] = elements[i];
      }
   }
   *max = *max-1;
}

float sqr(float x){
   return x*x;
} 

void keyname( char *pre, char *name, char keyword[80] ) {
  strcpy(keyword,"dq");
  strcat(keyword,pre);
  strcat(keyword,name);
}

int savefits( float *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits ) {
   int status=0;
   fitsfile *OUTFITS;    /* FITS file pointers defined in fitsio.h */
   char overwriteout[125];

   /* Make output file overwritable by prefixing a ! */
   strcpy( overwriteout, "!" );
   strcat( overwriteout, outfits );

   fits_create_file(&OUTFITS, overwriteout, &status); /* Open the output file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_copy_hdu(ORIGFITS, OUTFITS, 0, &status); /* Copy the original header */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   /* Write the fits file to disk */
   status = fits_write_img(OUTFITS, TFLOAT, 1, nx*ny, data, &status);
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_close_file( OUTFITS, &status ); /* Close the fits file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   return(1);
}

int savefits_i( int *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits ) {
   int status=0;
   fitsfile *OUTFITS;    /* FITS file pointers defined in fitsio.h */
   char overwriteout[125];

   /* Make output file overwritable by prefixing a ! */
   strcpy( overwriteout, "!" );
   strcat( overwriteout, outfits );

   fits_create_file(&OUTFITS, overwriteout, &status); /* Open the output file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_copy_hdu(ORIGFITS, OUTFITS, 0, &status); /* Copy the original header */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   /* Write the fits file to disk */
   status = fits_write_img(OUTFITS, TINT, 1, nx*ny, data, &status);
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_close_file( OUTFITS, &status ); /* Close the fits file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   return(1);
}

void getstats( float *array, int numitems, float *mean, float *sigma, int offset ) {
  int i;
  double _mean=0,_sigma=0;

  for(i=offset;i<numitems+offset;i++) {
    _mean += array[i];
    _sigma += array[i]*array[i];
  }
  _sigma = sqrt( (_sigma-_mean*_mean/numitems)/(numitems-1) );
  _mean /= (float)numitems;
  
  *mean = (float)_mean;
  *sigma = (float)_sigma;
}

void getstatsfilter(float *array, int numitems, float low, float high, int niter, float *mean, float *sigma, int offset ) {
  int i,j,count,prevcount;
  double _mean,_sigma;
  float *data;

  data = (float *)calloc((unsigned) numitems, sizeof(float));
  for(i=0;i<numitems;i++) data[i] = array[i+offset];

  count = numitems;
  _mean = (double)(*mean);
  _sigma = (double)(*sigma);
  for(i=1;i<=niter;i++) {
    prevcount = count;
    count = 0;
    for(j=0;j<prevcount;j++) {
      if ((data[j]>=_mean-low*_sigma)&&(data[j]<=_mean+high*_sigma)) {
        data[count] = data[j];
        count++;
      }
    }
    _mean = _sigma = 0;
    for(j=0;j<count;j++) {
      _mean += (double)data[j];
      _sigma += (double)(data[j]*data[j]);
    }
    _sigma = sqrt( (_sigma-_mean*_mean/count)/(count-1) );
    _mean /= count;
  }
  *mean = (float)_mean;
  *sigma = (float)_sigma;
  free( (float *)data );
}
