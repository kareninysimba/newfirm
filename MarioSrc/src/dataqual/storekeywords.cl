procedure storekeywords( class, id, sid, dm )

string class {prompt="Give PMAS class"}
string id {prompt="Give image name"}
string sid {prompt="Give image id"}
string dm {prompt="Give database connection"}

begin

    string pclass,pid,psid,pdm,ps1
    struct pline
    int pi
    real px

    pclass = class
    pid = id
    psid = sid
    pdm = dm

    # Retrieve keywords from the image
    ps1 = ""; hselect( pid, "enid", yes ) | scan( ps1 )
    if (ps1 != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="enid", value=ps1 )
    }
    ;

    ps1 = ""; hselect( pid, "seqid", yes ) | scan( ps1 )
    if (ps1 != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="seqid", value=ps1 )
    }
    ;

    pline = ""; hselect( pid, "object", yes ) | scan( pline )
    if (pline != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="object", value=pline )
    }
    ;

    pline = ""; hselect( pid, "telescop", yes ) | scan( pline )
    if (pline != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="telescope", value=pline )
    }
    ;

    pline = ""; hselect( pid, "detector", yes ) | scan( pline )
    if (pline != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="detector", value=pline )
    }
    ;

    pline = ""; hselect( pid, "filter", yes ) | scan( pline )
    if (pline != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="filter", value=pline )
    }
    ;

    pline = ""; hselect( pid, "ampname", yes ) | scan( pline )
    if (pline != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="ampname", value=pline )
    }
    ;

    pline = ""; hselect( pid, "obstype", yes ) | scan( pline )
    if (pline != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="obstype", value=pline )
    }
    ;

    ps1 = ""; hselect( pid, "obsid", yes ) | scan( ps1 )
    if (ps1 != "") {
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="obsid", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "airmass", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="airmass", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "zd", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="zd", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "ra", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="ra", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "dec", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="dec", value=ps1 )
    }
    ;

    # TODO: does this keyword actually give the epoch of the WCS?
    px = INDEF; hselect( pid, "radeceq", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="epoch", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "exptime", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="exptime", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "darktime", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="darktime", value=ps1 )
    }
    ;

    pi = INDEF; hselect( pid, "imageid", yes ) | scan( pi )
    if (!isindef(pi)) {
        printf("%d\n", pi ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="imageid", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "saturate", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="saturate", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "ccdtem", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="ccdtem", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "ccdtem2", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="ccdtem2", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "dewtem", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="dewtem", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "dewtem2", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="dewtem2", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "dewtem3", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="dewtem3", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "envtem", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="envtem", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "telfocus", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="telfocus", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "gain", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="gain", value=ps1 )
    }
    ;

    px = INDEF; hselect( pid, "rdnoise", yes ) | scan( px )
    if (!isindef(px)) {
        printf("%12.5e\n", px ) | scan( ps1 )
        setkeyval( class=pclass, id=psid, dm=pdm, keyword="rdnoise", value=ps1 )
    }
    ;

end
