/*

  biasstruct.c

  How to compile with graphical output: (set DOGRAPH to true)
  g77 biasstruct.c -o biasstruct -Wall -lcfitsio -lcpgplot -lpgplot -L/usr/X11R6/lib -lX11 -lm

  Without graphical output:
  g77 biasstruct.c -o biasstruct -Wall -lcfitsio -lm


  Description:

  HISTORY:
  8/19/04    Document created
  8/20/04    Version 1.0 ready

*/

#ifdef DOGRAPH
#include <cpgplot.h>
#endif
#include <fitsio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* declare global variables */
char programname[120];           /* name under which this program is run */
int verbose = 0;                 /* default level for verbose */

/* declare functions */
void flag(int num);
void error(char *message, int giveusage);
void usage( void );
void pop(char **elements, int num, int *max);
float sqr(float x);
void keyname( char *pre, char *name, char keyword[80] );

/* main body of the program */
int main(int argc, char **argv) {

   /* general variable */
   char buffer[80];              /* character buffer */
   char message[256];            /* message string */
   char *stop;                   /* string needed for string-to-float conversion */
   int i,j;                      /* counters */
   
   /* variables related to command line options */
   int addef=0;                  /* variables related to average flat mode */
   int fdef=0;                   /* variables related to bias/flat mode */
   char prefix[3];               /* df=domeflat,ze=zero */
   char keyword[80];
   int gdef=0;                   /* variables related to gain header keyword */
   char keygain[120];
   char keyoverscan[120];
   int rdef=0;                   /* variables related to rdnoise header keyword */
   int refdef=0;                 /* variables related to reference flat mode */
   int sfdef=0;                  /* variables related to sky flat mode */
   char keyrdnoise[120];

   /* variables related to reading of fits files */
   int status = 0;               /* status must always be initialized = 0  */
   int bitpix;                   /* bytes per pixel */
   int naxis;                    /* number of axes in FITS file */
   long naxes[2] = {1,1};        /* lower limit of pixels to be read */

   /* variables related to input fits file */
   fitsfile *INFITS;             /* fits file pointers defined in fitsio.h */
   long nx,ny;                   /* variables related to image size */
   char fitsfilename[512];       /* fits file name */
   float *data;                  /* array for overscan data */
   long ndat;                    /* number of data elements in array */
   float gain;                   /* gain as read from fits header */
   float rdnoise;                /* rdnoise as read from fits header */

   /* variables related to collapsed bias and bias statistics */
   double sum,sumsq;             /* sum of data and data squared */
   float dat;                    /* variable to store array element in */
   float *xstrip,*ystrip;        /* arrays for collapsed bias */
   float globalmean,globalsigma; /* mean and sigma over entire frame */
   float xmean,xsigma,ymean,ysigma; /* mean and sigma in collapsed biases */
   double sumx, sumy, sumxx, sumxy, sumyy; /* least-squares fitting */
   float avx;
   float xoffset, xslope, xcoeff;/* offset, slope, correlation coeffecient */
   float yoffset, yslope, ycoeff;/* offset, slope, correlation coeffecient */

#ifdef DOGRAPH
   /* variables related to plotting */
   float *plotx;                 /* x-axis for plotting */
#endif

   strcpy( programname, argv[0] ); /* store program name in global variable */

   /* Process command line options */
   i = argc-1;
   while (i>0) { /* Make a first loop to find -v arguments to determine verbose level */
      if ( strcmp(argv[i],"-v") == 0 ) {
	 verbose++; /* increase verbosity */
	 pop(argv,i,&argc); /* remove from argument list */
      }
      i--;
   }
   if (verbose>=1) printf("Verbose level: %i (out of 4)\n", verbose);
   if (verbose>=2) printf("Processing command line options:\n");
   if (verbose>=2) printf("   Number of command line arguments: %i\n", argc);
   i = argc-1;
   while (i>=1) { /* begin loop over command line arguments */
      if ( strcmp(argv[i], "-ad")==0 ) {
         addef = 1;
         pop(argv,i,&argc);
      }
      if ( strcmp(argv[i], "-f")==0 ) {
         fdef = 1;
         pop(argv,i,&argc);
      }
      if ( strcmp(argv[i], "-g")==0 ) {
	 if (i+1<argc) { /* Need one more argument for gain header keyword */
	    if (strcspn(argv[i+1],"-")==0) error("-g is missing its argument",0);
	    else strcpy(keygain,argv[i+1]);
	    if (verbose>=2) printf("   Reading gain from keyword: %s\n", keygain);
	    gdef = 1;
	    pop(argv,i,&argc);
	    pop(argv,i,&argc);
	 } else error("-g is missing its argument",0);
      }
      if ( strcmp(argv[i], "-h")==0) /* help is asked, so give it and exit */
	 usage();
      if ( strcmp(argv[i], "-r")==0 ) {
	 if (i+1<argc) { /* Need one more argument for rdnoise header keyword */
	    if (strcspn(argv[i+1],"-")==0) error("-r is missing its argument",0);
	    else strcpy(keyoverscan,argv[i+1]);
	    if (verbose>=2) printf("   Reading read noise from keyword: %s\n", keyoverscan);
	    rdef = 1;
	    pop(argv,i,&argc);
	    pop(argv,i,&argc);
	 } else error("-r is missing its argument",0);
      }
      if ( strcmp(argv[i], "-ref")==0 ) {
         refdef = 1;
         pop(argv,i,&argc);
      }
      if ( strcmp(argv[i], "-sf")==0 ) {
         sfdef = 1;
         pop(argv,i,&argc);
      }
      i--;
   } /* end loop over command line arguments */

   /* After the known options have been processed, one argument, the  
      input fits file, should remain. */
   /* List the remaining arguments */
   if (verbose>=2) printf("   Remaining command line arguments: %i ", argc-1);
   if ((verbose>=2)&&(argc>1)) {
      for(i=1;i<argc;i++) { printf("%s ",argv[i]); }
      printf("\n");
   }
   if (argc>2) { /* Check whether one argument remains */
      error("too many or unknown command line arguments given",1);
   } else if (argc<2) error("no input file name given",1);
   /* One command line argument left, copy to string */
   strcpy(fitsfilename, argv[1]);
   if (verbose>=2) {
      printf("   Input fits file: %s\n", fitsfilename );
   }

   /* Check whether all the necessary information is available. 
      Provide defaults where not, or give an error message. */
   if (gdef==0) strcpy(keygain,"GAIN");
   if (rdef==0) strcpy(keyrdnoise,"RDNOISE");

   /* Open data fits file and obtain basic information */
   fits_open_file(&INFITS, fitsfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(INFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(INFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(INFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",fitsfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }
   /* Get the gain and readnoise from header */
   fits_read_keyword(INFITS, keyrdnoise, buffer, NULL, &status); 
   if (status!=0) {
     sprintf( message, "Could not read header keyword %s", keyrdnoise );
     error( message, 0 );
   }
   rdnoise = (float) strtod(buffer, &stop); /* convert string to float */
   /* check for conversion error */
   if ( strlen(stop)!=0 ) error("cannot convert keyword rdnoise to float", 0);
   fits_read_keyword(INFITS, keygain, buffer, NULL, &status); 
   if (status!=0) {
     sprintf( message, "Could not read header keyword %s", keygain );
     error( message, 0 );
   }
   gain = (float) strtod(buffer, &stop); /* convert string to float */
   /* check for conversion error */
   if ( strlen(stop)!=0 ) error("cannot convert keyword gain to float", 0);
   /* set array dimensions and number of elements */
   nx = naxes[0];
   ny = naxes[1];
   ndat = nx*ny;
   if (verbose>=3) printf("nx: %li  ny: %li\n", nx, ny );
   /* Allocate memory for data array */
   data = (float *)calloc((unsigned) (ndat), sizeof(float));
   if (data == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",0);
   }
   /* Read the fits file into an array */
   naxes[0] = naxes[1] = 1;
   fits_read_pix(INFITS, TFLOAT, naxes, ndat, NULL, data, NULL, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=2)
     printf("Number of pixels read: %li\n", ndat );

   /* Collapse bias frame in both directions, calculate global statistics */
   xstrip = (float *)calloc((unsigned) nx, sizeof(float));
   ystrip = (float *)calloc((unsigned) ny, sizeof(float));
   sum = sumsq = 0;
   for(i=0;i<nx;i++) {
     for(j=0;j<ny;j++) {
       dat = data[j*nx+i];
       xstrip[i] += dat;
       ystrip[j] += dat;
       sum += dat;
       sumsq += dat*dat;
     }
     xstrip[i] /= ny;
   }
   globalmean = (float)(sum/ndat);
   globalsigma = (float)sqrt( (sumsq-sum*sum/ndat)/(ndat-1) );

   /* Calculate statistics along y strip and fit straight line */
   sumx = sumy = sumxy = sumxx = sumyy = sumsq = 0;
   for(i=0;i<ny;i++) {
     ystrip[i] /= nx; /* convert to average */
     sumx += i;
     sumy += ystrip[i];
     sumsq += ystrip[i]*ystrip[i];
   }
   avx = (float)(sumx/ny);
   ymean = (float)(sumy/ny);
   ysigma = (float)sqrt( (sumsq-sumy*sumy/ny)/(ny-1) );
   for(i=0;i<ny;i++) {
     sumxy += (i-avx)*(ystrip[i]-ymean);
     sumxx += (i-avx)*(i-avx);
     sumyy += (ystrip[i]-ymean)*(ystrip[i]-ymean);
   }
   yslope = (float)(sumxy/sumxx);
   yoffset = (float)(ymean-yslope*avx);
   if ((sumxy==0.0)&&((sumxx==0)||(sumyy==0))) {
      ycoeff = 1.0;
   } else {
     ycoeff = fabs((float)(sumxy/sqrt(sumxx*sumyy)));
   }
   
   /* Calculate statistics along x strip and fit straight line */
   sumx = sumy = sumxy = sumxx = sumyy = sumsq = 0;
   for(i=0;i<nx;i++) {
     sumx += i;
     sumy += xstrip[i];
     sumsq += xstrip[i]*xstrip[i];
   }
   avx = (float)(sumx/nx);
   xmean = (float)(sumy/nx);
   xsigma = (float)sqrt( (sumsq-sumy*sumy/nx)/(nx-1) );
   for(i=0;i<nx;i++) {
     sumxy += (i-avx)*(xstrip[i]-xmean);
     sumxx += (i-avx)*(i-avx);
     sumyy += (xstrip[i]-xmean)*(xstrip[i]-xmean);
   }
   xslope = (float)(sumxy/sumxx);
   xoffset = (float)(xmean-xslope*avx);
   if ((sumxy==0.0)&&((sumxx==0)||(sumyy==0))) {
     xcoeff = 1.0;
   } else {
     xcoeff = fabs((float)(sumxy/sqrt(sumxx*sumyy)));
   }

   /* Write out data quality information */
   printf("DQ | global mean and sigma [ADU]: %f %f\n", globalmean, globalsigma );
   printf("DQ | mean and sigma in x collapsed bias [ADU]: %f %f\n", xmean, xsigma );
   printf("DQ | mean and sigma in y collapsed bias [ADU]: %f %f\n", ymean, ysigma );
   printf("DQ | fit to x strip (a, b, r): %f %f %f\n", xslope, xoffset, xcoeff );
   printf("DQ | fit to y strip (a, b, r): %f %f %f\n", yslope, yoffset, ycoeff );

   strcpy(prefix,"ze");
   if (addef) { strcpy(prefix,"ad"); printf("Mode: average domeflat\n"); }
   if (fdef) { strcpy(prefix,"df"); printf("Mode: domeflat\n"); }
   if (refdef) { strcpy(prefix,"rf"); printf("Mode: reference domeflat\n"); }
   if (sfdef) { strcpy(prefix,"sf"); printf("Mode: skyflat\n"); }

   /* Write data quality information to header */
   keyname( prefix, "glme", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &globalmean, "DQ global mean of zero frame", &status); 
   keyname( prefix, "glsi", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &globalsigma, "DQ global sigma of zero frame", &status); 
   keyname( prefix, "xmea", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &xmean, "DQ mean of collapsed x bias", &status); 
   keyname( prefix, "xsig", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &xsigma, "DQ sigma of collapsed x bias", &status); 
   keyname( prefix, "xslp", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &xslope, "DQ slope of collapsed x bias", &status); 
   keyname( prefix, "xcef", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &xcoeff, "DQ correlation coeffecient of collapsed x bias", &status); 
   keyname( prefix, "ymea", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &ymean, "DQ mean of collapsed y bias", &status); 
   keyname( prefix, "ysig", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &ysigma, "DQ sigma of collapsed y bias", &status);
   keyname( prefix, "yslp", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &yslope, "DQ slope of collapsed y bias", &status); 
   keyname( prefix, "ycef", keyword );
   fits_update_key(INFITS, TFLOAT, keyword, &ycoeff, "DQ correlation coeffecient of collapsed y bias", &status); 
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }

   /* Close the fits file */
   fits_close_file( INFITS, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }

#ifdef DOGRAPH
   plotx = (float *)calloc((unsigned) nx, sizeof(float));
   for(i=0;i<nx;i++) plotx[i] = (float)i;
   cpgopen("/xserve");
   cpgenv( 0, nx, 1550, 1650, 0, 0);
   cpglab("Spectral axis (pixels)", "Counts", "");
   cpgline( nx, plotx, xstrip );
   cpgsci(5);
   cpgmove( 0, xoffset );
   cpgdraw( nx, xoffset+xslope*nx );
   cpgclos();

   free( (float *)plotx);
#endif

   free( (float *)xstrip);
   free( (float *)ystrip);
   free( (float *)data );

   exit(1);

} /* end main */

/* Start of function definitions */

void flag(int num) {
  printf ( "Flag %i\n", num );
}

void error(char *message, int giveusage) {
   printf("ERROR: %s\n", message);
   if (giveusage) usage();
   else exit(0);
}

void usage( void ) {
   printf("Usage: %s [options] <input file>\n", programname);
   printf("    [-ad] (average domeflat mode)\n");
   printf("    [-f] (flat field mode)\n");
   printf("    [-h] (provide this help)\n");
   printf("    [-ref] (reference flat field mode)\n");
   printf("    [-v (increase verbosity by 1)]\n");
   exit(0);
}

void pop(char **elements, int num, int *max) {
   int i;
   
   if (num<*max) {
      for(i=num+1;i<*max;i++) {
         elements[i-1] = elements[i];
      }
   }
   *max = *max-1;
}

float sqr(float x){
   return x*x;
} 

void keyname( char *pre, char *name, char keyword[80] ) {
  strcpy(keyword,"dq");
  strcat(keyword,pre);
  strcat(keyword,name);
}
