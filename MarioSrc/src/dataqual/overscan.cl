procedure overscan(infile)

file infile		 {prompt="file to determine overscan properties for"}
real threshold = 5.	 {prompt="sigma threshold in jump detection"}
real width = 10.	 {prompt="half the width of detection filter"}
int exclude = 0          {prompt="Number of pixels to exclude at edge"}

begin

    int e
    real t,w
    string ifile

    task $_overscan = "$overscan $(1).fits -w $2 -t $3 -v -e $4"

    #Get parameters
    ifile = infile
    t = threshold
    w = width
    e = exclude

    # Check whether the ifile ends in .fits already, and if so, drop
    # the extension
    if (strlstr(".fits",ifile)>0) {
        s1 = substr (ifile, 1, strlstr(".fits",ifile)-1)
    } else {
        s1 = ifile
    }

    _overscan( s1, t, w, e )

end
