# DQGSKY -- Make global sky map for an MEF from a file of extension sky maps.
# The extension sky maps must have a pattern described in the description file.
# This program allows for missing extensions an makes outputs that are
# tiled in the same way.

procedure dqgsky (skys, output, hdr, desc)

file	skys			{prompt="File of sky maps"}
file	output			{prompt="Output sky map"}
file	hdr			{prompt="Header for sky"}
file	desc			{prompt="Description file"}
real	blank = 0.		{prompt="Blank value"}
bool	verbose = no		{prompt="Verbose?"}

struct	*fd

begin
	file	in, out, sky
	int	i, j, i1, i2, j1, j2, m, n, nc, nl
	string	pat, sec

	in = skys
	out = output
	fd = desc

	# Get size.
	if (fscan (fd, pat, m, n) == EOF)
	    return

	# Make output.
	if (imaccess(out))
	    imdelete (out)
	while (fscan (fd, pat, i, j) != EOF) {
	    sky = ""; match (pat, in) | scan (sky)
	    if (sky == "")
	        next
	    if (!imaccess(out)) {
	        hselect (sky, "naxis1,naxis2", yes) | scan (nc, nl)
		i2 = nc * m; j2 = nl * n
		mkpattern (out, pattern="constant", option="replace",
		    v1=blank, size=1, pixtype="real", ndim=2,
		    ncols=i2, nlines=j2, header=hdr)
	    }
	    i1 = (i - 1) * nc + 1; i2 = i * nc
	    j1 = (j - 1) * nl + 1; j2 = j * nl
	    printf ("%s[%d:%d,%d:%d]\n", out, i1, i2, j1, j2) | scan (sec)
	    imcopy (sky, sec, verbose=verbose) 
	}
	fd = ""
end
