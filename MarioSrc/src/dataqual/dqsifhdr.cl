# DQSIFHDR -- Set SIF header with information from the MEF WCS/ACE analysis.

procedure dqsifhdr (image, image1, bpm, obm, cat, keywords, wcsdb, lfile)

file	image			{prompt="SIF image to update"}
file	image1			{prompt="SIF image to update"}
file	bpm			{prompt="SIF BPM image to update"}
file	obm			{prompt="SIF OBM image to update"}
file	cat			{prompt="SIF catalot to update"}
file	keywords		{prompt="Keyword file"}
file	wcsdb			{prompt="WCS solution database"}
file	lfile			{prompt="Logfile"}

struct	*list

begin
	string	images, wcs, cast
	real	mz1, mz, mze, pixscale, seeing
	real	skybg, skymag, skynoise, dpthadu, photdpth, photapd

	# Make a list to update all the images in one call.
	# The wcs list is a workaround for a bug in ccsetwcs (fixed in
	# the latest version).
	images = image
	wcs = "wcs"
	if (image1 != "") {
	    images += "," // image1
	    wcs += "," // "wcs"
	}
	if (bpm != "") {
	    images += "," // bpm
	    wcs += "," // "wcs"
	}
	if (obm != "") {
	    images += "," // obm
	    wcs += "," // "wcs"
	}

	# Set WCS.
	if (access (wcsdb)) {
	    hedit (images, "WCSCAL", "(1=1)", add+)
	    ccsetwcs (images, wcsdb, wcs, >> lfile)
	    if (cat != "")
		acesetwcs (cat, wcsdb, wcs, >> lfile) 
	} else
	    hedit (images, "WCSCAL", "(1=2)", add+)

	# Set keywords.
	if (access (keywords)) {
	    list = keywords
	    while (fscan (list, s1, line) != EOF) {
		if (line == "")
		    next
		hedit (images, s1, line, add+, show+, >> lfile)
		if (cat != "")
		    thedit (cat, s1, line, >> lfile)
	    }
	    list = ""

	    # Update catalog magnitudes.
	    if (cat != "") {
		thselect (cat, "MAGZERO,MAGZREF", yes) | scan (mz, mz1)
		mz1 = mz - mz1
		dmag (cat, mz1, fields="MAG", >> lfile)
	    }
	}

	# Get data for conversions to magnitude and arcseconds.
	hselect (image, "$MAGZERO,$PIXSCALE", yes ) | scan(mz, pixscale)
	
	# Set seeing, skymag, skynois1, and photometric depth.
	hselect (image, "$SEEINGP1", yes) | scan (seeing)
	if (isindef(pixscale)==NO && isindef(seeing)==NO) {
	    seeing = pixscale * seeing
	    printf ("%.2f\n", seeing) | scan (seeing)
	    nhedit (images, "SEEING1", seeing, "[arcsec] seeing", add+)
	}
	;
	hselect (image, "$SKYBG1", yes) | scan (skybg)
	if ( isindef(skybg) )
	    hselect (image, "$SKYADU1", yes) | scan (skybg)
	;
	if (isindef(mz)==NO && isindef(pixscale)==NO && isindef(skybg)==NO) {
	    if (skybg > 0) {
		skymag =  mz - 2.5 * log10 (skybg/pixscale**2)
		printf ("%.2f\n", skymag) | scan (skymag)
		nhedit (images, "SKYMAG1", skymag,
		    "[mag/arcsec^2] sky brightness", add+)
	    }
	    ;
	}
	;
	hselect (image, "$SKYNOIS1", yes) | scan (skynoise)
	if (isindef(skynoise)==NO)
	    nhedit (images, "SKYNOIS1", skynoise,
		"Mean sky noise (BUNIT)", add+)
	;
	hselect (image, "$DPTHADU1", yes) | scan (dpthadu)
	if (isindef(mz)==NO && isindef(dpthadu)==NO) {
	    if (dpthadu > 0) {
		photdpth = mz - 2.5 * log10 (dpthadu)
		printf ("%.2f\n", photdpth) | scan (photdpth)
		nhedit (images, "PHTDPTH1", photdpth,
		    "[mag] photometric depth", add+)
	    }
	    ;
	}
	;
	hselect (image, "$SEEINGP", yes) | scan (seeing)
	if (isindef(pixscale)==NO && isindef(seeing)==NO) {
	    seeing = pixscale * seeing
	    printf ("%.2f\n", seeing) | scan (seeing)
	    nhedit (images, "SEEING", seeing,
	        "[arcsec] seeing", add+)
	}
	;
	hselect (image, "$SKYBG", yes) | scan (skybg)
	if ( isindef(skybg) )
	    hselect (image, "$SKYADU", yes) | scan (skybg)
	;
	if (isindef(mz)==NO && isindef(pixscale)==NO && isindef(skybg)==NO) {
	    if (skybg > 0) {
		skymag =  mz - 2.5 * log10 (skybg/pixscale**2)
		printf ("%.2f\n", skymag) | scan (skymag)
		nhedit (images, "SKYMAG", skymag,
		    "[mag/arcsec^2] sky brightness", add+)
	    }
	    ;
	}
	;
	hselect (image, "$DPTHADU", yes) | scan (dpthadu)
	if (isindef(mz)==NO && isindef(dpthadu)==NO) {
	    if (dpthadu > 0) {
		photdpth = mz - 2.5 * log10 (dpthadu)
		printf ("%.2f\n", photdpth) | scan (photdpth)
		nhedit (images, "PHOTDPTH", photdpth,
		    "[mag] photometric depth", add+)
	    }
	    ;
	}
	;
	hselect (image, "$PHOTAPD", yes) | scan (photapd)
	if (isindef(photapd)==NO) {
	    nhedit( images, "PHOTAPD", photapd,
		"[pix] Aperture diameter for MAGZERO", add+)
	}
	;

	# Set PHOTAPD
	hselect (image, "$PHOTAPD", yes) | scan(photapd)
	if (isindef(photapd)==NO) {
		nhedit(images,"PHOTAPD",photapd,
			"[pix] Aperture diameter for MAGZERO",add+,update+)
	}
	;

	# Compute additional depth quantities for the SIF.
	hselect (image, "$SKYNOISE1,$MAGZERO1,$MAGZERR1", yes) |
	    scan (skynoise, mz1, mze)
	if (isindef(pixscale)==NO && isindef(mz)==NO &&
	    isindef(seeing)==NO && isindef(skynoise)==NO) {

	    photdpth (skynoise, seeing, mz, pixscale, verbose=verbose)

	    # Send to PMASS
	    printf("%12.5e\n", mz1 ) | scan( cast )
	    setkeyval( dm, "objectimage", image, "dqphzp", cast )
	    printf("%12.5e\n", mze ) | scan( cast )
	    setkeyval( dm, "objectimage", image, "dqphezp", cast )
	    printf("%12.5e\n", photdpth.dqphdpps ) | scan( cast )
	    setkeyval( dm, "objectimage", image, "dqphdpps", cast )
	    printf("%12.5e\n", photdpth.dqphdpap ) | scan( cast )
	    setkeyval( dm, "objectimage", image, "dqphdpap", cast )
	    printf("%12.5e\n", photdpth.dqphapsz ) | scan( cast )
	    setkeyval( dm, "objectimage", image, "dqphapsz", cast )
	    printf("%12.5e\n", photdpth.dqphdppx ) | scan( cast )
	    setkeyval( dm, "objectimage", image, "dqphdppx", cast )
	}
	;
end
