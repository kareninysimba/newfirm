include	<error.h>
include	<acecat.h>
include	<acecat1.h>


# T_DMAG -- Apply magnitude offset.

procedure t_dmag ()

int	inlist			# List of input catalogs
pointer	fields			# Catalog definitions
real	dmag			# Magnitude delta
bool	verbose			# Verbose?

int	i, id, irows
pointer	cat, rec, ptr, stp, sym
pointer	sp, input

bool	clgetb()
real	clgetr()
int	imtopenp(), imtgetim()
pointer	sthead(), stnext(), stname()
errchk	catopen()

begin
	call smark (sp)
	call salloc (input, SZ_FNAME, TY_CHAR)
	call salloc (fields, SZ_FNAME, TY_CHAR)

	# Get task parameters.
	inlist = imtopenp ("input")
	call clgstr ("fields", Memc[fields], SZ_LINE)
	dmag = clgetr ("dmag")
	verbose = clgetb ("verbose")

	# Modify catalogs.
	while (imtgetim (inlist, Memc[input], SZ_FNAME) != EOF) {
	    iferr {
		cat = NULL

		# Open the catalog.
		ptr = NULL
		call catopen (ptr, Memc[input], Memc[input], Memc[fields],
		    "", NULL)
		cat = ptr
		stp = TBL_STP(CAT_OUTTBL(cat))

		# Print actions.
		if (verbose) {
		    call printf ("%s:\n")
		        call pargstr (Memc[input])
		    for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
			i = ENTRY_TYPE(sym)
			if (i==TY_INT||i==TY_REAL||i==TY_DOUBLE) {
			    call printf ("  %s -> %s + (%g)\n")
				call pargstr (Memc[stname(stp,sym)])
				call pargstr (Memc[stname(stp,sym)])
				call pargr (dmag)
			}
		    }
		    call flush (STDOUT)
		}

		# Copy the entries.
		call catgeti (cat, "irows", irows)
		do i = 1, irows {
		    call catrrec (cat, rec, i)
		    for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
		        id = ENTRY_ID(sym)
			switch (ENTRY_TYPE(sym)) {
			case TY_INT:
			    RECI(rec,id) = RECI(rec,id) + dmag
			case TY_REAL:
			    RECR(rec,id) = RECR(rec,id) + dmag
			case TY_DOUBLE:
			    RECD(rec,id) = RECD(rec,id) + dmag
			}
		    }
		    call catwrec (cat, rec, i)
		}

	    } then
	        call erract (EA_WARN)

	    if (cat != NULL)
		call catclose (cat)
	}

	call imtclose (inlist)
	call sfree (sp)
end
