# DPHDR -- Set header for data product.
#
# This is called multiple times so the arguments are used to determine which
# things need to be done when.

procedure dphdr (image, proctype, obstype, extname)

file	image			{prompt="Image header to edit"}
string	proctype		{prompt="Processing type"}
string	obstype			{prompt="Observation type"}
string	extname			{prompt="Extension name"}

struct	*fd

begin
	int	i, j, k
	real	x1, x2
	string  s1, s2, s3
	struct	line
	file	caldir = "MC$"
	file	tmp

	tmp = mktemp ("dphdr") // ".tmp"
		
	if (obstype != "zero" && obstype != "dark") {

	    # Short image name for messages.
	    s1 = substr( image, strlstr("/",image)+1, 1000 )

	    # Set filter ID.
	    getcal ( image, "FILTID", cm, caldir, obstype="", detector="",
		imageid="", filter="!FILTER", exptime="", mjd="!MJD-OBS",
		>& "dev$null")
	    if (getcal.statcode == 0)
		nhedit (image, "FILTID", getcal.value,
		    "Unique filter identification", after="FILTER", add+)
	    else
		sendmsg ("WARNING", "Failed to set filter ID", s1, "VRFY")

	    # Set filter properities.  See dmMaster TOC for comments.
	    getcal ( image, "LAMRANGE", cm, caldir, obstype="", detector="",
		imageid="", filter="!FILTER", exptime="", mjd="!MJD-OBS",
		>& "dev$null")
	    if (getcal.statcode == 0 && fscan (getcal.value, x1, x2, s2) == 3) {
	        if (s2 == "A") {
		    x1 = x1 / 10.; x2 = x2 / 10.
		}
		;
		nhedit (image, "PHOTCLAM", x1, "[nm] Filter wavelenght width",
		    after="FILTID", add+)
		nhedit (image, "PHOTFWHM", x2, "[nm] Filter wavelength FWHM",
		    after="FILTID", add+)
		nhedit (image, "PHOTBW", x2, "[nm] Filter wavelength FWHM",
		    after="FILTID", add+)
		nhedit (image, "LAMRANGE", del+)
	    } else
		sendmsg ("WARNING", "Failed to set filter parameters",
		    s1, "VRFY")
	}
	
	# Set associated calibration files
	#
	# This is not as general as it should be since it doesn't make
	# use of the names routines.  The assumption is that after
	# the last hyphen their will be an underscore component.
	# It does work for both InstCal and Resampled.  In the latter
	# case the parent MEF name is set for information purposes.
	#
	# Note we retain BPM as well as creating DQMASK.  This is
	# because stacks are used for further processing.
		
	imhead (image, l+) | match (caldir, meta-) |
	    translit ("STDIN", "='", " ", > tmp)
	fd = tmp
	while (fscan (fd, s1) != EOF) {
	    hselect (image, s1, yes) | scan (line)
	    i = strstr ("MC$", line)
	    if (i > 1)
		line = substr (line, 1, i-1) // substr (line, i+3, 999)
	    else
		line = substr (line, i+3, 999)
	    nhedit (image, s1, line, ".")
	}
	fd = ""; delete (tmp)

	s3 = extname
	if (s3 != "")
	    s3 = "[" // s3 // "]"
        hselect( image, "BPM", yes ) | scan( s1 )
	if (nscan()==1) {
	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
	    if (k == 0)
	        k =  1000
	    if (s3 == "")
		s2 = substr( s1, 1, k-1 )
	    else
		s2 = substr( s1, 1, i-1 ) // s3
	    nhedit (image, "DQMASK", s2, "Data quality file", add+)
	}
        hselect( image, "ZERO", yes ) | scan( s1 )
	if (nscan()==1) {
	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
	    if (k == 0)
	        k = 1000
	    if (s3 == "")
		s2 = substr( s1, 1, k-1 )
	    else
		s2 = substr( s1, 1, i-1 ) // s3
	    s2 = substr( s1, 1, i-1 ) // s3
	    nhedit (image, "ZERO", "BIASFIL", rename+)
	    nhedit (image, "BIASFIL", s2, "Bias reference file")
	}
        hselect( image, "DARK", yes ) | scan( s1 )
	if (nscan()==1) {
	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
	    if (k == 0)
	        k = 1000
	    if (s3 == "")
		s2 = substr( s1, 1, k-1 )
	    else
		s2 = substr( s1, 1, i-1 ) // s3
	    s2 = substr( s1, 1, i-1 ) // s3
	    nhedit (image, "DARK", "DARKFIL", rename+)
	    nhedit (image, "DARKFIL", s2, "Dark reference file")
	}
        hselect( image, "FLAT", yes ) | scan( s1 )
	if (nscan()==1) {
	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
	    if (k == 0)
	        k = 1000
	    if (s3 == "")
		s2 = substr( s1, 1, k-1 )
	    else
		s2 = substr( s1, 1, i-1 ) // s3
	    s2 = substr( s1, 1, i-1 ) // s3
	    nhedit (image, "FLAT", "FLATFIL", rename+)
	    nhedit (image, "FLATFIL", s2, "Dome flat reference file")
	}
        hselect( image, "SFLAT", yes ) | scan( s1 )
	if (nscan()==1) {
	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
	    if (k == 0)
	        k = 1000
	    if (s3 == "")
		s2 = substr( s1, 1, k-1 )
	    else {
		s2 = substr( s1, 1, i-1 )
		if (j > i) {
		    if (k > j)
			s2 = s2 // "-" // substr (s1, j+1, k-1)
		    else
			s2 = s2 // "-" // substr (s1, j+1, 1000)
		}
		s2 = s2 // s3
	    }
	    nhedit (image, "SFLAT", "SFLATFIL", rename+)
	    nhedit (image, "SFLATFIL", s2, "Sky flat reference file")
	}
        hselect( image, "FRINGE", yes ) | scan( s1 )
	if (nscan()==1) {
	    i = strldx ("-", s1); j = strldx ("_", s1); k = strldx (".", s1)
	    if (k == 0)
	        k = 1000
	    if (s3 == "")
		s2 = substr( s1, 1, k-1 )
	    else {
		s2 = substr( s1, 1, i-1 )
		if (j > i) {
		    if (k > j)
			s2 = s2 // "-" // substr (s1, j+1, k-1)
		    else
			s2 = s2 // "-" // substr (s1, j+1, 1000)
		}
		s2 = s2 // s3
	    }
	    nhedit (image, "FRINGE", "FRNGFIL", rename+)
	    nhedit (image, "FRNGFIL", s2, "Fringe template reference file")
	}
        hselect( image, "PUPGHOST", yes ) | scan( s1 )
	if (nscan()==1) {
	    i = strldx ("-", s1)
	    j = strldx ("_", s1)
	    k = strldx (".", s1)
	    if (k == 0)
	        k = 1000
	    if (s3 == "")
		s2 = substr( s1, 1, k-1 )
	    else {
		s2 = substr( s1, 1, i-1 )
		if (j > i) {
		    if (k > j)
			s2 = s2 // "-" // substr (s1, j+1, k-1)
		    else
			s2 = s2 // "-" // substr (s1, j+1, 1000)
		}
		s2 = s2 // s3
	    }
	    nhedit (image, "PUPGHOST", "PUPLFIL", rename+)
	    nhedit (image, "PUPLFIL", s2, "Pupil template reference file")
	}

	# Add pipeline version.
	plver | scan (line)
	nhedit (image, "PLVER", line, "Pipeline version", add+)
end
