# DPEXPMAP -- Make exposure map data product.
#
# This routine makes a pipeline PLIO FITS exposure map data product.
# The headers are copied from the image headers except for a few
# EXPMAP specific keywords.
#
# The list of PL files must match the images in the input image file in
# extension order.  Any keyword in the input image(s) pointing to the
# EXPMAP must be set externally.

procedure dpexpmap (imfile, plfiles, expmap, tmp)

file	imfile			{prompt="Image file (simple or MEF)"}
string	plfiles			{prompt="List of exposure map plfiles"}
file	expmap			{prompt="Output exposure map"}
string	tmp			{prompt="Root name for temporary files"}
bool	mosaic			{prompt="Mosaic MEF?"}

begin
	int	i
	string	imfile1, expmap1, extn, imextn, emextn
	real	maskzero, maskscal

	# Remove any extension in image file. 
	imfile1 = imfile
	i = strstr (".fits", imfile1)
	if (i != 0)
	    imfile1 = substr (imfile1, 1, i-1)

	# Use explicit extension to allow common image rootnames.
	expmap1 = expmap
	if (strstr(".fits",expmap1) == 0)
	    expmap1 += ".fits"

	# Append exposure maps.
	files (plfiles, > tmp//"1.tmp")
	list = tmp//"1.tmp"
	for (i=1; fscan(list,plfile)!=EOF; i+=1) {
	    # Use explicit extension to allow common image rootnames.
	    if (strstr(".pl",plfile) == 0)
	        plfile += ".pl"

	    # Setup global header.
	    if (i == 1) {
		mkglbhdr (imfile1//"[0]", expmap1)
		nhedit (expmap1, "OBJECT", "Exposure map for "//imfile1,
		    ".", add+)
		nhedit (expmap1, "PRODTYPE", "expmap", "", add+)
		nhedit (expmap1, "EMFOR", imfile1,
		    "Data for which this mask applies", add+)
	    }
	    ;

	    # Set extension name.
	    if (mosaic) {
		printf ("%s[%d]\n", imfile1, i) | scan (imextn)
		hselect (imextn, "EXTNAME", yes) | scan (extn)
	        printf ("%s[%s]\n", imfile1, extn) | scan (imextn)
		printf ("%s[%s,append,type=mask,inherit]\n", expmap1, extn) |
		    scan (emextn)

		# Copy EXPMAP data.
		imcopy (plfile, emextn, verbose-)

		# Add extension header.  This funny construct is to workaround
		# a non-intuitive behavior of mkheader.
		print (imextn, > tmp//"2.tmp")
		mkheader (emextn, "@"//tmp//"2.tmp", append-, verbose-)
		delete (tmp//"2.tmp")

		# Add EXPMAP specific keywords.
		hselect (plfile, "$MASKZERO,$MASKSCAL", yes) |
		    scan (maskzero, maskscal)
		nhedit (plfile, "*", del+, >& "dev$null")
		if (isindef(maskzero)==NO)
		    nhedit (emextn, "MASKZERO", maskzero,
			"Conversion to exposure time")
		if (isindef(maskscal)==NO)
		    nhedit (emextn, "MASKSCAL", maskscal,
			"Conversion to exposure time")
		nhedit (emextn, "OBJECT", "Exposure map for "//imextn, ".", add+)
		nhedit (emextn, "PRODTYPE", "expmap", "Data product type", add+)
		nhedit (emextn, "EMFOR", imextn,
		    "Data for which this mask applies", add+)
	    } else {
		printf ("%s[pl,append,type=mask,inherit]\n", expmap1) |
		    scan (emextn)

		# Copy EXPMAP data.
		imcopy (plfile, emextn, verbose-)

		# Remove all extension keywords since they are in the global
		# header.
		nhedit (emextn, "*", delete+, >& "dev$null")
	    }

	    # Add EXPMAP specific keywords.
	    hselect (plfile, "$MASKZERO,$MASKSCAL", yes) |
	        scan (maskzero, maskscal)
	    if (isindef(maskzero)==NO)
	        nhedit (emextn, "MASKZERO", maskzero,
		    "Conversion to exposure time")
	    if (isindef(maskscal)==NO)
	        nhedit (emextn, "MASKSCAL", maskscal,
		    "Conversion to exposure time")
	}
	list = ""; delete (tmp//"1.tmp")
end
