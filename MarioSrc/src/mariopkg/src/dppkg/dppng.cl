# DPPNG -- Make PNG data product.
#
# This routine makes a pipeline encapsulated FITS PNG data product.
# This has global header copied from a specified image and two extensions
# of encapsulated PNGs.  The first extension is a block average of the
# input image(s) (made with mkgraphic) and the second is a small PNG of
# the specified size (made with convert).
#
# This has a built-in naming scheme.

procedure dppng (images, hdr, pngroot, png_blk, png_pix, png_pix2, tmp)

string	images			{prompt="List of input images for data"}
string	hdr			{prompt="Image defining header"}
file	pngroot			{prompt="Output FITS rootname"}
int	png_blk			{prompt="Blocking factor for large PNG"}
int	png_pix			{prompt="Display pixels for small PNG"}
int	png_pix2		{prompt="Display pixels for medium PNG"}
string	offsets = "none"	{prompt="Offsets"}
string	tmp			{prompt="Root name for temporary files"}
string	zscale = "none"		{prompt="Scaling expression"}
bool	delpng = no		{prompt="Delete PNG files?"}

begin
	file	png, png1, png2, png3

	# Set filenames.
	# png = pngroot // "_png"
	png = pngroot // "_png"
	if (png_blk > 1)
	    png1 = pngroot // "_x" // png_blk // ".png"
	png2 = pngroot // "_" // png_pix // ".png"
	png3 = pngroot // "_" // png_pix2 // ".png"

	# Make the PNGs.
	mkgraphic (images, png1, offsets, "png", png_blk, zscale)
	convert ("-resize "//png_pix//"x"//png_pix, png1, png2)
	convert ("-resize "//png_pix2//"x"//png_pix2, png1, png3)

	# Create encapsulated FITS. Note png3 is not added.
	# Now (2015/11) there will be 2 fits with 1 encapsulated each,
        #  instead of 1 fits with 2 pngs:
	#fgwrite (png1//" "//png2, tmp//".fits", group="pipeline", verbose-)
	# t_fgwrite ("-f "//tmp//".fits", png1, png2)
	t_fgwrite ("-f "//tmp//"1.fits", png1)
	t_fgwrite ("-f "//tmp//"2.fits", png2)

	# Make the global header based on the input header image.
	# mkglbhdr (hdr, png)
	# For the large png fits header:
	mkglbhdr (hdr, png//"1")
	nhedit (png//"1", "PRODTYPE", "png1", "Data product type", add+)
	nhedit (png//"1", "MIMETYPE", "image/png", "Mime type", add+)
	nhedit (png//"1[0]", "EXTEND", "(1==1)", ".", add+)
	# For the small png fits header:
	mkglbhdr (hdr, png//"2")
	nhedit (png//"2", "PRODTYPE", "png2", "Data product type", add+)
	nhedit (png//"2", "MIMETYPE", "image/png", "Mime type", add+)
	nhedit (png//"2[0]", "EXTEND", "(1==1)", ".", add+)

	# Make the final MEF.
	# fxcopy (tmp//".fits", png//".fits", groups="1,2", new_file-, verbose-)
	fxcopy (tmp//"1.fits", png//"1.fits", groups="1", new_file-, verbose-)
	fxcopy (tmp//"2.fits", png//"2.fits", groups="1", new_file-, verbose-)

	# Clean up.
	# delete (tmp//".fits")
	delete (tmp//"1.fits")
	delete (tmp//"2.fits")

#	if (delpng) {
#	    delete (png1)
#	    delete (png2)
#	    delete (png3)
#	}
	
end
