# DPCOL -- Create data for an HTML column from FITS PNG data product.
#
# The HTML is the standard output.
# This must be run from PNG directory.

procedure dpcol (pngfits, pngdir, propid)

file	pngfits			{prompt="FITS file with PNGs"}
file	pngdir			{prompt="Directory for PNGs"}
string	propid			{prompt="Proposal ID"}
bool	large = yes		{prompt="Include large size"}
int	png_pix1 = 800		{prompt="Additional sizes"}
int	png_pix2 = 0		{prompt="Additional sizes"}

begin
	file	node, fname, fname1, pngsmall, pnglarge, png1, png2, png3, hdr
	string	dsname, shortname, pname, delim1, delim2, pngroot
	string  rpng1, rpng2, rpng3

	# Check if there is a PNG FITS file.
	if (pngfits == "") {
	    printf ('<TD ALIGN="CENTER" VALIGN="BOTTOM">&nbsp;</TD>\n')
	    return
	}

	# Derive the PNG names from the PNG FITS file name.
	pngroot = substr( pngfits, 1, strlstr("_png", pngfits)-1 )
	rpng1 = pngroot // "_x" // png_blk // ".png"
        rpng2 = pngroot // "_" // png_pix // ".png"
        rpng3 = pngroot // "_" // png_pixm // ".png"

	dsname = substr (fname,strldx("/",fname)+1,strstr("_png.fits",fname)-1)
	if (i == 0)
	    shortname = dsname
	else
	    shortname = substr (dsname, i+1, 999)

	# Get the PNG data. Copy it locally if it is on a different node.
	fname = pngfits

	if ( 1 == 0 ) {
	node = substr (fname, 1, stridx("!",fname)-1)
	if (node != envget("NHPPS_NODE_NAME")) {
	    copy (fname, ".")
	    fname = substr (fname, strldx("/",fname)+1,999)
	    node = ""
	}
	}

	# Set the names.
	dsname = substr (fname,strldx("/",fname)+1,strstr("_png.fits",fname)-1)
	i = strldx ("-", dsname)
	if (i == 0)
	    shortname = dsname
	else
	    shortname = substr (dsname, i+1, 999)
	pname = propid // "_" // shortname
	pngsmall = pname // "_" // png_pix // ".png"
	if (png_blk == 1)
	    pnglarge = pname // ".png"
	else
	    pnglarge = pname // "_x" // png_blk // ".png"
	png1 = ""
	png2 = ""
	if (png_pix1 > 0)
	    png1 = pname // "_" // png_pix1 // ".png"
	if (png_pix2 > 0)
	    png2 = pname // "_" // png_pix2 // ".png"
	hdr = pname // ".hdr"

	# Create the header listing.
	imhead (fname//"[0]", l+, > hdr)

	if (large) {
	    move( rpng1, "." )
	    rpng1 = substr( rpng1, strldx("/",rpng1)+1, 999 )
	    rename( rpng1, pnglarge )
	} else {
	    pnglarge = ""
	}

	move( rpng2, "." )
	rpng2 = substr( rpng2, strldx("/",rpng2)+1, 999 )
	rename( rpng2, pngsmall )

	move( rpng3, "." )
	rpng3 = substr( rpng3, strldx("/",rpng3)+1, 999 )
	rename( rpng3, png1 )

	if ( 1 == 0 ) { # Begin blocked out code

	# Extract the PNGs.
	# Work around a bug.
	#fgread (fname, "1,2", "", verbose-)
	fxextract (fname, "dpcol1.fits", "1", verbose-)
	fgread ("dpcol1.fits", "1", "", verbose-)
	delete ("dpcol1.fits")
	fxextract (fname, "dpcol2.fits", "2", verbose-)
	fgread ("dpcol2.fits", "1", "", verbose-)
	delete ("dpcol2.fits")
	delete (fname)

	rename (dsname//"_"//png_pix//".png", pngsmall)
	if (png_blk == 1)
	    rename (dsname//".png", pnglarge)
	else
	    rename (dsname//"_x"//png_blk//".png", pnglarge)
	if (png1 != "")
	    resize (png_pix1, pnglarge, png1)
	if (png2 != "")
	    resize (png_pix2, pnglarge, png2)

	} # End blocked out code.

	# Enter cell data.
	printf ('<TD ALIGN="CENTER" VALIGN="BOTTOM">\n')
	printf ('<IMG SRC="%s%s">\n', pngdir, pngsmall)
	printf ('<BR><A HREF="%s%s">%s</A>\n', pngdir, hdr, shortname)
	delim1 = "<BR>["; delim2 = ""
	if (pnglarge != "") {
	    printf ("%s", delim1)
	    printf ('<A HREF="%s%s">x%s</A>', pngdir, pnglarge, png_blk)
	    delim1 = ", "; delim2 = "]\n"
	}
	if (png1 != "") {
	    printf ("%s", delim1)
	    printf ('<A HREF="%s%s">%s</A>', pngdir, png1, png_pix1)
	    delim1 = ", "; delim2 = "]\n"
	}
	if (png2 != "") {
	    printf ("%s", delim1)
	    printf ('<A HREF="%s%s">%s</A>', pngdir, png2, png_pix2)
	    delim1 = ", "; delim2 = "]\n"
	}
	printf ("%s", delim2)
	printf ('</TD>\n')
end
