#{ DPPKG -- Package for making data products.

# Define dependent packages and tasks.
fitsutil
images
utilities
proto
noao
artdata
task	$plver = "$!plver"
task	$mkgraphic = "$!mkgraphic $(*)"
task	$convert = "$!convert"

package	dppkg

task	dpcol    = dppkg$dpcol.cl
task	dpgwcs   = dppkg$dpgwcs.cl
task	dphdr    = dppkg$dphdr.cl
task	dppng    = dppkg$dppng.cl
task	dpdqmask = dppkg$dpdqmask.cl
task	dpexpmap = dppkg$dpexpmap.cl
task	dprwcs   = dppkg$dprwcs.cl
task	dpwcs    = dppkg$dpwcs.cl

clbye
