# DPWCS -- Set WCS related keywords and create data files for global values.

procedure dpwcs (image, obstype, output, mosaic)

file	image			{prompt="Image header to edit"}
string	obstype			{prompt="Observation type"}
file	output			{prompt="Output file"}
bool	mosaic			{prompt="Subimage of mosaic?"}

struct	*fd

begin
	int	i1, i2
	real	xc, yc, x1, x2, x3, x4, y1, y2, y3, y4
	string  s1, s2

	if (obstype != "object")
	    return
	
	# Compute the celestial coordinates of the center and corners.
	hselect (image, "NAXIS1,NAXIS2", yes) | scan (i1, i2)
	xc = (i1 + 1) / 2.; yc = (i2 + 1) / 2.
	printf("%g %g\n", xc, yc) |
	wcsctran ("STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (xc, yc)
	printf("%d %d\n", 1, 1) |
	wcsctran ("STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x1, y1)
	printf("%d %d\n", i1, 1) |
	wcsctran ("STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x2, y2)
	printf("%d %d\n", i1, i2) |
	wcsctran ("STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x3, y3)
	printf("%d %d\n", 1, i2) |
	wcsctran ("STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x4, y4)

	# Add to header.
	printf ("%.2H\n", xc) | scan (s1)
	nhedit (image, "RA1", "dummy", "Mosaic CCD center", add+)
	nhedit (image, "RA1", s1, "Mosaic CCD center", add+)
	printf ("%.1h\n", yc) | scan (s2)
	nhedit (image, "DEC1", "dummy", "Mosaic CCD center", add+)
	nhedit (image, "DEC1", s2, "Mosaic CCD center", add+)
	nhedit (image, "CENRA1",   xc, "Mosaic CCD center", add+)
	nhedit (image, "COR1RA1",  x1, "Mosaic CCD corner", add+)
	nhedit (image, "COR2RA1",  x2, "Mosaic CCD corner", add+)
	nhedit (image, "COR3RA1",  x3, "Mosaic CCD corner", add+)
	nhedit (image, "COR4RA1",  x4, "Mosaic CCD corner", add+)
	nhedit (image, "CENDEC1",  yc, "Mosaic CCD center", add+)
	nhedit (image, "COR1DEC1", y1, "Mosaic CCD corner", add+)
	nhedit (image, "COR2DEC1", y2, "Mosaic CCD corner", add+)
	nhedit (image, "COR3DEC1", y3, "Mosaic CCD corner", add+)
	nhedit (image, "COR4DEC1", y4, "Mosaic CCD corner", add+)

	# Save for global calculation.
	print (x1, y1, >> output)
	print (x2, y2, >> output)
	print (x3, y3, >> output)
	print (x4, y4, >> output)
end
