# DPDQMASK -- Make DQMASK data product.
#
# This routine makes a pipeline PLIO FITS DQMASK data product.
# The headers are copied from the image headers except for a few
# DQMASK specific keywords.
#
# The list of PL files must match the images in the input image file in
# extension order.  Any keyword in the input image(s) pointing to the
# DQMASK must be set externally.

procedure dpdqmask (imfile, plfiles, dqmask, tmp)

file	imfile			{prompt="Image file (simple or MEF)"}
string	plfiles			{prompt="List of plfiles"}
file	dqmask			{prompt="Output DQMASK"}
string	tmp			{prompt="Root name for temporary files"}
bool	mosaic			{prompt="Mosaic MEF?"}

begin
	int	i
	string	imfile1, dqmask1, extn, imextn, dqextn

	# Remove any extension in image file. 
	i = strstr (".fits", imfile)
	if (i == 0)
	    imfile1 = imfile
	else
	    imfile1 = substr (imfile, 1, i-1)

	# Use explicit extension to allow common image rootnames.
	dqmask1 = dqmask
	if (strstr(".fits",dqmask1) == 0)
	    dqmask1 += ".fits"

	# Append DQ masks.
	files (plfiles, > tmp//"1.tmp")
	list = tmp//"1.tmp"
	for (i=1; fscan(list,plfile)!=EOF; i+=1) {
	    # Use explicit extension to allow common image rootnames.
	    if (strstr(".pl",plfile) == 0)
	        plfile += ".pl"

	    # Setup global header.
	    if (i == 1) {
		mkglbhdr (imfile1//"[0]", dqmask1)
		nhedit (dqmask1, "OBJECT", "Mask for "//imfile1, ".", add+)
		nhedit (dqmask1, "PRODTYPE", "dqmask", "", add+)
		nhedit (dqmask1, "DQMFOR", imfile1,
		    "Data for which this mask applies", add+)
	    }
	    ;

	    # Set extension name.
	    if (mosaic) {
		printf ("%s[%d]\n", imfile1, i) | scan (imextn)
		hselect (imextn, "EXTNAME", yes) | scan (extn)
	        printf ("%s[%s]\n", imfile1, extn) | scan (imextn)
		printf ("%s[%s,append,type=mask,inherit]\n", dqmask1, extn) |
		    scan (dqextn)

		# Copy DQMASK data.
		imcopy (plfile, dqextn, verbose-)

		# Add extension header.  This funny construct is to workaround
		# a non-intuitive behavior of mkheader.
		print (imextn, > tmp//"2.tmp")
		mkheader (dqextn, "@"//tmp//"2.tmp", append-, verbose-)
		delete (tmp//"2.tmp")

		# Add DQMASK specific keywords.
		nhedit (dqextn, "OBJECT", "Mask for "//imextn, ".", add+)
		nhedit (dqextn, "PRODTYPE", "dqmask", "Data product type", add+)
		nhedit (dqextn, "DQMFOR", imextn,
		    "Data for which this mask applies", add+)
	    } else {
		printf ("%s[pl,append,type=mask,inherit]\n", dqmask1) |
		    scan (dqextn)

		# Copy DQMASK data.
		imcopy (plfile, dqextn, verbose-)

		# Remove all extension keywords since they are in the global
		# header.
		nhedit (dqextn, "*", delete+, >& "dev$null")
	    }
	}
	list = ""; delete (tmp//"1.tmp")
end
