# DPRWCS -- Set resampled header for WCS related keywords.

procedure dprwcs (image)

string	image			{prompt="Image header to edit"}

begin
	int	i1, i2
	real	xc, yc, x1, x2, x3, x4, y1, y2, y3, y4
	real	seeing, pixscale
	string  s1

	# Set the pixel scale and seeing.
	#hselect (image, "$I,$RSPSCALE,$SEEINGP1", yes)
	hselect (image, "$RSPSCALE,$SEEINGP1", yes) | scan (pixscale, seeing)
	if (isindef(seeing)==NO) {
	    nhedit (image, "SEEINGP", seeing, "[pix] seeing", add+)
	    if (isindef(pixscale)==NO) {
	        seeing *= pixscale
		nhedit (image, "SEEING", seeing, "[arcsec] seeing", add+)
		nhedit (image, "PIXSCALE", pixscale,
		    "[arcsec/pix] pixel scale at tangent point", add+)
	    }
	}

	# Compute the celestial coordinates of the center and corners.
	hselect (image, "NAXIS1,NAXIS2", yes) | scan (i1, i2)
	xc = (i1 + 1) / 2.; yc = (i2 + 1) / 2.
	printf("%g %g\n", xc, yc) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (xc, yc)
	printf("%d %d\n", 1, 1) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x1, y1)
	printf("%d %d\n", i1, 1) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x2, y2)
	printf("%d %d\n", i1, i2) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x3, y3)
	printf("%d %d\n", 1, i2) |
	wcsctran( "STDIN", "STDOUT", image, inwcs="logical",
	    outwcs="world", verbose=no ) | scan (x4, y4)

	# Update the header.
	printf("%.2H\n", xc) | scan (s1)
	nhedit (image, "RA",  s1, "[h] RA center of exposure")
	printf("%.1h\n", yc) | scan (s1)
	nhedit (image, "DEC", s1, "[deg] DEC center of exposure")

	nhedit (image, "CORN4DEC", y4, "[deg] DEC corner of exposure",
	    add+, after="DEC")
	nhedit (image, "CORN3DEC", y3, "[deg] DEC corner of exposure",
	    add+, after="DEC")
	nhedit (image, "CORN2DEC", y2, "[deg] DEC corner of exposure",
	    add+, after="DEC")
	nhedit (image, "CORN1DEC", y1, "[deg] DEC corner of exposure",
	    add+, after="DEC")
	nhedit (image, "CENTDEC", yc, "[deg] DEC center of exposure",
	    add+, after="DEC")
	nhedit (image, "CORN4RA", x4, "[deg] RA corner of exposure",
	    add+, after="DEC")
	nhedit (image, "CORN3RA", x3, "[deg] RA corner of exposure",
	    add+, after="DEC")
	nhedit (image, "CORN2RA", x2, "[deg] RA corner of exposure",
	    add+, after="DEC")
	nhedit (image, "CORN1RA", x1, "[deg] RA corner of exposure",
	    add+, after="DEC")
	nhedit (image, "CENTRA", xc, "[deg] RA center of exposure",
	    add+, after="DEC")
end
