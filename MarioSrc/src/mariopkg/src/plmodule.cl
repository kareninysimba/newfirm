#{ PLMODULE.CL -- Pipeline module initialization.

# Set all the dataset naming.
s1 = nhpps.pipeline // "names"
if (deftask(s1))
    printf ("%s (nhpps.dataset); keep\n", s1) | cl
else
    printf ("names (nhpps.pipeline, nhpps.dataset); keep\n") | cl

# Write standard log output.
printf ("\n%s (%s): ", strupr(nhpps.module), nhpps.dataset)
time

# Do things that depend on the datadir being present.
if (access(names.datadir)) {
    reset (tmp = names.datadir)
    reset (uparm = names.uparm)
    reset (pipedata = names.pipedata)
    cd (names.datadir)

    printf ("\n%s (%s): ", strupr(nhpps.module), nhpps.dataset,
	>> names.lfile)
    time (>> names.lfile)

    # Delete and restore files from previous processing.
    # It is the responsibility of the module to add files that need to
    # be delete or restored upon entry.
    fdelete = nhpps.module // "_delete.tmp"
    if (access(fdelete)) {
        delete ("@"//fdelete)
        delete (fdelete)
    }
    ;
    frestore = nhpps.module // "_restor.tmp"
    if (access(frestore)) {
        delete ("@"//frestore)
        delete (frestore)
    }
    ;
}
;

keep
