# PHOTDPTH -- Photometric depth data quality parameters.
# The results are written to output parameters.
#
# The assumption is that noise, as calculated by ACE, contains
# all contributions (e.g., sky noise, read noise), that
# the noise is not correlated between pixels, and that the 
# sky background is taken beyond where the PSF 'ends'.
# Note that the exposure times have all been scaled to
# 1 second, so the exposure time is left out of the calculations.

procedure photdpth (sig, seeing, magzero, pixscale)

real	sig		{prompt="Sky sigma per pixel"}
real	seeing		{prompt="Seeing (pixels)"}
real	magzero		{prompt="Magnitude zero point"}
real	pixscale	{prompt="Pixel scale (arcsec/pix)"}
real	aperture = 3	{prompt="Aperture size (arcsec)"}
int	verbose = 0	{prompt="Verbose level?\n"}

real	dpthadu		{prompt="5 sigma seeing detection (adu)"}
real	dqphapsz	{prompt="Aperture diameter (arcsec)"}
real	dqphdpap	{prompt="3 sigma aperture detection"}
real	dqphdppx	{prompt="3 sigma pixel detection"}
real	dqphdpps	{prompt="5 sigma seeing detection"}

begin
	real	r

	if (isindef(sig) || sig <= 0) {
	    dpthadu = INDEF
	    dqphapsz = INDEF
	    dqphdpap = INDEF
	    dqphdppx = INDEF
	    dqphdpps = INDEF

	} else  {

	    # 3 SIGMA DETECTION IN SINGLE PIXEL (dqphdppx)
	    dqphdppx = 3 * sig

	    # 5 SIGMA DETECTION OF POINT SOURCE (dqphdpps)
	    # T. Lauer requested 5 sigma threshold and an optimal
	    # aperture of 1.35 FWHM (which is formally optimal for
	    # pure Gaussian FWHM).
	    if (isindef(seeing) || seeing <= 0)
	        dpthadu = INDEF
	    else {
		r = 1.35 * seeing / 2
		dpthadu = 5 * sig * SQRTPI * r
	    }

	    # 3 SIGMA DETECTION WITHIN FIXED APERTURE (dqphdpap)
	    # TODO get aperture size from DQ ruleset, for now, assume 3 arcsec.
	    if (isindef(pixscale) || pixscale <= 0 ||
	        isindef(aperture) || aperture <= 0) {
		dqphapsz = INDEF
	        dqphdpap = INDEF
	    } else {
		dqphapsz = aperture
		r = (dqphapsz/pixscale) / 2
		dqphdpap = 3 * sig * SQRTPI * r
	    }
	}

	# Convert to magnitudes.
	if (!isindef(magzero)) {
	    if (!isindef(dpthadu))
		dqphdpps = -2.5 * log10 (dpthadu) + magzero
	    if (!isindef(dqphdpap))
		dqphdpap = -2.5 * log10 (dqphdpap) + magzero
	    if (!isindef(dqphdppx))
		dqphdppx = -2.5 * log10 (dqphdppx) + magzero
	}

	# Write output if desired.
	if (verbose >= 1) {
	    printf ("DQ | sig=%g, seeing=%g, magzero=%g, pixscale=%g\n",
	        sig, seeing, magzero, pixscale)
	    printf ("DQ | 5 sigma photometric depth within 1.35*seeing %f\n",
		dqphdpps )
	    printf ("DQ | 3 sigma photometric depth in %f arcsec aperture %f\n",
		dqphapsz, dqphdpap )
	    printf ("DQ | 3 sigma photometric depth in single pixel %f\n",
		dqphdppx )
	}
end
