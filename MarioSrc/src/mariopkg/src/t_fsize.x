# Copyright(c) 2004 Association of Universities for Research in Astronomy Inc.
# task fsize = t_fsize

include	<finfo.h>



# FSIZE -- Print file size in K given a file name.
# Returns -1 in case of error.

procedure t_fsize ()

pointer	sp
long	fi[LEN_FINFO]
real    size
char	fname[SZ_FNAME]			# file name

int	finfo(), access()

begin
    call smark (sp)
    
    call clgstr ("input", fname, SZ_FNAME)
    
    # Get file directory information.
	if (finfo (fname, fi) != OK) {
	    call eprintf ("Cannot get info on file `%s'\n")
		call pargstr (fname)
	    call sfree (sp)
	    return
	}
	
	size = real (FI_SIZE(fi)) / 1024.0
	
	call printf ("%f\n")
	    call pargr (size)
    
    # call flush ()
	call sfree (sp)
end
