#{ PLCONFIG.CL -- Pipeline configuration.

# Read a pipeline application configuration file and then allow overrides.
# Note that below we use cm for the pipeline application and dm for
# the configuration directory.

line = "NHPPS_PIPESRC$/" // nhpps.application // "/config/"
s2 = line // nhpps.application // ".cl"
if (access(s2)) {
    cl (< s2)

    line = "PipeConfig$/"
    s2 = line // nhpps.application // ".cl"
    if (access(s2))
	cl (< s2)
    ;

    if (nhpps.dataset != "None") {
	s1 = nhpps.dataset

	# Look for matches against elements of root dataset name.
	s3 = ""
	i = stridx ("-", s1)
	if (i > 0) {
	    s3 = substr (s1, i+1, 1000)
	    s1 = substr (s1, 1, i-1)
	    i = stridx ("-", s3)
	    if (i > 0)
		s3 = substr (s3, 1, i-1)
	    ;
	    s3 = "-" // s3
	}
	;
	for (i=stridx("_",s1); i>0; i=stridx("_",s1)) {
	    s2 = line // substr (s1, 1, i-1) // ".cl"
	    if (access(s2))
		cl (< s2)
	    ;
	    s2 = line // substr (s1, 1, i-1) // s3 // ".cl"
	    if (access(s2))
		cl (< s2)
	    ;
	    s1 = substr (s1, i+1, 1000)
	}
	s2 = line // s1 // ".cl"
	if (access(s2))
	    cl (< s2)
	;
	s2 = line // s1 // s3 // ".cl"
	if (access(s2))
	    cl (< s2)
	;

	# Look for mapped files.
	s1 = nhpps.dataset

	line = "NHPPS_PIPESRC$/" // nhpps.application // "/config/"
	s2 = line // "ConfigMap.list"
	if (access(s2)) {
	    list = s2
	    while (fscan (list, s3, s2) != EOF) {
		print (s1) | match (s3, meta+, stop-) | scan (s3)
		if (s3==s1 && access(line//s2))
		    cl (<line//s2)
		;
	    }
	    list = ""
	}
	;

	line = "PipeConfig$/"
	s2 = line // "ConfigMap.list"
	if (access(s2)) {
	    list = s2
	    while (fscan (list, s3, s2) != EOF) {
		print (s1) | match (s3, meta+, stop-) | scan (s3)
		if (s3==s1 && access(line//s2))
		    cl (<line//s2)
		;
	    }
	    list = ""
	}
	;
    }
    ;
}
;

# Now set global environment and CL variables that depend on the
# pipeline application.

if (strstr (nhpps.application, osfn ("MarioCal$")) == 0)
    set MC = "MarioCal$/" // nhpps.application // "/"
else
    set MC = "MarioCal$/"

dm = envget (nhpps.application//"_DM_PORT") // ":" // envget (nhpps.application//"_DM_NODE")
cm = envget (nhpps.application//"_CM_PORT") // ":" // envget (nhpps.application//"_CM_NODE")

keep
