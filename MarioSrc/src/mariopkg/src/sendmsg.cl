# SENDMSG -- Send a message or email.

procedure sendmsg (level, msg, arg, id)

string	level = "ERROR"			{prompt="Level"}
string	msg = ""			{prompt="Message"}
string	arg = ""			{prompt="Message argument"}
string	id = "None"			{prompt="ID"}
string	dataset = "!OSF_DATASET"	{prompt="Dataset"}
string	module = "!NHPPS_MODULE_NAME"	{prompt="Module"}
string	node = "!NHPPS_NODE_NAME"	{prompt="Node"}
string	server = "!NHPPS_NODE_MM"	{prompt="Server"}
string	port = "!NHPPS_PORT_MM"		{prompt="Port"}

begin
	string	lv, ds, md, nd, sr, pt
	struct	mg, mg1, ag, cmd

	lv = level
	mg = msg
	ag = arg
	ds = nhpps.dataset
	md = nhpps.module
	nd = node
	sr = server
	pt = port

	if (!deftask("translit"))
	    utilities
	print (mg) | translit ("STDIN", "'", del+) | scan (mg)
	print (ag) | translit ("STDIN", "'", del+) | scan (ag)

	mg1 = mg
	if (ag != "")
	    printf ("%s (%s)\n", mg1, ag) | scan (mg1)
	if (substr (ds, 1, 1) == "!")
	    ds = envget (substr(ds,2,1000))
	if (substr (md, 1, 1) == "!")
	    md = strupr (envget (substr(md,2,1000)))
	if (substr (nd, 1, 1) == "!")
	    nd = envget (substr(nd,2,1000))
	if (substr (sr, 1, 1) == "!")
	    sr = envget (substr(sr,2,1000))
	if (substr (pt, 1, 1) == "!")
	    pt = envget (substr(pt,2,1000))

	# Send message or email.
	if (id == "MAIL") {
	    if (email == "")
	        return

	    if (substr(mg,1,1) == "@")
		printf ("!!cat %s | mail -s '%s: %s' %s\n",
		   substr(mg,2,1000), mg, lv, ds, email) | cl
	    else
		printf ("!!echo '%s' | mail -s '%s: %s' %s\n",
		   mg1, lv, ds, email) | cl
	} else {
	    printf ("!!sendmsg.py -level %s -dataset %s -id %s -module %s \
		-node %s -server %s -port %s\n", lv, ds, id, md, nd, sr, pt) |
		scan (cmd)
	    if (strlen (cmd) + strlen (mg1) < 250)
		printf ("%s '%s'\n", cmd, mg1) | cl
	    else {
		if (strlen (cmd) + strlen (mg) < 250) {
		    printf ("%s '%s'\n", cmd, mg) | cl
		    if (ag != "")
			printf ("    %s\n", ag)
		} else {
		    printf ("%s\n", cmd) | cl
		    printf ("    %s\n", mg)
		    if (ag != "")
			printf ("    %s\n", ag)
		}
	    }
	}
end
