# SCMB -- Run IMCOMBINE with possible grouping.

procedure scmb (imlist, output, bpmask, scale, tmproot)

file	imlist				{prompt="File with images"}
file	output				{prompt="Output image"}
file	bpmask				{prompt="Output mask"}
file	scale				{prompt="File with scale values"}
file	tmproot				{prompt="Root name for temp files"}
string	imcmb = ""			{prompt="Keyword for contributors"}
string	reject = "pclip"		{prompt="Type of rejection"}
int	ngroup = 100			{prompt="Max number per group"}
string	masktype = "!objmask"		{prompt="Mask type"}
int	maskvalue = 0			{prompt="Mask value"}
string	logfile = ")mscred.logfile"	{prompt="Log file"}

struct	*fd

begin
	file	ims, out, im
	int	i, j, ncomb, ncomb1, ng, ngroups
	real	x, norm

	ims = imlist
	out = output

	# Check number to combine.
	count (ims) | scan (ncomb)
	if (ncomb == 0)
	    return
	if (isindef(ngroup))
	    ng = ncomb
	else
	    ng = ngroup

	# Create scales if needed.
	if (!access(scale)) {
	    imstatistics ("@"//imlist, fields="mode", format-,
		lower=-999, > tmproot//".tmp")
	    norm = INDEF
	    fd = tmproot//".tmp"
	    while (fscan (fd, x) != EOF) {
	        x = max (x, 1e-8)
	        if (isindef(norm))
		    norm = x
		x = norm / x
		print (x, >> scale)
	    }
	    fd = ""; delete (tmproot//".tmp")
	}

	# Combine with scaling and possible subgroups.
	ngroups = (ncomb-1) / ng + 1
	ncomb1 = (ncomb + ngroups - 1) / ngroups
	if (ngroups == 1)
	    imcombine ("@"//ims, out, bpmask=bpmask, masktype=masktype,
	        maskvalue=maskvalue, scale="@"//scale,
		reject=reject, imcmb=imcmb,
		headers="", rejmasks="", nrejmasks="", expmasks="",
		sigmas="", logfile=logfile, combine="average",
		project=no, outtype="real", outlimits="", offsets="none",
		blank=1., zero="none", weight="none", statsec="",
		expname="", lthreshold=INDEF, hthreshold=INDEF, nlow=0,
		nhigh=1, nkeep=1, mclip=yes, lsigma=3., hsigma=3.,
		rdnoise="rdnoise", gain="gain", snoise="0.", sigscale=0.1,
		pclip=-0.9, grow=3.)
	else {
	    printf ("ncomb=%d, ngroups=%d, ncomb1=%d\n", ncomb, ngroups, ncomb1)
	    joinlines (ims, scale, output=tmproot//"3.tmp", maxchar=512)
	    fd = tmproot//"3.tmp"
	    for (j=1; j<=ngroups; j+=1) {
		for (i=1; i<=ncomb1 && fscan(fd,im,x)!=EOF; i+=1) {
		    print (im, >> tmproot//"1.tmp")
		    print (x, >> tmproot//"2.tmp")
		    if (i == 1)
			print (x, >> tmproot//"4.tmp")
		    ;
		}

		imcombine ("@"//tmproot//"1.tmp", tmproot//"_im"+j,
		    bpmask=tmproot//"_bpm"+j, masktype=masktype,
		    maskvalue=maskvalue, scale="@"//tmproot//"2.tmp",
		    reject=reject, imcmb=imcmb,
		    headers="", rejmasks="", nrejmasks="", expmasks="",
		    sigmas="", logfile=logfile, combine="average",
		    project=no, outtype="real", outlimits="", offsets="none",
		    blank=1., zero="none", weight="none", statsec="",
		    expname="", lthreshold=INDEF, hthreshold=INDEF, nlow=0,
		    nhigh=1, nkeep=1, mclip=yes, lsigma=3., hsigma=3.,
		    rdnoise="rdnoise", gain="gain", snoise="0.", sigscale=0.1,
		    pclip=-0.9, grow=3.)

		delete (tmproot//"[12].tmp", verify-)
	    }
	    fd = ""; delete (tmproot//"3.tmp")

	    imcombine (tmproot//"_im*", out, bpmask=bpmask, masktype="!bpm",
	        maskvalue=0, scale="@"//tmproot//"4.tmp",
		reject="none", imcmb="",
		headers="", rejmasks="", nrejmasks="", expmasks="",
		sigmas="", logfile=logfile, combine="average",
		project=no, outtype="real", outlimits="", offsets="none",
		blank=1., zero="none", weight="none", statsec="",
		expname="", lthreshold=INDEF, hthreshold=INDEF, nlow=0,
		nhigh=1, nkeep=1, mclip=yes, lsigma=3., hsigma=3.,
		rdnoise="rdnoise", gain="gain", snoise="0.", sigscale=0.1,
		pclip=-0.9, grow=3.)

	    delete (tmproot//"4.tmp")
	    imdelete (tmproot//"_*")
	}
end
