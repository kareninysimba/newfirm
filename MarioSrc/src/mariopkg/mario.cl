#{ MARIO - Mario specific package

cl < "mario$lib/zzsetenv.def"
package mario, bin = mariobin$

task	$plconfig	= "mariosrc$plconfig.cl"
task	plmodule	= "mariosrc$plmodule.cl"
cache	plmodule

task	plexit		= "mariosrc$plexit.cl"
cache	plexit

set	dppkg		= "mariosrc$dppkg/"
task	$dppkg.pkg	= "dppkg$dppkg.cl"

task	dayfrgg,
	dmag,
	export,
	filesize,
	mefmerge,
	mskregions,
	ngtgroup,
	usnomap,
	usnomatch	= "mariosrc$x_mario.e"

task	photdpth	= "mariosrc$photdpth.cl"
cache	photdpth

task	sendmsg		= "mariosrc$sendmsg.cl"
sendmsg.node		= envget("NHPPS_NODE_NAME")
sendmsg.server		= envget("NHPPS_NODE_MM")
sendmsg.port		= envget("NHPPS_PORT_MM")

task	sendtrig	= "mariosrc$sendtrig.cl"

set	BIN		= "NHPPS_PIPESRC$/BIN/"

task	setdirs		= "BIN$setdirs.cl"
task	names		= "BIN$names.cl"
cache	setdirs, names

if (nhpps.application == "MOSAIC") {
    # Set instrument variable based on dataset.  This needs to be the
    # value used in the calibration manager.

    if (substr(nhpps.dataset,1,3)=="C4M")
        instrument = "Mosaic2"
    else if ( substr(nhpps.dataset,1,4)=="K4M0" ||
	substr(nhpps.dataset,1,6)=="K4M10A")
	instrument = "CCDMosaThin1"
    else if (substr(nhpps.dataset,1,3) == "K4M")
	instrument = "Mosaic1.1"
    ;

    # Setdirs
    setdirs.udetector = instrument
    setdirs.pdetector = instrument
    setdirs.pmatch = "!NEXTEND"

    # Names
    task calnames = "NHPPS_PIPEAPPSRC$/CAL/calnames.cl"
    task sclnames = "NHPPS_PIPEAPPSRC$/SCL/sclnames.cl"
    task mefnames = "NHPPS_PIPEAPPSRC$/MEF/mefnames.cl"
    task sifnames = "NHPPS_PIPEAPPSRC$/SIF/sifnames.cl"
    task daynames = "NHPPS_PIPEAPPSRC$/DAY/daynames.cl"
    task pgrnames = "NHPPS_PIPEAPPSRC$/PGR/pgrnames.cl"
    task frgnames = "NHPPS_PIPEAPPSRC$/FRG/frgnames.cl"
    task sftnames = "NHPPS_PIPEAPPSRC$/SFT/sftnames.cl"
    task rspnames = "NHPPS_PIPEAPPSRC$/RSP/rspnames.cl"
    task mdpnames = "NHPPS_PIPEAPPSRC$/MDP/mdpnames.cl"
    task mdcnames = "NHPPS_PIPEAPPSRC$/MDC/mdcnames.cl"
    task ctrnames = "NHPPS_PIPEAPPSRC$/CTR/ctrnames.cl"
    task strnames = "NHPPS_PIPEAPPSRC$/STR/strnames.cl"

    cache calnames, sclnames, mefnames, sifnames, daynames
    cache pgrnames, frgnames, sftnames, rspnames, mdpnames
    cache mdcnames, ctrnames, strnames
} else if (nhpps.application == "NEWFIRM") {
    instrument = "NEWFIRM"

    # Setdirs
    setdirs.udetector = instrument
    setdirs.pdetector = instrument

    # Names
    task gosnames = "NHPPS_PIPEAPPSRC$/GOS/gosnames.cl"
    task sdknames = "NHPPS_PIPEAPPSRC$/SDK/sdknames.cl"
    task sflnames = "NHPPS_PIPEAPPSRC$/SFL/sflnames.cl"
    task ogcnames = "NHPPS_PIPEAPPSRC$/OGC/ogcnames.cl"
    task skynames = "NHPPS_PIPEAPPSRC$/SKY/skynames.cl"
    task spinames = "NHPPS_PIPEAPPSRC$/SPI/spinames.cl"
    task spsnames = "NHPPS_PIPEAPPSRC$/SPS/spsnames.cl"
    task swcnames = "NHPPS_PIPEAPPSRC$/SWC/swcnames.cl"
    task wcsnames = "NHPPS_PIPEAPPSRC$/WCS/wcsnames.cl"
    task mkdnames = "NHPPS_PIPEAPPSRC$/MKD/mkdnames.cl"
    
    cache gosnames, sdknames, sflnames, ogcnames, spsnames
    cache skynames, swcnames, wcsnames, mkdnames, spinames

} else
    ;

clbye
