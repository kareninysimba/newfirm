#!/bin/env python

import getopt
import sys
import os

rootDataDir = 'MarioData'
rootPipeSrc = 'NHPPS_PIPESRC'

def getPipeApps (pipeSrc=os.environ [rootPipeSrc]):
    
    """
    Gets the pipeline applications names from the listing
    of the dataRoot directory...
    """
    
    pipeApps = os.listdir (pipeSrc)
    pipeApps = [item for item in pipeApps if item.isupper ()]
    if 'CVS' in pipeApps:
        pipeApps.remove ('CVS')
        pass
    if 'BIN' in pipeApps:
        pipeApps.remove ('BIN')
        pass
    return pipeApps

def createRootDataDir (prompt, target):
    
    """
    Generates the Root data directory...
    """

    print 'Creating %s...' % rootDataDir
    return mkmariodir (os.environ [rootDataDir], prompt, target)

def createPipeDataDirs (app, prompt, target):
    
    """
    """
    
    print 'Creating the Pipeline Application Data directories...'
    for pipeApp in app:
        os.makedirs (os.path.join (
            os.environ [rootDataDir],
            pipeApp,
            'definitions'))
        cleanall (pipeApp)
        pass
    return 0

def parseCmdLine ():
    opts, args = getopt.gnu_getopt (sys.argv [1:], 'a:,q', ['app=', 'query='])
    app   = None
    query = True
    for (arg, val) in opts:
        if '-a' in arg:
            app = val
            pass
        elif '-q' in arg:
            val = val.lower ()
            if val and val [0] in ['t', 'y']:
                query = True
            else:
                query = False
                pass
            pass
        pass
    if args:
        args = args [0]
        pass
    if app:
        app = [app]
    else:
        app = getPipeApps ()
        pass
    return app, query, args

def cleanall (pipeApp):
    return os.system ('cleanall %s' % (pipeApp))

def main ():
    try:
        marioData = os.environ [rootDataDir]
    except:
        sys.stderr.write ('%s environment variable is not defined.\n' % (rootDataDir))
        return 1
    
    app, query, directory = parseCmdLine ()
    createRootDataDir (query, directory)
    sys.exit (createPipeDataDirs (app, query, directory))

def getYN (msg):
    
    """
    Gets a y|n answer to a prompt...
    """
    
    y, n = 'y', 'n'
    ans = None
    while not ans:
        try:
            ans = input (msg)
            if ans != y and ans != n:
                ans = None
                pass
            pass
        except:
            pass
        pass
    return ans

def rmdir (directory, keepDir=False):
    """
    Emulates the system call `rm -rf $directory`
    """
    
    if os.path.exists (directory) and os.path.isdir (directory):
        for root, dirs, files in os.walk (directory, topdown=False):
            for name in files:
                os.remove (os.path.join (root, name))
                pass
            for name in dirs:
                currDir = os.path.join (root, name)
                if os.path.islink (currDir):
                    os.remove (currDir)
                elif os.path.isdir (currDir):
                    os.rmdir (currDir)
                    pass
                pass
            pass
        if not keepDir:
            if os.path.exists (directory):
                os.rmdir (directory)
                return os.path.exists (directory)
            pass
        return os.path.exists (directory)
    return False

def mkmariodir (directory, pipeapp, prompt=True, target=None):

    ans = 'y'
    
    if directory [-1] == os.path.sep:
        directory = directory [0:-1]
        pass
    
    if not target and os.path.islink (directory):
        # get the path the symlink points to...
        target = os.path.realpath (directory)
        pass
    oldtarg = ""
    
    if os.path.exists (directory):
        if os.path.islink (directory):
            if prompt:
                ans = getYN ('Old link %s exists. Reset (y|n)? ' % (directory))
                pass
            if ans == 'n':
                return 2
            oldtarg = os.path.realpath (directory)
            if os.path.exists (oldtarg):
                if prompt:
                    ans = getYN ('Old target directory %s exists.  Delete (y|n)? ' % (oldtarg))
                    if ans == 'y':
                        rmdir (oldtarg)
                        pass
                    pass
                else:
                    if target == oldtarg:
                        rmdir (oldtarg)
                        pass
                    pass
                pass
            rmdir (directory)
            pass
        else:
            if prompt:
                ans = getYN ('Old directory %s exists.  Delete (y|n)? ' % (directory))
                pass
            if ans == 'n':
                return 3
            rmdir (directory)
            pass
        pass
    
    ans = 'y'
    if not target:
        print 'Creating new directory', directory
        os.makedirs (directory)
    else:
        if os.path.exists (target):
            if target != oldtarg:
                if prompt:
                    ans = getYN ('New target directory %s exists.  Delete (y|n)? ' % (target))
                    pass
                if ans == 'y':
                    rmdir (target)
                    print 'Creating new target directory', target
                    os.makedirs (target)
                    pass
                pass
            else:
                ans = 'n'
                pass
            pass
        else:
            print 'Creating new target directory', target
            os.makedirs (target)
            pass
        
        if target != directory:
            print 'Creating new link', directory, 'to target directory', target
            if os.path.islink (directory):
                os.remove (directory)
                pass
            os.symlink (os.path.realpath (target), directory)
            pass
        pass
    
    if ans == 'y':
        return 0
    else:
        return 1
    pass

def mkrootdir (directory, prompt=True, target=None):
    
    ans = 'y'
    
    if directory [-1] == os.path.sep:
        directory = directory [0:-1]
        pass
    
    if not target and os.path.islink (directory):
        # get the path the symlink points to...
        target = os.path.realpath (directory)
        pass
    oldtarg = ""
    
    if os.path.exists (directory):
        if os.path.islink (directory):
            if prompt:
                ans = getYN ('Old link %s exists. Reset (y|n)? ' % (directory))
                pass
            if ans == 'n':
                return 2
            oldtarg = os.path.realpath (directory)
            if os.path.exists (oldtarg):
                if prompt:
                    ans = getYN (
                        'Old target directory %s exists.  Delete (y|n)? ' % (
                        oldtarg))
                    if ans == 'y':
                        rmdir (oldtarg)
                        pass
                    pass
                else:
                    if target == oldtarg:
                        rmdir (oldtarg)
                        pass
                    pass
                pass
            os.remove (directory)
            pass
        else:
            if prompt:
                ans = getYN ('Old directory %s exists.  Delete (y|n)? ' % (
                    directory))
                pass
            if ans == 'n':
                return 3
            rmdir (directory)
            pass
        pass
    
    ans = 'y'
    if not target:
        print 'Creating new directory', directory
        os.makedirs (directory)
    else:
        #target = os.path.join (target, os.path.basename (directory))
        if os.path.exists (target):
            if target != oldtarg:
                if prompt:
                    ans = getYN (
                        'New target directory %s exists.  Delete (y|n)? ' % (
                        target))
                    pass
                if ans == 'y':
                    rmdir (target)
                    print 'Creating new target directory', target
                    os.makedirs (target)
                    pass
                pass
            else:
                ans = 'n'
                pass
            pass
        else:
            print 'Creating new target directory', target
            os.makedirs (target)
            pass
        
        if target != directory:
            print 'Creating new link', directory, 'to target directory', target
            if os.path.islink (directory):
                os.remove (directory)
                pass
            os.symlink (os.path.realpath (target), directory)
            pass
        pass
    
    if ans == 'y':
        return 0
    else:
        return 1
    pass

#def cleanall ():
#    if '+MarioHome' in sys.argv:
#        rmdir (os.environ ['MarioHome'], keepDir=True)
#        sys.argv.remove ('+MarioHome')
#        pass
#    if '+MarioCal' in sys.argv:
#        rmdir (os.environ ['MarioCal'], keepDir=True)
#        sys.argv.remove ('+MarioCal')
#        pass
#    sys.argv.pop (0)
#
#    exclude = ""
#    for pipe in sys.argv:
#        exclude += pipe + ' '
#
#    for pipeapp in getPipeApps ():
#        os.system ('cleanPipeApp %s %s' % (pipeapp, exclude))
#        pass
#    return

if __name__ == '__main__':
    sys.exit (main ())
    pass
