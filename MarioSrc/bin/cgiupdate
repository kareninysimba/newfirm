#!/bin/csh -f
#
## =========
## cgiupdate
## =========
## ----------------------------------------------------------
## Update /www/cgi-bin from/to the local pipeline source code
## ----------------------------------------------------------
## 
## :Manual section: 1
## :Manual group:   MarioSrc/bin
## 
## Usage
## -----
##
## cgiupdate [options] *command*
##
## -n
##    perform trial run with no changes made
## -f
##    force execution without prompting user
##
## *command* is either srctocgi or cgitosrc.
##
## Description
## -----------
##
## Update the contents of /www/cgi-bin from the local pipeline source
## code installation, or the other way around. Retrieval from or sub-
## mission to the CVS archive is not handled by this script.
##
## To update /www/cgi-bin from the local pipeline source, use
## "cgiupdate srctocgi", and to merge changes made in /www/cgi-bin
## back into the local source, use "cgiupdate cgitosrc".
##
## Before updating, the script does a trial run, presents the results
## to the user and ask for confirmation to proceed. When **-n** is 
## specified, only the trial run is executed. With **-f**, the script
## carries out only the final update, without the trial run, and without
## asking for confirmation.

set noglob

if ($# == 0) then
    goto Usage
endif

set nflag = 0
set fflag = 0
@ counter = 0
while ($# > 0)
    switch ($1)
    case -f:
        set fflag = 1
	breaksw
    case -h:
        goto Usage
    case -n:
        set nflag = 1
	breaksw
    default:
        @ counter = $counter + 1
        set command = $1
    endsw
    shift
end

if ( $?command == 0 ) goto Usage
if ( $counter > 1 ) goto Usage

set src = "$pipeshared/www/cgi-bin/"
switch ($command)
    case srctocgi:
        set rsync = "rsync -avn --delete --exclude=CVS --exclude=*.pyc $src /www/cgi-bin/"
        set text = "Updating from source to /www/cgi-bin."
	breaksw
    case cgitosrc:
        set rsync = "rsync -avn --delete --exclude=CVS --exclude=*.pyc /www/cgi-bin/ $src"
	set text = "Updating from /www/cgi-bin to source."
	breaksw
    default:
        goto Usage
endsw

set answer = 'n'
if ( $fflag == 0 ) then
    $rsync
    if ( $nflag ) exit 0
    echo -n "$text OK to proceed (y/n)? "
    set answer = $<
endif

if ( ( $answer == 'y' ) || ( $fflag ) ) then
    set rsync = `echo $rsync | sed -e s/avn/av/`
    $rsync
endif

exit 0

Usage:
    echo Usage: cgiupdate \[options\] command
    echo "-f   force execution without prompting user"
    echo "-n   perform a trial run with no changes made"
    echo '"command" is either "srctocgi" or "cgitosrc".'
    exit 0
