#!/bin/env python

"""Data Manager (DM) database merge tool.

Description

Allows the contents of one DM-formatted database to be merged
with those in a running DM. Typically, this is designed
to support updating a master-mode DM with data from a nightly 
run of a telescope-mode DM.

Operation

The Data Manager (DM) runs as a server accepting connection through a given port.
The mode of operation is essentially that of a web server. Connections are
stateless. The Data Manager always return a value. Communications follow the
Pipeline Team RPC Protocol. The merge tool contacts the indicated Data Manager 
and transmits its information for inclusion in the DM. 

Usage

merge_dm_db.py [-clobber|-caldir <dir>|-v] -port <port> -addr <ip_addr> <database file>

"""

import re
import sys
import string
from PipelineServers.DataManager import open_connection, initTo
from pipeutil import *

HALT_ON_WARNING = False
PROG_NAME = "merge_dm_db.py"
TIMEOUT = 10 # how long to wait for response from master dm for response, in sec
DRY_RUN = False


def merge_dm_database (addr, port, database, caldir, clobber=False, verbose=False,\
                       dry_run=False, halt_on_warning=False):

    if(verbose): print "Start %s " % PROG_NAME

    # 1. Open our local database
    dbconnection = open_connection( database)
    if (verbose): print "  * DB Connection is %s " % dbconnection

    # do the merge
    initTo(addr, port, dbconnection, caldir=caldir, clobber=clobber, verbose=verbose,\
            dry_run=dry_run, halt_on_warn=halt_on_warning)

    # finished!
    return


"""
def warn (prefix, msg):

    warning (prefix, msg)
    if (HALT_ON_WARNING): 
        print "** %s halted on warning **" % PROG_NAME
        sys.exit(-1)
"""


if __name__ == '__main__':

    ADDR = None
    CALDIR = None
    CLOBBER = False
    PORT = 9000
    VERBOSE = False
    PROG_NAME = sys.argv[0]
    DATABASE = None

    VERBOSE_ARG_REGEX = re.compile("^-v.*$")
    ADDR_ARG_REGEX = re.compile("^-addr.*$")
    CALDIR_ARG_REGEX = re.compile("^-caldir.*$")
    CLOBBER_ARG_REGEX = re.compile("^-cl.*$")
    DRY_RUN_ARG_REGEX = re.compile("^-dry.*$")
    WHALT_ARG_REGEX = re.compile("^-halt_on_w.*$")
    VERBOSE = False

    if (len (sys.argv) <= 5):
        print "Usage: %s [-halt_on_warn|-clobber|-dry_run|-v] -addr <ip_addr:port> -caldir <dir> <database file>" % PROG_NAME
        sys.exit(-1)

    readAddrParam = False
    readCalDirParam = False
    for arg in sys.argv[1:]:
        #print "got arg: %s" % arg
        if (VERBOSE_ARG_REGEX.match(arg)):
            VERBOSE = True
            warning(PROG_NAME, "** running in VERBOSE mode **")
            continue
        elif (ADDR_ARG_REGEX.match(arg)):
            readAddrParam = True
            continue
        elif (CALDIR_ARG_REGEX.match(arg)):
            readCalDirParam = True
            continue
        elif (CLOBBER_ARG_REGEX.match(arg)):
            CLOBBER = True
            warning(PROG_NAME, "** running in CLOBBER mode (DB corruption may result) **")
            continue
        elif (DRY_RUN_ARG_REGEX.match(arg)):
            DRY_RUN = True
            warning(PROG_NAME, "** running in DRY_RUN mode **")
            continue
        elif (WHALT_ARG_REGEX.match(arg)):
            HALT_ON_WARNING = True
            continue
        elif (readAddrParam):
            try:
                (ADDR, PORT) = string.split(arg, ':')
            except ValueError:
                ADDR = arg

            readAddrParam = False
        elif (readCalDirParam):
            CALDIR = arg
            readCalDirParam = False
        else:
            DATABASE = arg

    # sanity checks
    if (not ADDR):
       fatalError ("can't proceed: missing IP address")

    if (not PORT):
       fatalError ("can't proceed: missing IP address PORT")

    if (not CALDIR):
       fatalError ("can't proceed: missing CALDIR argument")

    # run the program
    merge_dm_database(ADDR, PORT, DATABASE, CALDIR, CLOBBER, VERBOSE, DRY_RUN, HALT_ON_WARNING)

