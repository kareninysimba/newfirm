#!/bin/csh -f
#
## ========
## stbcheck
## ========
## -------------------------------------------------------------
## Compare number of submitted entries against number in archive
## -------------------------------------------------------------
##
## :Manual section: 1
## :Manual group: MarioSrc/bin
## 
## Usage
## -----
## 
## ``stbcheck dataset``
##
## Description
## -----------
##
## The number of files submitted to the archive is determined from
## the stbsubmit log files. The number of entries present in the 
## archive is determined from the contents of 'pldname'. Both
## numbers are output in a short report.
##
## Examples
## --------
##
## | % stbcheck C4N11A_20110504


# Print usage if no arguments.
if ($# < 1) then
    plman -o txt `which stbcheck`
    exit
endif

echo Report for $1
echo -----------------------------------------------
\echo -e -n "Number of entries in the archive:\t"
psql -A <<EOF | awk '{if(NR==2){print $0}}'
select count(pldname) from voi.siap where pldname like '$1%';
EOF
\echo -e -n "Number of submitted entries:\t\t"
cd $NEWFIRM_STB/data
cat $1_*/stbsubmit* | grep Sending | wc -l
echo
