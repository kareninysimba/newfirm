#!/bin/csh -f
#
# mosnewfilt -- Make source files to add a filter.
#
# This is specific to the Mosaic pipeline.
#
# Usage: mosnewfilt det fname sname SN photindx calops type lamrange pmassid
#
# All fields except pmassid must include single quotes.
#
# Example:
# In the following example the new filters are put in a file to avoid typing errors.
# The file is sourced to generate the commands for an update.
# After verifying this file it is sourced to update things.
#
# $cat new
# mosnewfilt "'CCDMosaThin1'" "'Us solid U k1044'" "'Us'" "'k1044'" "'U'" "'XOTZFWSBPG'" "'broad'" "'3552 630.71 kpno'" 81
# mosnewfilt "'CCDMosaThin1'" "'Ud Dey k1045'" "'Ud'" "'k1045'" "'U'" "'XOTZFWSBPG'" "'broad'" "'3552 630.71 kpno'" 82
# source new > sourceMe
# $ vi sourceMe
# $ source sourceMe
# $ rm sourceMe

if ($# != 9) exit 1

set detector = "$1"
set fullname = "$2"
set shortname = "$3"
set SN = "$4"
set photindx = "$5"
set calops = "$6"
set type = "$7"
set lamrange = "$8"
set pmassid = $9


# Add to the static dmMaster.
echo
echo cd $NHPPS_PIPESRC/MOSAIC/dmMaster
echo echo '"'"50000,60000,NULL,NULL,$fullname,NULL,NULL,'keyword',$type"'"'" >> bandtype/TOC"
echo echo '"'"50000,60000,'CCDMosaThin1',NULL,$fullname,NULL,NULL,'keyword',$calops"'"'" >> calops/TOC"
echo echo '"'"50000,60000,'Mosaic2',NULL,$fullname,NULL,NULL,'keyword',$calops"'"'" >> calops/TOC"
echo echo '"'"50000,60000,NULL,NULL,$fullname,NULL,NULL,'keyword',$lamrange"'"'" >> lamrange/TOC"
echo echo '"'"50000,60000,NULL,NULL,$fullname,NULL,NULL,'keyword',$photindx"'"'" >> photindx/TOC"
echo echo '"'"INSERT INTO Filters ( id, SN, name, description ) VALUES ( $pmassid, $SN, $shortname, $fullname);"'"'" >> pmass.sql"
cd $NHPPS_PIPESRC/MOSAIC/dmMaster
foreach f (pipedata/*/*/subsets.dat)
echo echo '"'"$fullname $shortname"'"'" >> $f"
end

# Make INSERT statements for the active catalog and PMas.
echo
echo cd $DMData/MOSAIC
echo echo '"'"INSERT INTO catalog VALUES('bandtype',50000,60000,NULL,NULL,$fullname,NULL,NULL,'keyword',$type);"'"' "| sqlite $DMDB"
echo echo '"'"INSERT INTO catalog VALUES('calops',50000,60000,'CCDMosaThin1',NULL,$fullname,NULL,NULL,'keyword',$calops);"'"' "| sqlite $DMDB"
echo echo '"'"INSERT INTO catalog VALUES('calops',50000,60000,'Mosaic2',NULL,$fullname,NULL,NULL,'keyword',$calops);"'"' "| sqlite $DMDB"
echo echo '"'"INSERT INTO catalog VALUES('lamrange',50000,60000,NULL,NULL,$fullname,NULL,NULL,'keyword',$lamrange);"'"' "| sqlite $DMDB"
echo echo '"'"INSERT INTO catalog VALUES('photindx',50000,60000,NULL,NULL,$fullname,NULL,NULL,'keyword',$photindx);"'"' "| sqlite $DMDB"
echo echo '"'"INSERT INTO Filters ( id, SN, name, description ) VALUES ( $pmassid, $SN, $shortname, $fullname);"'"' "| sqlite $DMDB"
